//
//  AppDelegate.h
//  BrEstate
//
//  Created by BrEstate LTDA on 10/23/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IAPHelper.h"
#import "FirebaseUtils.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>


@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) FirebaseUtils *fireBaseUtils;

- (IAPHelper *)iapHelper;

@end
