
//  AppDelegate.m
//  BrEstate
//
//  Created by BrEstate LTDA on 10/23/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "AppDelegate.h"
#import "AddressBookHelper.h"
#import "ImportarArquivoViewController.h"
#import "LoginViewController.h"
#import "ListaEventosViewController.h"
#import "EventosViewController.h"
#import "Evento+AlarmeLocalNotification.h"
#import "Sms.h"
#import "SmsManager.h"
#import "Carga.h"
#import "CRMUtils.h"
#import "ASIS3Request.h"

@interface AppDelegate ()

@property (nonatomic, strong) IAPHelper *iapHelper;

@end


@implementation AppDelegate

- (IAPHelper *)iapHelper
{
    if (_iapHelper) {
        return _iapHelper;
    }
    
    _iapHelper = [[IAPHelper alloc] init];
    
    [[SKPaymentQueue defaultQueue] addTransactionObserver:_iapHelper];
    
    return _iapHelper;
}


- (FirebaseUtils *)fireBaseUtils
{
    if (_fireBaseUtils) {
        return _fireBaseUtils;
    }
    
    _fireBaseUtils = [[FirebaseUtils alloc] init];
    
    return _fireBaseUtils;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self setVersion];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    // Flurry
    [Flurry setCrashReportingEnabled:YES];
    [Flurry setAppVersion:[Utils valueFromDefaults:@"version"]];
    
#ifdef DEBUG
    [Flurry startSession:@"KYKBWW738F8W27PGZ9ZK"]; // Desenv version
#else
    [Flurry startSession:@"7TZWRZH6TX7GKCKCGYN5"]; // Prod version
#endif
    
    // amazon s3
    [ASIS3Request setSharedSecretAccessKey:S3_SECRET_KEY];
    [ASIS3Request setSharedAccessKey:S3_ACCESS_KEY];
    
    // Environments
    NSLog(@"\n\nAmbiente Firebase - %@ \n\n", FIREBASE_APPCI_URL);
    NSLog(@"\n\nAmbiente Amazon - %@ \n", S3_BUCKET_NAME);
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        // Required so UIAlertView shows in the correct orientation in landscape mode
        [application setStatusBarOrientation:
         UIInterfaceOrientationLandscapeRight animated:NO];
    }
    
    [[UITextField appearance] setKeyboardAppearance:UIKeyboardAppearanceDark];
    
    [[JVFloatLabeledTextField appearance] setFloatingLabelFont:[UIFont fontWithName:@"HelveticaNeue" size:11]];
    [[JVFloatLabeledTextField appearance] setFloatingLabelActiveTextColor:[UIColor colorWithRed:0.086 green:0.494 blue:0.984 alpha:1]];
    [[JVFloatLabeledTextField appearance] setFloatingLabelTextColor:[UIColor grayColor]];
    
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    
    // Verifica se o app foi iniciado a partir de uma Local Notification.
    UILocalNotification *localNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    
    if (localNotif) {
        [self processLocalNotificationOnAppStart:localNotif];
    }
    
    // Push Notifications
    if (IS_OS_8_OR_LATER) {
        
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound
                                          categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
    } else {
        
        [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge|
         UIRemoteNotificationTypeAlert|
         UIRemoteNotificationTypeSound];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    // Clean sync flags
    [defaults removeObjectForKey:COUNT_SYNC];
    [defaults removeObjectForKey:SYNC_ERROR_MESSAGE_FOTO];
    [defaults removeObjectForKey:SYNC_ERROR_MESSAGE_NOTA];
    [defaults removeObjectForKey:SYNC_ERROR_MESSAGE_ACOMP];
    
    [defaults synchronize];
    
    [MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:@"BrEstate.sqlite"];
    
    NSPersistentStoreCoordinator *coordinator = [NSManagedObjectContext MR_defaultContext].persistentStoreCoordinator;
    
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             @{@"journal_mode": @"DELETE"}, NSSQLitePragmasOption,nil];
    
    
    NSError *error = nil;
    [coordinator addPersistentStoreWithType:NSSQLiteStoreType
                              configuration:@"Domain"
                                        URL:[self sourceStoreURL]
                                    options:options
                                      error:&error];
    
    [Utils initFrameworks];
    
    //    NSManagedObjectContext *context = [NSManagedObjectContext MR_contextForCurrentThread];
    //    Carga *carga = [[Carga alloc] initWithMoc:context];
    //    [carga iniciar];
    //    [context MR_saveToPersistentStoreAndWait];
    
    return YES;
}

- (NSURL *)sourceStoreURL
{
    NSError *error;
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSURL *urlDefaultData = [[fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    urlDefaultData = [urlDefaultData URLByAppendingPathComponent:@"DefaultData"];
    
    NSURL *urlVersion = [urlDefaultData URLByAppendingPathComponent:version];
    
    BOOL isDir = YES;
    
    if ([fileManager fileExistsAtPath:urlVersion.path isDirectory:&isDir]) {
        
        [urlVersion setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
        urlVersion = [urlVersion URLByAppendingPathComponent:SOURCE_STORE_FILE_NAME];
        
    } else {
        [fileManager removeItemAtURL:urlDefaultData error:&error];
        
        error = nil;
        [fileManager createDirectoryAtURL:urlVersion withIntermediateDirectories:YES attributes:nil error:&error];
        
        error = nil;
        [urlVersion setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
        
        urlVersion = [urlVersion URLByAppendingPathComponent:SOURCE_STORE_FILE_NAME];
        
        NSURL *urlSourceStoreFromBundle = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                                                  pathForResource:[SOURCE_STORE_FILE_NAME stringByDeletingPathExtension]
                                                                  ofType:[SOURCE_STORE_FILE_NAME pathExtension]]];
        
        NSData *fileData = [NSData dataWithContentsOfURL:urlSourceStoreFromBundle];
        [fileData writeToURL:urlVersion atomically:YES];
    }
    
    return urlVersion;
    
    //    return [NSURL fileURLWithPath:[[NSBundle mainBundle]
    //                                 pathForResource:[SOURCE_STORE_FILE_NAME stringByDeletingPathExtension]
    //                                   ofType:[SOURCE_STORE_FILE_NAME pathExtension]]];
}


#pragma mark - Parse Push Notifications
//- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
//{
//    // Store the deviceToken in the current installation and save it to Parse.
//    //    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
//    //    [currentInstallation setDeviceTokenFromData:deviceToken];
//    //    [currentInstallation saveInBackground];
//}

//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
//    //    [PFPush handlePush:userInfo];
//}

//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
//{
//    //    NSString *token = [[PFInstallation currentInstallation] deviceToken];
//    //
//    //    if (userInfo != nil && [[userInfo valueForKey:@"token"] isEqualToString:token]) {
//    //        completionHandler(UIBackgroundFetchResultNoData);
//    //    } else if ([[userInfo valueForKey:@"action"] isEqualToString:@"delete"]) {
//    //        [self deleteEntity:[userInfo valueForKey:@"entity"] withID:[userInfo valueForKey:@"parseID"]];
//    //        completionHandler(UIBackgroundFetchResultNewData);
//    //    }
//    //
//    //    [PFPush handlePush:userInfo];
//}


- (void)deleteEntity:(NSString *)name withID:(NSString *)parseID
{
    __block NSString *className = name;
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"parseID = %@",parseID];
        NSManagedObject *obj = [[CoreDataUtils fetchEntity:className withPredicate:predicate inContext:localContext] firstObject];
        
        if (obj != nil) {
            [obj MR_deleteInContext:localContext];
            //            [localContext deleteObject:obj];
        }
        
    } completion:^(BOOL success, NSError *error) {
        if ([className isEqualToString:@"Cliente"]) {
            className = @"Contato";
        }
        [Utils postEntitytNotification:className];
    }];
}


#pragma mark - Receive notification

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    [self processLocalNotificationOnAppAlreadyRunning:notification];
}


#pragma mark - Treatment for local notification

- (void)processLocalNotificationOnAppStart:(UILocalNotification *)notification
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSArray *eventoAlarme = [notification.userInfo objectForKey:@"EventoAlarme"];
    NSString *codigoSMS = [notification.userInfo objectForKey:@"SMS"];
    
    // Verifica se é um alarme de evento.
    if (eventoAlarme) {
        
        // Guarda o código do evento no user defaults para ser utilizado no ListaEventosViewController após a inicialização do app.
        // O primeiro objeto do array é o código do evento.
        NSNumber *codigoEvento = [eventoAlarme firstObject];
        
        [defaults setObject:codigoEvento forKey:EVENTO_NOTIFICATION];
        [defaults synchronize];
        
    } else if (codigoSMS) { // Verifica se é uma confirmação de SMS.
        
        // Guarda o código do sms no user defaults para ser utilizado no ListaEventosViewController após a inicialização do app.
        [defaults setObject:codigoSMS forKey:SMS_NOTIFICATION];
        [defaults synchronize];
    }
    
    [[UIApplication sharedApplication] cancelLocalNotification:notification];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}


- (void)processLocalNotificationOnAppAlreadyRunning:(UILocalNotification *)notification
{
    UIApplication *application = [UIApplication sharedApplication];
    
    NSArray *eventoAlarme = [notification.userInfo objectForKey:@"EventoAlarme"];
    NSString *codigoSMS = [notification.userInfo objectForKey:@"SMS"];
    
    // Verifica se é um alarme de evento.
    if (eventoAlarme) {
        
        // Se o app está inativo significa que ele foi aberto a partir da notification.
        if (application.applicationState == UIApplicationStateInactive) {
            
            NSNumber *codigoEvento = [eventoAlarme firstObject];
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@", codigoEvento];
            NSArray *eventos = [Evento MR_findAllWithPredicate:predicate];
            if (eventos.count > 0) {
                Evento *evento = [eventos firstObject];
                [self showEvento:evento];
            }
            
        } else if (application.applicationState == UIApplicationStateActive) { // Se a notification ocorreu com o app aberto exibe alert.
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Evento" message:notification.alertBody delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
        }
        
    } else if (codigoSMS) { // Verifica se é uma confirmação de SMS.
        
        // Se o app está inativo significa que ele foi aberto a partir da notification.
        if (application.applicationState == UIApplicationStateInactive) {
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigoSMS = %@", codigoSMS];
            NSArray *smsArray = [Sms MR_findAllWithPredicate:predicate];
            
            
            if (smsArray.count > 0) {
                Sms *sms = [smsArray firstObject];
                [self showEvento:sms.evento];
            }
            
        } else if (application.applicationState == UIApplicationStateActive) { // Se a notification ocorreu com o app aberto exibe alert.
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirmação de SMS" message:notification.alertBody delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
        }
        
    } else { // Outros tipos de notification
        
        if (application.applicationState == UIApplicationStateActive) {
            [Utils showMessage:notification.alertBody];
        }
    }
    
    [application cancelLocalNotification:notification];
    application.applicationIconBadgeNumber = 0;
}


#pragma mark - Show evento for notification

- (void)showEvento:(Evento *)evento
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"_Eventos_iPad" bundle:nil];
        
        UINavigationController *navController = [storyboard instantiateInitialViewController];
        EventosViewController *controller = [[navController viewControllers] objectAtIndex:0];
        
        controller.selectedEvento = evento;
        
        UINavigationController *rootNavController = (UINavigationController *)self.window.rootViewController;
        
        [rootNavController setViewControllers:@[controller] animated:YES];
        
    } else {
        LoginViewController *loginController = (LoginViewController *)self.window.rootViewController;
        UITabBarController *tabController = (UITabBarController *)loginController.presentedViewController;
        tabController.selectedIndex = 1;
        
        UINavigationController *navController = (UINavigationController *)tabController.selectedViewController;
        ListaEventosViewController *controller = [[navController viewControllers] firstObject];
        
        [controller selectRowWithEvento:evento];
    }
}


#pragma mark - External File Import

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if (url != nil && [url isFileURL]) {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            UINavigationController *navController = (UINavigationController *)self.window.rootViewController;
            BaseViewController *controller = (BaseViewController *)[navController.viewControllers firstObject];
            
            [controller importFileURL:url];
            
        } else {
            // Obtem o controller que está sendo apresentado para exibir o controller de importação de arquivo.
            LoginViewController *loginController = (LoginViewController *)self.window.rootViewController;
            UITabBarController *tabController = (UITabBarController *)loginController.presentedViewController;
            UINavigationController *navController = (UINavigationController *)tabController.selectedViewController;
            UIViewController *topController = [[navController viewControllers] lastObject];
            
            // Obtem o controller de importação de arquivos.
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Acessorios_iPhone" bundle:nil];
            UINavigationController *navImportController = [storyboard instantiateViewControllerWithIdentifier:@"ImportarArquivoViewController"];
            ImportarArquivoViewController *importarArquivoController = [navImportController.viewControllers firstObject];
            importarArquivoController.urlArquivo = url;
            
            // Se o topController estiver apresentando algum controller como modal, faz o dismiss.
            if ([topController presentedViewController]) {
                [[topController presentedViewController] dismissViewControllerAnimated:NO completion:^{
                    [topController presentViewController:navImportController animated:YES completion:nil];
                }];
                
            } else {
                [topController presentViewController:navImportController animated:YES completion:nil];
            }
        }
    }
    
    return YES;
}


#pragma mark - Application State Changes

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    application.applicationIconBadgeNumber = 0;
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    [MagicalRecord cleanUp];
    
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSURL *urlDefaultStore = [[fileManager URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask] lastObject];
    urlDefaultStore = [urlDefaultStore URLByAppendingPathComponent:SOURCE_STORE_FILE_NAME];
    [fileManager removeItemAtURL:urlDefaultStore error:&error];
    
    NSURL *urlOtherFiles = [urlDefaultStore URLByAppendingPathComponent:@"-shm"];
    [fileManager removeItemAtURL:urlOtherFiles error:&error];
    
    urlOtherFiles = [urlDefaultStore URLByAppendingPathComponent:@"-wal"];
    [fileManager removeItemAtURL:urlOtherFiles error:&error];
}


#pragma mark - Background Fetch

- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    // Verifica se o plano TOP está para terminar para gerar uma notification.
    [self.iapHelper checkPlanTopWillEndToCreateNotification];
    
    // Verifica se o plano TOP terminou para gerar uma notification.
    [self.iapHelper checkPlanTopIsOverToCreateNotification];
    
    [CRMUtils crmUpdateCheck];
    
    // Verifica se houve resposta das confirmações de SMS.
    [SmsManager checkConfirmationSmsWithMoc:[NSManagedObjectContext MR_contextForCurrentThread] andCompletionBlock:^(BOOL newData) {
        if (newData) {
            completionHandler(UIBackgroundFetchResultNewData);
        } else {
            completionHandler(UIBackgroundFetchResultNoData);
        }
    }];
    
}


#pragma mark - Set Version

// this function detects what is the CFBundle version of this application and set it in the settings bundle
- (void)setVersion
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *build = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    
    NSString *customVersion = [NSString stringWithFormat:@"%@ (%@)",version,build];
    
    //    [defaults setBool:YES forKey:@"iCloudEnabled"];
    [defaults setObject:customVersion forKey:@"version"];
    [defaults synchronize];
}

@end
