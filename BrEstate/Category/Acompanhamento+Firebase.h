//
//  Acompanhamento+Firebase.h
//  Appci
//
//  Created by Leonardo Barros on 08/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "Acompanhamento.h"

@interface Acompanhamento (Firebase)

- (NSString *)createFirebaseID;
- (void)saveFirebase;
- (void)deleteFirebase;
- (void)uploadToS3;

+ (void)saveAllPendingFirebase;
+ (void)startFirebaseObservers;
+ (void)convertAcompanhamentosInBatchWithDictionary:(NSDictionary *)acompDict;
+ (void)sendAllPendingAcompanhamentosToAmazon;

@end
