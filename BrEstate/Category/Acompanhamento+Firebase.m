//
//  Acompanhamento+Firebase.m
//  Appci
//
//  Created by Leonardo Barros on 08/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "Acompanhamento+Firebase.h"
#import "Imovel+Firebase.h"
#import "Cliente+Firebase.h"
#import "S3Utils.h"
#import "ASIS3ObjectRequest.h"

@implementation Acompanhamento (Firebase)


#pragma mark - Create Firebase ID

- (NSString *)createFirebaseID
{
    if (self.firebaseID) {
        return self.firebaseID;
    }
    
    Firebase *classRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Acompanhamento"];
    
    Firebase *objectRef = [classRef childByAutoId];
    
    self.firebaseID = objectRef.key;
    
    //    if (self.arquivo != nil) {
    //        [self uploadToS3];
    //    }
    
    [self.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    return objectRef.key;
}


#pragma mark - Dictionary Firebase

- (NSDictionary *)dictionaryFirebase
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    NSString *dataString = [DateUtils stringTimestampFromDate:self.timeStamp];
    
    [dic setObject:self.codigo != nil ? self.codigo : [NSNull null] forKey:@"codigo"];
    [dic setObject:self.titulo != nil ? self.titulo : [NSNull null] forKey:@"titulo"];
    [dic setObject:self.timeStamp != nil ? dataString : [NSNull null] forKey:@"timeStamp"];
    [dic setObject:self.filename != nil ? self.filename : [NSNull null] forKey:@"filename"];
    [dic setObject:self.extensao != nil ? self.extensao : [NSNull null] forKey:@"extensao"];
    
    [dic setObject:[Utils deviceID] forKey:@"deviceToken"];
    
    // version
    if (!self.version) {
        self.version = [NSDate date];
    }
    [dic setObject:[DateUtils stringTimestampFromDate:self.version] forKey:@"version"];
    
    // firebaseID
    [dic setObject:self.createFirebaseID forKey:@"firebaseID"];
    
    // imovel
    if (self.imovel) {
        [dic setObject:self.imovel.getRelationshipFirebase forKey:@"imovel"];
    } else {
        [dic setObject:[NSNull null] forKey:@"imovel"];
    }
    
    // cliente
    if (self.cliente) {
        [dic setObject:self.cliente.getRelationshipFirebase forKey:@"cliente"];
    } else {
        [dic setObject:[NSNull null] forKey:@"cliente"];
    }
    
    return dic;
}


#pragma mark - Convert Firebase Dictionary

- (void)convertFirebaseDictionary:(NSDictionary *)dictAcompanhamento withMoc:(NSManagedObjectContext *)moc
{
    self.codigo = dictAcompanhamento[@"codigo"];
    self.titulo = dictAcompanhamento[@"titulo"];
    self.timeStamp = [DateUtils dateFromStringTimestamp:dictAcompanhamento[@"timeStamp"]];
    self.filename = dictAcompanhamento[@"filename"];
    self.extensao = dictAcompanhamento[@"extensao"];
    
    self.firebaseID = dictAcompanhamento[@"firebaseID"];
    
    self.isSavedInFirebase = [NSNumber numberWithBool:YES];
    
    self.version = [DateUtils dateFromStringTimestamp:dictAcompanhamento[@"version"]];
    
    // cliente
    if (dictAcompanhamento[@"cliente"]) {
        NSDictionary *clienteDict = dictAcompanhamento[@"cliente"];
        
        Cliente *cliente = [Cliente MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", clienteDict[@"firebaseID"]]
                                                    inContext:moc];
        
        if (!cliente) {
            cliente = [Cliente MR_createInContext:moc];
            cliente.firebaseID = clienteDict[@"firebaseID"];
            cliente.codigo = clienteDict[@"codigo"];
            cliente.isSavedInFirebase = [NSNumber numberWithBool:YES];
            cliente.nome = @"Baixando informações";
        }
        self.cliente = cliente;
    }
    
    // imovel
    if (dictAcompanhamento[@"imovel"]) {
        NSDictionary *imovelDict = dictAcompanhamento[@"imovel"];
        
        Imovel *imovel = [Imovel MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", imovelDict[@"firebaseID"]]
                                                 inContext:moc];
        
        if (!imovel) {
            imovel = [Imovel MR_createInContext:moc];
            imovel.firebaseID = imovelDict[@"firebaseID"];
            imovel.codigo = imovelDict[@"codigo"];
            imovel.isSavedInFirebase = [NSNumber numberWithBool:YES];
        }
        self.imovel = imovel;
    }
    
    if (self.filename) {
        [self downloadFromS3];
    }
    
}


#pragma mark - Delete Firebase

- (void)deleteFirebase
{
    [[Utils firebaseUtils] deleteObjectWithID:self.firebaseID andClassName:@"Acompanhamento"];
    
    // amazon s3
    [self deleteFromS3];
}


#pragma mark - Save Firebase

- (void)saveFirebase
{
    self.isSavedInFirebase = [NSNumber numberWithBool:NO];
    
    // If not TOP don't save on Firebase.
    if (![[Utils iapHelper] checkPlanTopFromController:nil withMessage:nil]) {
        [self.managedObjectContext MR_saveToPersistentStoreAndWait];
        return;
    }
    
    [[Utils firebaseUtils] saveObjectWithDictionary:self.dictionaryFirebase andClassName:@"Acompanhamento" andCompletionBlock:^(NSError *error) {
        [self.managedObjectContext performBlock:^{
            if (!error) {
                self.isSavedInFirebase = [NSNumber numberWithBool:YES];
                
                if (self.arquivo != nil) {
                    [self uploadToS3];
                }
                
            }
            [self.managedObjectContext MR_saveToPersistentStoreAndWait];
        }];
    }];
}


+ (void)saveAllPendingFirebase
{
    NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSavedInFirebase = NO OR isSavedInFirebase = nil"];
    
    NSArray *acompanhamentos = [Acompanhamento MR_findAllWithPredicate:predicate inContext:moc];
    if (acompanhamentos.count == 0) {
        return;
    }
    
    NSMutableDictionary *acompanhamentosDic = [NSMutableDictionary new];
    
    for (Acompanhamento *acompanhamento in acompanhamentos) {
        
        if (acompanhamento.arquivo != nil && acompanhamento.filename == nil) {
            acompanhamento.filename = [NSString stringWithFormat:@"%@-%@",[DateUtils stringTimestampFromDate:[NSDate date]], acompanhamento.codigo];
        }
        
        [acompanhamentosDic setValue:acompanhamento.dictionaryFirebase forKey:acompanhamento.firebaseID];
    }
    
    dispatch_semaphore_t semaAcompanhamento = dispatch_semaphore_create(0);
    
    [moc MR_saveToPersistentStoreAndWait];
    
    Firebase *acompanhamentoRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Acompanhamento"];
    [acompanhamentoRef updateChildValues:acompanhamentosDic withCompletionBlock:^(NSError *error, Firebase *ref) {
        
        [moc performBlock:^{
            if (!error) {
                for (Acompanhamento *acompanhamento in acompanhamentos) {
                    acompanhamento.isSavedInFirebase = [NSNumber numberWithBool:YES];
                }
                [moc MR_saveToPersistentStoreAndWait];
            }
            
            [Utils postEntitytNotificationWithValue:@"UpdateSyncCounter" value:acompanhamentos.count];
            dispatch_semaphore_signal(semaAcompanhamento);
        }];
    }];
    
    dispatch_semaphore_wait(semaAcompanhamento, DISPATCH_TIME_FOREVER);
}


#pragma mark - Convert Acompanhamentos In Batch

+ (void)convertAcompanhamentosInBatchWithDictionary:(NSDictionary *)acompDict
{
    if (!acompDict || [acompDict isEqual:[NSNull null]] || acompDict.count == 0) {
        return;
    }
    
    NSManagedObjectContext *context = [NSManagedObjectContext MR_contextForCurrentThread];
    
    [context performBlockAndWait:^{
        
        BOOL hasNew = NO;
        NSNumber *alreadyDownloadAll = [Utils valueFromDefaults:FIREBASE_ALREADY_DOWNLOAD_ALL];
        
        for (NSString *firebaseID in acompDict.allKeys) {
            
            NSDictionary *oneAcomp = [acompDict objectForKey:firebaseID];
            
            // Check if is not the first download and the change was made by the current device.
            if (alreadyDownloadAll && [oneAcomp[@"deviceToken"] isEqualToString:[Utils deviceID]]) {
                continue;
            }
            
            Acompanhamento *acomp = [Acompanhamento MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", firebaseID] inContext:context];
            
            if (!acomp) {
                
                // Necessary because of the migration from Parse where users have more than one device.
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"timeStamp = %@ AND titulo = %@", [DateUtils dateFromStringTimestamp:oneAcomp[@"timeStamp"]], oneAcomp[@"titulo"]];
                acomp = [Acompanhamento MR_findFirstWithPredicate:predicate inContext:context];
                
                if (!acomp) {
                    acomp = [Acompanhamento MR_createInContext:context];
                }
                
            } else {
                NSString *objVersion = oneAcomp[@"version"];
                NSString *lastVersion = [Utils valueFromDefaults:FIREBASE_ACOMPANHAMENTO_INSERT_VERSION];
                
                if ([objVersion compare:lastVersion] == NSOrderedAscending || [objVersion compare:lastVersion] == NSOrderedSame) {
                    continue;
                }
            }
            
            hasNew = YES;
            
            [acomp convertFirebaseDictionary:oneAcomp withMoc:context];
        }
        
        if (hasNew) {
            [context MR_saveToPersistentStoreAndWait];
            
            NSArray *acompArray = [Acompanhamento MR_findAllSortedBy:@"version" ascending:YES inContext:context];
            Acompanhamento *lastAcomp = acompArray.lastObject;
            
            [Utils addObjectToDefaults:[DateUtils stringTimestampFromDate:lastAcomp.version] withKey:FIREBASE_ACOMPANHAMENTO_INSERT_VERSION];
        }
    }];
}


#pragma mark - Observers

+ (void)startFirebaseObservers
{
    [Acompanhamento startObserverAddedAndChangeAcompanhamento];
}


+ (void)startObserverAddedAndChangeAcompanhamento
{
    Firebase *acompanhamentoRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Acompanhamento"];
    
    NSString *versionString = [Utils valueFromDefaults:FIREBASE_ACOMPANHAMENTO_INSERT_VERSION];
    
    if (!versionString) {
        versionString = [DateUtils stringTimestampFromDate:[NSDate dateWithTimeIntervalSince1970:1]];
    }
    
    // Add 1 millisecond because the firebase query is always >=
    versionString = [NSString stringWithFormat:@"%lld", [versionString longLongValue]+1];
    
    [[[acompanhamentoRef queryOrderedByChild:@"version"] queryStartingAtValue:versionString]
     observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
         
         if ([snapshot.value isEqual:[NSNull null]]) {
             return;
         }
         
         dispatch_async([Utils firebaseUtils].queueDownload, ^{
             
             NSDictionary *acompDict = snapshot.value;
             
             [Acompanhamento convertAcompanhamentosInBatchWithDictionary:acompDict];
         });
     }];
}


#pragma mark - Amazon S3

- (void)uploadToS3Synchronous
{
    self.pendingUpload = [NSNumber numberWithBool:YES];
    [self.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    if(![Utils isConnected]) {
        return;
    }
    
    // If not TOP don't save on Firebase.
    if (![[Utils iapHelper] checkPlanTopFromController:nil withMessage:nil]) {
        return;
    }
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    // write to file
    if (!self.filename || [self.filename isEqualToString:@""]) {
        self.filename = [NSString stringWithFormat:@"%@-%@", [DateUtils stringTimestampFromDate:[NSDate date]], self.codigo];
    }
    
    NSString *filename = [NSString stringWithFormat:@"%@.%@",self.filename, self.extensao];
    NSString *path = [NSString stringWithFormat:@"%@acompanhamentos/%@", NSTemporaryDirectory(), filename];
    [[NSFileManager defaultManager] createDirectoryAtPath:[path stringByDeletingLastPathComponent] withIntermediateDirectories:YES attributes:nil error:nil];
    
    if ([self.arquivo writeToFile:path atomically:YES]) {
        
        
        NSURL *fileURL = [NSURL fileURLWithPath:path];
        
        // [AWSLogger defaultLogger].logLevel = AWSLogLevelVerbose;
        NSString *userID = [Utils valueFromDefaults:FIREBASE_USER_ID];
        NSString *fileName = [fileURL lastPathComponent];
        NSString *filePath = [NSString stringWithFormat:@"%@/acompanhamentos/%@",userID,fileName];
        
        ASIS3ObjectRequest *request =
        [ASIS3ObjectRequest PUTRequestForFile:fileURL.path withBucket:S3_BUCKET_NAME key:filePath];
        
        [request setTimeOutSeconds:60];
        [request startSynchronous];
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        if (request.error) {
            NSLog(@"%@", request.error);
            
        } else {
            
            [self.managedObjectContext performBlockAndWait:^{
                self.pendingUpload = [NSNumber numberWithBool:NO];
                [self.managedObjectContext MR_saveToPersistentStoreAndWait];
            }];
            
        }
        
    }
}


- (void)uploadToS3
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self uploadToS3Synchronous];
    });
}


- (void)deleteFromS3
{
    
    if ([self.pendingUpload boolValue]) {
        return;
    }
    
    NSString *email = [Utils valueFromDefaults:FIREBASE_USER_ID];
    
    NSString *filename = [NSString stringWithFormat:@"%@.%@",self.filename, self.extensao];
    NSString *filePath = [NSString stringWithFormat:@"%@/acompanhamentos/%@",email,filename];
    
    [S3Utils delete:filePath];
    
}

- (void)downloadFromS3
{
    if (self.arquivo) {
        self.pendingUpload = [NSNumber numberWithBool:NO];
        return;
    }
    
    @autoreleasepool {
        
        self.pendingDownload = [NSNumber numberWithBool:YES];
        NSString *userID = [Utils valueFromDefaults:FIREBASE_USER_ID];
        NSString *filePath = [NSString stringWithFormat:@"%@/acompanhamentos/%@.%@",userID,self.filename,self.extensao];
        self.arquivo = [S3Utils download:filePath];
        
        if (self.arquivo != nil) {
            self.pendingUpload = [NSNumber numberWithBool:NO];
            self.pendingDownload = [NSNumber numberWithBool:NO];
        }
    }
}


#pragma mark - Send All Pending Acompanhamentos to Amazon

+ (void)sendAllPendingAcompanhamentosToAmazon
{
    NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
    
    [moc performBlockAndWait:^{
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"arquivo != nil AND (pendingUpload = YES OR pendingUpload = nil)"];
        
        NSArray *acompanhamentos = [Acompanhamento MR_findAllWithPredicate:predicate inContext:moc];
        
        __block BOOL hasError = NO;
        
        for (Acompanhamento *acomp in acompanhamentos) {
            
            [acomp uploadToS3Synchronous];
            [moc MR_saveToPersistentStoreAndWait];
        }
        
        if (hasError) {
            [Utils addObjectToDefaults:@"Ocorreu erro no envio dos arquivos de alguns acompanhamentos."
                               withKey:SYNC_ERROR_MESSAGE_ACOMP];
            
        } else {
            [Utils removeObjectFromDefaults:SYNC_ERROR_MESSAGE_ACOMP];
        }
    }];
}


@end
