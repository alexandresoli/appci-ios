//
//  Bairro+Firebase.h
//  Appci
//
//  Created by Leonardo Barros on 26/12/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Bairro.h"

@interface Bairro (Firebase)

- (void)saveFirebase;
- (void)deleteFirebase;
- (NSString *)createFirebaseID;
- (NSDictionary *)dictionaryFirebase;
- (NSDictionary *)getRelationshipFirebase;

+ (void)saveAllPendingFirebase;
+ (void)startFirebaseObservers;
+ (void)convertBairrosInBatchWithDictionary:(NSDictionary *)bairrosDict;

@end
