//
//  Bairro+Firebase.m
//  Appci
//
//  Created by Leonardo Barros on 26/12/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Bairro+Firebase.h"
#import "Endereco+Firebase.h"
#import "Imovel+Firebase.h"
#import "Cliente+Firebase.h"
#import "PerfilCliente+Firebase.h"


@implementation Bairro (Firebase)


#pragma mark - Dictionary Firebase

- (NSDictionary *)dictionaryFirebase
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    [dic setObject:self.codigo != nil ? self.codigo : [NSNull null] forKey:@"codigo"];
    [dic setObject:self.nome != nil ? self.nome : [NSNull null] forKey:@"nome"];
    [dic setObject:self.cidade != nil ? self.cidade : [NSNull null] forKey:@"cidade"];
    [dic setObject:[Utils deviceID] forKey:@"deviceToken"];
    
    // firebaseID
    [dic setObject:self.createFirebaseID forKey:@"firebaseID"];
    
    
    // version
    if (!self.version) {
        self.version = [NSDate date];
    }
    [dic setObject:[DateUtils stringTimestampFromDate:self.version] forKey:@"version"];
    
    return dic;
}


#pragma mark - Delete Firebase

- (void)deleteFirebase
{
    [[Utils firebaseUtils] deleteObjectWithID:self.firebaseID andClassName:@"Bairro"];
    
    // Remove reference on Endereco.
    NSArray *enderecos = self.endereco.allObjects;
    for (Endereco *endereco in enderecos) {
        endereco.bairro = nil;
        
        for (Imovel *imovel in endereco.imovel) {
            [imovel saveFirebase];
        }
        
        for (Cliente *cliente in endereco.cliente) {
            [cliente saveFirebase];
        }
    }
    
    // Remove reference on Perfil.
    NSArray *perfis = self.perfil.allObjects;
    for (PerfilCliente *perfil in perfis) {
        [perfil removeBairroObject:self];
        [perfil saveFirebase];
    }
}


#pragma mark - Save Firebase

- (void)saveFirebase
{
    self.isSavedInFirebase = [NSNumber numberWithBool:NO];
    
    // If not TOP don't save on Firebase.
    if (![[Utils iapHelper] checkPlanTopFromController:nil withMessage:nil]) {
        [self.managedObjectContext MR_saveToPersistentStoreAndWait];
        return;
    }
    
    [[Utils firebaseUtils] saveObjectWithDictionary:self.dictionaryFirebase andClassName:@"Bairro" andCompletionBlock:^(NSError *error) {
        [self.managedObjectContext performBlock:^{
            if (!error) {
                self.isSavedInFirebase = [NSNumber numberWithBool:YES];
            }
            [self.managedObjectContext MR_saveToPersistentStoreAndWait];
        }];
    }];
}


+ (void)saveAllPendingFirebase
{
    NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSavedInFirebase = NO OR isSavedInFirebase = nil"];
    
    NSArray *bairros = [Bairro MR_findAllWithPredicate:predicate inContext:moc];
    if (bairros.count == 0) {
        return;
    }
    
    NSMutableDictionary *bairrosDic = [NSMutableDictionary new];
    
    for (Bairro *bairro in bairros) {
        [bairrosDic setValue:bairro.dictionaryFirebase forKey:bairro.firebaseID];
    }
    
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    
    [moc MR_saveToPersistentStoreAndWait];
    
    Firebase *bairroRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Bairro"];
    [bairroRef updateChildValues:bairrosDic withCompletionBlock:^(NSError *error, Firebase *ref) {
        
        [moc performBlock:^{
            if (!error) {
                for (Bairro *bairro in bairros) {
                    bairro.isSavedInFirebase = [NSNumber numberWithBool:YES];
                }
                [moc MR_saveToPersistentStoreAndWait];
            }
            
            [Utils postEntitytNotificationWithValue:@"UpdateSyncCounter" value:bairros.count];
            dispatch_semaphore_signal(sema);
        }];
    }];
    
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
}


#pragma mark - Convert Firebase Dictionary

- (void)convertFirebaseDictionary:(NSDictionary *)dictBairro withMoc:(NSManagedObjectContext *)moc
{
    self.codigo = dictBairro[@"codigo"];
    self.nome = dictBairro[@"nome"];
    self.cidade = dictBairro[@"cidade"];
    
    self.firebaseID = dictBairro[@"firebaseID"];
    
    self.isSavedInFirebase = [NSNumber numberWithBool:YES];
    
    self.version = [DateUtils dateFromStringTimestamp:dictBairro[@"version"]];
}


#pragma mark - Create Firebase ID

- (NSString *)createFirebaseID
{
    if (self.firebaseID) {
        return self.firebaseID;
    }
    
    Firebase *classRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Bairro"];
    
    Firebase *objectRef = [classRef childByAutoId];
    
    self.firebaseID = objectRef.key;
    
    [self.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    return objectRef.key;
}


#pragma mark - Get Relationship Firebase

- (NSDictionary *)getRelationshipFirebase
{
    return @{@"firebaseID": self.createFirebaseID,
             @"codigo": self.codigo};
}


#pragma mark - Convert Bairros In Batch

+ (void)convertBairrosInBatchWithDictionary:(NSDictionary *)bairrosDict
{
    if (!bairrosDict || [bairrosDict isEqual:[NSNull null]] || bairrosDict.count == 0) {
        return;
    }
    
    NSManagedObjectContext *context = [NSManagedObjectContext MR_contextForCurrentThread];
    
    [context performBlockAndWait:^{
        
        BOOL hasNew = NO;
        NSNumber *alreadyDownloadAll = [Utils valueFromDefaults:FIREBASE_ALREADY_DOWNLOAD_ALL];
        
        for (NSString *firebaseID in bairrosDict.allKeys) {
            
            NSDictionary *oneBairro = [bairrosDict objectForKey:firebaseID];
            
            // Check if is not the first download and the change was made by the current device.
            if (alreadyDownloadAll && [oneBairro[@"deviceToken"] isEqualToString:[Utils deviceID]]) {
                continue;
            }
            
            Bairro *bairro = [Bairro MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", firebaseID]
                                                     inContext:context];
            
            if (!bairro) {
                
                // Necessary because of the migration from Parse where users have more than one device.
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"nome = %@ AND cidade = %@", oneBairro[@"nome"], oneBairro[@"cidade"]];
                bairro = [Bairro MR_findFirstWithPredicate:predicate inContext:context];
                
                if (!bairro) {
                    bairro = [Bairro MR_createInContext:context];
                }
                
            } else {
                NSString *objVersion = oneBairro[@"version"];
                NSString *lastVersion = [Utils valueFromDefaults:FIREBASE_BAIRRO_INSERT_VERSION];
                
                if ([objVersion compare:lastVersion] == NSOrderedAscending || [objVersion compare:lastVersion] == NSOrderedSame) {
                    continue;
                }
            }
            
            hasNew = YES;
            
            [bairro convertFirebaseDictionary:oneBairro withMoc:context];
        }
        
        if (hasNew) {
            [context MR_saveToPersistentStoreAndWait];
            
            NSArray *bairrosArray = [Bairro MR_findAllSortedBy:@"version" ascending:YES inContext:context];
            Bairro *lastBairro = bairrosArray.lastObject;
            
            [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:REFRESH_IMOVEL];
            [Utils addObjectToDefaults:[DateUtils stringTimestampFromDate:lastBairro.version] withKey:FIREBASE_BAIRRO_INSERT_VERSION];
        }
    }];
}


#pragma mark - Observers

+ (void)startFirebaseObservers
{
    [Bairro startObserverAddedAndChangeBairro];
}


+ (void)startObserverAddedAndChangeBairro
{
    Firebase *bairroRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Bairro"];
    
    NSString *versionString = [Utils valueFromDefaults:FIREBASE_BAIRRO_INSERT_VERSION];
    
    if (!versionString) {
        versionString = [DateUtils stringTimestampFromDate:[NSDate dateWithTimeIntervalSince1970:1]];
    }
    
    // Add 1 millisecond because the firebase query is always >=
    versionString = [NSString stringWithFormat:@"%lld", [versionString longLongValue]+1];
    
    [[[bairroRef queryOrderedByChild:@"version"] queryStartingAtValue:versionString]
     observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
         
         if ([snapshot.value isEqual:[NSNull null]]) {
             return;
         }
         
         dispatch_async([Utils firebaseUtils].queueDownload, ^{
             
             NSDictionary *bairrosDict = snapshot.value;
             
             [Bairro convertBairrosInBatchWithDictionary:bairrosDict];
         });
     }];
}


@end
