//
//  Cliente+Firebase.h
//  Appci
//
//  Created by Leonardo Barros on 23/12/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Cliente.h"

@interface Cliente (Firebase)

- (NSString *)createFirebaseID;
- (void)saveFirebase;
- (void)deleteFirebase;
- (NSDictionary *)dictionaryFirebase;
- (NSDictionary *)getRelationshipFirebase;

+ (void)saveAllPendingFirebase;
+ (void)startFirebaseObservers;
+ (NSPredicate *)uniquePredicateWithDictionary:(NSDictionary *)dict;
+ (void)convertClientesInBatchWithDictionary:(NSDictionary *)clientesDict;
+ (void)hackDedup;

@end
