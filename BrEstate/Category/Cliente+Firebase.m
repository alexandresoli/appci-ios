//
//  Cliente+Firebase.m
//  Appci
//
//  Created by Leonardo Barros on 23/12/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Cliente+Firebase.h"
#import "Imovel+Firebase.h"
#import "Endereco+Firebase.h"
#import "Bairro+Firebase.h"
#import "Evento+Firebase.h"
#import "Acompanhamento+Firebase.h"
#import "Negociacao+Firebase.h"
#import "PerfilCliente+Firebase.h"
#import "Sms+Firebase.h"
#import "NSString+Custom.h"
#import "ClienteDeletado.h"
#import "Nota.h"
#import "TagImovel.h"

@implementation Cliente (Firebase)


#pragma mark - Dictionary Firebase

- (NSDictionary *)dictionaryFirebase
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    if (self.foto != nil) {
        NSString *fotoString = [self.foto base64EncodedStringWithOptions:0];
        [dic setObject:fotoString forKey:@"foto"];
    }
    
    [dic setObject:self.codigo != nil ? self.codigo : [NSNull null] forKey:@"codigo"];
    [dic setObject:self.email != nil ? self.email : [NSNull null] forKey:@"email"];
    [dic setObject:self.nome != nil ? self.nome : [NSNull null] forKey:@"nome"];
    [dic setObject:self.subtipo != nil ? self.subtipo : [NSNull null] forKey:@"subtipo"];
    [dic setObject:self.telefone != nil ? self.telefone : [NSNull null] forKey:@"telefone"];
    [dic setObject:self.telefoneExtra != nil ? self.telefoneExtra : [NSNull null] forKey:@"telefoneExtra"];
    [dic setObject:[Utils deviceID] forKey:@"deviceToken"];
    
    // version
    if (!self.version) {
        self.version = [NSDate date];
    }
    [dic setObject:[DateUtils stringTimestampFromDate:self.version] forKey:@"version"];
    
    // firebaseID
    [dic setObject:self.createFirebaseID forKey:@"firebaseID"];

    // endereco
    if (self.endereco) {
        [dic setObject:self.endereco.dictionaryFirebase forKey:@"endereco"];
        
    } else {
        [dic setObject:[NSNull null] forKey:@"endereco"];
    }
    
    // perfil
    if (self.perfil) {
        [dic setObject:self.perfil.getRelationshipFirebase forKey:@"perfil"];
    } else {
        [dic setObject:[NSNull null] forKey:@"perfil"];
    }
    
    return dic;
}


#pragma mark - Create Firebase ID

- (NSString *)createFirebaseID
{
    if (self.firebaseID) {
        return self.firebaseID;
    }
    
    Firebase *classRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Cliente"];
    
    Firebase *objectRef = [classRef childByAutoId];
    
    self.firebaseID = objectRef.key;
    
    [self.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    return objectRef.key;
}


#pragma mark - Get Relationship Firebase

- (NSDictionary *)getRelationshipFirebase
{
    return @{@"firebaseID": self.createFirebaseID,
             @"codigo": self.codigo};
}


#pragma mark - Convert Firebase Dictionary

- (void)convertFirebaseDictionary:(NSDictionary *)dictCliente withMoc:(NSManagedObjectContext *)moc
{
    self.codigo = dictCliente[@"codigo"];
    self.nome = dictCliente[@"nome"];
    self.telefone = dictCliente[@"telefone"];
    self.telefoneExtra = dictCliente[@"telefoneExtra"];
    self.email = dictCliente[@"email"];
    self.subtipo = dictCliente[@"subtipo"];
    self.firebaseID = dictCliente[@"firebaseID"];
    
    self.isSavedInFirebase = [NSNumber numberWithBool:YES];
    
    self.version = [DateUtils dateFromStringTimestamp:dictCliente[@"version"]];
    
    // foto
    if (dictCliente[@"foto"]) {
        self.foto = [[NSData alloc] initWithBase64EncodedString:dictCliente[@"foto"] options:0];
    }
    
    // endereco
    if (dictCliente[@"endereco"]) {
        if (!self.endereco) {
            self.endereco = [Endereco MR_createInContext:moc];
        }
        
        [self.endereco convertFirebaseDictionary:dictCliente[@"endereco"] withMoc:moc];
    }
    
    // perfil
    if (dictCliente[@"perfil"]) {
        NSDictionary *perfilDict = dictCliente[@"perfil"];
        
        PerfilCliente *perfil = [PerfilCliente MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", perfilDict[@"firebaseID"]] inContext:moc];
        
        if (perfil) {
            self.perfil = perfil;
            
        } else {
            perfil = [PerfilCliente MR_createInContext:moc];
            perfil.firebaseID = perfilDict[@"firebaseID"];
            perfil.codigo = perfilDict[@"codigo"];
            perfil.isSavedInFirebase = [NSNumber numberWithBool:YES];
            perfil.descricao = @"Baixando informações";
            self.perfil = perfil;
        }
    }
}


#pragma mark - Save Firebase

- (void)saveFirebase
{
    self.isSavedInFirebase = [NSNumber numberWithBool:NO];
    
    // If not TOP don't save on Firebase.
    if (![[Utils iapHelper] checkPlanTopFromController:nil withMessage:nil]) {
        [self.managedObjectContext MR_saveToPersistentStoreAndWait];
        return;
    }
    
    [[Utils firebaseUtils] saveObjectWithDictionary:self.dictionaryFirebase andClassName:@"Cliente" andCompletionBlock:^(NSError *error) {
        [self.managedObjectContext performBlock:^{
            if (!error) {
                self.isSavedInFirebase = [NSNumber numberWithBool:YES];
            }
            [self.managedObjectContext MR_saveToPersistentStoreAndWait];
        }];
    }];
    
    if (self.endereco.bairro) {
        [self.endereco.bairro saveFirebase];
    }
    
    if (self.perfil) {
        [self.perfil saveFirebase];
    }
}


+ (void)saveAllPendingFirebase
{
    NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSavedInFirebase = NO OR isSavedInFirebase = nil"];
    
    NSArray *clientes = [Cliente MR_findAllWithPredicate:predicate inContext:moc];
    if (clientes.count == 0) {
        return;
    }
    
    NSMutableDictionary *clientesDic = [NSMutableDictionary new];
    
    for (Cliente *cliente in clientes) {
        [clientesDic setValue:cliente.dictionaryFirebase forKey:cliente.firebaseID];
    }
    
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    
    [moc MR_saveToPersistentStoreAndWait];
    
    Firebase *clienteRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Cliente"];
    [clienteRef updateChildValues:clientesDic withCompletionBlock:^(NSError *error, Firebase *ref) {
        
        [moc performBlock:^{
            if (!error) {
                for (Cliente *cliente in clientes) {
                    cliente.isSavedInFirebase = [NSNumber numberWithBool:YES];
                }
                [moc MR_saveToPersistentStoreAndWait];
            }
            
            [Utils postEntitytNotificationWithValue:@"UpdateSyncCounter" value:clientes.count];            
            dispatch_semaphore_signal(sema);
        }];
    }];

    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
}


#pragma mark - Delete Firebase

- (void)deleteFirebase
{
    [[Utils firebaseUtils] deleteObjectWithID:self.firebaseID andClassName:@"Cliente"];
    
    // Remove reference on Imovel.
    NSArray *imoveis = self.imovel.allObjects;
    for (Imovel *imovel in imoveis) {
        imovel.cliente = nil;
        [imovel saveFirebase];
    }
    
    // Remove reference on Evento and SMS
    NSArray *smss = self.sms.allObjects;
    for (Sms *sms in smss) {
        sms.cliente = nil;
    }
    
    NSArray *eventos = self.evento.allObjects;
    for (Evento *evento in eventos) {
        [evento removeClienteObject:self];
        [evento saveFirebase];
    }
    
    // Remove reference on Acompanhamento.
    NSArray *acompanhamentos = self.acompanhamento.allObjects;
    for (Acompanhamento *acomp in acompanhamentos) {
        acomp.cliente = nil;

        // Acompanhamento is only for cliente, can delete.
        if (!acomp.imovel) {
            [acomp deleteFirebase];
        } else {
            [acomp saveFirebase];
        }
    }
    
    // Remove reference on Negociacao.
    NSArray *negociacoes = self.negociacao.allObjects;
    for (Negociacao *neg in negociacoes) {
        neg.cliente = nil;
        [neg saveFirebase];
    }
}


#pragma mark - Convert Clientes In Batch

+ (void)convertClientesInBatchWithDictionary:(NSDictionary *)clientesDict
{
    if (!clientesDict || [clientesDict isEqual:[NSNull null]] || clientesDict.count == 0) {
        return;
    }
    
    NSManagedObjectContext *context = [NSManagedObjectContext MR_contextForCurrentThread];
    
    [context performBlockAndWait:^{
        
        BOOL hasNew = NO;
        NSNumber *alreadyDownloadAll = [Utils valueFromDefaults:FIREBASE_ALREADY_RUN];
        
        // Not the first time.
        if (alreadyDownloadAll) {
            
            for (NSString *firebaseID in clientesDict.allKeys) {
                
                NSDictionary *oneCliente = [clientesDict objectForKey:firebaseID];
                
                // Check if was made by the current device.
                if ([oneCliente[@"deviceToken"] isEqualToString:[Utils deviceID]]) {
                    continue;
                }
                
                Cliente *cliente = [Cliente MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", firebaseID]
                                                            inContext:context];
                if (!cliente) {
                    
                    // Necessary because of the migration from Parse where users have more than one device.
                    NSPredicate *predicate = [Cliente uniquePredicateWithDictionary:oneCliente];
                    cliente = [Cliente MR_findFirstWithPredicate:predicate inContext:context];
                    
                    if (!cliente) {
                        cliente = [Cliente MR_createInContext:context];
                    }
                    
                } else {
                    NSString *objVersion = oneCliente[@"version"];
                    NSString *lastVersion = [Utils valueFromDefaults:FIREBASE_CLIENTE_INSERT_VERSION];
                    
                    if ([objVersion compare:lastVersion] == NSOrderedAscending || [objVersion compare:lastVersion] == NSOrderedSame) {
                        continue;
                    }
                }

                hasNew = YES;
                
                [cliente convertFirebaseDictionary:oneCliente withMoc:context];
            }
            
        } else { // First Download
            
            // Sort dictionary by version to the most recent keep on device in duplicates case.
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"version" ascending:NO];
            NSMutableArray *clientesArray = [clientesDict.allValues mutableCopy];
            [clientesArray sortUsingDescriptors:@[descriptor]];
            
            NSMutableArray *checkClientesArray = [NSMutableArray new];
            
            [Cliente MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"codigo > 0"] inContext:context];
            
            for (NSDictionary *oneCliente in clientesArray) {
                
                @autoreleasepool {
                    NSPredicate *predicate = [Cliente uniquePredicateWithDictionary:oneCliente];
                    NSDictionary *checkCliente = [checkClientesArray filteredArrayUsingPredicate:predicate].firstObject;
                    
                    if (!checkCliente) {
                        Cliente *cliente = [Cliente MR_createInContext:context];
                        [cliente convertFirebaseDictionary:oneCliente withMoc:context];
                        
                        [checkClientesArray addObject:oneCliente];
                        
                        hasNew = YES;
                    }
                }
            }
        }
        
        if (hasNew) {
            [context MR_saveToPersistentStoreAndWait];
            
            NSArray *clientesArray = [Cliente MR_findAllSortedBy:@"version" ascending:YES inContext:context];
            Cliente *lastCliente = clientesArray.lastObject;
            [Utils addObjectToDefaults:[DateUtils stringTimestampFromDate:lastCliente.version] withKey:FIREBASE_CLIENTE_INSERT_VERSION];
            
            [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:REFRESH_CLIENTE];
        }
    }];
}


+ (void)dedupAllWithMoc:(NSManagedObjectContext *)moc
{
    NSArray *clientes = [Cliente MR_findAllSortedBy:@"version" ascending:NO inContext:moc];
    
    for (Cliente *cliente in clientes) {
        
        if (![moc existingObjectWithID:cliente.objectID error:NULL]) {
            continue;
        }
        
        NSMutableArray *predicateArray = [NSMutableArray array];
        NSPredicate *predicate = nil;
        
        // name
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"nome = %@", cliente.nome]];
        
        // email
        if (cliente.email == nil || cliente.email == (id)[NSNull null] || cliente.email.trimmed.length == 0) {
            predicate = [NSPredicate predicateWithFormat:@"email = nil or email = ''"];
        } else {
            predicate = [NSPredicate predicateWithFormat:@"email = %@",cliente.email];
        }
        
        [predicateArray addObject:predicate];
        
        NSPredicate *coumpoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
        
        NSArray *dups = [Cliente MR_findAllWithPredicate:coumpoundPredicate inContext:moc];
        
        Cliente *prevCliente;
        
        for (Cliente *dupCliente in dups) {
            
            if (![moc existingObjectWithID:dupCliente.objectID error:NULL]) {
                continue;
            }
            
            if (prevCliente) {
                if (dupCliente.version == nil) {
//                    if (dupCliente.firebaseID) {
//                        [dupCliente deleteFirebase];
//                    }
                    [dupCliente MR_deleteInContext:moc];
                    
                } else if (prevCliente.version == nil) {
//                    if (prevCliente.firebaseID) {
//                        [prevCliente deleteFirebase];
//                    }
                    [prevCliente MR_deleteInContext:moc];
                    prevCliente = dupCliente;
                    
                } else if ([dupCliente.version compare:prevCliente.version] == NSOrderedAscending) {
//                    if (dupCliente.firebaseID) {
//                        [dupCliente deleteFirebase];
//                    }
                    [dupCliente MR_deleteInContext:moc];
                    
                } else {
//                    if (prevCliente.firebaseID) {
//                        [prevCliente deleteFirebase];
//                    }
                    [prevCliente MR_deleteInContext:moc];
                    prevCliente = dupCliente;
                }
                
            } else {
                prevCliente = dupCliente;
            }
        }
    }
    
}


#pragma mark - Unique Predicate

+ (NSPredicate *)uniquePredicateWithDictionary:(NSDictionary *)dict
{
    NSString *email = dict[@"email"];
    NSString *nome = dict[@"nome"];
    
    NSMutableArray *predicateArray = [NSMutableArray array];
    NSPredicate *predicate = nil;
    
    // name
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"nome = %@", nome]];
    
    // email
    if (email == nil || email == (id)[NSNull null] || email.trimmed.length == 0) {
        predicate = [NSPredicate predicateWithFormat:@"email = nil or email = ''"];
    } else {
        predicate = [NSPredicate predicateWithFormat:@"email = %@",email];
    }
    
    [predicateArray addObject:predicate];
    
    NSPredicate *coumpoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    
    return coumpoundPredicate;
}


#pragma mark - Deduplicator

+ (void)dedup:(NSDictionary *)dict withMoc:(NSManagedObjectContext *)moc
{
    NSPredicate *coumpoundPredicate = [Cliente uniquePredicateWithDictionary:dict];
    
    NSArray *clientes = [Cliente MR_findAllWithPredicate:coumpoundPredicate inContext:moc];
    
    Cliente *prevCliente;
    
    for (Cliente *cli in clientes) {
        
        if (![moc existingObjectWithID:cli.objectID error:NULL]) {
            continue;
        }
        
        if (prevCliente) {
            if (cli.version == nil) {
                if (cli.firebaseID) {
                    [cli deleteFirebase];
                }
                [cli MR_deleteInContext:moc];
                
            } else if (prevCliente.version == nil) {
                if (prevCliente.firebaseID) {
                    [prevCliente deleteFirebase];
                }
                [prevCliente MR_deleteInContext:moc];
                prevCliente = cli;
                
            } else if ([cli.version compare:prevCliente.version] == NSOrderedAscending) {
                if (cli.firebaseID) {
                    [cli deleteFirebase];
                }
                [cli MR_deleteInContext:moc];
                
            } else {
                if (prevCliente.firebaseID) {
                    [prevCliente deleteFirebase];
                }
                [prevCliente MR_deleteInContext:moc];
                prevCliente = cli;
            }
            
        } else {
            prevCliente = cli;
        }
    }
}


#pragma mark - Observers

+ (void)startFirebaseObservers
{
    [Cliente startObserverAddedAndChangeCliente];
}


+ (void)startObserverAddedAndChangeCliente
{
    Firebase *clienteRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Cliente"];
    
    NSString *versionString = [Utils valueFromDefaults:FIREBASE_CLIENTE_INSERT_VERSION];
    
    if (!versionString) {
        versionString = [DateUtils stringTimestampFromDate:[NSDate dateWithTimeIntervalSince1970:1]];
    }
    
    // Add 1 millisecond because the firebase query is always >=
    versionString = [NSString stringWithFormat:@"%lld", [versionString longLongValue]+1];
    
    [[[clienteRef queryOrderedByChild:@"version"] queryStartingAtValue:versionString]
     observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        
         if ([snapshot.value isEqual:[NSNull null]]) {
             return;
         }
         
         dispatch_async([Utils firebaseUtils].queueDownload, ^{
             
             NSDictionary *clientesDict = snapshot.value;
             
             [Cliente convertClientesInBatchWithDictionary:clientesDict];
         });
    }];
}


#pragma mark - Hack Dedup

+ (void)hackDedup
{
    NSLog(@"Início Hack Dedup - %@", [NSDate date]);
    
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    
    Firebase *clienteRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Cliente"];
    
    [clienteRef observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        
        if ([snapshot.value isEqual:[NSNull null]]) {
            dispatch_semaphore_signal(sema);
            return;
        }
        
        dispatch_async([Utils firebaseUtils].queueDownload, ^{
            
            NSDictionary *clientesDict = snapshot.value;
            
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"version" ascending:NO];
            NSMutableArray *clientesArray = [clientesDict.allValues mutableCopy];
            [clientesArray sortUsingDescriptors:@[descriptor]];
            
            NSMutableArray *clientesOk = [NSMutableArray new];
            NSMutableArray *clientesDup = [NSMutableArray new];
            
            for (NSDictionary *newCliente in clientesArray) {
                
                @autoreleasepool {
                    NSPredicate *predicate = [Cliente uniquePredicateWithDictionary:newCliente];
                    NSDictionary *prevCliente = [clientesOk filteredArrayUsingPredicate:predicate].firstObject;
                    
                    if (!prevCliente) {
                        [clientesOk addObject:newCliente];
                        continue;
                    }
                    
                    long prevCountKeys = prevCliente.allKeys.count;
                    long newCountKeys = newCliente.allKeys.count;
                    
                    if (newCountKeys > prevCountKeys) {
                        [clientesDup addObject:prevCliente];
                        [clientesOk removeObject:prevCliente];
                        [clientesOk addObject:newCliente];
                        
                    } else {
                        [clientesDup addObject:newCliente];
                    }
                }
            }
            
            dispatch_semaphore_t semaFire = dispatch_semaphore_create(0);
            Firebase *classRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Cliente"];
            [classRef removeValueWithCompletionBlock:^(NSError *error, Firebase *ref) {
                
                NSMutableDictionary *dictToSend = [NSMutableDictionary new];
                for (NSDictionary *dict in clientesOk) {
                    [dictToSend setObject:dict forKey:dict[@"firebaseID"]];
                }
                
                [classRef setValue:dictToSend withCompletionBlock:^(NSError *error, Firebase *ref) {
                    dispatch_semaphore_signal(semaFire);
                }];
                
            }];
            dispatch_semaphore_wait(semaFire, DISPATCH_TIME_FOREVER);
            
            // If already download all delete all local clientes and insert the new ones.
            if ([Utils valueFromDefaults:FIREBASE_ALREADY_DOWNLOAD_ALL]) {
                
                // limpa a data de atualizacao no userdefaults
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                
                // Sync
                [defaults removeObjectForKey:COUNT_SYNC];
                [defaults removeObjectForKey:SYNC_ERROR_MESSAGE_FOTO];
                [defaults removeObjectForKey:SYNC_ERROR_MESSAGE_NOTA];
                [defaults removeObjectForKey:SYNC_ERROR_MESSAGE_ACOMP];
                
                // Firebase
                [defaults removeObjectForKey:FIREBASE_CLIENTE_INSERT_VERSION];
                [defaults removeObjectForKey:FIREBASE_IMOVEL_INSERT_VERSION];
                [defaults removeObjectForKey:FIREBASE_FOTO_INSERT_VERSION];
                [defaults removeObjectForKey:FIREBASE_BAIRRO_INSERT_VERSION];
                [defaults removeObjectForKey:FIREBASE_EVENTO_INSERT_VERSION];
                [defaults removeObjectForKey:FIREBASE_NOTA_INSERT_VERSION];
                [defaults removeObjectForKey:FIREBASE_ACOMPANHAMENTO_INSERT_VERSION];
                [defaults removeObjectForKey:FIREBASE_NEGOCIACAO_INSERT_VERSION];
                [defaults removeObjectForKey:FIREBASE_PERFIL_INSERT_VERSION];
                [defaults removeObjectForKey:FIREBASE_CLIENTE_DELETADO_INSERT_VERSION];
                [defaults removeObjectForKey:FIREBASE_DELETADOS_INSERT_VERSION];
                [defaults removeObjectForKey:FIREBASE_PENDING_DELETED_OBJECTS_DICT];
                [defaults removeObjectForKey:FIREBASE_ALREADY_DOWNLOAD_ALL];
                
                [defaults synchronize];
                
                // limpa os dados das tabelas
                NSManagedObjectContext *context = [NSManagedObjectContext MR_contextForCurrentThread];
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo > 0"];
                
                [Acompanhamento MR_deleteAllMatchingPredicate:predicate inContext:context];
                [Bairro MR_deleteAllMatchingPredicate:predicate inContext:context];
                [Cliente MR_deleteAllMatchingPredicate:predicate inContext:context];
                [ClienteDeletado MR_deleteAllMatchingPredicate:predicate inContext:context];
                [Endereco MR_deleteAllMatchingPredicate:predicate inContext:context];
                [Evento MR_deleteAllMatchingPredicate:predicate inContext:context];
                [FotoImovel MR_deleteAllMatchingPredicate:predicate inContext:context];
                [Imovel MR_deleteAllMatchingPredicate:predicate inContext:context];
                [Negociacao MR_deleteAllMatchingPredicate:predicate inContext:context];
                [Nota MR_deleteAllMatchingPredicate:predicate inContext:context];
                [PerfilCliente MR_deleteAllMatchingPredicate:predicate inContext:context];
                [Sms MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"codigoSMS > 0"] inContext:context];
                [TagImovel MR_deleteAllMatchingPredicate:predicate inContext:context];
            }
            
            NSLog(@"Clientes OK - %ld", (unsigned long)clientesOk.count);
            NSLog(@"Clientes Dup - %ld", (unsigned long)clientesDup.count);
            
            NSLog(@"Fim Hack Dedup - %@", [NSDate date]);
            
            dispatch_semaphore_signal(sema);
        });
    }];
    
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
}


@end
