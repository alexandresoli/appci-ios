//
//  Cliente+Utils.h
//  Appci
//
//  Created by BrEstate LTDA on 29/05/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Cliente.h"

@interface Cliente (Utils)

@property (nonatomic, weak, readonly) NSString *iniciaisNome;
@property (nonatomic, weak, readonly) NSString *primeiroNome;

@end
