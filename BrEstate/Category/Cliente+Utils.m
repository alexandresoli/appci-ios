//
//  Cliente+Utils.m
//  Appci
//
//  Created by BrEstate LTDA on 29/05/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Cliente+Utils.h"

@implementation Cliente (Utils)

- (NSString *)iniciaisNome
{
    NSString *names = [self.nome uppercaseString];
    
    NSMutableCharacterSet *notAllowedChars = [[[NSCharacterSet letterCharacterSet] invertedSet] mutableCopy];
    [notAllowedChars removeCharactersInString:@" "];
    
    names = [[names componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
    names = [names stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSArray *namesArray = [names componentsSeparatedByString:@" "];
    NSString *initialName;
    
    if (namesArray.count > 1) {
        initialName = [NSString stringWithFormat:@"%@%@", [[namesArray firstObject] substringToIndex:1], [[namesArray lastObject] substringToIndex:1]];
    } else {
        
        if ([[namesArray firstObject] length] > 0) {
            initialName = [[namesArray firstObject] substringToIndex:1];
        } else {
            initialName = @"";
        }
    }
    
    return initialName;
}


- (NSString *)primeiroNome
{
    NSString *names = [self.nome stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSArray *namesArray = [names componentsSeparatedByString:@" "];
    NSString *firstName = [namesArray firstObject];
    
    return firstName;
}

@end
