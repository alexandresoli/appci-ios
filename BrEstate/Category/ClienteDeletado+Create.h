//
//  ClienteDeletado+Create.h
//  Appci
//
//  Created by BrEstate LTDA on 26/05/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "ClienteDeletado.h"
#import "Cliente.h"

@interface ClienteDeletado (Create)

+ (ClienteDeletado *)createWithCliente:(Cliente *)cliente andContext:(NSManagedObjectContext *)moc;

@end
