//
//  ClienteDeletado+Create.m
//  Appci
//
//  Created by BrEstate LTDA on 26/05/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "ClienteDeletado+Create.h"

@implementation ClienteDeletado (Create)


+ (ClienteDeletado *)createWithCliente:(Cliente *)cliente andContext:(NSManagedObjectContext *)moc
{
    ClienteDeletado *clienteDeletado = [ClienteDeletado MR_createInContext:moc];
    
    clienteDeletado.codigo = cliente.codigo;
    clienteDeletado.nome = cliente.nome;
    clienteDeletado.email = cliente.email;
    clienteDeletado.telefone = cliente.telefone;
    clienteDeletado.telefoneExtra = cliente.telefoneExtra;
    
    return clienteDeletado;
}


@end
