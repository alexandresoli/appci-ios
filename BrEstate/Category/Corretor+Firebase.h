//
//  Corretor+Firebase.h
//  Appci
//
//  Created by Leonardo Barros on 07/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "Corretor.h"

@interface Corretor (Firebase)

- (void)convertFirebaseDictionary:(NSDictionary *)dict;

+ (void)startFirebaseObservers;

@end
