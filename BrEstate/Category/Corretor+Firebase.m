//
//  Corretor+Firebase.m
//  Appci
//
//  Created by Leonardo Barros on 07/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "Corretor+Firebase.h"
#import "Corretor+Utils.h"

@implementation Corretor (Firebase)


#pragma mark - Queue

dispatch_queue_t queueFirebaseUserInfo;


#pragma mark - Convert Firebase Dictionary

- (void)convertFirebaseDictionary:(NSDictionary *)dict
{
    self.email = dict[@"email"];
    self.nome = dict[@"name"];
    self.telefone = dict[@"phone"];
    self.limiteDevices = dict[@"devicesLimit"];
    self.dataExpiraPlanoTop = [DateUtils dateFromStringTimestamp:dict[@"premiumExpiration"]];
    self.planoTopValido = dict[@"premiumExpiration"] != nil ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO];
}


#pragma mark - Observers

+ (void)startFirebaseObservers
{
    queueFirebaseUserInfo = dispatch_queue_create("Firebase.UserInfo", NULL);
    
    [Corretor startObserverChangedUserInfo];
}


+ (void)startObserverChangedUserInfo
{
    Firebase *userRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"UserInfo"];
    
    [userRef observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        
        dispatch_async(queueFirebaseUserInfo, ^{
            
            if ([snapshot.value isEqual:[NSNull null]]) {
                return;
            }
            
            BOOL wasTOP = [[Utils iapHelper] checkPlanTopFromController:nil withMessage:nil];
            
            NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
            Corretor *corretor = [Corretor corretorWithContext:moc];
            
            [corretor convertFirebaseDictionary:snapshot.value];
            
            [moc MR_saveToPersistentStoreAndWait];
            
            BOOL isTOPNow = [[Utils iapHelper] checkPlanTopFromController:nil withMessage:nil];
            
            if (!wasTOP && isTOPNow) {
                [Utils showMessage:@"Seu plano TOP foi ativado! Seus dados começaram a ser sincronizados! Depois verifique o status do seus dados na opção Menu > Sincronizar"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[Utils firebaseUtils] startObservers];
                });
            }
        });
    }];
}


@end
