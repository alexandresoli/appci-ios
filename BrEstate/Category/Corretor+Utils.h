//
//  Corretor+Utils.h
//  Appci
//
//  Created by BrEstate LTDA on 23/06/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Corretor.h"

@interface Corretor (Utils)


/** Método utilizado para retornar o corretor. Como só é permitido um corretor se já existir um corretor o mesmo é retornado, caso contrário, é criado um corretor que será retornado.
 
 @param moc         context a ser utilizado para obter o corretor.
 @return objeto Corretor com o corretor já existente ou criado.
 */
+ (Corretor *)corretorWithContext:(NSManagedObjectContext *)moc;


/** Método utilizado para atualizar a data de expiração do plano TOP e a flag que indica que o plano TOP está válido. Se o Dictionary não possuir a key "expiration" atualiza o corretor para não ser mais TOP.
 
 @param dict         Dictionary contendo as informações do plano TOP. Deve conter um item com a key "expiration" (Long) e outro item com a key "expired" (NSNumber contendo um BOOL).
 
 */
//- (void)updateExpirationDateWithDictionary:(NSDictionary *)dict;

- (void)updateExpirationDateWithDate:(NSDate *)dateExpiration;

//- (void)updateInformationFromParseWithMoc:(NSManagedObjectContext *)moc;

@end
