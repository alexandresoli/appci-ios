//
//  Corretor+Utils.m
//  Appci
//
//  Created by BrEstate LTDA on 23/06/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Corretor+Utils.h"
#import "DateUtils.h"
#import "Imobiliaria.h"


@implementation Corretor (Utils)


+ (Corretor *)corretorWithContext:(NSManagedObjectContext *)moc
{
    Corretor *corretor = [Corretor MR_findFirstInContext:moc];
    
    
    if (!corretor) {
        corretor = [Corretor MR_createInContext:moc];
        corretor.codigo = [NSNumber numberWithInt:1];
    }
    
    return corretor;
}


- (void)updateExpirationDateWithDate:(NSDate *)dateExpiration
{
    if (dateExpiration && ![dateExpiration isEqual:[NSNull null]]) {
        self.dataExpiraPlanoTop = [DateUtils dateWithoutTime:dateExpiration];
        
        // TODO: fazer serviço no Parse para testar os 30 dias do plano TOP e retornar se está válido.
        self.planoTopValido = [NSNumber numberWithBool:YES];
        
    } else { // Não veio data de expiração considera que não é mais TOP.
        self.dataExpiraPlanoTop = nil;
        self.planoTopValido = [NSNumber numberWithBool:NO];
    }
    
    self.version = [NSDate date];
}


@end
