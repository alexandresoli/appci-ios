//
//  Endereco+Firebase.h
//  Appci
//
//  Created by Leonardo Barros on 26/12/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Endereco.h"

@interface Endereco (Firebase)

- (NSDictionary *)dictionaryFirebase;

- (void)convertFirebaseDictionary:(NSDictionary *)dict withMoc:(NSManagedObjectContext *)moc;

@end
