//
//  Endereco+Firebase.m
//  Appci
//
//  Created by Leonardo Barros on 26/12/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Endereco+Firebase.h"
#import "Bairro+Firebase.h"


@implementation Endereco (Firebase)


#pragma mark - Dictionary Firebase

- (NSDictionary *)dictionaryFirebase
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    [dic setObject:self.cep != nil ? self.cep : [NSNull null] forKey:@"cep"];
    [dic setObject:self.cidade != nil ? self.cidade : [NSNull null] forKey:@"cidade"];
    [dic setObject:self.codigo != nil ? self.codigo : [NSNull null] forKey:@"codigo"];
    [dic setObject:self.complemento != nil ? self.complemento : [NSNull null] forKey:@"complemento"];
    [dic setObject:self.estado != nil ? self.estado : [NSNull null] forKey:@"estado"];
    [dic setObject:self.logradouro != nil ? self.logradouro : [NSNull null] forKey:@"logradouro"];
    [dic setObject:self.latitude != nil ? self.latitude : [NSNull null] forKey:@"latitude"];
    [dic setObject:self.longitude != nil ? self.longitude : [NSNull null] forKey:@"longitude"];
//    [dic setObject:[Utils deviceID] forKey:@"deviceToken"];
    
    // bairro
    if (self.bairro) {
        [dic setObject:self.bairro.getRelationshipFirebase forKey:@"bairro"];
        
    } else {
        [dic setObject:[NSNull null] forKey:@"bairro"];
    }
    
    return dic;
}


#pragma mark - Convert Firebase Dictionary

- (void)convertFirebaseDictionary:(NSDictionary *)dict withMoc:(NSManagedObjectContext *)moc
{
    self.codigo = dict[@"codigo"];
    self.cep = dict[@"cep"];
    self.cidade = dict[@"cidade"];
    self.estado = dict[@"estado"];
    self.logradouro = dict[@"logradouro"];
    self.complemento = dict[@"complemento"];
    self.latitude = dict[@"latitude"];
    self.longitude = dict[@"longitude"];
    
    // Bairro
    if (dict[@"bairro"]) {
        NSDictionary *bairroDict = dict[@"bairro"];
        
        Bairro *bairro = [Bairro MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", bairroDict[@"firebaseID"]] inContext:moc];
        
        if (bairro) {
            self.bairro = bairro;
            
        } else {
            bairro = [Bairro MR_createInContext:moc];
            bairro.codigo = bairroDict[@"codigo"];
            bairro.firebaseID = bairroDict[@"firebaseID"];
            bairro.isSavedInFirebase = [NSNumber numberWithBool:YES];
            bairro.nome = @"Baixando informações";
            self.bairro = bairro;
        }
    }
}


@end
