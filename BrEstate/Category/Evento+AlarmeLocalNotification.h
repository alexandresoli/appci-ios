//
//  Evento+AlarmeLocalNotification.h
//  Appci
//
//  Created by BrEstate LTDA on 19/05/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Evento.h"

@interface Evento (AlarmeLocalNotification)

- (void)registerAlarmesForLocalNotification:(NSManagedObjectContext *)moc;
- (void)cancelEventoLocalNotifications;
+ (void)refreshLocalNotificationForAllEventos:(NSManagedObjectContext *)moc;

@end
