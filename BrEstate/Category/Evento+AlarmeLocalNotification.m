//
//  Evento+AlarmeLocalNotification.m
//  Appci
//
//  Created by BrEstate LTDA on 19/05/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Evento+AlarmeLocalNotification.h"
#import "Alarme.h"

@implementation Evento (AlarmeLocalNotification)


- (void)registerAlarmesForLocalNotification:(NSManagedObjectContext *)moc
{
    // Remove todos os alarmes do evento para incluir os novos.
    [self cancelEventoLocalNotifications];
    
    NSArray *alarmesEvento = [NSKeyedUnarchiver unarchiveObjectWithData:self.alarmes];
    
    for (NSNumber *codigoAlarme in alarmesEvento) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@", codigoAlarme];
        
        Alarme *alarme = [Alarme MR_findFirstWithPredicate:predicate inContext:moc];
        
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        
        NSDate *fireDate;
        
        if (alarme.tempo != nil) {
            fireDate = [self.dataInicio dateByAddingTimeInterval:[alarme.tempo doubleValue]];
            NSString *formatAlarmeDescricao = [[alarme.descricao componentsSeparatedByString:@"antes"] firstObject];
            
            if (self.tipoEvento) {
                localNotification.alertBody = [NSString stringWithFormat:@"%@ - %@ daqui a %@", self.tipoEvento, self.titulo, formatAlarmeDescricao];
            } else {
                localNotification.alertBody = [NSString stringWithFormat:@"%@ daqui a %@", self.titulo, formatAlarmeDescricao];
            }
            
        
        } else { // Alarme na hora do evento
            fireDate = self.dataInicio;
            
            if (self.tipoEvento) {
                localNotification.alertBody = [NSString stringWithFormat:@"%@ - %@ agora!", self.tipoEvento, self.titulo];
            } else {
                localNotification.alertBody = [NSString stringWithFormat:@"%@ agora!", self.titulo];
            }
            
        }
        
        // Se a fireDate for menor que a data atual não cria o notification pois já passou da hora do alarme.
        if ([fireDate compare:[NSDate date]] == NSOrderedAscending) {
            localNotification = nil;
            continue;
        }
        
        localNotification.fireDate = fireDate;
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.alertAction = @"visualizar";
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.applicationIconBadgeNumber = 1;
        
        NSArray *eventoAlarmeArray = [NSArray arrayWithObjects:self.codigo, codigoAlarme, nil];
        
        NSDictionary *infoDict = [NSDictionary dictionaryWithObject:eventoAlarmeArray forKey:@"EventoAlarme"];
        
        localNotification.userInfo = infoDict;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        
    }
}


- (void)cancelEventoLocalNotifications
{
    NSArray *notifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
    
    for (UILocalNotification *notification in notifications) {
        NSArray *eventoAlarmeArray = [notification.userInfo objectForKey:@"EventoAlarme"];
        
        NSNumber *codigoEvento = [eventoAlarmeArray firstObject];
        
        if (!self.codigo || !codigoEvento) {
            continue;
        }
        
        if ([codigoEvento isEqualToNumber:self.codigo]) {
            [[UIApplication sharedApplication] cancelLocalNotification:notification];
        }
    }
}


+ (void)refreshLocalNotificationForAllEventos:(NSManagedObjectContext *)moc
{
    NSArray *eventos = [Evento MR_findAllInContext:moc];
    
    for (Evento *evento in eventos) {
        [evento registerAlarmesForLocalNotification:moc];
    }
}


@end
