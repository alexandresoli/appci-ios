//
//  Evento+Firebase.h
//  Appci
//
//  Created by Leonardo Barros on 07/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "Evento.h"

@interface Evento (Firebase)

- (NSString *)createFirebaseID;
- (void)saveFirebase;
- (void)deleteFirebase;

+ (void)saveAllPendingFirebase;
+ (void)startFirebaseObservers;
+ (void)convertEventosInBatchWithDictionary:(NSDictionary *)eventosDict;

@end
