//
//  Evento+Firebase.m
//  Appci
//
//  Created by Leonardo Barros on 07/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "Evento+Firebase.h"
#import "Imovel+Firebase.h"
#import "Cliente+Firebase.h"
#import "Evento+AlarmeLocalNotification.h"
#import "CustomCalendar.h"
#import "Sms+Firebase.h"

@implementation Evento (Firebase)


#pragma mark - Create Firebase ID

- (NSString *)createFirebaseID
{
    if (self.firebaseID) {
        return self.firebaseID;
    }
    
    Firebase *classRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Evento"];
    
    Firebase *objectRef = [classRef childByAutoId];
    
    self.firebaseID = objectRef.key;
    
    [self.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    return objectRef.key;
}


#pragma mark - Dictionary Firebase

- (NSDictionary *)dictionaryFirebase
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    NSString *dataInicio = [DateUtils stringTimestampFromDate:self.dataInicio];
    NSString *dataFim = [DateUtils stringTimestampFromDate:self.dataFim];
    
    [dic setObject:self.codigo != nil ? self.codigo : [NSNull null] forKey:@"codigo"];
    [dic setObject:self.dataFim != nil ? dataFim : [NSNull null] forKey:@"dataFim"];
    [dic setObject:self.dataInicio != nil ? dataInicio : [NSNull null] forKey:@"dataInicio"];
    [dic setObject:self.identificadorEventoCalendario != nil ? self.identificadorEventoCalendario : [NSNull null] forKey:@"identificadorEventoCalendario"];
    [dic setObject:self.recursivo != nil ? self.recursivo : [NSNull null] forKey:@"recursivo"];
    [dic setObject:self.local != nil ? self.local : [NSNull null] forKey:@"local"];
    [dic setObject:self.notas != nil ? self.notas : [NSNull null] forKey:@"notas"];
    [dic setObject:self.tipoEvento != nil ? self.tipoEvento : [NSNull null] forKey:@"tipoEvento"];
    [dic setObject:self.titulo != nil ? self.titulo : [NSNull null] forKey:@"titulo"];
    [dic setObject:[Utils deviceID] forKey:@"deviceToken"];
    
    // version
    if (!self.version) {
        self.version = [NSDate date];
    }
    [dic setObject:[DateUtils stringTimestampFromDate:self.version] forKey:@"version"];
    
    // firebaseID
    [dic setObject:self.createFirebaseID forKey:@"firebaseID"];
    
    // imovel
    if (self.imovel) {
        [dic setObject:self.imovel.getRelationshipFirebase forKey:@"imovel"];
    } else {
        [dic setObject:[NSNull null] forKey:@"imovel"];
    }
    
    // alarmes
    NSArray *alarmes = [NSKeyedUnarchiver unarchiveObjectWithData:self.alarmes];
    if (alarmes != nil && alarmes.count > 0) {
        [dic setObject:alarmes forKey:@"alarme"];
    } else {
        [dic setObject:[NSNull null] forKey:@"alarme"];
    }
    
    // convidados
    if (self.cliente && self.cliente.count > 0) {
        NSMutableArray *contatos = [NSMutableArray array];
        
        for (Cliente *cliente in self.cliente) {
            [contatos addObject:cliente.getRelationshipFirebase];
        }
        
        [dic setObject:contatos forKey:@"convidados"];
        
    } else {
        [dic setObject:[NSNull null] forKey:@"convidados"];
    }
    
    // sms
    if (self.sms && self.sms.count > 0) {
        NSMutableDictionary *smss = [NSMutableDictionary new];
        
        for (Sms *sms in self.sms) {
            [smss setObject:sms.dictionaryFirebase forKey:sms.createFirebaseID];
        }
        [dic setObject:smss forKey:@"sms"];
        
    } else {
        [dic setObject:[NSNull null] forKey:@"sms"];
    }
    
    return dic;
}


#pragma mark - Convert Firebase Dictionary

- (void)convertFirebaseDictionary:(NSDictionary *)dictEvento withMoc:(NSManagedObjectContext *)moc
{
    self.codigo = dictEvento[@"codigo"];
    self.dataFim =  [DateUtils dateFromStringTimestamp:dictEvento[@"dataFim"]];
    self.dataInicio = [DateUtils dateFromStringTimestamp:dictEvento[@"dataInicio"]];
    self.identificadorEventoCalendario = dictEvento[@"identificadorEventoCalendario"];
    self.recursivo = dictEvento[@"recursivo"];
    self.local = dictEvento[@"local"];
    self.notas = dictEvento[@"notas"];
    self.tipoEvento = dictEvento[@"tipoEvento"];
    self.titulo = dictEvento[@"titulo"];
    
    self.firebaseID = dictEvento[@"firebaseID"];
    
    self.isSavedInFirebase = [NSNumber numberWithBool:YES];
    
    self.version = [DateUtils dateFromStringTimestamp:dictEvento[@"version"]];
    
    // alarmes
    self.alarmes = [NSKeyedArchiver archivedDataWithRootObject:dictEvento[@"alarme"]];
    
    // imovel
    if (dictEvento[@"imovel"]) {
        NSDictionary *imovelDict = dictEvento[@"imovel"];
        
        Imovel *imovel = [Imovel MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", imovelDict[@"firebaseID"]]
                                                 inContext:moc];
        
        if (!imovel) {
            imovel = [Imovel MR_createInContext:moc];
            imovel.firebaseID = imovelDict[@"firebaseID"];
            imovel.codigo = imovelDict[@"codigo"];
            imovel.isSavedInFirebase = [NSNumber numberWithBool:YES];
        }
        self.imovel = imovel;
    }
    
    // convidados
    if (dictEvento[@"convidados"]) {
        NSArray *convidados = dictEvento[@"convidados"];
        
        for (NSDictionary *clienteDict in convidados) {
            Cliente *cliente = [Cliente MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", clienteDict[@"firebaseID"]]
                                                        inContext:moc];
            
            if (!cliente) {
                cliente = [Cliente MR_createInContext:moc];
                cliente.firebaseID = clienteDict[@"firebaseID"];
                cliente.codigo = clienteDict[@"codigo"];
                cliente.isSavedInFirebase = [NSNumber numberWithBool:YES];
                cliente.nome = @"Baixando informações";
            }
            [self addClienteObject:cliente];
        }
    }
    
    // sms
    if (dictEvento[@"sms"]) {
        NSDictionary *smss = dictEvento[@"sms"];
        
        for (NSString *smsFirebaseID in smss.allKeys) {
            Sms *sms = [Sms MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", smsFirebaseID]
                                            inContext:moc];
            
            if (!sms) {
                
                // Necessary because of the migration from Parse where users have more than one device.
                NSDictionary *dict = [smss objectForKey:smsFirebaseID];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigoSMS = %@", dict[@"codigoSMS"]];
                sms = [Sms MR_findFirstWithPredicate:predicate inContext:moc];
                
                if (!sms) {
                    sms = [Sms MR_createInContext:moc];
                }
            }
            
            [sms convertFirebaseDictionary:[smss objectForKey:smsFirebaseID] withMoc:moc];
            [self addSmsObject:sms];
        }
    }
    
    [self registerAlarmesForLocalNotification:moc];
}


#pragma mark - Delete Firebase

- (void)deleteFirebase
{
    [[Utils firebaseUtils] deleteObjectWithID:self.firebaseID andClassName:@"Evento"];
}


#pragma mark - Save Firebase

- (void)saveFirebase
{
    self.isSavedInFirebase = [NSNumber numberWithBool:NO];
    
    // If not TOP don't save on Firebase.
    if (![[Utils iapHelper] checkPlanTopFromController:nil withMessage:nil]) {
        [self.managedObjectContext MR_saveToPersistentStoreAndWait];
        return;
    }
    
    [[Utils firebaseUtils] saveObjectWithDictionary:self.dictionaryFirebase andClassName:@"Evento" andCompletionBlock:^(NSError *error) {
        [self.managedObjectContext performBlock:^{
            if (!error) {
                self.isSavedInFirebase = [NSNumber numberWithBool:YES];
            }
            [self.managedObjectContext MR_saveToPersistentStoreAndWait];
        }];
    }];
    
    // Save reference of the Evento on Imovel.
//    if (self.imovel) {
//        [self.imovel saveFirebase];
//    }
//    
//    // Save reference of the Evento on Cliente.
//    for (Cliente *cliente in self.cliente) {
//        [cliente saveFirebase];
//    }
}


+ (void)saveAllPendingFirebase
{
    NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSavedInFirebase = NO OR isSavedInFirebase = nil"];
    
    NSArray *eventos = [Evento MR_findAllWithPredicate:predicate inContext:moc];
    if (eventos.count == 0) {
        return;
    }
    
    NSMutableDictionary *eventoDic = [NSMutableDictionary new];
    
    for (Evento *evento in eventos) {
        [eventoDic setValue:evento.dictionaryFirebase forKey:evento.firebaseID];
    }
    
    dispatch_semaphore_t semaEvento = dispatch_semaphore_create(0);
    
    [moc MR_saveToPersistentStoreAndWait];
    
    Firebase *eventoRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Evento"];
    [eventoRef updateChildValues:eventoDic withCompletionBlock:^(NSError *error, Firebase *ref) {
        
        [moc performBlock:^{
            if (!error) {
                for (Evento *evento in eventos) {
                    evento.isSavedInFirebase = [NSNumber numberWithBool:YES];
                }
                [moc MR_saveToPersistentStoreAndWait];
            }
            
            [Utils postEntitytNotificationWithValue:@"UpdateSyncCounter" value:eventos.count];            
            dispatch_semaphore_signal(semaEvento);
        }];
    }];
    
    dispatch_semaphore_wait(semaEvento, DISPATCH_TIME_FOREVER);
}


#pragma mark - Convert Eventos In Batch

+ (void)convertEventosInBatchWithDictionary:(NSDictionary *)eventosDict
{
    if (!eventosDict || [eventosDict isEqual:[NSNull null]] || eventosDict.count == 0) {
        return;
    }
    
    NSManagedObjectContext *context = [NSManagedObjectContext MR_contextForCurrentThread];
    
    [context performBlockAndWait:^{
        
        BOOL hasNew = NO;
        NSNumber *alreadyDownloadAll = [Utils valueFromDefaults:FIREBASE_ALREADY_RUN];
        
        // Not the first time.
        if (alreadyDownloadAll) {
            
            for (NSString *firebaseID in eventosDict.allKeys) {
                
                NSDictionary *oneEvento = [eventosDict objectForKey:firebaseID];
                
                // Check if the change was made by the current device.
                if ([oneEvento[@"deviceToken"] isEqualToString:[Utils deviceID]]) {
                    continue;
                }
                
                Evento *evento = [Evento MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", firebaseID]
                                                         inContext:context];
                
                if (!evento) {
                    
                    // Necessary because of the migration from Parse where users have more than one device.
                    NSPredicate *predicate = [Evento uniquePredicateWithDictionary:oneEvento];
                    evento = [Evento MR_findFirstWithPredicate:predicate inContext:context];
                    
                    if (!evento) {
                        evento = [Evento MR_createInContext:context];
                    }
                    
                } else {
                    NSString *objVersion = oneEvento[@"version"];
                    NSString *lastVersion = [Utils valueFromDefaults:FIREBASE_EVENTO_INSERT_VERSION];
                    
                    if ([objVersion compare:lastVersion] == NSOrderedAscending || [objVersion compare:lastVersion] == NSOrderedSame) {
                        continue;
                    }
                }
                
                hasNew = YES;
                
                [evento convertFirebaseDictionary:oneEvento withMoc:context];
                [evento registerAlarmesForLocalNotification:context];
            }
            
        } else { // First Download
            
            // Sort dictionary by version to the most recent keep on device in duplicates case.
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"version" ascending:NO];
            NSMutableArray *eventosArray = [eventosDict.allValues mutableCopy];
            [eventosArray sortUsingDescriptors:@[descriptor]];
            
            NSMutableArray *checkEventosArray = [NSMutableArray new];
            
            [Evento MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"codigo > 0"] inContext:context];
            
            for (NSDictionary *oneEvento in eventosArray) {
                
                @autoreleasepool {
                    NSPredicate *predicate = [Evento uniquePredicateWithDictionary:oneEvento];
                    NSDictionary *checkEvento = [checkEventosArray filteredArrayUsingPredicate:predicate].firstObject;
                    
                    if (!checkEvento) {
                        Evento *evento = [Evento MR_createInContext:context];
                        [evento convertFirebaseDictionary:oneEvento withMoc:context];
                        [evento registerAlarmesForLocalNotification:context];
                        
                        [checkEventosArray addObject:oneEvento];
                        
                        hasNew = YES;
                    }
                }
            }
        }
        
        if (hasNew) {
            [context MR_saveToPersistentStoreAndWait];
            
            NSArray *eventosArray = [Evento MR_findAllSortedBy:@"version" ascending:YES inContext:context];
            Evento *lastEvento = eventosArray.lastObject;
            
            [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:REFRESH_EVENTO];
            [Utils addObjectToDefaults:[DateUtils stringTimestampFromDate:lastEvento.version] withKey:FIREBASE_EVENTO_INSERT_VERSION];
        }
    }];
}


#pragma mark - Unique Predicate

+ (NSPredicate *)uniquePredicateWithDictionary:(NSDictionary *)dict
{
    NSPredicate *predicate;
    
    if (dict[@"identificadorEventoCalendario"]) {
        predicate = [NSPredicate predicateWithFormat:@"identificadorEventoCalendario = %@", dict[@"identificadorEventoCalendario"]];
        
    } else {
        NSDate *dataInicio = [DateUtils dateFromStringTimestamp:dict[@"dataInicio"]];
        NSDate *dataFim = [DateUtils dateFromStringTimestamp:dict[@"dataFim"]];
        predicate = [NSPredicate predicateWithFormat:@"titulo = %@ AND dataInicio = %@ AND dataFim = %@", dict[@"titulo"], dataInicio, dataFim];
    }
    
    return predicate;
}


#pragma mark - Deduplicator

+ (void)dedup:(NSDictionary *)dict withMoc:(NSManagedObjectContext *)moc
{
    NSPredicate *predicate = [Evento uniquePredicateWithDictionary:dict];
    
    NSArray *eventos = [Evento MR_findAllWithPredicate:predicate inContext:moc];
    
    Evento *prevEvento;
    
    for (Evento *evento in eventos) {
        
        if (![moc existingObjectWithID:evento.objectID error:NULL]) {
            continue;
        }
        
        if (prevEvento) {
            if (evento.version == nil) {
                if (evento.firebaseID) {
                    [evento deleteFirebase];
                }
                [evento cancelEventoLocalNotifications];
                [evento MR_deleteInContext:moc];
                
            } else if (prevEvento.version == nil) {
                if (prevEvento.firebaseID) {
                    [prevEvento deleteFirebase];
                }
                [prevEvento cancelEventoLocalNotifications];
                [prevEvento MR_deleteInContext:moc];
                prevEvento = evento;
                
            } else if ([evento.version compare:prevEvento.version] == NSOrderedAscending) {
                if (evento.firebaseID) {
                    [evento deleteFirebase];
                }
                [evento cancelEventoLocalNotifications];
                [evento MR_deleteInContext:moc];
                
            } else {
                if (prevEvento.firebaseID) {
                    [prevEvento deleteFirebase];
                }
                [prevEvento cancelEventoLocalNotifications];
                [prevEvento MR_deleteInContext:moc];
                prevEvento = evento;
            }
            
        } else {
            prevEvento = evento;
        }
    }
}


#pragma mark - Observers

+ (void)startFirebaseObservers
{
    [Evento startObserverAddedAndChangeEvento];
}


+ (void)startObserverAddedAndChangeEvento
{
    Firebase *eventoRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Evento"];
    
    NSString *versionString = [Utils valueFromDefaults:FIREBASE_EVENTO_INSERT_VERSION];
    
    if (!versionString) {
        versionString = [DateUtils stringTimestampFromDate:[NSDate dateWithTimeIntervalSince1970:1]];
    }
    
    // Add 1 millisecond because the firebase query is always >=
    versionString = [NSString stringWithFormat:@"%lld", [versionString longLongValue]+1];
    
    [[[eventoRef queryOrderedByChild:@"version"] queryStartingAtValue:versionString]
     observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
         
         if ([snapshot.value isEqual:[NSNull null]]) {
             return;
         }
         
         dispatch_async([Utils firebaseUtils].queueDownload, ^{
             
             NSDictionary *eventosDict = snapshot.value;
             
             [Evento convertEventosInBatchWithDictionary:eventosDict];
         });
     }];
}


@end
