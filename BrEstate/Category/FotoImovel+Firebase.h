//
//  FotoImovel+Firebase.h
//  Appci
//
//  Created by Leonardo Barros on 23/12/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "FotoImovel.h"

@interface FotoImovel (Firebase)

- (NSString *)createFirebaseID;
- (NSDictionary *)dictionaryFirebase;
- (void)deleteFirebase;
- (void)convertFirebaseDictionary:(NSDictionary *)dictFoto withMoc:(NSManagedObjectContext *)moc;
- (void)downloadFromS3;

+ (void)sendAllPendingPhotosToAmazon;

@end
