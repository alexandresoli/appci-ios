//
//  FotoImovel+Firebase.m
//  Appci
//
//  Created by Leonardo Barros on 23/12/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "FotoImovel+Firebase.h"
#import "Imovel+Firebase.h"
#import "FotoImovel+InsertDeleteImagem.h"
#import "S3Utils.h"

@implementation FotoImovel (Firebase)


#pragma mark - Dictionary Firebase

- (NSDictionary *)dictionaryFirebase
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    [dic setObject:self.codigo != nil ? self.codigo : [NSNull null] forKey:@"codigo"];
    [dic setObject:self.descricao != nil ? self.descricao : [NSNull null] forKey:@"descricao"];
    [dic setObject:self.importacao != nil ? self.importacao : [NSNull null] forKey:@"importacao"];
    [dic setObject:self.video != nil ? self.video : [NSNull null] forKey:@"video"];
    [dic setObject:self.imagemURL != nil ? self.imagemURL : [NSNull null] forKey:@"imagemURL"];
    [dic setObject:self.capa != nil ? self.capa : [NSNull null] forKey:@"capa"];
    [dic setObject:self.codigoAnterior != nil ? self.codigoAnterior : [NSNull null] forKey:@"codigoAnterior"];
    
    // version
    if (!self.version) {
        self.version = [NSDate date];
    }
    [dic setObject:[DateUtils stringTimestampFromDate:self.version] forKey:@"version"];
    
    // firebaseID
    [dic setObject:self.createFirebaseID forKey:@"firebaseID"];
    
    return dic;
}


#pragma mark - Create Firebase ID

- (NSString *)createFirebaseID
{
    if (self.firebaseID) {
        return self.firebaseID;
    }
    
    NSString *path = [NSString stringWithFormat:@"Imovel/%@/fotos", self.imovel.createFirebaseID];
    
    Firebase *classRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:path];
    
    Firebase *objectRef = [classRef childByAutoId];
    
    self.firebaseID = objectRef.key;
    
    [self.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    return objectRef.key;
}


#pragma mark - Convert Firebase Dictionary

- (void)convertFirebaseDictionary:(NSDictionary *)dictFoto withMoc:(NSManagedObjectContext *)moc
{
    self.codigo = dictFoto[@"codigo"];
    self.capa = dictFoto[@"capa"];
    self.descricao = dictFoto[@"descricao"];
    self.importacao = dictFoto[@"importacao"];
    self.video = dictFoto[@"video"];
    self.imagemURL = dictFoto[@"imagemURL"];
    self.firebaseID = dictFoto[@"firebaseID"];
    self.codigoAnterior = dictFoto[@"codigoAnterior"];
    
    self.isSavedInFirebase = [NSNumber numberWithBool:YES];
    
    self.version = [DateUtils dateFromStringTimestamp:dictFoto[@"version"]];
    
}


#pragma mark - Delete Firebase

- (void)deleteFirebase
{
    [[Utils firebaseUtils] deleteObjectWithID:self.firebaseID andClassName:@"FotoImovel"];
    
    Imovel *imovel = self.imovel;
    self.imovel = nil;
    
    [imovel saveFirebase];
}


#pragma mark - Amazon S3
- (void)downloadFromS3
{
    
    // amazon s3
    NSNumber *alreadyDownloadAll = [Utils valueFromDefaults:FIREBASE_ALREADY_DOWNLOAD_ALL];
    
    if (!alreadyDownloadAll) {
        return;
    }
    
    if ([self isLocalFileExists]) {
        self.pendingUpload = [NSNumber numberWithBool:NO];
        self.pendingDownload = [NSNumber numberWithBool:NO];
        [self.managedObjectContext MR_saveToPersistentStoreAndWait];
        return;
    }
    
    self.pendingDownload = [NSNumber numberWithBool:YES];
    self.pendingUpload = [NSNumber numberWithBool:NO];
    
    @autoreleasepool {
        
        NSString *userID = [Utils valueFromDefaults:FIREBASE_USER_ID];
        NSString *filePath = [NSString stringWithFormat:@"%@/fotos/%@",userID,self.imagemURL];
        NSData *imageData = [S3Utils download:filePath];
        
        // success
        if (imageData != nil) {
            [self insertMediaOnDisk:imageData withURLString:self.imagemURL];
            self.pendingUpload = [NSNumber numberWithBool:NO];
            self.pendingDownload = [NSNumber numberWithBool:NO];
            [self.managedObjectContext MR_saveToPersistentStoreAndWait];
            return;
        }
        
    }
    
}


#pragma mark - Send All Pending Photos to Amazon

+ (void)sendAllPendingPhotosToAmazon
{
    NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
    
    [moc performBlockAndWait:^{
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pendingUpload = YES OR pendingUpload = nil"];
        
        NSArray *fotos = [FotoImovel MR_findAllWithPredicate:predicate inContext:moc];
        
        __block BOOL hasError = NO;
        
        for (FotoImovel *foto in fotos) {
            
            [foto upload];
            [moc MR_saveToPersistentStoreAndWait];
            [Utils postEntitytNotificationWithValue:@"UpdateSyncCounter" value:1];
        }
        
        if (hasError) {
            [Utils addObjectToDefaults:@"Ocorreu erro no envio de algumas fotos." withKey:SYNC_ERROR_MESSAGE_FOTO];
        } else {
            [Utils removeObjectFromDefaults:SYNC_ERROR_MESSAGE_FOTO];
        }
        
    }];
}


@end
