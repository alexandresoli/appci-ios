//
//  FotoImovel+InsertDeleteImagem.h
//  Appci
//
//  Created by BrEstate LTDA on 07/05/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "FotoImovel.h"

@interface FotoImovel (InsertDeleteImagem)

- (void)deleteFromDisk;
- (void)insertImageOnDisk:(NSData *)imageData;
- (void)insertVideoOnDisk:(NSData *)videoData;
- (void)insertMediaOnDisk:(NSData *)mediaData withURLString:(NSString *)urlString;

- (NSURL *)getFotoImovelURL;
- (UIImage *)getImageForFotoImovelURL;
- (UIImage *)getImageForFotoImovelWithMaxPixelSize:(int)maxPixel;
- (NSURL *)getBaseURLForFotoImovel;
- (BOOL)isLocalFileExists;
- (void)deleteImageFromS3;
- (void)upload;
@end
