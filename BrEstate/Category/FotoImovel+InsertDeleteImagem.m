//
//  FotoImovel+InsertDeleteImagem.m
//  Appci
//
//  Created by BrEstate LTDA on 07/05/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "FotoImovel+InsertDeleteImagem.h"
@import AVFoundation;
#import "Utils.h"
#import "ASIHTTPRequest.h"
#import "ASIS3Request.h"
#import "ASIS3ObjectRequest.h"
#import "S3Utils.h"
#import "Imovel+Firebase.h"
@import ImageIO;

@implementation FotoImovel (InsertDeleteImagem)

- (void)deleteFromDisk
{
    if (self.imagemURL) {
        NSURL *urlFoto = [self getBaseURLForFotoImovel];
        urlFoto = [urlFoto URLByAppendingPathComponent:self.imagemURL];
        
        [[NSFileManager defaultManager] removeItemAtPath:urlFoto.path error:nil];
    }
}


- (void)insertImageOnDisk:(NSData *)imageData
{
    if (self.imagemURL == nil) {
        self.imagemURL = [NSString stringWithFormat:@"%@-%@.png", [NSNumber numberWithLong:[[NSDate date] timeIntervalSince1970]], self.codigo];
    }
    
    NSURL *fileURL = [self getBaseURLForFotoImovel];
    fileURL = [fileURL URLByAppendingPathComponent:self.imagemURL];
    
    [[NSFileManager defaultManager] createFileAtPath:fileURL.path contents:imageData attributes:nil];
}


- (void)insertVideoOnDisk:(NSData *)videoData
{
    if (self.imagemURL == nil) {
        self.imagemURL = [NSString stringWithFormat:@"%@-%@.mov", [NSNumber numberWithLong:[[NSDate date] timeIntervalSince1970]], self.codigo];
    }
    
    NSURL *fileURL = [self getBaseURLForFotoImovel];
    fileURL = [fileURL URLByAppendingPathComponent:self.imagemURL];
    
    [[NSFileManager defaultManager] createFileAtPath:fileURL.path contents:videoData attributes:nil];
}


- (void)insertMediaOnDisk:(NSData *)mediaData withURLString:(NSString *)urlString
{
    NSURL *fileURL = [self getBaseURLForFotoImovel];
    fileURL = [fileURL URLByAppendingPathComponent:urlString];
    
    [[NSFileManager defaultManager] createFileAtPath:fileURL.path contents:mediaData attributes:nil];
}


#pragma mark - Check if local file exists

- (BOOL)isLocalFileExists
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSURL *fileURL = [self getFotoImovelURL];
    
    return [fileManager fileExistsAtPath:fileURL.path];
}


#pragma mark - Get Foto Imovel URL

- (NSURL *)getFotoImovelURL
{
    NSURL *fileURL;
    
    fileURL = [Utils getLocalFotosURL];
    fileURL = [fileURL URLByAppendingPathComponent:self.imagemURL];
    
    
    return fileURL;
}


#pragma mark - Get Image for FotoImovel
- (UIImage *)getImageForFotoImovelURL
{
    if (!self.imagemURL) {
        return nil;
    }
    
    BOOL usingICloud = NO;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSURL *fileURL;
    
    fileURL = [Utils getLocalFotosURL];
    fileURL = [fileURL URLByAppendingPathComponent:self.imagemURL];
    
    NSString *extension = [[fileURL path] pathExtension];
    
    UIImage *image;
    
    if ([extension isEqualToString:@"png"]) {
        
        BOOL exists = [fileManager fileExistsAtPath:fileURL.path];
        
        if (!exists) {
            return [UIImage imageNamed:@"nao-disponivel2"];
        }
        
//        // Check Temp file
//        NSString *tempPath = [NSString stringWithFormat:@"%@Fotos/%@", NSTemporaryDirectory(), self.imagemURL];
//        
//        if ([fileManager fileExistsAtPath:tempPath]) {
//            return [UIImage imageWithContentsOfFile:tempPath];
//        }
//        
//        [fileManager createDirectoryAtPath:[NSString stringWithFormat:@"%@Fotos", NSTemporaryDirectory()]
//              withIntermediateDirectories:YES attributes:nil error:nil];
        
        // Create temp file
        CGImageSourceRef src = CGImageSourceCreateWithURL((__bridge CFURLRef) fileURL, NULL);
        if (!src) {
            return [UIImage imageNamed:@"nao-disponivel2"];
        }
        
        int pixelSize;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            pixelSize = 1280;
        } else {
            pixelSize = 640;
        }
        
        CFDictionaryRef options = (__bridge CFDictionaryRef) @{
                                                               (id) kCGImageSourceCreateThumbnailWithTransform : @YES,
                                                               (id) kCGImageSourceCreateThumbnailFromImageAlways : @YES,
                                                               (id) kCGImageSourceThumbnailMaxPixelSize : @(pixelSize)
                                                               };
        CGImageRef thumbnail = CGImageSourceCreateThumbnailAtIndex(src, 0, options);
        CFRelease(src);
        
//        CFURLRef url = (__bridge CFURLRef) [NSURL fileURLWithPath:tempPath];
//        CGImageDestinationRef destination = CGImageDestinationCreateWithURL(url, kUTTypePNG, 1, NULL);
//        CGImageDestinationAddImage(destination, thumbnail, nil);
//        CGImageDestinationFinalize(destination);
        
        image = [UIImage imageWithCGImage:thumbnail];
        
    } else if ([extension isEqualToString:@"mov"]) {
        
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:fileURL options:nil];
        
        if (!asset && usingICloud) {
            [fileManager startDownloadingUbiquitousItemAtURL:fileURL error:nil];
            
            return nil;
        }
        
        AVAssetImageGenerator *imageGen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        imageGen.appliesPreferredTrackTransform = YES;
        CMTime time = CMTimeMakeWithSeconds(0.0, 1);
        NSError *error = nil;
        CMTime actualTime;
        
        
        CGImageRef imageRef = [imageGen copyCGImageAtTime:time actualTime:&actualTime error:&error];
        image = [[UIImage alloc] initWithCGImage:imageRef];
        CGImageRelease(imageRef);
        
    }
    
    return image;
}


- (UIImage *)getImageForFotoImovelWithMaxPixelSize:(int)maxPixel
{
    if (!self.imagemURL) {
        return nil;
    }
    
    BOOL usingICloud = NO;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSURL *fileURL;
    
    fileURL = [Utils getLocalFotosURL];
    fileURL = [fileURL URLByAppendingPathComponent:self.imagemURL];
    
    NSString *extension = [[fileURL path] pathExtension];
    
    UIImage *image;
    
    if ([extension isEqualToString:@"png"]) {
        
        BOOL exists = [fileManager fileExistsAtPath:fileURL.path];
        
        if (!exists) {
            return [UIImage imageNamed:@"nao-disponivel2"];
        }
        
        CGImageSourceRef src = CGImageSourceCreateWithURL((__bridge CFURLRef) fileURL, NULL);
        if (!src) {
            return [UIImage imageNamed:@"nao-disponivel2"];
        }
        
        CFDictionaryRef options = (__bridge CFDictionaryRef) @{
                                                               (id) kCGImageSourceCreateThumbnailWithTransform : @YES,
                                                               (id) kCGImageSourceCreateThumbnailFromImageAlways : @YES,
                                                               (id) kCGImageSourceThumbnailMaxPixelSize : @(maxPixel)
                                                               };
        CGImageRef thumbnail = CGImageSourceCreateThumbnailAtIndex(src, 0, options);
        CFRelease(src);
        
        image = [UIImage imageWithCGImage:thumbnail];
        
    } else if ([extension isEqualToString:@"mov"]) {
        
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:fileURL options:nil];
        
        if (!asset && usingICloud) {
            [fileManager startDownloadingUbiquitousItemAtURL:fileURL error:nil];
            
            return nil;
        }
        
        AVAssetImageGenerator *imageGen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        imageGen.appliesPreferredTrackTransform = YES;
        CMTime time = CMTimeMakeWithSeconds(0.0, 1);
        NSError *error = nil;
        CMTime actualTime;
        
        
        CGImageRef imageRef = [imageGen copyCGImageAtTime:time actualTime:&actualTime error:&error];
        image = [[UIImage alloc] initWithCGImage:imageRef];
        CGImageRelease(imageRef);
        
    }
    
    return image;
}


- (NSURL *)getBaseURLForFotoImovel
{
    NSURL *url;
    
    url = [Utils getLocalFotosURL];
    
    return url;
}

- (void)upload
{
    
    if (![self isLocalFileExists]) {
        return;
    }
    
    self.pendingUpload = [NSNumber numberWithBool:YES];
    [self.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    if(![Utils isConnected]) {
        return;
    }
    
    // If not TOP don't upload.
    if (![[Utils iapHelper] checkPlanTopFromController:nil withMessage:nil]) {
        return;
    }
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    NSURL *fileURL = [[Utils getLocalFotosURL] URLByAppendingPathComponent:self.imagemURL];
    
    NSString *email = [Utils valueFromDefaults:FIREBASE_USER_ID];
    NSString *fileName = [fileURL lastPathComponent];
    NSString *filePath = [NSString stringWithFormat:@"%@/fotos/%@",email,fileName];
    
    ASIS3ObjectRequest *request =
    [ASIS3ObjectRequest PUTRequestForFile:fileURL.path withBucket:S3_BUCKET_NAME key:filePath];
    
    [request setTimeOutSeconds:60];
    [request startSynchronous];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    if (request.error) {
        NSLog(@"%@", request.error.localizedDescription);
        
    } else {
        [self.managedObjectContext performBlockAndWait:^{
            self.pendingUpload = [NSNumber numberWithBool:NO];
            [self.managedObjectContext MR_saveToPersistentStoreAndWait];
            [self.imovel saveFirebase];
        }];
        
        
        // thumbnail
        //   [self uploadThumbnail:fileURL];
    }
}



- (NSURL *)thumbnail:(UIImage *)sourceImage withName:(NSString *)imageName
{
    CGSize destinationSize = CGSizeMake(100, 182);
    
    UIGraphicsBeginImageContext(destinationSize);
    [sourceImage drawInRect:CGRectMake(0,0,destinationSize.width,destinationSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    imageName = [NSString stringWithFormat:@"%@_thumbnail.%@",[[imageName componentsSeparatedByString:@"."] firstObject],[[imageName componentsSeparatedByString:@"."] lastObject]];
    
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:@"upload"];
    NSString *filePath = [[NSTemporaryDirectory() stringByAppendingPathComponent:@"upload"] stringByAppendingPathComponent:imageName];
    NSData *imageData = UIImageJPEGRepresentation(newImage, 0.3);
    
    NSError *error = nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:NO attributes:nil error:&error];
    }
    
    [imageData writeToFile:filePath atomically:YES];
    NSURL *thumbnailURL = [NSURL fileURLWithPath:filePath];
    
    
    return thumbnailURL;
}


- (void)uploadThumbnail:(NSURL *)fileURL
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    NSString *email = [Utils valueFromDefaults:FIREBASE_USER_ID];
    NSString *fileName = [fileURL lastPathComponent];
    
    NSData *imageData = [NSData dataWithContentsOfURL:fileURL];
    UIImage *sourceImage = [UIImage imageWithData:imageData];
    NSURL *thumbnail = [self thumbnail:sourceImage withName:fileName];
    NSString *filePath = [NSString stringWithFormat:@"%@/fotos/%@",email,thumbnail.lastPathComponent];
    
    
    ASIS3ObjectRequest *request =
    [ASIS3ObjectRequest PUTRequestForFile:fileURL.path withBucket:S3_BUCKET_NAME key:filePath];
    
    [request setTimeOutSeconds:60];
    [request startSynchronous];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    if (request.error) {
        NSLog(@"%@", request.error);
        self.pendingUpload = [NSNumber numberWithBool:YES];
        [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
        
    } else {
        //        self.pendingUpload = [NSNumber numberWithBool:NO];
        //        [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
    }
    
}

- (void)deleteImageFromS3
{
    
    if (self.pendingUpload.boolValue) {
        return;
    }
    
    [self addPendingDelete];
    
    if(![Utils isConnected]) {
        return;
    }
    
    NSString *email = [Utils valueFromDefaults:FIREBASE_USER_ID];
    
    NSString *filePath = [NSString stringWithFormat:@"%@/fotos/%@",email,self.imagemURL];
    
    // image
    [S3Utils delete:filePath];
    
    //    // thumbnai;
    NSString *extension = [[self.imagemURL componentsSeparatedByString:@"."] lastObject];
    NSString *filename = [[self.imagemURL componentsSeparatedByString:@"."] firstObject];
    filePath = [NSString stringWithFormat:@"%@/fotos/%@_thumbnail.%@",email,filename, extension];
    [S3Utils delete:filePath];
    
}


- (void)addPendingDelete
{
    NSMutableArray *toDeleteArray = [[Utils valueFromDefaults:S3_PENDING_DELETED_IMAGE] mutableCopy];
    if (!toDeleteArray) {
        toDeleteArray = [NSMutableArray array];
    }
    
    [toDeleteArray addObject:self.imagemURL];
    
}

- (void)removePendingDelete
{
    NSMutableArray *toDeleteArray = [[Utils valueFromDefaults:S3_PENDING_DELETED_IMAGE] mutableCopy];
    
    [toDeleteArray removeObject:self.imagemURL];
    
}


@end
