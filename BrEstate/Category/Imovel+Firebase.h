//
//  Imovel+Firebase.h
//  Appci
//
//  Created by Leonardo Barros on 23/12/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Imovel.h"

@interface Imovel (Firebase)

- (NSString *)createFirebaseID;
- (void)saveFirebase;
- (void)deleteFirebase;
- (NSDictionary *)dictionaryFirebase;
- (NSDictionary *)getRelationshipFirebase;

+ (void)saveAllPendingFirebase;
+ (void)startFirebaseObservers;
+ (void)convertImoveisInBatchWithDictionary:(NSDictionary *)imoveisDict;

@end
