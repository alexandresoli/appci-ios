//
//  Imovel+Firebase.m
//  Appci
//
//  Created by Leonardo Barros on 23/12/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Imovel+Firebase.h"
#import "Cliente+Firebase.h"
#import "TagImovel+Firebase.h"
#import "FotoImovel+Firebase.h"
#import "Endereco+Firebase.h"
#import "Bairro+Firebase.h"
#import "Evento+Firebase.h"
#import "Acompanhamento+Firebase.h"
#import "Negociacao+Firebase.h"
#import "FotoImovel+InsertDeleteImagem.h"

@implementation Imovel (Firebase)


#pragma mark - Dictionary Firebase

- (NSDictionary *)dictionaryFirebase
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    [dic setObject:self.areaUtil != nil ? self.areaUtil : [NSNull null] forKey:@"areaUtil"];
    [dic setObject:self.areaTotal != nil ? self.areaTotal : [NSNull null] forKey:@"areaTotal"];
    [dic setObject:self.codigo != nil ? self.codigo : [NSNull null] forKey:@"codigo"];
    [dic setObject:self.comissao != nil ? self.comissao : [NSNull null] forKey:@"comissao"];
    [dic setObject:self.condominio != nil ? self.condominio : [NSNull null] forKey:@"condominio"];
    [dic setObject:self.descricao != nil ? self.descricao : [NSNull null] forKey:@"descricao"];
    [dic setObject:self.identificacao != nil ? self.identificacao : [NSNull null] forKey:@"identificacao"];
    [dic setObject:self.iptu != nil ? self.iptu : [NSNull null] forKey:@"iptu"];
    [dic setObject:self.notaPrivada != nil ? self.notaPrivada : [NSNull null] forKey:@"notaPrivada"];
    [dic setObject:self.quartos != nil ? self.quartos : [NSNull null] forKey:@"quartos"];
    [dic setObject:self.valor != nil ? self.valor : [NSNull null] forKey:@"valor"];
    [dic setObject:self.oferta != nil ? self.oferta : [NSNull null] forKey:@"oferta"];
    [dic setObject:self.tipo != nil ? self.tipo : [NSNull null] forKey:@"tipo"];
    [dic setObject:self.codigoAnterior != nil ? self.codigoAnterior : [NSNull null] forKey:@"codigoAnterior"];
    [dic setObject:[Utils deviceID] forKey:@"deviceToken"];
    
    // firebaseID
    [dic setObject:self.createFirebaseID forKey:@"firebaseID"];
    
    // version
    if (!self.version) {
        self.version = [NSDate date];
    }
    [dic setObject:[DateUtils stringTimestampFromDate:self.version] forKey:@"version"];
    
    // cliente
    if (self.cliente) {
        [dic setObject:self.cliente.getRelationshipFirebase forKey:@"cliente"];
    } else {
        [dic setObject:[NSNull null] forKey:@"cliente"];
    }
    
    // tag
    if (self.tags) {
        [dic setObject:[self.tags dictionaryFirebase] forKey:@"tags"];
    } else {
        [dic setObject:[NSNull null] forKey:@"tags"];
    }
    
    // fotos
    if (self.fotos && self.fotos.count > 0) {
        NSMutableDictionary *fotosDic = [NSMutableDictionary new];
        
        for (FotoImovel *foto in self.fotos) {
            [fotosDic setObject:[foto dictionaryFirebase] forKey:[foto createFirebaseID]];
        }
        
        [dic setObject:fotosDic forKey:@"fotos"];
        
    } else {
        [dic setObject:[NSNull null] forKey:@"fotos"];
    }
    
    // endereco
    if (self.endereco) {
        [dic setObject:self.endereco.dictionaryFirebase forKey:@"endereco"];
        
    } else {
        [dic setObject:[NSNull null] forKey:@"endereco"];
    }
    
    return dic;
}


#pragma mark - Create Firebase ID

- (NSString *)createFirebaseID
{
    if (self.firebaseID) {
        return self.firebaseID;
    }
    
    Firebase *classRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Imovel"];
    
    Firebase *objectRef = [classRef childByAutoId];
    
    self.firebaseID = objectRef.key;
    
    [self.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    return objectRef.key;
}


#pragma mark - Get Relationship Firebase

- (NSDictionary *)getRelationshipFirebase
{
    return @{@"firebaseID": self.createFirebaseID,
             @"codigo": self.codigo};
}


#pragma mark - Save Firebase

- (void)saveFirebase
{
    self.version = [NSDate date];
    self.isSavedInFirebase = [NSNumber numberWithBool:NO];
    
    // If not TOP don't save on Firebase.
    if (![[Utils iapHelper] checkPlanTopFromController:nil withMessage:nil]) {
        [self.managedObjectContext MR_saveToPersistentStoreAndWait];
        return;
    }
    
    NSDictionary *dictImovel = self.dictionaryFirebase;
    
    [[Utils firebaseUtils] saveObjectWithDictionary:dictImovel andClassName:@"Imovel" andCompletionBlock:^(NSError *error) {
        [self.managedObjectContext performBlock:^{
            if (!error) {
                self.isSavedInFirebase = [NSNumber numberWithBool:YES];
            }
            [self.managedObjectContext MR_saveToPersistentStoreAndWait];
        }];
    }];
    
//    if (self.cliente) {
//        [self.cliente saveFirebase];
//    }
//    
//    if (self.endereco.bairro) {
//        [self.endereco.bairro saveFirebase];
//    }
}


+ (void)saveAllPendingFirebase
{
    NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSavedInFirebase = NO OR isSavedInFirebase = nil"];
    
    NSArray *imoveis = [Imovel MR_findAllWithPredicate:predicate inContext:moc];
    if (imoveis.count == 0) {
        return;
    }
    
    NSMutableDictionary *imoveisDic = [NSMutableDictionary new];
    
    for (Imovel *imovel in imoveis) {
        [imoveisDic setValue:imovel.dictionaryFirebase forKey:imovel.firebaseID];
    }
    
    dispatch_semaphore_t semaImovel = dispatch_semaphore_create(0);
    
    [moc MR_saveToPersistentStoreAndWait];
    
    Firebase *imovelRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Imovel"];
    [imovelRef updateChildValues:imoveisDic withCompletionBlock:^(NSError *error, Firebase *ref) {
        
        [moc performBlock:^{
            if (!error) {
                for (Imovel *imovel in imoveis) {
                    imovel.isSavedInFirebase = [NSNumber numberWithBool:YES];
                }
                [moc MR_saveToPersistentStoreAndWait];
            }
            
            [Utils postEntitytNotificationWithValue:@"UpdateSyncCounter" value:imoveis.count];
            dispatch_semaphore_signal(semaImovel);
        }];
    }];
    dispatch_semaphore_wait(semaImovel, DISPATCH_TIME_FOREVER);
}


#pragma mark - Convert Firebase Dictionary

- (void)convertFirebaseDictionary:(NSDictionary *)dictImovel withMoc:(NSManagedObjectContext *)moc
{
    self.codigo = dictImovel[@"codigo"];
    self.areaUtil = dictImovel[@"areaUtil"];
    self.areaTotal = dictImovel[@"areaTotal"];
    self.comissao = dictImovel[@"comissao"];
    self.condominio = dictImovel[@"condominio"];
    self.descricao = dictImovel[@"descricao"];
    self.identificacao = dictImovel[@"identificacao"];
    self.iptu = dictImovel[@"iptu"];
    self.notaPrivada = dictImovel[@"notaPrivada"];
    self.quartos = dictImovel[@"quartos"];
    self.valor = dictImovel[@"valor"];
    self.oferta = dictImovel[@"oferta"];
    self.tipo = dictImovel[@"tipo"];
    self.firebaseID = dictImovel[@"firebaseID"];
    self.codigoAnterior = dictImovel[@"codigoAnterior"];
    
    self.isSavedInFirebase = [NSNumber numberWithBool:YES];
    
    self.version = [DateUtils dateFromStringTimestamp:dictImovel[@"version"]];
    
    // Tags
    if (dictImovel[@"tags"]) {
        TagImovel *tags;
        NSDictionary *dictTags = dictImovel[@"tags"];
        
        if (self.tags) {
            tags = self.tags;
        } else {
            tags = [TagImovel MR_createInContext:moc];
        }
        tags.codigo = dictTags[@"codigo"];
        tags.armarioCozinha = dictTags[@"armarioCozinha"];
        tags.armarioEmbutido = dictTags[@"armarioEmbutido"];
        tags.childrenCare = dictTags[@"childrenCare"];
        tags.churrasqueira = dictTags[@"churrasqueira"];
        tags.garagem = dictTags[@"garagem"];
        tags.piscina = dictTags[@"piscina"];
        tags.playground = dictTags[@"playground"];
        tags.quadraPoliesportiva = dictTags[@"quadraPoliesportiva"];
        tags.salaGinastica = dictTags[@"salaGinastica"];
        tags.salaoFestas = dictTags[@"salaoFestas"];
        tags.salaoJogos = dictTags[@"salaoJogos"];
        tags.sauna = dictTags[@"sauna"];
        tags.varanda = dictTags[@"varanda"];
        tags.wcEmpregada = dictTags[@"wcEmpregada"];
        tags.imovel = self;
    }
    
    // cliente
    if (dictImovel[@"cliente"]) {
        
        NSDictionary *clienteDict = dictImovel[@"cliente"];
        
        if ([clienteDict isKindOfClass:[NSDictionary class]]) {
            Cliente *cliente = [Cliente MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", clienteDict[@"firebaseID"]]
                                                        inContext:moc];
            
            if (!cliente) {
                cliente = [Cliente MR_createInContext:moc];
                cliente.firebaseID = clienteDict[@"firebaseID"];
                cliente.codigo = clienteDict[@"codigo"];
                cliente.isSavedInFirebase = [NSNumber numberWithBool:YES];
                cliente.nome = @"Baixando informações";
            }
            self.cliente = cliente;
        }
    }
    
    // fotos
    if (dictImovel[@"fotos"]) {
        NSDictionary *fotos = dictImovel[@"fotos"];
        
        for (NSString *fotoFirebaseID in fotos.allKeys) {
            FotoImovel *foto = [FotoImovel MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", fotoFirebaseID]
                                                           inContext:moc];
            
            if (!foto) {
                
                // Necessary because of the migration from Parse where users have more than one device.
                NSDictionary *fotoDict = [fotos objectForKey:fotoFirebaseID];
                
                NSPredicate *predicate;
                NSNumber *codigoAnterior = fotoDict[@"codigoAnterior"];
                if (codigoAnterior && [codigoAnterior intValue] > 0) {
                    predicate = [NSPredicate predicateWithFormat:@"codigoAnterior = %@ AND imovel = %@", fotoDict[@"codigoAnterior"], self];
                } else {
                    predicate = [NSPredicate predicateWithFormat:@"codigo = %@ AND imovel = %@", fotoDict[@"codigo"], self];
                }
                
                foto = [FotoImovel MR_findFirstWithPredicate:predicate inContext:moc];
                
                if (!foto) {
                    foto = [FotoImovel MR_createInContext:moc];
                }
            }
            
            [foto convertFirebaseDictionary:[fotos objectForKey:fotoFirebaseID] withMoc:moc];
            [self addFotosObject:foto];
            
            // amazon s3
            [foto downloadFromS3];
        }
    }
    
    // endereco
    if (dictImovel[@"endereco"]) {
        if (!self.endereco) {
            self.endereco = [Endereco MR_createInContext:moc];
        }
        
        [self.endereco convertFirebaseDictionary:dictImovel[@"endereco"] withMoc:moc];
    }
}


#pragma mark - Delete Firebase

- (void)deleteFirebase
{
    [[Utils firebaseUtils] deleteObjectWithID:self.firebaseID andClassName:@"Imovel"];
    
    // Remove reference on Evento.
    NSArray *eventos = self.evento.allObjects;
    for (Evento *evento in eventos) {
        evento.imovel = nil;
        [evento saveFirebase];
    }
    
    // Remove reference on Acompanhamento.
    NSArray *acompanhamentos = self.acompanhamento.allObjects;
    for (Acompanhamento *acomp in acompanhamentos) {
        acomp.imovel = nil;
        
        // Acompanhamento is only for imovel, can delete.
        if (!acomp.cliente) {
            [acomp deleteFirebase];
        } else {
            [acomp saveFirebase];
        }
    }
    
    // Remove reference on Negociacao.
    NSArray *negociacoes = self.negociacao.allObjects;
    for (Negociacao *neg in negociacoes) {
        neg.imovel = nil;
        [neg deleteFirebase];
    }
}


#pragma mark - Convert Imoveis In Batch

+ (void)convertImoveisInBatchWithDictionary:(NSDictionary *)imoveisDict
{
    if (!imoveisDict || [imoveisDict isEqual:[NSNull null]] || imoveisDict.count == 0) {
        return;
    }
    
    NSManagedObjectContext *context = [NSManagedObjectContext MR_contextForCurrentThread];
    
    [context performBlockAndWait:^{
        
        BOOL hasNew = NO;
        NSNumber *alreadyDownloadAll = [Utils valueFromDefaults:FIREBASE_ALREADY_RUN];
        
        // Not the first time.
        if (alreadyDownloadAll) {
            
            for (NSString *firebaseID in imoveisDict.allKeys) {
                
                NSDictionary *oneImovel = [imoveisDict objectForKey:firebaseID];
                
                // Check if is not the first download and the change was made by the current device.
                if (alreadyDownloadAll && [oneImovel[@"deviceToken"] isEqualToString:[Utils deviceID]]) {
                    continue;
                }
                
                Imovel *imovel = [Imovel MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", firebaseID]
                                                         inContext:context];
                
                if (!imovel) {
                    
                    // Necessary because of the migration from Parse where users have more than one device.
                    NSPredicate *predicate = [Imovel uniquePredicateWithDictionary:oneImovel];
                    imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:context];
                    
                    if (!imovel) {
                        imovel = [Imovel MR_createInContext:context];
                    }
                    
                } else {
                    NSString *objVersion = oneImovel[@"version"];
                    NSString *lastVersion = [Utils valueFromDefaults:FIREBASE_IMOVEL_INSERT_VERSION];
                    
                    if ([objVersion compare:lastVersion] == NSOrderedAscending || [objVersion compare:lastVersion] == NSOrderedSame) {
                        continue;
                    }
                }
                
                hasNew = YES;
                
                [imovel convertFirebaseDictionary:oneImovel withMoc:context];
            }
            
        } else { // First Download
            
            // Sort dictionary by version to the most recent keep on device in duplicates case.
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"version" ascending:NO];
            NSMutableArray *imoveisArray = [imoveisDict.allValues mutableCopy];
            [imoveisArray sortUsingDescriptors:@[descriptor]];
            
            NSMutableArray *checkImoveisArray = [NSMutableArray new];
            
            [Imovel MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"codigo > 0"] inContext:context];
            
            for (NSDictionary *oneImovel in imoveisArray) {
                
                @autoreleasepool {
                    NSPredicate *predicate = [Imovel uniquePredicateWithDictionary:oneImovel];
                    NSDictionary *checkImovel = [checkImoveisArray filteredArrayUsingPredicate:predicate].firstObject;
                    
                    if (!checkImovel) {
                        Imovel *imovel = [Imovel MR_createInContext:context];
                        [imovel convertFirebaseDictionary:oneImovel withMoc:context];
                        
                        [checkImoveisArray addObject:oneImovel];
                        
                        hasNew = YES;
                    }
                }
            }
        }
        
        if (hasNew) {
            [context MR_saveToPersistentStoreAndWait];
            
            NSArray *imoveisArray = [Imovel MR_findAllSortedBy:@"version" ascending:YES inContext:context];
            Imovel *lastBairro = imoveisArray.lastObject;
            
            [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:REFRESH_IMOVEL];
            [Utils addObjectToDefaults:[DateUtils stringTimestampFromDate:lastBairro.version] withKey:FIREBASE_IMOVEL_INSERT_VERSION];
        }
    }];
}


#pragma mark - Unique Predicate

+ (NSPredicate *)uniquePredicateWithDictionary:(NSDictionary *)dict
{
    NSMutableArray *predicateArray = [NSMutableArray array];
    
    NSNumber *codigoAnterior = dict[@"codigoAnterior"];
    if (codigoAnterior && [codigoAnterior intValue] > 0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"codigoAnterior = %@", dict[@"codigoAnterior"]]];
        
    } else {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"codigo = %@", dict[@"codigo"]]];
    }
    
    if (dict[@"identificacao"]) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"identificacao = %@", dict[@"identificacao"]]];
        
    } else {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"identificacao = nil or identificacao = ''"]];
    }
    
    NSPredicate *coumpoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    
    return coumpoundPredicate;
}


#pragma mark - Observers

+ (void)startFirebaseObservers
{
    [Imovel startObserverAddedAndChangeImovel];
}


+ (void)startObserverAddedAndChangeImovel
{
    Firebase *imovelRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Imovel"];
    
    NSString *versionString = [Utils valueFromDefaults:FIREBASE_IMOVEL_INSERT_VERSION];
    
    if (!versionString) {
        versionString = [DateUtils stringTimestampFromDate:[NSDate dateWithTimeIntervalSince1970:1]];
    }
    
    // Add 1 millisecond because the firebase query is always >=
    versionString = [NSString stringWithFormat:@"%lld", [versionString longLongValue]+1];
    
    [[[imovelRef queryOrderedByChild:@"version"] queryStartingAtValue:versionString]
     observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
         
         if ([snapshot.value isEqual:[NSNull null]]) {
             return;
         }
         
         dispatch_async([Utils firebaseUtils].queueDownload, ^{
             
             NSDictionary *imoveisDict = snapshot.value;
             
             [Imovel convertImoveisInBatchWithDictionary:imoveisDict];
         });
     }];
}


#pragma mark - Photos Downloads
+ (void)downloadPhotos:(Imovel *)imovel
{
    for (FotoImovel *photo in [imovel.fotos allObjects]) {
        [photo downloadFromS3];
    }
}

@end
