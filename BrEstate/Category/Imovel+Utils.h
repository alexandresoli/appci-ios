//
//  Imovel+Utils.h
//  Appci
//
//  Created by BrEstate LTDA on 03/06/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Imovel.h"

@interface Imovel (Utils)

@property (nonatomic, weak, readonly) NSString *sectionIdentifier;

@end
