//
//  Imovel+Utils.m
//  Appci
//
//  Created by BrEstate LTDA on 03/06/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Imovel+Utils.h"
#import "Endereco.h"
#import "Bairro.h"

@implementation Imovel (Utils)


- (NSString *)sectionIdentifier
{
    if (self.endereco.bairro.nome) {
        return self.endereco.bairro.nome;
    } else {
        return @"Bairro Não Cadastrado";
    }
}


@end
