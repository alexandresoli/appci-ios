//
//  MapPin.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/6/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>

@import MapKit;

@interface MapPin : NSObject <MKAnnotation>

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, readonly, copy) NSString *title;
@property (nonatomic, readonly, copy) NSString *subtitle;

- (id)initWithCoordinates:(CLLocationCoordinate2D)location placeName:(NSString *)placeName description:(NSString *)description;

@end
