//
//  MapPin.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/6/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "MapPin.h"

@implementation MapPin

- (id)initWithCoordinates:(CLLocationCoordinate2D)location placeName:placeName description:description
{
    self = [super init];
    
    if (self) {
        
        _coordinate = location;
        _title = placeName;
        _subtitle = description;
        
    }
    return self;
}

@end
