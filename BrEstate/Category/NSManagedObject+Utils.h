//
//  NSManagedObject+Utils.h
//  Appci
//
//  Created by Leonardo Barros on 21/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (Utils)

- (NSNumber *)getUniqueCode;

@end
