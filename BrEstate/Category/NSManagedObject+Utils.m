//
//  NSManagedObject+Utils.m
//  Appci
//
//  Created by Leonardo Barros on 21/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "NSManagedObject+Utils.h"

@implementation NSManagedObject (Utils)


#pragma mark - Get Unique Code

- (NSNumber *)getUniqueCode
{
    NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
    
    NSString *className = NSStringFromClass([self class]);
    long count = [CoreDataUtils countEntity:className withPredicate:nil inContext:moc];
    
    NSNumber *dateNumber = [NSNumber numberWithDouble:round([[NSDate date] timeIntervalSince1970] * 1000)];
    long long time = [dateNumber longLongValue] + count + arc4random_uniform(10000);
    
    return [NSNumber numberWithLongLong:time];
}


@end
