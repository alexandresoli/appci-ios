//
//  NSSet+Custom.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/3/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "NSSet+Custom.h"

@implementation NSSet (Custom)

- (BOOL)containsKindOfClass:(Class)class {
    for (id element in self) {
        if ([element isKindOfClass:class])
            return YES;
    }
    return NO;
}

- (BOOL)containsMemberOfClass:(Class)class {
    for (id element in self) {
        if ([element isMemberOfClass:class])
            return YES;
    }
    return NO;
}


@end
