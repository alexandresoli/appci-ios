//
//  Negociacao+Firebase.h
//  Appci
//
//  Created by Leonardo Barros on 08/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "Negociacao.h"

@interface Negociacao (Firebase)

- (NSString *)createFirebaseID;
- (void)saveFirebase;
- (void)deleteFirebase;

+ (void)saveAllPendingFirebase;
+ (void)startFirebaseObservers;
+ (void)convertNegociacoesInBatchWithDictionary:(NSDictionary *)negDict;

@end
