//
//  Negociacao+Firebase.m
//  Appci
//
//  Created by Leonardo Barros on 08/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "Negociacao+Firebase.h"
#import "Cliente+Firebase.h"
#import "Imovel+Firebase.h"

@implementation Negociacao (Firebase)


#pragma mark - Create Firebase ID

- (NSString *)createFirebaseID
{
    if (self.firebaseID) {
        return self.firebaseID;
    }
    
    Firebase *classRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Negociacao"];
    
    Firebase *objectRef = [classRef childByAutoId];
    
    self.firebaseID = objectRef.key;
    
    [self.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    return objectRef.key;
}


#pragma mark - Dictionary Firebase

- (NSDictionary *)dictionaryFirebase
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    NSString *dataString = [DateUtils stringTimestampFromDate:self.data];
    
    [dic setObject:self.codigo != nil ? self.codigo : [NSNull null] forKey:@"codigo"];
    [dic setObject:self.comissao != nil ? self.comissao : [NSNull null] forKey:@"comissao"];
    [dic setObject:self.data != nil ? dataString : [NSNull null] forKey:@"data"];
    [dic setObject:self.notas != nil ? self.notas : [NSNull null] forKey:@"notas"];
    [dic setObject:self.parceria != nil ? self.parceria : [NSNull null] forKey:@"parceria"];
    [dic setObject:self.percentualParceria != nil ? self.percentualParceria : [NSNull null] forKey:@"percentualParceria"];
    [dic setObject:self.valor != nil ? self.valor : [NSNull null] forKey:@"valor"];
    [dic setObject:self.status != nil ? self.status : [NSNull null] forKey:@"status"];
    [dic setObject:[Utils deviceID] forKey:@"deviceToken"];
    
    // version
    if (!self.version) {
        self.version = [NSDate date];
    }
    [dic setObject:[DateUtils stringTimestampFromDate:self.version] forKey:@"version"];
    
    // firebaseID
    [dic setObject:self.createFirebaseID forKey:@"firebaseID"];
    
    // imovel
    if (self.imovel) {
        [dic setObject:self.imovel.getRelationshipFirebase forKey:@"imovel"];
    } else {
        [dic setObject:[NSNull null] forKey:@"imovel"];
    }
    
    // cliente
    if (self.cliente) {
        [dic setObject:self.cliente.getRelationshipFirebase forKey:@"cliente"];
    } else {
        [dic setObject:[NSNull null] forKey:@"cliente"];
    }
    
    return dic;
}


#pragma mark - Convert Firebase Dictionary

- (void)convertFirebaseDictionary:(NSDictionary *)dictNegociacao withMoc:(NSManagedObjectContext *)moc
{
    self.codigo = dictNegociacao[@"codigo"];
    self.comissao = dictNegociacao[@"comissao"];
    self.data = [DateUtils dateFromStringTimestamp:dictNegociacao[@"data"]];
    self.notas = dictNegociacao[@"notas"];
    self.parceria = dictNegociacao[@"parceria"];
    self.percentualParceria = dictNegociacao[@"percentualParceria"];
    self.valor = dictNegociacao[@"valor"];
    self.status = dictNegociacao[@"status"];
    
    self.firebaseID = dictNegociacao[@"firebaseID"];
    
    self.isSavedInFirebase = [NSNumber numberWithBool:YES];
    
    self.version = [DateUtils dateFromStringTimestamp:dictNegociacao[@"version"]];
    
    // cliente
    if (dictNegociacao[@"cliente"]) {
        NSDictionary *clienteDict = dictNegociacao[@"cliente"];
        
        Cliente *cliente = [Cliente MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", clienteDict[@"firebaseID"]]
                                                    inContext:moc];
        
        if (!cliente) {
            cliente = [Cliente MR_createInContext:moc];
            cliente.firebaseID = clienteDict[@"firebaseID"];
            cliente.codigo = clienteDict[@"codigo"];
            cliente.isSavedInFirebase = [NSNumber numberWithBool:YES];
            cliente.nome = @"Baixando informações";
        }
        self.cliente = cliente;
    }
    
    // imovel
    if (dictNegociacao[@"imovel"]) {
        NSDictionary *imovelDict = dictNegociacao[@"imovel"];
        
        Imovel *imovel = [Imovel MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", imovelDict[@"firebaseID"]]
                                                 inContext:moc];
        
        if (!imovel) {
            imovel = [Imovel MR_createInContext:moc];
            imovel.firebaseID = imovelDict[@"firebaseID"];
            imovel.codigo = imovelDict[@"codigo"];
            imovel.isSavedInFirebase = [NSNumber numberWithBool:YES];
        }
        self.imovel = imovel;
    }
}


#pragma mark - Delete Firebase

- (void)deleteFirebase
{
    [[Utils firebaseUtils] deleteObjectWithID:self.firebaseID andClassName:@"Negociacao"];
}


#pragma mark - Save Firebase

- (void)saveFirebase
{
    self.isSavedInFirebase = [NSNumber numberWithBool:NO];
    
    // If not TOP don't save on Firebase.
    if (![[Utils iapHelper] checkPlanTopFromController:nil withMessage:nil]) {
        [self.managedObjectContext MR_saveToPersistentStoreAndWait];
        return;
    }
    
    [[Utils firebaseUtils] saveObjectWithDictionary:self.dictionaryFirebase andClassName:@"Negociacao" andCompletionBlock:^(NSError *error) {
        [self.managedObjectContext performBlock:^{
            if (!error) {
                self.isSavedInFirebase = [NSNumber numberWithBool:YES];
            }
            [self.managedObjectContext MR_saveToPersistentStoreAndWait];
        }];
    }];
}


+ (void)saveAllPendingFirebase
{
    NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSavedInFirebase = NO OR isSavedInFirebase = nil"];
    
    NSArray *negociacoes = [Negociacao MR_findAllWithPredicate:predicate inContext:moc];
    if (negociacoes.count == 0) {
        return;
    }
    
    NSMutableDictionary *negociacoesDic = [NSMutableDictionary new];
    
    for (Negociacao *negociacao in negociacoes) {
        [negociacoesDic setValue:negociacao.dictionaryFirebase forKey:negociacao.firebaseID];
    }
    
    dispatch_semaphore_t semaNegociacao = dispatch_semaphore_create(0);
    
    [moc MR_saveToPersistentStoreAndWait];
    
    Firebase *negociacaoRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Negociacao"];
    [negociacaoRef updateChildValues:negociacoesDic withCompletionBlock:^(NSError *error, Firebase *ref) {
        
        [moc performBlock:^{
            if (!error) {
                for (Negociacao *negociacao in negociacoes) {
                    negociacao.isSavedInFirebase = [NSNumber numberWithBool:YES];
                }
                [moc MR_saveToPersistentStoreAndWait];
            }
            
            [Utils postEntitytNotificationWithValue:@"UpdateSyncCounter" value:negociacoes.count];            
            dispatch_semaphore_signal(semaNegociacao);
        }];
    }];
    
    dispatch_semaphore_wait(semaNegociacao, DISPATCH_TIME_FOREVER);
}


#pragma mark - Convert Negociacoes In Batch

+ (void)convertNegociacoesInBatchWithDictionary:(NSDictionary *)negDict
{
    if (!negDict || [negDict isEqual:[NSNull null]] || negDict.count == 0) {
        return;
    }
    
    NSManagedObjectContext *context = [NSManagedObjectContext MR_contextForCurrentThread];
    
    [context performBlockAndWait:^{
        
        BOOL hasNew = NO;
        NSNumber *alreadyDownloadAll = [Utils valueFromDefaults:FIREBASE_ALREADY_DOWNLOAD_ALL];
        
        for (NSString *firebaseID in negDict.allKeys) {
            
            NSDictionary *oneNeg = [negDict objectForKey:firebaseID];
            
            // Check if is not the first download and the change was made by the current device.
            if (alreadyDownloadAll && [oneNeg[@"deviceToken"] isEqualToString:[Utils deviceID]]) {
                continue;
            }
            
            Negociacao *neg = [Negociacao MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", firebaseID]
                                                          inContext:context];
            
            if (!neg) {
                
                // Necessary because of the migration from Parse where users have more than one device.
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"data = %@ AND status = %@", [DateUtils dateFromStringTimestamp:oneNeg[@"data"]], oneNeg[@"status"]];
                neg = [Negociacao MR_findFirstWithPredicate:predicate inContext:context];
                
                if (!neg) {
                    neg = [Negociacao MR_createInContext:context];
                }
                
            } else {
                NSString *objVersion = oneNeg[@"version"];
                NSString *lastVersion = [Utils valueFromDefaults:FIREBASE_NEGOCIACAO_INSERT_VERSION];
                
                if ([objVersion compare:lastVersion] == NSOrderedAscending || [objVersion compare:lastVersion] == NSOrderedSame) {
                    continue;
                }
            }
            
            hasNew = YES;
            
            [neg convertFirebaseDictionary:oneNeg withMoc:context];
        }
        
        if (hasNew) {
            [context MR_saveToPersistentStoreAndWait];
            
            NSArray *negArray = [Negociacao MR_findAllSortedBy:@"version" ascending:YES inContext:context];
            Negociacao *lastNeg = negArray.lastObject;
            
            [Utils addObjectToDefaults:[DateUtils stringTimestampFromDate:lastNeg.version] withKey:FIREBASE_NEGOCIACAO_INSERT_VERSION];
        }
    }];
}


#pragma mark - Observers

+ (void)startFirebaseObservers
{
    [Negociacao startObserverAddedAndChangeNegociacao];
}


+ (void)startObserverAddedAndChangeNegociacao
{
    Firebase *negociacaoRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Negociacao"];
    
    NSString *versionString = [Utils valueFromDefaults:FIREBASE_NEGOCIACAO_INSERT_VERSION];
    
    if (!versionString) {
        versionString = [DateUtils stringTimestampFromDate:[NSDate dateWithTimeIntervalSince1970:1]];
    }
    
    // Add 1 millisecond because the firebase query is always >=
    versionString = [NSString stringWithFormat:@"%lld", [versionString longLongValue]+1];
    
    [[[negociacaoRef queryOrderedByChild:@"version"] queryStartingAtValue:versionString]
     observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
         
         if ([snapshot.value isEqual:[NSNull null]]) {
             return;
         }
         
         dispatch_async([Utils firebaseUtils].queueDownload, ^{
             
             NSDictionary *negDict = snapshot.value;
             
             [Negociacao convertNegociacoesInBatchWithDictionary:negDict];
         });
     }];
}


@end
