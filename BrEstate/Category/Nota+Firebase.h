//
//  Nota+Firebase.h
//  Appci
//
//  Created by Leonardo Barros on 08/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "Nota.h"

@interface Nota (Firebase)

- (NSString *)createFirebaseID;
- (void)saveFirebase;
- (void)deleteFirebase;
- (void)uploadToS3;

+ (void)saveAllPendingFirebase;
+ (void)startFirebaseObservers;
+ (void)convertNotasInBatchWithDictionary:(NSDictionary *)notasDict;
+ (void)sendAllPendingNotasToAmazon;

@end
