//
//  Nota+Firebase.m
//  Appci
//
//  Created by Leonardo Barros on 08/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "Nota+Firebase.h"
#import "S3Utils.h"
#import "ASIS3ObjectRequest.h"

@implementation Nota (Firebase)


#pragma mark - Create Firebase ID

- (NSString *)createFirebaseID
{
    if (self.firebaseID) {
        return self.firebaseID;
    }
    
    Firebase *classRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Nota"];
    
    Firebase *objectRef = [classRef childByAutoId];
    
    self.firebaseID = objectRef.key;
    
    if (self.arquivo != nil) {
        [self uploadToS3];
    }
    
    [self.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    return objectRef.key;
}

#pragma mark - Dictionary Firebase

- (NSDictionary *)dictionaryFirebase
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    NSString *timestamp = [DateUtils stringTimestampFromDate:self.timeStamp];
    
    [dic setObject:self.codigo != nil ? self.codigo : [NSNull null] forKey:@"codigo"];
    [dic setObject:self.descricao != nil ? self.descricao : [NSNull null] forKey:@"descricao"];
    [dic setObject:self.timeStamp != nil ? timestamp : [NSNull null] forKey:@"timeStamp"];
    [dic setObject:self.sectionIdentifier != nil ? self.sectionIdentifier : [NSNull null] forKey:@"sectionIdentifier"];
    [dic setObject:self.filename != nil ? self.filename : [NSNull null] forKey:@"filename"];
    [dic setObject:self.extensao != nil ? self.extensao : [NSNull null] forKey:@"extensao"];
    [dic setObject:[Utils deviceID] forKey:@"deviceToken"];
    
    // version
    if (!self.version) {
        self.version = [NSDate date];
    }
    [dic setObject:[DateUtils stringTimestampFromDate:self.version] forKey:@"version"];
    
    // firebaseID
    [dic setObject:self.createFirebaseID forKey:@"firebaseID"];
    
    
    return dic;
}


#pragma mark - Convert Firebase Dictionary

- (void)convertFirebaseDictionary:(NSDictionary *)dictNota withMoc:(NSManagedObjectContext *)moc
{
    self.codigo = dictNota[@"codigo"];
    self.descricao = dictNota[@"descricao"];
    self.timeStamp = [DateUtils dateFromStringTimestamp:dictNota[@"timeStamp"]];
    self.extensao = dictNota[@"extensao"];
    self.filename = dictNota[@"filename"];
    self.firebaseID = dictNota[@"firebaseID"];
    
    self.isSavedInFirebase = [NSNumber numberWithBool:YES];
    
    self.version = [DateUtils dateFromStringTimestamp:dictNota[@"version"]];
    
    // file
    if (self.filename) {
        [self downloadFromS3];
    }
    
}


#pragma mark - Delete Firebase

- (void)deleteFirebase
{
    [[Utils firebaseUtils] deleteObjectWithID:self.firebaseID andClassName:@"Nota"];
    
    // amazon s3
    [self deleteFromS3];
}


#pragma mark - Save Firebase

- (void)saveFirebase
{
    self.isSavedInFirebase = [NSNumber numberWithBool:NO];
    
    // If not TOP don't save on Firebase.
    if (![[Utils iapHelper] checkPlanTopFromController:nil withMessage:nil]) {
        [self.managedObjectContext MR_saveToPersistentStoreAndWait];
        return;
    }
    
    [[Utils firebaseUtils] saveObjectWithDictionary:self.dictionaryFirebase andClassName:@"Nota" andCompletionBlock:^(NSError *error) {
        [self.managedObjectContext performBlock:^{
            if (!error) {
                self.isSavedInFirebase = [NSNumber numberWithBool:YES];
            }
            [self.managedObjectContext MR_saveToPersistentStoreAndWait];
        }];
    }];
}

+ (void)saveAllPendingFirebase
{
    NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSavedInFirebase = NO OR isSavedInFirebase = nil"];
    
    NSArray *notas = [Nota MR_findAllWithPredicate:predicate inContext:moc];
    if (notas.count == 0) {
        return;
    }
    
    NSMutableDictionary *notasDic = [NSMutableDictionary new];
    
    for (Nota *nota in notas) {
        [notasDic setValue:nota.dictionaryFirebase forKey:nota.firebaseID];
        
        if (nota.arquivo != nil && nota.filename == nil) {
            nota.filename = [NSString stringWithFormat:@"%@-%@",[DateUtils stringTimestampFromDate:[NSDate date]], nota.codigo];
        }
    }
    
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    
    [moc MR_saveToPersistentStoreAndWait];
    
    Firebase *notaRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Nota"];
    [notaRef updateChildValues:notasDic withCompletionBlock:^(NSError *error, Firebase *ref) {
        
        [moc performBlock:^{
            if (!error) {
                for (Nota *nota in notas) {
                    nota.isSavedInFirebase = [NSNumber numberWithBool:YES];
                }
                [moc MR_saveToPersistentStoreAndWait];
            }
            
            [Utils postEntitytNotificationWithValue:@"UpdateSyncCounter" value:notas.count];
            dispatch_semaphore_signal(sema);
        }];
    }];
    
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
}


#pragma mark - Convert Notas In Batch

+ (void)convertNotasInBatchWithDictionary:(NSDictionary *)notasDict
{
    if (!notasDict || [notasDict isEqual:[NSNull null]] || notasDict.count == 0) {
        return;
    }
    
    NSManagedObjectContext *context = [NSManagedObjectContext MR_contextForCurrentThread];
    
    [context performBlockAndWait:^{
        
        BOOL hasNew = NO;
        NSNumber *alreadyDownloadAll = [Utils valueFromDefaults:FIREBASE_ALREADY_DOWNLOAD_ALL];
        
        for (NSString *firebaseID in notasDict.allKeys) {
            
            NSDictionary *oneNota = [notasDict objectForKey:firebaseID];
            
            // Check if is not the first download and the change was made by the current device.
            if (alreadyDownloadAll && [oneNota[@"deviceToken"] isEqualToString:[Utils deviceID]]) {
                continue;
            }
            
            Nota *nota = [Nota MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", firebaseID]
                                               inContext:context];
            
            if (!nota) {
                
                // Necessary because of the migration from Parse where users have more than one device.
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"timeStamp = %@ AND descricao = %@", [DateUtils dateFromStringTimestamp:oneNota[@"timeStamp"]], oneNota[@"descricao"]];
                nota = [Nota MR_findFirstWithPredicate:predicate inContext:context];
                
                if (!nota) {
                    nota = [Nota MR_createInContext:context];
                }
                
            } else {
                NSString *objVersion = oneNota[@"version"];
                NSString *lastVersion = [Utils valueFromDefaults:FIREBASE_NOTA_INSERT_VERSION];
                
                if ([objVersion compare:lastVersion] == NSOrderedAscending || [objVersion compare:lastVersion] == NSOrderedSame) {
                    continue;
                }
            }
            
            hasNew = YES;
            
            [nota convertFirebaseDictionary:oneNota withMoc:context];
        }
        
        if (hasNew) {
            [context MR_saveToPersistentStoreAndWait];
            
            NSArray *notasArray = [Nota MR_findAllSortedBy:@"version" ascending:YES inContext:context];
            Nota *lastNota = notasArray.lastObject;
            
            [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:REFRESH_NOTA];
            [Utils addObjectToDefaults:[DateUtils stringTimestampFromDate:lastNota.version] withKey:FIREBASE_NOTA_INSERT_VERSION];
        }
    }];
}


#pragma mark - Observers

+ (void)startFirebaseObservers
{
    [Nota startObserverAddedAndChangeNota];
}


+ (void)startObserverAddedAndChangeNota
{
    Firebase *notaRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Nota"];
    
    NSString *versionString = [Utils valueFromDefaults:FIREBASE_NOTA_INSERT_VERSION];
    
    if (!versionString) {
        versionString = [DateUtils stringTimestampFromDate:[NSDate dateWithTimeIntervalSince1970:1]];
    }
    
    // Add 1 millisecond because the firebase query is always >=
    versionString = [NSString stringWithFormat:@"%lld", [versionString longLongValue]+1];
    
    [[[notaRef queryOrderedByChild:@"version"] queryStartingAtValue:versionString]
     observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
         
         if ([snapshot.value isEqual:[NSNull null]]) {
             return;
         }
         
         dispatch_async([Utils firebaseUtils].queueDownload, ^{
             
             NSDictionary *notasDict = snapshot.value;
             
             [Nota convertNotasInBatchWithDictionary:notasDict];
         });
     }];
}


#pragma mark - Amazon S3

- (void)uploadToS3Synchronous
{
    self.pendingUpload = [NSNumber numberWithBool:YES];
    [self.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    if(![Utils isConnected]) {
        return;
    }
    
    // If not TOP don't save on Firebase.
    if (![[Utils iapHelper] checkPlanTopFromController:nil withMessage:nil]) {
        return;
    }
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    // write to file
    if (!self.filename || [self.filename isEqualToString:@""]) {
        self.filename = [NSString stringWithFormat:@"%@-%@", [DateUtils stringTimestampFromDate:[NSDate date]], self.codigo];
    }
    
    NSString *filename = [NSString stringWithFormat:@"%@.%@",self.filename, self.extensao];
    NSString *path = [NSString stringWithFormat:@"%@/notas/%@", NSTemporaryDirectory(), filename];
    [[NSFileManager defaultManager] createDirectoryAtPath:[path stringByDeletingLastPathComponent] withIntermediateDirectories:YES attributes:nil error:nil];
    
    if ([self.arquivo writeToFile:path atomically:YES]) {
        
        NSURL *fileURL = [NSURL fileURLWithPath:path];
        
        NSString *userID = [Utils valueFromDefaults:FIREBASE_USER_ID];
        NSString *fileName = [fileURL lastPathComponent];
        NSString *filePath = [NSString stringWithFormat:@"%@/notas/%@",userID,fileName];
        
        ASIS3ObjectRequest *request =
        [ASIS3ObjectRequest PUTRequestForFile:fileURL.path withBucket:S3_BUCKET_NAME key:filePath];
        
        [request setTimeOutSeconds:60];
        [request startSynchronous];
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        if (request.error) {
            NSLog(@"%@", request.error);
        } else {
            
            [self.managedObjectContext performBlockAndWait:^{
                self.pendingUpload = [NSNumber numberWithBool:NO];
                [self.managedObjectContext MR_saveToPersistentStoreAndWait];
            }];
        }
    }
}


- (void)uploadToS3
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self uploadToS3Synchronous];
    });
}


- (void)deleteFromS3
{
    if ([self.pendingUpload boolValue]) {
        return;
    }
    
    NSString *email = [Utils valueFromDefaults:FIREBASE_USER_ID];
    
    NSString *filename = [NSString stringWithFormat:@"%@.%@",self.filename, self.extensao];
    NSString *filePath = [NSString stringWithFormat:@"%@/notas/%@",email,filename];
    
    [S3Utils delete:filePath];
    
}

- (void)downloadFromS3
{
    
    if (self.arquivo != nil) {
        self.pendingUpload = [NSNumber numberWithBool:NO];
        return;
    }
    
    @autoreleasepool {
        
        self.pendingDownload = [NSNumber numberWithBool:YES];
        NSString *userID = [Utils valueFromDefaults:FIREBASE_USER_ID];
        NSString *filePath = [NSString stringWithFormat:@"%@/notas/%@.%@",userID,self.filename,self.extensao];
        self.arquivo = [S3Utils download:filePath];
        
        if (self.arquivo != nil) {
            self.pendingUpload = [NSNumber numberWithBool:NO];
            self.pendingDownload = [NSNumber numberWithBool:NO];
        }
        
    }
}


#pragma mark - Send All Pending Notas to Amazon

+ (void)sendAllPendingNotasToAmazon
{
    NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
    
    [moc performBlockAndWait:^{
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"arquivo != nil AND (pendingUpload = YES OR pendingUpload = nil)"];
        
        NSArray *notas = [Nota MR_findAllWithPredicate:predicate inContext:moc];
        
        __block BOOL hasError = NO;
        
        for (Nota *nota in notas) {
            
            [nota uploadToS3Synchronous];
            [moc MR_saveToPersistentStoreAndWait];
            
        }
        
        if (hasError) {
            [Utils addObjectToDefaults:@"Ocorreu erro no envio dos arquivos de algumas notas."
                               withKey:SYNC_ERROR_MESSAGE_NOTA];
            
        } else {
            [Utils removeObjectFromDefaults:SYNC_ERROR_MESSAGE_NOTA];
        }
    }];
}


@end
