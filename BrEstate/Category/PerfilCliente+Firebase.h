//
//  PerfilCliente+Firebase.h
//  Appci
//
//  Created by Leonardo Barros on 12/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "PerfilCliente.h"

@interface PerfilCliente (Firebase)

- (NSString *)createFirebaseID;
- (void)saveFirebase;
- (void)deleteFirebase;
- (NSDictionary *)getRelationshipFirebase;

+ (void)saveAllPendingFirebase;
+ (void)startFirebaseObservers;
+ (void)convertPerfisInBatchWithDictionary:(NSDictionary *)perfisDict;

@end
