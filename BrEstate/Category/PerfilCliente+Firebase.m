//
//  PerfilCliente+Firebase.m
//  Appci
//
//  Created by Leonardo Barros on 12/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "PerfilCliente+Firebase.h"
#import "Bairro+Firebase.h"
#import "Cliente+Firebase.h"

@implementation PerfilCliente (Firebase)


#pragma mark - Create Firebase ID

- (NSString *)createFirebaseID
{
    if (self.firebaseID) {
        return self.firebaseID;
    }
    
    Firebase *classRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"PerfilCliente"];
    
    Firebase *objectRef = [classRef childByAutoId];
    
    self.firebaseID = objectRef.key;
    
    [self.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    return objectRef.key;
}


#pragma mark - Get Relationship Firebase

- (NSDictionary *)getRelationshipFirebase
{
    return @{@"firebaseID": self.createFirebaseID,
             @"codigo": self.codigo};
}


#pragma mark - Dictionary Firebase

- (NSDictionary *)dictionaryFirebase
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    [dic setObject:self.codigo != nil ? self.codigo : [NSNull null] forKey:@"codigo"];
    [dic setObject:self.descricao != nil ? self.descricao : [NSNull null] forKey:@"descricao"];
    [dic setObject:self.quartos != nil ? self.quartos : [NSNull null] forKey:@"quartos"];
    [dic setObject:self.area != nil ? self.area : [NSNull null] forKey:@"area"];
    [dic setObject:self.cidade != nil ? self.cidade : [NSNull null] forKey:@"cidade"];
    [dic setObject:self.estado != nil ? self.estado : [NSNull null] forKey:@"estado"];
    [dic setObject:self.tipoImovel != nil ? self.tipoImovel : [NSNull null] forKey:@"tipoImovel"];
    [dic setObject:self.tipoPerfil != nil ? self.tipoPerfil : [NSNull null] forKey:@"tipoPerfil"];
    [dic setObject:self.valor != nil ? self.valor : [NSNull null] forKey:@"valor"];
    [dic setObject:[Utils deviceID] forKey:@"deviceToken"];
    
    // version
    if (!self.version) {
        self.version = [NSDate date];
    }
    [dic setObject:[DateUtils stringTimestampFromDate:self.version] forKey:@"version"];
    
    // firebaseID
    [dic setObject:self.createFirebaseID forKey:@"firebaseID"];
    
    // bairros
    if (self.bairro && self.bairro.count > 0) {
        NSMutableArray *bairros = [NSMutableArray array];
        
        for (Bairro *bairro in self.bairro) {
            [bairros addObject:bairro.getRelationshipFirebase];
        }
        
        [dic setObject:bairros forKey:@"bairros"];
        
    } else {
        [dic setObject:[NSNull null] forKey:@"bairros"];
    }
    
    return dic;
}


#pragma mark - Convert Firebase Dictionary

- (void)convertFirebaseDictionary:(NSDictionary *)dictPerfil withMoc:(NSManagedObjectContext *)moc
{
    self.codigo = dictPerfil[@"codigo"];
    self.descricao = dictPerfil[@"descricao"];
    self.quartos = dictPerfil[@"quartos"];
    self.area = dictPerfil[@"area"];
    self.estado = dictPerfil[@"estado"];
    self.cidade = dictPerfil[@"cidade"];
    self.tipoImovel = dictPerfil[@"tipoImovel"];
    self.tipoPerfil = dictPerfil[@"tipoPerfil"];
    self.valor = dictPerfil[@"valor"];
    
    self.firebaseID = dictPerfil[@"firebaseID"];
    
    self.isSavedInFirebase = [NSNumber numberWithBool:YES];
    
    self.version = [DateUtils dateFromStringTimestamp:dictPerfil[@"version"]];
    
    // bairros
    if (dictPerfil[@"bairros"]) {
        NSArray *bairros = dictPerfil[@"bairros"];
        
        for (NSDictionary *bairroDict in bairros) {
            Bairro *bairro = [Bairro MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", bairroDict[@"firebaseID"]]
                                                     inContext:moc];
            
            if (!bairro) {
                bairro = [Bairro MR_createInContext:moc];
                bairro.firebaseID = bairroDict[@"firebaseID"];
                bairro.codigo = bairroDict[@"codigo"];
                bairro.nome = @"Baixando informações";
                bairro.isSavedInFirebase = [NSNumber numberWithBool:YES];
            }
            [self addBairroObject:bairro];
        }
    }
}


#pragma mark - Delete Firebase

- (void)deleteFirebase
{
    [[Utils firebaseUtils] deleteObjectWithID:self.firebaseID andClassName:@"PerfilCliente"];
}


#pragma mark - Save Firebase

- (void)saveFirebase
{
    self.isSavedInFirebase = [NSNumber numberWithBool:NO];
    
    // If not TOP don't save on Firebase.
    if (![[Utils iapHelper] checkPlanTopFromController:nil withMessage:nil]) {
        [self.managedObjectContext MR_saveToPersistentStoreAndWait];
        return;
    }
    
    [[Utils firebaseUtils] saveObjectWithDictionary:self.dictionaryFirebase andClassName:@"PerfilCliente" andCompletionBlock:^(NSError *error) {
        [self.managedObjectContext performBlock:^{
            if (!error) {
                self.isSavedInFirebase = [NSNumber numberWithBool:YES];
            }
            [self.managedObjectContext MR_saveToPersistentStoreAndWait];
        }];
    }];
}


+ (void)saveAllPendingFirebase
{
    NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSavedInFirebase = NO OR isSavedInFirebase = nil"];
    
    NSArray *perfis = [PerfilCliente MR_findAllWithPredicate:predicate inContext:moc];
    if (perfis.count == 0) {
        return;
    }
    
    NSMutableDictionary *perfisDic = [NSMutableDictionary new];
    
    for (PerfilCliente *perfil in perfis) {
        [perfisDic setValue:perfil.dictionaryFirebase forKey:perfil.firebaseID];
    }
    
    dispatch_semaphore_t semaPerfil = dispatch_semaphore_create(0);
    
    [moc MR_saveToPersistentStoreAndWait];
    
    Firebase *perfilRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"PerfilCliente"];
    [perfilRef updateChildValues:perfisDic withCompletionBlock:^(NSError *error, Firebase *ref) {
        
        [moc performBlock:^{
            if (!error) {
                for (PerfilCliente *perfil in perfis) {
                    perfil.isSavedInFirebase = [NSNumber numberWithBool:YES];
                }
                [moc MR_saveToPersistentStoreAndWait];
            }
            
            [Utils postEntitytNotificationWithValue:@"UpdateSyncCounter" value:perfis.count];            
            dispatch_semaphore_signal(semaPerfil);
        }];
    }];
    
    dispatch_semaphore_wait(semaPerfil, DISPATCH_TIME_FOREVER);
}


#pragma mark - Convert Perfis In Batch

+ (void)convertPerfisInBatchWithDictionary:(NSDictionary *)perfisDict
{
    if (!perfisDict || [perfisDict isEqual:[NSNull null]] || perfisDict.count == 0) {
        return;
    }
    
    NSManagedObjectContext *context = [NSManagedObjectContext MR_contextForCurrentThread];
    
    [context performBlockAndWait:^{
        
        BOOL hasNew = NO;
        NSNumber *alreadyDownloadAll = [Utils valueFromDefaults:FIREBASE_ALREADY_DOWNLOAD_ALL];
        
        for (NSString *firebaseID in perfisDict.allKeys) {
            
            NSDictionary *onePerfil = [perfisDict objectForKey:firebaseID];
            
            // Check if is not the first download and the change was made by the current device.
            if (alreadyDownloadAll && [onePerfil[@"deviceToken"] isEqualToString:[Utils deviceID]]) {
                continue;
            }
            
            PerfilCliente *perfil = [PerfilCliente MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", firebaseID]
                                                                   inContext:context];
            
            if (!perfil) {
                
                // Necessary because of the migration from Parse where users have more than one device.
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"descricao = %@", onePerfil[@"descricao"]];
                perfil = [PerfilCliente MR_findFirstWithPredicate:predicate inContext:context];
                
                if (!perfil) {
                    perfil = [PerfilCliente MR_createInContext:context];
                }
                
            } else {
                NSString *objVersion = onePerfil[@"version"];
                NSString *lastVersion = [Utils valueFromDefaults:FIREBASE_PERFIL_INSERT_VERSION];
                
                if ([objVersion compare:lastVersion] == NSOrderedAscending || [objVersion compare:lastVersion] == NSOrderedSame) {
                    continue;
                }
            }
            
            hasNew = YES;
            
            [perfil convertFirebaseDictionary:onePerfil withMoc:context];
        }
        
        if (hasNew) {
            [context MR_saveToPersistentStoreAndWait];
            
            NSArray *perfisArray = [PerfilCliente MR_findAllSortedBy:@"version" ascending:YES inContext:context];
            PerfilCliente *lastPerfil = perfisArray.lastObject;
            
            [Utils addObjectToDefaults:[DateUtils stringTimestampFromDate:lastPerfil.version] withKey:FIREBASE_PERFIL_INSERT_VERSION];
        }
    }];
}


#pragma mark - Observers

+ (void)startFirebaseObservers
{
    [PerfilCliente startObserverAddedAndChangePerfil];
}


+ (void)startObserverAddedAndChangePerfil
{
    Firebase *perfilRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"PerfilCliente"];
    
    NSString *versionString = [Utils valueFromDefaults:FIREBASE_PERFIL_INSERT_VERSION];
    
    if (!versionString) {
        versionString = [DateUtils stringTimestampFromDate:[NSDate dateWithTimeIntervalSince1970:1]];
    }
    
    // Add 1 millisecond because the firebase query is always >=
    versionString = [NSString stringWithFormat:@"%lld", [versionString longLongValue]+1];
    
    [[[perfilRef queryOrderedByChild:@"version"] queryStartingAtValue:versionString]
     observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
         
         if ([snapshot.value isEqual:[NSNull null]]) {
             return;
         }
         
         dispatch_async([Utils firebaseUtils].queueDownload, ^{
             
             NSDictionary *perfisDict = snapshot.value;
             
             [PerfilCliente convertPerfisInBatchWithDictionary:perfisDict];
         });
     }];
}


@end
