//
//  Sms+Firebase.h
//  Appci
//
//  Created by Leonardo Barros on 13/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "Sms.h"

@interface Sms (Firebase)

- (NSString *)createFirebaseID;
- (NSDictionary *)dictionaryFirebase;
- (void)convertFirebaseDictionary:(NSDictionary *)dictSms withMoc:(NSManagedObjectContext *)moc;

@end
