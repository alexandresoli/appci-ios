//
//  Sms+Firebase.m
//  Appci
//
//  Created by Leonardo Barros on 13/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "Sms+Firebase.h"
#import "Evento+Firebase.h"
#import "Cliente+Firebase.h"

@implementation Sms (Firebase)


#pragma mark - Create Firebase ID

- (NSString *)createFirebaseID
{
    if (self.firebaseID) {
        return self.firebaseID;
    }
    
    NSString *path = [NSString stringWithFormat:@"Evento/%@/sms", self.evento.createFirebaseID];
    
    Firebase *classRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:path];
    
    Firebase *objectRef = [classRef childByAutoId];
    
    self.firebaseID = objectRef.key;
    
    [self.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    return objectRef.key;
}


#pragma mark - Dictionary Firebase

- (NSDictionary *)dictionaryFirebase
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    NSString *dataEnvio = [DateUtils stringTimestampFromDate:self.dataEnvio];
    
    [dic setObject:self.codigoSMS != nil ? self.codigoSMS : [NSNull null] forKey:@"codigoSMS"];
    [dic setObject:self.dataEnvio != nil ? dataEnvio : [NSNull null] forKey:@"dataEnvio"];
    [dic setObject:self.respostaConfirmacao != nil ? self.respostaConfirmacao : [NSNull null] forKey:@"timeStamp"];
    //    [dic setObject:[Utils deviceID] forKey:@"deviceToken"];
    
    // version
    if (!self.version) {
        self.version = [NSDate date];
    }
    [dic setObject:[DateUtils stringTimestampFromDate:self.version] forKey:@"version"];
    
    // firebaseID
    [dic setObject:self.createFirebaseID forKey:@"firebaseID"];
    
    // cliente
    if (self.cliente) {
        [dic setObject:self.cliente.getRelationshipFirebase forKey:@"cliente"];
    } else {
        [dic setObject:[NSNull null] forKey:@"cliente"];
    }
    
    return dic;
}


#pragma mark - Convert Firebase Dictionary

- (void)convertFirebaseDictionary:(NSDictionary *)dictSms withMoc:(NSManagedObjectContext *)moc
{
    self.codigoSMS = dictSms[@"codigoSMS"];
    self.dataEnvio = [DateUtils dateFromStringTimestamp:dictSms[@"dataEnvio"]];
    self.respostaConfirmacao = dictSms[@"respostaConfirmacao"];
    self.firebaseID = dictSms[@"firebaseID"];
    
    self.isSavedInFirebase = [NSNumber numberWithBool:YES];
    
    self.version = [DateUtils dateFromStringTimestamp:dictSms[@"version"]];
    
    // cliente
    if (dictSms[@"cliente"]) {
        NSDictionary *clienteDict = dictSms[@"cliente"];
        
        Cliente *cliente = [Cliente MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", clienteDict[@"firebaseID"]]
                                                    inContext:moc];
        
        if (!cliente) {
            cliente = [Cliente MR_createInContext:moc];
            cliente.firebaseID = clienteDict[@"firebaseID"];
            cliente.codigo = clienteDict[@"codigo"];
            cliente.isSavedInFirebase = [NSNumber numberWithBool:YES];
            cliente.nome = @"Baixando informações";
        }
        self.cliente = cliente;
    }
}


@end
