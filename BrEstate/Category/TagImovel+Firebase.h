//
//  TagImovel+Firebase.h
//  Appci
//
//  Created by Leonardo Barros on 23/12/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "TagImovel.h"

@interface TagImovel (Firebase)

- (NSDictionary *)dictionaryFirebase;

@end
