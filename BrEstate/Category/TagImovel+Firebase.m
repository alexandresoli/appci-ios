//
//  TagImovel+Firebase.m
//  Appci
//
//  Created by Leonardo Barros on 23/12/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "TagImovel+Firebase.h"

@implementation TagImovel (Firebase)


#pragma mark - Dictionary Firebase

- (NSDictionary *)dictionaryFirebase
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    [dic setObject:self.codigo != nil ? self.codigo : [NSNull null] forKey:@"codigo"];
    [dic setObject:self.armarioCozinha != nil ? self.armarioCozinha : [NSNull null] forKey:@"armarioCozinha"];
    [dic setObject:self.armarioEmbutido != nil ? self.armarioEmbutido : [NSNull null] forKey:@"armarioEmbutido"];
    [dic setObject:self.childrenCare != nil ? self.childrenCare : [NSNull null] forKey:@"childrenCare"];
    [dic setObject:self.churrasqueira != nil ? self.churrasqueira : [NSNull null] forKey:@"churrasqueira"];
    [dic setObject:self.garagem != nil ? self.garagem : [NSNull null] forKey:@"garagem"];
    [dic setObject:self.piscina != nil ? self.piscina : [NSNull null] forKey:@"piscina"];
    [dic setObject:self.playground != nil ? self.playground : [NSNull null] forKey:@"playground"];
    [dic setObject:self.quadraPoliesportiva != nil ? self.quadraPoliesportiva : [NSNull null] forKey:@"quadraPoliesportiva"];
    [dic setObject:self.salaGinastica != nil ? self.salaGinastica : [NSNull null] forKey:@"salaGinastica"];
    [dic setObject:self.salaoFestas != nil ? self.salaoFestas : [NSNull null] forKey:@"salaoFestas"];
    [dic setObject:self.salaoJogos != nil ? self.salaoJogos : [NSNull null] forKey:@"salaoJogos"];
    [dic setObject:self.sauna != nil ? self.sauna : [NSNull null] forKey:@"sauna"];
    [dic setObject:self.varanda != nil ? self.varanda : [NSNull null] forKey:@"varanda"];
    [dic setObject:self.wcEmpregada != nil ? self.wcEmpregada : [NSNull null] forKey:@"wcEmpregada"];
    [dic setObject:[Utils deviceID] forKey:@"deviceToken"];
    
    return dic;
}


@end
