//
//  UIImage+Custom.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/3/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Custom)

- (UIImage *)drawWatermarkText:(NSString*)text;
- (UIImage *)drawText:(NSString*)text inImage:(UIImage*)image atPoint:(CGPoint)point;
- (UIImage *)normalizedImage;
+ (UIImage *)squareImageWithColor:(UIColor *)color dimension:(int)dimension;
@end
