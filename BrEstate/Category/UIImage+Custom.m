//
//  UIImage+Custom.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/3/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "UIImage+Custom.h"

@implementation UIImage (Custom)

- (UIImage *)drawText:(NSString*)text inImage:(UIImage*)image atPoint:(CGPoint)point
{
    
    // usage
    //    UIImage *img = [self drawText:@"Some text"
    //                                inImage:img
    //                                atPoint:CGPointMake(0, 0)];
//    
//    UIFont *font = [UIFont boldSystemFontOfSize:12];
//    UIGraphicsBeginImageContext(image.size);
//    [image drawInRect:CGRectMake(0,0,image.size.width,image.size.height)];
//    CGRect rect = CGRectMake(point.x, point.y, image.size.width, image.size.height);
//    
//    // paint a rectangle of solid color behind the text
//    [[UIColor brownColor] set];
//    CGContextFillRect(UIGraphicsGetCurrentContext(),
//                      CGRectMake(0, (image.size.height-[text sizeWithFont:font].height),
//                                 image.size.width, image.size.height));
//    
//    // end of rectangle paint
//    
//    [[UIColor whiteColor] set];
//    [text drawInRect:CGRectIntegral(rect) withFont:font];
//    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
//    return newImage;
    return nil;
}

- (UIImage *)drawWatermarkText:(NSString*)text
{
    
    
    // usage
    
    // UIImage *image = [UIImage imageNamed:@"mona_lisa"];
    // image = [image drawWatermarkText:@"Leonardo da Vinci"];
    
    UIColor *textColor = [UIColor colorWithWhite:0.5 alpha:1.0];
    UIFont *font = [UIFont systemFontOfSize:50];
    CGFloat paddingX = 20.f;
    CGFloat paddingY = 20.f;
    
    // Compute rect to the text draw inside
    CGSize imageSize = self.size;
    NSDictionary *attr = @{NSForegroundColorAttributeName: textColor, NSFontAttributeName: font};
    CGSize textSize = [text sizeWithAttributes:attr];
    CGRect textRect = CGRectMake(imageSize.width - textSize.width - paddingX, imageSize.height - textSize.height - paddingY, textSize.width, textSize.height);
    
    // Create the image
    UIGraphicsBeginImageContext(imageSize);
    [self drawInRect:CGRectMake(0, 0, imageSize.width, imageSize.height)];
    [text drawInRect:CGRectIntegral(textRect) withAttributes:attr];
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultImage;
}

- (UIImage *)normalizedImage
{
    if (self.imageOrientation == UIImageOrientationUp) return self;
    
    UIGraphicsBeginImageContextWithOptions(self.size, NO, self.scale);
    [self drawInRect:(CGRect){0, 0, self.size}];
    UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return normalizedImage;
}

+ (UIImage *)squareImageWithColor:(UIColor *)color dimension:(int)dimension {
    CGRect rect = CGRectMake(0, 0, dimension, dimension);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


@end
