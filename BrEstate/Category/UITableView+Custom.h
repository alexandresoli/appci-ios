//
//  UITableView+Custom.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/2/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (Custom)

- (void)reloadData:(BOOL)animated;

@end
