//
//  UITableView+Custom.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/2/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "UITableView+Custom.h"

@implementation UITableView (Custom)

- (void)reloadData:(BOOL)animated
{
    
    if (animated) {
        
        [UIView transitionWithView:self
                          duration:0.5f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^(void) {
                            [self reloadData];
                        } completion:nil];
        
        return;
    }
    
    [self reloadData];
    
}

@end
