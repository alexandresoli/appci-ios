//
//  ClienteDeletado+Firebase.h
//  Appci
//
//  Created by Leonardo Barros on 12/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "ClienteDeletado.h"

@interface ClienteDeletado (Firebase)

- (void)saveFirebase;

+ (void)saveAllPendingFirebase;
+ (void)startFirebaseObservers;
+ (void)convertClientesDeletadosInBatchWithDictionary:(NSDictionary *)clientesDict;

@end
