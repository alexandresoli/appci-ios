//
//  ClienteDeletado+Firebase.m
//  Appci
//
//  Created by Leonardo Barros on 12/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "ClienteDeletado+Firebase.h"
#import "Cliente+Firebase.h"

@implementation ClienteDeletado (Firebase)


#pragma mark - Create Firebase ID

- (NSString *)createFirebaseID
{
    if (self.firebaseID) {
        return self.firebaseID;
    }
    
    Firebase *classRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"ClienteDeletado"];
    
    Firebase *objectRef = [classRef childByAutoId];
    
    self.firebaseID = objectRef.key;
    
    [self.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    return objectRef.key;
}


#pragma mark - Dictionary Firebase

- (NSDictionary *)dictionaryFirebase
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    [dic setObject:self.codigo != nil ? self.codigo : [NSNull null] forKey:@"codigo"];
    [dic setObject:self.email != nil ? self.email : [NSNull null] forKey:@"email"];
    [dic setObject:self.nome != nil ? self.nome : [NSNull null] forKey:@"nome"];
    [dic setObject:self.telefone != nil ? self.telefone : [NSNull null] forKey:@"telefone"];
    [dic setObject:self.telefoneExtra != nil ? self.telefoneExtra : [NSNull null] forKey:@"telefoneExtra"];
    [dic setObject:[Utils deviceID] forKey:@"deviceToken"];
    
    // version
    if (!self.version) {
        self.version = [NSDate date];
    }
    [dic setObject:[DateUtils stringTimestampFromDate:self.version] forKey:@"version"];
    
    // firebaseID
    [dic setObject:self.createFirebaseID forKey:@"firebaseID"];
    
    return dic;
}


#pragma mark - Convert Firebase Dictionary

- (void)convertFirebaseDictionary:(NSDictionary *)dictCliente withMoc:(NSManagedObjectContext *)moc
{
    self.codigo = dictCliente[@"codigo"];
    self.email = dictCliente[@"email"];
    self.nome = dictCliente[@"nome"];
    self.telefone = dictCliente[@"telefone"];
    self.telefoneExtra = dictCliente[@"telefoneExtra"];
    
    self.firebaseID = dictCliente[@"firebaseID"];
    
    self.isSavedInFirebase = [NSNumber numberWithBool:YES];
    
    self.version = [DateUtils dateFromStringTimestamp:dictCliente[@"version"]];
}


#pragma mark - Save Firebase

- (void)saveFirebase
{
    self.isSavedInFirebase = [NSNumber numberWithBool:NO];
    
    // If not TOP don't save on Firebase.
    if (![[Utils iapHelper] checkPlanTopFromController:nil withMessage:nil]) {
        [self.managedObjectContext MR_saveToPersistentStoreAndWait];
        return;
    }
    
    [[Utils firebaseUtils] saveObjectWithDictionary:self.dictionaryFirebase andClassName:@"ClienteDeletado" andCompletionBlock:^(NSError *error) {
        [self.managedObjectContext performBlock:^{
            if (!error) {
                self.isSavedInFirebase = [NSNumber numberWithBool:YES];
            }
            [self.managedObjectContext MR_saveToPersistentStoreAndWait];
        }];
    }];
}


+ (void)saveAllPendingFirebase
{
    NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSavedInFirebase = NO OR isSavedInFirebase = nil"];
    
    NSArray *clientes = [ClienteDeletado MR_findAllWithPredicate:predicate inContext:moc];
    if (clientes.count == 0) {
        return;
    }
    
    NSMutableDictionary *clientesDic = [NSMutableDictionary new];
    
    for (ClienteDeletado *cliente in clientes) {
        [clientesDic setValue:cliente.dictionaryFirebase forKey:cliente.firebaseID];
    }
    
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    
    [moc MR_saveToPersistentStoreAndWait];
    
    Firebase *clienteRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"ClienteDeletado"];
    [clienteRef updateChildValues:clientesDic withCompletionBlock:^(NSError *error, Firebase *ref) {
        
        [moc performBlock:^{
            if (!error) {
                for (ClienteDeletado *cliente in clientes) {
                    cliente.isSavedInFirebase = [NSNumber numberWithBool:YES];
                }
                [moc MR_saveToPersistentStoreAndWait];
            }
            
            [Utils postEntitytNotificationWithValue:@"UpdateSyncCounter" value:clientes.count];
            dispatch_semaphore_signal(sema);
        }];
    }];
    
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
}


#pragma mark - Convert Clientes Deletados In Batch

+ (void)convertClientesDeletadosInBatchWithDictionary:(NSDictionary *)clientesDict
{
    if (!clientesDict || [clientesDict isEqual:[NSNull null]] || clientesDict.count == 0) {
        return;
    }
    
    NSManagedObjectContext *context = [NSManagedObjectContext MR_contextForCurrentThread];
    
    [context performBlockAndWait:^{
        
        BOOL hasNew = NO;
        NSNumber *alreadyDownloadAll = [Utils valueFromDefaults:FIREBASE_ALREADY_DOWNLOAD_ALL];
        
        for (NSString *firebaseID in clientesDict.allKeys) {
            
            NSDictionary *oneCliente = [clientesDict objectForKey:firebaseID];
            
            // Check if is not the first download and the change was made by the current device.
            if (alreadyDownloadAll && [oneCliente[@"deviceToken"] isEqualToString:[Utils deviceID]]) {
                continue;
            }
            
            ClienteDeletado *cliente = [ClienteDeletado MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", firebaseID] inContext:context];
            
            if (!cliente) {
                
                // Necessary because of the migration from Parse where users have more than one device.
                NSPredicate *predicate = [Cliente uniquePredicateWithDictionary:oneCliente];
                cliente = [ClienteDeletado MR_findFirstWithPredicate:predicate inContext:context];
                
                if (!cliente) {
                    cliente = [ClienteDeletado MR_createInContext:context];
                }
                
            } else {
                NSString *objVersion = oneCliente[@"version"];
                NSString *lastVersion = [Utils valueFromDefaults:FIREBASE_CLIENTE_DELETADO_INSERT_VERSION];
                
                if ([objVersion compare:lastVersion] == NSOrderedAscending || [objVersion compare:lastVersion] == NSOrderedSame) {
                    continue;
                }
            }
            
            hasNew = YES;
            
            [cliente convertFirebaseDictionary:oneCliente withMoc:context];
        }
        
        if (hasNew) {
            [context MR_saveToPersistentStoreAndWait];
            
            NSArray *clientesArray = [ClienteDeletado MR_findAllSortedBy:@"version" ascending:YES inContext:context];
            ClienteDeletado *lastCliente = clientesArray.lastObject;
            
            [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:REFRESH_CLIENTE];
            [Utils addObjectToDefaults:[DateUtils stringTimestampFromDate:lastCliente.version] withKey:FIREBASE_CLIENTE_DELETADO_INSERT_VERSION];
        }
    }];
}


#pragma mark - Observers

+ (void)startFirebaseObservers
{
    [ClienteDeletado startObserverAddedAndChangeClienteDeletado];
}


+ (void)startObserverAddedAndChangeClienteDeletado
{
    Firebase *clienteRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"ClienteDeletado"];
    
    NSString *versionString = [Utils valueFromDefaults:FIREBASE_CLIENTE_DELETADO_INSERT_VERSION];
    
    if (!versionString) {
        versionString = [DateUtils stringTimestampFromDate:[NSDate dateWithTimeIntervalSince1970:1]];
    }
    
    // Add 1 millisecond because the firebase query is always >=
    versionString = [NSString stringWithFormat:@"%lld", [versionString longLongValue]+1];
    
    [[[clienteRef queryOrderedByChild:@"version"] queryStartingAtValue:versionString]
     observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
         
         if ([snapshot.value isEqual:[NSNull null]]) {
             return;
         }
         
         dispatch_async([Utils firebaseUtils].queueDownload, ^{
             
             NSDictionary *clientesDeletadoDict = snapshot.value;
             
             [ClienteDeletado convertClientesDeletadosInBatchWithDictionary:clientesDeletadoDict];
         });
     }];
}


@end
