
//  Config.h
//  BrEstate
//
//  Created by BrEstate LTDA on 03/10/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Config : NSObject

+ (Config *)instance;
- (NSString *)url;
- (NSString *)version;

@end
