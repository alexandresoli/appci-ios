

//  Config.m
//  BrEstate
//
//  Created by BrEstate LTDA on 03/10/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Config.h"
#import "AppDelegate.h"

@implementation Config

static Config *instance = nil;
static NSDictionary *keys;

+ (Config *)instance
{
    if (instance == nil) {
        
        instance =  [[Config alloc] init];
        NSString *path = [[NSBundle mainBundle] pathForResource:@"config" ofType:@"plist"];
        
        keys = [[NSDictionary alloc] initWithContentsOfFile:path];
    }
    
    
    return instance;
}

- (NSString *)url
{
    return  [keys valueForKey:@"URL"];
}

- (NSString *)version
{
    return  [keys valueForKey:@"version"];
}


@end
