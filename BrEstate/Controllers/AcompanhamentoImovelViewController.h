//
//  AcompanhamentoImovelViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 1/27/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "BaseViewController.h"

@class Imovel;
@interface AcompanhamentoImovelViewController : BaseViewController

@property (nonatomic, strong) Imovel *selectedImovel;
@end
