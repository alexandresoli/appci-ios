//
//  AcompanhamentoImovelViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 1/27/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "AcompanhamentoImovelViewController.h"

#import "ListaAcompanhamentoImovelViewController.h"

#import "Imovel.h"
#import "Cliente.h"
#import "StatusNegociacao.h"
#import "AcompanhamentoCell.h"
#import "Acompanhamento+Firebase.h"
#import "Negociacao+Firebase.h"
#import "Evento.h"

@interface AcompanhamentoImovelViewController () <NSFetchedResultsControllerDelegate,UITableViewDataSource,UITableViewDelegate,SWTableViewCellDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableViewClientes;
@property (nonatomic, weak) IBOutlet UILabel *tituloLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalPropostasLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalAnaliseLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalFechadasLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalCanceladasLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalVisitasLabel;
@property (nonatomic, weak) IBOutlet UIView *backBar;

@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@property (nonatomic, strong) Negociacao *selectedNegociacao;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) ListaAcompanhamentoImovelViewController *listaAcompanhamentoViewController;

@end


@implementation AcompanhamentoImovelViewController


#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //NSLog(@"children : %@", self.childViewControllers);
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        UINavigationController *navController = [self.childViewControllers objectAtIndex:0];
        self.ListaAcompanhamentoViewController = [[navController viewControllers] firstObject];
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self configureCounter:self.selectedImovel];
    
    [Utils trackWithName:@"Acompanhamento Imovel"];
    [Utils trackFlurryWithName:@"Acompanhamento Imovel"];
}


#pragma mark - Back
- (IBAction)back:(id)sende
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"imovel = %@ AND status = 'Proposta'",_selectedImovel];
    
    _fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Negociacao" andKey:@"codigo" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
    
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
    
}


#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AcompanhamentoCell";
    AcompanhamentoCell *cell = (AcompanhamentoCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
//    UIView *bgColorView = [[UIView alloc] init];
//    bgColorView.backgroundColor = [UIColor colorWithRed:253.0/255 green:190.0/255 blue:117.0/255 alpha:1];
//    cell.selectedBackgroundView = bgColorView;
    
    // SWTableViewCell
//    [cell setCellHeight:cell.frame.size.height];
//    cell.containingTableView = tableView;
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


- (void)configureCell:(AcompanhamentoCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    
    // Tratamento para marcar a cell selecionada.
    if ([self.selectedIndexPath isEqual:indexPath]) {
        cell.selecionada.hidden = NO;
    } else {
        cell.selecionada.hidden = YES;
    }
    
    Negociacao *negociacao = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.descricao.text = negociacao.cliente.nome;
    
    [self configureTitle:negociacao];
    
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    
    [self configureLeftSwipe:leftUtilityButtons negociacao:negociacao];
    
    cell.leftUtilityButtons = leftUtilityButtons;
    
    cell.delegate = self;
    
}

- (void)configureLeftSwipe:(NSMutableArray *)leftUtilityButtons negociacao:(Negociacao *)negociacao
{
    // left swipe
    if ([negociacao.status isEqualToString:NEGOCIACAO_PROPOSTA]) {
        
        [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"indicador-analise"]];
        [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"indicador-fechada"]];
        [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"indicador-cancelada"]];
        
    } else if ([negociacao.status isEqualToString:NEGOCIACAO_ANALISE]) {
        
        [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"indicador-fechada"]];
        [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"indicador-cancelada"]];
        
    }
}

- (void)configureTitle:(Negociacao *)negociacao
{
    if ([negociacao.status isEqualToString:NEGOCIACAO_PROPOSTA]) {
        self.tituloLabel.backgroundColor = [UIColor colorWithRed:78.0/255 green:219.0/255 blue:109.0/255 alpha:1.0];
        self.tituloLabel.text = @"   Proposta";
    } else if ([negociacao.status isEqualToString:NEGOCIACAO_ANALISE]) {
        self.tituloLabel.backgroundColor = [UIColor colorWithRed:252.0/255 green:150.0/255 blue:42.0/255 alpha:1.0];
        self.tituloLabel.text = @"   Análise";
    } else if ([negociacao.status isEqualToString:NEGOCIACAO_FECHADA]) {
        self.tituloLabel.backgroundColor = [UIColor colorWithRed:230.0/255 green:77.0/255 blue:64.0/255 alpha:1.0];
        self.tituloLabel.text = @"   Fechada";
    } else if ([negociacao.status isEqualToString:NEGOCIACAO_CANCELADA]) {
        self.tituloLabel.backgroundColor = [UIColor colorWithRed:155.0/255 green:89.0/255 blue:182.0/255 alpha:1.0];
        self.tituloLabel.text = @"   Cancelada";
    }
}


#pragma mark - UITableViewDelegate
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"Apagar";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Negociacao *negociacao = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    AcompanhamentoCell *cellSelected = (AcompanhamentoCell *) [tableView cellForRowAtIndexPath:indexPath];
    AcompanhamentoCell *cellOld = (AcompanhamentoCell *) [tableView cellForRowAtIndexPath:self.selectedIndexPath];
    
    cellSelected.selecionada.hidden = NO;
    
    if (cellSelected != cellOld) {
        cellOld.selecionada.hidden = YES;
    }
    
    self.selectedIndexPath = indexPath;
    
    [self.listaAcompanhamentoViewController changeNegociacao:negociacao];
    
    if (![[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.selectedNegociacao = negociacao;
        [self performSegueWithIdentifier:@"ListaAcompanhamentosSegue" sender:self];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


#pragma mark - SWTableViewCellDelegate
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index
{
    
    NSIndexPath *indexPath = [self.tableViewClientes indexPathForCell:cell];
    Negociacao *negociacao = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if ([negociacao.status isEqualToString:NEGOCIACAO_PROPOSTA]) {
        
        switch (index) {
            case 0:
                negociacao.status = NEGOCIACAO_ANALISE;
                break;
            case 1:
                if (![self canCloseNegotiation]) {
                    return;
                }
                negociacao.status = NEGOCIACAO_FECHADA;
                break;
            case 2:
                negociacao.status = NEGOCIACAO_CANCELADA;
                break;
            default:
                break;
        }
        
    } else if ([negociacao.status isEqualToString:NEGOCIACAO_ANALISE]) {
        
        switch (index) {
            case 0:
                if (![self canCloseNegotiation]) {
                    return;
                }
                negociacao.status = NEGOCIACAO_FECHADA;
                break;
            case 1:
                negociacao.status = NEGOCIACAO_CANCELADA;
                break;
            default:
                break;
        }
    }
    
    negociacao.version = [NSDate date];
    
    [self addAcompanhamento:negociacao];
    
    // Firebase
    [negociacao saveFirebase];
    
    [self saveMOC];
    
    // Parse SDK
//    [negociacao saveParse:self.moc];
    
    [self configureCounter:self.selectedImovel];
    [self configureTitle:negociacao];
    [self showCategory:negociacao];
    
    if ([negociacao.status isEqualToString:NEGOCIACAO_FECHADA]) {
        [Utils postEntitytNotification:@"ImovelFechado"];
    } else {
        [Utils postEntitytNotification:@"Imovel"];
    }
}

- (void)addAcompanhamento:(Negociacao *)negociacao
{
    Acompanhamento *acompanhamento = [Acompanhamento MR_createInContext:self.moc];
    
    acompanhamento.codigo = [acompanhamento getUniqueCode]; //[NSNumber numberWithLong:codigo];
    acompanhamento.timeStamp = [NSDate date];
    
    
    if ([negociacao.status isEqualToString:NEGOCIACAO_ANALISE]) {
        acompanhamento.titulo = [NSString stringWithFormat:@"Iniciada %@ da proposta.",negociacao.status];
        
    } else if ([negociacao.status isEqualToString:NEGOCIACAO_CANCELADA]) {
        acompanhamento.titulo = [NSString stringWithFormat:@"Proposta %@.",negociacao.status];
        
    } else if ([negociacao.status isEqualToString:NEGOCIACAO_FECHADA]) {
        acompanhamento.titulo = [NSString stringWithFormat:@"Negociação %@ com sucesso!",negociacao.status];
    }
    
    acompanhamento.cliente = negociacao.cliente;
    acompanhamento.imovel = negociacao.imovel;
    
    // Firebase
    [acompanhamento saveFirebase];
    
    // Parse SDK
//    [acompanhamento saveParse:self.moc];
    
}


#pragma mark - Check Can Close Negotiation

- (BOOL)canCloseNegotiation
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"imovel = %@ AND status = %@", self.selectedImovel, NEGOCIACAO_FECHADA];
    NSUInteger countFechadas = [Negociacao MR_countOfEntitiesWithPredicate:predicate inContext:self.moc];
    
    if (countFechadas > 0) {
        [Utils showMessage:@"Este imóvel já possui uma negociação fechada."];
        return NO;
    }
    
    return YES;
}


#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if (![[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        if ([segue.identifier isEqualToString:@"ListaAcompanhamentosSegue"]) {
            ListaAcompanhamentoImovelViewController *controller = (ListaAcompanhamentoImovelViewController *)segue.destinationViewController;
            controller.selectedNegociacao = self.selectedNegociacao;
        }
    } else {
        UINavigationController *navController = segue.destinationViewController;
        ListaAcompanhamentoImovelViewController *controller = [[navController viewControllers]firstObject];
        controller.backBar = self.backBar;
    }
        
}

- (void)configureCounter:(Imovel *)imovel
{
    // propostas
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"imovel = %@ AND status = 'Proposta'",imovel];
    
    long count = [Negociacao MR_countOfEntitiesWithPredicate:predicate inContext:self.moc];
    self.totalPropostasLabel.text = [NSString stringWithFormat:@"%ld",count];

    
    // analise
    predicate = [NSPredicate predicateWithFormat:@"imovel = %@ AND status = 'Análise'",imovel];
    count = [Negociacao MR_countOfEntitiesWithPredicate:predicate inContext:self.moc];
    self.totalAnaliseLabel.text = [NSString stringWithFormat:@"%ld",count];

    
    // fechadas
    predicate = [NSPredicate predicateWithFormat:@"imovel = %@ AND status = 'Fechada'",imovel];
    count = [Negociacao MR_countOfEntitiesWithPredicate:predicate inContext:self.moc];
    self.totalFechadasLabel.text = [NSString stringWithFormat:@"%ld",count];
    
    // canceladas
    predicate = [NSPredicate predicateWithFormat:@"imovel = %@ AND status = 'Cancelada'",imovel];
    count = [Negociacao MR_countOfEntitiesWithPredicate:predicate inContext:self.moc];
    self.totalCanceladasLabel.text = [NSString stringWithFormat:@"%ld",count];
    
    // total of visits
    predicate = [NSPredicate predicateWithFormat:@"imovel = %@ AND tipoEvento = 'Visita'",imovel];
    count = [Evento MR_countOfEntitiesWithPredicate:predicate inContext:self.moc];
    self.totalVisitasLabel.text = [NSString stringWithFormat:@"%ld",count];
}


- (void)showCategory:(Negociacao *)negociacao
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"imovel = %@ AND status = %@",negociacao.imovel,negociacao.status];
    
    _fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Negociacao" andKey:@"codigo" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
    
    self.selectedIndexPath = nil;
    [self.fetchedResultsController performFetch:nil];
    [self.tableViewClientes reloadData:YES];
    
    if ([negociacao.status isEqualToString:NEGOCIACAO_ANALISE]) {
        self.tituloLabel.backgroundColor = [UIColor colorWithRed:252.0/255 green:150.0/255 blue:42.0/255 alpha:1.0];
    } else if ([negociacao.status isEqualToString:NEGOCIACAO_CANCELADA]) {
        self.tituloLabel.backgroundColor = [UIColor colorWithRed:155.0/255 green:89.0/255 blue:182.0/255 alpha:1.0];
    } else if ([negociacao.status isEqualToString:NEGOCIACAO_FECHADA]) {
        self.tituloLabel.backgroundColor = [UIColor colorWithRed:230.0/255 green:77.0/255 blue:64.0/255 alpha:1.0];
    }
    
    self.tituloLabel.backgroundColor = [UIColor colorWithRed:78.0/255 green:219.0/255 blue:109.0/255 alpha:1.0];
    self.tituloLabel.text = [NSString stringWithFormat:@"   %@",negociacao.status];
    
    
    [self.listaAcompanhamentoViewController changeNegociacao:negociacao];
    
}
#pragma mark - IBActions
- (IBAction)showPropostas:(id)sender
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"imovel = %@ AND status = %@",_selectedImovel, NEGOCIACAO_PROPOSTA];
    
    _fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Negociacao" andKey:@"codigo" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
    
    self.selectedIndexPath = nil;
    [self.fetchedResultsController performFetch:nil];
    [self.tableViewClientes reloadData:YES];
    
    self.tituloLabel.backgroundColor = [UIColor colorWithRed:78.0/255 green:219.0/255 blue:109.0/255 alpha:1.0];
    self.tituloLabel.text = @"   Proposta";
    
    [self.listaAcompanhamentoViewController changeNegociacao:nil];
}

- (IBAction)showAnalise:(id)sender
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"imovel = %@ AND status = %@",_selectedImovel, NEGOCIACAO_ANALISE];
    
    _fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Negociacao" andKey:@"codigo" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
    
    self.selectedIndexPath = nil;
    [self.fetchedResultsController performFetch:nil];
    [self.tableViewClientes reloadData:YES];
    
    self.tituloLabel.backgroundColor = [UIColor colorWithRed:252.0/255 green:150.0/255 blue:42.0/255 alpha:1.0];
    self.tituloLabel.text = @"   Análise";
    
    [self.listaAcompanhamentoViewController changeNegociacao:nil];
}

- (IBAction)showFechadas:(id)sender
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"imovel = %@ AND status = %@",_selectedImovel, NEGOCIACAO_FECHADA];
    
    _fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Negociacao" andKey:@"codigo" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];

    self.selectedIndexPath = nil;
    [self.fetchedResultsController performFetch:nil];
    [self.tableViewClientes reloadData:YES];
    
    self.tituloLabel.backgroundColor = [UIColor colorWithRed:230.0/255 green:77.0/255 blue:64.0/255 alpha:1.0];
    self.tituloLabel.text = @"   Fechada";
    
    [self.listaAcompanhamentoViewController changeNegociacao:nil];
}

- (IBAction)showCanceladas:(id)sender
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"imovel = %@ AND status = %@",_selectedImovel, NEGOCIACAO_CANCELADA];
    
    _fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Negociacao" andKey:@"codigo" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
    
    self.selectedIndexPath = nil;
    [self.fetchedResultsController performFetch:nil];
    [self.tableViewClientes reloadData:YES];
    
    self.tituloLabel.backgroundColor = [UIColor colorWithRed:155.0/255 green:89.0/255 blue:182.0/255 alpha:1.0];
    self.tituloLabel.text = @"   Cancelada";
    
    [self.listaAcompanhamentoViewController changeNegociacao:nil];
    
}
@end
