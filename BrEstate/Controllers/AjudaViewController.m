//
//  AjudaViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 1/13/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "AjudaViewController.h"

@interface AjudaViewController ()

@property (nonatomic, weak) IBOutlet UIWebView *webView;

@end

@implementation AjudaViewController

@synthesize webView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"sample" ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [webView loadHTMLString:htmlString baseURL:nil];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [Utils trackWithName:@"Tela Ajuda"];
    [Utils trackFlurryWithName:@"Tela Ajuda"];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
