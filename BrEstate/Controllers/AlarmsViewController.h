//
//  AlarmsViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/25/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlarmsViewController : BaseTableViewController

@property (nonatomic, strong) NSMutableArray *selectedAlarmes;

@end
