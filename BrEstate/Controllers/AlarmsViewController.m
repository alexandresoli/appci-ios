//
//  AlarmsViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/25/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "AlarmsViewController.h"
#import "EditarEventoViewController.h"
#import "Alarme.h"

@interface AlarmsViewController () <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation AlarmsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (_selectedAlarmes == nil) {
        _selectedAlarmes = [NSMutableArray new];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Selecionar Alarme - Adicionar/Editar Evento (Agenda)"];
    [Utils trackFlurryWithName:@"Selecionar Alarme - Adicionar/Editar Evento (Agenda)"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AlarmeCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15];
    cell.selectionStyle = UITableViewCellEditingStyleNone;
    Alarme *alarme = [_fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.textLabel.text = alarme.descricao;
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    for (NSNumber *codigoAlarme in _selectedAlarmes) {
        if ([codigoAlarme isEqualToNumber:alarme.codigo]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            break;
        }
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Alarme *alarme = [_fetchedResultsController objectAtIndexPath:indexPath];
    
    if (!alarme) {
        return;
    }
    
    UITableViewCell *cell =  [tableView cellForRowAtIndexPath:indexPath];

    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        [_selectedAlarmes removeObject:alarme.codigo];
        cell.accessoryType = UITableViewCellAccessoryNone;
    } else {
        
        // only 2 allowed

        if (_selectedAlarmes.count < 2) {
            
            [_selectedAlarmes addObject:alarme.codigo];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            
        } else {
            [Utils showMessage:@"São permitidos apenas 2 alertas por evento."];
        }

    }
}


- (IBAction)save:(id)sender
{
    [self performSegueWithIdentifier:@"UnwindFromAlarme" sender:self];
}

#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    _fetchedResultsController.delegate = self;
    
    
    _fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Alarme" andKey:@"descricao" withPredicate:nil withOrder:NO withLimit:0 inContext:self.moc];
    
    
    return _fetchedResultsController;
}
@end
