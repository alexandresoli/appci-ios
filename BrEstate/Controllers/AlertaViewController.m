//
//  AlertViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 1/13/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "AlertaViewController.h"
#import "Alerta.h"
#import "AjudaViewController.h"

@interface AlertaViewController () <NSFetchedResultsControllerDelegate,UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@end

@implementation AlertaViewController

@synthesize fetchedResultsController;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"AlertaCell"];
 
    
//    self.tableView.estimatedRowHeight = 60;
    
    
    // configure the close button
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc]
                                     initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                     target:self
                                     action:@selector(close:)];
    self.navigationItem.leftBarButtonItem = closeButton;
    
//    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [Utils trackWithName:@"Tela Alerta"];
    [Utils trackFlurryWithName:@"Tela Alerta"];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (fetchedResultsController != nil) {
        return fetchedResultsController;
    }
    
    fetchedResultsController.delegate = self;
    
    fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Alerta" andKey:@"codigo" withPredicate:nil withOrder:YES withLimit:0 inContext:self.moc];
    
    
    return fetchedResultsController;
}


#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AlertaCell";

    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;;
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Alerta *alerta = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = alerta.descricao;
    cell.imageView.image = [UIImage imageNamed:@"dropdown-alert"];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Alerta *alerta = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if ([alerta.tipo isEqualToString:@"permissao-contatos"]) {

        [self performSegueWithIdentifier:@"AjudaSegue" sender:nil];
    
    }

}

#pragma mark - Close

- (IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
