//
//  AtivacaoDispositivoViewController.m
//  Appci
//
//  Created by BrEstate LTDA on 24/06/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "AtivacaoDispositivoViewController.h"
#import "SVProgressHUD.h"
#import "FirebaseUtilsUser.h"


@interface AtivacaoDispositivoViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UIButton *resendTokenButton;

@end


@implementation AtivacaoDispositivoViewController


#pragma mark - View Life Cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSString *text = [NSString stringWithFormat:@"Foi enviado um código de ativação para o email %@ \n\nInforme o código para ativar o dispositivo.", self.loginUser];
    
    self.titleLabel.text = text;
}


-(BOOL)shouldAutorotate
{
    return NO;
}


#pragma mark - Active Code

- (IBAction)activeCode:(id)sender
{
    if ([self formIsValid]) {
        
        [self.view endEditing:YES];
        
        [SVProgressHUD showWithStatus:@"Ativando dispositivo. Aguarde..." maskType:SVProgressHUDMaskTypeBlack];
        
        
        NSDictionary *params = @{@"email": self.loginUser,
                                 @"token": self.codeTextField.text};
        
        NSString *url = [NSString stringWithFormat:@"%@%@",[[Config instance] url], @"auth/validate-token.json"];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSNumber *success = [responseObject objectForKey:@"success"];
            
            if ([success boolValue]) {
                
                NSDictionary *device = [Utils valueFromDefaults:DEVICE_FOR_SUBSTITUTION];
                
                [[Utils firebaseUtils] deleteObjectWithID:device[@"deviceID"] andClassName:@"Device"];
                
                [FirebaseUtilsUser removeDeviceWithID:device[@"deviceID"] andCompletionBlock:^(NSError *error) {
                    
                    [Utils dismissProgress];
                    
                    if (error) {
                        NSLog(@"%@", error.localizedDescription);
                        
                        [Utils showMessage:@"Ocorreu um erro ao remover o dispositivo. Tente novamente. Caso o erro persista entre em contato com o suporte@brestate.com.br"];
                        
                        return;
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults removeObjectForKey: DEVICE_FOR_SUBSTITUTION];
                        [defaults synchronize];
                        
                        // Retorna para tela de login.
                        [self performSegueWithIdentifier:@"UnwindFromAtivacaoSegue" sender:self];
                    });
                }];
                
            } else {
                
                [Utils dismissProgress];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSString *errorMessage = [responseObject objectForKey:@"message"];
                    
                    if (!errorMessage) {
                        errorMessage = @"Ocorreu um erro ao realizar a substituição do dispositivo. Tente novamente. Caso o erro persista entre em contato com o suporte@brestate.com.br";
                    }
                    
                    [Utils showMessage:errorMessage];
                });
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [Utils dismissProgress];
                
                [Utils showMessage:[NSString stringWithFormat:@"Ocorreu um erro ao realizar a substituição do dispositivo. Tente novamente. Caso o erro persista entre em contato com o suporte@brestate.com.br \nErro - %@", error]];
            });
        }];
    }
}


#pragma mark - Form Validation

- (BOOL)formIsValid
{
    BOOL isValid = YES;
    
    if (self.codeTextField.text.length == 0) {
        [Utils addRedCorner:self.codeTextField];
        isValid = NO;
    }
    
    return isValid;
}


#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([[UIScreen mainScreen] bounds].size.height < 568) {
        [self animateTextField:textField up:YES];
    }
    
    [Utils removeRedCorner:self.codeTextField];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([[UIScreen mainScreen] bounds].size.height < 568) {
        [self animateTextField:textField up:NO];
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


- (void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -90; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


#pragma mark - Resend Token

- (IBAction)resendToken:(id)sender
{
    [Utils sendTokenToEmail:self.loginUser withCompletionBlock:^(BOOL success) {
        if (success) {
            self.resendTokenButton.enabled = NO;
            [Utils showMessage:@"Código de ativação enviado."];
        }
    }];
}


#pragma mark - Dismiss Keyboard

- (IBAction)dismissKeyboard:(id)sender
{
    [self.view endEditing:YES];
}


@end
