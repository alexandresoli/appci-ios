//
//  BairrosPerfilViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 12/7/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "BaseTableViewController.h"

@protocol BairroPerfilDelegate <NSObject>

@required
- (void)didSelectBairros:(NSArray *)bairros;

@end

@class Bairro;
@interface BairrosPerfilViewController : BaseTableViewController

@property (nonatomic, weak) NSString *selectedCidade;
@property (nonatomic, strong) NSMutableArray *selectedBairros;

@property (nonatomic, weak) id<BairroPerfilDelegate> delegate;
@end
