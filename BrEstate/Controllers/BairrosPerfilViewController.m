//
//  BairrosPerfilViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 12/7/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "BairrosPerfilViewController.h"
#import "Cidade.h"
#import "EditarBairroViewController.h"
#import "CustomCell.h"
#import "ImageUtils.h"
#import "Bairro+Firebase.h"

@interface BairrosPerfilViewController () <NSFetchedResultsControllerDelegate,SWTableViewCellDelegate,UIActionSheetDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, weak) Bairro *selectedBairro;

@end

@implementation BairrosPerfilViewController

@synthesize fetchedResultsController,selectedCidade,selectedBairros;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // add model changes notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDataModelChange:) name:NSManagedObjectContextObjectsDidChangeNotification object:self.moc];
    

    if (!selectedBairros) {
        selectedBairros = [NSMutableArray new];
    }
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Tela Bairros"];
    [Utils trackFlurryWithName:@"Tela Bairros"];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.delegate didSelectBairros:selectedBairros];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Notification Center
- (void)handleDataModelChange:(NSNotification *)note
{
    NSSet *updatedObjects = [[note userInfo] objectForKey:NSUpdatedObjectsKey];
    NSSet *deletedObjects = [[note userInfo] objectForKey:NSDeletedObjectsKey];
    NSSet *insertedObjects = [[note userInfo] objectForKey:NSInsertedObjectsKey];
    
    if ([updatedObjects containsMemberOfClass:[Bairro class]] ||
        [insertedObjects containsMemberOfClass:[Bairro class]] ||
        [deletedObjects containsMemberOfClass:[Bairro class]])
    {
        

        fetchedResultsController = nil;
        [self fetchedResultsController];
        
        [self.tableView reloadData];
    }
    
    
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"BairroCell";
    CustomCell *cell = (CustomCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15];
    
    // SWTableViewCell
//    [cell setCellHeight:cell.frame.size.height];
//    cell.containingTableView = tableView;
    
    [self configureCell:cell];
    
    Bairro *bairro = [fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.textLabel.text = bairro.nome;
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    if ([self.selectedBairros containsObject:bairro]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    return cell;
}

- (void)configureCell:(CustomCell *)cell
{
    
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        // left swipe
        [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"contato-editar"] scaledToSize:CGSizeMake(54, 70)]];
        
        // right swipe
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"swipe-apagar"] scaledToSize:CGSizeMake(54, 70)]];
        
    } else {
        // left swipe
        [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"contato-editar"] scaledToSize:CGSizeMake(42, 55)]];
        
        // right swipe
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"swipe-apagar"] scaledToSize:CGSizeMake(42, 55)]];
        
    }
    
    cell.leftUtilityButtons = leftUtilityButtons;
    cell.rightUtilityButtons = rightUtilityButtons;
    cell.delegate = self;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Bairro *bairro = [fetchedResultsController objectAtIndexPath:indexPath];
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        [selectedBairros removeObject:bairro];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
    } else {
        [selectedBairros addObject:bairro];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


#pragma mark - SWTableViewCellDelegate
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index
{
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    self.selectedBairro = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    switch (index) {
        case 0:
            [self performSegueWithIdentifier:@"EditarBairroSegue" sender:self];
            break;
        default:
            break;
    }
    
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    self.selectedBairro = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    UIActionSheet *sheet =[[UIActionSheet alloc] initWithTitle:@"Remover bairro?" delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:@"Remover" otherButtonTitles:nil, nil];
    [sheet showInView:cell];
    
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        
        [selectedBairros removeObject:self.selectedBairro];
        
//        [self.selectedBairro removeFromAllPerfis];
//        [ParseUtils deleteEntity:self.selectedBairro withName:@"Bairro" usingMoc:self.moc];
        
        // Firebase
        [self.selectedBairro deleteFirebase];
        
        [self.selectedBairro MR_deleteInContext:self.moc];
        
        [self.moc MR_saveToPersistentStoreAndWait];
        
        [self.fetchedResultsController performFetch:nil];
        
        [self.tableView reloadData:YES];
    }
}

#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (fetchedResultsController != nil) {
        return fetchedResultsController;
    }
    
    fetchedResultsController.delegate = self;
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cidade = %@",selectedCidade];
    fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Bairro" andKey:@"nome" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
    
    
    return fetchedResultsController;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"EditarBairroSegue"]) {
        EditarBairroViewController *controller = segue.destinationViewController;
        controller.selectedCidade = selectedCidade;
        controller.selectedBairro = self.selectedBairro;
        
    } else if ([segue.identifier isEqualToString:@"AdicionarBairroSegue"]) {
        EditarBairroViewController *controller = segue.destinationViewController;
        controller.selectedCidade = selectedCidade;
    }
}

@end
