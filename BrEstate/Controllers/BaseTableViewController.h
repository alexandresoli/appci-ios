//
//  BaseViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 12/5/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableViewController : UITableViewController

@property (nonatomic, strong) NSManagedObjectContext *moc;
@property (nonatomic, strong) NSNumberFormatter* formatter;

- (void)resignKeyboard;
- (NSString *)formatQuartos:(int)quantidade;
- (void)savePhotoToAlbum:(UIImage *)image;

- (IAPHelper *)iapHelper;
- (void)saveMOC;
@end
