//
//  BaseViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 12/5/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "BaseTableViewController.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"
#import "LoginViewController.h"


@interface BaseTableViewController () <UITextFieldDelegate>

@property (strong, atomic) ALAssetsLibrary *library;

@end

@implementation BaseTableViewController

@synthesize formatter,library;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.moc = [NSManagedObjectContext MR_defaultContext];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Validate Email
-(BOOL)validateEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


-(void)resignKeyboard
{
    [self.view endEditing:YES];
}


-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"Apagar";
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = @"";
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [Utils removeRedCorner:textField];
    return YES;
}


#pragma mark - Numver Formatter
- (NSNumberFormatter *)formatter
{
    if (formatter == nil) {
        formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [formatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [formatter setDecimalSeparator:@","];
        [formatter setGeneratesDecimalNumbers:YES];
        [formatter setCurrencySymbol:@"R$ "];
        [formatter setLenient:YES];
    }
    
    return formatter;
}

#pragma mark - Quartos Formatter
- (NSString *)formatQuartos:(int)quantidade
{
    NSString *message = @"";
    
    if (quantidade == 0) {
        message = @"indiferente";
    } else if (quantidade == 1) {
        message = [NSString stringWithFormat:@"%d quarto", quantidade];
    } else if (quantidade == 10) {
        message = [NSString stringWithFormat:@"%d quartos ou mais", quantidade];
    } else {
        message = [NSString stringWithFormat:@"%d quartos", quantidade];
    }
    
    return message;
}

- (void)savePhotoToAlbum:(UIImage *)image
{
    [self.library saveImage:image toAlbum:@"BrEstate" withCompletionBlock:^(NSError *error) {
        
        if (error!=nil) {
            NSLog(@"Error on BaseTableViewController savePhotoToAlbum - %@", [error description]);
        }
        
    }];
}


#pragma mark - Get IAPHelper
- (IAPHelper *)iapHelper
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    return appDelegate.iapHelper;
}

#pragma mark - Save
- (void)saveMOC
{
    [self.moc MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if (error) {
            NSLog(@"%@",error.localizedDescription);
        }
    }];
}

@end
