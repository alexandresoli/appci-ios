//
//  BaseViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 1/8/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BBBadgeBarButtonItem.h"
#import "IAPHelper.h"


@interface BaseViewController : UIViewController <UIPopoverControllerDelegate>

@property (nonatomic, strong) NSManagedObjectContext *moc;

@property (nonatomic, weak) IBOutlet UIButton *alertButton;
@property (nonatomic, weak) IBOutlet UIView *badgeView;
@property (nonatomic, strong) NSNumberFormatter* formatter;
@property (nonatomic, strong) BBBadgeBarButtonItem *alertBarButton;

- (void)configureAlert;
- (void)configureAlertWithBarButton;
- (NSString *)formatQuartos:(int)quantidade;
- (IBAction)showConfig:(UIButton *)button;

- (void)importFileURL:(NSURL *)url;
- (IBAction)showEventos:(id)sender;

- (IAPHelper *)iapHelper;
- (void)saveMOC;
@end
