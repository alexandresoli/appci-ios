//
//  BaseViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 1/8/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "BaseViewController.h"
#import "EventosViewController.h"
#import "ContatoViewController.h"
#import "PortfolioViewController.h"
#import "NotasViewController.h"
#import "AlertaViewController.h"
#import "CustomBadge.h"
#import "Hp12cViewController.h"
#import "HelpViewController.h"
#import "LoginViewController.h"
#import "Alerta.h"

@interface BaseViewController () <UIPopoverPresentationControllerDelegate>

@property (nonatomic, strong) CustomBadge *badge;
@property (nonatomic, strong) UIPopoverController *popoverController;

@end

@implementation BaseViewController

@synthesize formatter,popoverController;

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationPopover;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.moc = [NSManagedObjectContext MR_defaultContext];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    // Notification used when the user code is no more active.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noAccess) name:@"NoAccess" object:nil];
}


- (BOOL)prefersStatusBarHidden
{
    //    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
    //        return YES;
    //    }
    
    return NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Navigate Between Storyboards
- (IBAction)showContatos:(id)sender
{
    // Remove current view controller from observing notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // Get the storyboard named secondStoryBoard from the main bundle:
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"_Contatos_iPad" bundle:nil];
    
    // Load the initial view controller from the storyboard.
    ContatoViewController *controller = [storyboard instantiateInitialViewController];
    
    //[self.navigationController pushViewController:controller animated:NO];
    // substituído o pushViewController pelo setViewControllers por causa de problema de memória.
    [self.navigationController setViewControllers:@[controller] animated:NO];
}

- (IBAction)showPortfolio:(id)sender
{
    // Remove current view controller from observing notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // Get the storyboard named secondStoryBoard from the main bundle:
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"_Portfolio_iPad" bundle:nil];
    
    // Load the initial view controller from the storyboard.
    PortfolioViewController *controller = [storyboard instantiateInitialViewController];
    
    //[self.navigationController pushViewController:controller animated:NO];
    // substituído o pushViewController pelo setViewControllers por causa de problema de memória.
    [self.navigationController setViewControllers:@[controller] animated:NO];
}

- (IBAction)showEventos:(id)sender
{
    // Remove current view controller from observing notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // Get the storyboard named secondStoryBoard from the main bundle:
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"_Eventos_iPad" bundle:nil];
    
    // Load the initial view controller from the storyboard.
    UINavigationController *navController = [storyboard instantiateInitialViewController];
    EventosViewController *controller = [[navController viewControllers] objectAtIndex:0];
    
    //[self.navigationController pushViewController:controller animated:NO];
    // substituído o pushViewController pelo setViewControllers por causa de problema de memória.
    [self.navigationController setViewControllers:@[controller] animated:NO];
}


- (IBAction)showNotas:(id)sender
{
    // Remove current view controller from observing notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // Get the storyboard named secondStoryBoard from the main bundle:
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"_Notas_iPad" bundle:nil];
    
    // Load the initial view controller from the storyboard.
    NotasViewController *controller = [storyboard instantiateInitialViewController];
    
    //[self.navigationController pushViewController:controller animated:NO];
    // substituído o pushViewController pelo setViewControllers por causa de problema de memória.
    [self.navigationController setViewControllers:@[controller] animated:NO];
}



#pragma mark - Alert Notification
- (void)configureAlert
{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ativo == 1"];
    long count = [Alerta MR_countOfEntitiesWithPredicate:predicate inContext:self.moc];
    
    if (count > 0) {
        
        for (UIView* view in self.badgeView.subviews) {
            if ([view isKindOfClass:[CustomBadge class]]) {
                [view removeFromSuperview];
            }
        }
        
        self.badge = [CustomBadge customBadgeWithString:[NSString stringWithFormat:@"%ld",count]
                                        withStringColor:[UIColor redColor]
                                         withInsetColor:[UIColor clearColor]
                                         withBadgeFrame:NO
                                    withBadgeFrameColor:[UIColor clearColor]
                                              withScale:1.0
                                            withShining:NO];
        
        self.badge.userInteractionEnabled = NO;
        [self.alertButton setBackgroundImage:[UIImage imageNamed:@"alerta-on"] forState:UIControlStateNormal];
        self.alertButton.enabled = YES;
        [self.badge removeFromSuperview];
        [self.badgeView addSubview:self.badge];
        
    } else {
        [self.alertButton setBackgroundImage:[UIImage imageNamed:@"alerta-off"] forState:UIControlStateNormal];
        self.alertButton.enabled = NO;
        
    }
}

- (IBAction)showAlerts:(id)sender
{
    //    AlertaViewController *controller = [[AlertaViewController alloc] initWithNibName:@"AlertaViewController" bundle:nil];
    //
    //    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
    //
    //    navController.modalPresentationStyle = UIModalPresentationFormSheet;
    //
    //    [self presentViewController:navController animated:YES completion:nil];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Acessorios_iPad" bundle:nil];
    UITableViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"AlertaViewController"];
    controller.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:controller animated:YES completion:nil];
    
    
}


#pragma mark - Alert Notification iPhone

- (void)configureAlertWithBarButton
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ativo == 1"];
    long count = [Alerta MR_countOfEntitiesWithPredicate:predicate inContext:self.moc];
    
    UIButton *customButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 26, 28)];
    [customButton addTarget:self action:@selector(showAlertsIphone:) forControlEvents:UIControlEventTouchUpInside];
    
    if (count > 0) {
        [customButton setImage:[UIImage imageNamed:@"alerta-off"] forState:UIControlStateNormal];
        
        BBBadgeBarButtonItem *barButton = [[BBBadgeBarButtonItem alloc] initWithCustomUIButton:customButton];
        
        barButton.badgeValue = [NSString stringWithFormat:@"%ld",count];
        
        barButton.badgeOriginX = 15;
        barButton.badgeOriginY = -9;
        
        //barButton.badgeBGColor = [UIColor clearColor];
        //barButton.badgeTextColor = [UIColor redColor];
        
        self.alertBarButton = barButton;
        
    } else {
        [customButton setImage:[UIImage imageNamed:@"alerta-off"] forState:UIControlStateNormal];
        
        BBBadgeBarButtonItem *barButton = [[BBBadgeBarButtonItem alloc] initWithCustomUIButton:customButton];
        
        barButton.badgeValue = nil;
        
        self.alertBarButton = barButton;
        self.alertBarButton.enabled = NO;
    }
    
    self.navigationItem.leftBarButtonItem = self.alertBarButton;
}

- (IBAction)showAlertsIphone:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Acessorios_iPhone" bundle:nil];
    UITableViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"AlertaViewController"];
    
    [self presentViewController:controller animated:YES completion:nil];
}


#pragma mark - Quartos Formatter
- (NSString *)formatQuartos:(int)quantidade
{
    NSString *message = @"";
    
    if (quantidade == 0) {
        message = @"indiferente";
    } else if (quantidade == 1) {
        message = [NSString stringWithFormat:@"%d quarto", quantidade];
    } else if (quantidade == 10) {
        message = [NSString stringWithFormat:@"%d quartos ou mais", quantidade];
    } else {
        message = [NSString stringWithFormat:@"%d quartos", quantidade];
    }
    
    return message;
}


#pragma mark - Numver Formatter
- (NSNumberFormatter *)formatter
{
    if (formatter == nil) {
        formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [formatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [formatter setDecimalSeparator:@","];
        [formatter setGeneratesDecimalNumbers:YES];
        [formatter setCurrencySymbol:@"R$ "];
        [formatter setLenient:YES];
    }
    
    return formatter;
}


#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = @"";
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [Utils removeRedCorner:textField];
    return YES;
}


#pragma mark - Config
- (IBAction)showConfig:(UIButton *)button
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Acessorios_iPad" bundle:nil];
    UINavigationController *controller = [storyboard instantiateViewControllerWithIdentifier:@"OpcoesViewController"];
    controller.modalPresentationStyle = UIModalPresentationPopover;
    
    if (IS_OS_8_OR_LATER) {
        
        UIPopoverPresentationController *popPC = controller.popoverPresentationController;
        popPC.delegate = self;
        popPC.sourceView = self.view;
        popPC.sourceRect = button.frame;
        
        [self presentViewController:controller animated:YES completion:Nil];
        
        return;
    }
    
    self.popoverController = [[UIPopoverController alloc] initWithContentViewController:controller];
    self.popoverController.delegate = self;
    [self.popoverController presentPopoverFromRect:button.frame
                                            inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionAny
                                          animated:YES];
    
}


- (IBAction)showConfigIphone:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Acessorios_iPhone" bundle:nil];
    
    UITableViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"OpcoesViewController"];
    
    [self presentViewController:controller animated:YES completion:nil];
}


#pragma mark - Import File URL
- (void)importFileURL:(NSURL *)url
{
    // A importação do arquivo ocorre no storyboard de Eventos, por isso o mesmo precisa ser iniciado.
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"_Eventos_iPad" bundle:nil];
    
    UINavigationController *navController = [storyboard instantiateInitialViewController];
    EventosViewController *controller = [[navController viewControllers] objectAtIndex:0];
    
    [self.navigationController setViewControllers:@[controller] animated:NO];
    
    [controller startImportFileURL:url];
}


#pragma  mark - Access Not Valid

- (void)noAccess
{
    // salva o ultimo login do usuario
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *email = [defaults objectForKey:USER_LOGIN];
    NSNumber *lostAccess = [defaults objectForKey:LOST_ACCESS];
    
    if (lostAccess) {
        return;
    }
    
    [defaults setObject:email forKey:USER_LAST_LOGIN];
    
    [defaults setObject:[NSNumber numberWithBool:YES] forKey:LOST_ACCESS];
    [defaults synchronize];
    
    [Utils showLogin];
}


#pragma mark - Get IAPHelper
- (IAPHelper *)iapHelper
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    return appDelegate.iapHelper;
}

#pragma mark - Save
- (void)saveMOC
{
    //    [self.moc MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
    //        if (error) {
    //            NSLog(@"%@",error.localizedDescription);
    //        }
    //    }];
    
    [self.moc MR_saveToPersistentStoreAndWait];
    
}

@end
