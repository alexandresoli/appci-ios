//
//  CRMCollectionViewController.m
//  Appci
//
//  Created by BrEstate LTDA on 30/04/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "CRMCollectionViewController.h"
#import "FotoCell.h"
#import "HelpViewController.h"


@interface CRMCollectionViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

@property(nonatomic,strong) NSArray *crms;
@property (weak, nonatomic) IBOutlet UICollectionView *crmCollectionView;

@end


@implementation CRMCollectionViewController


#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    _crms = [NSArray arrayWithObjects:@"Consir",@"Imóvel PRO",@"Simbo",@"ImobiBrasil",@"Imo",@"Vista",@"Alohin",@"Gaia",@"Sub100",@"Wmb",@"Arco", nil];
    
    _crms = [NSArray arrayWithObjects:@"Consir",@"Imóvel PRO",@"Simbo",@"ImobiBrasil",@"Imo",@"Vista",@"Alohin",@"Gaia",@"Wmb",@"Arco", nil];
    
}


#pragma mark- UICollectionViewDataSource, UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.crms count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CRMCell" forIndexPath:indexPath];
    
    NSString *nameCRM = [self.crms objectAtIndex:indexPath.row];
    cell.label.text = nameCRM;
    
    NSString *imageName = [NSString stringWithFormat:@"icon-%@", nameCRM.lowercaseString];
    cell.imageView.image = [UIImage imageNamed:imageName];
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (![self.iapHelper checkPlanTopFromController:self withMessage:PLAN_TOP_MESSAGE_CRM]) {
        return;
    }
    
    NSString *nameCRM = [self.crms objectAtIndex:indexPath.row];
    NSString *segueName = [NSString stringWithFormat:@"%@Segue", nameCRM];
    
    [self performSegueWithIdentifier:segueName sender:nil];
    
    [collectionView deselectItemAtIndexPath:indexPath animated:NO];
}


#pragma mark - IBActions

- (IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"IntegreCRMSegue"]) {
        UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
        HelpViewController *controller = (HelpViewController *)[navController.viewControllers firstObject];
        
        controller.motivoProblema = @"IntegrarCRM";
    }
}

@end
