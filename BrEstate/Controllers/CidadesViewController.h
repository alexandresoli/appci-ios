//
//  CidadesViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 12/7/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "BaseTableViewController.h"


@interface CidadesViewController : BaseTableViewController

@property (nonatomic, strong) NSString *selectedCidade;
@property (nonatomic, strong) NSString *selectedEstado;

@end
