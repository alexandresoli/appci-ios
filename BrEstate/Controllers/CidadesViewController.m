//
//  CidadesViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 12/7/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "CidadesViewController.h"
#import "Cidade.h"
#import "Estado.h"
#import "EditarContatoViewController.h"

@interface CidadesViewController () <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation CidadesViewController

@synthesize fetchedResultsController;
@synthesize selectedEstado,selectedCidade;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [Utils trackWithName:@"Tela Cidades"];
    [Utils trackFlurryWithName:@"Tela Cidades"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CidadesCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15];
    
    Cidade *cidade = [fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.textLabel.text = cidade.nome;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedCidade = [[fetchedResultsController objectAtIndexPath:indexPath] nome];
    [self performSegueWithIdentifier:@"UnwindFromCidades" sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];

}

- (NSArray *) sectionIndexTitlesForTableView: (UITableView *) tableView
{
    return [self.fetchedResultsController sectionIndexTitles];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [self.fetchedResultsController sectionForSectionIndexTitle:title atIndex:index];
}


#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (fetchedResultsController != nil) {
        return fetchedResultsController;
    }
    
    fetchedResultsController.delegate = self;
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"estado = %@",selectedEstado];
    fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Cidade" andKey:@"nome" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
    
    
    return fetchedResultsController;
}

@end
