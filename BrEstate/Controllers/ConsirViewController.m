//
//  HospedariaViewController.m
//  Appci
//
//  Created by BrEstate LTDA on 4/29/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "ConsirViewController.h"
#import "ConsirImporter.h"
#import "SVProgressHUD.h"

@import QuickLook;

@interface ConsirViewController () <UITextViewDelegate,QLPreviewControllerDataSource, QLPreviewControllerDelegate,UIAlertViewDelegate>

@property (nonatomic, strong) NSManagedObjectContext *moc;
@property (nonatomic, weak) IBOutlet UITextField *textField;
@property (nonatomic, weak) IBOutlet UIProgressView *progressView;
@property (nonatomic, weak) IBOutlet UILabel *progressLabel;
@property (nonatomic, weak) IBOutlet UILabel *tutorialLabel;
@property (nonatomic, strong) UIImageView *coverImageView;
@property (nonatomic) BOOL hasAppeared;

@end

@implementation ConsirViewController


#pragma mark - View Life Cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [Utils trackWithName:@"Entrou Consir"];
    [Utils trackFlurryWithName:@"Entrou Consir"];
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [UIApplication sharedApplication].idleTimerDisabled = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSString *consirURL = [Utils valueFromDefaults:SITE_CONSIR];
    
    if (consirURL) {
        self.textField.text = consirURL;
    }

    [self formatTutorial];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (!_hasAppeared && ![Utils valueFromDefaults:SITE_CONSIR]) {
        [self showCover];
    }
    
}

#pragma mark - Tutorial Format
- (void)formatTutorial
{
    NSString *str = @"Você está a 1 passo de importar os imóveis cadastrados no seu sistema para dentro do AppCi. Basta inserir o seu site no campo abaixo e apertar o botão Importar. Em caso de dúvidas consulte o passo a passo clicando no ícone de ajuda na lateral superior a direita.";
    
    
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:str];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:15] range:NSMakeRange(0, str.length-1)];
      //  [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15] range:NSMakeRange(184, 9)];
        
    } else {
        [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:12] range:NSMakeRange(0, str.length-1)];
        //[attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:12] range:NSMakeRange(184, 9)];
    }
    
    _tutorialLabel.attributedText = attrStr;
}

#pragma mark - Cover
- (void)showCover
{
    _hasAppeared = YES;
    
    UIImage *image;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        image = [UIImage imageNamed:@"cover-integracao-ipad"];
    } else {
        image = [UIImage imageNamed:@"cover-integracao-iphone"];
    }
    
    _coverImageView = [[UIImageView alloc] initWithImage:image];
    _coverImageView.userInteractionEnabled = YES;
    _coverImageView.alpha = 0.0;
    _coverImageView.frame = self.view.frame;

    
    [UIView animateWithDuration:1.0 animations:^{
        _coverImageView.alpha = 1.0;

        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [self.navigationController.view addSubview:_coverImageView];
        } else {
            [[[[UIApplication sharedApplication] delegate] window] addSubview:_coverImageView];
        }
    }];
    
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeCover)];
    [_coverImageView addGestureRecognizer:tapGesture];
}

- (void)closeCover
{
    [_coverImageView removeFromSuperview];
}

#pragma mark - Start

- (IBAction)start:(id)sender
{
    [Utils trackWithName:@"Iniciou importação Consir"];
    [Utils trackFlurryWithName:@"Iniciou importação Consir"];

    
    [self resignFirstResponder];
    
    _progressView.progress = 0.0f;
    _progressLabel.text = @"";
    
    if([self validate]) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:self.textField.text forKey:SITE_CONSIR];
        [defaults synchronize];
        
        [SVProgressHUD showWithStatus:@"Iniciando importação..." maskType:SVProgressHUDMaskTypeBlack];
        [self.textField resignFirstResponder];
        
        [self.view endEditing:YES];
        [self import:NO];
        
    }
    

}


#pragma mark - Validate Form

- (BOOL)validate
{
    if(_textField.text == nil || [_textField.text isEqualToString:@""]) {
        [Utils showMessage:@"Digite o endereço do site que deseja importar!"];
        return NO;
    }
    
    return YES;
}


-  (IBAction)showHelp:(id)sender
{
    QLPreviewController *previewController=[[QLPreviewController alloc]init];
    previewController.delegate = self;
    previewController.dataSource = self;
    [self presentViewController:previewController animated:YES completion:nil];
}


#pragma mark - QLPreviewControllerDataSource

- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller
{
    return 1;
}


- (id <QLPreviewItem>)previewController:(QLPreviewController *)controller previewItemAtIndex:(NSInteger)index
{
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"help-integracao-consir" withExtension:@"pdf"];
    return url;
}


#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


- (void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
    
        int movementDistance = 0; // tweak as needed
        
        if ([[UIScreen mainScreen] bounds].size.height < 568) {
            movementDistance = -60;
        }
        
        
        const float movementDuration = 0.3f; // tweak as needed
        
        int movement = (up ? movementDistance : -movementDistance);
        
        [UIView beginAnimations: @"animateTextField" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];
        
    }
}

#pragma mark - Back
- (IBAction)back:(id)sender
{
    if (_hasStarted) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Aviso" message:@"Sair desta tela irá interromper a importação, prosseguir mesmo assim?" delegate:self cancelButtonTitle:@"Continuar" otherButtonTitles:@"Interromper", nil];
        alertView.tag = 1000;
        [alertView show];
        
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            break;
        case 1:
            
            if (alertView.tag == 1000) {
                [self.navigationController popViewControllerAnimated:YES];
                break;
            }
            
            [self import:YES];
            break;
            
        default:
            break;
    }
}


#pragma mark - Import
- (void)import:(BOOL)isForced
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        ConsirImporter *imp = [[ConsirImporter alloc] init];
        imp.progressLabel = _progressLabel;
        imp.progressView = _progressView;
        imp.parentController = self;
        
        [imp startWithURL:self.textField.text usingMoc:self.moc forced:isForced];
        
        
    });
}

@end
