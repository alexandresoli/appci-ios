//
//  ContatoViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/11/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Cliente;
@interface ContatoViewController : BaseViewController

@property (nonatomic, weak) Cliente *selectedCliente;


@end
