//
//  ContatoViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/11/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "ContatoViewController.h"
#import "ListaContatosViewController.h"
#import "Alerta.h"
#import "Cliente.h"
#import "AlertaUtils.h"

@interface ContatoViewController ()

@property (nonatomic, strong) ListaContatosViewController *listaContatosController;

@end

@implementation ContatoViewController

@synthesize alertButton,badgeView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    // same order from storyboard controllers
   // NSLog(@"children : %@", self.childViewControllers);
    self.listaContatosController = [self.childViewControllers objectAtIndex:0];
    
    UINavigationController *navController = [self.childViewControllers objectAtIndex:1];
    self.listaContatosController.detalhesContatoViewController = [[navController viewControllers] firstObject];
    self.listaContatosController.selectedContato = self.selectedCliente;
    
    
    //[AlertaUtils checkIncompleteContacts:self.moc];
    
    // add model changes notification
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDataModelChange:) name:NSManagedObjectContextObjectsDidChangeNotification object:self.moc];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [Utils trackWithName:@"Aba Contatos"];
//    [Utils trackFlurryWithName:@"Aba Contatos"];
    
    [self configureAlert];
}

- (void)didReceiveMemoryWarning1
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//- (BOOL)prefersStatusBarHidden
//{
//    return YES;
//}


#pragma mark - Notification Center
//- (void)handleDataModelChange:(NSNotification *)note
//{
//    NSSet *updatedObjects = [[note userInfo] objectForKey:NSUpdatedObjectsKey];
//    NSSet *insertedObjects = [[note userInfo] objectForKey:NSInsertedObjectsKey];
//    NSSet *deletedObjects = [[note userInfo] objectForKey:NSDeletedObjectsKey];
//    
//    if ([updatedObjects containsMemberOfClass:[Cliente class]] || [insertedObjects containsMemberOfClass:[Cliente class]] || [deletedObjects containsMemberOfClass:[Cliente class]]) {
//        [AlertaUtils checkIncompleteContacts:self.moc];
//        [self configureAlert];
//    }
//}


@end
