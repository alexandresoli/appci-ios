//
//  CorretorTableViewController.m
//  Appci
//
//  Created by BrEstate LTDA on 6/2/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "CorretorTableViewController.h"
#import "Corretor+Utils.h"
#import "DateUtils.h"
#import "SyncViewController.h"
#import "Imobiliaria.h"
#import "FirebaseUtilsUser.h"


@interface CorretorTableViewController () <UITextFieldDelegate,UIAlertViewDelegate>

@property (nonatomic, strong) Corretor *corretor;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (nonatomic, weak) IBOutlet UITextField *nameTextField;
@property (nonatomic, weak) IBOutlet UITextField *phoneTextField;

@property (weak, nonatomic) IBOutlet UILabel *planTopLabel;
@property (weak, nonatomic) IBOutlet UITableViewCell *planTopCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *buttonsCell;
@property (nonatomic, strong) NSString *currentEmail;

@property (nonatomic) BOOL buyPlanTop;

@end


@implementation CorretorTableViewController


#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self load];
    
    [Utils trackWithName:@"Entrou Tela Corretor"];
    [Utils trackFlurryWithName:@"Entrou Tela Corretor"];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self load];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self configurePlanTopCell];
    
    if ([Imobiliaria MR_findFirstInContext:self.moc]) {
        self.emailTextField.enabled = NO;
        [self.emailTextField setTextColor:[UIColor grayColor]];
    }
}


#pragma mark - Get IAPHelper

- (IAPHelper *)iapHelper
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    return appDelegate.iapHelper;
}


#pragma mark - Load

- (void)load
{
    self.corretor = [Corretor MR_findFirstInContext:self.moc];
    
    if (self.corretor != nil) {
        
        _currentEmail = [Utils valueFromDefaults:USER_LOGIN];
        
        self.emailTextField.text = _currentEmail;
        self.nameTextField.text = self.corretor.nome;
        self.phoneTextField.text = self.corretor.telefone;
    }
}


#pragma mark - Configure Plan Top Cell

- (void)configurePlanTopCell
{
    self.planTopCell.hidden = YES;
    self.buttonsCell.hidden = YES;
    
    if (self.corretor) {
        
        // Single user.
        self.planTopCell.hidden = NO;
        
        if (!self.corretor.dataExpiraPlanoTop) {
            
            self.planTopLabel.text = @"Se torne um usuário TOP! \n \nAssine o plano TOP por 1 mês pelo valor de 15,99 doláres. Caso já tenha assinado em outro dispositivo selecione Assinar Agora para restaurar sua assinatura.";
            self.buttonsCell.hidden = NO;
            
        } else {
            
            if ([self.iapHelper isPlanTopValidForCorretor:self.corretor]) {
                
                NSString *text = [NSString stringWithFormat:@"Você é um usuário TOP! \n \nSeu plano TOP expira em %@.", [DateUtils stringFromDate:self.corretor.dataExpiraPlanoTop]];
                
                self.planTopLabel.text = text;
                self.buttonsCell.hidden = YES;
                
            } else {
                
                NSString *text = [NSString stringWithFormat:@"Seu plano TOP expirou em %@. \n \n Renove o plano selecionando o botão Assinar Agora. Caso já tenha renovado em outro dispositivo selecione Assinar Agora.", [DateUtils stringFromDate:self.corretor.dataExpiraPlanoTop]];
                
                self.planTopLabel.text = text;
                self.buttonsCell.hidden = NO;
                
            }
        }
    }
}


#pragma mark - Validate

- (BOOL)formIsValid
{
    
    BOOL isValid = YES;
    if (_nameTextField.text.length == 0) {
        [Utils addRedCorner:_nameTextField];
        isValid = NO;
    }
    
    if (_phoneTextField.text.length == 0) {
        [Utils addRedCorner:_phoneTextField];
        isValid = NO;
    }
    
    return isValid;
}


#pragma mark - Save

- (IBAction)save:(id)sender
{
    if ([self formIsValid]) {
        
        if (![_currentEmail isEqualToString:_emailTextField.text]) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Atenção" message:@"Mudar seu e-mail irá alterar seu login no AppCi. Tem certeza que deseja prosseguir?" delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles:@"OK", nil];
            [alertView show];
            
        } else {
            
            [self saveCorretor];
        }
        
    }
}

- (void)saveCorretor
{
    [self.view endEditing:YES];
    
    if (![Utils isConnected]) {
        [Utils showMessage:@"É necessário estar conectado à internet para atualizar suas informações"];
        return;
    }
    
    [Utils showProgress:@"Atualizando informações..."];
    
    if (![_currentEmail isEqualToString:_emailTextField.text]) {
        
        [FirebaseUtilsUser changeEmailForUser:_currentEmail toNewEmail:_emailTextField.text withCompletionBlock:^(NSError *error) {
            if (error) {
                NSLog(@"%@", error.localizedDescription);
                
                [Utils dismissProgress];
                [Utils showMessage:@"Ocorreu um erro ao atualizar suas informações. Se o erro persistir entre em contato com suporte@brestate.com.br"];
                return;
            }
            
            [Utils addObjectToDefaults:_emailTextField.text withKey:USER_LOGIN];
            
            [self updateUserInFirebase];
        }];
        
    } else {
        [self updateUserInFirebase];
    }
}

- (void)updateUserInFirebase
{
    [FirebaseUtilsUser updateUserInfoWithEmail:self.emailTextField.text
                                       andName:self.nameTextField.text
                                      andPhone:self.phoneTextField.text
                            andCompletionBlock:^(NSError *error)
     {
         if (error) {
             NSLog(@"%@", error.localizedDescription);
             
             [Utils dismissProgress];
             [Utils showMessage:@"Ocorreu um erro ao atualizar suas informações. Se o erro persistir entre em contato com suporte@brestate.com.br"];
             return;
         }
         
         dispatch_async(dispatch_get_main_queue(), ^{
             if (_corretor == nil) {
                 _corretor = [Corretor corretorWithContext:self.moc];
             }
             
             _corretor.email = _emailTextField.text;
             _corretor.nome = _nameTextField.text;
             _corretor.telefone = _phoneTextField.text;
             _corretor.version = [NSDate date];
             
             [self.moc MR_saveToPersistentStoreAndWait];
             
             [Utils dismissProgress];
             
             if (_buyPlanTop) {
                 [self.iapHelper buyPlanTOP];
                 [self dismissViewControllerAnimated:YES completion:nil];
                 return;
             }
             
             [Utils showMessage:@"Informações atualizadas com sucesso."];
         });
     }];
}


#pragma mark - Close

- (IBAction)close:(id)sender
{
    long count = [[self.navigationController viewControllers] count];
    
    // Veio da tela de config
    if (count == 1) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        
        // veio da tela de eventos
        [self.navigationController popViewControllerAnimated:YES];
    }
}


#pragma mark - UITextFieldDelegate

// return NO to not change text
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    long length = textField.text.length;
    
    if ([textField isEqual:_phoneTextField]) {
        
        if (range.length == 1) { // backspace
            return YES;
        }
        
        if (length == 0) {
            textField.text = [NSString stringWithFormat:@"(%@",string];
            return NO;
        } else if (length == 3) {
            textField.text = [NSString stringWithFormat:@"%@) %@",textField.text,string];
            return NO;
        } else if (length == 10) {
            textField.text = [NSString stringWithFormat:@"%@-%@",textField.text,string];
            return NO;
        }
    }
    
    return YES;
}


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.hidden) {
        return 0;
        
    } else {
        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }
}


#pragma mark - Buy Plan Top

- (IBAction)buy:(id)sender
{
    if ([self formIsValid]) {
        
        _buyPlanTop = YES;
        [self saveCorretor];
    }
}


#pragma mark - Dismiss Keyboard

- (IBAction)dismissKeyboard:(id)sender
{
    [self.view endEditing:YES];
}


#pragma mark - Logout

- (IBAction)showLogout
{
    long count = [[Utils firebaseUtils] countDataToSend];
    
    NSString *message;
    
    if (count > 0) {
        message = @"Existem informações não sincronizadas, estas informações serão perdidas caso você continue. Prosseguir mesmo assim?";
    } else {
        message = @"Tem certeza ?";
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Aviso" message:message delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Sim", nil];
    alertView.tag = 1000;
    [alertView show];
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch (buttonIndex) {
        case 0:
            
            if (alertView.tag != 1000) {
                _emailTextField.text = _currentEmail;
            }
            
            break;
            
        case 1:
            
            // logoff
            if (alertView.tag == 1000) {
                [self doLogout];
                
            } else {
                [self saveCorretor];
            }
            
            break;
            
        default:
            break;
    }
}

- (void)doLogout
{
    // Firebase logoff
    Firebase *ref = [[Firebase alloc] initWithUrl:FIREBASE_APPCI_URL];
    [ref unauth];
    
    [Utils prepareLogout];
    [Utils showLogin];
}


@end
