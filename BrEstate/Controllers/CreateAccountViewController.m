//
//  CreateAccountViewController.m
//  Appci
//
//  Created by Leonardo Barros on 23/10/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "CreateAccountViewController.h"
#import "Corretor+Utils.h"
#import "EventosViewController.h"
#import "PasswordViewController.h"
#import "FirebaseUtilsUser.h"


@interface CreateAccountViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordRetypeTextField;
@property (nonatomic, strong) NSManagedObjectContext *moc;

@end


@implementation CreateAccountViewController


#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    self.moc = [NSManagedObjectContext MR_defaultContext];
    
    [self configureView];
}


-(BOOL)shouldAutorotate
{
    return NO;
}


#pragma mark - View Customization

- (void)configureView
{
    // textfield image
    UIImageView *emailImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 16, 11)];
    emailImageView.image = [UIImage imageNamed:@"mail"];
    
    UIView *emailPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 24)];
    [emailPaddingView addSubview:emailImageView];
    
    self.emailTextField.leftView = emailPaddingView;
    self.emailTextField.leftViewMode = UITextFieldViewModeAlways;
    
    // password image
    UIImageView *passwordImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 12, 16)];
    passwordImageView.image = [UIImage imageNamed:@"lock"];
    
    UIView *passwordPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 24)];
    [passwordPaddingView addSubview:passwordImageView];
    
    self.passwordTextField.leftView = passwordPaddingView;
    self.passwordTextField.leftViewMode = UITextFieldViewModeAlways;
    
    // password retype image
    UIImageView *passwordRetypeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 12, 16)];
    passwordRetypeImageView.image = [UIImage imageNamed:@"lock"];
    
    UIView *passwordRetypePaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 24)];
    [passwordRetypePaddingView addSubview:passwordRetypeImageView];
    
    self.passwordRetypeTextField.leftView = passwordRetypePaddingView;
    self.passwordRetypeTextField.leftViewMode = UITextFieldViewModeAlways;
}


#pragma mark - Back

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - IBAction Create Account

- (IBAction)createAccount:(id)sender
{
    [self.view endEditing:YES];
    
    // connection check
    if (![Utils isConnected]) {
        [Utils showMessage:@"Verifique sua conexão com a internet."];
        return;
    }
    
    // form validation
    if (![self formLoginIsValid]) {
        return;
    }
    
//    [self checkUser];
    [self createUser];
}


#pragma mark - Create User

- (void)createUser
{
    [Utils showProgress:@"Criando usuário. Aguarde..."];
    
    [Utils resetDatabaseIfNeededWithEmail:self.emailTextField.text andMoc:self.moc];
    [self.moc MR_saveToPersistentStoreAndWait];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        // Create user Firebase
        [FirebaseUtilsUser createUser:self.emailTextField.text withPassword:self.passwordTextField.text andCompletionBlock:^(NSError *error) {
            
            if (error) {
                
                [Utils dismissProgress];
                
                if (error.code == FAuthenticationErrorEmailTaken) {
                    [Utils showMessage:[NSString stringWithFormat:@"Usuário já existente com o e-mail %@! Você será redirecionado para tela de login.", self.emailTextField.text]];
                    [self back:nil];
                    
                    return;
                }
                
                NSLog(@"%@", error.localizedDescription);
                
                [Utils showMessage:@"Ocorreu um erro ao criar o usuário. Tente novamente. Se o erro persistir entre em contato com suporte@brestate.com.br"];
                
                return;
            }
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                [Utils saveLoginUserDefaultsWithEmail:self.emailTextField.text andPassword:self.passwordTextField.text];
                
                [FirebaseUtilsUser updateCorretorInformationFromFirebase];
                
                [FirebaseUtilsUser registerDevice];
                
                [Utils dismissProgress];
                
                // Observers Firebase.
                [[Utils firebaseUtils] startObservers];
                
                [self enterAppci];
            });
            
        }];
    });
}


#pragma mark - Enter Appci

- (void)enterAppci
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:LOST_ACCESS];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Utils checkFirstRunForPrepareDataWithMoc:self.moc];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            // Get the storyboard named secondStoryBoard from the main bundle:
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"_Eventos_iPad" bundle:nil];
            
            // Load the initial view controller from the storyboard.
            UINavigationController *navController = [storyboard instantiateInitialViewController];
            EventosViewController *controller = [[navController viewControllers] objectAtIndex:0];
            
            [self.navigationController setViewControllers:@[controller] animated:NO];
            
        } else {
            [self performSegueWithIdentifier:@"EnterSegue" sender:self];
        }
    });
}


#pragma mark - Form Validation

- (BOOL)formLoginIsValid
{
    BOOL isValid = YES;
    
    // email
    if (self.emailTextField.text.length > 0) {
        if (![Utils validateEmail:self.emailTextField.text]) {
            [Utils addRedCorner:self.emailTextField];
            isValid = NO;
        }
        
    } else {
        [Utils addRedCorner:self.emailTextField];
        isValid = NO;
    }
    
    if (_passwordTextField.text.length == 0 ) {
        [Utils addRedCorner:_passwordTextField];
        isValid = NO;
    }
    
    if (_passwordRetypeTextField.text.length == 0 ) {
        [Utils addRedCorner:_passwordRetypeTextField];
        isValid = NO;
    }
    
    if (![_passwordTextField.text isEqualToString:_passwordRetypeTextField.text] && isValid == YES) {
        [Utils showMessage:@"Senha não confere!"];
        isValid = NO;
    }
    
    return isValid;
}


#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPad) {
        
        if ([[UIScreen mainScreen] bounds].size.height < 568) {
            [self animateTextField:textField up:YES];
        }
    }
    
    [Utils removeRedCorner:textField];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPad) {
        
        if ([[UIScreen mainScreen] bounds].size.height < 568) {
            [self animateTextField:textField up:NO];
        }
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _emailTextField) {
        [_passwordTextField becomeFirstResponder];
        
    } else if(textField == _passwordTextField) {
        [_passwordRetypeTextField becomeFirstResponder];
        
    } else if (textField == _passwordRetypeTextField) {
        [textField resignFirstResponder];
        [self createAccount:nil];
    }
    
    return YES;
}


- (void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -85; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


#pragma mark - IBAction Dismiss Keyboard

- (IBAction)dismissKeyboard:(id)sender
{
    [self.view endEditing:YES];
}


#pragma mark - Prepare For Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"EnterSegue"]) {
        // Define o segundo item do tab bar como o selecionado na versão do iPhone.
        UITabBarController *tabBar = (UITabBarController *)segue.destinationViewController;
        tabBar.selectedIndex = 1;
    }
}


@end
