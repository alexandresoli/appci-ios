//
//  DetalhesClienteViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/2/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Cliente;
@interface DetalhesContatoViewController : BaseViewController

@property (nonatomic, weak) UINavigationController *superNavigationController;

@property (nonatomic, strong) Cliente *selectedCliente;

- (void)changeCliente:(Cliente *)cliente;

@end
