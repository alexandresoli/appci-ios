//
//  DetalhesClienteViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/2/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "DetalhesContatoViewController.h"
#import "MailViewController.h"
#import "ContatoViewController.h"
#import "EditarContatoViewController.h"
#import "EventosViewController.h"
#import "PortfolioViewController.h"
#import "MapaViewController.h"
#import "DetalhesProprietarioViewController.h"
#import "DetalhesInteressadoViewController.h"
#import "ListaAcompanhamentoContatoViewController.h"

#import "DetalhesContatoCell.h"
#import "Evento.h"
#import "Cliente.h"
#import "DateUtils.h"
#import "TipoCliente.h"
#import "SubtipoCliente.h"
#import "Endereco.h"
#import "Cidade.h"
#import "Bairro.h"
#import "AjudaViewController.h"
@interface DetalhesContatoViewController ()

@property (nonatomic, weak) IBOutlet UILabel *nomeContatoLabel;
@property (nonatomic, weak) IBOutlet UILabel *tipoContatoLabel;
@property (nonatomic, weak) IBOutlet UIImageView *tipoContatoImageView;

@property (nonatomic, weak) IBOutlet UILabel *telefoneLabel;
@property (nonatomic, weak) IBOutlet UILabel *emailLabel;
@property (nonatomic, weak) IBOutlet UIButton *acompanhamentoButton;


@property (nonatomic, weak) IBOutlet UIView *containerProprietarioView;
@property (nonatomic, weak) IBOutlet UIView *containerInteressadoView;
@property (nonatomic, weak) IBOutlet UIView *containerSemInformacoesView;

@end

@implementation DetalhesContatoViewController

@synthesize selectedCliente;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.containerProprietarioView.hidden = YES;
    self.containerInteressadoView.hidden = YES;
    
    if (self.selectedCliente) {
        [self changeClienteIphone:self.selectedCliente];
    }

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    [ParseUtils queryAllAcompanhamentosFromParse];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Detalhes Contato"];
    [Utils trackFlurryWithName:@"Detalhes Contato"];
    
}


- (void)clean
{
    self.tipoContatoLabel.layer.borderColor = [UIColor clearColor].CGColor;
    self.tipoContatoLabel.layer.borderWidth = 0.0;
    self.tipoContatoImageView.image = nil;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)changeCliente:(Cliente *)cliente
{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    self.acompanhamentoButton.enabled = YES;

    self.containerSemInformacoesView.hidden = NO;
    
    self.selectedCliente = cliente;
    
    [self loadDetalhesContato];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",selectedCliente.subtipo];
    SubtipoCliente *subtipo = [SubtipoCliente MR_findFirstWithPredicate:predicate inContext:self.moc];
    
    if ([subtipo.tipoCliente.descricao isEqualToString:@"Proprietário"]) {
        
        self.containerInteressadoView.hidden = YES;
        
        if ([[cliente.imovel allObjects] count] == 0) {
            self.containerProprietarioView.hidden = YES;
            self.containerSemInformacoesView.hidden = NO;
        } else {
            self.containerProprietarioView.hidden = NO;
            self.containerSemInformacoesView.hidden = YES;
        }
        
        DetalhesProprietarioViewController *controller = [self.childViewControllers objectAtIndex:0];
        controller.superNavigationController = _superNavigationController;
        [controller loadImovel:cliente];
        
    } else if ([subtipo.tipoCliente.descricao isEqualToString:@"Interessado"]) {

        self.containerSemInformacoesView.hidden = YES;
        self.containerInteressadoView.hidden = NO;
        self.containerProprietarioView.hidden = YES;

        DetalhesInteressadoViewController *controller = [self.childViewControllers objectAtIndex:1];
        controller.superNavigationController = self.superNavigationController;
        [controller loadPerfil:cliente];
    }
}


- (void)changeClienteIphone:(Cliente *)cliente
{
    self.acompanhamentoButton.enabled = YES;
    
    self.containerSemInformacoesView.hidden = NO;
    
    self.selectedCliente = cliente;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",selectedCliente.subtipo];
    SubtipoCliente *subtipo = [SubtipoCliente MR_findFirstWithPredicate:predicate inContext:self.moc];
    
    [self loadDetalhesContato];
    
    if ([subtipo.tipoCliente.descricao isEqualToString:@"Proprietário"]) {
        
        self.containerInteressadoView.hidden = YES;
        
        if ([[cliente.imovel allObjects] count] == 0) {
            self.containerProprietarioView.hidden = YES;
            self.containerSemInformacoesView.hidden = NO;
        } else {
            self.containerProprietarioView.hidden = NO;
            self.containerSemInformacoesView.hidden = YES;
        }
        
        DetalhesProprietarioViewController *controller = [self.childViewControllers objectAtIndex:0];
        controller.superNavigationController = self.superNavigationController;
        [controller loadImovel:cliente];
        
    } else if ([subtipo.tipoCliente.descricao isEqualToString:@"Interessado"]) {
        
        self.containerSemInformacoesView.hidden = YES;
        self.containerInteressadoView.hidden = NO;
        self.containerProprietarioView.hidden = YES;
        
        DetalhesInteressadoViewController *controller = [self.childViewControllers objectAtIndex:1];
        controller.superNavigationController = self.superNavigationController;
        [controller loadPerfil:cliente];
    }
}


- (void)loadDetalhesContato
{
    [self clean];
    
    Cliente *cliente = self.selectedCliente;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",selectedCliente.subtipo];
    SubtipoCliente *subtipo = [SubtipoCliente MR_findFirstWithPredicate:predicate inContext:self.moc];
    
    self.tipoContatoLabel.text = subtipo.descricao;
    self.nomeContatoLabel.text = cliente.nome;
    
    if (subtipo.tipoCliente.descricao.length > 0) {
        
        // border around label
        self.tipoContatoLabel.layer.borderColor = [UIColor blackColor].CGColor;
        self.tipoContatoLabel.layer.borderWidth = 1.0;
        
        if ([subtipo.tipoCliente.descricao isEqualToString:@"Proprietário"]) {
            self.tipoContatoImageView.image = [UIImage imageNamed:@"contato-p"];
        } else if ([subtipo.tipoCliente.descricao isEqualToString:@"Interessado"]) {
            self.tipoContatoImageView.image = [UIImage imageNamed:@"contato-i"];
        } else {
            self.tipoContatoImageView.image = [UIImage imageNamed:@"contato-o"];
        }
    }
    

    NSString *telefone = cliente.telefone;
    
    if (telefone.length == 0) {
        telefone = @" - ";
    }
    
    if (cliente.telefoneExtra.length > 0) {
        telefone = [telefone stringByAppendingString:[NSString stringWithFormat:@" / %@",cliente.telefoneExtra]];
    }
    
    self.telefoneLabel.text = telefone;
    
    if (cliente.email.length == 0) {
        self.emailLabel.text = @" - ";
    } else {
        self.emailLabel.text = cliente.email;
    }
}


#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([segue.identifier isEqualToString:@"MapaSegue"]) {
        
        MapaViewController *mapaController = segue.destinationViewController;
        mapaController.selectedContato = selectedCliente;
        
    } else if ([segue.identifier isEqualToString:@"AcompanhamentoSegue"]) {
        
        ListaAcompanhamentoContatoViewController *controller = segue.destinationViewController;
        controller.selectedCliente = self.selectedCliente;
    }
}
@end
