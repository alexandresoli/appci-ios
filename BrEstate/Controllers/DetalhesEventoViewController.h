//
//  DetalhesEventoViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/1/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Evento;

@interface DetalhesEventoViewController : BaseViewController
@property (nonatomic, weak) IBOutlet UILabel *detalhesEvento;
@property (nonatomic, weak) IBOutlet UILabel *horarioEvento;

@property (nonatomic, weak) UINavigationController *superNavigationController;

@property (nonatomic, weak) Evento *eventoSelected;

- (void)changeDetalhes:(Evento *)evento;
@end
