//
//  DetalhesEventoViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/1/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "DetalhesEventoViewController.h"
#import "ListaContatosAgendaViewController.h"
#import "ListaImoveisAgendaViewController.h"
#import "Evento.h"
#import "TipoEvento.h"
#import "DateUtils.h"

@interface DetalhesEventoViewController ()

@property (nonatomic, weak) IBOutlet UILabel *tituloLabel;

@property (nonatomic, weak) IBOutlet UIView *semInformacoesContainerView;
@property (nonatomic, weak) IBOutlet UIView *contatosContainerView;
@property (nonatomic, weak) IBOutlet UIView *imovelContainerView;

@end

@implementation DetalhesEventoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.eventoSelected) {
        [self changeDetalhes:self.eventoSelected];
    }
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Detalhes Evento (Agenda)"];
    [Utils trackFlurryWithName:@"Detalhes Evento (Agenda)"];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)changeDetalhes:(Evento *)evento
{
    if (evento) {
        NSString *tipo;
        
        self.semInformacoesContainerView.hidden = NO;
        [self.view bringSubviewToFront:self.semInformacoesContainerView];
        
        if (evento.tipoEvento == nil) {
            //tipo = @"Novo Evento";
            tipo = evento.titulo;
        } else {
            tipo = [NSString stringWithFormat:@"%@ - %@", evento.tipoEvento, evento.titulo];
        }
        
        self.tituloLabel.text = [NSString stringWithFormat:@"%@ -- %@ - %@",[tipo capitalizedString],[DateUtils timeFromDate:evento.dataInicio],[DateUtils timeFromDate:evento.dataFim]];
        
        if ([evento.cliente allObjects].count > 0 ) {
            [self.view bringSubviewToFront:self.contatosContainerView];
            self.semInformacoesContainerView.hidden = YES;
        }
        
        if (evento.imovel != nil) {
            [self.view bringSubviewToFront:self.imovelContainerView];
            self.semInformacoesContainerView.hidden = YES;
        }
        
        
        ListaContatosAgendaViewController *contatosController = [self.childViewControllers objectAtIndex:0];
        contatosController.superNavigationController = self.superNavigationController;
        [contatosController changeContatos:evento];
        
        ListaImoveisAgendaViewController *imoveisController = [self.childViewControllers objectAtIndex:1];
        imoveisController.superNavigationController = self.superNavigationController;
        [imoveisController changeImoveis:evento];
        
    } else {
        self.tituloLabel.text = nil;
        self.semInformacoesContainerView.hidden = NO;
        [self.view bringSubviewToFront:self.semInformacoesContainerView];
    }
    
}
@end
