//
//  DetalhesImovelViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/5/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Imovel;

@interface DetalhesImovelViewController : BaseViewController

@property (nonatomic, weak) UINavigationController *superNavigationController;
@property (nonatomic, strong) Imovel *selectedImovel;

- (void)loadImovel:(Imovel *)imovel;

@end
