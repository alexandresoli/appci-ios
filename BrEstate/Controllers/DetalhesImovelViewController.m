//
//  DetalhesImovelViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/5/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "DetalhesImovelViewController.h"
#import "MapaViewController.h"
#import "EditarImovelViewController.h"
#import "FotosViewController.h"
#import "ContatoViewController.h"
#import "AcompanhamentoImovelViewController.h"
#import "ListaContatosViewController.h"
#import "DocumentosImovelViewController.h"

#import "Cliente.h"
#import "Imovel.h"
#import "Endereco.h"
#import "Estado.h"
#import "Cidade.h"
#import "TipoImovel.h"
#import "TipoOferta.h"
#import "TagImovel.h"
#import "Negociacao.h"
#import "Evento.h"

@interface DetalhesImovelViewController ()

@property (nonatomic, weak) IBOutlet UITextView *descricaoTextView;
@property (nonatomic, weak) IBOutlet UITextView *detalhesTextView;
@property (nonatomic, weak) IBOutlet UITextView *caracteristicasImovelTextView;
@property (nonatomic, weak) IBOutlet UITextView *caracteristicasAreaComumTextView;
@property (nonatomic, weak) IBOutlet UIButton *proprietarioButton;
@property (nonatomic, weak) IBOutlet UIButton *acompanhamentoButton;
@property (nonatomic, weak) IBOutlet UIButton *documentosButton;
@property (nonatomic, weak) IBOutlet UILabel *totalPropostasLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalAnaliseLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalFechadasLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalCanceladasLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalVisitasLabel;
@property (weak, nonatomic) IBOutlet UIView *detalhesView;
@property (weak, nonatomic) IBOutlet UIView *outrosView;
@property (weak, nonatomic) IBOutlet UIView *descricaoView;
@property (weak, nonatomic) IBOutlet UIButton *expandDescricaoButton;
@property (weak, nonatomic) IBOutlet UIButton *expandDetalhesButton;
@property (weak, nonatomic) IBOutlet UIButton *expandOutrosButton;

@end


@implementation DetalhesImovelViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Detalhes Imovel"];
    [Utils trackFlurryWithName:@"Detalhes Imovel"];

    
    if (![[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self expandDetalhesIphone];
        self.detalhesTextView.hidden = NO;
    }
    
    if (self.selectedImovel) {
        [self loadImovel:self.selectedImovel];
    }
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadImovel:(Imovel *)imovel
{
    
    // total of visits
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"imovel = %@ AND tipoEvento = 'Visita'",imovel];
    
    long count = [Evento MR_countOfEntitiesWithPredicate:predicate inContext:self.moc];
    
    self.totalVisitasLabel.text = [NSString stringWithFormat:@"%ld",count];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
    self.documentosButton.enabled = (imovel != nil) ? YES : NO;
    self.acompanhamentoButton.enabled = (imovel != nil) ? YES : NO;
    
    self.selectedImovel = imovel;
    
    if (imovel == nil) {
        
        self.descricaoTextView.text = @"";
        self.detalhesTextView.text = @"";
        self.caracteristicasImovelTextView.attributedText  = [[NSAttributedString alloc] initWithString:@""];
        self.caracteristicasAreaComumTextView.attributedText =  [[NSAttributedString alloc] initWithString:@""];
        self.totalVisitasLabel.text = @"";
        self.totalPropostasLabel.text = @"";
        self.totalAnaliseLabel.text = @"";
        self.totalCanceladasLabel.text = @"";
        self.totalFechadasLabel.text = @"";
        
    } else {
        
        [self configureCounter:imovel];
        
        self.descricaoTextView.text = imovel.descricao;
        self.detalhesTextView.text = [self formatDetalhes];
        self.caracteristicasImovelTextView.attributedText = [self formatCaracteristicasImovel];
        self.caracteristicasAreaComumTextView.attributedText  = [self formatCaracteristicasAreaComum];
        
        
    }
    
    self.proprietarioButton.enabled = (imovel.cliente == nil) ? NO : YES;
}


- (void)configureCounter:(Imovel *)imovel
{
    // propostas
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"imovel = %@ AND status = 'Proposta'",imovel];
    long count = [Negociacao MR_countOfEntitiesWithPredicate:predicate inContext:self.moc];
    self.totalPropostasLabel.text = [NSString stringWithFormat:@"%ld",count];
    
    // analise
    predicate = [NSPredicate predicateWithFormat:@"imovel = %@ AND status = 'Análise'",imovel];
    count = [Negociacao MR_countOfEntitiesWithPredicate:predicate inContext:self.moc];
    self.totalAnaliseLabel.text = [NSString stringWithFormat:@"%ld",count];
    
    // fechadas
    predicate = [NSPredicate predicateWithFormat:@"imovel = %@ AND status = 'Fechada'",imovel];
    count = [Negociacao MR_countOfEntitiesWithPredicate:predicate inContext:self.moc];
    self.totalFechadasLabel.text = [NSString stringWithFormat:@"%ld",count];
    
    // canceladas
    predicate = [NSPredicate predicateWithFormat:@"imovel = %@ AND status = 'Cancelada'",imovel];
    count = [Negociacao MR_countOfEntitiesWithPredicate:predicate inContext:self.moc];
    self.totalCanceladasLabel.text = [NSString stringWithFormat:@"%ld",count];
}

- (NSString *)formatDetalhes
{
    NSString *str = [NSString stringWithFormat:@"Quartos: %@ \n", self.selectedImovel.quartos];
    
    if([self.selectedImovel.areaUtil intValue] > 0){
        str = [str stringByAppendingString:[NSString stringWithFormat:@"Área útil: %@ m2 \n", self.selectedImovel.areaUtil]];
    }
    
    
    str =  [str stringByAppendingString:[NSString stringWithFormat:@"Valor: %@ \n", [self.formatter stringFromNumber: self.selectedImovel.valor]]];
    str =  [str stringByAppendingString:[NSString stringWithFormat:@"Condomínio: %@ \n", [self.formatter stringFromNumber: self.selectedImovel.condominio]]];
    str =  [str stringByAppendingString:[NSString stringWithFormat:@"IPTU: %@ \n", [self.formatter stringFromNumber: self.selectedImovel.iptu]]];
    
    return str;
}

- (NSAttributedString *)formatCaracteristicasImovel
{
    
    TagImovel *tags = self.selectedImovel.tags;
    
    NSString *str = [NSString stringWithFormat:@"Características do Imóvel: \n\n"];
    
    if ([tags.armarioCozinha boolValue]) {
        str = [str stringByAppendingString:@"Armário de Cozinha \n"];
    }
    
    if ([tags.armarioEmbutido boolValue]) {
        str = [str stringByAppendingString:@"Armário Embutido \n"];
    }
    
    if ([tags.varanda boolValue]) {
        str = [str stringByAppendingString:@"Varanda \n"];
    }
    
    if ([tags.wcEmpregada boolValue]) {
        str = [str stringByAppendingString:@"WC Empregada \n"];
    }
    
    if ([str isEqualToString:@"Características do Imóvel: \n\n"]) {
        str = [str stringByAppendingString:@" - "];
    }
    
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:str];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:15] range:NSMakeRange(0, [str length]-1)];
        [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15] range:NSMakeRange(0, 28)];
        
    } else {
        [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:14] range:NSMakeRange(0, [str length]-1)];
        [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14] range:NSMakeRange(0, 28)];
    }
    
    return attrStr;
    
}


- (NSAttributedString *)formatCaracteristicasAreaComum
{
    
    TagImovel *tags = self.selectedImovel.tags;
    
    NSString *str = [NSString stringWithFormat:@"Características - Áreas Comuns: \n\n"];
    
    
    if ([tags.childrenCare boolValue]) {
        str = [str stringByAppendingString:@"Children Care \n"];
    }
    
    if ([tags.churrasqueira boolValue]) {
        str = [str stringByAppendingString:@"Churrasqueira \n"];
    }
    
    if ([tags.piscina boolValue]) {
        str = [str stringByAppendingString:@"Piscina \n"];
    }
    
    if ([tags.playground boolValue]) {
        str = [str stringByAppendingString:@"Playground \n"];
    }
    
    if ([tags.quadraPoliesportiva boolValue]) {
        str = [str stringByAppendingString:@"Quadra Poliesportiva \n"];
    }
    
    if ([tags.salaGinastica boolValue]) {
        str = [str stringByAppendingString:@"Sala de Ginástica \n"];
    }
    
    if ([tags.salaoFestas boolValue]) {
        str = [str stringByAppendingString:@"Salão de Festas \n"];
    }
    
    if ([tags.salaoJogos boolValue]) {
        str = [str stringByAppendingString:@"Salão de Jogos \n"];
    }
    
    if ([tags.sauna boolValue]) {
        str = [str stringByAppendingString:@"Sauna \n"];
    }
    
    if ([tags.garagem boolValue]) {
        str = [str stringByAppendingString:@"Garagem \n"];
    }
    
    if ([str isEqualToString:@"Características - Áreas Comuns: \n\n"]) {
        str = [str stringByAppendingString:@" - "];
    }
    
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:str];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:15] range:NSMakeRange(0, str.length-1)];
        [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15] range:NSMakeRange(0, 33)];
        
    } else {
        [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:14] range:NSMakeRange(0, str.length-1)];
        [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14] range:NSMakeRange(0, 33)];
    }
    
    return attrStr;
    
}

#pragma mark - Show Proprietario
- (IBAction)showProprietario:(UIButton *)button
{
    
    // Get the storyboard named secondStoryBoard from the main bundle:
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"_Contatos_iPad" bundle:nil];
    
    // Load the initial view controller from the storyboard.
    ContatoViewController *controller = [storyboard instantiateInitialViewController];
    controller.selectedCliente = self.selectedImovel.cliente;
    
    //[self.superNavigationController pushViewController:controller animated:YES];
    // substituído o pushViewController pelo setViewControllers por causa de problema de memória.
    [self.superNavigationController setViewControllers:@[controller] animated:YES];
    
}

- (IBAction)showProprietarioIphone:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Contatos_iPhone" bundle:nil];
    UINavigationController *navController = [storyboard instantiateInitialViewController];
    ListaContatosViewController *listaContatosController = [navController.childViewControllers firstObject];
    listaContatosController.selectedContatoNavigationIphone = self.selectedImovel.cliente;
    
    UINavigationController *navContatoController = [self.tabBarController.viewControllers firstObject];
    [navContatoController setViewControllers:@[listaContatosController]];
    
    self.tabBarController.selectedIndex = 0;
    
}



#pragma mark - Show Documents
- (IBAction)showDocuments:(UIButton *)button
{
    [self performSegueWithIdentifier:@"DocumentosImovelSegue" sender:nil];
}



#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"AcompanhamentoImovelSegue"]) {
        
        // role check
//        NSString *role = [Utils valueFromDefaults:CURRENT_ROLE];
//        if ([role isEqualToString:@"Atendimento"]) {
//            [Utils showMessage:@"Permissão Negada!"];
//            return;
//        }
        
        [Utils trackWithName:@"Clicou em acompanhamento - Imovel"];
        [Utils trackFlurryWithName:@"Clicou em acompanhamento - Imovel"];

        
        AcompanhamentoImovelViewController *controller = segue.destinationViewController;
        controller.selectedImovel = self.selectedImovel;
        
    } else if([segue.identifier isEqualToString:@"DocumentosImovelSegue"]) {
        
        [Utils trackWithName:@"Clicou em documentos - Imovel"];
        [Utils trackFlurryWithName:@"Clicou em documentos - Imovel"];

        
        DocumentosImovelViewController *controller = segue.destinationViewController;
        controller.selectedImovel = self.selectedImovel;
    }
}

- (IBAction)barClick:(id)sender
{
    [Utils trackWithName:@"Clicou na barra de negociacao - Imovel"];
    [Utils trackFlurryWithName:@"Clicou na barra de negociacao - Imovel"];
    
}


#pragma mark - Expand sections

- (IBAction)expandDescricao:(id)sender
{
    [UIView animateWithDuration:0.5 animations:^{
        
        if (![[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            self.caracteristicasAreaComumTextView.hidden = YES;
            self.caracteristicasImovelTextView.hidden = YES;
            self.detalhesTextView.hidden = YES;
            
            self.expandDescricaoButton.enabled = NO;
            self.expandDetalhesButton.enabled = YES;
            self.expandOutrosButton.enabled = YES;
            
            CGRect newRect;
            
            // Move outrosView.
            newRect = self.outrosView.frame;
            newRect.origin.y = self.documentosButton.frame.origin.y - 35;
            self.outrosView.frame = newRect;
            
            // Move detalhesView.
            newRect = self.detalhesView.frame;
            newRect.origin.y = self.outrosView.frame.origin.y - 35;
            self.detalhesView.frame = newRect;
            
            // Move descricaoTextView
            newRect = self.descricaoTextView.frame;
            newRect.origin.y = self.descricaoView.frame.origin.y + 31;
            newRect.size.height = self.detalhesView.frame.origin.y - newRect.origin.y;
            self.descricaoTextView.frame = newRect;
            
        }
        
    } completion:^(BOOL finished) {
        self.descricaoTextView.hidden = NO;
    }];
}


- (IBAction)expandDetalhes:(id)sender
{
    [UIView animateWithDuration:0.5 animations:^{
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            self.expandDetalhesButton.enabled = NO;
            self.expandOutrosButton.enabled = YES;
            
            // Expand DetalhesTextView
            self.detalhesTextView.frame = CGRectMake(4.0f, 270.0f, 509.0f, 160.0f);
            
            
            // Move outrosView
            self.outrosView.frame = CGRectMake(0.0f, 427.0f, 513.0f, 45.0f);
            
            // Move caracteristicasAreaComumTextView.
            self.caracteristicasImovelTextView.frame = CGRectMake(4.0f, 473.0f, 253.0f, 10.0f);
            
            // Move caracteristicasImovelTextView.
            
            self.caracteristicasAreaComumTextView.frame = CGRectMake(259.0f, 473.0f, 254.0f, 10.0f);
            
        } else {
            
            [self expandDetalhesIphone];
        }
        
        
    } completion:^(BOOL finished) {
        self.detalhesTextView.hidden = NO;
    }];
}


- (void)expandDetalhesIphone
{
    self.caracteristicasAreaComumTextView.hidden = YES;
    self.caracteristicasImovelTextView.hidden = YES;
    self.descricaoTextView.hidden = YES;
    
    self.expandDetalhesButton.enabled = NO;
    self.expandOutrosButton.enabled = YES;
    self.expandDescricaoButton.enabled = YES;
    
    CGRect newRect;
    
    // Move detalhesView.
    newRect = self.detalhesView.frame;
    newRect.origin.y = self.descricaoView.frame.origin.y + 35;
    self.detalhesView.frame = newRect;
    
    // Move outrosView.
    newRect = self.outrosView.frame;
    newRect.origin.y = self.documentosButton.frame.origin.y - 35;
    self.outrosView.frame = newRect;
    
    // Move detalhesTextView
    newRect = self.detalhesTextView.frame;
    newRect.origin.y = self.detalhesView.frame.origin.y + 31;
    newRect.size.height = self.outrosView.frame.origin.y - newRect.origin.y;
    self.detalhesTextView.frame = newRect;
}

- (IBAction)expandOutros:(id)sender
{
    [UIView animateWithDuration:0.5 animations:^{
        
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            self.expandDetalhesButton.enabled = YES;
            self.expandOutrosButton.enabled = NO;
            
            // Short DetalhesTextView
            self.detalhesTextView.frame = CGRectMake(4.0f, 270.0f, 509.0f, 10.0f);
            
            // Move outrosView
            self.outrosView.frame = CGRectMake(0.0f, 281.0f, 513.0f, 45.0f);
            
            // Move caracteristicasImovelTextView.
            self.caracteristicasImovelTextView.frame = CGRectMake(4.0f, 334.0f, 253.0f, 149.0f);
            
            // Move caracteristicasAreaComumTextView.
            self.caracteristicasAreaComumTextView.frame = CGRectMake(254.0f, 334.0f, 254.0f, 149.0f);
            
            
            
        } else {
            
            
            self.detalhesTextView.hidden = YES;
            self.descricaoTextView.hidden = YES;
            
            self.expandOutrosButton.enabled = NO;
            self.expandDescricaoButton.enabled = YES;
            self.expandDetalhesButton.enabled = YES;
            
            
            CGRect newRect;
            
            // Move detahesVew.
            newRect = self.detalhesView.frame;
            newRect.origin.y = self.descricaoView.frame.origin.y + 35;
            self.detalhesView.frame = newRect;
            
            // Move outrosView.
            newRect = self.outrosView.frame;
            newRect.origin.y = self.detalhesView.frame.origin.y + 35;
            self.outrosView.frame = newRect;
            
            // Move caracteristicasAreaComumTextView.
            newRect = self.caracteristicasAreaComumTextView.frame;
            newRect.origin.y = self.outrosView.frame.origin.y + 31;
            newRect.size.height = self.documentosButton.frame.origin.y - newRect.origin.y;
            self.caracteristicasAreaComumTextView.frame = newRect;
            
            // Move caracteristicasImovelTextView.
            newRect = self.caracteristicasImovelTextView.frame;
            newRect.origin.y = self.outrosView.frame.origin.y + 31;
            newRect.size.height = self.documentosButton.frame.origin.y - newRect.origin.y;
            self.caracteristicasImovelTextView.frame = newRect;
            
            
        }
        
        
    } completion:^(BOOL finished) {
        self.caracteristicasAreaComumTextView.hidden = NO;
        self.caracteristicasImovelTextView.hidden = NO;
    }];
}


@end
