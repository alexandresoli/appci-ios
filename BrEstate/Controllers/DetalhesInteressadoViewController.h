//
//  DetalhesInteressadoViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 1/22/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Cliente;
@interface DetalhesInteressadoViewController : BaseViewController

@property (nonatomic, weak) UINavigationController *superNavigationController;

- (void)loadPerfil:(Cliente *)cliente;

@end
