//
//  DetalhesInteressadoViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 1/22/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "DetalhesInteressadoViewController.h"
#import "PortfolioViewController.h"
#import "Cliente.h"
#import "PerfilCliente.h"
#import "Valor.h"
#import "Area.h"
#import "TipoImovel.h"
#import "Estado.h"
#import "Cidade.h"
#import "Bairro.h"
#import "ListaImoveisViewController.h"

@interface DetalhesInteressadoViewController ()

@property (nonatomic, weak) IBOutlet UITextView *buscaTextView;
@property (nonatomic, weak) IBOutlet UITextView *outrosTextView;
@property (nonatomic, strong) Cliente *selectedCliente;

@property (nonatomic, strong) UITextView *activeTextView;

@end

@implementation DetalhesInteressadoViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Tela Detalhes Interessado"];
    [Utils trackFlurryWithName:@"Tela Detalhes Interessado"];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)loadPerfil:(Cliente *)cliente
{
    
    self.selectedCliente = cliente;
    
    PerfilCliente *perfil = cliente.perfil;
    
    NSString *str = @"";
    
    if ([perfil.quartos intValue] > 0) {
        str = [str stringByAppendingString:[NSString stringWithFormat:@"Quartos: %@\n",perfil.quartos]];
    }
    
    if (perfil.valor != nil) {
        
        
        Valor *valor;
        if(perfil.tipoPerfil != nil) {
            
            NSPredicate *predicate;
            
            if ([perfil.tipoPerfil intValue] == 1) {
                
                predicate = [NSPredicate predicateWithFormat:@"tipo.codigo = 1 AND codigo = %@",perfil.valor];
                
            } else if ([perfil.tipoPerfil intValue] == 2) {
                
                predicate = [NSPredicate predicateWithFormat:@"tipo.codigo = 2 AND codigo = %@",perfil.valor];
            }
            
            valor = [Valor MR_findFirstWithPredicate:predicate sortedBy:@"codigo" ascending:YES inContext:self.moc];
            
        }
        
        
        str = [str stringByAppendingString:[NSString stringWithFormat:@"Valor:      %@\n",valor.descricao]];
        
        
    }
    
    
    if ([perfil.area intValue] > 0) {
        str = [str stringByAppendingString:[NSString stringWithFormat:@"Área:       %@ m²\n",[perfil.area stringValue]]];
    }
    
    if ([perfil.tipoImovel intValue] > 0) {
        NSPredicate *predicateTipoImovel = [NSPredicate predicateWithFormat:@"codigo = %@", perfil.tipoImovel];
        NSArray *arrayTipoImovel = [TipoImovel MR_findAllWithPredicate:predicateTipoImovel inContext:self.moc];
        
        TipoImovel *tipoImovel = [arrayTipoImovel firstObject];
        str = [str stringByAppendingString:[NSString stringWithFormat:@"Tipo:       %@ \n", tipoImovel.descricao]];
    }
    
    
    if (perfil.estado.length > 0) {
        str = [str stringByAppendingString:[NSString stringWithFormat:@"Estado:   %@\n",perfil.estado]];
    }
    
    if (perfil.cidade.length > 0) {
        str = [str stringByAppendingString:[NSString stringWithFormat:@"Cidade:   %@\n\n",perfil.cidade]];
    }
    
    if ([perfil.bairro allObjects].count > 0) {
        
        NSString *bairros = @"Bairros: \n";
        for (Bairro *ojb in [perfil.bairro allObjects]) {
            bairros = [bairros stringByAppendingString:[NSString stringWithFormat:@"  %@ \n", ojb.nome]];
        }
        
        str = [str stringByAppendingString:bairros];
    }
    
    
    self.buscaTextView.text = str;
    
    
}

- (IBAction)match:(id)sender
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        // Get the storyboard named secondStoryBoard from the main bundle:
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"_Portfolio_iPad" bundle:nil];
        
        // Load the initial view controller from the storyboard.
        PortfolioViewController *controller = [storyboard instantiateInitialViewController];
        
        controller.predicate = [self createPredicate];
        
        //[self.superNavigationController pushViewController:controller animated:YES];
        // substituído o pushViewController pelo setViewControllers por causa de problema de memória.
        [self.superNavigationController setViewControllers:@[controller] animated:YES];
        
    } else {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Portfolio_iPhone" bundle:nil];
        
        UINavigationController *navController = [storyboard instantiateInitialViewController];
        ListaImoveisViewController *listaImoveisController = [navController.childViewControllers firstObject];
        listaImoveisController.selectedPredicate = [self createPredicate];
        
        UINavigationController *navImovelController = [self.tabBarController.viewControllers objectAtIndex:2];
        [navImovelController setViewControllers:@[listaImoveisController]];
        
        self.tabBarController.selectedIndex = 2;
    }
}


- (NSPredicate *)createPredicate
{
    
    NSMutableArray *predicateArray = [NSMutableArray new];
    
    PerfilCliente *perfil = self.selectedCliente.perfil;
    
    if (perfil.quartos != nil && [perfil.quartos intValue] > 0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"quartos = %@", perfil.quartos]];
    }

    if (perfil.area != nil && [perfil.area intValue] > 0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"areaUtil >= %@",perfil.area]];
    }
    
    if (perfil.tipoImovel != nil && [perfil.tipoImovel intValue] > 0) {
        NSPredicate *predicateTipoImovel = [NSPredicate predicateWithFormat:@"codigo = %@", perfil.tipoImovel];
        NSArray *arrayTipoImovel = [TipoImovel MR_findAllWithPredicate:predicateTipoImovel inContext:self.moc];
        TipoImovel *tipoImovel = [arrayTipoImovel firstObject];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"tipo = %@",tipoImovel.descricao]];
    }
    
    if (perfil.estado != nil && ![perfil.estado isEqualToString:@""]) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"endereco.estado.nome = %@",perfil.estado]];
    }
    
    if (perfil.cidade != nil) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"endereco.cidade.nome= %@",perfil.cidade]];
    }
    
    if (perfil.bairro != nil && perfil.bairro.count > 0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"ANY endereco.bairro IN %@",perfil.bairro]];
    }
    
    
    if (perfil.valor != nil && [perfil.valor intValue] > 0) {
        
        
        Valor *valor;
        if (perfil.tipoPerfil != nil && [perfil.tipoPerfil intValue] > 0) {
            
            NSPredicate *predicate;
            
            if ([perfil.tipoPerfil intValue] == 2) { // Comprar
                predicate = [NSPredicate predicateWithFormat:@"tipo.codigo = 2 AND codigo = %@",perfil.valor];
                
            } else if ([perfil.tipoPerfil intValue] == 1) { // Alugar
                
                predicate = [NSPredicate predicateWithFormat:@"tipo.codigo = 1 AND codigo = %@",perfil.valor];
            }
            
            valor = [Valor MR_findFirstWithPredicate:predicate sortedBy:@"codigo" ascending:YES inContext:self.moc];
            
        }
        
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"valor >= %@ AND valor <= %@",valor.minimo,valor.maximo]];
    }
    
    if (perfil.tipoPerfil != nil && [perfil.tipoPerfil intValue] > 0) {
        if ([perfil.tipoPerfil intValue] == 2) { // Comprar
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"oferta = 'Venda'"]];
            
        } else if ([perfil.tipoPerfil intValue] == 1) { // Alugar
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"oferta = 'Locação'"]];
        }
    }
    
    NSPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    
    return compoundPredicate;
    
}


@end