//
//  DispositivosViewController.h
//  Appci
//
//  Created by BrEstate LTDA on 20/06/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "BaseViewController.h"

@interface DispositivosViewController : BaseViewController

@property (nonatomic, strong) NSArray *devices;
@property (nonatomic, strong) NSString *loginUser;

@end
