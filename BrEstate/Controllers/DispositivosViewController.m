//
//  DispositivosViewController.m
//  Appci
//
//  Created by BrEstate LTDA on 20/06/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "DispositivosViewController.h"
#import "DispositivoCell.h"
#import "SVProgressHUD.h"
#import "DateUtils.h"


@interface DispositivosViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end


@implementation DispositivosViewController


#pragma mark - View Life Cycle

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //[self.tableView setEditing:YES animated:YES];
}


#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.devices.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DispositivoCell *cell = (DispositivoCell *) [self.tableView dequeueReusableCellWithIdentifier:@"DispositivoCell" forIndexPath:indexPath];
    
    NSDictionary *device = [self.devices objectAtIndex:indexPath.row];

    
    NSString *registerDateText = [DateUtils stringFromDate:[DateUtils dateFromStringTimestamp:device[@"lastAccess"]] withFormat:@"dd/MM/yyyy HH:mm"];
    registerDateText = [NSString stringWithFormat:@"Último acesso em %@", registerDateText];
    
    cell.registerDate.text = registerDateText;
    
    NSString *nameDevice = [device objectForKey:@"deviceName"];
    
    cell.nameLabel.text = nameDevice;
    
    if ([nameDevice.uppercaseString rangeOfString:@"IPAD"].location == NSNotFound) {
        cell.iconImageView.image = [UIImage imageNamed:@"iphone-device-icon"];
    } else {
        cell.iconImageView.image = [UIImage imageNamed:@"ipad-device-icon"];
    }
    
    return cell;
}


#pragma mark - IBActions

- (IBAction)replaceDevice:(id)sender
{
    if (![self.tableView indexPathForSelectedRow]) {
        [Utils showMessage:@"Selecione um dispositivo."];
        return;
    }
    
    if (![Utils isConnected]) {
        [Utils showMessage:@"Verifique sua conexão com a internet."];
        
    } else {
        
        [SVProgressHUD showWithStatus:@"Gerando código de ativação. Aguarde..." maskType:SVProgressHUDMaskTypeBlack];
        
        NSIndexPath *index = [self.tableView indexPathForSelectedRow];
        NSDictionary *selectedDevice = [self.devices objectAtIndex:index.row];
        
        NSString *url = [NSString stringWithFormat:@"%@%@",[[Config instance] url], @"auth/ask-token.json"];
        
        NSDictionary *params = @{@"email": self.loginUser};
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSNumber *success = [responseObject objectForKey:@"success"];
            
            if ([success boolValue]) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setObject:selectedDevice forKey:DEVICE_FOR_SUBSTITUTION];
                    [defaults synchronize];
                    
                    // Retorna para tela de login.
                    [self performSegueWithIdentifier:@"UnwindFromDispositivosSegue" sender:self];
                });
                
            } else {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    
                    NSString *errorMessage = [responseObject objectForKey:@"message"];
                    
                    if (!errorMessage) {
                        errorMessage = @"Ocorreu um erro ao solicitar código de ativação. Tente novamente. Se o erro persistir entre em contato com suporte@brestate.com.br";
                    }
                    
                    [Utils showMessage:errorMessage];
                });
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                [Utils showMessage:[NSString stringWithFormat:@"Ocorreu um erro ao solicitar código de ativação. Tente novamente. Caso o erro persista entre em contato com suporte@brestate.com.br \nErro - %@", error]];
            });
        }];
    }
}


- (IBAction)cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end