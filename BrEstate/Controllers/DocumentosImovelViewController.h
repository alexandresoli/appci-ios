//
//  DocumentosImovelViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 1/24/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "BaseViewController.h"

@class Imovel;
@interface DocumentosImovelViewController : BaseViewController

@property (nonatomic, strong) Imovel *selectedImovel;
@end
