//
//  DocumentosImovelViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 1/24/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "DocumentosImovelViewController.h"
#import "EditarDocumentoViewController.h"
#import "AcompanhamentoCell.h"
#import "Acompanhamento.h"
#import "Imovel.h"

#import "DateUtils.h"
#import "ImageUtils.h"
#import "CTAssetsPickerController.h"
#import "SVProgressHUD.h"
#import "FotoImovel+InsertDeleteImagem.h"
#import "Acompanhamento+Firebase.h"

// Bug - http://stackoverflow.com/questions/11928885/uipopovercontroller-orientation-crash-in-ios-6
@interface UIImagePickerController (Orientation)

-(BOOL)shouldAutorotate;
- (NSUInteger)supportedInterfaceOrientations;

@end

@implementation UIImagePickerController (Orientation)

-(BOOL)shouldAutorotate
{
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    
    // ATTENTION! Only return orientation MASK values
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        return UIInterfaceOrientationMaskLandscape;
    } else {
        return UIInterfaceOrientationMaskPortrait;
    }
}

@end

@interface DocumentosImovelViewController () <UITableViewDataSource, UITableViewDelegate,NSFetchedResultsControllerDelegate,UISearchBarDelegate,UIActionSheetDelegate, SWTableViewCellDelegate,CTAssetsPickerControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;
@property (nonatomic, weak) IBOutlet UIButton *photoButton;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, strong) Acompanhamento *selectedAcompanhamento;

@property (nonatomic, strong) UIPopoverController *popover;

@property (nonatomic, strong) UIActionSheet *actionSheetDelete;

@end

@implementation DocumentosImovelViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // add model changes notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDataModelChange:) name:NSManagedObjectContextObjectsDidChangeNotification object:self.moc];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Tela Acompanhamento Cliente"];
    [Utils trackFlurryWithName:@"Tela Acompanhamento Cliente"];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Notification Center
- (void)handleDataModelChange:(NSNotification *)note
{
    NSSet *updatedObjects = [[note userInfo] objectForKey:NSUpdatedObjectsKey];
    NSSet *deletedObjects = [[note userInfo] objectForKey:NSDeletedObjectsKey];
    NSSet *insertedObjects = [[note userInfo] objectForKey:NSInsertedObjectsKey];
    
    if ([updatedObjects containsMemberOfClass:[Acompanhamento class]] ||
        [insertedObjects containsMemberOfClass:[Acompanhamento class]] ||
        [deletedObjects containsMemberOfClass:[Acompanhamento class]])
    {
        [_fetchedResultsController performFetch:nil];
        [self.tableView reloadData];
    }
    
    
}


- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (_fetchedResultsController != nil)
    {
        return _fetchedResultsController;
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"imovel = %@ AND arquivo != nil",self.selectedImovel];
    
    _fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Acompanhamento" withSortKey:@"timeStamp" withSectionName:@"sectionIdentifier" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
    
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
    
}


#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AcompanhamentoCell";
    AcompanhamentoCell *cell = (AcompanhamentoCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // SWTableViewCell
    //    [cell setCellHeight:cell.frame.size.height];
    //    cell.containingTableView = tableView;
    
    cell.backgroundColor = [UIColor colorWithRed:0.933 green:0.937 blue:0.937 alpha:1];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


- (void)configureCell:(AcompanhamentoCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Acompanhamento *acompanhamento = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.descricao.text = acompanhamento.titulo;;
    
    NSString *dia = [DateUtils stringFromDate:acompanhamento.timeStamp withFormat:@"dd"];
    NSString *hora = [DateUtils stringFromDate:acompanhamento.timeStamp withFormat:@"HH:mm"];
    
    cell.dia.text = dia;
    cell.hora.text = hora;
    
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        // right swipe
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"swipe-apagar"]];
        
    } else {
        // right swipe
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"swipe-apagar"] scaledToSize:CGSizeMake(42, 55)]];
        
    }
    
    cell.rightUtilityButtons = rightUtilityButtons;
    cell.delegate = self;
    
    
    // Exibe o ícone de anexo caso o acompanhamento possua um arquivo.
    if (acompanhamento.arquivo != nil) {
        cell.anexoImagem.hidden = NO;
    } else {
        cell.anexoImagem.hidden = YES;
    }
}


- (NSString *)formatTitle:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> theSection = [[self.fetchedResultsController sections] objectAtIndex:section];
    
    /*
     Section information derives from an event's sectionIdentifier, which is a string representing the number (year * 1000) + month.
     To display the section title, convert the year and month components to a string representation.
     */
    static NSDateFormatter *formatter = nil;
    
    if (!formatter)
    {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setCalendar:[NSCalendar currentCalendar]];
        
        NSString *formatTemplate = [NSDateFormatter dateFormatFromTemplate:@"MMMM YYYY" options:0 locale:[NSLocale currentLocale]];
        [formatter setDateFormat:formatTemplate];
    }
    
    NSInteger numericSection = [[theSection name] integerValue];
    NSInteger year = numericSection / 1000;
    NSInteger month = numericSection - (year * 1000);
    
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    dateComponents.year = year;
    dateComponents.month = month;
    NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
    
    
    NSString *titleString = [formatter stringFromDate:date];
    NSString *firstLetter = [[titleString substringToIndex:1] capitalizedString];
    titleString = [titleString stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstLetter];
    
    
    return [NSString stringWithFormat:@"   %@",titleString];
}




- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *labelView = [[UILabel alloc] init];
    labelView.textAlignment = NSTextAlignmentLeft;
    labelView.backgroundColor = [UIColor colorWithRed:0.851 green:0.851 blue:0.851 alpha:1];
    labelView.textColor = [UIColor blackColor];
    labelView.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
    labelView.text = [self formatTitle:section];
    
    return labelView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


#pragma mark - SWTableViewCellDelegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    self.selectedAcompanhamento = [_fetchedResultsController objectAtIndexPath:indexPath];
    
    _actionSheetDelete =[[UIActionSheet alloc] initWithTitle:@"Confirma remoção?" delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:@"Remover" otherButtonTitles:nil, nil];
    
    [_actionSheetDelete showInView:cell];
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    self.selectedAcompanhamento = [_fetchedResultsController objectAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"EditarAcompanhamentoSegue" sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    searchBar.showsCancelButton = YES;
    
    return YES;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self filterContentForSearchText:searchText];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self filterContentForSearchText:searchBar.text];
    searchBar.showsCancelButton = NO;
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    searchBar.text = nil;
    searchBar.showsCancelButton = NO;
    [self filterContentForSearchText:nil];
    [searchBar resignFirstResponder];
}

// The method to change the predicate of the FRC
- (void)filterContentForSearchText:(NSString*)searchText
{
    NSString *query = searchText;
    
    if (query && query.length) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"imovel = %@ AND titulo contains[c] %@", self.selectedImovel, query];
        
        [_fetchedResultsController.fetchRequest setPredicate:predicate];
        [_fetchedResultsController.fetchRequest setFetchLimit:100];
        
    } else {
        
        _fetchedResultsController = nil;
        
    }
    
    [self.fetchedResultsController performFetch:nil];
    
    [self.tableView reloadData];
}



#pragma mark - Add Photo
- (IBAction)addPhoto:(UIButton *)button
{
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Selecione"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancelar"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Tirar Foto",@"Foto Existente",nil];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        actionSheet.actionSheetStyle = UIActionSheetStyleAutomatic;
        
        [actionSheet showFromRect: _photoButton.frame inView:self.view animated:YES];
        
    } else {
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    }
    
}


- (void)removeDocument
{
    [_selectedAcompanhamento deleteFirebase];
    
    [_selectedAcompanhamento MR_deleteInContext:self.moc];
    
    [self.moc MR_saveToPersistentStoreAndWait];
    
    NSError *error;
    [self.fetchedResultsController performFetch:&error];
    
    [self.tableView reloadData:YES];
    
    
    
    
}

#pragma mark - UIActionSheetDelegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch (buttonIndex) {
        case 0:
            
            if ([actionSheet isEqual:_actionSheetDelete]) {
                [self removeDocument];
            } else {
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self takePhoto];
                }];
            }
            break;
            
        case 1:
            if (![actionSheet isEqual:_actionSheetDelete]) {
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self cameraRoll:@"Fotos"];
                }];
            }
            
            break;
            
        default:
            return;
    }
}


#pragma mark - Photo
- (void)takePhoto
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    
#if TARGET_IPHONE_SIMULATOR
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary | UIImagePickerControllerSourceTypeSavedPhotosAlbum;
#else
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.editing = YES;
    imagePickerController.allowsEditing = YES;
    imagePickerController.showsCameraControls = YES;
#endif
    imagePickerController.delegate = (id)self;
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (void)cameraRoll:(NSString *)tipo
{
    CTAssetsPickerController *picker = [[CTAssetsPickerController alloc] init];
    picker.delegate = self;
    
    if([tipo isEqualToString:@"Fotos"]) {
        picker.assetsFilter = [ALAssetsFilter allPhotos];
    } else if([tipo isEqualToString:@"Videos"]) {
        picker.assetsFilter = [ALAssetsFilter allVideos];
    }
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        _popover = [[UIPopoverController alloc] initWithContentViewController:picker];
        
        [_popover presentPopoverFromRect:_photoButton.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    } else {
        [self presentViewController:picker animated:YES completion:nil];
    }
    
    
}

#pragma mark - CTAssetsPickerControllerDelegate
- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    [SVProgressHUD showWithStatus:@"Processando..." maskType:SVProgressHUDMaskTypeBlack];
    
    NSArray *array = @[picker, assets];
    
    // Execução em método separado para que seja exibido o SVProgress e
    // por causa do iPhone 4 onde executando em outras threads o processo era encerrado sem concluir.
    [self performSelector:@selector(processPhotos:) withObject:array afterDelay:0.1];
}


- (void)processPhotos:(NSArray *)pickerAndAssets
{
    CTAssetsPickerController *picker = [pickerAndAssets firstObject];
    NSArray *assets = [pickerAndAssets lastObject];
    
    for (ALAsset *asset in assets) {
        
        if([[asset valueForProperty:@"ALAssetPropertyType"] isEqualToString:@"ALAssetTypePhoto"]) {
            
            @autoreleasepool {
                
                ALAssetRepresentation *assetRepresentation = [asset defaultRepresentation];
                CGImageRef imageRef = [assetRepresentation fullResolutionImage];
                
                UIImage *image = [UIImage imageWithCGImage:imageRef scale:assetRepresentation.scale orientation:(UIImageOrientation)assetRepresentation.orientation];
                imageRef = nil;
                
                [self savePhoto:image];
                
            }
        }
        
    }
    
    [Utils postEntitytNotification:@"FotoImovel"];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    [_popover dismissPopoverAnimated:YES];
    
    [SVProgressHUD dismiss];
}


- (void)assetsPickerControllerDidCancel:(CTAssetsPickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    [_popover dismissPopoverAnimated:YES];
}


- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker shouldSelectAsset:(ALAsset *)asset
{
    if (picker.selectedAssets.count >= 10)
    {
        UIAlertView *alertView =
        [[UIAlertView alloc] initWithTitle:@"Atenção"
                                   message:@"Selecione no máximo 10 fotos por vez."
                                  delegate:nil
                         cancelButtonTitle:nil
                         otherButtonTitles:@"OK", nil];
        
        [alertView show];
        
        return NO;
        
    } else {
        return YES;
    }
}


#pragma mark - UIImagePickerControllerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    if([mediaType isEqualToString:@"public.image"]) {
        
        UIImage *sourceImage = [info objectForKey:UIImagePickerControllerEditedImage];
        
        [self savePhoto:sourceImage];
        
        [Utils postEntitytNotification:@"Acompanhamento"];
        
    }
    
    [_fetchedResultsController performFetch:nil];
    [_tableView reloadData:YES];
    [picker dismissViewControllerAnimated:YES completion:nil];
    [_popover dismissPopoverAnimated:YES];
    
}


- (void)savePhoto:(UIImage *)image
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"imovel = %@",_selectedImovel];
    long codigo = [Acompanhamento MR_countOfEntitiesWithPredicate:predicate inContext:self.moc]+1;
    
    Acompanhamento *acompanhamento = [Acompanhamento MR_createInContext:self.moc];
    acompanhamento.codigo = [acompanhamento getUniqueCode]; //[NSNumber numberWithLong:codigo];
    acompanhamento.titulo =  [NSString stringWithFormat:@"Documento %ld",codigo] ;
    acompanhamento.version = [NSDate date];
    acompanhamento.timeStamp = [NSDate date];
    acompanhamento.imovel = _selectedImovel;
    acompanhamento.arquivo = UIImageJPEGRepresentation(image, 0.6);
    acompanhamento.extensao = @"png";
    acompanhamento.filename = [NSString stringWithFormat:@"%@-%@",[DateUtils stringTimestampFromDate:[NSDate date]], acompanhamento.codigo];;
    [acompanhamento saveFirebase];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [acompanhamento uploadToS3];
    });
    
    [self saveMOC];
    
}


#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    EditarDocumentoViewController *controller = segue.destinationViewController;
    if ([segue.identifier isEqualToString:@"EditarAcompanhamentoSegue"]) {
        
        [Utils trackWithName:@"Tela de edição de documento - imóvel"];
        [Utils trackFlurryWithName:@"Tela de edição de documento - imóvel"];
        
        controller.selectedAcompanhamento = self.selectedAcompanhamento;
    }
}


@end
