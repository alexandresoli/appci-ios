//
//  EditarAcompanhamentoViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 1/27/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "BaseViewController.h"

@class Acompanhamento,Cliente,Imovel,Negociacao;
@interface EditarAcompanhamentoContatoViewController : BaseViewController

@property (nonatomic, strong) Acompanhamento *selectedAcompanhamento;
@property (nonatomic, strong) Cliente *selectedCliente;
@property (nonatomic, strong) Negociacao *selectedNegociacao;

@end
