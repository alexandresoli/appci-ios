//
//  EditarAcompanhamentoViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 1/27/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "EditarAcompanhamentoContatoViewController.h"

#import "Negociacao.h"
#import "Acompanhamento.h"
#import "Cliente.h"
#import "Acompanhamento+Firebase.h"

@import QuickLook;


@interface EditarAcompanhamentoContatoViewController () <QLPreviewControllerDataSource, QLPreviewControllerDelegate>

@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *abrirArquivoButton;
@property (nonatomic) CGRect textViewDefaultRect;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *salvarAcompanhamentoButton;

@end

@implementation EditarAcompanhamentoContatoViewController


#pragma mark - IBActions

- (IBAction)back:(id)sender
{
    
    [self save];

    [self.navigationController popViewControllerAnimated:YES];
    
}


- (IBAction)abrirArquivo:(id)sender
{
    if (self.selectedAcompanhamento.arquivo != nil) {
        
        QLPreviewController *previewController=[[QLPreviewController alloc]init];
        previewController.delegate = self;
        previewController.dataSource = self;
        [self presentViewController:previewController animated:YES completion:nil];
    }
}


#pragma mark - IBActions Acompanhamento Imóvel iPhone

- (IBAction)closeAcompanhamentoImovelIphone:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)saveAcompanhamentoImovelIphone:(id)sender
{
    [self save];
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - View Life Cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _textView.keyboardAppearance = UIKeyboardAppearanceDark;
    [self loadAcompanhamento];
    
    if (self.selectedAcompanhamento.arquivo != nil) {
        self.abrirArquivoButton.hidden = NO;
    } else {
        self.abrirArquivoButton.hidden = YES;
    }
    
    // Observers do teclado para manipular o tamamho do textView durante a edição.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    // Guarda o tamanho original do textView para utilizar no redimensionamento ao exibir o teclado.
    self.textViewDefaultRect = self.textView.frame;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Tela Editar/Adicionar Acompanhamento"];
    [Utils trackFlurryTimedWithName:@"Tela Editar/Adicionar Acompanhamento"];
    
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [Flurry endTimedEvent:@"Tela Editar/Adicionar Acompanhamento" withParameters:nil];
}


#pragma mark - Tratamento para o textView quando o teclado é exibido.

// Quando o teclado aparece o tamanho do texView é reduzido para que seja possível acessar todo o conteúdo.
- (void)keyboardDidShow:(NSNotification *)notification
{
    [UIView beginAnimations:nil context:nil];
    CGRect keyboradRect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect newRect = self.textViewDefaultRect;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        // No iPad não tem tabBar, então usa o tamanho do botão que simula o tabbar mais os espaços que
        //  exitem entre o botão e a base do textView (57 + 20).
        CGFloat keyboardHeight;
        if (IS_OS_8_OR_LATER) {
            keyboardHeight = keyboradRect.size.height;
        } else {
            keyboardHeight = keyboradRect.size.width; // Em landscape no iOS7 a altura e largura do teclado vem trocada.
        }
        newRect.size.height -= (keyboardHeight - 115);
        
    } else {
        newRect.size.height -= (keyboradRect.size.height - (self.tabBarController.tabBar.frame.size.height - 10));
    }
    
    self.textView.frame = newRect;
    [UIView commitAnimations];
}


// Quando o teclado desaparece o textView volta para seu tamanho original.
- (void)keyboardWillHide:(NSNotification *)notification
{
    [UIView beginAnimations:nil context:nil];
    self.textView.frame = self.textViewDefaultRect;
    [UIView commitAnimations];
}


#pragma mark - QLPreviewControllerDataSource

- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller
{
    return 1;
}


- (id <QLPreviewItem>)previewController:(QLPreviewController *)controller previewItemAtIndex:(NSInteger)index
{
    
    NSString *fileName = [NSString stringWithFormat:@"arquivo.%@",self.selectedAcompanhamento.extensao];
    
    NSURL *url = [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] URLByAppendingPathComponent:fileName];
    [self.selectedAcompanhamento.arquivo writeToURL:url atomically:YES];
    return url;
}


#pragma mark - Load Acompanhamento

- (void)loadAcompanhamento
{
    _textView.text = _selectedAcompanhamento.titulo;
}


#pragma mark - Save

- (void)save
{
    if (self.selectedAcompanhamento != nil) {
        
        // editing existent
        _selectedAcompanhamento.titulo = _textView.text;
        _selectedAcompanhamento.version = [NSDate date];
        _selectedAcompanhamento.pendingUpdateParse = [NSNumber numberWithBool:YES];
        
        
    } else {
        
        if (_textView.text.length > 0) {
            
            if (self.selectedNegociacao != nil) {
                self.selectedCliente = self.selectedNegociacao.cliente;
            }
            
            // adding new
            Acompanhamento *acompanhamento = [Acompanhamento MR_createInContext:self.moc];
            acompanhamento.codigo = [acompanhamento getUniqueCode]; //[NSNumber numberWithLong:codigo];
            acompanhamento.cliente = self.selectedCliente;
            acompanhamento.imovel = self.selectedNegociacao.imovel;
            acompanhamento.timeStamp = [NSDate date];
            acompanhamento.titulo = _textView.text;
            acompanhamento.version = [NSDate date];
            _selectedAcompanhamento = acompanhamento;
            
        }
    }
    
    // Firebase
    [_selectedAcompanhamento saveFirebase];
    
    [self saveMOC];
    
    // Parse SDK
//    [_selectedAcompanhamento saveParse:self.moc];
    
}

@end
