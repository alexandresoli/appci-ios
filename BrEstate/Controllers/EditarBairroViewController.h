//
//  EditarBairroViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 12/7/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "BaseTableViewController.h"

@class Bairro;
@interface EditarBairroViewController : BaseTableViewController

@property (nonatomic, weak) NSString *selectedCidade;
@property (nonatomic, weak) Bairro *selectedBairro;

@end
