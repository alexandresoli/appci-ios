//
//  EditarBairroViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 12/7/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "EditarBairroViewController.h"
#import "Bairro.h"
#import "Cidade.h"
#import "Utils.h"
#import "Bairro+Firebase.h"


@interface EditarBairroViewController ()

@property (nonatomic, weak) IBOutlet UILabel *cidadeLabel;
@property (nonatomic, weak) IBOutlet UITextField *nomeTextField;

@end

@implementation EditarBairroViewController

@synthesize cidadeLabel,nomeTextField,selectedCidade;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    cidadeLabel.text = selectedCidade;
    nomeTextField.text = self.selectedBairro.nome;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Tela Adicionar Bairro"];
    [Utils trackFlurryWithName:@"Tela Adicionar Bairro"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [Utils removeRedCorner:textField];
}

- (IBAction)save:(id)sender
{
    
    if ([self formIsValid]) {
        
        Bairro *bairro;
        if (self.selectedBairro != nil) { // Edit
            
            bairro = self.selectedBairro;
            bairro.pendingUpdateParse = [NSNumber numberWithBool:YES];
            
        } else { // New
            
            bairro = [Bairro MR_createInContext:self.moc];
            bairro.codigo = [bairro getUniqueCode]; //[NSNumber numberWithLong:codigo];
            bairro.cidade = selectedCidade;
            
            
        }
        
        bairro.nome = nomeTextField.text;
        
        bairro.version = [NSDate date];
        
        // Firebase
        [bairro saveFirebase];
        
        [self saveMOC];
        
        // parse save
//        [bairro saveParse:self.moc];
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    
}

- (BOOL)formIsValid
{
    
    [self resignKeyboard];
    
    BOOL isValid = YES;
    
    if (nomeTextField.text.length == 0) {
        [Utils addRedCorner:nomeTextField];
        isValid = NO;
    }
    
    return isValid;
    
}


@end
