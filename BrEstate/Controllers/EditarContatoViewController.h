//
//  AdicionarContatoViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/12/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Cliente,Bairro,SubtipoCliente,Imovel;

@interface EditarContatoViewController : BaseTableViewController

@property (nonatomic, weak) Cliente *selectedCliente;

@property (nonatomic, strong) Bairro *selectedBairro;
@property (nonatomic, strong) SubtipoCliente *selectedTipo;
@property (nonatomic, strong) Imovel *selectedImovel;
@end
