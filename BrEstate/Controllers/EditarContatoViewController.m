//
//  AdicionarContatoViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/12/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "EditarContatoViewController.h"
#import "Cliente.h"
#import "Endereco.h"
#import "Estado.h"
#import "Cidade.h"
#import "Bairro.h"
#import "SubtipoCliente.h"
#import "TipoCliente.h"
#import "PerfilCliente.h"
#import "EstadosViewController.h"
#import "CidadesViewController.h"
#import "BairrosViewController.h"
#import "TipoClienteViewController.h"
#import "SelecionarImovelViewController.h"
#import "ListaPerfilViewController.h"
#import "AddressBookHelper.h"
#import "Utils.h"
#import "CEP.h"
#import "SVProgressHUD.h"
#import "NSString+Custom.h"
#import "Cliente+Firebase.h"
#import "Imovel+Firebase.h"
#import "Bairro+Firebase.h"


@interface EditarContatoViewController () <UIActionSheetDelegate, UITextFieldDelegate>



@property (nonatomic, weak) IBOutlet UITextField *nomeTextField;
@property (nonatomic, weak) IBOutlet UITextField *emailTextField;
@property (nonatomic, weak) IBOutlet UITextField *telefoneTextField;
@property (nonatomic, weak) IBOutlet UITextField *telefoneExtraTextField;
@property (nonatomic, weak) IBOutlet UITextField *cepTextField;
@property (nonatomic, weak) IBOutlet UITextField *logradouroTextField;
@property (nonatomic, weak) IBOutlet UITextField *complementoTextField;
@property (nonatomic, weak) IBOutlet UITextField *bairroTextField;

@property (nonatomic, weak) IBOutlet UILabel *estadoLabel;
@property (nonatomic, weak) IBOutlet UILabel *cidadeLabel;
@property (nonatomic, weak) IBOutlet UILabel *bairroLabel;
@property (nonatomic, weak) IBOutlet UILabel *tipoLabel;
@property (nonatomic, weak) IBOutlet UILabel *imovelLabel;
@property (nonatomic, weak) IBOutlet UILabel *perfilLabel;

@property (nonatomic, weak) IBOutlet UITableViewCell *imovelCell;
@property (nonatomic, weak) IBOutlet UITableViewCell *extraPhoneCell;
@property (nonatomic, weak) IBOutlet UITableViewCell *perfilCell;

@property (nonatomic, weak) IBOutlet UIBarButtonItem *doneButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *cancelButton;

@property (nonatomic, strong) NSArray *estados;
@property (nonatomic, strong) NSArray *cidades;
@property (nonatomic, strong) NSArray *bairros;

@property (nonatomic, strong) NSString *selectedEstado;
@property (nonatomic, strong) NSString *selectedCidade;
@property (nonatomic, strong) PerfilCliente *selectedPerfil;

@property (nonatomic, strong) UIActionSheet *actionSheet;

@property (nonatomic) BOOL changedImovel;

@end

@implementation EditarContatoViewController

@synthesize nomeTextField,emailTextField,telefoneTextField,telefoneExtraTextField,logradouroTextField,cepTextField;
@synthesize imovelCell,extraPhoneCell,perfilCell;
@synthesize estadoLabel,cidadeLabel,bairroLabel,tipoLabel,imovelLabel,perfilLabel;
@synthesize selectedEstado,selectedCidade,selectedBairro,selectedTipo,selectedImovel,selectedPerfil,selectedCliente;
@synthesize doneButton;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (selectedCliente != nil) {
        self.navigationItem.title = @"Editar Contato";
        [self loadCliente];
    }
    
    doneButton.enabled = selectedCliente.nome.length > 0 ? YES : NO;
    
    
    // Register for text change in nomeTextField
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:@"UITextFieldTextDidChangeNotification" object:nomeTextField];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (![estadoLabel.text isEqualToString:selectedEstado]) {
        selectedCidade = nil;
        selectedBairro = nil;
    } else if (![cidadeLabel.text isEqualToString:selectedCidade]) {
        selectedBairro = nil;
    }
    
    estadoLabel.text = selectedEstado;
    cidadeLabel.text = selectedCidade;
    bairroLabel.text = selectedBairro.nome;
    tipoLabel.text = selectedTipo.descricao;
    
    [self checkCellVisibility];
    
    if (self.selectedImovel == nil) {
        self.imovelLabel.text = @"Nenhum";
    }
    
    [Utils trackWithName:@"Tela Editar/Adicionar Contato"];
    [Utils trackFlurryTimedWithName:@"Tela Editar/Adicionar Contato"];

}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [Flurry endTimedEvent:@"Tela Editar/Adicionar Contato" withParameters:nil];
}


- (void)checkCellVisibility
{
    
    // imovel cell
    if ([selectedTipo.tipoCliente.descricao isEqualToString:@"Proprietário"]) {
        imovelCell.hidden = NO;
        selectedPerfil = nil;
    } else {
        imovelCell.hidden = YES;
        selectedImovel = nil;
        imovelLabel.text = @"Nenhum";
    }
    
    
    // perfil cell
    if ([selectedTipo.tipoCliente.descricao isEqualToString:@"Interessado"]) {
        perfilCell.hidden = NO;
        
        if (selectedPerfil == nil) {
            perfilLabel.text = @"Nenhum";
        }
        
    } else {
        perfilCell.hidden = YES;
    }
    
    [self.tableView reloadData];
}


#pragma mark - Close
- (IBAction)close:(id)sender
{
    if (doneButton.enabled) {
        
        if (self.changedImovel) {
            [self showConfirmation];
        } else {
            [self dismiss];
        }
    } else {
        
        [self dismiss];
    }
}


- (void)showConfirmation
{
    
    if (_actionSheet == nil) {
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            
            _actionSheet = [[UIActionSheet alloc] initWithTitle:@"O contato não foi salvo!"
                                                       delegate:self
                                              cancelButtonTitle:nil
                                         destructiveButtonTitle:@"Sair"
                                              otherButtonTitles:@"Salvar",nil];
            
        } else {
            
            _actionSheet = [[UIActionSheet alloc] initWithTitle:@"O contato não foi salvo!"
                                                       delegate:self
                                              cancelButtonTitle:@"Salvar"
                                         destructiveButtonTitle:@"Sair"
                                              otherButtonTitles:nil];
            
        }
        
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        _actionSheet.actionSheetStyle = UIActionSheetStyleAutomatic;
        
        [_actionSheet showFromBarButtonItem:self.cancelButton animated:YES];
        
    } else {
        [_actionSheet showFromRect:self.view.frame inView:self.view animated:YES];
    }
    
}

#pragma mark - Dismiss
- (void)dismiss
{
    
    selectedCliente = nil;
    
    if ([self.navigationController.viewControllers count] == 1) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}


#pragma mark - UIActionSheetDelegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch (buttonIndex) {
        case 0:
            
            [self dismiss];;
            
            break;
        case 1:
            
            // save pressed
            [self save:nil];
            
            break;
        default:
            break;
    }
    
}

#pragma mark - Load Cliente
- (void)loadCliente
{
    
    nomeTextField.text = selectedCliente.nome;
    emailTextField.text = selectedCliente.email;
    telefoneTextField.text = selectedCliente.telefone;
    
    
    Endereco *endereco = selectedCliente.endereco;
    
    
    if ([endereco.cep intValue] > 0) {
        
        NSString *cep = [endereco.cep stringValue];
        
        if (cep.length == 7) {
            cep = [NSString stringWithFormat:@"0%@",cep];
        }
        
        NSMutableString *mutableString  = [cep mutableCopy];
        [mutableString insertString:@"-" atIndex:5];
        cepTextField.text = mutableString;
    }
    
    logradouroTextField.text = endereco.logradouro;
    _complementoTextField.text = endereco.complemento;
    estadoLabel.text = endereco.estado;
    cidadeLabel.text = endereco.cidade;
    bairroLabel.text = endereco.bairro.nome;
    
    selectedEstado = endereco.estado;
    selectedCidade = endereco.cidade;
    selectedBairro = endereco.bairro;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",selectedCliente.subtipo];
    selectedTipo = [SubtipoCliente MR_findFirstWithPredicate:predicate inContext:self.moc];
    
    
    if (selectedCliente.imovel != nil) {
        selectedImovel = [[selectedCliente.imovel allObjects] firstObject];
    }
    imovelLabel.text = selectedImovel.identificacao;
    
    selectedPerfil = selectedCliente.perfil;
    perfilLabel.text = selectedCliente.perfil.descricao;
    
    
    if (selectedCliente.telefoneExtra != nil) {
        [self showExtraPhone:nil];
        telefoneExtraTextField.text = selectedCliente.telefoneExtra;
        
    }
    
}

#pragma mark - Save Form
- (IBAction)save:(id)sender
{
    
    
    BOOL isNew = NO;;
    
    
    Cliente *cliente = nil;
    
    // already exists
    if (selectedCliente != nil) {
        
        [Utils trackWithName:@"Novo Contato"];
        [Utils trackFlurryWithName:@"Novo Contato"];

        
        cliente = selectedCliente;
        cliente.pendingUpdateParse = [NSNumber numberWithBool:YES];
        
    } else {
        
        [Utils trackWithName:@"Editou Contato"];
        [Utils trackFlurryWithName:@"Editou Contato"];

        
//        count = [Cliente MR_countOfEntitiesWithContext:self.moc]+1;
        
        // new Cliente
        cliente = [Cliente MR_createInContext:self.moc];
        cliente.codigo = [cliente getUniqueCode]; //[NSNumber numberWithLong:count];
        
        isNew = YES;
        
    }
    
    cliente.nome = nomeTextField.text;
    cliente.email = emailTextField.text;
    cliente.telefone = telefoneTextField.text;
    cliente.telefoneExtra = telefoneExtraTextField.text;
    cliente.subtipo = selectedTipo.codigo;
    
    if (selectedImovel != nil) {
        [cliente addImovelObject:selectedImovel];
    }
    
    cliente.perfil =  selectedPerfil;
    
    // Endereco
    if (cepTextField.text.trimmed.length > 0 || logradouroTextField.text.trimmed.length > 0 || _complementoTextField.text.trimmed.length > 0 || estadoLabel.text.trimmed.length > 0 || cidadeLabel.text.trimmed.length > 0 || bairroLabel.text.trimmed.length > 0) {
        
        Endereco *endereco = nil;
        
        if (selectedCliente.endereco != nil) {
            
            endereco = selectedCliente.endereco;
            
        } else {
            
//            count = [Endereco MR_countOfEntitiesWithContext:self.moc]+1;
            
            endereco = [Endereco MR_createInContext:self.moc];
            endereco.codigo = [endereco getUniqueCode]; //[NSNumber numberWithLong:count];
            
        }
        
        NSString *cep = [cepTextField.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
        endereco.cep = [NSNumber numberWithInt:[cep intValue]];
        
        endereco.logradouro = logradouroTextField.text;
        endereco.complemento = _complementoTextField.text;
        endereco.estado = selectedEstado;
        endereco.cidade = selectedCidade;
        endereco.bairro = selectedBairro;
        
        cliente.endereco = endereco;
        
    }
    
    cliente.version = [NSDate date];

    [self saveMOC];

    if (isNew) {
        
        AddressBookHelper *addressBookHelper = [[AddressBookHelper alloc] initWithMoc:self.moc];
        [addressBookHelper addContact:cliente];
    }
    
    // Parse Save
//    [cliente saveParse:self.moc];
    
    
    // Firebase Save
    [cliente saveFirebase];
    
    if (self.changedImovel) {
        [selectedImovel saveFirebase];
    }
    
    [self saveMOC];
    
    [Utils postEntitytNotification:@"Contato"];

    [self dismiss];
    
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.0;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([indexPath section] == 0) {
        
        if (extraPhoneCell.isHidden && indexPath.row == 3) {
            return 0.0;
        }
        
        if (imovelCell.isHidden && indexPath.row == 5) {
            return 0.0;
        }
        
        if (perfilCell.isHidden && indexPath.row == 6) {
            return 0.0;
        }
        
    }
    return 44.0;
    
}


#pragma mark - CEP Search
- (CEP *)searchCEP:(NSString *)cepToSearch
{
    
    if (![Utils isConnected] || cepToSearch.length == 0) {
        return nil;
    }
    
    
    [SVProgressHUD showWithStatus:@"Buscando CEP..." maskType:SVProgressHUDMaskTypeBlack];
    
    cepToSearch = [cepToSearch stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    
    __block CEP *cep = nil;
    
    
    NSString *url = [NSString stringWithFormat:@"%@%@?cep=%@",[[Config instance] url], @"carrega-cep.json", cepToSearch];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSNumber *success = [responseObject valueForKey:@"resultado"];
        
        if ([success boolValue]) {
            
            cep = [[CEP alloc]  init];
            cep.uf = [responseObject valueForKey:@"uf"];
            cep.cidade = [responseObject valueForKey:@"cidade"];
            cep.bairro = [responseObject valueForKey:@"bairro"];
            cep.tipoLogradouro = [responseObject valueForKey:@"tipo_logradouro"];
            cep.logradouro = [responseObject valueForKey:@"logradouro"];
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self convertCEP:cep];
            });
        }
        
        [Utils  dismissProgress];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%s - %@",__FUNCTION__,error);
        [Utils dismissProgress];
    }];
    
    
    return cep;
}

- (void)convertCEP:(CEP *)cep
{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sigla == [cd] %@", cep.uf];
    Estado *estado = [Estado MR_findFirstWithPredicate:predicate inContext:self.moc];
    estadoLabel.text = estado.nome;
    selectedEstado = estado.nome;
    
    predicate = [NSPredicate predicateWithFormat:@"nome == [cd] %@", cep.cidade];
    Cidade *cidade = [Cidade MR_findFirstWithPredicate:predicate inContext:self.moc];
    cidadeLabel.text = cidade.nome;
    selectedCidade = cidade.nome;
    
    predicate = [NSPredicate predicateWithFormat:@"nome == [cd] %@", cep.bairro];
    Bairro *bairro = [Bairro MR_findFirstWithPredicate:predicate inContext:self.moc];
    
    if (bairro == nil) {
        
//        long codigo = [Bairro MR_countOfEntitiesWithContext:self.moc]+1;
        bairro = [Bairro MR_createInContext:self.moc];
        bairro.codigo = [bairro getUniqueCode]; //[NSNumber numberWithLong:codigo];
        bairro.nome = [cep.bairro capitalizedString];
        bairro.cidade = cidade.nome;
        
        bairro.version = [NSDate date];
        
        // Firebase
        [bairro saveFirebase];
        
        [self saveMOC];
    }
    
    bairroLabel.text = bairro.nome;
    selectedBairro = bairro;
    
    // Parse SDK
//    if (bairro.parseID == nil) {
//        [bairro saveParse:self.moc];
//    }
    
    logradouroTextField.text = [NSString stringWithFormat:@"%@ %@",cep.tipoLogradouro, cep.logradouro];
    
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField isEqual:cepTextField] && cepTextField.text.length > 8) {
        
        [self searchCEP:textField.text];
        
    }
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.changedImovel = YES; //[NSNumber numberWithBool:YES];
}

// return NO to not change text
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    long length = textField.text.length;
    
    if ([textField isEqual:telefoneTextField] || [textField isEqual:telefoneExtraTextField]) {
        
        if (range.length == 1) { // backspace
            return YES;
        }
        
        if (length == 0) {
            textField.text = [NSString stringWithFormat:@"(%@",string];
            return NO;
        } else if (length == 3) {
            textField.text = [NSString stringWithFormat:@"%@) %@",textField.text,string];
            return NO;
        } else if (length == 10) {
            textField.text = [NSString stringWithFormat:@"%@-%@",textField.text,string];
            return NO;
        }
    }
    else if ([textField isEqual:cepTextField]) {
        
        
        if (range.length == 1) { // backspace
            return YES;
        }
        
        
        // only numbers
        NSCharacterSet *nonNumberSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        
        if ([string rangeOfCharacterFromSet:nonNumberSet].location != NSNotFound)
        {
            return NO;
        }
        
        
        // no more than 8 xxxxx-xxx
        if (length > 8) {
            return NO;
        }
        
        // format number
        if (length == 5) {
            
            textField.text = [NSString stringWithFormat:@"%@-%@",textField.text,string];
            return NO;
        }
        
    }
    
    return YES;
}


#pragma mark - Extra Phone
- (IBAction)showExtraPhone:(id)sender
{
    //    extraPhoneCell.hidden = extraPhoneCell.isHidden ? NO : YES;
    
    if (extraPhoneCell.isHidden) {
        extraPhoneCell.hidden = NO;
    } else {
        extraPhoneCell.hidden = YES;
        telefoneExtraTextField.text = @"";
    }
    
    [self.tableView reloadData];
}

#pragma mark - Notifiation
- (void)textFieldDidChange :(NSNotification *)notif
{
    doneButton.enabled = nomeTextField.text.length > 0 ? YES : NO;
}


#pragma mark - Segue
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"CidadesSegue"]) {
        
        if (selectedEstado == nil) {
            return NO;
        }
    } else if ([identifier isEqualToString:@"BairrosSegue"]) {
        
        if (selectedCidade == nil) {
            return NO;
        }
    }
    
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    self.changedImovel = YES; //[NSNumber numberWithBool:YES];
    
    [self resignKeyboard];
    
    if ([segue.identifier isEqualToString:@"CidadesSegue"]) {
        CidadesViewController *controller = segue.destinationViewController;
        controller.selectedEstado = selectedEstado;
    } else if ([segue.identifier isEqualToString:@"BairrosSegue"]) {
        BairrosViewController *controller = segue.destinationViewController;
        controller.selectedCidade = selectedCidade;
    } else if ([segue.identifier isEqualToString:@"PerfilSegue"]) {
        ListaPerfilViewController *controller = segue.destinationViewController;
        controller.selectedTipo = self.selectedTipo;
    } else if ([segue.identifier isEqualToString:@"SelecionarImovelSegue"]) {
        self.selectedImovel = nil;
    }
}


#pragma mark - Unwind Segue

- (IBAction)unwindFromImoveis:(UIStoryboardSegue *)segue
{
    SelecionarImovelViewController *controller = segue.sourceViewController;
    selectedImovel = controller.selectedImovel;
    
    
    if (selectedImovel.identificacao.length > 0) {
        imovelLabel.text = selectedImovel.identificacao;
    } else if (selectedImovel != nil) {
        imovelLabel.text = @"1 imóvel selecionado";
    } else {
        imovelLabel.text = @"Nenhum";
    }
    
    self.changedImovel = YES;
}

- (IBAction)unwindFromEstados:(UIStoryboardSegue *)segue
{
    EstadosViewController *controller = segue.sourceViewController;
    selectedEstado = controller.selectedEstado;
    
    selectedCidade = nil;
    selectedBairro = nil;
    cidadeLabel.text = nil;
    bairroLabel.text = nil;
    estadoLabel.text = selectedEstado;
}

- (IBAction)unwindFromCidades:(UIStoryboardSegue *)segue
{
    CidadesViewController *controller = segue.sourceViewController;
    selectedCidade = controller.selectedCidade;
    
    selectedBairro = nil;
    bairroLabel.text = nil;
    
    cidadeLabel.text = selectedCidade;
}

- (IBAction)unwindFromBairros:(UIStoryboardSegue *)segue
{
    BairrosViewController *controller = segue.sourceViewController;
    selectedBairro = controller.selectedBairro;
    
    bairroLabel.text = selectedBairro.nome;
}

- (IBAction)unwindFromPerfilCliente:(UIStoryboardSegue *)segue
{
    ListaPerfilViewController *controller = segue.sourceViewController;
    selectedPerfil = controller.selectedPerfil;
    
    perfilLabel.text = selectedPerfil.descricao;
}


- (IBAction)unwindFromTipoCliente:(UIStoryboardSegue *)segue
{
    TipoClienteViewController *controller = segue.sourceViewController;
    self.selectedTipo = controller.selectedTipo;
    
    tipoLabel.text = self.selectedTipo.descricao;
}
@end
