//
//  EditarEventoViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/25/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Evento;
@interface EditarEventoViewController : BaseTableViewController

@property (nonatomic, strong) NSString *selectedTipoEvento;
@property (nonatomic, strong) Evento *selectedEvento;
@end
