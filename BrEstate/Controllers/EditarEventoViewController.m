//
//  EditarEventoViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/25/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "EditarEventoViewController.h"
#import "Evento+AlarmeLocalNotification.h"
#import "TipoEvento.h"
#import "DateUtils.h"
#import "Imovel.h"
#import "CustomCalendar.h"
#import "AlarmsViewController.h"
#import "TipoEventoViewController.h"
#import "ParticipantesViewController.h"
#import "SelecionarImovelViewController.h"
#import "Alarme.h"
#import "Acompanhamento.h"
#import "Utils.h"
#import "Evento+Firebase.h"

@interface EditarEventoViewController () <UITextFieldDelegate,UITextViewDelegate,UIActionSheetDelegate>

@property (nonatomic, weak) IBOutlet UITextField *tituloTextField;
@property (nonatomic, weak) IBOutlet UITextField *localTextField;
@property (nonatomic, weak) IBOutlet UITextView *notasTextView;
@property (nonatomic, weak) IBOutlet UILabel *tipoLabel;
@property (nonatomic, weak) IBOutlet UILabel *dataInicioLabel;
@property (nonatomic, weak) IBOutlet UILabel *dataInicioTituloLabel;
@property (nonatomic, weak) IBOutlet UILabel *dataFimTituloLabel;
@property (nonatomic, weak) IBOutlet UILabel *alarmLabel;
@property (nonatomic, weak) IBOutlet UILabel *quantidadeContatosLabel;
@property (nonatomic, weak) IBOutlet UILabel *imovelLabel;

@property (nonatomic, weak) IBOutlet UILabel *dataFimLabel;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePickerInicio;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePickerFim;

@property BOOL datePickerInicioVisible;
@property BOOL datePickerFimVisible;

@property (nonatomic, copy) NSDate *dataInicio;
@property (nonatomic, copy) NSDate *dataFim;

@property (nonatomic, strong) NSMutableArray *selectedContatos;
@property (nonatomic, strong) Imovel *selectedImovel;
@property (nonatomic, strong) NSArray *selectedAlarmes;

@property (nonatomic, weak) IBOutlet UIView *viewDatePickerInicio;
@property (nonatomic, weak) IBOutlet UIView *viewDatePickerFim;
@property (nonatomic, strong) UIActionSheet *actionSheet;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *cancelButton;

@end

@implementation EditarEventoViewController

@synthesize datePickerInicioVisible,datePickerFimVisible;


#pragma mark - View Life Cycle

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.datePickerInicioVisible = NO;
    self.datePickerFimVisible =  NO;
    
    _notasTextView.keyboardAppearance = UIKeyboardAppearanceDark;
    
    if (self.selectedEvento != nil) {
        [self loadEvento];
    } else {
        [self configureView];
    }
    
    
    
}

- (void)configureView
{
    
    NSTimeInterval timeSince1970 = NSDate.date.timeIntervalSince1970;
    timeSince1970 -= fmod(timeSince1970, 60); // subtract away any extra seconds
    NSDate *nowWithoutSeconds = [NSDate dateWithTimeIntervalSince1970:timeSince1970];
    NSDate *oneHourLater = [DateUtils dateByAddingHours:NSDate.date hours:1];
    
    self.dataInicio = nowWithoutSeconds;
    self.dataFim = oneHourLater;
    
    _dataInicioLabel.text = [DateUtils stringFromDate:nowWithoutSeconds withFormat:@"dd/MM/yyyy HH:mm"];
    _dataFimLabel.text = [DateUtils stringFromDate:oneHourLater withFormat:@"dd/MM/yyyy HH:mm"];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Editar/Adicionar Evento (Agenda)"];
    [Utils trackFlurryTimedWithName:@"Editar/Adicionar Evento (Agenda)"];
    
    if (_selectedTipoEvento != nil) {
        _tipoLabel.text = _selectedTipoEvento;
    }
    
    if (_selectedImovel ==  nil) {
        _imovelLabel.text = @"Nenhum";
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self loadDatePickers];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [Flurry endTimedEvent:@"Editar/Adicionar Evento (Agenda)" withParameters:nil];
}


#pragma mark - Load Date Pickers

- (void)loadDatePickers
{
    CGRect rectDatePicker;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        rectDatePicker = CGRectMake(0, 0, 540, 216);
        
    } else {
        rectDatePicker = CGRectMake(0, 0, 320, 162);
    }
    
    UIDatePicker *datePickerInicio = [[UIDatePicker alloc] init];
    datePickerInicio.frame = rectDatePicker;
    datePickerInicio.datePickerMode = UIDatePickerModeDateAndTime;
    datePickerInicio.backgroundColor = [UIColor whiteColor];
    datePickerInicio.hidden = YES;
    [datePickerInicio addTarget:self action:@selector(dataInicioChanged:) forControlEvents:UIControlEventValueChanged];
    self.datePickerInicio = datePickerInicio;
    [self.viewDatePickerInicio addSubview:self.datePickerInicio];
    
    
    UIDatePicker *datePickerFim = [[UIDatePicker alloc] init];
    datePickerFim.frame = rectDatePicker;
    datePickerFim.datePickerMode = UIDatePickerModeDateAndTime;
    datePickerFim.backgroundColor = [UIColor whiteColor];
    datePickerFim.hidden = YES;
    [datePickerFim addTarget:self action:@selector(dataFimChanged:) forControlEvents:UIControlEventValueChanged];
    self.datePickerFim = datePickerFim;
    [self.viewDatePickerFim addSubview:self.datePickerFim];
}


#pragma mark - Load Evento

- (void)loadEvento
{
    Evento *evento = self.selectedEvento;
    _tituloTextField.text = evento.titulo;
    _localTextField.text = evento.local;
    _dataInicioLabel.text = [DateUtils stringFromDate:evento.dataInicio withFormat:@"dd/MM/yyyy HH:mm"];
    _dataFimLabel.text = [DateUtils stringFromDate:evento.dataFim withFormat:@"dd/MM/yyyy HH:mm"];
    _notasTextView.text = evento.notas;
    _selectedContatos = [NSMutableArray arrayWithArray:[evento.cliente allObjects]];
    _quantidadeContatosLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)[_selectedContatos count]];
    _selectedImovel = evento.imovel;
    
    if (_selectedImovel) {
        _imovelLabel.text = @"1 imóvel selecionado";
    }
    
    _selectedTipoEvento = evento.tipoEvento;
    _tipoLabel.text = evento.tipoEvento;
    NSArray *alarmes = [NSKeyedUnarchiver unarchiveObjectWithData:evento.alarmes];
    
    _alarmLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)[alarmes count]];
    _selectedAlarmes = alarmes;
    
    self.dataInicio = evento.dataInicio;
    self.dataFim = evento.dataFim;
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    [Utils removeRedCorner:textField];
    [self hideDatePickerInicio];
    [self hideDatePickerFim];
    return YES;
}
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = @"";
    return YES;
}

// called when 'return' key pressed. return NO to ignore.
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    [self hideDatePickerInicio];
    [self hideDatePickerFim];
    
    if ([_notasTextView.text isEqualToString:@"Notas"]) {
        _notasTextView.text = @"";
    }
    
    return YES;
}


#pragma mark - Data Inicio / Fim Change

- (IBAction)dataInicioChanged:(UIDatePicker *)datePicker
{
	self.dataInicio = datePicker.date;
	self.dataInicioLabel.text = [self formatDate:self.dataInicio];
	self.dataFimLabel.text = [self formatDate:[DateUtils dateByAddingHours:datePicker.date hours:1]];
    self.dataFim = [DateUtils dateByAddingHours:datePicker.date hours:1];
}

- (IBAction)dataFimChanged:(UIDatePicker *)datePicker
{
	self.dataFim = datePicker.date;
	self.dataFimLabel.text = [self formatDate:self.dataFim];
}


#pragma mark - Format Date

- (NSString *)formatDate:(NSDate *)theDate
{
	static NSDateFormatter *formatter;
    
	if (formatter == nil) {
		formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
	}
    
	return [formatter stringFromDate:theDate];
}


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)theTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    NSIndexPath *dataInicioCellIndexPath = [NSIndexPath indexPathForRow:1 inSection:1];
    //    NSIndexPath *dataFimCellIndexPath = [NSIndexPath indexPathForRow:3 inSection:1];
    
    // make the entire row invisible
	if (indexPath.section == 2 && indexPath.row == 1) {
		return datePickerInicioVisible ? 217 : 0.0;
	}
    
	if (indexPath.section == 2 && indexPath.row == 3) {
		return datePickerFimVisible ? 217 : 0.0;
	}
    
	if (indexPath.section == 4) {
		return 160;
	}
    
    return 44;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // data inicio
	if (indexPath.section == 2 && indexPath.row == 0) {
        
        if (!datePickerInicioVisible) {
            [self showDatePickerInicio];
            [self hideDatePickerFim];
        } else {
            [self hideDatePickerInicio];
        }
        
        return;
    }
    
    // data fim
    if (indexPath.section == 2 && indexPath.row == 2) {
        
        
        if (!datePickerFimVisible) {
            [self hideDatePickerInicio];
            [self showDatePickerFim];
        } else {
            [self hideDatePickerFim];
        }
        
        return;
    }
    
    [self hideDatePickerInicio];
    
    [self hideDatePickerFim];
    
}


#pragma mark - Show/Hide Picker

- (void)showDatePickerInicio
{
    
    [self.view endEditing:YES];
    
    // change the usual to give a edit indicator to the user
	NSIndexPath *dateRowIndexPath = [NSIndexPath indexPathForRow:0 inSection:2];
    [self.tableView deselectRowAtIndexPath:dateRowIndexPath animated:YES];
    
	_dataInicioTituloLabel.textColor = [UIColor colorWithRed:0.0/255 green:122.0/255 blue:255.0/255 alpha:1.0];
	_dataInicioLabel.textColor = [UIColor colorWithRed:0.0/255 green:122.0/255 blue:255.0/255 alpha:1.0];
    
    
    
    //    // provide the existent data to date picker instance
	[self.datePickerInicio setDate:self.dataInicio animated:NO];
    
    //    // now its visible (will make its size 217 at heightForRowAtIndexPath)
	datePickerInicioVisible = YES;
    
    //    // calling beginUpdates followed immediately by endUpdates causes the table view to re-layout it cells.
	[self.tableView beginUpdates];
	[self.tableView endUpdates];
    
    //    // fade the date picker instance
	self.datePickerInicio.hidden = NO;
	self.datePickerInicio.alpha = 0.0f;
	[UIView animateWithDuration:0.25 animations:^{
		self.datePickerInicio.alpha = 1.0f;
	}];
    
    
    // if necessary, scrolls the table view to guarantee the date picker is fully on screen.
	NSIndexPath *pickerIndexPath = [NSIndexPath indexPathForRow:dateRowIndexPath.row inSection:dateRowIndexPath.section];
    
	[self.tableView scrollToRowAtIndexPath:pickerIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)hideDatePickerInicio
{
	if (datePickerInicioVisible) {
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:2];
        
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        // restore the original color
        _dataInicioTituloLabel.textColor = [UIColor blackColor];
        _dataInicioLabel.textColor = [UIColor blackColor];
        
        
		// animate the row sliding shut
		datePickerInicioVisible = NO;
		[self.tableView beginUpdates];
		[self.tableView endUpdates];
        
		// fade out and hide the date picker view
		[UIView animateWithDuration:0.25 animations:^{
			self.datePickerInicio.alpha = 0.0f;
		} completion:^(BOOL finished) {
			self.datePickerInicio.hidden = YES;
		}];
	}
}

- (void)showDatePickerFim
{
    
    [self.view endEditing:YES];
    // change the usual to give a edit indicator to the user
	NSIndexPath *dateRowIndexPath = [NSIndexPath indexPathForRow:2 inSection:2];
    [self.tableView deselectRowAtIndexPath:dateRowIndexPath animated:YES];
	_dataFimTituloLabel.textColor = [UIColor colorWithRed:0.0/255 green:122.0/255 blue:255.0/255 alpha:1.0];
    _dataFimLabel.textColor = [UIColor colorWithRed:0.0/255 green:122.0/255 blue:255.0/255 alpha:1.0];
    
    
    //    // provide the existent data to date picker instance
	[self.datePickerFim setDate:self.dataFim animated:NO];
    
    self.datePickerFim.minimumDate = self.dataInicio;
    
    //    // now its visible (will make its size 217 at heightForRowAtIndexPath)
	datePickerFimVisible = YES;
    
    //    // calling beginUpdates followed immediately by endUpdates causes the table view to re-layout it cells.
	[self.tableView beginUpdates];
	[self.tableView endUpdates];
    
    //    // fade the date picker instance
	self.datePickerFim.hidden = NO;
	self.datePickerFim.alpha = 0.0f;
	[UIView animateWithDuration:0.25 animations:^{
		self.datePickerFim.alpha = 1.0f;
	}];
    
    
    // if necessary, scrolls the table view to guarantee the date picker is fully on screen.
	NSIndexPath *pickerIndexPath = [NSIndexPath indexPathForRow:dateRowIndexPath.row inSection:dateRowIndexPath.section];
    
	[self.tableView scrollToRowAtIndexPath:pickerIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}


- (void)hideDatePickerFim
{
	if (datePickerFimVisible) {
        
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:2];
        
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        // restore the original color
        _dataFimTituloLabel.textColor = [UIColor blackColor];
        _dataFimLabel.textColor = [UIColor blackColor];
        
		// animate the row sliding shut
		datePickerFimVisible = NO;
		[self.tableView beginUpdates];
		[self.tableView endUpdates];
        
		// fade out and hide the date picker view
		[UIView animateWithDuration:0.25 animations:^{
			self.datePickerFim.alpha = 0.0f;
		} completion:^(BOOL finished) {
			self.datePickerFim.hidden = YES;
		}];
	}
}


#pragma mark - Close
- (IBAction)close
{
    [self showConfirmation];
}


- (void)showConfirmation
{
    if (_actionSheet == nil) {
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            _actionSheet = [[UIActionSheet alloc] initWithTitle:@"O evento não foi salvo!"
                                                       delegate:self
                                              cancelButtonTitle:nil
                                         destructiveButtonTitle:@"Sair"
                                              otherButtonTitles:@"Salvar",nil];
            
        } else {
            
            _actionSheet = [[UIActionSheet alloc] initWithTitle:@"O evento não foi salvo!"
                                                       delegate:self
                                              cancelButtonTitle:@"Salvar"
                                         destructiveButtonTitle:@"Sair"
                                              otherButtonTitles:nil];
            
        }
        
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        _actionSheet.actionSheetStyle = UIActionSheetStyleAutomatic;
        
        [_actionSheet showFromBarButtonItem:self.cancelButton animated:YES];
        
    } else {
        [_actionSheet showFromRect:self.view.frame inView:self.view animated:YES];
    }
    
}



#pragma mark - Save
- (IBAction)save
{
    
    Evento *evento;
    NSDate *oldStartDate;
    NSDate *oldEndDate;
    
    if (self.selectedEvento == nil) {
        
        [Utils trackWithName:@"Novo Evento"];
        [Utils trackFlurryWithName:@"Novo Evento"];
        
        evento = [Evento MR_createInContext:self.moc];
        evento.codigo = [evento getUniqueCode]; //[NSNumber numberWithLong:[[NSDate date] timeIntervalSince1970]]; //[NSNumber numberWithLong:count];
        
    } else {
        [Utils trackWithName:@"Editou Evento"];
        [Utils trackFlurryWithName:@"Editou Evento"];

        evento = self.selectedEvento;
        evento.pendingUpdateParse = [NSNumber numberWithBool:YES];
        oldStartDate = evento.dataInicio;
        oldEndDate = evento.dataFim;

    }
    
    if (_tituloTextField.text.length > 0) {
        [Utils trackWithName:@"Inseriu titulo Evento"];
        [Utils trackFlurryWithName:@"Inseriu titulo Evento"];
        
        evento.titulo = _tituloTextField.text;
    } else {
        evento.titulo = @"Novo Evento";
    }
    
    evento.local = _localTextField.text;
    
    if (_dataInicioLabel.text.length == 0) {
        
        NSTimeInterval timeSince1970 = [[NSDate date] timeIntervalSince1970];
        timeSince1970 -= fmod(timeSince1970, 60); // subtract away any extra seconds
        NSDate *nowWithoutSeconds = [NSDate dateWithTimeIntervalSince1970:timeSince1970];
        
        evento.dataInicio = nowWithoutSeconds;
        evento.dataFim = [DateUtils dateByAddingHours:evento.dataInicio hours:1];
        
    } else {
        
        evento.dataInicio = [DateUtils dateFromString:_dataInicioLabel.text withFormat:@"dd/MM/yyyy HH:mm"];
        evento.dataFim = [DateUtils dateFromString:_dataFimLabel.text withFormat:@"dd/MM/yyyy HH:mm"];
        
    }
    
    if (!oldStartDate) { // Evento novo.
        oldStartDate = evento.dataInicio;
        oldEndDate = evento.dataFim;
    }
    
    
    evento.notas = _notasTextView.text;
    evento.cliente = [NSSet setWithArray:_selectedContatos];
    evento.imovel = _selectedImovel;
    evento.tipoEvento = _selectedTipoEvento;
    evento.alarmes = [NSKeyedArchiver archivedDataWithRootObject:_selectedAlarmes];
    
    [evento registerAlarmesForLocalNotification:self.moc];
    
    
    // google analytics
    if ([_selectedAlarmes count] > 0) {
        [Utils trackWithName:@"Adicionou alarme no evento"];
        [Utils trackFlurryWithName:@"Adicionou alarme no evento"];
        
    }
    
    [Utils trackWithName:[NSString stringWithFormat:@"Tipo de Evento: %@",_selectedTipoEvento]];
    [Utils trackFlurryWithName:[NSString stringWithFormat:@"Tipo de Evento: %@",_selectedTipoEvento]];
    
    
    if (_selectedImovel != nil) {
        [Utils trackWithName:@"Adicionou imovel no evento"];
        [Utils trackFlurryWithName:@"Adicionou imovel no evento"];
        
    }
    
    evento.version = [NSDate date];
    
    // Firebase
    [evento saveFirebase];
    
    [self saveMOC];
    
    [self saveCalendar:evento withOldStartDate:oldStartDate andOldEndDate:oldEndDate];

    [Utils postEntitytNotification:@"Evento"];

    // Parse SDK
//    [evento saveParse:self.moc];
    
    [self dismiss];
    
}


#pragma mark - Save Device Calendar
- (void)saveCalendar:(Evento *)evento withOldStartDate:(NSDate *)oldStartDate andOldEndDate:(NSDate *)oldEndDate
{
    
    [CustomCalendar requestAccess:^(BOOL granted, NSError *error) {
        
        if (granted) {
            
            NSManagedObjectContext *moc = evento.managedObjectContext;
            
            [moc performBlock:^{
                
                BOOL success = [CustomCalendar addEvent:evento withMoc:moc andOldStartDate:oldStartDate andOldEndDate:oldEndDate];
                
                if (!success) {
                    [Utils showMessage:@"Ocorreu um erro ao salvar no calendário do dispositivo!"];
                }
            }];
            
        } else {
            
//            [Utils showMessage:@"O Appci não conseguiu salvar seu evento no calendário do dispositivo :-( \nPara habilitar o evento no calendario do dispositivo, abra as configurações > Privacidade > Calendários e habilite o Appci!"];
            
            
        }
        
    }];
}

#pragma mark - Dismiss
- (void)dismiss
{
    
    self.selectedEvento = nil;
    
    if ([self.navigationController.viewControllers count] == 1) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}


#pragma mark - UIActionSheetDelegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch (buttonIndex) {
        case 0:
            
            [self dismiss];;
            
            break;
        case 1:
            
            // save pressed
            [self save];
            
            break;
        default:
            break;
    }
    
}



#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self resignKeyboard];
    
    if ([segue.identifier isEqualToString:@"TipoEventoSegue"]) {
        TipoEventoViewController *controller = segue.destinationViewController;
        controller.editarEventoViewController = self;
        
    } else if ([segue.identifier isEqualToString:@"AlarmSegue"]) {
        AlarmsViewController *controller = segue.destinationViewController;
        controller.selectedAlarmes = [_selectedAlarmes mutableCopy];
        
    } else if ([segue.identifier isEqualToString:@"ParticipantesSegue"]) {
        ParticipantesViewController *controller = segue.destinationViewController;
        controller.selectedClientes = self.selectedContatos;
    } else if ([segue.identifier isEqualToString:@"SelecionarImovelSegue"]) {
        _selectedImovel = nil;
    }
}

#pragma mark - Unwind Segues
- (IBAction)unwindFromContatos:(UIStoryboardSegue *)segue
{
    ParticipantesViewController *controller = segue.sourceViewController;
    _selectedContatos = controller.selectedClientes;
    
    _quantidadeContatosLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)[_selectedContatos count]];
    
    [controller.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)unwindFromImoveis:(UIStoryboardSegue *)segue
{
    SelecionarImovelViewController *controller = segue.sourceViewController;
    _selectedImovel = controller.selectedImovel;
    
    
    if (_selectedImovel.identificacao.length > 0) {
        _imovelLabel.text = _selectedImovel.identificacao;
    } else if (_selectedImovel != nil) {
        _imovelLabel.text = @"1 imóvel selecionado"; //_selectedImovel.identificacao;
    } else {
        _imovelLabel.text = @"Nenhum";
    }
    
    [controller.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)UnwindFromAlarme:(UIStoryboardSegue *)segue
{
    AlarmsViewController *controller = segue.sourceViewController;
    _selectedAlarmes = controller.selectedAlarmes;
    
    if ([_selectedAlarmes count] == 0) {
        _alarmLabel.text = @"Nenhum";
    } else {
        _alarmLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)[_selectedAlarmes count]];
    }
    
    [controller.navigationController popViewControllerAnimated:YES];
}
@end
