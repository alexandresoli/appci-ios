//
//  ImovelViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/17/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Imovel;
@interface EditarImovelViewController : BaseTableViewController


@property (nonatomic, strong) Imovel *selectedImovel;
@end
