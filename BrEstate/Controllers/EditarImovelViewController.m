//
//  ImovelViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/17/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "EditarImovelViewController.h"
#import "Estado.h"
#import "Cidade.h"
#import "Bairro.h"
#import "Endereco.h"
#import "TipoImovel.h"
#import "Imovel.h"
#import "TipoOferta.h"
#import "Cliente.h"
#import "TagImovel.h"
#import "ListaImoveisViewController.h"

#import "EstadosViewController.h"
#import "CidadesViewController.h"
#import "BairrosViewController.h"
#import "TipoImovelViewController.h"
#import "TipoOfertaViewController.h"
#import "SelecionarClienteViewController.h"
#import "CTCheckbox.h"
#import "Utils.h"
#import "CEP.h"
#import "SVProgressHUD.h"
#import "NSString+Custom.h"

#import "Imovel+Firebase.h"
#import "Cliente+Firebase.h"
#import "Bairro+Firebase.h"


@interface EditarImovelViewController () <UIActionSheetDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;


@property (nonatomic, weak) IBOutlet UITextField *codigoImovelTextField;
@property (nonatomic, weak) IBOutlet UITextView *descricaoTextView;
@property (nonatomic, weak) IBOutlet UITextField *cepTextField;
@property (nonatomic, weak) IBOutlet UITextField *logradouroTextField;
@property (nonatomic, weak) IBOutlet UITextField *complementoTextField;
@property (nonatomic, weak) IBOutlet UITextField *bairroTextField;
@property (nonatomic, weak) IBOutlet UITextField *valorTextField;
@property (nonatomic, weak) IBOutlet UITextField *condominioTextField;
@property (nonatomic, weak) IBOutlet UITextField *iptuTextField;
@property (weak, nonatomic) IBOutlet UITextField *areaUtilTextField;
@property (weak, nonatomic) IBOutlet UITextField *areaTotalTextField;


@property (nonatomic, weak) IBOutlet UITextView *notaPrivadaTextView;

@property (nonatomic, weak) IBOutlet UILabel *estadoLabel;
@property (nonatomic, weak) IBOutlet UILabel *cidadeLabel;
@property (nonatomic, weak) IBOutlet UILabel *bairroLabel;
@property (nonatomic, weak) IBOutlet UILabel *tipoImovelLabel;
@property (nonatomic, weak) IBOutlet UILabel *tipoOfertaLabel;
@property (nonatomic, weak) IBOutlet UILabel *comissaoLabel;
@property (nonatomic, weak) IBOutlet UILabel *quartosLabel;
@property (nonatomic, weak) IBOutlet UILabel *contatoLabel;
@property (nonatomic, weak) IBOutlet UILabel *condominioLabel;


@property (nonatomic, weak) IBOutlet UISlider *quartosSlider;
@property (nonatomic, weak) IBOutlet UISlider *comissaoSlider;
@property (weak, nonatomic) IBOutlet UIStepper *comissaoStepper;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *doneButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *cancelButton;

@property (nonatomic, strong) NSArray *estados;
@property (nonatomic, strong) NSArray *cidades;
@property (nonatomic, strong) NSArray *bairros;
@property (nonatomic, strong) NSArray *tipoImoveis;
@property (nonatomic, strong) NSArray *tipoOfertas;


@property (nonatomic, strong) NSString *selectedEstado;
@property (nonatomic, strong) NSString *selectedCidade;
@property (nonatomic, strong) Bairro *selectedBairro;
@property (nonatomic, strong) NSString *selectedTipoImovel;
@property (nonatomic, strong) NSString *selectedTipoOferta;
@property (nonatomic, strong) Cliente *selectedContato;


@property (weak, nonatomic) IBOutlet CTCheckbox *armarioCozinhaCheckbox;
@property (weak, nonatomic) IBOutlet CTCheckbox *armarioEmbutidoCheckbox;
@property (weak, nonatomic) IBOutlet CTCheckbox *varandaCheckbox;
@property (weak, nonatomic) IBOutlet CTCheckbox *wcEmpregadaCheckbox;

@property (weak, nonatomic) IBOutlet CTCheckbox *childrenCareCheckbox;
@property (weak, nonatomic) IBOutlet CTCheckbox *churrasqueiraCheckbox;
@property (weak, nonatomic) IBOutlet CTCheckbox *piscinaCheckbox;
@property (weak, nonatomic) IBOutlet CTCheckbox *playgroundCheckbox;
@property (weak, nonatomic) IBOutlet CTCheckbox *quadraPoliesportivaCheckbox;
@property (weak, nonatomic) IBOutlet CTCheckbox *salaGinasticaCheckbox;
@property (weak, nonatomic) IBOutlet CTCheckbox *salaoFestasCheckbox;
@property (weak, nonatomic) IBOutlet CTCheckbox *salaoJogosCheckbox;
@property (weak, nonatomic) IBOutlet CTCheckbox *saunaCheckbox;
@property (weak, nonatomic) IBOutlet CTCheckbox *garagemCheckbox;

@property (nonatomic, strong) Imovel *createdImovel;

@property (nonatomic, strong) UIActionSheet *actionSheet;


@end


@implementation EditarImovelViewController

@synthesize comissaoSlider,quartosSlider;
@synthesize selectedEstado,selectedCidade,selectedBairro,selectedTipoImovel,selectedTipoOferta,selectedImovel,selectedContato;
@synthesize estadoLabel,cidadeLabel,bairroLabel,tipoImovelLabel,tipoOfertaLabel,comissaoLabel,quartosLabel,contatoLabel,condominioLabel;
@synthesize descricaoTextView, cepTextField,logradouroTextField,complementoTextField,bairroTextField,valorTextField,condominioTextField,iptuTextField,codigoImovelTextField;
@synthesize notaPrivadaTextView;
@synthesize doneButton;

@synthesize armarioCozinhaCheckbox,armarioEmbutidoCheckbox,varandaCheckbox,wcEmpregadaCheckbox;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  
    self.estados = [Estado MR_findAllSortedBy:@"nome" ascending:YES inContext:self.moc];
  
    self.bairros = [Bairro MR_findAllSortedBy:@"nome" ascending:YES inContext:self.moc];

    self.tipoImoveis = [TipoImovel MR_findAllSortedBy:@"descricao" ascending:YES inContext:self.moc];
    
    self.tipoOfertas = [TipoOferta MR_findAllSortedBy:@"descricao" ascending:YES inContext:self.moc];
    
    
    if (selectedImovel != nil) {
        self.navigationItem.title = @"Editar Imóvel";
        [self loadImovel];
    }
    
    descricaoTextView.keyboardAppearance = UIKeyboardAppearanceDark;
    notaPrivadaTextView.keyboardAppearance = UIKeyboardAppearanceDark;
    
    
    //    doneButton.enabled = selectedImovel.descricao.length > 0 ? YES : NO;
    
    
    // Register for text change in nomeTextField
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:@"UITextViewTextDidChangeNotification" object:descricaoTextView];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [Utils trackWithName:@"Tela Editar/Adicionar Imovel"];
    [Utils trackFlurryTimedWithName:@"Tela Editar/Adicionar Imovel"];
    
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [Flurry endTimedEvent:@"Tela Editar/Adicionar Imovel" withParameters:nil];
}


#pragma mark - Action Sheet

- (void)showConfirmation
{
    if (_actionSheet == nil) {
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            _actionSheet = [[UIActionSheet alloc] initWithTitle:@"O imóvel não foi salvo!"
                                                       delegate:self
                                              cancelButtonTitle:nil
                                         destructiveButtonTitle:@"Sair"
                                              otherButtonTitles:@"Salvar",nil];
            _actionSheet.actionSheetStyle = UIActionSheetStyleAutomatic;
            
        } else {
            
            _actionSheet = [[UIActionSheet alloc] initWithTitle:@"O imóvel não foi salvo!"
                                                       delegate:self
                                              cancelButtonTitle:@"Salvar"
                                         destructiveButtonTitle:@"Sair"
                                              otherButtonTitles:nil];
            
        }
        
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [_actionSheet showFromBarButtonItem:self.cancelButton animated:YES];
        
    } else {
        [_actionSheet showFromRect:self.view.frame inView:self.view animated:YES];
    }
    
}

#pragma mark - Close
- (IBAction)close:(id)sender
{
    [self showConfirmation];
}


#pragma mark - UIActionSheetDelegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch (buttonIndex) {
        case 0:
            
            [self dismiss];;
            
            break;
        case 1:
            
            // save pressed
            [self save:nil];
            
            break;
        default:
            break;
    }
    
}

#pragma mark - Dismiss
- (void)dismiss
{
    // cancel pressed
    if (self.createdImovel) {
        
        [self performSegueWithIdentifier:@"UnwindFromAdicionarImovel" sender:self];
    } else {
        if ([self.navigationController.viewControllers count] == 1) {
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    
}
#pragma mark - Notifiation
- (void)textFieldDidChange :(NSNotification *)notif
{
    //    doneButton.enabled = descricaoTextView.text.length > 0 ? YES : NO;
}


#pragma mark - Load
- (void)loadImovel
{
    codigoImovelTextField.text = selectedImovel.identificacao;
    descricaoTextView.text = selectedImovel.descricao;
    quartosLabel.text = [self formatQuartos:[selectedImovel.quartos intValue]];
    
    quartosSlider.value = [selectedImovel.quartos floatValue];
    
    valorTextField.text =  [self.formatter stringFromNumber:selectedImovel.valor];
    condominioTextField.text =  [self.formatter stringFromNumber:selectedImovel.condominio];
    iptuTextField.text =  [self.formatter stringFromNumber:selectedImovel.iptu];
    
    comissaoLabel.text =  [NSString stringWithFormat:@"%@ %@",selectedImovel.comissao, @"%"];
//    comissaoSlider.value = [selectedImovel.comissao intValue];
    self.comissaoStepper.value = [selectedImovel.comissao intValue];
    
    self.areaUtilTextField.text = [selectedImovel.areaUtil stringValue];
    self.areaTotalTextField.text = [selectedImovel.areaTotal stringValue];
    
    if ([selectedImovel.endereco.cep intValue] > 0) {
        
        NSString *cep = [selectedImovel.endereco.cep stringValue];
        
        if (cep.length == 7) {
            cep = [NSString stringWithFormat:@"0%@",cep];
        }
        
        NSMutableString *mutableString  = [cep mutableCopy];
        [mutableString insertString:@"-" atIndex:5];
        cepTextField.text = mutableString;
    }
    
    
    logradouroTextField.text = selectedImovel.endereco.logradouro;
    complementoTextField.text = selectedImovel.endereco.complemento;
    estadoLabel.text = selectedImovel.endereco.estado;
    cidadeLabel.text = selectedImovel.endereco.cidade;
    bairroLabel.text = selectedImovel.endereco.bairro.nome;
    tipoImovelLabel.text = selectedImovel.tipo;
    tipoOfertaLabel.text = selectedImovel.oferta;
    
    selectedEstado = selectedImovel.endereco.estado;
    selectedCidade = selectedImovel.endereco.cidade;
    selectedBairro = selectedImovel.endereco.bairro;
    selectedTipoImovel = selectedImovel.tipo;
    selectedTipoOferta = selectedImovel.oferta;
    
    selectedContato = selectedImovel.cliente;
    contatoLabel.text = selectedImovel.cliente.nome;
    
    notaPrivadaTextView.text = selectedImovel.notaPrivada;
    
    // tags
    
    TagImovel *tags = selectedImovel.tags;
    
    self.armarioCozinhaCheckbox.checked = [tags.armarioCozinha boolValue];
    self.armarioEmbutidoCheckbox.checked = [tags.armarioEmbutido boolValue];
    self.varandaCheckbox.checked = [tags.varanda boolValue];
    self.wcEmpregadaCheckbox.checked = [tags.wcEmpregada boolValue];
    self.childrenCareCheckbox.checked = [tags.childrenCare boolValue];
    self.churrasqueiraCheckbox.checked = [tags.churrasqueira boolValue];
    self.piscinaCheckbox.checked = [tags.piscina boolValue];
    self.playgroundCheckbox.checked = [tags.playground boolValue];
    self.quadraPoliesportivaCheckbox.checked = [tags.quadraPoliesportiva boolValue];
    self.salaGinasticaCheckbox.checked = [tags.salaGinastica boolValue];
    self.salaoFestasCheckbox.checked = [tags.salaoFestas boolValue];
    self.salaoJogosCheckbox.checked = [tags.salaoJogos boolValue];
    self.saunaCheckbox.checked = [tags.sauna boolValue];
    self.garagemCheckbox.checked = [tags.garagem boolValue];
    
}

#pragma mark - Save
- (IBAction)save:(id)sender
{
    
    Imovel *imovel = nil;
    
    if (selectedImovel != nil) {
        imovel = selectedImovel;
        self.createdImovel = nil;
        imovel.pendingUpdateParse = [NSNumber numberWithBool:YES];
        
    } else {
        
        imovel = [Imovel MR_createInContext:self.moc];
        imovel.codigo = [imovel getUniqueCode]; //[NSNumber numberWithLong:count];
        
        self.createdImovel = imovel;
    }
    
    imovel.identificacao = codigoImovelTextField.text;
    imovel.descricao = descricaoTextView.text;
    imovel.quartos = [NSNumber numberWithInt:roundf(quartosSlider.value)];
    
    NSNumber *valor = [self.formatter numberFromString:valorTextField.text];
    
    imovel.valor = [NSDecimalNumber decimalNumberWithDecimal:[valor decimalValue]];
    imovel.comissao = [NSNumber numberWithInt:roundf(self.comissaoStepper.value)]; //[NSNumber numberWithInt:roundf(comissaoSlider.value)];
    
    imovel.areaUtil = [NSNumber numberWithInt:[self.areaUtilTextField.text intValue]];
    imovel.areaTotal = [NSNumber numberWithInt:[self.areaTotalTextField.text intValue]];
    
    NSNumber *condominio = [self.formatter numberFromString:condominioTextField.text];
    imovel.condominio = [NSDecimalNumber decimalNumberWithDecimal:[condominio decimalValue]];
    
    NSNumber *iptu = [self.formatter numberFromString:iptuTextField.text];
    imovel.iptu = [NSDecimalNumber decimalNumberWithDecimal:[iptu decimalValue]];
    imovel.tipo = selectedTipoImovel;
    imovel.oferta = selectedTipoOferta;
    
    // endereco
    if (imovel.endereco == nil) {
        
        
        if (cepTextField.text.trimmed.length > 0 ||
            logradouroTextField.text.trimmed.length > 0 ||
            complementoTextField.text.trimmed.length > 0 ||
            (estadoLabel.text.trimmed.length > 0  && ![estadoLabel.text.trimmed containsString:@"Nenhum"])||
            (cidadeLabel.text.trimmed.length > 0 && ![cidadeLabel.text.trimmed containsString:@"Nenhum"]) ||
            (bairroLabel.text.trimmed.length > 0 && ![bairroLabel.text.trimmed containsString:@"Nenhum"])) {
            
            imovel.endereco = [Endereco MR_createInContext:self.moc];
            imovel.endereco.codigo = [imovel.endereco getUniqueCode]; //[NSNumber numberWithLong:count];
            
        }
    }
    
    NSString *cep = [cepTextField.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
    imovel.endereco.cep = [NSNumber numberWithInt:[cep intValue]];
    imovel.endereco.logradouro = logradouroTextField.text;
    imovel.endereco.complemento = complementoTextField.text;
    imovel.endereco.estado = selectedEstado;
    imovel.endereco.cidade = selectedCidade;
    imovel.endereco.bairro = selectedBairro;
    imovel.endereco.latitude = [NSNumber numberWithInt:0];
    imovel.endereco.longitude = [NSNumber numberWithInt:0];
    
    //tags
    
    if (imovel.tags == nil) {
        
        if (self.armarioCozinhaCheckbox.checked ||
            self.armarioEmbutidoCheckbox.checked ||
            self.varandaCheckbox.checked ||
            self.wcEmpregadaCheckbox.checked ||
            self.childrenCareCheckbox.checked ||
            self.churrasqueiraCheckbox.checked ||
            self.piscinaCheckbox.checked ||
            self.playgroundCheckbox.checked ||
            self.quadraPoliesportivaCheckbox.checked ||
            self.salaGinasticaCheckbox.checked ||
            self.salaoFestasCheckbox.checked ||
            self.salaoJogosCheckbox.checked ||
            self.saunaCheckbox.checked ||
            self.garagemCheckbox.checked) {
            
            imovel.tags = [TagImovel MR_createInContext:self.moc];
            imovel.tags.codigo = [imovel.tags getUniqueCode]; //[NSNumber numberWithLong:count];
            
        }
    }
    
    imovel.tags.armarioCozinha = [NSNumber numberWithBool:self.armarioCozinhaCheckbox.checked];
    imovel.tags.armarioEmbutido = [NSNumber numberWithBool:self.armarioEmbutidoCheckbox.checked];
    imovel.tags.varanda = [NSNumber numberWithBool:self.varandaCheckbox.checked];
    imovel.tags.wcEmpregada = [NSNumber numberWithBool:self.wcEmpregadaCheckbox.checked];
    imovel.tags.childrenCare = [NSNumber numberWithBool:self.childrenCareCheckbox.checked];
    imovel.tags.churrasqueira = [NSNumber numberWithBool:self.churrasqueiraCheckbox.checked];
    imovel.tags.piscina = [NSNumber numberWithBool:self.piscinaCheckbox.checked];
    imovel.tags.playground = [NSNumber numberWithBool:self.playgroundCheckbox.checked];
    imovel.tags.quadraPoliesportiva = [NSNumber numberWithBool:self.quadraPoliesportivaCheckbox.checked];
    imovel.tags.salaGinastica = [NSNumber numberWithBool:self.salaGinasticaCheckbox.checked];
    imovel.tags.salaoFestas = [NSNumber numberWithBool:self.salaoFestasCheckbox.checked];
    imovel.tags.salaoJogos = [NSNumber numberWithBool:self.salaoJogosCheckbox.checked];
    imovel.tags.sauna = [NSNumber numberWithBool:self.saunaCheckbox.checked];
    imovel.tags.garagem = [NSNumber numberWithBool:self.garagemCheckbox.checked];
    
    imovel.notaPrivada = notaPrivadaTextView.text;
    
    imovel.cliente = selectedContato;
    
    imovel.version = [NSDate date];
    
    // Firebase
    [imovel saveFirebase];
    
    [self saveMOC];
    
    [Utils postEntitytNotification:@"Imovel"];
    
    // Parse SDK
//    [imovel saveParse:self.moc];
    
    
    [self dismiss];
    
}



#pragma mark - CEP Search
- (CEP *)searchCEP:(NSString *)cepToSearch
{
    
    if (![Utils isConnected] || cepToSearch.length == 0) {
        return nil;
    }
    
    
    [SVProgressHUD showWithStatus:@"Buscando CEP..." maskType:SVProgressHUDMaskTypeBlack];
    
    cepToSearch = [cepToSearch stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    
    __block CEP *cep = nil;
    
    
    NSString *url = [NSString stringWithFormat:@"%@%@?cep=%@",[[Config instance] url], @"carrega-cep.json", cepToSearch];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSNumber *success = [responseObject valueForKey:@"resultado"];
        
        if ([success boolValue]) {
            
            cep = [[CEP alloc]  init];
            cep.uf = [responseObject valueForKey:@"uf"];
            cep.cidade = [responseObject valueForKey:@"cidade"];
            cep.bairro = [responseObject valueForKey:@"bairro"];
            cep.tipoLogradouro = [responseObject valueForKey:@"tipo_logradouro"];
            cep.logradouro = [responseObject valueForKey:@"logradouro"];
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self convertCEP:cep];
            });
        }
        
        [Utils dismissProgress];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%s - %@",__FUNCTION__,error);
        [Utils dismissProgress];
    }];
    
    
    return cep;
}

- (void)convertCEP:(CEP *)cep
{

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sigla == [cd] %@", cep.uf];
    Estado *estado = [Estado MR_findFirstWithPredicate:predicate inContext:self.moc];
    
    estadoLabel.text = estado.nome;
    selectedEstado = estado.nome;
    
    predicate = [NSPredicate predicateWithFormat:@"nome == [cd] %@", cep.cidade];
    Cidade *cidade = [Cidade MR_findFirstWithPredicate:predicate inContext:self.moc];

    cidadeLabel.text = cidade.nome;
    selectedCidade = cidade.nome;
    
    predicate = [NSPredicate predicateWithFormat:@"nome == [cd] %@", cep.bairro];
    Bairro *bairro = [Bairro MR_findFirstWithPredicate:predicate inContext:self.moc];
    
    if (bairro == nil) {
        
        bairro = [Bairro MR_createInContext:self.moc];
        bairro.codigo = [bairro getUniqueCode]; //[NSNumber numberWithLong:codigo];
        bairro.nome = [cep.bairro capitalizedString];
        bairro.cidade = cidade.nome;
        
        bairro.version = [NSDate date];
        
        // Firebase
        [bairro saveFirebase];
        
        [self saveMOC];
    }
    
    bairroLabel.text = bairro.nome;
    selectedBairro = bairro;
    
    logradouroTextField.text = [NSString stringWithFormat:@"%@ %@",cep.tipoLogradouro, cep.logradouro];
    
    // Parse SDK
//    [bairro saveParse:self.moc];
    
}



#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if ([textField isEqual:cepTextField] && cepTextField.text.length > 8) {
        
        [self searchCEP:textField.text];
        
    }
    
}

#pragma mark - Format CEP
- (BOOL)formatCEP:(NSRange)range string:(NSString *)string textField:(UITextField *)textField
{
    if ([textField isEqual:cepTextField]) {
        
        
        if (range.length == 1) { // backspace
            return YES;
        }
        
        
        // only numbers
        NSCharacterSet *nonNumberSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        
        if ([string rangeOfCharacterFromSet:nonNumberSet].location != NSNotFound)
        {
            return NO;
        }
        
        long length = textField.text.length;
        
        
        // no more than 8 xxxxx-xxx
        if (length > 8) {
            return NO;
        }
        
        // format number
        if (length == 5) {
            
            textField.text = [NSString stringWithFormat:@"%@-%@",textField.text,string];
            return NO;
        }
        
    }
    
    return YES;
}

// return NO to not change text
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([textField isEqual:valorTextField] || [textField isEqual:condominioTextField] || [textField isEqual:iptuTextField]) {
        
        [self currencyFormat:string range:range textField:textField];
        return NO;
        
    } else if ([textField isEqual:cepTextField]) {
        
        return [self formatCEP:range string:string textField:textField];
        
    }
    
    return YES;
}

#pragma mark - Currency Format
- (void)currencyFormat:(NSString *)string range:(NSRange)range textField:(UITextField *)textField
{
    
    
    NSString *replaced = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSDecimalNumber *amount = (NSDecimalNumber*) [self.formatter numberFromString:replaced];
    if (amount == nil)
    {
        // Something screwed up the parsing. Probably an alpha character.
        return;
    }
    // If the field is empty (the inital case) the number should be shifted to
    // start in the right most decimal place.
    short powerOf10 = 0;
    if ([textField.text isEqualToString:@""])
    {
        powerOf10 = -self.formatter.maximumFractionDigits;
    }
    // If the edit point is to the right of the decimal point we need to do
    // some shifting.
    else if (range.location + self.formatter.maximumFractionDigits >= textField.text.length)
    {
        // If there's a range of text selected, it'll delete part of the number
        // so shift it back to the right.
        if (range.length) {
            powerOf10 = -range.length;
        }
        // Otherwise they're adding this many characters so shift left.
        else {
            powerOf10 = [string length];
        }
    }
    amount = [amount decimalNumberByMultiplyingByPowerOf10:powerOf10];
    
    // Replace the value and then cancel this change.
    textField.text = [self.formatter stringFromNumber:amount];
}

#pragma mark - Sliders
#pragma mark - Quartos Slider
- (IBAction)sliderQuartosValueChanged:(UISlider *)sender
{
    int value = roundf(sender.value);
    quartosLabel.text  = [self formatQuartos:value];
}

//#pragma mark - Slider Comissao
//- (IBAction)sliderComissaoValueChanged:(UISlider *)sender
//{
//    int value = roundf(sender.value);
//    
//    comissaoLabel.text = [NSString stringWithFormat:@"%d %@", value, @"%"];
//}

#pragma mark - Stepper Comissao
- (IBAction)comissaoStepperValueChanged:(UIStepper *)sender
{
    int value = roundf(sender.value);
    comissaoLabel.text = [NSString stringWithFormat:@"%d %@", value, @"%"];
}


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.0;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    [self resignKeyboard];
    
    if ([segue.identifier isEqualToString:@"CidadesSegue"]) {
        CidadesViewController *controller = segue.destinationViewController;
        controller.selectedEstado = selectedEstado;
        
    } else if ([segue.identifier isEqualToString:@"BairrosSegue"]) {
        BairrosViewController *controller = segue.destinationViewController;
        controller.selectedCidade = selectedCidade;
        
    } else if ([segue.identifier isEqualToString:@"UnwindFromAdicionarImovel"]) {
        ListaImoveisViewController *controller = segue.destinationViewController;
        controller.selectedImovel = self.createdImovel;
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"CidadesSegue"]) {
        
        if (selectedEstado == nil) {
            return NO;
        }
    } else if ([identifier isEqualToString:@"BairrosSegue"]) {
        
        if (selectedCidade == nil) {
            return NO;
        }
    }
    
    return YES;
}


#pragma mark - Unwind Segue
- (IBAction)unwindFromContatos:(UIStoryboardSegue *)segue
{
    SelecionarClienteViewController *controller = segue.sourceViewController;
    selectedContato = controller.selectedContato;
    
    if (selectedContato != nil) {
        contatoLabel.text = selectedContato.nome;
    } else {
        contatoLabel.text = @"Nenhum";
    }
    
    [controller.navigationController popViewControllerAnimated:YES];
    
}
- (IBAction)unwindFromEstados:(UIStoryboardSegue *)segue
{
    EstadosViewController *controller = segue.sourceViewController;
    selectedEstado = controller.selectedEstado;
    
    selectedCidade = nil;
    selectedBairro = nil;
    cidadeLabel.text = nil;
    bairroLabel.text = nil;
    estadoLabel.text = selectedEstado;
    
}

- (IBAction)unwindFromCidades:(UIStoryboardSegue *)segue
{
    CidadesViewController *controller = segue.sourceViewController;
    selectedCidade = controller.selectedCidade;
    
    selectedBairro = nil;
    bairroLabel.text = nil;
    
    cidadeLabel.text = selectedCidade;
}

- (IBAction)unwindFromBairros:(UIStoryboardSegue *)segue
{
    BairrosViewController *controller = segue.sourceViewController;
    selectedBairro = controller.selectedBairro;
    
    bairroLabel.text = selectedBairro.nome;
}

- (IBAction)unwindFromTipoImovel:(UIStoryboardSegue *)segue
{
    TipoImovelViewController *controller = segue.sourceViewController;
    selectedTipoImovel = controller.selectedTipoImovel.descricao;
    
    tipoImovelLabel.text = selectedTipoImovel;
}

- (IBAction)unwindFromTipoOferta:(UIStoryboardSegue *)segue
{
    TipoOfertaViewController *controller = segue.sourceViewController;
    selectedTipoOferta = controller.selectedTipoOferta;
    
    tipoOfertaLabel.text = selectedTipoOferta;
}


#pragma mark - IBAction Dismiss Keyboard

- (IBAction)dismissKeyboard:(id)sender
{
    [self.view endEditing:YES];
}


@end
