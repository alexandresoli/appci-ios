//
//  AdicionarNegociacaoViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/27/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Imovel;

@interface EditarNegociacaoViewController : BaseTableViewController

@property (nonatomic, strong) Imovel *selectedImovel;

@end
