//
//  AdicionarNegociacaoViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/27/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "EditarNegociacaoViewController.h"
#import "SelecionarClienteViewController.h"
#import "StatusNegociacao.h"
#import "Imovel.h"
#import "Cliente.h"
#import "Negociacao.h"
#import "TipoCliente.h"
#import "SubtipoCliente.h"
#import "DateUtils.h"
#import "Utils.h"
#import "StatusNegociacaoViewController.h"
#import "Acompanhamento+Firebase.h"
#import "Negociacao+Firebase.h"

@interface EditarNegociacaoViewController () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UILabel *valorLabel;
@property (nonatomic, weak) IBOutlet UILabel *statusNegociacaoLabel;
@property (nonatomic, weak) IBOutlet UILabel *imovelLabel;
@property (nonatomic, weak) IBOutlet UILabel *clienteLabel;
@property (nonatomic, weak) IBOutlet UILabel *comissaoLabel;
@property (nonatomic, weak) IBOutlet UILabel *parceriaLabel;
@property (nonatomic, weak) IBOutlet UILabel *calculoParceriaLabel;

@property (nonatomic, strong) Cliente *selectedCliente;
@property (nonatomic, strong) StatusNegociacao *selectedStatusNegociacao;

@property (weak, nonatomic) IBOutlet UIStepper *parceriaStepper;
@property (nonatomic, weak) IBOutlet UISwitch *parceriaSwitch;
@property (nonatomic, weak) IBOutlet UISlider *parceriaSlider;
@property (nonatomic, weak) IBOutlet UITextView *notasTextView;


@end

@implementation EditarNegociacaoViewController

@synthesize valorLabel;
@synthesize statusNegociacaoLabel;
@synthesize imovelLabel;
@synthesize clienteLabel;
@synthesize selectedImovel;
@synthesize selectedCliente;
@synthesize comissaoLabel;
@synthesize parceriaSwitch;
@synthesize parceriaLabel;
@synthesize parceriaSlider;
@synthesize notasTextView;
@synthesize calculoParceriaLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = 1"];
    StatusNegociacao *status =  [StatusNegociacao MR_findFirstWithPredicate:predicate inContext:self.moc];
    statusNegociacaoLabel.text = status.descricao;
    _selectedStatusNegociacao = status;
    
    notasTextView.keyboardAppearance = UIKeyboardAppearanceDark;

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    imovelLabel.text = selectedImovel.identificacao;
    valorLabel.text = [self.formatter stringFromNumber:selectedImovel.valor];
    comissaoLabel.text = [NSString stringWithFormat:@"%@ %@",selectedImovel.comissao, @"%"];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Close
- (IBAction)close
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Save

- (BOOL)formIsValid
{
    
    BOOL valid = YES;
    
    if (_selectedStatusNegociacao == nil) {
        [Utils addRedCorner:statusNegociacaoLabel];
        valid = NO;
    }
    
    if (selectedCliente == nil) {
        [Utils addRedCorner:clienteLabel];
        valid = NO;
    }
    
    
    return valid;
    
}

- (IBAction)save
{
    if (![self formIsValid]) {
        return;
    }
    
    Negociacao *negociacao = nil;
    
    negociacao = [Negociacao MR_createInContext:self.moc];
    negociacao.codigo = [negociacao getUniqueCode]; //[NSNumber numberWithLong:count];
    
    
    NSNumber *valor = [self.formatter numberFromString:valorLabel.text];
    
    negociacao.valor = [NSDecimalNumber decimalNumberWithDecimal:[valor decimalValue]];
    negociacao.data = [NSDate date];
    negociacao.status = _selectedStatusNegociacao.descricao;
    negociacao.imovel = selectedImovel;
    negociacao.cliente = selectedCliente;
    
    NSString *comissao = [comissaoLabel.text stringByReplacingOccurrencesOfString:@" %" withString:@""];
    
    negociacao.comissao = [self.formatter numberFromString:comissao];
    negociacao.parceria = [NSNumber numberWithBool:parceriaSwitch.isOn];
    
    if (parceriaSwitch.isOn) {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            negociacao.percentualParceria = [NSNumber numberWithInt:roundf(parceriaSlider.value)];
        } else {
            negociacao.percentualParceria = [NSNumber numberWithInt:roundf(self.parceriaStepper.value)];
        }
    }
    
    negociacao.notas = notasTextView.text;
    
    negociacao.version = [NSDate date];
    
    // Firebase
    [negociacao saveFirebase];
    
    [self saveMOC];
    
    [Utils postEntitytNotification:@"Imovel"];
    
    [self addAcompanhamento];
    
    
    // Parse SDK
//    [negociacao saveParse:self.moc];
    
    [self close];
    
}

#pragma mark - Acompanhamento
- (void)addAcompanhamento
{
    Acompanhamento *acompanhamento = [Acompanhamento MR_createInContext:self.moc];
    acompanhamento.codigo = [acompanhamento getUniqueCode]; //[NSNumber numberWithLong:codigo];
    acompanhamento.timeStamp = [NSDate date];
    acompanhamento.titulo = [NSString stringWithFormat:@"Iniciada negociação do imóvel %@",self.selectedImovel.identificacao];
    acompanhamento.cliente = self.selectedCliente;
    acompanhamento.imovel = self.selectedImovel;
    
    [acompanhamento saveFirebase];
    
    [self saveMOC];
    
    // Parse SDK
//    [acompanhamento saveParse:self.moc];
}

#pragma mark - UITextFieldDelegate
// return NO to disallow editing.
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    [Utils removeRedCorner:textField];
    return YES;
}


// return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}


// return NO to not change text
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

// called when clear button pressed. return NO to ignore (no notifications)
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = @"";
    return YES;
}

// called when 'return' key pressed. return NO to ignore.
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}




#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    [self resignKeyboard];
    
    if ([segue.identifier isEqualToString:@"SelecionarContatoSegue"]) {
        [Utils removeRedCorner:clienteLabel];
    }
}

#pragma mark - Unwind Segue

- (IBAction)unwindFromStatusNegociacao:(UIStoryboardSegue *)segue
{
    StatusNegociacaoViewController *controller = segue.sourceViewController;
    _selectedStatusNegociacao = controller.selectedStatusNegociacao;
    
    if (_selectedStatusNegociacao != nil) {
        statusNegociacaoLabel.text = _selectedStatusNegociacao.descricao;
    } else {
        statusNegociacaoLabel.text = @"Nenhum";
    }
    
    [controller.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)unwindFromContatos:(UIStoryboardSegue *)segue
{
    SelecionarClienteViewController *controller = segue.sourceViewController;
    selectedCliente = controller.selectedContato;
    
    if (selectedCliente != nil) {
        clienteLabel.text = selectedCliente.nome;
    }
    
    [controller.navigationController popViewControllerAnimated:YES];
    
}




#pragma mark - Parceria Switch
- (IBAction)switchParceriaValueChanged:(UISwitch *)sender
{
    
    [self changeParceria:sender];
}

- (void)changeParceria:(UISwitch *)sender
{
    if (sender.isOn) {
        
        parceriaSlider.userInteractionEnabled = YES;
        self.parceriaStepper.userInteractionEnabled = YES;
        
        parceriaSlider.alpha = 1.0;
        self.parceriaStepper.alpha = 1.0;
        
        parceriaLabel.alpha = 1.0;
        
    } else {
        parceriaSlider.userInteractionEnabled = NO;
        self.parceriaStepper.userInteractionEnabled = NO;
        
        parceriaSlider.alpha =  0.3;
        self.parceriaStepper.alpha = 0.3;
        
        parceriaLabel.alpha = 0.3;
        
    }
}


#pragma mark - Slider Parceria
- (IBAction)sliderParceriaValueChanged:(UISlider *)sender
{
    [self changePercentParceria:sender.value];
    
//    int value = roundf(sender.value);
//    
//    parceriaLabel.text = [NSString stringWithFormat:@"%d %@", value, @"%"];
//    
//    NSNumber *valor = [self.formatter numberFromString:valorLabel.text];
//    
//    NSDecimalNumber *val = [NSDecimalNumber decimalNumberWithDecimal:[valor decimalValue]];
//    NSDecimalNumber *percentual = [[NSDecimalNumber alloc] initWithInt:value];
//    NSDecimalNumber *cem = [[NSDecimalNumber alloc] initWithInt:100];
//    
//    
//    NSDecimalNumber *total = [[val decimalNumberByMultiplyingBy:percentual] decimalNumberByDividingBy:cem];
//    
//    
//    calculoParceriaLabel.text = [self.formatter stringFromNumber:total];
}

- (IBAction)stepperParceriaValueChanged:(UIStepper *)sender
{
    [self changePercentParceria:sender.value];
}

- (void)changePercentParceria:(int)value
{
    parceriaLabel.text = [NSString stringWithFormat:@"%d %@", value, @"%"];
    
    NSNumber *valor = [self.formatter numberFromString:valorLabel.text];
    
    NSDecimalNumber *val = [NSDecimalNumber decimalNumberWithDecimal:[valor decimalValue]];
    NSDecimalNumber *percentual = [[NSDecimalNumber alloc] initWithInt:value];
    NSDecimalNumber *cem = [[NSDecimalNumber alloc] initWithInt:100];
    
    
    NSDecimalNumber *total = [[val decimalNumberByMultiplyingBy:percentual] decimalNumberByDividingBy:cem];
    
    
    calculoParceriaLabel.text = [self.formatter stringFromNumber:total];
}


#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}


@end
