//
//  EditarNotasViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 2/6/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "BaseViewController.h"

@class Nota;
@interface EditarNotasViewController : BaseViewController

@property (nonatomic, strong) Nota *selectNota;

- (void)showNota:(Nota *)nota;


@end
