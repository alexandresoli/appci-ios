//
//  DetalhesNotasViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 2/6/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "EditarNotasViewController.h"
#import "Nota.h"
#import "DateUtils.h"
#import "Nota+Firebase.h"

@import QuickLook;

@interface EditarNotasViewController () <UITextViewDelegate, QLPreviewControllerDataSource, QLPreviewControllerDelegate>

@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (nonatomic, weak) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIButton *abrirArquivoButton;
@property (nonatomic) CGRect textViewDefaultRect;


@end

@implementation EditarNotasViewController


#pragma mark - IBActions
- (IBAction)abrirArquivo:(id)sender
{
    if (self.selectNota.arquivo != nil) {

        QLPreviewController *previewController=[[QLPreviewController alloc]init];
        previewController.delegate = self;
        previewController.dataSource = self;
        [self presentViewController:previewController animated:YES completion:nil];
    }
}


#pragma mark - View Life Cycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _textView.keyboardAppearance = UIKeyboardAppearanceDark;
    
//    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enableEditing)];
//    tapGesture.numberOfTapsRequired = 1;
//    
//    [_textView addGestureRecognizer:tapGesture];
    
    if (self.selectNota) {
        self.selectNota.pendingUpdateParse = [NSNumber numberWithBool:YES];
        [self showNota:self.selectNota];
    }
    
    // Observers do teclado para manipular o tamamho do textView durante a edição.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    // Guarda o tamanho original do textView para utilizar no redimensionamento ao exibir o teclado.
    self.textViewDefaultRect = self.textView.frame;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Detalhe Notas"];
    [Utils trackFlurryWithName:@"Detalhe Notas"];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


#pragma mark - Tratamento para o textView quando o teclado é exibido.
// Quando o teclado aparece o tamanho do texView é reduzido para que seja possível acessar todo o conteúdo.
- (void)keyboardDidShow:(NSNotification *)notification
{
    [UIView beginAnimations:nil context:nil];
    CGRect keyboradRect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect newRect = self.textViewDefaultRect;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        // No iPad não tem tabBar, então usa o tamanho do botão que simula o tabbar mais os espaços que
        //  exitem entre o botão e a base do textView (57 + 20).
        CGFloat keyboardHeight;
        if (IS_OS_8_OR_LATER) {
            keyboardHeight = keyboradRect.size.height;
        } else {
            keyboardHeight = keyboradRect.size.width; // Em landscape no iOS7 a altura e largura do teclado vem trocada.
        }
        newRect.size.height -= (keyboardHeight - 77);
        
    } else {
        newRect.size.height -= (keyboradRect.size.height - (self.tabBarController.tabBar.frame.size.height - 10));
    }
    
    self.textView.frame = newRect;
    [UIView commitAnimations];
}


// Quando o teclado desaparece o textView volta para seu tamanho original.
- (void)keyboardWillHide:(NSNotification *)notification
{
    [UIView beginAnimations:nil context:nil];
    self.textView.frame = self.textViewDefaultRect;
    [UIView commitAnimations];
}


#pragma mark - QLPreviewControllerDataSource
- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller
{
    return 1;
}


- (id <QLPreviewItem>)previewController:(QLPreviewController *)controller previewItemAtIndex:(NSInteger)index
{
    NSString *fileName = [NSString stringWithFormat:@"arquivo.%@",self.selectNota.extensao];
    
    NSURL *url = [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] URLByAppendingPathComponent:fileName];
    [self.selectNota.arquivo writeToURL:url atomically:YES];
    return url;
}


#pragma mark - Show Nota
- (void)showNota:(Nota *)nota
{
    if (nota) {
        _backgroundImageView.hidden = YES;
        //_textView.editable = NO;
        _selectNota = nota;
        _textView.text = nota.descricao;;
        
        if (self.selectNota.arquivo != nil) {
            self.abrirArquivoButton.hidden = NO;
        } else {
            self.abrirArquivoButton.hidden = YES;
        }
        
    } else {
        _backgroundImageView.hidden = NO;
        self.abrirArquivoButton.hidden = YES;
    }
}


#pragma mark - UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView;
{
    if ([textView.text isEqualToString:@"Nova Nota"]) {
        textView.text = nil;
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView;
{
    if (textView.text.length == 0) {
        textView.text = @"Nova Nota";
    }
    
    // Firebase
    NSError *error = nil;
    if (_selectNota) {
        if ([self.moc existingObjectWithID:_selectNota.objectID error:&error]) {
            [_selectNota saveFirebase];
            [self saveMOC];
//            [_selectNota saveParse:self.moc];
        }
    }
}

- (void)textViewDidChange:(UITextView *)textView
{
    _selectNota.timeStamp = [NSDate date];
    _selectNota.descricao = textView.text;
    
    _selectNota.version = [NSDate date];
    
    [self saveMOC];
    
    [Utils postEntitytNotification:@"Nota"];
}

//- (void)enableEditing
//{
//    _textView.editable = YES;
//}


@end
