//
//  EditarPerfilViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 12/7/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "BaseTableViewController.h"

@class PerfilCliente;
@class SubtipoCliente;
@interface EditarPerfilViewController : BaseTableViewController

@property (nonatomic, strong) PerfilCliente *selectedPerfil;
@property (nonatomic, strong) SubtipoCliente *selectedTipo;

@end
