//
//  EditarPerfilViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 12/7/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "EditarPerfilViewController.h"
#import "Valor.h"
#import "Estado.h"
#import "Cidade.h"
#import "Bairro.h"
#import "PerfilCliente.h"
#import "TipoImovel.h"
#import "TipoPerfilCliente.h"
#import "SubtipoCliente.h"
#import "EstadosViewController.h"
#import "CidadesViewController.h"
#import "BairrosPerfilViewController.h"
#import "TipoImovelViewController.h"
#import "Utils.h"
#import "Area.h"
#import "PerfilCliente+Firebase.h"

@interface EditarPerfilViewController () <BairroPerfilDelegate>

@property (nonatomic, weak) IBOutlet UISlider *quartosSlider;
@property (nonatomic, weak) IBOutlet UISlider *valorSlider;
@property (nonatomic, weak) IBOutlet UISlider *tipoSlider;

@property (weak, nonatomic) IBOutlet UIStepper *quartosStepper;
@property (weak, nonatomic) IBOutlet UIStepper *valorStepper;

@property (nonatomic, weak) IBOutlet UILabel *quartosLabel;
@property (nonatomic, weak) IBOutlet UILabel *valorLabel;
@property (nonatomic, weak) IBOutlet UILabel *tipoLabel;

@property (nonatomic, weak) IBOutlet UILabel *estadoLabel;
@property (nonatomic, weak) IBOutlet UILabel *cidadeLabel;
@property (nonatomic, weak) IBOutlet UILabel *bairroLabel;

@property (nonatomic, weak) IBOutlet UITextField *nomeTextField;


@property (nonatomic) int selectedQuartos;
@property (nonatomic, strong) Valor *selectedValor;
@property (nonatomic, strong) TipoImovel *selectedTipoImovel;


@property (nonatomic, strong) NSArray *valores;
@property (nonatomic, strong) NSArray *areas;
@property (nonatomic, strong) NSArray *tiposImovel;

@property (nonatomic, strong) NSString *selectedEstado;
@property (nonatomic, strong) NSString *selectedCidade;
@property (nonatomic, strong) NSArray *selectedBairros;

@property (weak, nonatomic) IBOutlet UITextField *areaUtilTextField;

@end

@implementation EditarPerfilViewController

@synthesize quartosSlider,valorSlider,tipoSlider;
@synthesize estadoLabel,cidadeLabel,bairroLabel;
@synthesize quartosLabel,valorLabel,tipoLabel;
@synthesize selectedQuartos,selectedValor,selectedTipoImovel;
@synthesize valores,areas,tiposImovel;
@synthesize selectedEstado,selectedCidade,selectedBairros;
@synthesize nomeTextField;
@synthesize selectedPerfil;
@synthesize selectedTipo;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSPredicate *predicate;
    if ([selectedTipo.descricao isEqualToString:@"Comprar"]) {
        predicate = [NSPredicate predicateWithFormat:@"tipo.codigo = 2"];
    } else if ([selectedTipo.descricao isEqualToString:@"Alugar"]) {
        predicate = [NSPredicate predicateWithFormat:@"tipo.codigo = 1"];
        valorSlider.maximumValue = 17;
    }
    
    valores = [Valor MR_findAllSortedBy:@"codigo" ascending:YES withPredicate:predicate inContext:self.moc];
    
    self.valorStepper.maximumValue = valores.count - 1;
    
    areas = [Area MR_findAllSortedBy:@"codigo" ascending:YES inContext:self.moc];
    tiposImovel = [TipoImovel MR_findAllInContext:self.moc];
    
    
    if (self.selectedPerfil != nil) {
        [self loadPerfil];
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Tela Edicao Perfil"];
    [Utils trackFlurryTimedWithName:@"Tela Edicao Perfil"];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [Flurry endTimedEvent:@"Tela Edicao Perfil" withParameters:nil];
}


#pragma mark - Load
- (void)loadPerfil
{
    nomeTextField.text = selectedPerfil.descricao;
    
    quartosLabel.text = [self formatQuartos:[selectedPerfil.quartos floatValue]];
    quartosSlider.value = [selectedPerfil.quartos floatValue];
    self.quartosStepper.value = [selectedPerfil.quartos floatValue];
    
    NSPredicate *predicate;
    if ([selectedTipo.descricao isEqualToString:@"Comprar"]) {
        predicate = [NSPredicate predicateWithFormat:@"tipo.codigo = 2 AND codigo = %@",selectedPerfil.valor];
        
    } else if ([selectedTipo.descricao isEqualToString:@"Alugar"]) {
        predicate = [NSPredicate predicateWithFormat:@"tipo.codigo = 1 AND codigo = %@",selectedPerfil.valor];
    }
    
    selectedValor = [Valor MR_findFirstWithPredicate:predicate sortedBy:@"codigo" ascending:YES inContext:self.moc];
    
    predicate = [NSPredicate predicateWithFormat:@"codigo = %@",selectedValor.codigo];
    Valor *valor = [[valores filteredArrayUsingPredicate:predicate] firstObject];
    valorLabel.text = valor.descricao;
    long selectedIndex = [valores indexOfObject:valor];
    valorSlider.value = selectedIndex;
    self.valorStepper.value = selectedIndex;
    
    _areaUtilTextField.text = [selectedPerfil.area stringValue];
    
    predicate = [NSPredicate predicateWithFormat:@"codigo = %@",selectedPerfil.tipoImovel];
    selectedTipoImovel = [TipoImovel MR_findFirstWithPredicate:predicate inContext:self.moc];
    tipoLabel.text = selectedTipoImovel.descricao;
    selectedIndex = [tiposImovel indexOfObject:selectedTipoImovel];
    tipoSlider.value = selectedIndex;
    
    
    selectedEstado = selectedPerfil.estado;
    estadoLabel.text = selectedPerfil.estado;
    
    
    cidadeLabel.text = selectedPerfil.cidade;
    selectedCidade = selectedPerfil.cidade;
    
    
    selectedBairros = [selectedPerfil.bairro allObjects];
    bairroLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)selectedPerfil.bairro.count];
}


#pragma mark - Save
- (IBAction)save:(id)sender
{
    if ([self formIsValid]) {
        
        PerfilCliente *perfil;
        
        if (selectedPerfil == nil) {
            perfil = [PerfilCliente MR_createInContext:self.moc];
            perfil.codigo = [perfil getUniqueCode]; //[NSNumber numberWithLong:count];
            
        } else {
            perfil = selectedPerfil;
//            perfil.pendingUpdateParse = [NSNumber numberWithBool:YES];
        }
        
        perfil.descricao = nomeTextField.text;
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            perfil.quartos = [NSNumber numberWithFloat:quartosSlider.value];
        } else {
            perfil.quartos = [NSNumber numberWithFloat:self.quartosStepper.value];
        }
        
        perfil.valor = selectedValor.codigo;
        perfil.area = [NSNumber numberWithInt:[self.areaUtilTextField.text intValue]];
        perfil.tipoImovel = selectedTipoImovel.codigo;
        perfil.estado = selectedEstado;
        perfil.cidade = selectedCidade;
        perfil.bairro = [NSSet setWithArray:selectedBairros];
        
        NSNumber *codigo = nil;
        
        if ([self.selectedTipo.descricao isEqualToString:@"Alugar"]) {
            codigo = [NSNumber numberWithInt:1];
            
        } else if ([self.selectedTipo.descricao isEqualToString:@"Comprar"]) {
            codigo = [NSNumber numberWithInt:2];
        }
        perfil.tipoPerfil = codigo;
        
        // Firebase
        [perfil saveFirebase];
        
        [self saveMOC];
        
        // Parse SDK
//        [perfil saveParse:self.moc];
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }
}


- (BOOL)formIsValid
{
    
    [self resignKeyboard];
    
    BOOL isValid = YES;
    
    if (nomeTextField.text.length == 0) {
        [Utils addRedCorner:nomeTextField];
        isValid = NO;
    }
    
    return isValid;
    
}


#pragma mark - Quartos Slider
- (IBAction)sliderQuartosValueChanged:(UISlider *)sender
{
    int value = roundf(sender.value);
    selectedQuartos = value;
    quartosLabel.text  = [self formatQuartos:value];
}

#pragma mark - Valor Slider
- (IBAction)sliderValorValueChanged:(UISlider *)sender
{
    int value = roundf(sender.value);
    selectedValor = [valores objectAtIndex:value];
    
    valorLabel.text  = selectedValor.descricao;
}

#pragma mark - Tipo Imovel Slider
- (IBAction)sliderTipoImovelValueChanged:(UISlider *)sender
{
    int value = roundf(sender.value);
    selectedTipoImovel = [tiposImovel objectAtIndex:value];
    
    tipoLabel.text  = selectedTipoImovel.descricao;
}


#pragma mark - BairroPerfilDelegate
- (void)didSelectBairros:(NSArray *)bairros
{
    selectedBairros = bairros;
    bairroLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)bairros.count];
}


#pragma mark - Stepper - Used on iPhone

- (IBAction)stepperQuartosValueChanged:(UIStepper *)sender
{
    int value = roundf(sender.value);
    selectedQuartos = value;
    quartosLabel.text  = [self formatQuartos:value];
}

- (IBAction)stepperValorValueChanged:(UIStepper *)sender
{
    int value = roundf(sender.value);
    selectedValor = [valores objectAtIndex:value];
    
    valorLabel.text  = selectedValor.descricao;
}

#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self resignKeyboard];
    
    if ([segue.identifier isEqualToString:@"EstadosSegue"]) {
        
        
    }else if ([segue.identifier isEqualToString:@"CidadesSegue"]) {
        CidadesViewController *controller = segue.destinationViewController;
        controller.selectedEstado = selectedEstado;
    } else if ([segue.identifier isEqualToString:@"BairrosSegue"]) {
        BairrosPerfilViewController *controller = segue.destinationViewController;
        controller.delegate=  self;
        controller.selectedCidade = selectedCidade;
        controller.selectedBairros = [selectedBairros mutableCopy];
    }
}


#pragma mark - Unwind Segue

- (IBAction)unwindFromEstados:(UIStoryboardSegue *)segue
{
    EstadosViewController *controller = segue.sourceViewController;
    selectedEstado = controller.selectedEstado;
    
    estadoLabel.text = selectedEstado;
}

- (IBAction)unwindFromCidades:(UIStoryboardSegue *)segue
{
    CidadesViewController *controller = segue.sourceViewController;
    selectedCidade = controller.selectedCidade;
    
    cidadeLabel.text = selectedCidade;
}

- (IBAction)unwindFromTipoImovel:(UIStoryboardSegue *)segue
{
    TipoImovelViewController *controller = segue.sourceViewController;
    selectedTipoImovel = controller.selectedTipoImovel;
    
    tipoLabel.text  = selectedTipoImovel.descricao;
}


@end
