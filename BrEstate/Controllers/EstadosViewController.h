//
//  EstadosViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 12/7/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "BaseTableViewController.h"

@interface EstadosViewController : BaseTableViewController

@property (nonatomic, strong) NSString *selectedEstado;

@end
