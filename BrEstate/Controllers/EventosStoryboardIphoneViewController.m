//
//  EventosStoryboardIphoneViewController.m
//  Appci
//
//  Created by BrEstate LTDA on 07/03/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "EventosStoryboardIphoneViewController.h"

@interface EventosStoryboardIphoneViewController ()

@end

@implementation EventosStoryboardIphoneViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Eventos_iPhone" bundle:nil];
    
    // Load the initial view controller from the storyboard.
    UINavigationController *navController = [storyboard instantiateInitialViewController];
    UIViewController *controller = [[navController viewControllers] objectAtIndex:0];
    
    [self.navigationController setViewControllers:@[controller] animated:NO];
}


@end
