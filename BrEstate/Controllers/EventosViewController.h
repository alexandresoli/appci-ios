//
//  CompromissosViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 10/23/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Evento.h"


@interface EventosViewController : BaseViewController

@property (nonatomic, strong) Evento *selectedEvento;

- (void)startImportFileURL:(NSURL *)url;

@end
