//
//  CompromissosViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 10/23/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "EventosViewController.h"
#import "ListaEventosViewController.h"
#import "DetalhesEventoViewController.h"
#import "Carga.h"
#import "AddressBookHelper.h"
#import "ImportarArquivoViewController.h"
#import "SVProgressHUD.h"

@interface EventosViewController ()

@property (nonatomic, strong) ListaEventosViewController *listaEventosController;
@property (nonatomic, strong) NSURL *urlArquivoImportacao;

@end

@implementation EventosViewController


#pragma mark - View Life Cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // same order from storyboard controllers
    //  NSLog(@"children : %@", self.childViewControllers);
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.listaEventosController = [self.childViewControllers objectAtIndex:0];
        self.listaEventosController.detalhesEventoViewController = [self.childViewControllers objectAtIndex:1];
        self.listaEventosController.detalhesEventoViewController.superNavigationController = self.navigationController;
        self.listaEventosController.selectedEvento = self.selectedEvento;
    }


}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [Utils trackWithName:@"Aba Agenda"];
//    [Utils trackFlurryWithName:@"Aba Agenda"];
    
    [self configureAlert];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *helpWasShown = [defaults objectForKey:HELP_WAS_SHOWN];
    
    if (!helpWasShown) {
        [self performSegueWithIdentifier:@"HelpSegue" sender:self];
    }
}

#pragma mark - Import File

- (void)startImportFileURL:(NSURL *)url
{
    self.urlArquivoImportacao = url;
    
    [self dismissViewControllerAnimated:NO completion:^{
        [self performSegueWithIdentifier:@"ImportarArquivoSegue" sender:self];
    }];
    
    [self performSegueWithIdentifier:@"ImportarArquivoSegue" sender:self];
}

#pragma mark - Prepare For Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ImportarArquivoSegue"]) {
        ImportarArquivoViewController *controller = segue.destinationViewController;
        controller.urlArquivo = self.urlArquivoImportacao;
    }
}


@end
