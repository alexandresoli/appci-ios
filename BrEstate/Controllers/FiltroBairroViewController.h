//
//  FiltroBairroViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/17/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Bairro;

@interface FiltroBairroViewController : BaseViewController

@property (nonatomic, strong) Bairro *selectedBairro;

@end
