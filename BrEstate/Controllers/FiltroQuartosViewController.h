//
//  FiltroQuartosViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/17/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FiltroQuartosViewController : BaseViewController

@property (nonatomic, assign) int minimoQuartos;

- (IBAction) sliderValueChanged:(UISlider *)sender;

@end
