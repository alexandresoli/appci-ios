//
//  FiltroQuartosViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/17/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "FiltroQuartosViewController.h"

@interface FiltroQuartosViewController ()

@property (nonatomic, weak) IBOutlet UILabel  *valueLabel;
@property (nonatomic, weak) IBOutlet UISlider *slider;
@end

@implementation FiltroQuartosViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [Utils trackWithName:@"Filtro Quarto"];
    [Utils trackFlurryWithName:@"Filtro Quarto"];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)sliderValueChanged:(UISlider *)sender
{
    int quantidade = roundf(sender.value);
    NSString *message = @"";
    
    if (quantidade == 0) {
        message = @"indiferente";
    } else if (quantidade == 1) {
        message = [NSString stringWithFormat:@"%d quarto", quantidade];
    } else if (quantidade == 10) {
        message = [NSString stringWithFormat:@"%d quartos ou mais", quantidade];
    } else {
        message = [NSString stringWithFormat:@"%d quartos", quantidade];
    }
    
    _valueLabel.text  = message;
    self.minimoQuartos = quantidade;
    
    
}

- (IBAction)save:(id)sender
{
    [self performSegueWithIdentifier:@"UnwindFromFiltroQuartos" sender:self];

}

@end
