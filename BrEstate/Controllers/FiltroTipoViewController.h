//
//  FiltroTipoViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/17/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TipoImovel;

@interface FiltroTipoViewController : BaseViewController

@property (nonatomic, strong) NSString *selectedTipoImovel;

@end
