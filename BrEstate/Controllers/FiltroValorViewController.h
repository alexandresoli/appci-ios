//
//  FiltroValorViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/17/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FiltroValorViewController : BaseViewController

@property (nonatomic, assign) float valor;

- (IBAction) sliderValueChanged:(UISlider *)sender;

@end
