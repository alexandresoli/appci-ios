//
//  FiltroValorViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/17/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "FiltroValorViewController.h"

@interface FiltroValorViewController ()

@property (nonatomic, weak) IBOutlet UILabel  *valueLabel;
@property (nonatomic, weak) IBOutlet UISlider *slider;

@property (nonatomic) int lastStep;
@property (nonatomic) int stepValue;

@end

@implementation FiltroValorViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.valor = 10000.0f;
    self.stepValue = 50000.0f;
    self.lastStep = (self.slider.value) / self.stepValue;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [Utils trackWithName:@"Filtro Valor"];
    [Utils trackFlurryWithName:@"Filtro Valor"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)sliderValueChanged:(UISlider *)sender
{
    float valor = roundf((sender.value) / self.stepValue);
    
    self.slider.value = valor * self.stepValue;;
    
    _valueLabel.text  = [NSString localizedStringWithFormat:@"R$ %.2f",sender.value];;
    
    self.valor = self.slider.value;
    
    
    if ([_valueLabel.text isEqualToString:@"R$ 5.000.000,00"]) {
        _valueLabel.text = @"Acima de 5 milhões";
    }
    
}

- (IBAction)save:(id)sender
{
    [self performSegueWithIdentifier:@"UnwindFromFiltroValor" sender:self];
    
}

@end
