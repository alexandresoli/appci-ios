//
//  ForgotPasswordViewController.h
//  Appci
//
//  Created by Alexandre Oliveira on 9/23/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "BaseViewController.h"

@interface ForgotPasswordViewController : BaseViewController

@property (nonatomic, strong) NSString *email;

@end
