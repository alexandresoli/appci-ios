//
//  ForgotPasswordViewController.m
//  Appci
//
//  Created by Alexandre Oliveira on 9/23/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "NSString+Custom.h"

@interface ForgotPasswordViewController ()

@property (nonatomic, weak) IBOutlet UITextField *emailTextField;

@end


@implementation ForgotPasswordViewController


#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // textfield image
    UIImageView *emailImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 16, 11)];
    emailImageView.image = [UIImage imageNamed:@"mail"];
    
    UIView *emailPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 24)];
    [emailPaddingView addSubview:emailImageView];
    
    self.emailTextField.leftView = emailPaddingView;
    self.emailTextField.leftViewMode = UITextFieldViewModeAlways;
    
    if (self.email) {
        self.emailTextField.text = self.email;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Request

- (IBAction)request:(id)sender
{
    if (_emailTextField.text.length == 0) {
        [Utils addRedCorner:_emailTextField];
        return;
    }
    
    if (![Utils isConnected]) {
        [Utils showMessage:@"Verifique sua conexão com a internet e tente novamente. Se o erro persistir entre em contato com suporte@brestate.com.br."];
        return;
    }
    
    [Utils startNetworkIndicator];
    
    [Utils showProgress:@"Aguarde..."];
    
    [[Utils firebaseUtils].rootRefFirebase resetPasswordForUser:self.emailTextField.text withCompletionBlock:^(NSError *error) {
        
        if (error) {
            [Utils stopNetworkIndicator];
            [Utils dismissProgress];
            [Utils showMessage:[[Utils firebaseUtils] messageForError:error]];
            return;
        }
        
        [Utils showMessage:@"Uma senha temporária foi enviada para o e-mail especificado. Utilize esta senha para fazer o login'"];
        [Utils dismissProgress];
        [Utils stopNetworkIndicator];
        
        [Utils addObjectToDefaults:_emailTextField.text.trimmed withKey:USER_LOGIN];
        
        [self performSegueWithIdentifier:@"UnwindFromLostPassword" sender:self];
        
        [self close:nil];

    }];
}


#pragma mark - Close

- (IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:Nil];
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _emailTextField) {
        [textField resignFirstResponder];
        [self request:nil];
    }
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}


#pragma mark - Dismiss Keyboard

- (IBAction)dismissKeyboard:(id)sender
{
    [self.view endEditing:YES];
}


@end
