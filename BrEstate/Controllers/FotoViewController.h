//
//  FotoViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/4/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Nota;
@interface FotoViewController : UIViewController
@property (nonatomic, strong) Nota *selectedNota;
@end
