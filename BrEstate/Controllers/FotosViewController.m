//
//  FotosViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/20/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "FotosViewController.h"
#import "MostrarFotoViewController.h"
#import "FotoCell.h"
#import "FotosHeaderView.h"
#import "FotoImovel+InsertDeleteImagem.h"
#import "Imovel.h"
#import "CTAssetsPickerController.h"
#import "SVProgressHUD.h"
#import "ImageUtils.h"
#import "Deduplicator.h"
#import "FotoImovel+Firebase.h"
#import "Imovel+Firebase.h"
#import "S3Utils.h"
#import "FotoImovel+InsertDeleteImagem.h"
@import ImageIO;

@import MediaPlayer;
@import AVFoundation;

// Bug - http://stackoverflow.com/questions/11928885/uipopovercontroller-orientation-crash-in-ios-6
@interface UIImagePickerController (Orientation)

-(BOOL)shouldAutorotate;
- (NSUInteger)supportedInterfaceOrientations;

@end

@implementation UIImagePickerController (Orientation)

-(BOOL)shouldAutorotate
{
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    
    // ATTENTION! Only return orientation MASK values
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        return UIInterfaceOrientationMaskLandscape;
    } else {
        return UIInterfaceOrientationMaskPortrait;
    }
}

@end

@interface FotosViewController () <UICollectionViewDataSource,UICollectionViewDelegate,NSFetchedResultsControllerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, CTAssetsPickerControllerDelegate>

@property (nonatomic, weak) IBOutlet UICollectionView *fotosCollectionView;
@property (nonatomic, weak) IBOutlet UISegmentedControl *segmentControl;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) UIActionSheet *actionSheetDelete;


@property (nonatomic, strong) NSPredicate *predicate;
@property (nonatomic, strong) FotoImovel *selectedFoto;
@property (nonatomic, strong) UIPopoverController *popover;
@property (nonatomic, strong) NSCache *fotosCache;
@property (nonatomic, strong) MPMoviePlayerController *videoPlayer;

@end

@implementation FotosViewController

@synthesize fotosCollectionView;
@synthesize fetchedResultsController;
@synthesize selectedImovel;
@synthesize selectedFoto;

#pragma mark - Queue

dispatch_queue_t queueLoadFoto;


#pragma mark - View Life Cycle

- (BOOL)prefersStatusBarHidden
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        return YES;
    } else {
        return NO;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    fotosCollectionView.delegate = self;
    fotosCollectionView.dataSource = self;
    
    
    // add model changes notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadFetchedResults:) name:@"FotoImovel" object:nil];
    
    queueLoadFoto = dispatch_queue_create("FotosViewController.LoadFoto", NULL);
}


- (NSCache *)fotosCache
{
    if (_fotosCache) {
        return _fotosCache;
    }
    
    _fotosCache = [[NSCache alloc] init];
    [_fotosCache setCountLimit:15];
    
    return _fotosCache;
}


#pragma mark - Icloud notification
- (void)reloadFetchedResults:(NSNotification*)note
{
    self.fetchedResultsController = nil;
    [self.fetchedResultsController performFetch:nil];
    [self.fotosCollectionView reloadData];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
    
    [Utils trackWithName:@"Tela Fotos"];
    [Utils trackFlurryTimedWithName:@"Tela Fotos"];
    
    
    // add Icloud changes notification
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadFetchedResults:) name:ICLOUD_UPDATED object:nil];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [Flurry endTimedEvent:@"Tela Fotos" withParameters:nil];
}


// Bug - http://stackoverflow.com/questions/11928885/uipopovercontroller-orientation-crash-in-ios-6
-(BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark- UICollectionViewDataSource,UICollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    
    return [sectionInfo numberOfObjects];
}




- (void)configureCell:(NSIndexPath *)indexPath cell:(FotoCell *)cell
{
    
    FotoImovel *fotoImovel = [fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.favoriteButton.selected = [fotoImovel.capa boolValue];
    
    cell.label.text = fotoImovel.descricao;
    cell.imageView.image = [self.fotosCache objectForKey:indexPath];
    
    if (!cell.imageView.image) {
        dispatch_async(queueLoadFoto, ^{
            
            UIImage *image = [fotoImovel getImageForFotoImovelWithMaxPixelSize:500];
            
            if (image) {
                [self.fotosCache setObject:image forKey:indexPath];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                FotoCell *updateCell = (FotoCell *)[self.fotosCollectionView cellForItemAtIndexPath:indexPath];
                [UIView transitionWithView:updateCell.imageView
                                  duration:0.2f
                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                animations:^{
                                    updateCell.imageView.image = image;
                                } completion:NULL];
            });
            
        });
        
    }
    
    cell.layerImageView.hidden = ([fotoImovel.video boolValue] == YES) ? NO : YES;
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FotoCell" forIndexPath:indexPath];
    
    [self configureCell:indexPath cell:cell];
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    FotosHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"FotosHeaderView" forIndexPath:indexPath];
    
    FotoImovel *fotoImovel = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if ([fotoImovel.descricao rangeOfString:@"Foto"].location != NSNotFound) {
        [headerView setSearchText:@"Fotos"];
    } else {
        [headerView setSearchText:@"Videos"];
    }
    
    return headerView;
}



- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(20, 20, 20, 20);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    FotoImovel *fotoImovel = [fetchedResultsController objectAtIndexPath:indexPath];
    selectedFoto = fotoImovel;
    
    if ([fotoImovel.video boolValue] == YES) {
        [self playVideo];
    } else {
        
        [self performSegueWithIdentifier:@"MostrarFotoSegue" sender:fotoImovel];
        [self.fotosCollectionView deselectItemAtIndexPath:indexPath animated:YES];
    }
}

#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (fetchedResultsController != nil && self.predicate == nil) {
        return fetchedResultsController;
    }
    
    if (selectedImovel != nil) {
        [self reloadFetchResultsController];
    }
    
    fetchedResultsController.delegate = self;
    
    if (self.predicate == nil) {
        self.predicate = [NSPredicate predicateWithFormat:@"imovel = %@ AND (video = null OR video = NO)",selectedImovel];
    }
    
    fetchedResultsController = [FotoImovel MR_fetchAllSortedBy:@"version"
                                                     ascending:YES
                                                 withPredicate:self.predicate
                                                       groupBy:nil
                                                      delegate:self
                                                     inContext:self.moc];
    //[CoreDataUtils fetchedResultsControllerWithAlphabetic:@"FotoImovel" andKey:@"descricao" withPredicate:self.predicate withOrder:YES withLimit:0 inContext:self.moc];
    
    self.predicate = nil;
    
    return fetchedResultsController;
}

- (void)reloadFetchResultsController
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"imovel = %@ and (video = null OR video = NO)",selectedImovel];
    
    fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithAlphabetic:@"FotoImovel" andKey:@"descricao" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    //    UICollectionView *collectionView = fotosCollectionView;
    FotoCell *fotoCell = (FotoCell *) [fotosCollectionView cellForItemAtIndexPath:indexPath];
    
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            //            [collectionView insertItemsAtIndexPaths:@[newIndexPath]];
            break;
            
        case NSFetchedResultsChangeDelete:
            //            [collectionView deleteItemsAtIndexPaths:@[indexPath]];
            break;
        case NSFetchedResultsChangeMove:
            break;
        case NSFetchedResultsChangeUpdate:
            [self configureCell:indexPath cell:fotoCell];
            break;
    }
}


#pragma mark - Close
- (IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Add Photo
- (IBAction)addPhoto:(UIButton *)button
{
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Selecione"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancelar"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Tirar Foto",@"Foto Existente",@"Gravar Video", @"Video Existente",nil];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        actionSheet.actionSheetStyle = UIActionSheetStyleAutomatic;
        
        UIView *view= (UIView *)[self.navigationController.navigationBar.subviews objectAtIndex:2];
        [actionSheet showFromRect: view.frame inView:self.view animated:YES];
        
    } else {
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    }
    
}

#pragma mark - Segment Control
- (IBAction)changeSegment:(UISegmentedControl  *)segment
{
    
    switch (segment.selectedSegmentIndex) {
        case 0:
            self.predicate = [NSPredicate predicateWithFormat:@"imovel = %@ AND (video = null OR video = NO)",selectedImovel];
            break;
        case 1:
            self.predicate = [NSPredicate predicateWithFormat:@"imovel = %@  AND video = YES",selectedImovel];
            break;
            
        default:
            break;
    }
    
    [self.fotosCache removeAllObjects];
    [Utils postEntitytNotification:@"FotoImovel"];
    
}

#pragma mark - Favorite Photo
- (IBAction)favoritePhoto:(UIButton *)button
{
    if (![self.iapHelper checkPlanTopFromController:self withMessage:PLAN_TOP_MESSAGE_FAVORITE_PHOTO]) {
        return;
    }
    
    CGPoint buttonPosition = [button convertPoint:CGPointZero toView:self.fotosCollectionView];
    NSIndexPath *indexPath = [self.fotosCollectionView indexPathForItemAtPoint:buttonPosition];
    
    selectedFoto = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // nothing to change
    if ([selectedFoto.capa boolValue] == YES) {
        return;
    }
    
    if ([selectedFoto.video boolValue] == YES) {
        return;
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"capa = YES"];
    NSArray *photos = [self.fetchedResultsController fetchedObjects];
    photos = [photos filteredArrayUsingPredicate:predicate];
    
    FotoImovel *favoritePhoto = photos.firstObject;
    
    if (favoritePhoto != nil) {
        favoritePhoto.capa = [NSNumber numberWithBool:NO];
    }
    
    selectedFoto.capa = [NSNumber numberWithBool:YES];
    
    [selectedImovel saveFirebase];
    
    [self.moc MR_saveToPersistentStoreAndWait];
    
    [Utils postEntitytNotification:@"FotoImovel"];
    [Utils postEntitytNotification:@"Imovel"];
    
}

#pragma mark - Delete Photo
- (void)removePhoto
{
    
    // Firebase
    [selectedFoto deleteFirebase];
    
    // amazon s3
    [selectedFoto deleteImageFromS3];
    
    [selectedFoto deleteFromDisk];
    
    [selectedFoto MR_deleteInContext:self.moc];
    
    [self.moc MR_saveToPersistentStoreAndWait];
    
    [self.fotosCache removeAllObjects];
    [Utils postEntitytNotification:@"FotoImovel"];
    [Utils postEntitytNotification:@"Imovel"];
    
}


- (IBAction)deletePhoto:(UIButton *)button
{
    
    CGPoint buttonPosition = [button convertPoint:CGPointZero toView:self.fotosCollectionView];
    NSIndexPath *selectedIndexPath = [self.fotosCollectionView indexPathForItemAtPoint:buttonPosition];
    
    _actionSheetDelete = [[UIActionSheet alloc] initWithTitle:@"Aviso"
                                                     delegate:self
                                            cancelButtonTitle:@"Cancelar"
                                       destructiveButtonTitle:@"Apagar"
                                            otherButtonTitles:nil];
    
    selectedFoto = [fetchedResultsController objectAtIndexPath:selectedIndexPath];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
//        FotoCell *fotoCell = (FotoCell *) [fotosCollectionView cellForItemAtIndexPath:selectedIndexPath];
//        [_actionSheetDelete showFromRect:fotoCell.frame inView:self.view animated:YES];
        [_actionSheetDelete showInView:button];
        
    } else {
        [_actionSheetDelete showFromTabBar:self.tabBarController.tabBar];
    }
    
    
}

#pragma mark - UIActionSheetDelegate
// iOS 8 fix: http://stackoverflow.com/questions/24942282/uiimagepickercontroller-not-presenting-in-ios-8
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch (buttonIndex) {
        case 0:
            
            
            if ([actionSheet isEqual:_actionSheetDelete]) {
                [self removePhoto];
                
            } else {
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self takePhoto];
                }];
                
                selectedFoto = nil;
                
            }
            break;
            
        case 1:
            
            if (![actionSheet isEqual:_actionSheetDelete]) {
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self cameraRoll:@"Fotos"];
                }];
                
                selectedFoto = nil;
            }
            
            break;
            
        case 2:
        {
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self takeVideo];
            }];
            selectedFoto = nil;
            break;
        }
            
        case 3:
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self cameraRoll:@"Videos"];
            }];
            
            selectedFoto = nil;
            break;
        }
            
        default:
            selectedFoto = nil;
            return;
    }
}



#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"MostrarFotoSegue"])
    {
        MostrarFotoViewController *controller = segue.destinationViewController;
        controller.selectedImovel = self.selectedImovel;
        controller.selectedFoto = self.selectedFoto;
    }
}


#pragma mark - Photo
- (void)takePhoto
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
#if TARGET_IPHONE_SIMULATOR
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary | UIImagePickerControllerSourceTypeSavedPhotosAlbum;
#else
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.editing = YES;
    imagePickerController.allowsEditing = YES;
    imagePickerController.showsCameraControls = YES;
#endif
    imagePickerController.delegate = (id)self;
    
    [self.fotosCache removeAllObjects];
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (void)cameraRoll:(NSString *)tipo
{
    CTAssetsPickerController *picker = [[CTAssetsPickerController alloc] init];
    picker.delegate = self;
    
    if([tipo isEqualToString:@"Fotos"]) {
        picker.assetsFilter = [ALAssetsFilter allPhotos];
    } else if([tipo isEqualToString:@"Videos"]) {
        picker.assetsFilter = [ALAssetsFilter allVideos];
    }
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        _popover = [[UIPopoverController alloc] initWithContentViewController:picker];
        
        UIView *view= (UIView *)[self.navigationController.navigationBar.subviews objectAtIndex:2];
        
        [_popover presentPopoverFromRect:view.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    } else {
        [self presentViewController:picker animated:YES completion:nil];
    }
    
    
}


#pragma mark - CTAssetsPickerControllerDelegate

- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    [SVProgressHUD showWithStatus:@"Processando..." maskType:SVProgressHUDMaskTypeBlack];
    
    NSArray *array = @[picker, assets];
    
    // Execução em método separado para que seja exibido o SVProgress e
    // por causa do iPhone 4 onde executando em outras threads o processo era encerrado sem concluir.
    [self performSelector:@selector(processPhotosOrVideos:) withObject:array afterDelay:0.1];
}


- (void)processPhotosOrVideos:(NSArray *)pickerAndAssets
{
    CTAssetsPickerController *picker = [pickerAndAssets firstObject];
    NSArray *assets = [pickerAndAssets lastObject];
    
    for (ALAsset *asset in assets) {
        
        if([[asset valueForProperty:@"ALAssetPropertyType"] isEqualToString:@"ALAssetTypePhoto"]) {
            
            @autoreleasepool {
                
                NSDictionary *options = @{
                                          (id) kCGImageSourceCreateThumbnailWithTransform : @YES,
                                          (id) kCGImageSourceCreateThumbnailFromImageAlways : @YES,
                                          (id) kCGImageSourceThumbnailMaxPixelSize : @(1280)
                                          };
                
                ALAssetRepresentation *assetRepresentation = [asset defaultRepresentation];
                CGImageRef imageRef = [assetRepresentation CGImageWithOptions:options]; //[assetRepresentation fullResolutionImage];
                
                UIImage *sourceImage = [UIImage imageWithCGImage:imageRef scale:assetRepresentation.scale orientation:(UIImageOrientation)assetRepresentation.orientation];
                
                imageRef = nil;
                assetRepresentation = nil;
                
                [self savePhoto:sourceImage];
                
                
            }
        } else if([[asset valueForProperty:@"ALAssetPropertyType"] isEqualToString:@"ALAssetTypeVideo"]) {
            
            @autoreleasepool {
                
                ALAssetRepresentation *rep = [asset defaultRepresentation];
                Byte *buffer = (Byte *)malloc((long)rep.size);
                NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:(long)rep.size error:nil];
                NSData *videoData = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];
                
                if (![Utils hasProperSize:videoData]) {
                    [Utils dismissProgress];
                    videoData = nil;
                    return;
                }
                
                [self saveVideo:videoData];
                
            }
            
            
        }
        
    }
    
    [self.moc MR_saveToPersistentStoreAndWait];
    
    [Utils dismissProgress];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    [_popover dismissPopoverAnimated:YES];
    
    [self.fotosCache removeAllObjects];
    [Utils postEntitytNotification:@"FotoImovel"];
}


//- (void)assetsPickerControllerDidCancel:(CTAssetsPickerController *)picker
//{
//    [picker dismissViewControllerAnimated:YES completion:nil];
//    [_popover dismissPopoverAnimated:YES];
//}


- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker shouldSelectAsset:(ALAsset *)asset
{
    if (picker.selectedAssets.count >= 10)
    {
        UIAlertView *alertView =
        [[UIAlertView alloc] initWithTitle:@"Atenção"
                                   message:@"Selecione no máximo 10 fotos por vez."
                                  delegate:nil
                         cancelButtonTitle:nil
                         otherButtonTitles:@"OK", nil];
        
        [alertView show];
        
        return NO;
        
    } else {
        return YES;
    }
}


#pragma mark - UIImagePickerControllerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    if([mediaType isEqualToString:@"public.image"]) {
        
        UIImage *sourceImage = [info objectForKey:UIImagePickerControllerEditedImage];
        
        [self savePhoto:sourceImage];
        
    }  else if ([mediaType isEqualToString:@"public.movie"]) {
        
        
        @autoreleasepool {
            
            NSURL *url = [info objectForKey:UIImagePickerControllerMediaURL];
            
            NSData *videoData = [NSData dataWithContentsOfURL:url];
            
            if (![Utils hasProperSize:videoData]) {
                [Utils dismissProgress];
                videoData = nil;
                return;
            }
            
            [self saveVideo:videoData];
        }
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    [_popover dismissPopoverAnimated:YES];
    
    [self.fotosCache removeAllObjects];
    [Utils postEntitytNotification:@"FotoImovel"];
}

#pragma mark - Video Save
- (void)saveVideo:(NSData *)videoData
{
    FotoImovel *fotoImovel;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"video = YES and imovel = %@",selectedImovel];
    long codigo = [FotoImovel MR_countOfEntitiesWithPredicate:predicate inContext:self.moc]+1;
    fotoImovel = [FotoImovel MR_createInContext:self.moc];
    fotoImovel.codigo = [fotoImovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
    
    fotoImovel.descricao =  [NSString stringWithFormat:@"Video %ld",codigo];
    fotoImovel.video = [NSNumber numberWithBool:YES];
    fotoImovel.imovel = selectedImovel;
    
    [fotoImovel insertVideoOnDisk:videoData];
    
    fotoImovel.version = [NSDate date];
    
    [self.moc MR_saveToPersistentStoreAndWait];
    
    // amazon s3
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [fotoImovel upload];
    });
    
//    [Utils postEntitytNotification:@"FotoImovel"];
//    [Utils dismissProgress];
}

#pragma mark - Photo Save
- (void)savePhoto:(UIImage *)image
{
    FotoImovel *fotoImovel;
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.6);
    
    if (![Utils hasProperSize:imageData]) {
        [Utils dismissProgress];
        imageData = nil;
        return;
    }
    
    fotoImovel = [FotoImovel MR_createInContext:self.moc];
    fotoImovel.codigo = [fotoImovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
    
    long imageCount = [selectedImovel.fotos count]+1;
    
    fotoImovel.descricao =  [NSString stringWithFormat:@"Foto %ld",imageCount] ;
    
    fotoImovel.imovel =  self.selectedImovel;
    
    [fotoImovel insertImageOnDisk:imageData];
    
    fotoImovel.version = [NSDate date];
    
    [self.moc MR_saveToPersistentStoreAndWait];
    
    // amazon s3
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [fotoImovel upload];
    });
    
//    [Utils postEntitytNotification:@"FotoImovel"];
//    [Utils dismissProgress];
}

#pragma mark - Play Video
-(void)playVideo
{
    
    NSURL *url = [selectedFoto getFotoImovelURL];
    
    _videoPlayer =  [[MPMoviePlayerController alloc] initWithContentURL:url];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:_videoPlayer];
    
    _videoPlayer.controlStyle = MPMovieControlStyleDefault;
    _videoPlayer.shouldAutoplay = YES;
    [self.view addSubview:_videoPlayer.view];
    [_videoPlayer setFullscreen:YES animated:YES];
}

- (void)moviePlayBackDidFinish:(NSNotification*)notification
{
    MPMoviePlayerController *player = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    
    if ([player respondsToSelector:@selector(setFullscreen:animated:)]){
        [player.view removeFromSuperview];
    }
}

#pragma mark - Record Video
- (void)takeVideo
{
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.delegate = (id)self;
    pickerController.allowsEditing = YES;
    pickerController.videoQuality = UIImagePickerControllerQualityTypeMedium;
    [pickerController setVideoMaximumDuration:60];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        NSArray* mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
        
        if ([mediaTypes containsObject:@"public.movie"]) {
            pickerController.mediaTypes = [NSArray arrayWithObject:@"public.movie"];
            [self presentViewController:pickerController animated:YES completion:nil];
        } else {
            [Utils showMessage:@"Gravação não disponível!"];
        }
    }
    else {
        [Utils showMessage:@"Dispositivo não possui camera!"];
    }
    
}




@end

