//
//  HelpDataSourceViewController.h
//  Appci
//
//  Created by BrEstate LTDA on 02/04/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpDataSourceViewController : UIViewController <UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageImages;

@end
