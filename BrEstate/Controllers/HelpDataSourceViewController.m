//
//  HelpDataSourceViewController.m
//  Appci
//
//  Created by BrEstate LTDA on 02/04/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "HelpDataSourceViewController.h"
#import "HelpPageContentViewController.h"

@interface HelpDataSourceViewController ()

@end


@implementation HelpDataSourceViewController


#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Define o array de imagens do help.
    if ([UIScreen mainScreen].scale >= 2) { // Retina display
        self.pageImages = @[@"ipad-help-1-bemvindo@2x.jpg", @"ipad-help-2-agenda@2x.gif", @"ipad-help-3-contatos@2x.gif", @"ipad-help-4-portfolio@2x.gif", @"ipad-help-5-notas@2x.gif", @"ipad-help-6-notficacoes@2x.jpg"];
        
    } else {
        self.pageImages = @[@"ipad-help-1-bemvindo.jpg", @"ipad-help-2-agenda.gif", @"ipad-help-3-contatos.gif", @"ipad-help-4-portfolio.gif", @"ipad-help-5-notas.gif", @"ipad-help-6-notficacoes.jpg"];
    }
    
    // Instancia o UIPageViewController
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpPageViewController"];
    self.pageViewController.dataSource = self;
    
    // Busca nas subViews do pageViewController o seu pageControl para alterar a aparência.
    NSArray *subViews = [self.pageViewController.view subviews];
    for (UIView *view in subViews) {
        if ([view isKindOfClass:[UIPageControl class]]) {
            UIPageControl *pageControl = (UIPageControl *)view;
            pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
            pageControl.currentPageIndicatorTintColor = [UIColor blueColor];
            pageControl.backgroundColor = [UIColor whiteColor];
        }
    }
    
    // Inicia com a primeira imagem do help.
    HelpPageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    //self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 30);
    
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
}


//- (BOOL)prefersStatusBarHidden
//{
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
//        return YES;
//    }
//    
//    return NO;
//}


#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = (((HelpPageContentViewController *) viewController).pageIndex);
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}


- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = (((HelpPageContentViewController *) viewController).pageIndex);
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageImages count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}


- (HelpPageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageImages count] == 0) || (index >= [self.pageImages count])) {
        return nil;
    }
    
    HelpPageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpPageContentViewController"];
    
    pageContentViewController.imageName = self.pageImages[index];
    
    pageContentViewController.pageIndex = index;
    
    if (index == [self.pageImages count] - 1) {
        pageContentViewController.isLastPage = YES;
    }
    
    return pageContentViewController;
}


- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageImages count];
}


- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}


@end
