//
//  HelpPageContentViewController.h
//  Appci
//
//  Created by BrEstate LTDA on 02/04/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YLImageView.h"

@interface HelpPageContentViewController : UIViewController

@property (weak, nonatomic) IBOutlet YLImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIButton *finishHelpButton;

@property (nonatomic, strong) NSString *imageName;
@property NSUInteger pageIndex;
@property BOOL isLastPage;

@end
