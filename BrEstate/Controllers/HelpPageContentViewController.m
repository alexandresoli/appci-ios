//
//  HelpPageContentViewController.m
//  Appci
//
//  Created by BrEstate LTDA on 02/04/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "HelpPageContentViewController.h"
#import "YLGIFImage.h"

@interface HelpPageContentViewController ()

@end


@implementation HelpPageContentViewController

#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.backgroundImageView.hidden = YES;
    
    if (self.isLastPage) {
        [self.finishHelpButton setTitle:@"Concluir" forState:UIControlStateNormal];
    }
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (!self.backgroundImageView.image) {
        self.backgroundImageView.image = [YLGIFImage imageNamed:self.imageName];
    }
    
    self.backgroundImageView.hidden = NO;
}


#pragma mark - IBActions

- (IBAction)finishHelp:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *sim = [NSNumber numberWithBool:YES];
    [defaults setObject:sim forKey:HELP_WAS_SHOWN];
    [defaults synchronize];
    
    [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
}


@end
