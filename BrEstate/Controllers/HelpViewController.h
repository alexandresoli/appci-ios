//
//  HelpViewController.h
//  Appci
//
//  Created by BrEstate LTDA on 2/26/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : BaseViewController

@property (nonatomic, strong) NSString *motivoProblema;

@end
