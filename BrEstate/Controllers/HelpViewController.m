//
//  HelpViewController.m
//  Appci
//
//  Created by BrEstate LTDA on 2/26/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "HelpViewController.h"
#import "ZendeskDropbox.h"
#import "SVProgressHUD.h"
#import "Utils.h"

//warning Verificar futuramente a API do zendesk pois não é compatível com a arquiterura 64 bits. Foi necessário alterar os Settings do projeto.

@interface HelpViewController () <ZendeskDropboxDelegate>

@property (nonatomic, weak) IBOutlet UITextField *emailTextField;
@property (nonatomic, weak) IBOutlet UITextField *subjectTextField;
@property (nonatomic, weak) IBOutlet UITextView *messageTextView;

@end

@interface HelpViewController() <ZendeskDropboxDelegate>

@end

@implementation HelpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _messageTextView.keyboardAppearance = UIKeyboardAppearanceDark;
    
    if ([self.motivoProblema isEqualToString:@"IntegrarCRM"]) {
        self.subjectTextField.text = @"Integração com CRM ou Site";
        
        self.messageTextView.text = @"Nome do CRM ou Site: \n \nEndereço do seu site: ";
        
        self.navigationItem.title = @"Integrar CRM";
        
        [self showMessage:@"Preencha o seu e-mail e informe os dados do seu CRM / Site que em breve entraremos em contato."];
        
    } else if ([self.motivoProblema isEqualToString:@"IntegreCRMImobiBrasil"]) {

        self.emailTextField.text = [[NSUserDefaults standardUserDefaults] valueForKey:USER_LOGIN];
        self.subjectTextField.text = @"Integração ImobiBrasil";
        
        self.messageTextView.text = @"Endereço do seu site: ";
        
        self.navigationItem.title = @"Integrar CRM ImobiBrasil";
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)close
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)validated
{
    if (_emailTextField.text.length == 0 || _subjectTextField.text.length == 0 || _messageTextView.text.length == 0) {
        [self showMessage:@"Todos os campos devem ser preenchidos!"];
        return NO;
    }
    
    if ([Utils validateEmail:self.emailTextField.text] == NO) {
        [self showMessage:@"E-mail inválido!"];
        return NO;
        
    }
    
    return YES;
}

- (IBAction)send
{
    
    if ([self validated]) {
        
        NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
        NSString *build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
        
        NSString *device;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            device = @"iPad";
        } else {
            device = @"iPhone";
        }
        
        NSString *title = [NSString stringWithFormat:@"(%@) Versão: %@ Build: %@  iOS: %@ - %@",device, version, build,[[UIDevice currentDevice ] systemVersion], _subjectTextField.text];
        
        ZendeskDropbox *dropbox = [[ZendeskDropbox alloc] initWithDelegate:self];
        [dropbox submitWithEmail:_emailTextField.text subject:title andDescription:_messageTextView.text];

        [self.view endEditing:YES];
        [SVProgressHUD showWithStatus:@"Aguarde ..." maskType:SVProgressHUDMaskTypeBlack];

        
    }
}

#pragma mark - ZendeskDropboxDelegate
- (void)submissionDidFinishLoading:(ZendeskDropbox *)connection
{
    
    [SVProgressHUD dismiss];
    [self showMessage:@"Ticket enviado com sucesso! \nAguarde que a nossa equipe entrará em contato em breve."];
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)submission:(ZendeskDropbox *)connection didFailWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    [self showMessage:@"Ocorreu um erro ao abrir o ticket. Verifique sua conexão e tente novamente!"];
}

#pragma mark - Show Alert
- (void)showMessage:(NSString *)message
{
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Aviso" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
    
}


@end
