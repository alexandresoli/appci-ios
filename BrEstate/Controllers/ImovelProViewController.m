//
//  ImovelProViewController.m
//  Appci
//
//  Created by BrEstate LTDA on 4/29/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "ImovelProViewController.h"
#import "ImovelProImporter.h"
#import "SVProgressHUD.h"
@import QuickLook;

@interface ImovelProViewController () <UITextViewDelegate,QLPreviewControllerDataSource, QLPreviewControllerDelegate,UIAlertViewDelegate>

@property (nonatomic, strong) NSManagedObjectContext *moc;
@property (nonatomic, weak) IBOutlet UITextField *textField;

@property (nonatomic, weak) IBOutlet UIProgressView *progressView;
@property (nonatomic, weak) IBOutlet UILabel *progressLabel;
@property (nonatomic, weak) IBOutlet UILabel *tutorialLabel;
@property (nonatomic, strong) UIImageView *coverImageView;
@property (nonatomic) BOOL hasAppeared;
@end

@implementation ImovelProViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    NSString *url = [Utils valueFromDefaults:SITE_IMOVELPRO];
    
    if (url) {
        self.textField.text = url;
    }
    
    [self formatTutorial];
    
    [Utils trackWithName:@"Entrou ImovelPro"];
    [Utils trackFlurryWithName:@"Entrou ImovelPro"];
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [UIApplication sharedApplication].idleTimerDisabled = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (!_hasAppeared && ![Utils valueFromDefaults:SITE_IMOVELPRO]) {
        [self showCover];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Tutorial Format
- (void)formatTutorial
{
    NSString *str = @"Você está a 1 passo de importar os imóveis cadastrados no seu sistema para dentro do AppCi. Caso já possua o código de integração basta inseri-lo no campo abaixo e apertar o botão Importar, se você ainda não tem um código descubra como através de um passo a passo clicando no ícone de ajuda na lateral superior a direita.";
    
    
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:str];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:15] range:NSMakeRange(0, str.length-1)];
       // [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15] range:NSMakeRange(184, 9)];
        
    } else {
        [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:12] range:NSMakeRange(0, str.length-1)];
      //  [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:12] range:NSMakeRange(184, 9)];
    }
    
    _tutorialLabel.attributedText = attrStr;
}

#pragma mark - Cover
- (void)showCover
{
    _hasAppeared = YES;
    
    UIImage *image;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        image = [UIImage imageNamed:@"cover-integracao-ipad"];
    } else {
        image = [UIImage imageNamed:@"cover-integracao-iphone"];
    }
    
    _coverImageView = [[UIImageView alloc] initWithImage:image];
    _coverImageView.userInteractionEnabled = YES;
    _coverImageView.alpha = 0.0;
    _coverImageView.frame = self.view.frame;
    
    
    [UIView animateWithDuration:1.0 animations:^{
        _coverImageView.alpha = 1.0;
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [self.navigationController.view addSubview:_coverImageView];
        } else {
            [[[[UIApplication sharedApplication] delegate] window] addSubview:_coverImageView];
        }
    }];
    
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeCover)];
    [_coverImageView addGestureRecognizer:tapGesture];
}

- (void)closeCover
{
    [_coverImageView removeFromSuperview];
}

- (IBAction)start:(id)sender
{
    [Utils trackWithName:@"Iniciou importação ImovelPro"];
    [Utils trackFlurryWithName:@"Iniciou importação ImovelPro"];

    _progressView.progress = 0.0f;
    _progressLabel.text = @"";
    
    
    if([self validate]) {
        
        [Utils addObjectToDefaults:self.textField.text withKey:SITE_IMOVELPRO];
        
        [SVProgressHUD showWithStatus:@"Iniciando importação..." maskType:SVProgressHUDMaskTypeBlack];
        [_textField resignFirstResponder];

        [self.view endEditing:YES];
        [self import:NO];
        
    }
    

}

//- (NSString *)cleanURL:(NSString *)strURL
//{
//    strURL = [strURL stringByReplacingOccurrencesOfString:@"integracao.imovelpro.com.br/integracao/exportacao_imoveis_url.cfm?portal=uETMalAbnZMWVqud&" withString:@""];
//    strURL = [strURL stringByReplacingOccurrencesOfString:@"http://integracao.imovelpro.com.br/integracao/exportacao_imoveis_url.cfm?portal=uETMalAbnZMWVqud&" withString:@""];
//    
//    return strURL;
//}

- (BOOL)validate
{
    if(_textField.text == nil || [_textField.text isEqualToString:@""]) {
        [Utils showMessage:@"Digite o código da importação!"];
        return NO;
    }
    
    return YES;
}


-  (IBAction)showHelp:(id)sender
{
    QLPreviewController *previewController=[[QLPreviewController alloc]init];
    previewController.delegate = self;
    previewController.dataSource = self;
    [self presentViewController:previewController animated:YES completion:nil];
}


#pragma mark - QLPreviewControllerDataSource

- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller
{
    return 1;
}


- (id <QLPreviewItem>)previewController:(QLPreviewController *)controller previewItemAtIndex:(NSInteger)index
{
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"help-integracao-imovelpro" withExtension:@"pdf"];
    return url;
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


- (void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        int movementDistance = 0; // tweak as needed
        
        if ([[UIScreen mainScreen] bounds].size.height < 568) {
            movementDistance = -100;
        }
        
        
        const float movementDuration = 0.3f; // tweak as needed
        
        int movement = (up ? movementDistance : -movementDistance);
        
        [UIView beginAnimations: @"animateTextField" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];
        
    }
    

}

#pragma mark - Back
- (IBAction)back:(id)sender
{
    if (_hasStarted) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Aviso" message:@"Sair desta tela irá interromper a importação, prosseguir mesmo assim?" delegate:self cancelButtonTitle:@"Continuar" otherButtonTitles:@"Interromper", nil];
        alertView.tag = 1000;
        [alertView show];
        
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            break;
        case 1:
            
            if (alertView.tag == 1000) {
                [self.navigationController popViewControllerAnimated:YES];
                break;
            }
            
            [self import:YES];
            break;
            
        default:
            break;
    }
}


#pragma mark - Import
- (void)import:(BOOL)isForced
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        ImovelProImporter *imp = [[ImovelProImporter alloc] init];
        imp.progressLabel = _progressLabel;
        imp.progressView = _progressView;
        imp.parentController = self;
        
//        NSString *url = [NSString stringWithFormat:@"http://integracao.imovelpro.com.br/integracao/exportacao_imoveis_url.cfm?portal=uETMalAbnZMWVqud&hash=%@",self.textField.text];
        
        [imp startWithURL:self.textField.text usingMoc:self.moc forced:isForced];
        
        
    });
}

@end
