//
//  ImportacaoContatoTableViewController.h
//  Appci
//
//  Created by BrEstate LTDA on 5/21/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface ImportacaoContatoTableViewController : BaseTableViewController

@property (nonatomic, strong) NSString *nomeArquivo;
@property(nonatomic, strong) NSURL *urlArquivo;


@end
