//
//  ImportacaoContatoTableViewController.m
//  Appci
//
//  Created by BrEstate LTDA on 5/21/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "ImportacaoContatoTableViewController.h"
#import "ContatoCell.h"
#import "Cliente+Utils.h"
#import "SubtipoCliente.h"
#import "TipoCliente.h"
#import "Acompanhamento.h"
#import "Acompanhamento+Firebase.h"

@interface ImportacaoContatoTableViewController () <NSFetchedResultsControllerDelegate,SWTableViewCellDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSCache *fotosCache;

@end

@implementation ImportacaoContatoTableViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSCache *)fotosCache
{
    if (_fotosCache) {
        return _fotosCache;
    }
    
    _fotosCache = [[NSCache alloc] init];
    [_fotosCache setCountLimit:30];
    
    return _fotosCache;
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}


- (NSArray *) sectionIndexTitlesForTableView: (UITableView *) tableView
{
    return [self.fetchedResultsController sectionIndexTitles];
}


- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [self.fetchedResultsController sectionForSectionIndexTitle:title atIndex:index];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *labelView = [[UILabel alloc] init];
    labelView.textAlignment = NSTextAlignmentLeft;
    labelView.backgroundColor = [UIColor colorWithRed:0.851 green:0.851 blue:0.851 alpha:1];
    labelView.textColor = [UIColor blackColor];
    labelView.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
    
    NSString *headerText = [[self.fetchedResultsController sectionIndexTitles] objectAtIndex:section];
    labelView.text = [NSString stringWithFormat:@"   %@",headerText];
    
    return labelView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ContatoCell";
    ContatoCell *cell = (ContatoCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;;
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];
    
    // Criação do botão de importar arquivo.
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//    [button addTarget:self action:@selector(realizarImportacaoComContato:) forControlEvents:UIControlEventTouchDown];
//    [button setImage:[UIImage imageNamed:@"importar"] forState:UIControlStateNormal];
//    button.frame = CGRectMake(0, 0, 39, 39);
//    cell.accessoryView = button;
    
    // SWTableViewCell
    //    [cell setCellHeight:cell.frame.size.height];
    //    cell.containingTableView = tableView;
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


- (void)configureCell:(ContatoCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Cliente *cliente = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.nome.text = cliente.nome;
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",cliente.subtipo];
    SubtipoCliente *subtipo = [SubtipoCliente MR_findFirstWithPredicate:predicate inContext:self.moc];
    // Foto.
    cell.fotoLabel.text = cliente.iniciaisNome;
    cell.fotoLabel.hidden = NO;
    
    if (cliente.foto == nil) {
        cell.fotoImageView.image = nil;
        
    } else {
        cell.fotoImageView.layer.cornerRadius = cell.fotoImageView.frame.size.height /2;
        cell.fotoImageView.layer.masksToBounds = YES;
        cell.fotoImageView.layer.borderWidth = 0;
        cell.fotoImageView.image = [self.fotosCache objectForKey:indexPath];
        
        if (cell.fotoImageView.image) {
            cell.fotoLabel.hidden = YES;
            
        } else {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                UIImage *image = [UIImage imageWithData:cliente.foto];
                
                [self.fotosCache setObject:image forKey:indexPath];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    ContatoCell *updateCell = (ContatoCell *)[self.tableView cellForRowAtIndexPath:indexPath];
                    
                    [UIView transitionWithView:cell.fotoImageView
                                      duration:0.2f
                                       options:UIViewAnimationOptionTransitionCrossDissolve
                                    animations:^{
                                        
                                        updateCell.fotoLabel.hidden = YES;
                                        updateCell.fotoImageView.image = image;
                                        
                                    } completion:NULL];
                });
            });
        }
    }
    
    // tipo
    cell.tipoImageView.hidden = subtipo.tipoCliente.descricao.length > 0 ? NO : YES;
    cell.subTipo.hidden = subtipo.tipoCliente.descricao.length > 0 ? NO : YES;
    
    if (subtipo.tipoCliente.descricao.length > 0) {
        // border around label
        cell.subTipo.layer.borderColor = [UIColor whiteColor].CGColor;
        cell.subTipo.layer.borderWidth = 2.0;
        cell.subTipo.layer.cornerRadius = 8.0;
        
        cell.subTipo.text = subtipo.descricao;
        cell.subTipo.textColor = [UIColor whiteColor];
        
        if ([subtipo.tipoCliente.descricao isEqualToString:@"Proprietário"]) {
            cell.tipoImageView.image = [UIImage imageNamed:@"contato-p"];
            cell.subTipo.backgroundColor = [UIColor colorWithRed:77.0/255 green:219.0/255 blue:109.0/255 alpha:1];
        } else if ([subtipo.tipoCliente.descricao isEqualToString:@"Interessado"]) {
            cell.tipoImageView.image = [UIImage imageNamed:@"contato-i"];
            cell.subTipo.backgroundColor = [UIColor colorWithRed:252.0/255 green:150.0/255 blue:42.0/255 alpha:1];
        } else {
            cell.tipoImageView.image = [UIImage imageNamed:@"contato-o"];
            cell.subTipo.backgroundColor = [UIColor colorWithRed:255.0/255 green:44.0/255 blue:85.0/255 alpha:1];
        }
    }
    
    
    cell.delegate = self;
}

#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    _fetchedResultsController.delegate = self;
    
    _fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithAlphabetic:@"Cliente" andKey:@"nome" withPredicate:nil withOrder:YES withLimit:0 inContext:self.moc];
    
    [_fetchedResultsController performFetch:nil];
    
    return _fetchedResultsController;
}

- (ContatoCell *)cellFromTableView:(UITableView *)tableView sender:(id)sender
{
    CGPoint pos = [sender convertPoint:CGPointZero toView:tableView];
    NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:pos];
    return (ContatoCell *) [tableView cellForRowAtIndexPath:indexPath];
}

#pragma mark - Importação do Arquivo
- (IBAction)realizarImportacaoComContato:(id)sender
{
    // O superview do button é um UITableViewCellScrollView e seu superview é um ContatoCell.
    ContatoCell *cell = [self cellFromTableView:self.tableView sender:sender];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    Cliente *cliente = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // Inclusão do acompanhamento.
    Acompanhamento *acompanhamento = [Acompanhamento MR_createInContext:self.moc];
    acompanhamento.codigo = [acompanhamento getUniqueCode]; //[NSNumber numberWithLong:codigoAcompanhamento];
    acompanhamento.cliente = cliente;
    acompanhamento.timeStamp = [NSDate date];
    acompanhamento.titulo = self.nomeArquivo;
    acompanhamento.arquivo = [NSData dataWithContentsOfURL:self.urlArquivo];
    acompanhamento.extensao = [self.urlArquivo pathExtension];
    acompanhamento.version = [NSDate date];
    acompanhamento.filename = [NSString stringWithFormat:@"%@-%@",[DateUtils stringTimestampFromDate:[NSDate date]], acompanhamento.codigo];
    
    acompanhamento.pendingUpload = [NSNumber numberWithBool:YES];
    
    // firebase
    [acompanhamento saveFirebase];
    
    [self saveMOC];
    
    [Utils showMessage:@"Arquivo importado com sucesso. O mesmo pode ser acessado nos detalhes do contato > Acompanhamento."];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
