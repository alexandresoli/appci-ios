//
//  ImportarImovelTableViewController.m
//  Appci
//
//  Created by BrEstate LTDA on 5/21/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "ImportacaoImovelTableViewController.h"
#import "ImovelCell.h"
#import "Imovel+Utils.h"
#import "Endereco.h"
#import "Bairro.h"
#import "Acompanhamento+Firebase.h"
#import "FotoImovel+InsertDeleteImagem.h"


@interface ImportacaoImovelTableViewController () <NSFetchedResultsControllerDelegate,SWTableViewCellDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSCache *fotosCache;

@end

@implementation ImportacaoImovelTableViewController


#pragma mark - Queue

dispatch_queue_t queueLoadFoto;


#pragma mark - View Life Cycle

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    queueLoadFoto = dispatch_queue_create("ImportacaoImovelTableViewController.LoadFoto", NULL);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSCache *)fotosCache
{
    if (_fotosCache) {
        return _fotosCache;
    }
    
    _fotosCache = [[NSCache alloc] init];
    [_fotosCache setCountLimit:15];
    
    return _fotosCache;
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *labelView = [[UILabel alloc] init];
    labelView.textAlignment = NSTextAlignmentLeft;
    labelView.backgroundColor = [UIColor colorWithRed:0.851 green:0.851 blue:0.851 alpha:1];
    labelView.textColor = [UIColor blackColor];
    labelView.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
    
    
    if ([[_fetchedResultsController sections] count] > 0) {
        NSString *text = [[[_fetchedResultsController sections] objectAtIndex:section] name];
        
        if (text.length == 0) {
            text = @"Bairro Não Cadastrado";
        }
        
        labelView.text = [NSString stringWithFormat:@"   %@",text];
    }
    
    
    return labelView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ImovelCell";
    ImovelCell *cell = (ImovelCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;;
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];
    
    
    // Criação do botão de importar arquivo.
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(realizarImportacaoComImovel:) forControlEvents:UIControlEventTouchDown];
    [button setImage:[UIImage imageNamed:@"importar"] forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, 39, 39);
    cell.accessoryView = button;
    
    // SWTableViewCell
    //    [cell setCellHeight:cell.frame.size.height];
    //    cell.containingTableView = tableView;
    cell.delegate = self;
    
    Imovel *imovel = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [self configureTags:imovel cell:cell];
    [self configureCell:cell atIndexPath:[indexPath copy] forImovel:imovel];
    
    return cell;
}

- (void)configureCell:(ImovelCell *)cell atIndexPath:(NSIndexPath *)indexPath forImovel:(Imovel *)imovel
{
    NSArray *fotos = [imovel.fotos allObjects];
    
    if (fotos.count == 0) {
        cell.photoView.image = [UIImage imageNamed:@"nao-disponivel2"];
        
    } else {
        cell.photoView.image = [self.fotosCache objectForKey:indexPath];
        
        if (!cell.photoView.image) {
            __block FotoImovel *fotoImovel = [[fotos filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"capa = YES"]] firstObject];
            
            dispatch_async(queueLoadFoto, ^{
                
                if (![self.tableView.indexPathsForVisibleRows containsObject:indexPath]) {
                    return;
                }
                
                if (fotoImovel == nil) { // sem capa
                    fotoImovel = [fotos firstObject];
                }
                
                UIImage *image = [fotoImovel getImageForFotoImovelWithMaxPixelSize:500];
                
                if (image) {
                    [self.fotosCache setObject:image forKey:indexPath];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    ImovelCell *updateCell = (ImovelCell *)[self.tableView cellForRowAtIndexPath:indexPath];
                    [UIView transitionWithView:updateCell.photoView
                                      duration:0.2f
                                       options:UIViewAnimationOptionTransitionCrossDissolve
                                    animations:^{
                                        
                                        updateCell.photoView.image = image;
                                        
                                    } completion:NULL];
                });
            });
        }
    }
}

#pragma mark - Tag Configuration
- (void)configureTags:(Imovel *)imovel cell:(ImovelCell *)cell
{
    // reset tags
    cell.tag1.text = nil;
    cell.tag2.text = nil;
    cell.tag3.text = nil;
    cell.tag4.text = nil;
    cell.tag5.text = nil;
    cell.tag6.text = nil;
    
    cell.tag1.text = imovel.tipo;
    cell.tag2.text = imovel.endereco.bairro.nome;
    cell.tag3.text = [self formatQuartos:[imovel.quartos intValue]];
    cell.tag4.text =  [self.formatter stringFromNumber:imovel.valor];
    
    if(imovel.areaUtil != nil) {
        cell.tag5.text =  [NSString stringWithFormat:@"%@ m²",imovel.areaUtil];
    }
    
    cell.tag6.text =  imovel.oferta;
    
    
}

#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    _fetchedResultsController.delegate = self;
    
    _fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Imovel"
                                                                      withSortKey:@"endereco.bairro.nome"
                                                                  withSectionName:@"sectionIdentifier"
                                                                    withPredicate:nil
                                                                        withOrder:YES
                                                                        withLimit:0
                                                                        inContext:self.moc];
    //[CoreDataUtils fetchedResultsControllerWithEntity:@"Imovel" andKey:@"endereco.bairro.nome" withPredicate:nil withOrder:YES withLimit:0 inContext:self.moc];
    
    return _fetchedResultsController;
}



#pragma mark - Importação do Arquivo
- (ImovelCell *)cellFromTableView:(UITableView *)tableView sender:(id)sender
{
    CGPoint pos = [sender convertPoint:CGPointZero toView:tableView];
    NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:pos];
    return (ImovelCell *) [tableView cellForRowAtIndexPath:indexPath];
}

- (void)realizarImportacaoComImovel:(id)sender
{
    ImovelCell *cell = [self cellFromTableView:self.tableView sender:sender];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    Imovel *imovel = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // Inclusão do acompanhamento.
    Acompanhamento *acompanhamento = [Acompanhamento MR_createInContext:self.moc];
    acompanhamento.codigo = [acompanhamento getUniqueCode]; //[NSNumber numberWithLong:codigoAcompanhamento];
    acompanhamento.imovel = imovel;
    acompanhamento.timeStamp = [NSDate date];
    acompanhamento.titulo = self.nomeArquivo;
    acompanhamento.arquivo = [NSData dataWithContentsOfURL:self.urlArquivo];
    acompanhamento.extensao = [self.urlArquivo pathExtension];
    acompanhamento.version = [NSDate date];
    acompanhamento.filename = [NSString stringWithFormat:@"%@-%@",[DateUtils stringTimestampFromDate:[NSDate date]], acompanhamento.codigo];
    
    acompanhamento.pendingUpload = [NSNumber numberWithBool:YES];
    
    // Firebase
    [acompanhamento saveFirebase];
    
    [self saveMOC];
    
    [Utils showMessage:@"Arquivo importado com sucesso. O mesmo pode ser acessado nos detalhes do imóvel > Documentos."];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


@end
