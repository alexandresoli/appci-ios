//
//  ImportarArquivoViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 19/02/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "BaseViewController.h"

@interface ImportarArquivoViewController : BaseViewController

@property(nonatomic, strong) NSURL *urlArquivo;

@end
