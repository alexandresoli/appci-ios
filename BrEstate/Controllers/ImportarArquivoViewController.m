//
//  ImportarArquivoViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 19/02/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "ImportarArquivoViewController.h"
#import "Nota.h"
#import "UIView+Genie.h"
#import "ImportacaoImovelTableViewController.h"
#import "ImportacaoContatoTableViewController.h"

#import "Nota+Firebase.h"

@interface ImportarArquivoViewController () <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *containerContato;
@property (weak, nonatomic) IBOutlet UIView *containerImovel;
@property (weak, nonatomic) IBOutlet UILabel *nomeArquivoLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *tipoImportacaoSegmentedControl;
@property (weak, nonatomic) IBOutlet UIImageView *arquivoIconeImageView;


@end


@implementation ImportarArquivoViewController


#pragma mark - IBActions

- (IBAction)cancelarImportacao:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)mudarTipoImportacao:(UISegmentedControl *)segment
{
    switch (segment.selectedSegmentIndex) {
        case 0:
            self.containerImovel.hidden = YES;
            self.containerContato.hidden = NO;
            break;
        case 1:
        {
            UIAlertView *alert = [[ UIAlertView alloc] initWithTitle:@"Importar Arquivo"
                                                             message:MENSAGEM_IMPORTAR_ARQUIVO_CRIAR_NOTA
                                                            delegate:self
                                                   cancelButtonTitle:@"Não"
                                                   otherButtonTitles:@"Sim", nil];
            [alert show];
            
            break;
        }
        case 2:
        {
            self.containerImovel.hidden = NO;
            self.containerContato.hidden = YES;
            break;
        }
            
        default:
            break;
    }
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView.message isEqualToString:MENSAGEM_IMPORTAR_ARQUIVO_CRIAR_NOTA]) {
        if (buttonIndex == 1) { // Sim
            [self realizarImportacaoComNota];
            
        } else {
            self.tipoImportacaoSegmentedControl.selectedSegmentIndex = 0;
        }
    }
}


#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *urlString = [self.urlArquivo path];
    NSArray *parts = [urlString componentsSeparatedByString:@"/"];
    self.nomeArquivoLabel.text = [parts objectAtIndex:[parts count]-1];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Importar Arquivos"];
    [Utils trackFlurryWithName:@"Importar Arquivos"];
    
    
    ImportacaoContatoTableViewController *contatoController =  self.childViewControllers.firstObject;
    contatoController.nomeArquivo = self.nomeArquivoLabel.text;
    contatoController.urlArquivo = self.urlArquivo;
    
    
    ImportacaoImovelTableViewController *imovelController = self.childViewControllers.lastObject;
    imovelController.nomeArquivo = self.nomeArquivoLabel.text;
    imovelController.urlArquivo = self.urlArquivo;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Importação do Arquivo
- (void)realizarImportacaoComNota
{
    
    // Inclusão da Nota.
    Nota *nota = [Nota MR_createInContext:self.moc];
    nota.codigo = [nota getUniqueCode]; //[NSNumber numberWithLong:codigoNota];
    nota.timeStamp = [NSDate date];
    nota.descricao = self.nomeArquivoLabel.text;
    nota.arquivo = [NSData dataWithContentsOfURL:self.urlArquivo];
    nota.filename = [NSString stringWithFormat:@"%@-%@",[DateUtils stringTimestampFromDate:[NSDate date]], nota.codigo];
    nota.extensao = [self.urlArquivo pathExtension];
    nota.version = [NSDate date];
    
    [self saveMOC];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [Utils postEntitytNotification:@"Nota"];
    
    // Firebase
    [nota saveFirebase];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [nota uploadToS3];
    });
}
@end
