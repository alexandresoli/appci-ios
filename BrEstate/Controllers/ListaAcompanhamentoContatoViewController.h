//
//  AcompanhamentoClienteViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 1/24/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "BaseViewController.h"

@class Cliente;
@interface ListaAcompanhamentoContatoViewController : BaseViewController

@property (nonatomic, strong) Cliente *selectedCliente;
@end
