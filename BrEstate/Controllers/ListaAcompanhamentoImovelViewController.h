//
//  ListaAcompanhamentoViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 1/28/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "BaseTableViewController.h"

@class Negociacao;
@interface ListaAcompanhamentoImovelViewController : BaseViewController 

@property (nonatomic, strong) Negociacao *selectedNegociacao;
@property (nonatomic, strong) UIView *backBar;
- (void)changeNegociacao:(Negociacao *)negociacao;

@end
