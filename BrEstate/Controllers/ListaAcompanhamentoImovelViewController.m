//
//  ListaAcompanhamentoViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 1/28/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "ListaAcompanhamentoImovelViewController.h"
#import "EditarAcompanhamentoContatoViewController.h"
#import "Negociacao.h"
#import "Cliente.h"
#import "Imovel.h"
#import "AcompanhamentoCell.h"
#import "ImageUtils.h"
#import "Acompanhamento+Firebase.h"

@interface ListaAcompanhamentoImovelViewController () <NSFetchedResultsControllerDelegate,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,SWTableViewCellDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;
@property (nonatomic, weak) IBOutlet UIButton *addButton;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) Acompanhamento *selectedAcompanhamento;

@end

@implementation ListaAcompanhamentoImovelViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // add model changes notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDataModelChange:) name:NSManagedObjectContextObjectsDidChangeNotification object:self.moc];
    
    
    self.addButton.enabled = NO;
    
    if (self.selectedNegociacao) {
        [self changeNegociacao:self.selectedNegociacao];
    }
    
}

- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    [Utils trackWithName:@"Lista Acompanhamento Imovel"];
    [Utils trackFlurryWithName:@"Lista Acompanhamento Imovel"];
    
    self.backBar.hidden = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Notification Center
- (void)handleDataModelChange:(NSNotification *)note
{
    NSSet *updatedObjects = [[note userInfo] objectForKey:NSUpdatedObjectsKey];
    NSSet *deletedObjects = [[note userInfo] objectForKey:NSDeletedObjectsKey];
    NSSet *insertedObjects = [[note userInfo] objectForKey:NSInsertedObjectsKey];
    
    if ([updatedObjects containsMemberOfClass:[Acompanhamento class]] ||
        [insertedObjects containsMemberOfClass:[Acompanhamento class]] ||
        [deletedObjects containsMemberOfClass:[Acompanhamento class]])
    {
        [_fetchedResultsController performFetch:nil];
        [self.tableView reloadData];
    }
    
    
}

#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (_fetchedResultsController != nil)
    {
        return _fetchedResultsController;
    }
    
    
    if (self.selectedNegociacao == nil) {
        return _fetchedResultsController;
    }
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cliente = %@ AND imovel = %@",self.selectedNegociacao.cliente,self.selectedNegociacao.imovel];
    
    _fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Acompanhamento" withSortKey:@"timeStamp" withSectionName:@"sectionIdentifier" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
    
    _fetchedResultsController.delegate = self;
    
	return _fetchedResultsController;
    
}



#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AcompanhamentoCell";
    AcompanhamentoCell *cell = (AcompanhamentoCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        cell.backgroundColor = [UIColor colorWithRed:0.945 green:0.949 blue:0.949 alpha:1];
    }

    // SWTableViewCell
//    [cell setCellHeight:cell.frame.size.height];
//    cell.containingTableView = tableView;
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


- (void)configureCell:(AcompanhamentoCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Acompanhamento *acompanhamento = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.descricao.text = acompanhamento.titulo;;
    
    NSString *dia = [DateUtils stringFromDate:acompanhamento.timeStamp withFormat:@"dd"];
    NSString *hora = [DateUtils stringFromDate:acompanhamento.timeStamp withFormat:@"HH:mm"];
    
    cell.dia.text = dia;
    cell.hora.text = hora;
    
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"swipe-apagar"]];
        
    } else {
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"swipe-apagar"] scaledToSize:CGSizeMake(47, 60)]];
    }
    
    cell.rightUtilityButtons = rightUtilityButtons;
    
    cell.delegate = self;
    
}

#pragma mark - SWTableViewCellDelegate
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    self.selectedAcompanhamento = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    switch (index) {
            
        case 0: // Deletar
        {
            // Firebase
            [_selectedAcompanhamento deleteFirebase];
            
            [_selectedAcompanhamento MR_deleteInContext:self.moc];
            [self.moc MR_saveToPersistentStoreAndWait];
            
            [self.fetchedResultsController performFetch:nil];
            [self.tableView reloadData:YES];
            break;
        }
            
        default:
            break;
    }
}


- (void)deleteObject:(Acompanhamento *)acompanhamento
{
    // Firebase
    [acompanhamento deleteFirebase];
    
    [acompanhamento MR_deleteInContext:self.moc];
    
    NSError *error = nil;

    [self.moc MR_saveToPersistentStoreAndWait];
    
    // Parse SDK
//    [ParseUtils deleteEntity:acompanhamento withName:@"Acompanhamento" usingMoc:self.moc];
    
    [self.fetchedResultsController performFetch:&error];
    
    [self.tableView reloadData:YES];
    
}


- (NSString *)formatTitle:(NSInteger)section
{
	id <NSFetchedResultsSectionInfo> theSection = [[self.fetchedResultsController sections] objectAtIndex:section];
    
    /*
     Section information derives from an event's sectionIdentifier, which is a string representing the number (year * 1000) + month.
     To display the section title, convert the year and month components to a string representation.
     */
    static NSDateFormatter *formatter = nil;
    
    if (!formatter)
    {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setCalendar:[NSCalendar currentCalendar]];
        
        NSString *formatTemplate = [NSDateFormatter dateFormatFromTemplate:@"MMMM YYYY" options:0 locale:[NSLocale currentLocale]];
        [formatter setDateFormat:formatTemplate];
    }
    
    NSInteger numericSection = [[theSection name] integerValue];
	NSInteger year = numericSection / 1000;
	NSInteger month = numericSection - (year * 1000);
    
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    dateComponents.year = year;
    dateComponents.month = month;
    NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
    
    
	NSString *titleString = [formatter stringFromDate:date];
    NSString *firstLetter = [[titleString substringToIndex:1] capitalizedString];
    titleString = [titleString stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstLetter];
    
    
	return [NSString stringWithFormat:@"   %@",titleString];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *labelView = [[UILabel alloc] init];
    labelView.textAlignment = NSTextAlignmentLeft;
    //labelView.backgroundColor = [UIColor colorWithRed:0.0/255 green:122.0/255 blue:255.0/255 alpha:1.0];
    //labelView.textColor = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0];
    labelView.backgroundColor = [UIColor colorWithRed:0.851 green:0.851 blue:0.851 alpha:1];
    labelView.textColor = [UIColor blackColor];
    labelView.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
    labelView.text = [self formatTitle:section];
    
    return labelView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedAcompanhamento = [_fetchedResultsController objectAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"EditarAcompanhamentoSegue" sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    searchBar.showsCancelButton = YES;
    
    return YES;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self filterContentForSearchText:searchText];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self filterContentForSearchText:searchBar.text];
    searchBar.showsCancelButton = NO;
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    searchBar.text = nil;
    searchBar.showsCancelButton = NO;
    [self filterContentForSearchText:nil];
    [searchBar resignFirstResponder];
}


// The method to change the predicate of the FRC
- (void)filterContentForSearchText:(NSString*)searchText
{
    NSString *query = searchText;
    
    if (query && query.length) {
        
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cliente = %@ AND imovel = %@ AND titulo contains[c] %@",self.selectedNegociacao.cliente,self.selectedNegociacao.imovel, query];
        
        [_fetchedResultsController.fetchRequest setPredicate:predicate];
        [_fetchedResultsController.fetchRequest setFetchLimit:100];
        
    }  else {
        
        _fetchedResultsController = nil;
        
    }
    
    [self.fetchedResultsController performFetch:nil];
    
    [self.tableView reloadData];
}


#pragma mark - Add
- (IBAction)add:(id)sender
{
    [self performSegueWithIdentifier:@"AdicionarAcompanhamentoSegue" sender:self];
}

#pragma mark - Back
- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Change Negociacao
- (void)changeNegociacao:(Negociacao *)negociacao
{
    self.addButton.enabled = (negociacao == nil) ? NO : YES;
    
    self.selectedNegociacao = negociacao;
    self.fetchedResultsController = nil;
    
    [self.fetchedResultsController performFetch:nil];
    [self.tableView reloadData:YES];
}


#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    EditarAcompanhamentoContatoViewController *controller = segue.destinationViewController;
    self.backBar.hidden = YES;
    if ([segue.identifier isEqualToString:@"EditarAcompanhamentoSegue"]) {
        
        controller.selectedAcompanhamento = self.selectedAcompanhamento;
        
    } else  if ([segue.identifier isEqualToString:@"AdicionarAcompanhamentoSegue"]) {
        
        controller.selectedNegociacao = self.selectedNegociacao;
    }
}
@end
