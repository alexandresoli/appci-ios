//
//  ListaContatosAgendaViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 2/4/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "BaseTableViewController.h"

@class Evento;
@interface ListaContatosAgendaViewController : BaseTableViewController

@property (nonatomic, weak) UINavigationController *superNavigationController;

- (void)changeContatos:(Evento *)evento;
@end
