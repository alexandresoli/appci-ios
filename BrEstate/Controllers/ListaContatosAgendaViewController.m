//
//  ListaContatosAgendaViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 2/4/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "ListaContatosAgendaViewController.h"
#import "ContatoViewController.h"

#import "ImageUtils.h"
#import "Cliente.h"
#import "TipoCliente.h"
#import "SubtipoCliente.h"
#import "Evento.h"
#import "ContatoCell.h"
#import "ListaContatosViewController.h"

@interface ListaContatosAgendaViewController () <UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) Evento *selectedEvento;

@end

@implementation ListaContatosAgendaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Listar Contatos (Agenda)"];
    [Utils trackFlurryWithName:@"Listar Contatos (Agenda)"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    _fetchedResultsController.delegate = self;
    
    
    NSPredicate *predicate = nil;
    
    if (self.selectedEvento != nil) {
        
        predicate = [NSPredicate predicateWithFormat:@"ANY evento = %@",_selectedEvento];
        
    _fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Cliente" andKey:@"nome" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
    }
    
    return _fetchedResultsController;
}



#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ContatoCell";
    ContatoCell *cell = (ContatoCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;;
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];

    
    // SWTableViewCell
//    [cell setCellHeight:cell.frame.size.height];
//    cell.containingTableView = tableView;
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(ContatoCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Cliente *cliente = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.nome.text = cliente.nome;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",cliente.subtipo];
    SubtipoCliente *subtipo = [SubtipoCliente MR_findFirstWithPredicate:predicate inContext:self.moc];
    
    
    // telefone
    NSString *telefone = cliente.telefone;
    
    if (telefone.length == 0) {
        telefone = @" - ";
    }
    
    if (cliente.telefoneExtra.length > 0) {
        telefone = [telefone stringByAppendingString:[NSString stringWithFormat:@" / %@",cliente.telefoneExtra]];
    }
    
    cell.telefone.text = telefone;
    
    
    // email
    if (cliente.email.length == 0) {
        cell.email.text = @" - ";
    } else {
        cell.email.text = cliente.email;
    }
    
    // tipo
    if (subtipo.tipoCliente.descricao.length > 0) {
        cell.subTipo.hidden = NO;
        cell.tipoImageView.hidden = NO;
        
        // border around label
//        cell.subTipo.layer.borderColor = [UIColor whiteColor].CGColor;
//        cell.subTipo.layer.borderWidth = 2.0;
//        cell.subTipo.layer.cornerRadius = 8.0;
        
        cell.subTipo.text = subtipo.descricao;
        cell.subTipo.textColor = [UIColor whiteColor];
        
        if ([subtipo.tipoCliente.descricao isEqualToString:@"Proprietário"]) {
            cell.tipoImageView.image = [UIImage imageNamed:@"contato-p"];
            cell.subTipo.backgroundColor = [UIColor colorWithRed:77.0/255 green:219.0/255 blue:109.0/255 alpha:1];
        } else if ([subtipo.tipoCliente.descricao isEqualToString:@"Interessado"]) {
            cell.tipoImageView.image = [UIImage imageNamed:@"contato-i"];
            cell.subTipo.backgroundColor = [UIColor colorWithRed:252.0/255 green:150.0/255 blue:42.0/255 alpha:1];
        } else {
            cell.tipoImageView.image = [UIImage imageNamed:@"contato-o"];
            cell.subTipo.backgroundColor = [UIColor colorWithRed:255.0/255 green:44.0/255 blue:85.0/255 alpha:1];
        }
        
    } else {
        cell.subTipo.hidden = YES;
        cell.tipoImageView.hidden = YES;
    }
    
    
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [Utils trackWithName:@"Clicou em proprietario - Detalhes do Evento"];
    [Utils trackFlurryWithName:@"Clicou em proprietario - Detalhes do Evento"];

    
    Cliente *cliente = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        // Get the storyboard named secondStoryBoard from the main bundle:
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"_Contatos_iPad" bundle:nil];
        
        // Load the initial view controller from the storyboard.
        ContatoViewController *controller = [storyboard instantiateInitialViewController];
        
        controller.selectedCliente = cliente;
        
        [self.superNavigationController setViewControllers:@[controller] animated:YES];
        
    } else {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Contatos_iPhone" bundle:nil];
        UINavigationController *navController = [storyboard instantiateInitialViewController];
        ListaContatosViewController *listaContatosController = [navController.childViewControllers firstObject];
        listaContatosController.selectedContatoNavigationIphone = cliente;
        
        UINavigationController *navContatoController = [self.tabBarController.viewControllers firstObject];
        [navContatoController setViewControllers:@[listaContatosController]];
        
        self.tabBarController.selectedIndex = 0;
    }
    
}

#pragma mark - Change Contatos
- (void)changeContatos:(Evento *)evento
{
    self.selectedEvento = evento;
    
    self.fetchedResultsController = nil;
    [self.fetchedResultsController performFetch:nil];
    [self.tableView reloadData];
}


@end
