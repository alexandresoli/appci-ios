//
//  ListaContatosViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/7/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetalhesContatoViewController.h"
#import "DetalhesImovelViewController.h"

@interface ListaContatosViewController : BaseViewController

@property (nonatomic, strong) DetalhesContatoViewController *detalhesContatoViewController;
@property (nonatomic, strong) DetalhesImovelViewController *detalhesImovelViewController;

@property (nonatomic, strong) Cliente *selectedContato;
@property (nonatomic, strong) Cliente *selectedContatoNavigationIphone;

@end
