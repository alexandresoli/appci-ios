//
//  ListaContatosViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/7/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "ListaContatosViewController.h"
#import "Cliente+Utils.h"
#import "SubtipoCliente.h"
#import "TipoCliente.h"
#import "ContatoCell.h"
#import "SubtipoCliente.h"
#import "EditarContatoViewController.h"
#import "ImageUtils.h"
#import "AddressBookHelper.h"
#import "MailViewController.h"
#import "MapaViewController.h"
#import "Acompanhamento.h"
#import "AddressBookHelper.h"
#import "AlertaUtils.h"
#import "OrganizarContatosViewController.h"
#import "ClienteDeletado+Create.h"
#import "Deduplicator.h"
#import "Endereco.h"
#import "Bairro.h"
#import "Cliente+Firebase.h"
#import "Acompanhamento+Firebase.h"
#import "ClienteDeletado+Firebase.h"
#import "TSMessage.h"

@interface ListaContatosViewController () <SWTableViewCellDelegate, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate,UISearchBarDelegate, UIActionSheetDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, weak) IBOutlet UISegmentedControl *tipoSegment;
@property (nonatomic, weak) IBOutlet UISearchBar *contatoSearchBar;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;
@property (nonatomic, strong) MailViewController *mailViewController;
@property (nonatomic, strong) NSCache *fotosCache;
@property (nonatomic, strong) NSTimer *timerRefresh;

@end


@implementation ListaContatosViewController


#pragma mark - Queue

dispatch_queue_t queueLoadSwipeButtons;
dispatch_queue_t queueLoadPhoto;


#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // iOS 7 Bug - http://stackoverflow.com/questions/19066845/uitableview-section-index-overlapping-row-delete-button
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    
    if (![[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        // No iPhone tem que reduzir a fonte para não cortar as palavras.
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:11.0f]};
        [self.tipoSegment setTitleTextAttributes:attributes forState:UIControlStateNormal];
    }
    
    // Se for iPhone configura o BarButton de Alertas.
    if (![[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self configureAlertWithBarButton];
    }
    
    // add model changes notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshContato:) name:@"Contato" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshAlerta:) name:@"Alerta" object:nil];
    
    queueLoadSwipeButtons = dispatch_queue_create("ListaContatosViewController.LoadSwipeButtons", NULL);
    queueLoadPhoto = dispatch_queue_create("ListaContatosViewController.LoadPhoto", NULL);
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [Utils trackWithName:@"Lista Contatos"];
    [Utils trackFlurryTimedWithName:@"Lista Contatos"];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // came from contatos screen on iPad
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        if (self.selectedContato != nil) {
            [self selectRow];
        }
        
    } else {
        if (self.selectedContatoNavigationIphone != nil) {
            [self selectRowIphone];
            self.selectedContatoNavigationIphone = nil;
        }
    }
    
    [self checkRefresh];
    
    self.timerRefresh = [NSTimer scheduledTimerWithTimeInterval:30.0
                                                         target:self
                                                       selector:@selector(checkRefreshWithNotification)
                                                       userInfo:nil
                                                        repeats:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self.timerRefresh invalidate];
    [TSMessage dismissActiveNotification];
    
    [Flurry endTimedEvent:@"Lista Contatos" withParameters:nil];
}


- (NSCache *)fotosCache
{
    if (_fotosCache) {
        return _fotosCache;
    }
    
    _fotosCache = [[NSCache alloc] init];
    [_fotosCache setCountLimit:30];
    
    return _fotosCache;
}


#pragma mark - Check Refresh

- (void)checkRefresh
{
    NSNumber *refresh = [Utils valueFromDefaults:REFRESH_CLIENTE];
    if ([refresh boolValue]) {
        [TSMessage dismissActiveNotification];
        [self refreshContato:nil];
    }
}

- (void)checkRefreshWithNotification
{
    NSNumber *refresh = [Utils valueFromDefaults:REFRESH_CLIENTE];
    if ([refresh boolValue]) {
        [self showNotificationRefresh];
    }
}

- (void)showNotificationRefresh
{
    if (![Utils valueFromDefaults:FIREBASE_ALREADY_DOWNLOAD_ALL]) {
        return;
    }
    
    if ([TSMessage queuedMessages].count > 0) {
        return;
    }
    
    [TSMessage showNotificationInViewController:self.parentViewController.parentViewController
                                          title:@"Novas informações disponíveis. Toque aqui para atualizar."
                                       subtitle:nil
                                          image:nil
                                           type:TSMessageNotificationTypeSuccess
                                       duration:TSMessageNotificationDurationEndless
                                       callback:^{
                                           
                                           [Utils trackWithName:@"Atualização Firebase - Lista Contatos - Clicou na faixa"];
                                           [Utils trackFlurryWithName:@"Atualização Firebase - Lista Contatos - Clicou na faixa"];
                                           
                                           [TSMessage dismissActiveNotification];
                                           [self refreshContato:nil];
                                       }
                                    buttonTitle:nil
                                 buttonCallback:nil
                                     atPosition:TSMessageNotificationPositionTop
                           canBeDismissedByUser:YES];
}


#pragma mark - Select Specific Row

- (void)selectRow
{
    NSIndexPath *indexPath = [self.fetchedResultsController indexPathForObject:self.selectedContato];
    
    // trigger the selection
    [self.tableView.delegate tableView:self.tableView didSelectRowAtIndexPath:indexPath];
    
    // scroll to cell position
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
}

- (void)selectRowIphone
{
    NSIndexPath *indexPath = [self.fetchedResultsController indexPathForObject:self.selectedContatoNavigationIphone];
    
    // trigger the selection
    [self.tableView.delegate tableView:self.tableView didSelectRowAtIndexPath:indexPath];
    
    // scroll to cell position
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
}


#pragma mark - Notification Center

- (void)refreshContato:(NSNotification *)note
{
    [Utils addObjectToDefaults:[NSNumber numberWithBool:NO] withKey:REFRESH_CLIENTE];
    
    self.fetchedResultsController = nil;
    
    self.selectedIndexPath = nil;
    self.selectedContato = nil;
    
    [self.fotosCache removeAllObjects];
    [self.tableView reloadData:YES];
    
}

- (void)refreshAlerta:(NSNotification *)note
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self configureAlert];
    } else {
        [self configureAlertWithBarButton];
    }
}



#pragma mark - NSFetchedResultsControllerDelegate

- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSPredicate *predicate = nil;
    _fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithAlphabetic:@"Cliente" andKey:@"nome" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
    
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    
    return [sectionInfo numberOfObjects];
}

- (NSArray *) sectionIndexTitlesForTableView: (UITableView *) tableView
{
    return [self.fetchedResultsController sectionIndexTitles];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [self.fetchedResultsController sectionForSectionIndexTitle:title atIndex:index];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *labelView = [[UILabel alloc] init];
    labelView.textAlignment = NSTextAlignmentLeft;
    labelView.backgroundColor = [UIColor colorWithRed:0.851 green:0.851 blue:0.851 alpha:1]; //[UIColor colorWithRed:0.0/255 green:122.0/255 blue:255.0/255 alpha:1.0];
    labelView.textColor = [UIColor blackColor]; //[UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0];
    labelView.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
    
    NSString *text = [[[self.fetchedResultsController sections] objectAtIndex:section] name];
    
    if (text.length == 0) {
        text = @"Cadastro imcompleto";
    }
    
    labelView.text = [NSString stringWithFormat:@"   %@",text];
    
    return labelView;
}

//
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 20;
//}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 100;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ContatoCell";
    ContatoCell *cell = (ContatoCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;;
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];
    
    
    // SWTableViewCell
//    [cell setCellHeight:cell.frame.size.height];
//    cell.containingTableView = tableView;
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(ContatoCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Cliente *cliente = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.nome.text = cliente.nome;
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",cliente.subtipo];
    SubtipoCliente *subtipo = [SubtipoCliente MR_findFirstWithPredicate:predicate inContext:self.moc];
    // Tratamento para marcar a cell selecionada.
    if ([self.selectedIndexPath isEqual:indexPath]) {
        cell.selecionada.hidden = NO;
    } else {
        cell.selecionada.hidden = YES;
    }
    
    // Foto.
    cell.fotoLabel.text = cliente.iniciaisNome;
    cell.fotoLabel.hidden = NO;
    
    if (cliente.foto == nil) {
        cell.fotoImageView.image = nil;
        
    } else {
        cell.fotoImageView.layer.cornerRadius = cell.fotoImageView.frame.size.height /2;
        cell.fotoImageView.layer.masksToBounds = YES;
        cell.fotoImageView.layer.borderWidth = 0;
        cell.fotoImageView.image = [self.fotosCache objectForKey:indexPath];
        
        if (cell.fotoImageView.image) {
            cell.fotoLabel.hidden = YES;
            
        } else {
            
            dispatch_async(queueLoadPhoto, ^{
                
                if ([self.tableView.indexPathsForVisibleRows containsObject:indexPath]) {
                    
                    UIImage *image = [UIImage imageWithData:cliente.foto];
                    
                    [self.fotosCache setObject:image forKey:indexPath];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        ContatoCell *updateCell = (ContatoCell *)[self.tableView cellForRowAtIndexPath:indexPath];
                        
                        [UIView transitionWithView:updateCell
                                          duration:0.2f
                                           options:UIViewAnimationOptionTransitionCrossDissolve
                                        animations:^{
                                            
                                            updateCell.fotoLabel.hidden = YES;
                                            updateCell.fotoImageView.image = image;
                                            
                                        } completion:NULL];
                    });
                }
            });
        }
    }
    
    // tipo
    cell.tipoImageView.hidden = subtipo.tipoCliente.descricao.length > 0 ? NO : YES;
    cell.subTipo.hidden = subtipo.tipoCliente.descricao.length > 0 ? NO : YES;
    
    if (subtipo.tipoCliente.descricao.length > 0) {
        // border around label
        cell.subTipo.layer.borderColor = [UIColor whiteColor].CGColor;
        cell.subTipo.layer.borderWidth = 2.0;
        //        cell.subTipo.layer.cornerRadius = 8.0;
        
        cell.subTipo.text = subtipo.descricao;
        cell.subTipo.textColor = [UIColor whiteColor];
        
        if ([subtipo.tipoCliente.descricao isEqualToString:@"Proprietário"]) {
            cell.tipoImageView.image = [UIImage imageNamed:@"contato-p"];
            cell.subTipo.backgroundColor = [UIColor colorWithRed:77.0/255 green:219.0/255 blue:109.0/255 alpha:1];
        } else if ([subtipo.tipoCliente.descricao isEqualToString:@"Interessado"]) {
            cell.tipoImageView.image = [UIImage imageNamed:@"contato-i"];
            cell.subTipo.backgroundColor = [UIColor colorWithRed:252.0/255 green:150.0/255 blue:42.0/255 alpha:1];
        } else {
            cell.tipoImageView.image = [UIImage imageNamed:@"contato-o"];
            cell.subTipo.backgroundColor = [UIColor colorWithRed:255.0/255 green:44.0/255 blue:85.0/255 alpha:1];
        }
    }
    
    BOOL isMapEnabled = YES;
    if (cliente.endereco.logradouro == nil || cliente.endereco.logradouro == (id)[NSNull null] || cliente.endereco.logradouro.length == 0) {
        isMapEnabled = NO;
    }
    
    dispatch_async(queueLoadSwipeButtons, ^{
        
        if ([self.tableView.indexPathsForVisibleRows containsObject:indexPath]) {
            
            NSMutableArray *leftUtilityButtons = [NSMutableArray new];
            NSMutableArray *rightUtilityButtons = [NSMutableArray new];
            
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                
                // left swipe
                if (isMapEnabled) {
                    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"contato-mapa"]];
                } else {
                    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"mapa-disable"]];
                }
                
                [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"contato-e-mail"]];
                [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"contato-editar"]];

                // right swipe
                [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"swipe-organizar"]];
                [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"swipe-apagar"]];
                
            } else { // iPhone
                
                // left swipe
                if (isMapEnabled) {
                    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor]
                                                                icon:[ImageUtils resize:[UIImage imageNamed:@"contato-mapa"] scaledToSize:CGSizeMake(47, 60)]];
                    
                } else {
                    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor]
                                                                icon:[ImageUtils resize:[UIImage imageNamed:@"mapa-disable"] scaledToSize:CGSizeMake(47, 60)]];
                }
                
                [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor]
                                                            icon:[ImageUtils resize:[UIImage imageNamed:@"contato-e-mail"] scaledToSize:CGSizeMake(47, 60)]];
                
                [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor]
                                                            icon:[ImageUtils resize:[UIImage imageNamed:@"contato-editar"] scaledToSize:CGSizeMake(47, 60)]];
                
                [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor]
                                                            icon:[ImageUtils resize:[UIImage imageNamed:@"contato-ligar"] scaledToSize:CGSizeMake(47, 60)]];
                
                // right swipe
                [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor]
                                                             icon:[ImageUtils resize:[UIImage imageNamed:@"swipe-organizar"] scaledToSize:CGSizeMake(47, 60)]];
                
                [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor]
                                                             icon:[ImageUtils resize:[UIImage imageNamed:@"swipe-apagar"] scaledToSize:CGSizeMake(47, 60)]];
            }

            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                ContatoCell *updateCell = (ContatoCell *)[self.tableView cellForRowAtIndexPath:indexPath];
                
                updateCell.leftUtilityButtons = leftUtilityButtons;
                updateCell.rightUtilityButtons = rightUtilityButtons;
            });
        }
    });
    
    cell.delegate = self;
}


#pragma mark - SWTableViewCellDelegate
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index
{
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    self.selectedContato = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    switch (index) {
        case 0:
            
            if (_selectedContato.endereco.logradouro.length > 0) {
                [self performSegueWithIdentifier:@"MapaSegue" sender:self];
            } else {
                [Utils showMessage:@"Para acessar o mapa, você deve preencher o endereço do contato!"];
            }
            
            break;
        case 1:
            [self sendMail:indexPath];
            break;
        case 2:
            [self performSegueWithIdentifier:@"EditarContatoSegue" sender:self];
            break;
        case 3:
            [self makeCall];
            break;
        default:
            break;
    }
    
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    self.selectedContato = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    switch (index) {
        case 0: // Organizar contatos
            [self performSegueWithIdentifier:@"OrganizarContatosSegue" sender:self];
            break;
            
        case 1: // Deletar
        {
            UIActionSheet *sheet =[[UIActionSheet alloc] initWithTitle:@"Remover contato?" delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:@"Remover" otherButtonTitles:nil, nil];
            [sheet showInView:cell];
            break;
        }
            
        default:
            break;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //
    //        // remove hightlight of all cells
    //        for (NSInteger s = 0; s < self.tableView.numberOfSections; s++) {
    //            for (NSInteger r = 0; r < [self.tableView numberOfRowsInSection:s]; r++) {
    //                ContatoCell *cell = (ContatoCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:r inSection:s]];
    //
    //                if (cell.isHighlighted) {
    //                    [cell setHighlighted:NO];
    //                }
    //            }
    //        }
    //
    //        // highlight selected cell
    //        ContatoCell *cell = (ContatoCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    //        [cell setHighlighted:YES];
    
    // Tratamento para marcar a cell selecionada e desmarcar a anterior.
    ContatoCell *cellSelected = (ContatoCell *) [self.tableView cellForRowAtIndexPath:indexPath];
    ContatoCell *cellOld = (ContatoCell *) [self.tableView cellForRowAtIndexPath:self.selectedIndexPath];
    
    cellSelected.selecionada.hidden = NO;
    
    if (cellSelected != cellOld) {
        cellOld.selecionada.hidden = YES;
    }
    
    self.selectedIndexPath = indexPath;
    
    Cliente *cliente = [self.fetchedResultsController objectAtIndexPath:indexPath];
    self.selectedContato = cliente;
    
    [self changeCliente:cliente];
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
}


#pragma mark - Send Email
- (void)sendMail:(NSIndexPath *)indexPath
{
    Cliente *cliente = [self.fetchedResultsController objectAtIndexPath:indexPath];
    self.mailViewController = [[MailViewController alloc] init];
    [self.mailViewController sendEmail:cliente forSender:self];
}


#pragma mark - Call to Contact

- (void)makeCall
{
    if (self.selectedContato.telefone && ![self.selectedContato.telefone isEqualToString:@""]) {
        
        if (self.selectedContato.telefoneExtra && ![self.selectedContato.telefoneExtra isEqualToString:@""]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ligação"
                                                            message:@"Para qual número deseja ligar?"
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancelar"
                                                  otherButtonTitles:self.selectedContato.telefone, self.selectedContato.telefoneExtra, nil];
            [alert show];
            
        } else {
            [self callToPhone:self.selectedContato.telefone];
        }
        
    } else {
        [Utils showMessage:@"Contato não possui telefone cadastrado."];
    }
}


- (void)callToPhone:(NSString *)phone
{
    // save the call history
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        Acompanhamento *acompanhamento = [Acompanhamento MR_createInContext:self.moc];
        acompanhamento.cliente = self.selectedContato;
        acompanhamento.codigo = [acompanhamento getUniqueCode]; //[NSNumber numberWithLong:codigo];
        acompanhamento.timeStamp = [NSDate date];
        acompanhamento.titulo = @"Ligação efetuada";
        
        [acompanhamento saveFirebase];
        
        [self saveMOC];
//        [acompanhamento saveParse:self.moc];
        
    }
    
    phone = [phone stringByReplacingOccurrencesOfString:@"(" withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@")" withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@"-" withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSURL *urlPhone = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@", phone]];
    [[UIApplication sharedApplication] openURL:urlPhone];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView.title isEqualToString:@"Ligação"]) {
        switch (buttonIndex) {
            case 1: // telefone principal
                [self callToPhone:self.selectedContato.telefone];
                break;
            case 2: // telefone extra
                [self callToPhone:self.selectedContato.telefoneExtra];
                break;
            default:
                break;
        }
    }
}


#pragma mark - Change Cliente
- (void)changeCliente:(Cliente *)cliente
{
    self.detalhesContatoViewController.superNavigationController = self.navigationController;
    [self.detalhesContatoViewController changeCliente:cliente];
    
    // Se for iPhone tem que navegar para a tela de detalhes.
    if (!([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) && cliente) {
        [self performSegueWithIdentifier:@"DetalhesContatoSegue" sender:self];
    }
}

#pragma mark - Filtro
- (IBAction)changeSegment:(UISegmentedControl *)segment
{
    
    [self changeCliente:nil];
    
    NSPredicate *predicate = nil;
    
    switch (segment.selectedSegmentIndex) {
        case 1:
            predicate = [NSPredicate predicateWithFormat:@"subtipo >= 1 AND subtipo <= 2"];
            break;
        case 2:
            predicate = [NSPredicate predicateWithFormat:@"subtipo >= 3 AND subtipo <= 4"];
            break;
        case 3:
            predicate = [NSPredicate predicateWithFormat:@"subtipo >= 5"];
            break;
        default:
            break;
    }
    
    self.fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithAlphabetic:@"Cliente" andKey:@"nome" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
    
    [self.fotosCache removeAllObjects];
    [self.tableView reloadData];
    
    [Utils trackFlurryWithName:@"Mudou Segmented Control da Lista de Contatos"];
}

#pragma mark - UISearchBarDelegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    [Utils trackWithName:@"Iniciou busca - Cliente"];
    [Utils trackFlurryWithName:@"Iniciou busca - Cliente"];
    
    self.tipoSegment.selectedSegmentIndex = 0;
    [self changeSegment:self.tipoSegment];
    
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    searchBar.showsCancelButton = YES;
    
    return YES;
}

// First use the Searchbar delegate methods
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self changeCliente:nil];
    [self filterContentForSearchText:searchText];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self filterContentForSearchText:searchBar.text];
    searchBar.showsCancelButton = NO;
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    searchBar.text = nil;
    searchBar.showsCancelButton = NO;
    [self filterContentForSearchText:nil];
    [searchBar resignFirstResponder];
}

// The method to change the predicate of the FRC
- (void)filterContentForSearchText:(NSString*)searchText
{
    NSString *query = searchText;
    
    if (query && query.length) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(nome contains[c] %@ OR email contains[c] %@ OR subtipo.descricao contains[c] %@ OR endereco.cep contains[c] %@ OR endereco.logradouro contains[c] %@ OR endereco.complemento contains[c] %@ OR endereco.estado contains[c] %@ OR endereco.cidade contains[c] %@ OR endereco.bairro.nome contains[c] %@ OR telefone contains[c] %@ OR telefoneExtra contains[c] %@)", query,query,query,query,query,query,query,query,query,query,query];
        
        [self.fetchedResultsController.fetchRequest setPredicate:predicate];
        [self.fetchedResultsController.fetchRequest setFetchLimit:100]; // Optional, but with large datasets - this helps speed lots
        
    } else {
        
        NSPredicate *predicate = nil;
        self.fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithAlphabetic:@"Cliente" andKey:@"nome" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
        
    }
    
    [self.fetchedResultsController performFetch:nil];
    
    [self.fotosCache removeAllObjects];
    [self.tableView reloadData];
}


#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        
        // Parse Delete
//        [self.selectedContato removeFromAllEventos];
//        [ParseUtils deleteEntity:self.selectedContato withName:@"Cliente" usingMoc:self.moc];
        
        // Firebase Delete
        [self.selectedContato deleteFirebase];
        
        // Grava o contato na tabela de deletado para não ser mais importado.
        ClienteDeletado *clienteDeletado = [ClienteDeletado createWithCliente:self.selectedContato andContext:self.moc];
        
        // Firebase
        [clienteDeletado saveFirebase];
        
//        [clienteDeletado saveParse:self.moc];
        
        [_selectedContato MR_deleteInContext:self.moc];
        
        [self.moc MR_saveToPersistentStoreAndWait];
        
        self.selectedIndexPath = nil;
        
        [self.fetchedResultsController performFetch:nil];
        
        [self.fotosCache removeAllObjects];
        [self.tableView reloadData:YES];
        
    }
}


#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"EditarContatoSegue"]) {
        
        UINavigationController *navController = segue.destinationViewController;
        EditarContatoViewController *controller = [[navController viewControllers] firstObject];
        controller.selectedCliente = self.selectedContato;
        
    } else if ([segue.identifier isEqualToString:@"MapaSegue"]) {
        
        MapaViewController *controller = segue.destinationViewController;
        controller.selectedContato = self.selectedContato;
        
    } else if ([segue.identifier isEqualToString:@"DetalhesContatoSegue"]) {
        DetalhesContatoViewController *controller = (DetalhesContatoViewController *)segue.destinationViewController;
        controller.selectedCliente = self.selectedContato;
        
    } else if ([segue.identifier isEqualToString:@"OrganizarContatosSegue"]) {
        UINavigationController *navController = segue.destinationViewController;
        OrganizarContatosViewController *controller = [[navController viewControllers] firstObject];
        controller.selectedContato = self.selectedContato;
    }
}


@end
