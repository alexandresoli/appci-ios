//
//  ListaEventosViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/24/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Evento.h"

@class DetalhesEventoViewController;

@interface ListaEventosViewController : BaseViewController

@property (nonatomic, strong) DetalhesEventoViewController *detalhesEventoViewController;

@property (nonatomic, strong) Evento *selectedEvento;

- (void)selectRowWithEvento:(Evento *)evento;

@end
