//
//  ListaEventosViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/24/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "ListaEventosViewController.h"
#import "DetalhesEventoViewController.h"
#import "DetalhesContatoViewController.h"
#import "DetalhesImovelViewController.h"
#import "EditarEventoViewController.h"
#import "MailViewController.h"
#import "CustomCalendar.h"

#import "Alarme.h"
#import "EventoCell.h"
#import "Evento+AlarmeLocalNotification.h"
#import "TipoEvento.h"
#import "Cliente.h"
#import "MZDayPicker.h"
#import "DateUtils.h"
#import "ImageUtils.h"
#import "Utils.h"
#import "Alerta.h"
#import "SmsManager.h"
#import "ImportarArquivoViewController.h"
#import "Sms.h"
#import "Corretor.h"
#import "Deduplicator.h"
#import "PasswordViewController.h"
#import "Imovel.h"
#import "Evento+Firebase.h"
#import "TSMessage.h"
#import "FotoImovel+InsertDeleteImagem.h"

@interface ListaEventosViewController () <SWTableViewCellDelegate,UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate,MZDayPickerDelegate,MZDayPickerDataSource,UISearchBarDelegate,UIActionSheetDelegate,UIAlertViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet MZDayPicker *dayPicker;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;
@property (nonatomic, strong) NSNumber *day;
@property (nonatomic, strong) NSNumber *month;
@property (nonatomic) BOOL usingPicker;

@property (nonatomic, weak) IBOutlet UISearchBar *eventosSearchBar;

@property (nonatomic, strong) MailViewController *mailViewController;

@property (nonatomic, strong) NSDateComponents *today;

@property (nonatomic, weak) IBOutlet UILabel *selectedMonthLabel;

@property (nonatomic, strong) UIPopoverController *popover;
@property (nonatomic, strong) UIDatePicker *changeDatePicker;

@property (nonatomic, strong) NSTimer *timerRefresh;

@end

@implementation ListaEventosViewController

@synthesize fetchedResultsController;


#pragma mark - Queue

dispatch_queue_t queueLoadSwipeButtons;


#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _today = [NSDateComponents new];
    _today.calendar = [NSCalendar currentCalendar];
    _today.year = [DateUtils currentYear];
    _today.hour = 0;
    _today.minute = 0;
    _today.month = [DateUtils currentMonth];
    _today.day = [DateUtils currentDay];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self configureCalendarIpad];
    } else {
        [self configureCalendarIphone];
    }
    
    self.selectedMonthLabel.text = [DateUtils monthYearDate:self.dayPicker.currentDate];
    
    // add model changes notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshEvento:) name:@"Evento" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshAlerta:) name:@"Alerta" object:nil];
    
    // Se for iPhone configura o BarButton de Alertas.
    if (![[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self configureAlertWithBarButton];
    }
    
    queueLoadSwipeButtons = dispatch_queue_create("ListaEventosViewController.LoadSwipeButtons", NULL);
    
    NSString *email = [Utils valueFromDefaults:USER_LOGIN];

    NSNumber *shouldHack = [Utils valueFromDefaults:FIREBASE_AMAURY_HACK];
    
    if (shouldHack == nil && [email isEqualToString:@"amaury@barrabest.com.br"]) {
        
        NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
        NSArray *photos = [FotoImovel MR_findAllInContext:moc];
        
        for (FotoImovel *photo in photos) {
            photo.pendingUpload = [NSNumber numberWithBool:YES];
        }
        
        [self.moc MR_saveToPersistentStoreAndWait];
        
        shouldHack = [NSNumber numberWithBool:NO];
        
        [Utils addObjectToDefaults:shouldHack withKey:FIREBASE_AMAURY_HACK];
    }
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Lista Eventos (Agenda)"];
    [Utils trackFlurryTimedWithName:@"Lista Eventos (Agenda)"];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // No iPad se o selectedEvento foi preenchido o mesmo é selecionado na tela.
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        if (self.selectedEvento) {
            [self performSelector:@selector(selectRowWithEvento:) withObject:self.selectedEvento afterDelay:0.2];
        }
    }
    
    // Verifica se o app foi iniciado através de uma local notification de evento.
    [self checkOpenFromLocalNotification];
    
    [self checkRefresh];
    
    self.timerRefresh = [NSTimer scheduledTimerWithTimeInterval:30.0
                                                         target:self
                                                       selector:@selector(checkRefreshWithNotification)
                                                       userInfo:nil
                                                        repeats:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self.timerRefresh invalidate];
    [TSMessage dismissActiveNotification];
    
    [Flurry endTimedEvent:@"Lista Eventos (Agenda)" withParameters:nil];
}


#pragma mark - Check Refresh

- (void)checkRefresh
{
    NSNumber *refresh = [Utils valueFromDefaults:REFRESH_EVENTO];
    if ([refresh boolValue]) {
        [TSMessage dismissActiveNotification];
        [self refreshEvento:nil];
    }
}

- (void)checkRefreshWithNotification
{
    NSNumber *refresh = [Utils valueFromDefaults:REFRESH_EVENTO];
    if ([refresh boolValue]) {
        [self showNotificationRefresh];
    }
}

- (void)showNotificationRefresh
{
    if (![Utils valueFromDefaults:FIREBASE_ALREADY_DOWNLOAD_ALL]) {
        return;
    }
    
    if ([TSMessage queuedMessages].count > 0) {
        return;
    }
    
    [TSMessage showNotificationInViewController:self.parentViewController.parentViewController
                                          title:@"Novas informações disponíveis. Toque aqui para atualizar."
                                       subtitle:nil
                                          image:nil
                                           type:TSMessageNotificationTypeSuccess
                                       duration:TSMessageNotificationDurationEndless
                                       callback:^{
                                           
                                           [Utils trackWithName:@"Atualização Firebase - Lista Eventos - Clicou na faixa"];
                                           [Utils trackFlurryWithName:@"Atualização Firebase - Lista Eventos - Clicou na faixa"];
                                           
                                           [TSMessage dismissActiveNotification];
                                           [self refreshEvento:nil];
                                       }
                                    buttonTitle:nil
                                 buttonCallback:nil
                                     atPosition:TSMessageNotificationPositionTop
                           canBeDismissedByUser:YES];
}


#pragma mark - Check open from local notification

- (void)checkOpenFromLocalNotification
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSNumber *codigoEvento = [userDefaults objectForKey:EVENTO_NOTIFICATION];
    NSString *codigoSMS = [userDefaults objectForKey:SMS_NOTIFICATION];
    
    if (codigoEvento) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@", codigoEvento];
        NSArray *eventos = [Evento MR_findAllWithPredicate:predicate inContext:self.moc];
        
        if (eventos.count > 0) {
            Evento *evento = [eventos firstObject];
            
            [self performSelector:@selector(selectRowWithEvento:) withObject:evento afterDelay:0.2];
        }
        
        [userDefaults removeObjectForKey:EVENTO_NOTIFICATION];
        [userDefaults synchronize];
        
    } else if (codigoSMS) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigoSMS = %@", codigoSMS];
        NSArray *smsArray = [Sms MR_findAllWithPredicate:predicate inContext:self.moc];
        
        if (smsArray.count > 0) {
            Sms *sms = [smsArray firstObject];
            
            predicate = [NSPredicate predicateWithFormat:@"codigo = %@", sms.evento.codigo];
            NSArray *eventos = [Evento MR_findAllWithPredicate:predicate inContext:self.moc];
            
            if (eventos.count > 0) {
                Evento *evento = [eventos firstObject];
                
                [self performSelector:@selector(selectRowWithEvento:) withObject:evento afterDelay:0.2];
            }
        }
        
        [userDefaults removeObjectForKey:SMS_NOTIFICATION];
        [userDefaults synchronize];
    }
}


#pragma mark - Select Specific Row

- (void)selectRowWithEvento:(Evento *)evento
{
    MZDay *mzday = [[MZDay alloc] init];
    
    mzday.date = evento.dataInicio;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:mzday.date];
    
    mzday.day = [NSNumber numberWithInteger:[components day]];
    mzday.month = [NSNumber numberWithInteger:[components month]];
    mzday.year = [NSNumber numberWithInteger:[components year]];
    
    [self.dayPicker setCurrentDate:[NSDate date] animated:YES];
    
    [self dayPicker:self.dayPicker didSelectDay:mzday];
    
    NSIndexPath *indexPath = [fetchedResultsController indexPathForObject:evento];
    
    // trigger the selection
    [self.tableView.delegate tableView:self.tableView didSelectRowAtIndexPath:indexPath];
    
    // scroll to cell position
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
}


#pragma mark - IBActions

- (IBAction)goToday:(id)sender
{
    MZDay *today = [[MZDay alloc] init];
    
    today.date = [NSDate date];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:today.date];
    
    today.day = [NSNumber numberWithInteger:[components day]];
    today.month = [NSNumber numberWithInteger:[components month]];
    today.year = [NSNumber numberWithInteger:[components year]];
    
    [self.dayPicker setCurrentDate:[NSDate date] animated:YES];
    
    [self dayPicker:self.dayPicker didSelectDay:today];
    
    [self scrollTableViewToTop];
}


// Cria um popover com um date picker e um button para seleção da data.
- (IBAction)selectDate:(id)sender
{
    UIViewController* popoverContent = [[UIViewController alloc] init];
    
    UIView *popoverView = [[UIView alloc] init];
    popoverView.backgroundColor = [UIColor whiteColor];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    datePicker.frame = CGRectMake(0, 0, 320, 216);
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.date = self.dayPicker.currentDate;
    datePicker.minimumDate = self.dayPicker.startDate;
    datePicker.maximumDate = self.dayPicker.endDate;
    
    [popoverView addSubview:datePicker];
    
    UIButton *buttonOk = [UIButton buttonWithType:UIButtonTypeSystem];
    buttonOk.frame = CGRectMake(0, 230, 320, 30);
    [buttonOk setTitle:@"OK" forState:UIControlStateNormal];
    [buttonOk addTarget:self action:@selector(changeDateByDatePicker) forControlEvents:UIControlEventTouchUpInside];
    
    [popoverView addSubview:buttonOk];
    
    
    popoverContent.view = popoverView;
    UIPopoverController *popoverController = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
    popoverController.delegate = self;
    
    UIButton *button = (UIButton *)sender;
    
    [popoverController setPopoverContentSize:CGSizeMake(320, 270) animated:NO];
    [popoverController presentPopoverFromRect:button.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
    self.popover = popoverController;
    self.changeDatePicker = datePicker;
}


- (IBAction)selectDateIphone:(id)sender
{
    UIView *modalView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    modalView.opaque = NO;
    modalView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    datePicker.frame = CGRectMake(0, 215, 320, 162);
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.date = self.dayPicker.currentDate;
    datePicker.minimumDate = self.dayPicker.startDate;
    datePicker.maximumDate = self.dayPicker.endDate;
    datePicker.backgroundColor = [UIColor whiteColor];
    
    [modalView addSubview:datePicker];
    
    UIButton *buttonOk = [UIButton buttonWithType:UIButtonTypeSystem];
    buttonOk.frame = CGRectMake(0, 372, 320, 40);
    [buttonOk setTitle:@"OK" forState:UIControlStateNormal];
    [buttonOk addTarget:self action:@selector(changeDateByDatePicker) forControlEvents:UIControlEventTouchUpInside];
    buttonOk.backgroundColor = [UIColor whiteColor];
    
    [modalView addSubview:buttonOk];
    
    [UIView transitionWithView:self.view
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^ { [self.view addSubview:modalView]; }
                    completion:nil];
    
    self.changeDatePicker = datePicker;
    
}


// Método executado ao escolher uma data pela opção "Selecionar Data".
- (void)changeDateByDatePicker
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self.popover dismissPopoverAnimated:YES];
    } else {
        NSArray *subviews = [self.view subviews];
        UIView *viewDatePicker = [subviews lastObject];
        [UIView transitionWithView:self.view
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^ { [viewDatePicker removeFromSuperview]; }
                        completion:nil];
    }
    
    MZDay *daySelected = [[MZDay alloc] init];
    
    daySelected.date = self.changeDatePicker.date;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:daySelected.date];
    
    daySelected.day = [NSNumber numberWithInteger:[components day]];
    daySelected.month = [NSNumber numberWithInteger:[components month]];
    daySelected.year = [NSNumber numberWithInteger:[components year]];
    
    [self.dayPicker setCurrentDate:daySelected.date animated:YES];
    
    [self dayPicker:self.dayPicker didSelectDay:daySelected];
    
    self.popover = nil;
    self.changeDatePicker = nil;
}


#pragma mark - Notification Center

- (void)refreshEvento:(NSNotification *)note
{
    [Utils addObjectToDefaults:[NSNumber numberWithBool:NO] withKey:REFRESH_EVENTO];
    
    self.selectedEvento = nil;
    self.selectedIndexPath = nil;
    [self.detalhesEventoViewController changeDetalhes:nil];
    
    fetchedResultsController = nil;
    [self.tableView reloadData:YES];
}

- (void)refreshAlerta:(NSNotification *)note
{
    // Se houve mudança de alertas atualiza o BarButton no iPhone.
    if (![[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self configureAlertWithBarButton];
    }
}


#pragma mark - UITableViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    self.usingPicker = NO;
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    self.usingPicker = NO;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Tratamento para marcar a cell selecionada e desmarcar a anterior.
    EventoCell *cellSelected = (EventoCell *) [self.tableView cellForRowAtIndexPath:indexPath];
    EventoCell *cellOld = (EventoCell *) [self.tableView cellForRowAtIndexPath:self.selectedIndexPath];
    
    cellSelected.selecionada.hidden = NO;
    
    if (cellSelected != cellOld) {
        cellOld.selecionada.hidden = YES;
    }
    
    // Exibição dos detalhes do evento.
    Evento *evento = [fetchedResultsController objectAtIndexPath:indexPath];
    
    [self.detalhesEventoViewController changeDetalhes:evento];
    
    self.selectedIndexPath = indexPath;
    
    // Se o device for iPhone é necessário fazer o performSegue.
    if (![[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self performSegueWithIdentifier:@"DetalhesEventoSegue" sender:self];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventoCell *cell = (EventoCell *) [self.tableView cellForRowAtIndexPath:indexPath];
    
    //    cell.tipo.layer.borderWidth = 0.0;
    
    cell.horarioInicio.textColor = [UIColor blackColor];
    cell.horarioFim.textColor = [UIColor blackColor];
    cell.descricao.textColor = [UIColor blackColor];
    
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"EventoCell";
    EventoCell *cell = (EventoCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;;
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];
    
    // SWTableViewCell
    //    [cell setCellHeight:cell.frame.size.height];
    //    cell.containingTableView = tableView;
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


- (void)configureCell:(EventoCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    
    Evento *evento = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // Tratamento para marcar a cell selecionada.
    if ([self.selectedIndexPath isEqual:indexPath]) {
        cell.selecionada.hidden = NO;
    } else {
        cell.selecionada.hidden = YES;
    }
    
    // disable style - Retirado pois agora a cell selecionada fica marcada de cinza.
    /*
     NSComparisonResult result = [DateUtils compare:[NSDate date] withDate:evento.dataFim];
     
     if(result == NSOrderedDescending )
     {
     cell.contentView.backgroundColor = [UIColor colorWithRed:234.0/255 green:235.0/255 blue:237.0/255 alpha:1.0];
     }
     */
    
    cell.horarioInicio.text = [DateUtils stringFromDate:evento.dataInicio withFormat:@"HH:mm"];
    cell.horarioFim.text = [DateUtils stringFromDate:evento.dataFim withFormat:@"HH:mm"];
    cell.descricao.text = evento.titulo;
    
    // alarm
    
    NSArray *alarmes = [NSKeyedUnarchiver unarchiveObjectWithData:evento.alarmes];
    if ([alarmes count] > 0) {
        NSString *textoAlarme;
        
        for (NSNumber *codigoAlarme in alarmes) {
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",codigoAlarme];
            
            Alarme *alarm = [Alarme MR_findFirstWithPredicate:predicate inContext:self.moc];
            
            if (textoAlarme) {
                textoAlarme = [NSString stringWithFormat:@"%@ - %@", textoAlarme, alarm.descricao];
            } else {
                textoAlarme = alarm.descricao;
            }
        }
        
        cell.alarm.text = textoAlarme;
        cell.alarmImage.image = [UIImage imageNamed:@"alarme-on.png"];
        
    } else {
        cell.alarm.text = @"";
        cell.alarmImage.image = [UIImage imageNamed:@"alarme-off.png"];
    }
    
    
    if (evento.local.length == 0) {
        cell.localizacao.text = @"Não informado";
    } else {
        cell.localizacao.text = evento.local;
    }
    
    [self configureTipo:cell withEvento:evento];
    
    
    if (!self.usingPicker) {
        [self.dayPicker setCurrentDate:evento.dataInicio animated:YES];
    }
    
    BOOL canSendSms = NO;
    
    if ([evento.sms allObjects].count == 0) {
        canSendSms = YES;
    }
    
    dispatch_async(queueLoadSwipeButtons, ^{
        
        if ([self.tableView.indexPathsForVisibleRows containsObject:indexPath]) {
            
            NSMutableArray *leftUtilityButtons = [NSMutableArray new];
            NSMutableArray *rightUtilityButtons = [NSMutableArray new];
            
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                // left swipe
                
                if (canSendSms) {
                    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"swipe-sms"]];
                } else {
                    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"swipe-sms-hover"]]; // imagem cinza
                }
                
                [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"portfolio-e-mail"]];
                [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"portfolio-editar"]];
                
                // right swipe
                [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"swipe-apagar"]];
                
            } else {
                
                if (canSendSms) {
                    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"swipe-sms"] scaledToSize:CGSizeMake(47, 60)]];
                } else {
                    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"swipe-sms-hover"] scaledToSize:CGSizeMake(47, 60)]];
                }
                
                // left swipe
                [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"portfolio-e-mail"] scaledToSize:CGSizeMake(47, 60)]];
                [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"portfolio-editar"] scaledToSize:CGSizeMake(47, 60)]];
                
                // right swipe
                [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"swipe-apagar"] scaledToSize:CGSizeMake(47, 60)]];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                EventoCell *updateCell = (EventoCell *)[self.tableView cellForRowAtIndexPath:indexPath];
                
                [UIView transitionWithView:updateCell
                                  duration:0.2f
                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                animations:^{
                                    
                                    updateCell.leftUtilityButtons = leftUtilityButtons;
                                    updateCell.rightUtilityButtons = rightUtilityButtons;
                                    
                                } completion:NULL];
            });
        }
    });
    
    cell.delegate = self;
}


- (void)configureTipo:(EventoCell *)cell withEvento:(Evento *)evento
{
    // border around label
    //    cell.tipo.layer.borderColor = [UIColor whiteColor].CGColor;
    //    cell.tipo.layer.borderWidth = 2.0;
    //    cell.tipo.layer.cornerRadius = 8.0;
    
    cell.tipo.text = evento.tipoEvento;
    cell.tipo.textColor = [UIColor whiteColor];
    
    NSString *tipoText = cell.tipo.text;
    
    cell.tipo.hidden = (tipoText.length > 0) ? NO : YES;
    
    if ([tipoText isEqualToString:@"Visita"]) {
        cell.tipo.backgroundColor =  [UIColor colorWithRed:77.0/255 green:219.0/255 blue:109.0/255 alpha:1.0];
    } else if([tipoText isEqualToString:@"Captação"]) {
        cell.tipo.backgroundColor =  [UIColor colorWithRed:255.0/255 green:146.0/255 blue:113.0/255 alpha:1.0];
    } else if([tipoText isEqualToString:@"Acompanhamento"]) {
        cell.tipo.backgroundColor =  [UIColor colorWithRed:98.0/255 green:121.0/255 blue:235.0/255 alpha:1.0];
    } else if([tipoText isEqualToString:@"Reunião"]) {
        cell.tipo.backgroundColor =  [UIColor colorWithRed:28.0/255 green:121.0/255 blue:235.0/255 alpha:1.0];
    } else if([tipoText isEqualToString:@"Outros"]) {
        cell.tipo.backgroundColor =  [UIColor colorWithRed:98.0/255 green:101.0/255 blue:135.0/255 alpha:1.0];
    }
    
}


#pragma mark - SWTableViewCellDelegate
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index
{
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    self.selectedEvento = [fetchedResultsController objectAtIndexPath:indexPath];
    
    switch (index) {
        case 0: // SMS
        {
            [Utils trackWithName:@"Clicou no icone de SMS"];
            [Utils trackFlurryWithName:@"Clicou no icone de SMS"];
            
            if (![self.iapHelper checkPlanTopFromController:self withMessage:PLAN_TOP_MESSAGE_SMS]) {
                return;
            }
            
            if (![self isFirstSMS]) {
                return;
            }
            
            
            if (self.selectedEvento.cliente.count == 0) {
                [Utils showMessage:@"Esse evento não possui contatos associados."];
                return;
            }
            
            if (![self hasValidPhoneNumbers]) {
                
                [Utils showMessage:@"Um ou mais contatos não possuem um telefone válido. O telefone do contato deve conter o DDD."];
                return;
            }
            
            if (![Utils personalDataIsFilled:self
                                 withContext:self.moc
                                  andMessage:@"Para confirmação de eventos é necessário o preenchimento de suas informações!"]) {
                return;
            }
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SMS" message:@"Enviar SMS para os contatos deste evento solicitando confirmação no evento?" delegate:self cancelButtonTitle:@"Não" otherButtonTitles:@"Sim", nil];
            
            [alert show];
            
            break;
        }
            
        case 1: // email
            [self sendMail:indexPath];
            break;
            
        case 2: // editar
            [self performSegueWithIdentifier:@"EditarEventoSegue" sender:self];
            break;
            
        default:
            break;
    }
    
}


#pragma mark - SMS Validation

- (BOOL)isFirstSMS
{
    if ([self.selectedEvento.sms count] > 0) {
        [Utils trackWithName:@"Tentou enviar mais de 1 SMS por evento"];
        [Utils trackFlurryWithName:@"Tentou enviar mais de 1 SMS por evento"];
        
        
        [Utils showMessage:@"Não é possível enviar mais de um SMS por evento."];
        return NO;
    }
    
    
    return YES;
}

- (BOOL)hasValidPhoneNumbers
{
    
    BOOL isValid = YES;
    NSArray *contacts = [self.selectedEvento.cliente allObjects];
    
    
    for (Cliente *contact in contacts) {
        
        if (contact.telefone.length < 10) { //([contact.telefone rangeOfString:@"("].location == NSNotFound) {
            isValid = NO;
            break;
        }
    }
    
    return isValid;
}


- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)indexPath
{
    
    self.selectedIndexPath = [self.tableView indexPathForCell:cell];
    
    self.selectedEvento = [fetchedResultsController objectAtIndexPath:self.selectedIndexPath];
    
    NSString *message;
    UIActionSheet *sheet;
    
    if ([_selectedEvento.recursivo boolValue]) {
        sheet =[[UIActionSheet alloc] initWithTitle:@"Este evento se repete mais de uma vez. Apagar todas as entradas?" delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:@"Apagar Tudo" otherButtonTitles:@"Apenas Evento Selecionado", nil];
        [sheet showInView:cell];
    } else {
        sheet =[[UIActionSheet alloc] initWithTitle:message delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:@"Remover" otherButtonTitles:nil, nil];
    }
    
    [sheet showInView:cell];
    
}


- (NSString *)formatTitle:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> theSection = [[self.fetchedResultsController sections] objectAtIndex:section];
    
    /*
     Section information derives from an event's sectionIdentifier, which is a string representing the number (year * 1000) + month.
     To display the section title, convert the year and month components to a string representation.
     */
    static NSDateFormatter *formatter = nil;
    
    if (!formatter)
    {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setCalendar:[NSCalendar currentCalendar]];
        
        NSString *formatTemplate = [NSDateFormatter dateFormatFromTemplate:@"d MMMM YYYY" options:0 locale:[NSLocale currentLocale]];
        [formatter setDateFormat:formatTemplate];
    }
    
    //NSInteger numericSection = [[theSection name] integerValue];
    
    NSString *sectionName = [theSection name];
    
    NSInteger year = [[sectionName substringWithRange:NSMakeRange(0, 4)] integerValue]; //numericSection / 1000;
    NSInteger month = [[sectionName substringWithRange:NSMakeRange(4, 2)] integerValue]; //(numericSection - (year * 1000)) / 100;
    NSInteger day = [[sectionName substringWithRange:NSMakeRange(6, 2)] integerValue]; //numericSection - ((year * 1000) + (month *100));
    
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    dateComponents.year = year;
    dateComponents.month = month;
    dateComponents.day = day;
    NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
    
    
    NSString *titleString = [formatter stringFromDate:date];
    NSString *firstLetter = [[titleString substringToIndex:1] capitalizedString];
    titleString = [titleString stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstLetter];
    
    
    return [NSString stringWithFormat:@"   %@",titleString];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *labelView = [[UILabel alloc] init];
    labelView.textAlignment = NSTextAlignmentLeft;
    //labelView.backgroundColor = [UIColor colorWithRed:0.0/255 green:122.0/255 blue:255.0/255 alpha:1.0];
    //labelView.textColor = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0];
    labelView.backgroundColor = [UIColor colorWithRed:0.851 green:0.851 blue:0.851 alpha:1];
    labelView.textColor = [UIColor blackColor];
    
    labelView.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
    labelView.text = [self formatTitle:section];
    
    return labelView;
}


#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (fetchedResultsController != nil) {
        return fetchedResultsController;
    }
    
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dataInicio > %@",_today.date];
    
    fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Evento" withSortKey:@"dataInicio" withSectionName:@"sectionIdentifier" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
    
    //    fetchedResultsController = [Evento MR_fetchAllGroupedBy:@"sectionIdentifier" withPredicate:predicate sortedBy:@"dataInicio" ascending:YES];
    
    fetchedResultsController.delegate = self;
    
    
    return fetchedResultsController;
}


#pragma mark - Calendar

- (void)configureCalendarIpad
{
    self.dayPicker.delegate = self;
    
    self.dayPicker.dayNameLabelFontSize = 14;
    self.dayPicker.dayLabelFontSize = 25;
    self.dayPicker.dayLabelZoomScale = 0;
    self.dayPicker.dateNameFormat = DATE_NAME_FORMAT_IPAD;
    
    [self.dayPicker setStartDate:[NSDate dateFromDay:_today.day month:_today.month-1 year:_today.year] endDate:[NSDate dateFromDay:_today.day month:_today.month year:_today.year+1]];
    
    
    [self.dayPicker setCurrentDate:[NSDate date] animated:NO];
    
    self.day = [NSNumber numberWithInteger:[DateUtils currentDay]];
    self.month = [NSNumber numberWithInteger:[DateUtils currentMonth]];
    
}


- (void)configureCalendarIphone
{
    self.dayPicker.delegate = self;
    
    self.dayPicker.dayNameLabelFontSize = 12;
    self.dayPicker.dayLabelFontSize = 17;
    self.dayPicker.dayLabelZoomScale = 0;
    self.dayPicker.dateNameFormat = DATE_NAME_FORMAT_IPHONE;
    
    [self.dayPicker setStartDate:[NSDate dateFromDay:_today.day month:_today.month-1 year:_today.year] endDate:[NSDate dateFromDay:_today.day month:_today.month year:_today.year+1]];
    [self.dayPicker setCurrentDate:[NSDate date] animated:YES];
    
    self.day = [NSNumber numberWithInteger:[DateUtils currentDay]];
    self.month = [NSNumber numberWithInteger:[DateUtils currentMonth]];
}


#pragma mark - MZDayPickerDelegate

- (void)dayPicker:(MZDayPicker *)dayPicker didSelectDay:(MZDay *)day
{
    self.day = day.day;
    self.month = day.month;
    
    _today.day = [self.day intValue];
    _today.month = [self.month intValue];
    _today.year = [day.year intValue];
    
    self.usingPicker = YES;
    
    self.selectedEvento = nil;
    self.selectedIndexPath = nil;
    [self.detalhesEventoViewController changeDetalhes:nil];
    
    fetchedResultsController  = nil;
    [fetchedResultsController performFetch:nil];
    [self.tableView reloadData];
    
    self.selectedMonthLabel.text = [DateUtils monthYearDate:day.date];
}

- (void)scrollTableViewToTop
{
    if ([self numberOfSectionsInTableView:self.tableView] > 0){
        NSIndexPath* top = [NSIndexPath indexPathForRow:NSNotFound inSection:0];
        [self.tableView scrollToRowAtIndexPath:top atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}


#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    
    [Utils trackWithName:@"Iniciou busca - Evento"];
    [Utils trackFlurryWithName:@"Iniciou busca - Evento"];
    
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    searchBar.showsCancelButton = YES;
    
    return YES;
}


// First use the Searchbar delegate methods
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self filterContentForSearchText:searchText];
}


-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self filterContentForSearchText:searchBar.text];
    searchBar.showsCancelButton = NO;
    [searchBar resignFirstResponder];
}


- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    searchBar.text = nil;
    searchBar.showsCancelButton = NO;
    [self filterContentForSearchText:nil];
    [searchBar resignFirstResponder];
}


// The method to change the predicate of the FRC
- (void)filterContentForSearchText:(NSString*)searchText
{
    NSString *query = searchText;
    
    if (query && query.length) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(local contains[c] %@ OR notas contains[c] %@ OR titulo contains[c] %@ OR cliente.nome contains[c] %@ OR imovel.endereco.logradouro contains[c] %@ OR imovel.endereco.bairro.nome contains[c] %@)",query,query,query,query,query,query];
        
        [fetchedResultsController.fetchRequest setPredicate:predicate];
        [fetchedResultsController.fetchRequest setFetchLimit:100];
        
    } else {
        
        fetchedResultsController  = nil;
        [fetchedResultsController performFetch:nil];
        //        [self fetch];
        
    }
    
    [self.fetchedResultsController performFetch:nil];
    
    [self.tableView reloadData];
}


#pragma mark - Send Email

- (void)sendMail:(NSIndexPath *)indexPath
{
    Evento *evento = [self.fetchedResultsController objectAtIndexPath:indexPath];
    self.mailViewController = [[MailViewController alloc] init];
    [self.mailViewController sendEmailEvento:evento forSender:self];
}


#pragma mark - UIActionSheetDelegate
- (void)reloadTable
{
    self.selectedEvento = nil;
    self.selectedIndexPath = nil;
    [self.detalhesEventoViewController changeDetalhes:nil];
    
    [self.fetchedResultsController performFetch:nil];
    [self.tableView reloadData:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // recursivo
    if (_selectedEvento.recursivo) {
        
        if (buttonIndex == 0) {
            [Utils showProgress:@"Aguarde..."];
            [self performSelector:@selector(deleteRecurrenceEvents) withObject:nil afterDelay:0.2];
            
        } else if (buttonIndex == 1) {
            [self deleteEvent:_selectedEvento];
            [self reloadTable];
        }
        
        
    } else { // normal
        
        if (buttonIndex == 0) {
            [self deleteEvent:_selectedEvento];
            [self reloadTable];
        }
    }
    
    
}

- (void)deleteRecurrenceEvents
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"identificadorEventoCalendario = %@ AND recursivo = YES",_selectedEvento.identificadorEventoCalendario];
    
    NSArray *events = [Evento MR_findAllWithPredicate:predicate inContext:self.moc];
    
    for (Evento *event in events) {
        [self deleteEvent:event];
    }
    
    [self.moc MR_saveToPersistentStoreAndWait];
    
    [Utils dismissProgress];
    [self reloadTable];
}

- (void)deleteEvent:(Evento *)event
{
    [self deleteEventFromCalendar:event.identificadorEventoCalendario
                    withStartDate:event.dataInicio
                       andEndDate:event.dataFim];
    
    [event cancelEventoLocalNotifications];
    
    // Parse SDK
    //    [ParseUtils deleteEntity:event withName:@"Evento" usingMoc:self.moc];
    [event deleteFirebase];
    
    [event MR_deleteInContext:self.moc];
    
    [self.moc MR_saveToPersistentStoreAndWait];
    
}

- (void)deleteEventFromCalendar:(NSString *)eventIdentifier withStartDate:(NSDate *)startDate andEndDate:(NSDate *)endDate
{
    [CustomCalendar requestAccess:^(BOOL granted, NSError *error) {
        
        if (granted) {
            BOOL success = [CustomCalendar deleteEventWithIdentifier:eventIdentifier andStartDate:startDate andEndDate:endDate];
            
            if (!success) {
                [Utils showMessage:@"Ocorreu um erro ao excluir o evento no calendário do dispositivo!"];
            }
            
        } else {
//            [Utils showMessage:@"O Appci não conseguiu excluir seu evento no calendário do dispositivo :-( \nPara habilitar o evento no calendario do dispositivo, abra as configurações > Privacidade > Calendários e habilite o Appci!"];
        }
    }];
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView.title isEqualToString:@"SMS"]) {
        
        if (buttonIndex == 1) { // Sim
            
            [Utils trackWithName:@"Enviou SMS"];
            [Utils trackFlurryWithName:@"Enviou SMS"];
            
            
            BOOL isSentSms = NO;
            
            for (Cliente *cliente in self.selectedEvento.cliente) {
                
                if (cliente.telefone) {
                    [SmsManager sendSmsTo:cliente forEvento:self.selectedEvento withContext:self.moc];
                    isSentSms = YES;
                }
            }
            
            if (!isSentSms) {
                [Utils showMessage:@"Nenhum dos contatos do evento possui telefone cadastrado."];
            }
        }
    }
}


#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([segue.identifier isEqualToString: @"EditarEventoSegue"]) {
        
        UINavigationController *navController = segue.destinationViewController;
        EditarEventoViewController *controller = [navController.viewControllers firstObject];
        controller.selectedEvento = self.selectedEvento;
        
    } else if ([segue.identifier isEqualToString:@"DetalhesEventoSegue"]) {
        
        Evento *evento = [fetchedResultsController objectAtIndexPath:self.selectedIndexPath];
        
        DetalhesEventoViewController *controller = (DetalhesEventoViewController *)segue.destinationViewController;
        
        controller.eventoSelected = evento;
    }
}


@end
