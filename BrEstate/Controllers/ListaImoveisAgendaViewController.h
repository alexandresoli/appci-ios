//
//  ListaImoveisAgendaViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 2/5/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "BaseTableViewController.h"

@class Evento;
@interface ListaImoveisAgendaViewController : BaseTableViewController

@property (nonatomic, weak) UINavigationController *superNavigationController;

- (void)changeImoveis:(Evento *)evento;

@end
