//
//  ListaImoveisAgendaViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 2/5/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "ListaImoveisAgendaViewController.h"
#import "PortfolioViewController.h"

#import "DetalhesImovelCell.h"
#import "Imovel.h"
#import "TipoImovel.h"
#import "TipoOferta.h"
#import "Endereco.h"
#import "Bairro.h"
#import "FotoImovel+InsertDeleteImagem.h"
#import "Evento.h"
#import "ListaImoveisViewController.h"

@interface ListaImoveisAgendaViewController () <UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) Evento *selectedEvento;
@property (nonatomic, strong) NSCache *fotosCache;

@end

@implementation ListaImoveisAgendaViewController


#pragma mark - Queue

dispatch_queue_t queueLoadFoto;


#pragma mark - View Life Cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    queueLoadFoto = dispatch_queue_create("ListaImoveisAgendaViewController.LoadFoto", NULL);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Listar Imoveis (Agenda)"];
    [Utils trackFlurryWithName:@"Listar Imoveis (Agenda)"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSCache *)fotosCache
{
    if (_fotosCache) {
        return _fotosCache;
    }
    
    _fotosCache = [[NSCache alloc] init];
    [_fotosCache setCountLimit:15];
    
    return _fotosCache;
}


#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    _fetchedResultsController.delegate = self;
    
    
    NSPredicate *predicate = nil;
    
    if (self.selectedEvento != nil) {
        
        predicate = [NSPredicate predicateWithFormat:@"ANY evento == %@",_selectedEvento];
        
        _fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Imovel" andKey:@"codigo" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
    }
    
    return _fetchedResultsController;
}



#pragma mark - UITableViewDatasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"DetalhesImovelCell";
    DetalhesImovelCell *cell = (DetalhesImovelCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;;
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];
    
    Imovel *imovel = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [self configureTags:imovel cell:cell];
    [self configureCell:cell atIndexPath:indexPath forImovel:imovel];
    
    return cell;
}



- (void)configureCell:(DetalhesImovelCell *)cell atIndexPath:(NSIndexPath *)indexPath forImovel:(Imovel *)imovel
{
    NSArray *fotos = [imovel.fotos allObjects];
    
    cell.codigo.text = [NSString stringWithFormat:@"%@",imovel.identificacao];
    
    if(imovel.endereco.logradouro.length > 0) {
        cell.enderecoLabel.text = imovel.endereco.logradouro;
    } else {
        cell.enderecoLabel.text = @"Não informado.";
    }
    
    cell.fotoImageView.image = nil;
    
    if (fotos.count == 0) {
        cell.fotoImageView.image = [UIImage imageNamed:@"nao-disponivel2"];
        
    } else {
        cell.fotoImageView.image = [self.fotosCache objectForKey:indexPath];
        
        if (!cell.fotoImageView.image) {
            __block FotoImovel *fotoImovel = [[fotos filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"capa = YES"]] firstObject];
            
            dispatch_async(queueLoadFoto, ^{
                
                if (![self.tableView.indexPathsForVisibleRows containsObject:indexPath]) {
                    return;
                }
                
                if (fotoImovel == nil) { // sem capa
                    fotoImovel = [fotos firstObject];
                }
                
                UIImage *image = [fotoImovel getImageForFotoImovelWithMaxPixelSize:500];
                
                if (image) {
                    [self.fotosCache setObject:image forKey:indexPath];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    DetalhesImovelCell *updateCell = (DetalhesImovelCell *)[self.tableView cellForRowAtIndexPath:indexPath];
                    [UIView transitionWithView:updateCell.fotoImageView
                                      duration:0.2f
                                       options:UIViewAnimationOptionTransitionCrossDissolve
                                    animations:^{
                                        
                                        updateCell.fotoImageView.image = image;
                                        
                                    } completion:NULL];
                });
            });
        }
    }
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [Utils trackWithName:@"Clicou em imovel - Detalhes do Evento"];
    [Utils trackFlurryWithName:@"Clicou em imovel - Detalhes do Evento"];

    Imovel *selectedImovel = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        // Get the storyboard named secondStoryBoard from the main bundle:
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"_Portfolio_iPad" bundle:nil];
        
        // Load the initial view controller from the storyboard.
        PortfolioViewController *controller = [storyboard instantiateInitialViewController];
        controller.selectedImovel = selectedImovel;
        
        //[self.superNavigationController pushViewController:controller animated:YES];
        // substituído o pushViewController pelo setViewControllers por causa de problema de memória.
        [self.superNavigationController setViewControllers:@[controller] animated:YES];
        
    } else {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Portfolio_iPhone" bundle:nil];
        UINavigationController *navController = [storyboard instantiateInitialViewController];
        ListaImoveisViewController *listaImoveisController = [navController.childViewControllers firstObject];
        listaImoveisController.selectedImovelNavigationIphone = selectedImovel;
        
        UINavigationController *navImovelController = [self.tabBarController.viewControllers objectAtIndex:2];
        [navImovelController setViewControllers:@[listaImoveisController]];
        
        self.tabBarController.selectedIndex = 2;
    }

    
}



#pragma mark - Tag Configuration
- (void)configureTags:(Imovel *)imovel cell:(DetalhesImovelCell *)cell
{
    // reset tags
    cell.tag1.text = nil;
    cell.tag2.text = nil;
    cell.tag3.text = nil;
    cell.tag4.text = nil;
    cell.tag5.text = nil;
    cell.tag6.text = nil;
    
    UIColor *color = [UIColor colorWithRed:252.0/255 green:35.0/255 blue:88.0/255 alpha:1];;
    cell.tag1.backgroundColor = color;
    cell.tag2.backgroundColor = color;
    cell.tag3.backgroundColor = color;
    cell.tag4.backgroundColor = color;
    cell.tag5.backgroundColor = color;
    cell.tag6.backgroundColor = color;
    
    cell.tag1.text = imovel.tipo;
    cell.tag2.text = imovel.endereco.bairro.nome;
    cell.tag3.text = [self formatQuartos:[imovel.quartos intValue]];
    cell.tag4.text =  [self.formatter stringFromNumber:imovel.valor];
    
    if(imovel.areaUtil != nil) {
        cell.tag5.text =  [NSString stringWithFormat:@"%@ m²",imovel.areaUtil];
    }
    
    cell.tag6.text =  imovel.oferta;
    
    
}

#pragma mark - Change Imoveis
- (void)changeImoveis:(Evento *)evento
{
    self.selectedEvento = evento;
    
    self.fetchedResultsController = nil;
    [self.fetchedResultsController performFetch:nil];
    [self.tableView reloadData];
}


@end
