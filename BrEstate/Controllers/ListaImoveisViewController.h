//
//  ListaImoveisViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/14/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetalhesContatoViewController.h"
#import "DetalhesImovelViewController.h"

@interface ListaImoveisViewController : BaseViewController

@property (nonatomic, strong) DetalhesContatoViewController *detalhesClienteViewController;
@property (nonatomic, strong) DetalhesImovelViewController *detalhesImovelViewController;

@property (nonatomic, strong) Imovel *selectedImovel;
@property (nonatomic, strong) Imovel *selectedImovelNavigationIphone;

@property (nonatomic, strong) NSPredicate *selectedPredicate;


@end
