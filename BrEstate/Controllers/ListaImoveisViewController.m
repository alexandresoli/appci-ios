//
//  ListaImoveisViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/14/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "ListaImoveisViewController.h"
#import "FiltroTipoViewController.h"
#import "FiltroBairroViewController.h"
#import "FiltroQuartosViewController.h"
#import "FiltroValorViewController.h"
#import "EditarImovelViewController.h"
#import "EditarNegociacaoViewController.h"
#import "Imovel+Utils.h"
#import "ImovelCell.h"
#import "Endereco.h"
#import "Bairro.h"
#import "Cidade.h"
#import "FotoImovel+InsertDeleteImagem.h"
#import "TipoImovel.h"
#import "TipoOferta.h"
#import "MapaViewController.h"
#import "FotosViewController.h"
#import "MailViewController.h"
#import "ImageUtils.h"
#import "Utils.h"
#import "Alerta.h"
#import "MapaImoveisViewController.h"
#import "Deduplicator.h"
#import "IAPHelper.h"
#import "DummyNavigationViewController.h"
#import "Imovel+Firebase.h"
#import "TSMessage.h"


@interface ListaImoveisViewController ()  <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate,UISearchBarDelegate,SWTableViewCellDelegate,UIActionSheetDelegate,UIAlertViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UISearchBar *imovelSearchBar;
@property (nonatomic, weak) IBOutlet UISegmentedControl *segment;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) MailViewController *mailController;
@property (nonatomic, strong) NSCache *fotosCache;
@property (nonatomic) long selectedIndex;
@property (nonatomic) NSIndexPath *selectedIndexPath;
@property (nonatomic, strong) NSTimer *timerRefresh;

@end

@implementation ListaImoveisViewController


#pragma mark - Queue

dispatch_queue_t queueLoadFoto;
dispatch_queue_t queueLoadSwipeButtons;


#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // iOS 7 Bug - http://stackoverflow.com/questions/19066845/uitableview-section-index-overlapping-row-delete-button
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    
    // Se for iPhone configura o BarButton de Alertas.
    if (![[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self configureAlertWithBarButton];
    }
    
    // add model changes notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshImovel:) name:@"Imovel" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshImovelFechado:) name:@"ImovelFechado" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshImovel:) name:@"FotoImovel" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshAlerta:) name:@"Alerta" object:nil];
    
    queueLoadFoto = dispatch_queue_create("ListaImoveisViewController.LoadFoto", NULL);
    queueLoadSwipeButtons = dispatch_queue_create("ListaImoveisViewController.LoadSwipeButtons", NULL);
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // came from contatos screen
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        if (self.selectedImovel != nil) {
            [self selectRow];
        }
        
        if (self.selectedPredicate != nil) {
            
            self.fetchedResultsController = nil;
            [self.fetchedResultsController performFetch:nil];
            
            [self.tableView reloadData];
            
        }
        
        
    } else {
        if (self.selectedImovelNavigationIphone != nil) {
            [self selectRowIphone];
            self.selectedImovelNavigationIphone = nil;
        }
    }
    
    [self checkRefresh];
    
    self.timerRefresh = [NSTimer scheduledTimerWithTimeInterval:30.0
                                                         target:self
                                                       selector:@selector(checkRefreshWithNotification)
                                                       userInfo:nil
                                                        repeats:YES];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self.timerRefresh invalidate];
    [TSMessage dismissActiveNotification];
    
    [Flurry endTimedEvent:@"Lista Imoveis" withParameters:nil];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [Utils trackWithName:@"Lista Imoveis"];
    [Utils trackFlurryTimedWithName:@"Lista Imoveis"];
}


- (NSCache *)fotosCache
{
    if (_fotosCache) {
        return _fotosCache;
    }
    
    _fotosCache = [[NSCache alloc] init];
    [_fotosCache setCountLimit:15];
    
    return _fotosCache;
}


#pragma mark - Check Refresh

- (void)checkRefresh
{
    NSNumber *refresh = [Utils valueFromDefaults:REFRESH_IMOVEL];
    if ([refresh boolValue]) {
        [TSMessage dismissActiveNotification];
        [self refreshImovel:nil];
    }
}

- (void)checkRefreshWithNotification
{
    NSNumber *refresh = [Utils valueFromDefaults:REFRESH_IMOVEL];
    if ([refresh boolValue]) {
        [self showNotificationRefresh];
    }
}

- (void)showNotificationRefresh
{
    if (![Utils valueFromDefaults:FIREBASE_ALREADY_DOWNLOAD_ALL]) {
        return;
    }
    
    if ([TSMessage queuedMessages].count > 0) {
        return;
    }
    
    [TSMessage showNotificationInViewController:self.parentViewController.parentViewController
                                          title:@"Novas informações disponíveis. Toque aqui para atualizar."
                                       subtitle:nil
                                          image:nil
                                           type:TSMessageNotificationTypeSuccess
                                       duration:TSMessageNotificationDurationEndless
                                       callback:^{
                                           
                                           [Utils trackWithName:@"Atualização Firebase - Lista Imovéis - Clicou na faixa"];
                                           [Utils trackFlurryWithName:@"Atualização Firebase - Lista Imovéis - Clicou na faixa"];
                                           
                                           [TSMessage dismissActiveNotification];
                                           [self refreshImovel:nil];
                                       }
                                    buttonTitle:nil
                                 buttonCallback:nil
                                     atPosition:TSMessageNotificationPositionTop
                           canBeDismissedByUser:YES];
}


#pragma mark - Load Imovel

- (void)loadImovel:(Imovel *)imovel
{
    self.detalhesImovelViewController.superNavigationController = self.navigationController;
    [self.detalhesImovelViewController loadImovel:imovel];
}


#pragma mark - Select Specific Row
- (void)selectRow
{
    NSIndexPath *indexPath = [self.fetchedResultsController indexPathForObject:self.selectedImovel];
    
    [self loadImovel:self.selectedImovel];
    
    // scroll to cell position
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)selectRowIphone
{
    
    if (self.selectedImovelNavigationIphone != nil) {
        self.selectedImovel = self.selectedImovelNavigationIphone;
    }
    
    NSIndexPath *indexPath = [self.fetchedResultsController indexPathForObject:self.selectedImovel];
    
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    // Utilizando o performSelector porque executando a segue direto não está funcionando.
    [self performSelector:@selector(showDetalhesImovelOnIphone) withObject:nil afterDelay:0.1];
    
}

- (void)showDetalhesImovelOnIphone
{
    [self performSegueWithIdentifier:@"DetalhesImovelSegue" sender:nil];
}

#pragma mark - Notification Center

- (void)refreshImovelFechado:(NSNotification *)note
{
    [self refreshImovel:note];
    [self loadImovel:nil];
}


- (void)refreshImovel:(NSNotification *)note
{
    [Utils addObjectToDefaults:[NSNumber numberWithBool:NO] withKey:REFRESH_IMOVEL];
    
    self.fetchedResultsController = nil;
    self.selectedPredicate = nil;
    //[self loadImovel:nil];
    [self.fetchedResultsController performFetch:nil];
    
    [self.fotosCache removeAllObjects];
    [self.tableView reloadData:YES];
}

- (void)refreshAlerta:(NSNotification *)note
{
    
    // Se houve mudança de alertas atualiza o BarButton no iPhone.
    if (![[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self configureAlertWithBarButton];
    }
}


#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSPredicate *predicate;
    if (self.selectedPredicate == nil) {
        
        predicate = [NSPredicate predicateWithFormat:@"((SUBQUERY(negociacao, $n, $n.status = %@).@count == 0) OR (ANY negociacao = nil))", NEGOCIACAO_FECHADA];
        
    } else {
        predicate = self.selectedPredicate;
    }
    
    _fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Imovel"
                                                                          andKey1:@"endereco.bairro.nome"
                                                                          andKey2:@"codigo"
                                                                  withSectionName:@"sectionIdentifier"
                                                                    withPredicate:predicate
                                                                        withOrder:YES
                                                                        withLimit:0
                                                                        inContext:self.moc];
    self.selectedPredicate = nil;
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ImovelCell";
    ImovelCell *cell = (ImovelCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;;
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];
    
    // SWTableViewCell
//    [cell setCellHeight:cell.frame.size.height];
//    cell.containingTableView = tableView;
    cell.delegate = self;
    
    Imovel *imovel = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [self configureTags:imovel cell:cell];
    [self configureCell:cell atIndexPath:[indexPath copy] forImovel:imovel];
    
    return cell;
}

- (void)configureCell:(ImovelCell *)cell atIndexPath:(NSIndexPath *)indexPath forImovel:(Imovel *)imovel
{
    NSArray *fotos = [imovel.fotos allObjects];
    
    if (fotos.count == 0) {
        cell.photoView.image = [UIImage imageNamed:@"nao-disponivel2"];
        
    } else {
        cell.photoView.image = [self.fotosCache objectForKey:indexPath];
        
        if (!cell.photoView.image) {
            __block FotoImovel *fotoImovel = [[fotos filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"capa = YES"]] firstObject];
            
            dispatch_async(queueLoadFoto, ^{
                
                if ([self.tableView.indexPathsForVisibleRows containsObject:indexPath]) {
                    
                    if (fotoImovel == nil) { // sem capa
                        fotoImovel = [fotos firstObject];
                    }
                    
                    UIImage *image = [fotoImovel getImageForFotoImovelWithMaxPixelSize:500];
                    
                    if (image) {
                        [self.fotosCache setObject:image forKey:indexPath];
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        ImovelCell *updateCell = (ImovelCell *)[self.tableView cellForRowAtIndexPath:indexPath];
                        [UIView transitionWithView:updateCell.photoView
                                          duration:0.2f
                                           options:UIViewAnimationOptionTransitionCrossDissolve
                                        animations:^{
                                            
                                            updateCell.photoView.image = image;
                                            
                                        } completion:NULL];
                    });
                }
                
            });
        }
    }
    
    BOOL isMapEnabled = YES;
    if ((imovel.endereco.logradouro == nil ||
         imovel.endereco.logradouro == (id)[NSNull null] ||
         imovel.endereco.logradouro.length == 0) && imovel.endereco.latitude.doubleValue == 0) {
        isMapEnabled = NO;
    }
    
    dispatch_async(queueLoadSwipeButtons, ^{
        
        if ([self.tableView.indexPathsForVisibleRows containsObject:indexPath]) {
            
            NSMutableArray *leftUtilityButtons = [NSMutableArray new];
            NSMutableArray *rightUtilityButtons = [NSMutableArray new];
            
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                
                // left swipe
                [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"portfolio-fotos"]];
                
                if (isMapEnabled) {
                    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"portfolio-mapa"]];
                    
                } else {
                    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"mapa-disable"]];
                }
                
                [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"portfolio-e-mail"]];
                [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"portfolio-editar"]];
                
                // right swipe
                [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"portfolio-negociacao"]];
                [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"swipe-apagar"]];
                
            } else {
                
                // left swipe
                [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"portfolio-fotos"] scaledToSize:CGSizeMake(47, 60)]];
                
                if (isMapEnabled) {
                    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"portfolio-mapa"] scaledToSize:CGSizeMake(47, 60)]];
                    
                } else {
                    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"mapa-disable"] scaledToSize:CGSizeMake(47, 60)]];
                }
                
                [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"portfolio-e-mail"] scaledToSize:CGSizeMake(47, 60)]];
                [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"portfolio-editar"] scaledToSize:CGSizeMake(47, 60)]];
                
                // right swipe
                [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"portfolio-negociacao"] scaledToSize:CGSizeMake(47, 60)]];
                [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"swipe-apagar"] scaledToSize:CGSizeMake(47, 60)]];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                ImovelCell *updateCell = (ImovelCell *)[self.tableView cellForRowAtIndexPath:indexPath];
                updateCell.leftUtilityButtons = leftUtilityButtons;
                updateCell.rightUtilityButtons = rightUtilityButtons;
            });
        }
    });
}


- (void)configureTags:(Imovel *)imovel cell:(ImovelCell *)cell
{
    // reset tags
    cell.tag1.text = nil;
    cell.tag2.text = nil;
    cell.tag3.text = nil;
    cell.tag4.text = nil;
    cell.tag5.text = nil;
    cell.tag6.text = nil;
    
    cell.tag1.text = imovel.tipo;
    cell.tag2.text = imovel.endereco.bairro.nome;
    cell.tag3.text = [self formatQuartos:[imovel.quartos intValue]];
    cell.tag4.text =  [self.formatter stringFromNumber:imovel.valor];
    
    if(imovel.areaUtil != nil && imovel.areaUtil.intValue > 0) {
        cell.tag5.text =  [NSString stringWithFormat:@"%@ m²",imovel.areaUtil];
    } else if(imovel.areaTotal != nil && imovel.areaTotal.intValue > 0) {
        cell.tag5.text =  [NSString stringWithFormat:@"%@ m²",imovel.areaTotal];
    } else {
        cell.tag5.text = @"0 m²";
    }
    
    cell.tag6.text =  imovel.oferta;
    
    
}


#pragma mark - SWTableViewCellDelegate
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index
{
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    self.selectedImovel = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    switch (index) {
        case 0:
            
            [Utils trackWithName:@"Clicou em foto - Imovel"];
            [Utils trackFlurryWithName:@"Clicou em foto - Imovel"];

            
            [self performSegueWithIdentifier:@"FotosSegue" sender:nil];
            break;
        case 1:
            
            [Utils trackWithName:@"Clicou em Mapa - Imovel"];
            [Utils trackFlurryWithName:@"Clicou em Mapa - Imovel"];

            
            if ((_selectedImovel.endereco.logradouro == nil ||
                 _selectedImovel.endereco.logradouro == (id)[NSNull null] ||
                 _selectedImovel.endereco.logradouro.length == 0) && _selectedImovel.endereco.latitude.doubleValue == 0) {
                
                [Utils showMessage:@"Para acessar o mapa, você deve preencher o endereço do imóvel!"];
                
            } else {
                [self performSegueWithIdentifier:@"MapaSegue" sender:nil];                
            }
            
            break;
        case 2:
            
            [Utils trackWithName:@"Clicou em Email - Imovel"];
            [Utils trackFlurryWithName:@"Clicou em Email - Imovel"];
            
            [self sendMail:indexPath];
            break;
        case 3:
            
            [Utils trackWithName:@"Clicou em Editar - Imovel"];
            [Utils trackFlurryWithName:@"Clicou em Editar - Imovel"];

            
            [self performSegueWithIdentifier:@"EditarImovelSegue" sender:self];
            break;
        default:
            break;
    }
    
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    self.selectedImovel = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    switch (index) {
        case 0:
        {
            // role check
//            NSString *role = [Utils valueFromDefaults:CURRENT_ROLE];
//            if ([role isEqualToString:@"Atendimento"]) {
//                [Utils showMessage:@"Permissão Negada!"];
//                return;
//            }
            
            
            
            //if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [Utils trackWithName:@"Clicou em Iniciar Negociacao - Imovel"];
            [Utils trackFlurryWithName:@"Clicou em Iniciar Negociacao - Imovel"];

            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self = %@ AND ANY negociacao.status = %@", self.selectedImovel, NEGOCIACAO_FECHADA];
            long closedCount = [Imovel MR_countOfEntitiesWithPredicate:predicate inContext:self.moc];
            
            
            // properties with closed negotiation cannot start a new negotiation
            if (closedCount == 0) {
                [self performSegueWithIdentifier:@"IniciarNegociacaoSegue" sender:nil];
                
            } else {
                [Utils showMessage:@"Este imóvel possui uma negociação fechada. Não é possível iniciar uma nova negociação."];
            }
            
            break;
            
        }
        case 1:
        {
            // Remover
            [Utils trackWithName:@"Clicou em Remover - Imovel"];
            [Utils trackFlurryWithName:@"Clicou em Remover - Imovel"];

            
            self.selectedImovel = [self.fetchedResultsController objectAtIndexPath:indexPath];
            
            UIActionSheet *sheet =[[UIActionSheet alloc] initWithTitle:@"Remover imóvel?" delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:@"Remover" otherButtonTitles:nil, nil];
            [sheet showInView:cell];
            
            break;
        }
        case 2:
            
            // ARQUIVAR
            [Utils trackWithName:@"Clicou em Arquivar - Imovel"];
            [Utils trackFlurryWithName:@"Clicou em Arquivar - Imovel"];

            
            break;
        default:
            break;
    }
}

#pragma mark - Send Email
- (void)sendMail:(NSIndexPath *)indexPath
{
    _selectedIndexPath = indexPath;
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Aviso" message:@"Escolha a qualidade das fotos" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Alta",@"Baixa", nil];
    [alertView show];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // parse sdk
//    [ParseUtils queryAllNegociacoesFromParse];
    
    self.selectedImovel = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self loadImovel:self.selectedImovel];
        
    } else { // Se o device for iPhone é necessário fazer o performSegue.
        [self performSegueWithIdentifier:@"DetalhesImovelSegue" sender:self];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *labelView = [[UILabel alloc] init];
    labelView.textAlignment = NSTextAlignmentLeft;
    //labelView.backgroundColor = [UIColor colorWithRed:0.0/255 green:122.0/255 blue:255.0/255 alpha:1.0];
    //labelView.textColor = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0];
    labelView.backgroundColor = [UIColor colorWithRed:0.851 green:0.851 blue:0.851 alpha:1];
    labelView.textColor = [UIColor blackColor];
    labelView.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
    
    
    if ([[self.fetchedResultsController sections] count] > 0) {
        NSString *text = [[[self.fetchedResultsController sections] objectAtIndex:section] name];
        
        if (text.length == 0) {
            text = @"Bairro Não Cadastrado";
        }
        
        labelView.text = [NSString stringWithFormat:@"   %@",text];
    }
    
    
    return labelView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    
    return [sectionInfo name];
}


#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        for (FotoImovel *fotoImovel in self.selectedImovel.fotos) {
            // amazon s3
            [fotoImovel deleteImageFromS3];
            [fotoImovel deleteFromDisk];
        }
        
        
        // Parse SDK
//        [ParseUtils deleteEntity:_selectedImovel withName:@"Imovel" usingMoc:self.moc];
        
        // Firebase
        [self.selectedImovel deleteFirebase];
        
        [_selectedImovel MR_deleteInContext:self.moc];
        
        
        [self.moc MR_saveToPersistentStoreAndWait];
        
        NSError *error = nil;
        
        self.selectedImovel = nil;
        [self.fetchedResultsController performFetch:&error];
        
        [self.fotosCache removeAllObjects];
        [self.tableView reloadData:YES];
    }
}


#pragma mark - Segue

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"AdicionarImovelSegue"]) {
        
        NSUInteger count = [Imovel MR_countOfEntitiesWithContext:self.moc];
        
        if (count >= 50) {
            BOOL isTop = [self.iapHelper checkPlanTopFromController:self
                                                        withMessage:PLAN_TOP_MESSAGE_IMOVEIS_LIMIT];
            if (!isTop) {
                return NO;
            }
        }
    }
    
    return YES;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"EditarImovelSegue"]) {
        
        UINavigationController *navController = segue.destinationViewController;
        EditarImovelViewController *controller = [[navController viewControllers] firstObject];
        controller.selectedImovel = self.selectedImovel;
        
    } else if ([segue.identifier isEqualToString:@"AdicionarImovelSegue"]) {
        
        DummyNavigationViewController *navController = segue.destinationViewController;
        navController.sourceViewController = self;
        
    } else if ([segue.identifier isEqualToString:@"MapaSegue"]) {
        
        MapaViewController *mapaController = segue.destinationViewController;
        mapaController.selectedImovel = self.selectedImovel;
        
    } else if ([segue.identifier isEqualToString:@"FotosSegue"]) {
        
        FotosViewController *controller = segue.destinationViewController;
        controller.selectedImovel = self.selectedImovel;
        
    } else if ([segue.identifier isEqualToString:@"IniciarNegociacaoSegue"]) {
        
        UINavigationController *navController = segue.destinationViewController;
        EditarNegociacaoViewController *controller = [[navController viewControllers] firstObject];
        controller.selectedImovel = self.selectedImovel;
        
    } else if ([segue.identifier isEqualToString:@"DetalhesImovelSegue"]) { // iPhone
        DetalhesImovelViewController *controller = segue.destinationViewController;
        controller.selectedImovel = self.selectedImovel;
    }
    
}


#pragma mark - Unwind Segue

- (IBAction)unwindFromMapaImoveis:(UIStoryboardSegue *)segue
{
    MapaImoveisViewController *controller = segue.sourceViewController;
    self.selectedImovel = controller.selectedImovel;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self selectRow];
    } else {
        [self selectRowIphone];
    }
}

- (IBAction)unwindFromFiltroTipoImovel:(UIStoryboardSegue *)segue
{
    FiltroTipoViewController *controller = segue.sourceViewController;
    
    if (controller.selectedTipoImovel != nil) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"tipo = %@",controller.selectedTipoImovel];
        self.fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Imovel"
                                                                              withSortKey:@"endereco.bairro.nome"
                                                                          withSectionName:@"sectionIdentifier"
                                                                            withPredicate:predicate
                                                                                withOrder:YES
                                                                                withLimit:0
                                                                                inContext:self.moc];
        
        [self.fotosCache removeAllObjects];
        [self.tableView reloadData];
    }
}

- (IBAction)unwindFromFiltroBairro:(UIStoryboardSegue *)segue
{
    FiltroBairroViewController *controller = segue.sourceViewController;
    
    if (controller.selectedBairro != nil) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"endereco.bairro = %@",controller.selectedBairro];
        self.fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Imovel"
                                                                              withSortKey:@"endereco.bairro.nome"
                                                                          withSectionName:@"sectionIdentifier"
                                                                            withPredicate:predicate
                                                                                withOrder:YES
                                                                                withLimit:0
                                                                                inContext:self.moc];
        //[CoreDataUtils fetchedResultsControllerWithEntity:@"Imovel" andKey:@"endereco.bairro.nome" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
        
        [self.fotosCache removeAllObjects];
        [self.tableView reloadData];
    }
}


- (IBAction)unwindFromFiltroQuartos:(UIStoryboardSegue *)segue
{
    FiltroQuartosViewController *controller = segue.sourceViewController;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"quartos >= %d",controller.minimoQuartos];
    self.fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Imovel"
                                                                          withSortKey:@"endereco.bairro.nome"
                                                                      withSectionName:@"sectionIdentifier"
                                                                        withPredicate:predicate
                                                                            withOrder:YES
                                                                            withLimit:0
                                                                            inContext:self.moc];
    //[CoreDataUtils fetchedResultsControllerWithEntity:@"Imovel" andKey:@"endereco.bairro.nome" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
    
    [self.fotosCache removeAllObjects];
    [self.tableView reloadData];
    
}


- (IBAction)unwindFromFiltroValor:(UIStoryboardSegue *)segue
{
    FiltroValorViewController *controller = segue.sourceViewController;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"valor >= %f",controller.valor];
    self.fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Imovel"
                                                                          withSortKey:@"endereco.bairro.nome"
                                                                      withSectionName:@"sectionIdentifier"
                                                                        withPredicate:predicate
                                                                            withOrder:YES
                                                                            withLimit:0
                                                                            inContext:self.moc];
    //[CoreDataUtils fetchedResultsControllerWithEntity:@"Imovel" andKey:@"endereco.bairro.nome" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
    
    [self.fotosCache removeAllObjects];
    [self.tableView reloadData];
    
    
//    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)unwindFromAdicionarImovel:(UIStoryboardSegue *)sender
{

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Novo Imóvel"
                                                    message:@"Deseja incluir agora as fotos do imóvel?"
                                                   delegate:self cancelButtonTitle:@"Não"
                                          otherButtonTitles:@"Sim", nil];
    [alert show];
}


#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // novo imóvel
    if ([alertView.title isEqualToString:@"Novo Imóvel"]) {
        if (buttonIndex == 1) {
            [self performSegueWithIdentifier:@"FotosSegue" sender:nil];
        }
        return;
    }
    
    // qualidade da foto
    Imovel *imovel = [self.fetchedResultsController objectAtIndexPath:_selectedIndexPath];
    _mailController = [[MailViewController alloc] init];
    
    switch (buttonIndex) {
        case 0:
            [_mailController sendPorfolio:imovel highQuality:YES forSender:self];
            break;
        case 1:
            [_mailController sendPorfolio:imovel highQuality:NO forSender:self];
            break;
        default:
            break;
    }
    
    _selectedIndexPath = nil;
    
    
}


#pragma mark - UISearchBarDelegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    
    [Utils trackWithName:@"Iniciou busca - Imovel"];
    [Utils trackFlurryWithName:@"Iniciou busca - Imovel"];

    
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.detalhesClienteViewController changeCliente:nil];
    
    searchBar.showsCancelButton = YES;
    
    return YES;
}

// First use the Searchbar delegate methods
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self filterContentForSearchText:searchText];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self filterContentForSearchText:searchBar.text];
    searchBar.showsCancelButton = NO;
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    searchBar.text = nil;
    searchBar.showsCancelButton = NO;
    [self filterContentForSearchText:nil];
    [searchBar resignFirstResponder];
    NSLog(@"");
}

// The method to change the predicate of the FRC
- (void)filterContentForSearchText:(NSString*)searchText
{
    NSString *query = searchText;
    
    NSPredicate *predicate = nil;
    
    if (query && query.length) {
        
        predicate = [NSPredicate predicateWithFormat:@"(descricao contains[c] %@ OR identificacao contains[c] %@ OR endereco.cep contains[c] %@ OR endereco.logradouro contains[c] %@ OR endereco.complemento contains[c] %@ OR endereco.estado contains[c] %@ OR endereco.cidade contains[c] %@ OR endereco.bairro.nome contains[c] %@ OR oferta contains[c] %@)", query, query,query,query,query,query,query,query,query];
        
        [self.fetchedResultsController.fetchRequest setPredicate:predicate];
        [self.fetchedResultsController.fetchRequest setFetchLimit:100];
        
    } else {
        
        self.fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Imovel"
                                                                              withSortKey:@"endereco.bairro.nome"
                                                                          withSectionName:@"sectionIdentifier"
                                                                            withPredicate:predicate
                                                                                withOrder:YES
                                                                                withLimit:0
                                                                                inContext:self.moc];
        //[CoreDataUtils fetchedResultsControllerWithEntity:@"Imovel" andKey:@"endereco.bairro.nome" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
        
    }
    
    [self.fetchedResultsController performFetch:nil];
    
    [self.fotosCache removeAllObjects];
    [self.tableView reloadData];
}


#pragma mark - Filtro
- (IBAction)clearAllFitlers:(id)sender
{
    [self filterContentForSearchText:nil];
}

- (IBAction)changeSegment:(UISegmentedControl *)segment
{
    
    switch (segment.selectedSegmentIndex) {
        case 1:
            self.selectedIndex = segment.selectedSegmentIndex;
            self.selectedPredicate = [NSPredicate predicateWithFormat:@"(ANY negociacao.status = %@)", NEGOCIACAO_FECHADA];
            break;
        case 2:
        {
            if ([self.iapHelper checkPlanTopFromController:self withMessage:PLAN_TOP_MESSAGE_IMOVEIS_MAPS]) {
                [self performSegueWithIdentifier:@"MapaImoveisSegue" sender:nil];
            }
            
            segment.selectedSegmentIndex = self.selectedIndex;
            break;
        }
        default:
            self.selectedIndex = segment.selectedSegmentIndex;
            break;
    }
    
    [self loadImovel:nil];
    self.fetchedResultsController = nil;
    [self.fetchedResultsController performFetch:nil];
    
    [self.fotosCache removeAllObjects];
    [self.tableView reloadData:YES];
    
    
}

@end
