//
//  ListaNotasViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 2/6/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "ListaNotasViewController.h"
#import "EditarNotasViewController.h"
#import "NotaCell.h"
#import "Nota.h"
#import "DateUtils.h"
#import "ImageUtils.h"
#import "Deduplicator.h"
#import "Alerta.h"
#import "Nota+Firebase.h"
#import "TSMessage.h"

@interface ListaNotasViewController () <SWTableViewCellDelegate, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate,UISearchBarDelegate,UIActionSheetDelegate>


@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;
@property (nonatomic, strong) Nota *selectedNota;
@property (nonatomic, strong) NSTimer *timerRefresh;

@end

@implementation ListaNotasViewController


#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Se for iPhone configura o BarButton de Alertas.
    if (![[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self configureAlertWithBarButton];
    }
    
    // add model changes notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshNota:) name:@"Nota" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshAlerta:) name:@"Alerta" object:nil];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self checkRefresh];
    
    self.timerRefresh = [NSTimer scheduledTimerWithTimeInterval:30.0
                                                         target:self
                                                       selector:@selector(checkRefreshWithNotification)
                                                       userInfo:nil
                                                        repeats:YES];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Lista Notas"];
    [Utils trackFlurryTimedWithName:@"Lista Notas"];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self.timerRefresh invalidate];
    [TSMessage dismissActiveNotification];
    
    [Flurry endTimedEvent:@"Lista Notas" withParameters:nil];
}


#pragma mark - Check Refresh

- (void)checkRefresh
{
    NSNumber *refresh = [Utils valueFromDefaults:REFRESH_NOTA];
    if ([refresh boolValue]) {
        [TSMessage dismissActiveNotification];
        [self refreshNota:nil];
    }
}


- (void)checkRefreshWithNotification
{
    NSNumber *refresh = [Utils valueFromDefaults:REFRESH_NOTA];
    if ([refresh boolValue]) {
        [self showNotificationRefresh];
    }
}


- (void)showNotificationRefresh
{
    if (![Utils valueFromDefaults:FIREBASE_ALREADY_DOWNLOAD_ALL]) {
        return;
    }
    
    if ([TSMessage queuedMessages].count > 0) {
        return;
    }
    
    [TSMessage showNotificationInViewController:self.parentViewController.parentViewController
                                          title:@"Novas informações disponíveis. Toque aqui para atualizar."
                                       subtitle:nil
                                          image:nil
                                           type:TSMessageNotificationTypeSuccess
                                       duration:TSMessageNotificationDurationEndless
                                       callback:^{
                                           
                                           [Utils trackWithName:@"Atualização Firebase - Lista Notas - Clicou na faixa"];
                                           [Utils trackFlurryWithName:@"Atualização Firebase - Lista Notas - Clicou na faixa"];
                                           
                                           [TSMessage dismissActiveNotification];
                                           [self refreshNota:nil];
                                       }
                                    buttonTitle:nil
                                 buttonCallback:nil
                                     atPosition:TSMessageNotificationPositionTop
                           canBeDismissedByUser:YES];
}


#pragma mark - Notification Center
- (void)refreshNota:(NSNotification *)note
{
    [Utils addObjectToDefaults:[NSNumber numberWithBool:NO] withKey:REFRESH_NOTA];
    
    self.selectedIndexPath = nil;
    self.selectedNota = nil;
    
    [self.fetchedResultsController performFetch:nil];
    [self.tableView reloadData:YES];
}

- (void)refreshAlerta:(NSNotification *)note
{
    
    // Se houve mudança de alertas atualiza o BarButton no iPhone.
    if (![[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self configureAlertWithBarButton];
    }
}


#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (_fetchedResultsController != nil)
    {
        return _fetchedResultsController;
    }
    
    _fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Nota" withSortKey:@"timeStamp" withSectionName:@"sectionIdentifier" withPredicate:nil withOrder:NO withLimit:0 inContext:self.moc];
    
    _fetchedResultsController.delegate = self;

	return _fetchedResultsController;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NotaCell";
    NotaCell *cell = (NotaCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];
    
    //    UIView *bgColorView = [[UIView alloc] init];
    //    bgColorView.backgroundColor = [UIColor colorWithRed:108.0/255 green:233.0/255 blue:162.0/255 alpha:1];
    //    cell.selectedBackgroundView = bgColorView;
    
    // SWTableViewCell
//    [cell setCellHeight:cell.frame.size.height];
//    cell.containingTableView = tableView;
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(NotaCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Nota *nota = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // Tratamento para marcar a cell selecionada.
    if ([self.selectedIndexPath isEqual:indexPath]) {
        cell.selecionada.hidden = NO;
    } else {
        cell.selecionada.hidden = YES;
    }
    
    cell.descricao.text = nota.descricao;
    cell.dia.text = [DateUtils stringFromDate:nota.timeStamp withFormat:@"dd"];
    cell.hora.text = [DateUtils stringFromDate:nota.timeStamp withFormat:@"HH:mm"];
    
    if (nota.arquivo != nil) {
        cell.anexo.hidden = NO;
    } else {
        cell.anexo.hidden = YES;
    }
    
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    
    // right swipe
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[UIImage imageNamed:@"swipe-apagar"]];
    } else {
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"swipe-apagar"] scaledToSize:CGSizeMake(47, 60)]];
    }
    
    cell.leftUtilityButtons = leftUtilityButtons;
    cell.rightUtilityButtons = rightUtilityButtons;
    
    cell.delegate = self;
}


#pragma mark - SWTableViewCellDelegate
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    self.selectedNota = [_fetchedResultsController objectAtIndexPath:indexPath];
    
    UIActionSheet *sheet =[[UIActionSheet alloc] initWithTitle:@"Remover Nota?" delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:@"Remover" otherButtonTitles:nil, nil];
    [sheet showInView:cell];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Tratamento para marcar a cell selecionada e desmarcar a anterior.
    NotaCell *cellSelected = (NotaCell *) [self.tableView cellForRowAtIndexPath:indexPath];
    NotaCell *cellOld = (NotaCell *) [self.tableView cellForRowAtIndexPath:self.selectedIndexPath];
    
    cellSelected.selecionada.hidden = NO;
    
    if (cellSelected != cellOld) {
        cellOld.selecionada.hidden = YES;
    }
    
    self.selectedIndexPath = indexPath;
    
    self.selectedNota = [_fetchedResultsController objectAtIndexPath:indexPath];
    [_detalhesNotasViewController showNota:self.selectedNota];
    
    // Se o device for iPhone é necessário fazer o performSegue.
    if (![[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self performSegueWithIdentifier:@"DetalhesNotaSegue" sender:self];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

- (NSString *)formatTitle:(NSInteger)section
{
	id <NSFetchedResultsSectionInfo> theSection = [[self.fetchedResultsController sections] objectAtIndex:section];
    
    /*
     Section information derives from an event's sectionIdentifier, which is a string representing the number (year * 1000) + month.
     To display the section title, convert the year and month components to a string representation.
     */
    static NSDateFormatter *formatter = nil;
    
    if (!formatter)
    {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setCalendar:[NSCalendar currentCalendar]];
        
        NSString *formatTemplate = [NSDateFormatter dateFormatFromTemplate:@"MMMM YYYY" options:0 locale:[NSLocale currentLocale]];
        [formatter setDateFormat:formatTemplate];
    }
    
    NSInteger numericSection = [[theSection name] integerValue];
	NSInteger year = numericSection / 1000;
	NSInteger month = numericSection - (year * 1000);
    
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    dateComponents.year = year;
    dateComponents.month = month;
    NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
    
    
	NSString *titleString = [formatter stringFromDate:date];
    NSString *firstLetter = [[titleString substringToIndex:1] capitalizedString];
    titleString = [titleString stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstLetter];
    
    
	return [NSString stringWithFormat:@"   %@",titleString];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *labelView = [[UILabel alloc] init];
    labelView.textAlignment = NSTextAlignmentLeft;
    //labelView.backgroundColor = [UIColor colorWithRed:0.0/255 green:122.0/255 blue:255.0/255 alpha:1.0];
    //labelView.textColor = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0];
    labelView.backgroundColor = [UIColor colorWithRed:0.851 green:0.851 blue:0.851 alpha:1];
    labelView.textColor = [UIColor blackColor];
    
    labelView.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
    labelView.text = [self formatTitle:section];
    
    return labelView;
}



#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    searchBar.showsCancelButton = YES;
    
    return YES;
}


// First use the Searchbar delegate methods
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self filterContentForSearchText:searchText];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self filterContentForSearchText:searchBar.text];
    searchBar.showsCancelButton = NO;
    [searchBar resignFirstResponder];
}


- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    searchBar.text = nil;
    searchBar.showsCancelButton = NO;
    [self filterContentForSearchText:nil];
    [searchBar resignFirstResponder];
}


// The method to change the predicate of the FRC
- (void)filterContentForSearchText:(NSString*)searchText
{
    NSString *query = searchText;
    
    if (query && query.length) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(descricao contains[c] %@) ", query];
        
        [self.fetchedResultsController.fetchRequest setPredicate:predicate];
        [self.fetchedResultsController.fetchRequest setFetchLimit:100]; // Optional, but with large datasets - this helps speed lots
        
    } else {
        
        self.fetchedResultsController = nil;
        
    }
    
    [self.fetchedResultsController performFetch:nil];
    
    [self.tableView reloadData];
}


#pragma mark - Add Nota

- (IBAction)add:(id)sender
{
    [Utils trackWithName:@"Adicionar - Notas"];
    [Utils trackFlurryWithName:@"Adicionar - Notas"];

    Nota *nota = [Nota MR_createInContext:self.moc];
    nota.timeStamp = [NSDate date];
    nota.codigo = [nota getUniqueCode]; //[NSNumber numberWithLong:codigo];
    nota.descricao = @"Nova Nota";
    nota.version = [NSDate date];
    
    // Firebase
    [nota saveFirebase];
    
    [self saveMOC];
    
    self.selectedNota = nota;
    
    [_detalhesNotasViewController showNota:nota];
    
    // Se for iPhone, após a inclusão da nota navega para a tela de edição da nota.
    if (![[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self performSegueWithIdentifier:@"DetalhesNotaSegue" sender:self];
    }
    
    // Parse SDK
//    [_selectedNota saveParse:self.moc];
    
    [Utils postEntitytNotification:@"Nota"];
    
}


#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        
        // Firebase
        [_selectedNota deleteFirebase];
        
        [_selectedNota MR_deleteInContext:self.moc];
        
        [self.moc MR_saveToPersistentStoreAndWait];
        
        self.selectedIndexPath = nil;
        
        [self.fetchedResultsController performFetch:nil];
        
        [_detalhesNotasViewController showNota:nil];
        
        [self.tableView reloadData:YES];
        
    }
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"DetalhesNotaSegue"]) {
        EditarNotasViewController *controller = (EditarNotasViewController *)segue.destinationViewController;
        controller.selectNota = self.selectedNota;
    }
}


@end
