//
//  ListaPerfisContatoViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 12/9/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "BaseTableViewController.h"

@class PerfilCliente,SubtipoCliente;

@interface ListaPerfilViewController : BaseTableViewController

@property (nonatomic, strong) PerfilCliente *selectedPerfil;
@property (nonatomic, strong) SubtipoCliente *selectedTipo;

@end
