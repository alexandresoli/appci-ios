//
//  ListaPerfisContatoViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 12/9/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "ListaPerfilViewController.h"
#import "PerfilCliente.h"
#import "CustomCell.h"
#import "Cliente.h"
#import "TipoCliente.h"
#import "SubtipoCliente.h"
#import "TipoPerfilCliente.h"
#import "EditarPerfilViewController.h"
#import "ImageUtils.h"
#import "PerfilCliente+Firebase.h"

@interface ListaPerfilViewController () <NSFetchedResultsControllerDelegate,SWTableViewCellDelegate,UIActionSheetDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end


@implementation ListaPerfilViewController

//@synthesize fetchedResultsController,selectedPerfil;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // add model changes notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDataModelChange:) name:NSManagedObjectContextObjectsDidChangeNotification object:self.moc];
    
    UIBarButtonItem *addButton = self.navigationItem.rightBarButtonItem;
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:addButton, nil]];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Tela Lista Perfis"];
    [Utils trackFlurryWithName:@"Tela Lista Perfis"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Done
- (void)done
{
    [self performSegueWithIdentifier:@"UnwindFromPerfilCliente" sender:self];
}

#pragma mark - Notification Center
- (void)handleDataModelChange:(NSNotification *)note
{
    NSSet *updatedObjects = [[note userInfo] objectForKey:NSUpdatedObjectsKey];
    NSSet *deletedObjects = [[note userInfo] objectForKey:NSDeletedObjectsKey];
    NSSet *insertedObjects = [[note userInfo] objectForKey:NSInsertedObjectsKey];
    
    if ([updatedObjects containsMemberOfClass:[PerfilCliente class]] ||
        [insertedObjects containsMemberOfClass:[PerfilCliente class]] ||
        [deletedObjects containsMemberOfClass:[PerfilCliente class]])
    {

        [self.fetchedResultsController performFetch:nil];
        [self.tableView reloadData:YES];
    }
    
    
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PerfilCell";
    CustomCell *cell = (CustomCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15];
    
    // SWTableViewCell
//    [cell setCellHeight:cell.frame.size.height];
//    cell.containingTableView = tableView;
    
    [self configureCell:cell];
    
    PerfilCliente *perfil = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.textLabel.text = perfil.descricao;
    
    return cell;
}


- (void)configureCell:(CustomCell *)cell
{
    
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        // left swipe
        [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"contato-editar"] scaledToSize:CGSizeMake(54, 70)]];
        
        // right swipe
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"swipe-apagar"] scaledToSize:CGSizeMake(54, 70)]];
        
    } else {
        // left swipe
        [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"contato-editar"] scaledToSize:CGSizeMake(42, 55)]];
        
        // right swipe
        [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor whiteColor] icon:[ImageUtils resize:[UIImage imageNamed:@"swipe-apagar"] scaledToSize:CGSizeMake(42, 55)]];
        
    }
    
    cell.leftUtilityButtons = leftUtilityButtons;
    cell.rightUtilityButtons = rightUtilityButtons;
    cell.delegate = self;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedPerfil = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [self performSegueWithIdentifier:@"UnwindFromPerfilCliente" sender:self];
    
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}




#pragma mark - SWTableViewCellDelegate
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index
{
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    self.selectedPerfil = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    switch (index) {
        case 0:
            
            [self performSegueWithIdentifier:@"EditarPerfilSegue" sender:self];
            
            break;
            
        default:
            break;
    }
    
}


- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    self.selectedPerfil = [self.fetchedResultsController objectAtIndexPath:indexPath];

    UIActionSheet *sheet =[[UIActionSheet alloc] initWithTitle:@"Remover perfil?" delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:@"Remover" otherButtonTitles:nil, nil];
    [sheet showInView:cell];
    
}


#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    _fetchedResultsController.delegate = self;
    
    
    NSNumber *codigo = nil;
    
    if ([self.selectedTipo.descricao isEqualToString:@"Alugar"]) {
        codigo = [NSNumber numberWithInt:1];
        
    } else if ([self.selectedTipo.descricao isEqualToString:@"Comprar"]) {
        codigo = [NSNumber numberWithInt:2];
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"tipoPerfil = %@",codigo];
    
    _fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"PerfilCliente" andKey:@"descricao" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
    
    
    return _fetchedResultsController;
}



#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        
        // Parse Delete
//        [ParseUtils deleteEntity:self.selectedPerfil withName:@"PerfilCliente" usingMoc:self.moc];
        
        // Firebase
        [_selectedPerfil deleteFirebase];
        
        [_selectedPerfil MR_deleteInContext:self.moc];

        [self.moc MR_saveToPersistentStoreAndWait];
        
        [self.fetchedResultsController performFetch:nil];
        
        [self.tableView reloadData:YES];
    }
}

#pragma mark - Add Perfil
- (IBAction)addPerfil:(id)sender
{
    [self performSegueWithIdentifier:@"AdicionarPerfilSegue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([segue.identifier isEqualToString:@"EditarPerfilSegue"]) {
        EditarPerfilViewController *controller = segue.destinationViewController;
        controller.selectedPerfil = self.selectedPerfil;
        controller.selectedTipo = self.selectedTipo;
    } else if ([segue.identifier isEqualToString:@"AdicionarPerfilSegue"]) {
        EditarPerfilViewController *controller = segue.destinationViewController;
        controller.selectedTipo = self.selectedTipo;
    }
    
}

@end
