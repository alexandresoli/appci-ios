//
//  LoginViewController.h
//  Appci
//
//  Created by BrEstate LTDA on 17/03/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController 

@property (nonatomic) BOOL lostAccess;
@property (nonatomic) BOOL definePassword;

@end
