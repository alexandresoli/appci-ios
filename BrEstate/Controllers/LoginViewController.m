//
//  LoginViewController.m
//  Appci
//
//  Created by BrEstate LTDA on 17/03/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "LoginViewController.h"

#import "AddressBookHelper.h"
#import "EventosViewController.h"
#import "CustomCalendar.h"
#import "DispositivosViewController.h"
#import "Corretor+Utils.h"
#import "AtivacaoDispositivoViewController.h"
#import "AlertaUtils.h"
#import "PasswordViewController.h"
#import "Migrator.h"
#import "SmsManager.h"
#import "ForgotPasswordViewController.h"
#import "Imobiliaria+Utils.h"
#import "DateUtils.h"
#import "FirebaseUtilsUser.h"
#import "Cliente+Firebase.h"

@interface LoginViewController () <UITextFieldDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *esqueciSenhaButton;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic, strong) NSManagedObjectContext *moc;
@property (nonatomic, strong) NSArray *devices;
@property (nonatomic) BOOL shouldSkip;

@end


@implementation LoginViewController


#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    self.moc = [NSManagedObjectContext MR_defaultContext];
    
    [self configureView];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self hideObjects];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (_shouldSkip) {
        return;
    }
    
    Migrator *migrator = [[Migrator alloc] init];
    [migrator checkMigrateStoreToParseWithCompletionBlock:^(BOOL finished) {
        
        if (self.lostAccess) {
            [self processLostAccess];
            
        } else if (self.definePassword) {
            [self showObjects];
            return;
            
        } else {
            [self verifyAccess];
        }
    }];
}


- (void)hideObjects
{
    [self.view bringSubviewToFront:self.backgroundImageView];
}


- (void)showObjects
{
    [self.view sendSubviewToBack:self.backgroundImageView];
}


//- (BOOL)prefersStatusBarHidden
//{
//    return YES;
//}


-(BOOL)shouldAutorotate
{
    return NO;
}


#pragma mark - View Customization

- (void)configureView
{
    // textfield image
    UIImageView *emailImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 16, 11)];
    emailImageView.image = [UIImage imageNamed:@"mail"];
    
    UIView *emailPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 24)];
    [emailPaddingView addSubview:emailImageView];
    
    self.emailTextField.leftView = emailPaddingView;
    self.emailTextField.leftViewMode = UITextFieldViewModeAlways;
    
    // password image
    UIImageView *passwordImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 12, 16)];
    passwordImageView.image = [UIImage imageNamed:@"lock"];
    
    UIView *passwordPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 24)];
    [passwordPaddingView addSubview:passwordImageView];
    
    self.passwordTextField.leftView = passwordPaddingView;
    self.passwordTextField.leftViewMode = UITextFieldViewModeAlways;
}


#pragma mark - Forgot Password

- (IBAction)forgotPassword:(id)sender
{
    [self performSegueWithIdentifier:@"ForgotPasswordSegue" sender:nil];
}


#pragma mark - Password Check

- (BOOL)hasPassword
{
    Corretor *corretor = [Corretor corretorWithContext:self.moc];
    
    if (corretor != nil && [Utils valueFromDefaults:USER_PASS] == nil) {
        
        if (![Utils isConnected]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Verificação de Senha"
                                                            message:@"Necessário estar conectado à internet para realizar o primeiro acesso à nova versão. \n Verifique sua conexão."
                                                           delegate:self
                                                  cancelButtonTitle:@"Tentar Novamente"
                                                  otherButtonTitles: nil];
            [alert show];
            return NO;
        }
        
        // Verifica se o usuário já existe no parse. Se já existir abre a tela de login.
        //        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        //        NSString *email = [defaults objectForKey:USER_LOGIN];
        //        BOOL exists = [ParseUtils userExists:email];
        //
        //        if (exists) {
        //            [self showObjects];
        //            return NO;
        //        }
        
        // Abre a tela para definição de senha
        [self performSegueWithIdentifier:@"PasswordSegue" sender:self];
        
        return NO;
    }
    
    return YES;
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView.title isEqualToString:@"Verificação de Senha"]) {
        // Has only one option.
        [self verifyAccess];
    }
}


#pragma mark - Verify Access

// Verifica se o usuário já logou anteriormente.
- (void)verifyAccess
{
    // Pega o login do usuário caso já tenha logado.
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *login = [defaults objectForKey:USER_LOGIN];
    NSString *password = [[NSString alloc] initWithData:[Utils valueFromDefaults:USER_PASS] encoding:NSUTF8StringEncoding];
    
    // first time use
    if (!login) {
        [self showObjects];
        return;
    }
    
    [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:FIREBASE_ALREADY_RUN];
    
    self.emailTextField.text = login;
    
    // check if user has a password setted
    if (![self hasPassword]) {
        return;
    }
    
    // check if the user is on Firebase
    if (![Utils valueFromDefaults:FIREBASE_USER_ID]) {
        [Utils showMessage:@"Por motivo de segurança é necessário que você informe novamente sua senha."];
        [self performSegueWithIdentifier:@"PasswordSegue" sender:self];
        return;
    }
    
    if (![Utils isConnected]) {
        // Observers Firebase.
        [[Utils firebaseUtils] startObservers];
        
        [self enterAppci];
        return;
    }
    
    // Login Firebase
    [FirebaseUtilsUser loginUser:login withPassword:password andCompletionBlock:^(NSError *error) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            if (error) {
                
                [Flurry logError:@"Erro no verifyAccess" message:error.localizedDescription exception:nil];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [Utils addObjectToDefaults:@"Ocorreu um erro durante a verificação de acesso. Por favor, realize novamente o login." withKey:ERROR_MESSAGE_ON_LOGIN];
                    [self performSelector:@selector(postNotificationNoAccess) withObject:nil afterDelay:1];
                });
                return;
            }
            
            [FirebaseUtilsUser updateCorretorInformationFromFirebase];
            
            // Check Devices.
//            self.devices = [FirebaseUtilsUser getDevices];
//            
//            if (_devices.count >= [self getDevicesLimitForCorretor]) {
//                
//                if (![self deviceExists]) {
//                    
//                    [Utils addObjectToDefaults:@"Este dispositivo perdeu o acesso à conta. Faça o login novamente."
//                                       withKey:ERROR_MESSAGE_ON_LOGIN];
//                    
//                    [Utils prepareLogout];
//                    
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [self performSelector:@selector(postNotificationNoAccess) withObject:nil afterDelay:1];
//                    });
//                    
//                    return;
//                }
//            }
            
            // Hack clientes Amaury.
//            NSString *user = [Utils valueFromDefaults:USER_LOGIN];
//            if ([user isEqualToString:@"amaury@barrabest.com.br"] && ![Utils valueFromDefaults:FIREBASE_AMAURY_HACK_CLIENTE]) {
//                
//                [Utils showProgress:@"Aguarde Amaury. Seus contatos estão sendo otimizados."];
//                
//                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                    [Cliente hackDedup];
//                    
//                    [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:FIREBASE_AMAURY_HACK_CLIENTE];
//                    
//                    [FirebaseUtilsUser registerDevice];
//                    
//                    [Utils dismissProgress];
//                    
//                    // Observers Firebase.
//                    [[Utils firebaseUtils] startObservers];
//                    
//                    [self verifyAccessCorretor];
//                });
//                
//                return;
//            }
            
            [FirebaseUtilsUser registerDevice];
            
            // Observers Firebase.
            [[Utils firebaseUtils] startObservers];
            
            [self verifyAccessCorretor];
            
        });
    }];
    
    [self enterAppci];
}


- (void)verifyAccessCorretor
{
    NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
    
    // Verifica se houve resposta das confirmações de SMS.
    [SmsManager checkConfirmationSmsWithMoc:moc andCompletionBlock:nil];
    
    // Busca novos contatos no device.
//    AddressBookHelper *addressBookHelper = [[AddressBookHelper alloc] initWithMoc:[NSManagedObjectContext MR_defaultContext]];
//    [addressBookHelper checkContactsShowReturnMessage:NO];
    
    // Importa novos eventos da agenda do device.
//    [CustomCalendar importEventsShowReturnMessage:NO];
    
    // Verifica se houve erro no registro da compra do plano TOP (PARAMS_REGISTER_PURCHASE preenchido) para tentar registrar novamente.
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    NSDictionary *paramsPurchase = [[NSUserDefaults standardUserDefaults] objectForKey:PARAMS_REGISTER_PURCHASE];
    if (paramsPurchase) {
        [FirebaseUtilsUser resendPurchaseToFirebase:paramsPurchase];
    }
    
    // Verifica se o plano TOP está para terminar para gerar uma notification.
    [delegate.iapHelper checkPlanTopWillEndToCreateNotification];
    
    // Verifica se o plano TOP terminou para gerar uma notification.
    [delegate.iapHelper checkPlanTopIsOverToCreateNotification];
}


#pragma mark - Check Expiration Imobiliária

- (void)checkExpirationImobiliaria
{
    Imobiliaria *imobiliaria = [Imobiliaria MR_findFirst];
    
    if ([DateUtils compare:[NSDate date] withDate:imobiliaria.expiration] == NSOrderedDescending) {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:@"Assinatura da sua imobiliária expirou!" forKey:ERROR_MESSAGE_ON_LOGIN];
        [defaults synchronize];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSelector:@selector(postNotificationNoAccess) withObject:nil afterDelay:1];
        });
    }
}


#pragma mark - Enter Appci

- (void)enterAppci
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:LOST_ACCESS];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [Utils checkFirstRunForPrepareDataWithMoc:self.moc];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [Flurry setUserID:[Utils valueFromDefaults:USER_LOGIN]];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            // Get the storyboard named secondStoryBoard from the main bundle:
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"_Eventos_iPad" bundle:nil];
            
            // Load the initial view controller from the storyboard.
            UINavigationController *navController = [storyboard instantiateInitialViewController];
            EventosViewController *controller = [[navController viewControllers] objectAtIndex:0];
            
            [self.navigationController setViewControllers:@[controller] animated:NO];
            
        } else {
            [self performSegueWithIdentifier:@"EnterSegue" sender:self];
        }
    });
}


#pragma mark - Login

- (IBAction)login:(id)sender
{
    [self.view endEditing:YES];
    
    // connection check
    if (![Utils isConnected]) {
        [Utils showMessage:@"Verifique sua conexão com a internet."];
        return;
    }
    
    // form validation
    if (![self formLoginIsValid]) {
        return;
    }
    
    [Utils showProgress:@"Realizando login. Aguarde..."];
    
    [self performSelector:@selector(doLogin) withObject:nil afterDelay:0.2];
}


- (void)doLogin
{
    [Utils resetDatabaseIfNeededWithEmail:self.emailTextField.text andMoc:self.moc];
    [self.moc MR_saveToPersistentStoreAndWait];
    
    // Login Firebase
    [FirebaseUtilsUser loginUser:self.emailTextField.text withPassword:self.passwordTextField.text andCompletionBlock:^(NSError *error) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            if (error) {
                [Utils dismissProgress];
                
                if (error.code == -42) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [Utils showMessage:error.localizedDescription];
                        
                        [Utils removeObjectFromDefaults:USER_PASS];
                        
                        [self performSegueWithIdentifier:@"ChangePasswordSegue" sender:self];
                    });
                    
                    return;
                }
                
                NSString *message = [[Utils firebaseUtils] messageForError:error];
                [Utils showMessage:message];
                
                return;
            }
            
            [FirebaseUtilsUser updateCorretorInformationFromFirebase];
            
            // Check Devices.
//            self.devices = [FirebaseUtilsUser getDevices];
//            
//            if (_devices.count >= [self getDevicesLimitForCorretor]) {
//                
//                if (![self deviceExists]) {
//                    
//                    [Utils dismissProgress];
//                    [Utils showMessage:@"Limite de dispositivos excedido!"];
//                    
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [self performSegueWithIdentifier:@"DispositivosSegue" sender:self];
//                    });
//                    return;
//                }
//            }
            
            [Utils saveLoginUserDefaultsWithEmail:self.emailTextField.text andPassword:self.passwordTextField.text];
            
            [FirebaseUtilsUser registerDevice];
            
            // Hack clientes Amaury.
//            NSString *user = [Utils valueFromDefaults:USER_LOGIN];
//            if ([user isEqualToString:@"amaury@barrabest.com.br"] && ![Utils valueFromDefaults:FIREBASE_AMAURY_HACK_CLIENTE]) {
//                
//                [Utils showProgress:@"Aguarde Amaury. Seus contatos estão sendo otimizados."];
//                
//                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                    [Cliente hackDedup];
//                    
//                    [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:FIREBASE_AMAURY_HACK_CLIENTE];
//                    
//                    // Observers Firebase.
//                    [[Utils firebaseUtils] startObservers];
//                    
//                    [Utils dismissProgress];
//                    
//                    [self enterAppci];
//                });
//                
//                return;
//            }
            
            [Utils dismissProgress];
            
            // Observers Firebase.
            [[Utils firebaseUtils] startObservers];
            
            [self enterAppci];
        });
    }];
}


#pragma mark - Compare Devices
- (BOOL)deviceExists
{
    NSString *deviceID = [Utils deviceID];
    
    NSArray *list = [_devices filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"deviceID = %@",deviceID]];
    if (list.count > 0 ) {
        return YES;
    }
    
    return NO;
}


#pragma mark - Get Devices

- (int)getDevicesLimitForCorretor
{
    int devicesLimit;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // if TOP the devices limit is 2, if not devices limit is 1.
    if ([appDelegate.iapHelper checkPlanTopFromController:nil withMessage:nil]) {
        devicesLimit = 2;
    } else {
        devicesLimit = 1;
    }
    
    // check if the corretor has a custom devices limit.
    Corretor *corretor = [Corretor corretorWithContext:self.moc];
    
    if (corretor.limiteDevices && [corretor.limiteDevices intValue] > 0) {
        devicesLimit = [corretor.limiteDevices intValue];
    }
    
    return devicesLimit;
}


#pragma mark - Form Validation

- (BOOL)formLoginIsValid
{
    BOOL isValid = YES;
    
    // email
    if (self.emailTextField.text.length > 0) {
        
        if (![Utils validateEmail:self.emailTextField.text]) {
            [Utils addRedCorner:self.emailTextField];
            isValid = NO;
        }
        
    } else {
        [Utils addRedCorner:self.emailTextField];
        isValid = NO;
    }
    
    
    if (_passwordTextField.text.length == 0 ) {
        [Utils addRedCorner:_passwordTextField];
        isValid = NO;
    }
    
    return isValid;
}


#pragma mark - Post Notification No Access

- (void)postNotificationNoAccess
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NoAccess" object:nil];
}


#pragma mark - Proccess Lost Access

- (void)processLostAccess
{
    [self showObjects];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *errorMessage = [defaults objectForKey:ERROR_MESSAGE_ON_LOGIN];
    NSNumber *errorCode = [defaults objectForKey:ERROR_CODE_ON_LOGIN];
    
    self.emailTextField.text = [defaults objectForKey:USER_LOGIN];
    self.passwordTextField.text = [[NSString alloc] initWithData:[defaults objectForKey:USER_PASS] encoding:NSUTF8StringEncoding];
    
    if (errorMessage) {
        [Utils showMessage:errorMessage];
        
        if ([errorCode intValue] == ERROR_CODE_DEVICES_EXCEEDED) {
            self.devices = [defaults objectForKey:ARRAY_DEVICES_EXCEEDED];
            
            [self performSegueWithIdentifier:@"DispositivosSegue" sender:self];
        }
        
        [defaults removeObjectForKey:ERROR_MESSAGE_ON_LOGIN];
        [defaults removeObjectForKey:ERROR_CODE_ON_LOGIN];
        [defaults removeObjectForKey:ARRAY_DEVICES_EXCEEDED];
        [defaults synchronize];
    }
    
    self.lostAccess = NO;
}


#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"EnterSegue"]) {
        // Define o segundo item do tab bar como o selecionado na versão do iPhone.
        UITabBarController *tabBar = (UITabBarController *)segue.destinationViewController;
        tabBar.selectedIndex = 1;
        
    } else if ([segue.identifier isEqualToString:@"DispositivosSegue"]) {
        UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
        DispositivosViewController *controller = [[navController viewControllers] objectAtIndex:0];
        
        controller.devices = self.devices;
        controller.loginUser = self.emailTextField.text;
        
    } else if ([segue.identifier isEqualToString:@"AtivacaoSegue"]) {
        AtivacaoDispositivoViewController *controller = segue.destinationViewController;
        controller.loginUser = self.emailTextField.text;
        
    } else if ([segue.identifier isEqualToString:@"PasswordSegue"]) {
        self.definePassword = YES;
        //        [Utils sendTokenToEmail:self.emailTextField.text withCompletionBlock:nil];
        
        PasswordViewController *controller = segue.destinationViewController;
        controller.loginUser = self.emailTextField.text;
    } else if ([segue.identifier isEqualToString:@"ForgotPasswordSegue"]) {
        ForgotPasswordViewController *controller = segue.destinationViewController;
        controller.email = self.emailTextField.text;
    } else if ([segue.identifier isEqualToString:@"ChangePasswordSegue"]) {
        PasswordViewController *controller = segue.destinationViewController;
        controller.loginUser = self.emailTextField.text;
        controller.password = _passwordTextField.text;
        controller.changePassword = YES;
    }
}


#pragma mark - Unwind segues

- (IBAction)unwindFromDispositivos:(UIStoryboardSegue *)segue
{
    [segue.sourceViewController dismissViewControllerAnimated:YES completion:^{
        [self performSegueWithIdentifier:@"AtivacaoSegue" sender:self];
    }];
}


- (IBAction)unwindFromChangePassword:(UIStoryboardSegue *)segue
{
    _passwordTextField.text = [[NSString alloc] initWithData:[Utils valueFromDefaults:USER_PASS] encoding:NSUTF8StringEncoding];
    self.definePassword = NO;
    self.shouldSkip = YES;
    
    [segue.sourceViewController dismissViewControllerAnimated:YES completion:^{
        [self login:nil];
    }];
}

- (IBAction)unwindFromAtivacao:(UIStoryboardSegue *)segue
{
    [segue.sourceViewController dismissViewControllerAnimated:YES completion:^{
        [self login:nil];
    }];
}


- (IBAction)unwindFromLostPassword:(UIStoryboardSegue *)segue
{
    self.definePassword = YES;
    self.emailTextField.text = [Utils valueFromDefaults:USER_LOGIN];
    
}

- (IBAction)unwindFromPassword:(UIStoryboardSegue *)segue
{
    self.emailTextField.text = [Utils valueFromDefaults:USER_LOGIN];
    
    self.passwordTextField.text = [[NSString alloc] initWithData:[Utils valueFromDefaults:USER_PASS] encoding:NSUTF8StringEncoding];
    
    [segue.sourceViewController dismissViewControllerAnimated:YES completion:^{
        [self showObjects];
        [self login:nil];
    }];
}


//- (IBAction)unwindFromCreateAccount:(UIStoryboardSegue *)segue
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//
//    self.emailTextField.text = [defaults objectForKey:USER_LOGIN];
//    self.passwordTextField.text = [[NSString alloc] initWithData:[defaults objectForKey:USER_PASS] encoding:NSUTF8StringEncoding];
//
//    [segue.sourceViewController dismissViewControllerAnimated:YES completion:^{
//        [self showObjects];
//        [self login:nil];
//    }];
//}


#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPad) {
        
        if ([[UIScreen mainScreen] bounds].size.height < 568) {
            [self animateTextField:textField up:YES];
        }
    }
    
    [Utils removeRedCorner:textField];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPad) {
        
        if ([[UIScreen mainScreen] bounds].size.height < 568) {
            [self animateTextField:textField up:NO];
        }
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _emailTextField) {
        [_passwordTextField becomeFirstResponder];
        
    } else if (textField == _passwordTextField) {
        [textField resignFirstResponder];
        [self login:nil];
    }
    
    return YES;
}


- (void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -85; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


#pragma mark - IBAction Dismiss Keyboard

- (IBAction)dismissKeyboard:(id)sender
{
    [self.view endEditing:YES];
}


@end
