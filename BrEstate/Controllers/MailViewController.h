//
//  MailViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/2/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Cliente,Imovel,Evento;
@interface MailViewController : BaseViewController

- (void)sendEmail:(Cliente *)cliente forSender:(UIViewController *)sender;
- (void)sendEmailEvento:(Evento *)evento forSender:(UIViewController *)sender;
- (void)sendPorfolio:(Imovel *)imovel highQuality:(BOOL)highQuality forSender:(UIViewController *)sender;

@end
