//
//  MailViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/2/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "MailViewController.h"
@import MessageUI;

#import "Evento.h"
#import "Cliente.h"
#import "Imovel.h"
#import "FotoImovel+InsertDeleteImagem.h"
#import "Endereco.h"
#import "Bairro.h"
#import "DateUtils.h"
#import "TagImovel.h"
#import "ImageUtils.h"
#import "SVProgressHUD.h"
#import "Acompanhamento+Firebase.h"

@interface MailViewController ()  <MFMailComposeViewControllerDelegate>

@property (nonatomic,strong) MFMailComposeViewController* mailComposer;
@property (nonatomic, strong) Cliente *selectedCliente;
@property (nonatomic, strong) Evento *selectedEvento;
@property BOOL isEvento;
@property BOOL isPortfolio;
@property BOOL isCliente;

@property (nonatomic, strong) UIViewController *sender;

@end

@implementation MailViewController

@synthesize mailComposer;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSManagedObjectContext *)moc
{
    return [NSManagedObjectContext MR_defaultContext];
}


#pragma mark - Evento
- (void)sendEmailEvento:(Evento *)evento forSender:(UIViewController *)sender
{
    
    if([MFMailComposeViewController canSendMail])
    {
        
        [Utils trackWithName:@"Abriu email Evento"];
        [Utils trackFlurryWithName:@"Abriu email Evento"];
        
        
        self.selectedEvento = evento;
        self.isEvento = YES;
        
        mailComposer = [[MFMailComposeViewController alloc] init];
        mailComposer.mailComposeDelegate = self;
        
        NSMutableArray *emails = [NSMutableArray new];
        for (Cliente *cliente in [evento.cliente allObjects]) {
            if (cliente.email) {
                [emails addObject:cliente.email];
            }
        }
        
        [mailComposer setToRecipients:emails];
        
        NSString *subject = @"[AppCi] Contato";
        
        if (evento.titulo != nil) {
            subject = [NSString stringWithFormat:@"[AppCi] %@",evento.titulo];
        }
        
        [mailComposer setSubject:subject];
        [mailComposer setModalPresentationStyle:UIModalPresentationPageSheet];
        [mailComposer setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        
        
        //[[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:mailComposer animated:YES completion:nil];
        
        self.sender = sender;
        [self.sender.parentViewController presentViewController:mailComposer animated:YES completion:nil];
        
    } else
    {
        [self showMessage];
    }
}

#pragma mark - Cliente
- (void)sendEmail:(Cliente *)cliente forSender:(UIViewController *)sender
{
    
    
    if([MFMailComposeViewController canSendMail])
    {
        
        [Utils trackWithName:@"Abriu email Cliente"];
        [Utils trackFlurryWithName:@"Abriu email Cliente"];
        
        
        self.selectedCliente = cliente;
        self.isCliente = YES;
        
        mailComposer = [[MFMailComposeViewController alloc] init];
        mailComposer.mailComposeDelegate = self;
        
        if (cliente.email.length > 0) {
            [mailComposer setToRecipients:[NSArray arrayWithObject:cliente.email]];
        }
        
        [mailComposer setSubject:@"[AppCi] Contato"];
        [mailComposer setModalPresentationStyle:UIModalPresentationPageSheet];
        [mailComposer setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        
        
        //[[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:mailComposer animated:YES completion:nil];
        
        self.sender = sender;
        [self.sender.parentViewController presentViewController:mailComposer animated:YES completion:nil];
        
    } else
    {
        [self showMessage];
    }
    
}


//#pragma mark - Portfolio
//- (NSString *)formatBody:(Imovel *)imovel
//{
//    NSError *error = nil;
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"bootstrap.min" ofType:@"css"];
//    NSString *bootstrap = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];;
//    path = [[NSBundle mainBundle] pathForResource:@"bootstrap-override" ofType:@"css"];
//    NSString *override = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
//    path = [[NSBundle mainBundle] pathForResource:@"main-style" ofType:@"css"];
//    NSString *main = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];;
//
//    NSString *htmlBody = [NSString stringWithFormat:
//    @"<!DOCTYPE html>"
//        "<head>"
//            "<meta charset='utf-8'>"
//            "<style type='text/css'>%@ %@ %@</style>"
//        "</head>"
//
//                          "<body>"
//                          "<div class='container-fluid'>"
//
//
//                          "<div class='container-fluid'>"
//                          "<div class='row'>"
//                          "<div class='content-area'>"
//                          " <div class='col-xs-12'>"
//
//                          "<div class='row'>"
//                          "<div class='col-sm-12'>"
//                          "<div class='inner-content'>"
//
//                          "<nav class='navbar navbar-default' role='navigation'>"
//                          "<div class='container-fluid no-margin'>"
//
//                          "<ul class='nav navbar-nav'>"
//                          "<li><a href='#' class='btn btn-primary btn-lg'>Tipo</a></li>"
//                          "<li><a href='#' class='btn btn-primary btn-lg'>Bairro</a></li>"
//                          "<li><a href='#' class='btn btn-primary btn-lg'>Quartos</a></li>"
//                          "<li><a href='#' class='btn btn-primary btn-lg'>Valor</a></li>"
//                          "<li><a href='#' class='btn btn-primary btn-lg'>Area Util</a></li>"
//                          "<li><a href='#' class='btn btn-primary btn-lg'>Oferta</a></li>"
//
//                          "</ul>"
//
//
//                          "<div class='row'>"
//                          "<div class='col-sm-8'>"
//                          "<div class='inner-content'>"
//                          "<div class='section-header'>Descrição</div>"
//                          "Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum."
//                          "</div>"
//                          "</div>"
//                          "<div class='col-sm-4'>"
//                          "<div class='block-gray'>"
//                          "<ul>"
//                          "<li>salão de festas</li>"
//                          "<li>armário embutido</li>"
//                          "<li class='text-blue'>varanda</li>"
//                          "<li>wc empregada</li>"
//                          "<li>children care</li>"
//                          "<li class='text-blue'>churrasqueira</li>"
//                          "<li class='text-blue'>piscina</li>"
//                          "<li>playground</li>"
//                          "<li class='text-blue'>salão de jogos</li>"
//                          "<li>salão de festas</li>"
//                          "<li class='text-blue'>quadra poliestportiva</li>"
//                          "<li>sala de ginástica</li>"
//                          "</ul>"
//
//                          "</div>"
//                          "<p>Condomínio: <strong>R$ XXX.XXX,XX</strong>   &nbsp;   &bull; &nbsp; IPTU: <strong>XXX.XXX,XX</strong></p>"
//                          "</div>"
//                          "</div><!-- row -->"
//                          "<div class='clearfix'></div>"
//                          "</div>"
//
//                          "</div>"
//
//                          "</div>"
//                          "</div>"
//
//
//                          "</body>"
//    "</html>", bootstrap,override,main];
//
//
//    return htmlBody;
//
//}


- (NSString *)formatBody:(Imovel *)imovel
{
    // body
    NSMutableString *body = [NSMutableString string];
    [body appendString:@"<html><body><p>"];
    
    // descricao do imovel
    if (imovel.descricao.length > 0) {
        [body appendString:[NSString stringWithFormat:@"<b>Descrição:</b> </br>%@</br></br>",imovel.descricao]];
    }
    
    // tipo
    if (imovel.tipo.length > 0) {
        [body appendString:[NSString stringWithFormat:@"<b>Tipo:</b> %@</br></br>",imovel.tipo]];
    }
    
    // oferta
    if (imovel.oferta.length > 0) {
        [body appendString:[NSString stringWithFormat:@"<b>Oferta:</b> %@</br></br>",imovel.oferta]];
    }
    
    // valor
    if (imovel.valor.longLongValue > 0) {
        [body appendString:[NSString stringWithFormat:@"<b>Valor:</b> %@</br></br>",[self.formatter stringFromNumber:imovel.valor]]];
    }
    
    // condominio
    if (imovel.condominio.longLongValue > 0) {
        [body appendString:[NSString stringWithFormat:@"<b>Condomínio:</b> %@</br></br>",[self.formatter stringFromNumber:imovel.condominio]]];
    }
    
    // iptu
    if (imovel.iptu.longLongValue > 0) {
        [body appendString:[NSString stringWithFormat:@"<b>IPTU:</b> %@</br></br>",[self.formatter stringFromNumber:imovel.iptu]]];
    }
    
    // quartos
    if (imovel.quartos.intValue > 0) {
        [body appendString:[NSString stringWithFormat:@"<b>Quartos:</b> %d</br></br>",imovel.quartos.intValue]];
    }
    
    // area total
    if (imovel.areaTotal.intValue > 0) {
        [body appendString:[NSString stringWithFormat:@"<b>Área Total:</b> %@ m²</br></br>",imovel.areaTotal]];
    }
    
    // area util
    if (imovel.areaUtil.intValue > 0) {
        [body appendString:[NSString stringWithFormat:@"<b>Área Útil:</b> %@ m²</br></br>",imovel.areaUtil]];
    }
    
    
    NSString *tags = [self checkTags:imovel];
    
    if (tags.length > 0) {
        //        [body appendString:@"<b>Características do imóvel:</b> </br></br>"];
        [body appendString:tags];
    }
    
    [body appendString:@"</p></body></html>"];
    
    return body;
}


- (NSString *)formatSubject:(Imovel *)imovel
{
    // subject
    NSMutableString *subject = [NSMutableString string];
    [subject appendString:@"[AppCi] Informações do Imóvel"];
    
    if (imovel.identificacao.length > 0) {
        [subject appendString:[NSString stringWithFormat:@" - %@",imovel.identificacao]];
    }
    
    if (imovel.endereco.bairro.nome.length > 0) {
        [subject appendString:[NSString stringWithFormat:@" - %@",imovel.endereco.bairro.nome]];
    }
    
    return subject;
}
- (void)sendPorfolio:(Imovel *)imovel highQuality:(BOOL)highQuality forSender:(UIViewController *)sender
{
    
    if([MFMailComposeViewController canSendMail])
    {
        [SVProgressHUD showWithStatus:@"Aguarde..." maskType:SVProgressHUDMaskTypeBlack];
        
        [Utils trackWithName:@"Abriu email Portfolio"];
        [Utils trackFlurryWithName:@"Abriu email Portfolio"];
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            mailComposer = [[MFMailComposeViewController alloc] init];
            mailComposer.mailComposeDelegate = self;
            [mailComposer setSubject:[NSString stringWithFormat:@"[AppCi] Imóvel"]];
            
            // subject
            NSString *subject = [self formatSubject:imovel];
            [mailComposer setSubject:subject];
            
            // body
            NSString *body = [self formatBody:imovel];
            [mailComposer setMessageBody:body isHTML:YES];
            
            
            for (FotoImovel *fotoImovel in imovel.fotos) {
                
                if ([fotoImovel.video boolValue] == YES) {
                    continue;
                }
                
                UIImage *image = [fotoImovel getImageForFotoImovelURL];
                
                //CGSize size = CGSizeMake(640.0f, 480.0f);
                //image = [ImageUtils resize:image scaledToSize:size];
                
                NSData *imageData;
                NSString *mimeType;
                NSString *fileName;
                if (highQuality) {
                    imageData = UIImagePNGRepresentation(image);
                    mimeType = @"image/png";
                    fileName = [NSString stringWithFormat:@"%@.png",fotoImovel.descricao];
                } else {
                    imageData = UIImageJPEGRepresentation(image, 1.0f);
                    mimeType = @"image/jpeg";
                    fileName = [NSString stringWithFormat:@"%@.jpeg",fotoImovel.descricao];
                }
                
                
                
                [mailComposer addAttachmentData:imageData mimeType:mimeType fileName:fileName];
            }
            
            [mailComposer setModalPresentationStyle:UIModalPresentationPageSheet];
            [mailComposer setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            
            self.sender = sender;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.sender.parentViewController presentViewController:mailComposer animated:YES completion:nil];
            });
            
            [Utils dismissProgress];
            
        });
        
    } else {
        [Utils dismissProgress];
        [self showMessage];
    }
    
}


#pragma mark - Warning Message
- (void)showMessage
{
    
    [Utils trackWithName:@"Usuario sem conta de email"];
    [Utils trackFlurryWithName:@"Usuario sem conta de email"];
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Alerta" message:@"Nenhuma conta de e-mail foi encontrada. Configure uma conta e tente novamente." delegate:nil cancelButtonTitle:@"Dispensar" otherButtonTitles:nil, nil];
    [alertView show];
}


#pragma mark - MFMailComposerDelegate Methods
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if (result == MFMailComposeResultSent) {
        
        if (self.isCliente) {
            [Utils trackWithName:@"Enviou email cliente"];
            [Utils trackFlurryWithName:@"Enviou email cliente"];
            [self salvarAcompanhamentoCliente];
        } else if (self.isEvento) {
            [Utils trackWithName:@"Enviou email evento"];
            [Utils trackFlurryWithName:@"Enviou email evento"];
            [self salvarAcompanhamentoEvento];
        } else if (self.isPortfolio) {
            [Utils trackWithName:@"Enviou email portfolio"];
            [Utils trackFlurryWithName:@"Enviou email portfolio"];
            [self salvarAcompanhamentoPortfolio];
        }
        
    }
    
    //[[[[[UIApplication sharedApplication] delegate] window] rootViewController] dismissViewControllerAnimated:YES completion:nil];
    [self.sender dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Acompanhamento Cliente
- (void)salvarAcompanhamentoCliente
{
    Acompanhamento *acompanhamento = [Acompanhamento MR_createInContext:self.moc];
    acompanhamento.codigo = [acompanhamento getUniqueCode]; //[NSNumber numberWithLong:codigo];
    acompanhamento.titulo = [NSString stringWithFormat:@"Email enviado"];
    acompanhamento.timeStamp = [NSDate date];
    acompanhamento.cliente = self.selectedCliente;
    
    [acompanhamento saveFirebase];
    
    [self saveMOC];
    
}

#pragma mark - Acompanhamento Evento
- (void)salvarAcompanhamentoEvento
{
    // save for each client
    for (Cliente *cliente in [self.selectedEvento.cliente allObjects]) {
        
        Acompanhamento *acompanhamento = [Acompanhamento MR_createInContext:self.moc];
        acompanhamento.codigo = [acompanhamento getUniqueCode]; //[NSNumber numberWithLong:codigo];
        acompanhamento.titulo = [NSString stringWithFormat:@"Email enviado - Evento: %@",self.selectedEvento.titulo];
        acompanhamento.timeStamp = [NSDate date];
        acompanhamento.cliente = cliente;
        
        [acompanhamento saveFirebase];
        
        [self saveMOC];
        //        [acompanhamento saveParse:self.moc];
    }
}


#pragma mark - Acompanhamento Portfolio
- (void)salvarAcompanhamentoPortfolio
{
    
}



#pragma mark - Portfolio Utility Methods
- (NSString *)checkTags:(Imovel *)imovel;
{
    
    NSString *outrasCaracteristicas = @"";
    NSString *areaComum = @"";
    
    TagImovel *tag = imovel.tags;
    
    
    // outras caracteristicas
    if ([tag.armarioCozinha boolValue] == YES) {
        outrasCaracteristicas = [outrasCaracteristicas stringByAppendingString:@"- Armário de Cozinha </br>"];
    }
    if ([tag.armarioEmbutido boolValue] == YES) {
        outrasCaracteristicas = [outrasCaracteristicas stringByAppendingString:@"- Armário Embutido </br>"];
    }
    if ([tag.varanda boolValue] == YES) {
        outrasCaracteristicas = [outrasCaracteristicas stringByAppendingString:@"- Varanda </br>"];
    }
    if ([tag.wcEmpregada boolValue] == YES) {
        outrasCaracteristicas = [outrasCaracteristicas stringByAppendingString:@"- WC Empregada </br>"];
    }
    
    if (outrasCaracteristicas.length > 0) {
        outrasCaracteristicas = [@"Outras características: </br>" stringByAppendingString:outrasCaracteristicas];
    }
    
    
    // area comum
    if ([tag.childrenCare boolValue] == YES) {
        areaComum = [areaComum stringByAppendingString:@"- Children Care </br>"];
    }
    if ([tag.churrasqueira boolValue] == YES) {
        areaComum = [areaComum stringByAppendingString:@"- Churrasqueira </br>"];
    }
    if ([tag.piscina boolValue] == YES) {
        areaComum = [areaComum stringByAppendingString:@"- Piscina </br>"];
    }
    if ([tag.playground boolValue] == YES) {
        areaComum = [areaComum stringByAppendingString:@"- Playground </br>"];
    }
    if ([tag.quadraPoliesportiva boolValue] == YES) {
        areaComum = [areaComum stringByAppendingString:@"- Quadra Poliesportiva </br>"];
    }
    if ([tag.salaGinastica boolValue] == YES) {
        areaComum = [areaComum stringByAppendingString:@"- Sala de Ginástica </br>"];
    }
    if ([tag.salaoFestas boolValue] == YES) {
        areaComum = [areaComum stringByAppendingString:@"- Salão de Festas </br>"];
    }
    if ([tag.salaoJogos boolValue] == YES) {
        areaComum = [areaComum stringByAppendingString:@"- Salão de Jogos </br>"];
    }
    if ([tag.sauna boolValue] == YES) {
        areaComum = [areaComum stringByAppendingString:@"- Sauna </br>"];
    }
    if ([tag.garagem boolValue] == YES) {
        areaComum = [areaComum stringByAppendingString:@"- Garagem </br>"];
    }
    
    
    if (areaComum.length > 0) {
        areaComum = [@"Características das áreas comuns: </br>" stringByAppendingString:areaComum];
    }
    
    NSString *message = [NSString stringWithFormat:@"%@ </br> %@",areaComum, outrasCaracteristicas];
    
    
    
    return message;
}



@end
