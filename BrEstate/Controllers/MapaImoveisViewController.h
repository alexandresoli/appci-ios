//
//  MapaImoveisViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/5/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Imovel;

@interface MapaImoveisViewController : BaseViewController

@property (nonatomic, strong) Imovel *selectedImovel;

@end
