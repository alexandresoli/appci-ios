//
//  MapaImoveisViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/5/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "MapaImoveisViewController.h"
#import "OpcoesMapaViewController.h"
#import "MapPin.h"
#import "Estado.h"
#import "Endereco.h"
#import "Imovel.h"
#import "FotoImovel+InsertDeleteImagem.h"
#import "Cliente.h"
#import <GoogleMaps/GoogleMaps.h>
#import "JPSThumbnailAnnotation.h"
#import "Utils.h"
#import "PortfolioViewController.h"
#import "Bairro.h"
#import "ImageUtils.h"
#import "Imovel+Firebase.h"

@import MapKit;

@interface MapaImoveisViewController () <MKMapViewDelegate,CLLocationManagerDelegate>

@property (nonatomic, weak) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic) BOOL didUpdate;

@end


@implementation MapaImoveisViewController


#pragma mark - Queue

dispatch_queue_t queueLoadPin;


#pragma mark - Group Dispatch For Syncronize Add Pin

dispatch_group_t group_add_pin;


#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    queueLoadPin = dispatch_queue_create("MapaImoveisViewController.LoadPin", NULL);
    group_add_pin = dispatch_group_create();
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self pinImovel];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [Utils trackWithName:@"Mapa Imóveis"];
    [Utils trackFlurryTimedWithName:@"Mapa Imóveis"];
    
}


// Memory Leak Fix
// http://stackoverflow.com/questions/16420018/memory-not-being-released-for-mkmapview-w-arc
- (void)viewDidDisappear:(BOOL)animated
{
    
    [super viewDidDisappear:animated];
    
    // Switching map types causes cache purging, so switch to a different map type
    _mapView.mapType = MKMapTypeSatellite;
    _locationManager.delegate = nil;
    _locationManager = nil;
    _mapView.delegate = nil;
    [_mapView removeFromSuperview];
    _mapView = nil;
    
    [Flurry endTimedEvent:@"Mapa Imóveis" withParameters:nil];
}

//- (BOOL)prefersStatusBarHidden
//{
//    return YES;
//}



#pragma mark - Geolocation
- (void)pinImovel
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"((SUBQUERY(negociacao, $n, $n.status = %@).@count == 0) OR (ANY negociacao = nil))", NEGOCIACAO_FECHADA];
    
    NSArray *imoveis = [Imovel MR_findAllWithPredicate:predicate inContext:self.moc];
    
    for (Imovel *imovel in imoveis) {
        
        if((imovel.endereco.latitude == nil || [imovel.endereco.latitude doubleValue] == 0)
           && (imovel.endereco.longitude == nil || [imovel.endereco.longitude doubleValue] == 0)) {
            
            [self geocode:imovel];
            
        } else {
            
            CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([imovel.endereco.latitude doubleValue], [imovel.endereco.longitude doubleValue]);
            [self addAnnotation:coordinate withImovel:imovel];
        }
    }
    
    dispatch_group_notify(group_add_pin, dispatch_get_main_queue(), ^{
        // Ajusta o mapa de forma que todos os annotations fiquem visíveis.
        [self.mapView showAnnotations:self.mapView.annotations animated:YES];
    });
}


#pragma mark - Reverse Geocoding
- (void)geocode:(Imovel *)imovel
{
    NSString *address = [self getAddress:imovel];
    
    if (address == nil) {
        return;
    }
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    [geocoder geocodeAddressString:address completionHandler:^(NSArray *placemarks, NSError *error) {
        if(!error){
            
            CLPlacemark *placemark = [placemarks objectAtIndex:0];
            CLLocation *location = placemark.location;
            CLLocationCoordinate2D coordinate = location.coordinate;
            
            imovel.endereco.latitude = [NSNumber numberWithDouble:coordinate.latitude];
            imovel.endereco.longitude = [NSNumber numberWithDouble:coordinate.longitude];
            
            [imovel saveFirebase];
            
            [self saveMOC];
            
            [self addAnnotation:location.coordinate withImovel:imovel];
            
            // distance in meters vertically and horizontally to get the desired zoom
            //                        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coordinate, 5000, 5000);
            
            //                        MKCoordinateRegion adjustedRegion = [_mapView regionThatFits:region];
            //                        [_mapView setRegion:adjustedRegion animated:NO];
            
            //                        _mapView.centerCoordinate = location.coordinate;
            
        }
    }];
}


- (NSString *)getAddress:(Imovel *)imovel
{
    NSString *address = nil;
    
    if (imovel.endereco.logradouro != nil && imovel.endereco.logradouro.length > 5) {
        
        address = [NSString stringWithFormat:@"%@",imovel.endereco.logradouro];
        
        
        if (imovel.endereco.bairro.nome.length > 0) {
            address = [address stringByAppendingString:[NSString stringWithFormat:@", %@",imovel.endereco.bairro.nome]];
        }
        
        if (imovel.endereco.cidade.length > 0) {
            address = [address stringByAppendingString:[NSString stringWithFormat:@", %@",imovel.endereco.cidade]];
        }
        
        if (imovel.endereco.estado.length > 0) {
            address = [address stringByAppendingString:[NSString stringWithFormat:@", %@",imovel.endereco.estado]];
        }
        
        
        if ([imovel.endereco.cep intValue] > 0) {
            NSMutableString *cep = [NSMutableString stringWithFormat:@"%@",imovel.endereco.cep];
            [cep insertString:@"-" atIndex:5];
            address = [address stringByAppendingString:[NSString stringWithFormat:@" cep %@",cep]];
            
        }
        
    } else if ([imovel.endereco.cep intValue] > 0) {
        
        NSMutableString *cep = [NSMutableString stringWithFormat:@"%@",imovel.endereco.cep];
        [cep insertString:@"-" atIndex:5];
        address = [NSString stringWithFormat:@" cep %@",cep];
        
        if (imovel.endereco.cidade.length > 0) {
            address = [address stringByAppendingString:[NSString stringWithFormat:@", %@",imovel.endereco.cidade]];
        }
        
        return address;
    }
    
    
    
    return address;
}


#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    if ([view conformsToProtocol:@protocol(JPSThumbnailAnnotationViewProtocol)]) {
        [((NSObject<JPSThumbnailAnnotationViewProtocol> *)view) didSelectAnnotationViewInMap:mapView];
    }
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    if ([view conformsToProtocol:@protocol(JPSThumbnailAnnotationViewProtocol)]) {
        [((NSObject<JPSThumbnailAnnotationViewProtocol> *)view) didDeselectAnnotationViewInMap:mapView];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    if ([annotation conformsToProtocol:@protocol(JPSThumbnailAnnotationProtocol)]) {
        return [((NSObject<JPSThumbnailAnnotationProtocol> *)annotation) annotationViewInMap:mapView];
    }
    return nil;
}

//-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
//{
//    
//    if (!_didUpdate) {
//        _didUpdate = YES;
//        
//        MKCoordinateRegion mapRegion;
//        mapRegion.center = mapView.userLocation.coordinate;
//        mapRegion.span.latitudeDelta = 0.2;
//        mapRegion.span.longitudeDelta = 0.2;
//        
//        [mapView setRegion:mapRegion animated: YES];
//    }
//
//}
//
//-(void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views
//{
//    MKAnnotationView *ulv = [mapView viewForAnnotation:mapView.userLocation];
//    ulv.hidden = YES;
//}


#pragma mark - Close
- (IBAction)close:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Add Pin
- (void)addAnnotation:(CLLocationCoordinate2D)coordinate withImovel:(Imovel *)imovel
{
    JPSThumbnail *thumbnail = [[JPSThumbnail alloc] init];
    
    thumbnail.title = imovel.descricao;
    
    if (imovel.descricao.length == 0) {
        thumbnail.title = imovel.identificacao;
    }
    
    thumbnail.subtitle = imovel.endereco.logradouro;
    thumbnail.coordinate = coordinate;
    
    thumbnail.disclosureBlock = ^{
        [self openImovel:imovel];
    };
    
    NSArray *fotos = [imovel.fotos allObjects];
    
    __block FotoImovel *fotoImovel = [[fotos filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"capa = YES"]] firstObject];
    
    dispatch_group_enter(group_add_pin);
    
    dispatch_async(queueLoadPin, ^{
        
        UIImage *image = nil;
        
        if (fotos.count == 0) {
            image = [UIImage imageNamed:@"imovel-sem-foto-mapa"];
            
        } else {
            if (fotoImovel == nil) {
                fotoImovel = [fotos firstObject];
            }
            
            image = [fotoImovel getImageForFotoImovelWithMaxPixelSize:100];
            
//            UIImage *originalImage = [fotoImovel getImageForFotoImovelURL];
//            
//            if (originalImage) {
//                CGSize size = CGSizeMake(originalImage.size.width / 10, originalImage.size.height / 10);
//                image = [ImageUtils resize:originalImage scaledToSize:size];
//                originalImage = nil;
//            }
            
        }
        
        thumbnail.image = image;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [_mapView addAnnotation:[JPSThumbnailAnnotation annotationWithThumbnail:thumbnail]];
            dispatch_group_leave(group_add_pin);
        });
    });
}

- (void)openImovel:(Imovel *)imovel
{
    
    //    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
    //        NSArray *controllers = [self.navigationController viewControllers];
    //
    //        PortfolioViewController *controller = [controllers firstObject];
    //        controller.selectedImovel = imovel;
    //        [self.navigationController popToRootViewControllerAnimated:YES];
    //
    //    } else {
    
    self.selectedImovel = imovel;
    
    [self performSegueWithIdentifier:@"UnwindFromMapaImoveis" sender:nil];
    
    //    }
    
}

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end