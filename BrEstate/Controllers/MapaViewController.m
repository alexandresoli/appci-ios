//
//  MapaViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/5/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "MapaViewController.h"
#import "OpcoesMapaViewController.h"
#import "MapPin.h"
#import "Estado.h"
#import "Imovel+Firebase.h"
#import "Cliente+Firebase.h"
#import "Endereco.h"
#import <GoogleMaps/GoogleMaps.h>

@import MapKit;


enum MapType: NSUInteger {
    
    kMKMapTypeStandard  = 0,
    kMKMapTypeHybrid    = 1,
    kMKMapTypeSatellite = 2
};

@interface MapaViewController () <MKMapViewDelegate, OpcoesMapaDelegate,CLLocationManagerDelegate,UIPopoverControllerDelegate, UIActionSheetDelegate>

@property (nonatomic, weak) IBOutlet UIToolbar *toolBar;
@property (nonatomic, weak) IBOutlet UIView *mainView;
@property (nonatomic, weak) IBOutlet UISegmentedControl *typeSegment;

@property (nonatomic, strong) MKMapView *mapView;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLGeocoder *geocoder;
@property (nonatomic, assign) long selectedType;
@property (nonatomic, strong) UIStoryboardPopoverSegue *popoverSegue;

@property CLLocationCoordinate2D selectedCoordinate;
@property CLPlacemark *placemark;
@property GMSPanoramaView *panoView;
@property MKRoute *routeDetails;
@property (weak, nonatomic) IBOutlet UIButton *verRotaButton;
@property (weak, nonatomic) IBOutlet UIView *rotaView;
@property (nonatomic, weak) IBOutlet UILabel *distanceLabel;
@property (nonatomic, weak) IBOutlet UITextView *directionsTextView;

- (IBAction)segmentDirectionsChange:(UISegmentedControl *)segment;

@end

@implementation MapaViewController

@synthesize popoverSegue,selectedCoordinate;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [Utils trackWithName:@"Tela Mapa"];
    [Utils trackFlurryWithName:@"Tela Mapa"];

    _mapView = [[MKMapView alloc] initWithFrame:_mainView.bounds];
    _mapView.delegate = self;
    
    [_mainView addSubview:_mapView];
    
    if (_locationManager == nil) {
        _locationManager = [[CLLocationManager alloc] init];
        [_locationManager setDelegate:self];
        [_locationManager setDesiredAccuracy:kCLLocationAccuracyKilometer];
        
        // request user authorization
        if(IS_OS_8_OR_LATER) {
            [self.locationManager requestWhenInUseAuthorization];
            //            [self.locationManager requestAlwaysAuthorization];
        }
        
    }
    
    if (_geocoder == nil) {
        _geocoder = [[CLGeocoder alloc] init];
    }
    
    [self pinLocation];
    
    _toolBar.barStyle = UIBarStyleDefault;
    
}


// Memory Leak Fix
// http://stackoverflow.com/questions/16420018/memory-not-being-released-for-mkmapview-w-arc
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    // Switching map types causes cache purging, so switch to a different map type
    switch (_mapView.mapType) {
        case MKMapTypeSatellite:
            _mapView.mapType = MKMapTypeStandard;
            break;
        case MKMapTypeStandard:
            _mapView.mapType = MKMapTypeSatellite;
            break;
        case MKMapTypeHybrid:
            _mapView.mapType = MKMapTypeStandard;
            break;
        default:
            break;
    }
    
    _locationManager.delegate = nil;
    _locationManager = nil;
    _mapView.delegate = nil;
    [_mapView removeFromSuperview];
    _mapView = nil;
    //    [_mainView removeFromSuperview];
    //    _mainView = nil;
}


- (BOOL)prefersStatusBarHidden
{
    return YES;
}

-(BOOL)shouldAutorotate
{
    return NO;
}


#pragma mark - Geolocation
- (void)pinLocation
{
    if (_selectedImovel != nil) {
        [self pinImovel];
    } else if(_selectedContato != nil) {
        [self pinCliente];
    }
}

- (void)pinCliente
{
    
    Endereco *endereco  = _selectedContato.endereco;
    
    if((endereco.latitude == nil || [endereco.latitude doubleValue] == 0)
       && (endereco.longitude == nil || [endereco.longitude doubleValue] == 0)) {
        
        NSString *address = [NSString stringWithFormat:@"%@ - %@",endereco.logradouro, endereco.estado];
        [self geocodeFromAddress:address];
        
    } else {
        
        self.selectedCoordinate = CLLocationCoordinate2DMake([endereco.latitude doubleValue], [endereco.longitude doubleValue]);
        [self addAnnotation];
    }
    
    
    
}

- (void)pinImovel
{
    Endereco *endereco  = _selectedImovel.endereco;
    
    if((endereco.latitude == nil || [endereco.latitude doubleValue] == 0) && (endereco.longitude == nil || [endereco.longitude doubleValue] == 0)) {
        
        NSString *address = [NSString stringWithFormat:@"%@ - %@",_selectedImovel.endereco.logradouro,_selectedImovel.endereco.estado];
        [self geocodeFromAddress:address];
        
    } else {
        
        self.selectedCoordinate = CLLocationCoordinate2DMake([endereco.latitude doubleValue], [endereco.longitude doubleValue]);
        [self addAnnotation];
    }
    
    
    
}

#pragma mark - Reverse Geocoding
- (void)geocodeFromAddress:(NSString *)address
{
    [Utils showProgress:@"Obtendo geolocalização..."];
    
    [self.geocoder geocodeAddressString:address completionHandler:^(NSArray *placemarks, NSError *error) {
        
        [Utils dismissProgress];
        
        if ([placemarks count] > 0) {
            
            CLPlacemark *placemark = [placemarks objectAtIndex:0];
            CLLocation *location = placemark.location;
            
            selectedCoordinate = location.coordinate;
            _placemark = placemark;
            
            [self addAnnotation];
            
        } else {
            [Utils showMessage:@"Não foi possível obter a geolocalização. Verifique o endereço e tente novamente mais tarde."];
        }
    }];
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MKMapViewDelegate
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    
    // distance in meters vertically and horizontally to get the desired zoom
    //    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([userLocation coordinate], 500, 500);
    //    [mapView setRegion:region animated:YES];
    ;
    
    //    [_mapView selectAnnotation:[_mapView.annotations lastObject] animated:YES];
    
    //        _mapView.camera.altitude *= 1.4;
    
    
}


-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    if ([overlay isKindOfClass:[MKCircle class]]) {
        
        MKCircle *circle = (MKCircle *)overlay;
        
        MKCircleRenderer *circleRenderer = [[MKCircleRenderer alloc] initWithCircle:circle];
        circleRenderer.fillColor = [[UIColor blueColor] colorWithAlphaComponent:0.2];
        circleRenderer.strokeColor = [[UIColor blueColor] colorWithAlphaComponent:0.7];
        circleRenderer.lineWidth = 3;
        
        return circleRenderer;
        
    } else {
        
        MKPolylineRenderer *routeLineRenderer = [[MKPolylineRenderer alloc] initWithPolyline:_routeDetails.polyline];
        routeLineRenderer.fillColor = [[UIColor blueColor] colorWithAlphaComponent:0.2];
        routeLineRenderer.strokeColor = [[UIColor blueColor] colorWithAlphaComponent:0.7];
        routeLineRenderer.lineWidth = 5;
        return routeLineRenderer;
        
        
    }
    
    return nil;
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    // If it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    // Handle any custom annotations.
    if ([annotation isKindOfClass:[MKPointAnnotation class]]) {
        // Try to dequeue an existing pin view first.
        MKPinAnnotationView *pinView = (MKPinAnnotationView*)[_mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        if (!pinView)
        {
            // If an existing pin view was not available, create one.
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
            pinView.canShowCallout = YES;
        } else {
            pinView.annotation = annotation;
        }
        return pinView;
    }
    return nil;
}

- (void)mapView:(MKMapView *)mv didAddAnnotationViews:(NSArray *)views
{
    MKAnnotationView *annotationView = [views objectAtIndex:0];
    id <MKAnnotation> mp = [annotationView annotation];
    
    if (![mp isKindOfClass:[MKUserLocation class]]) {
        
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([mp coordinate], 1000, 1000);
        [mv setRegion:region animated:YES];
    }
    
    [mv selectAnnotation:mp animated:YES];
    
    
}


#pragma mark - Close
- (IBAction)close:(id)sender
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [popoverSegue.popoverController dismissPopoverAnimated:YES];
        [self.navigationController popViewControllerAnimated:YES];
        
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"OpcoesMapaSegue"]) {
        
        popoverSegue  = (UIStoryboardPopoverSegue *)segue;
        popoverSegue.popoverController.delegate = self;
        
        OpcoesMapaViewController  *opcoesMapaController = segue.destinationViewController;
        opcoesMapaController.selectedType = _selectedType;
        opcoesMapaController.delegate = self;
        
    }
}

#pragma mark - Change Map Type
- (void)changeMapType:(long)type
{
    [popoverSegue.popoverController dismissPopoverAnimated:YES];
    
    // Street
    if (type == 3) {
        // Plano TOP
        if (![self.iapHelper checkPlanTopFromController:self withMessage:PLAN_TOP_MESSAGE_STREET_VIEW]) {
            return;
        }
    }
    
    
    _selectedType = type;
    
    [_mapView removeOverlays:_mapView.overlays];
    [_mapView removeAnnotations:_mapView.annotations];
    [_mapView removeFromSuperview];
    [_panoView removeFromSuperview];
    
    _mapView.showsUserLocation = NO;
    
    
    [self showView:NO];
    
    
    [UIView animateWithDuration:0.5 animations:^{
        _mapView.frame = _mainView.bounds;
    }];
    
    
    
    switch (type)
    {
        case kMKMapTypeStandard:
            
            [Utils trackWithName:@"Mudou para mapa padrao"];
            [Utils trackFlurryWithName:@"Mudou para mapa padrao"];

            _toolBar.barStyle = UIBarStyleDefault;
            _mapView.mapType = MKMapTypeStandard;
            
            [self checkMapSuperview];
            
            [self addAnnotation];
            [self addCircleOverlay];
            
            break;
        case 1:
            
            [Utils trackWithName:@"Mudou para mapa hibrido"];
            [Utils trackFlurryWithName:@"Mudou para mapa hibrido"];

            _toolBar.barStyle = UIBarStyleBlackTranslucent;
            _mapView.mapType = MKMapTypeHybrid;
            
            [self checkMapSuperview];
            
            [self addAnnotation];
            [self addCircleOverlay];
            
            break;
            
        case 2:
            
            [Utils trackWithName:@"Mudou para mapa satelite"];
            [Utils trackFlurryWithName:@"Mudou para mapa satelite"];

            _toolBar.barStyle = UIBarStyleBlackTranslucent;
            _mapView.mapType = MKMapTypeSatellite;
            
            [self checkMapSuperview];
            
            [self addAnnotation];
            [self addCircleOverlay];
            
            break;
            
        case 3:
            
        {
            [Utils showProgress:@"Carregando Street View..."];
            
            [Utils trackWithName:@"Mudou para street view"];
            [Utils trackFlurryWithName:@"Mudou para street view"];

            GMSPanoramaService *panoramaService = [[GMSPanoramaService alloc] init];
            [panoramaService requestPanoramaNearCoordinate: selectedCoordinate callback: ^(GMSPanorama *panorama, NSError *error) {
                
                [Utils dismissProgress];
                
                if (error || !panorama) {
                    [Utils showMessage:@"Não foi possível abrir o Street View para este endereço."];
                    [self changeMapType:kMKMapTypeStandard];
                    
                } else {
//                    _panoView = [GMSPanoramaView panoramaWithFrame:_mainView.bounds nearCoordinate:selectedCoordinate];
                    _panoView = [[GMSPanoramaView alloc] initWithFrame:_mainView.bounds];
                    _panoView.panorama = panorama;
                    
                    [_panoView setZoomGestures:YES];
                    
                    if (_panoView.superview == nil) {
                        [_mainView addSubview:_panoView];
                    }
                }
            }];
            
            break;
        }
        case 4:
        {
            
            [Utils trackWithName:@"Mudou para rota no mapa"];
            [Utils trackFlurryWithName:@"Mudou para rota no mapa"];

            _toolBar.barStyle = UIBarStyleDefault;
            _mapView.mapType = MKMapTypeStandard;
            
            [self showView:YES];
            _mapView.showsUserLocation = YES;
            [_mapView.userLocation setTitle:@"Sua localicação"];
            
            
            [self checkMapSuperview];
            
            [self addAnnotation];
            
            [self traceRoute:MKDirectionsTransportTypeAutomobile];
            
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                [UIView animateWithDuration:0.5 animations:^{
                    _mapView.frame = CGRectMake(350, 0, 672, 724);
                }];
            }
            
            break;
        }
            
        default:
            break;
    }
}

- (void)checkMapSuperview
{
    if (_mapView.superview == nil) {
        _mapView.delegate = self;
        [_mainView addSubview:_mapView];
    }
}

- (void)addCircleOverlay
{
    MKCircle *circle = [MKCircle circleWithCenterCoordinate:selectedCoordinate radius:100];
    [_mapView addOverlay:circle];
}


#pragma mark - iPhone Show Map Type

- (IBAction)showMapType:(id)sender
{
    self.verRotaButton.hidden = YES;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Opções de mapa" delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:nil otherButtonTitles:@"Padrão", @"Híbrido", @"Satélite", @"Street", @"Rota", nil];
    
    [actionSheet showInView:self.view];
    
}


- (IBAction)showRota:(id)sender
{
    CGRect startRect = self.rotaView.frame;
    CGRect endRect = self.rotaView.frame;
    
    startRect.origin.y = 568;
    endRect.origin.y = 0;
    
    self.rotaView.frame = startRect;
    
    [self.view bringSubviewToFront:self.rotaView];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.rotaView.hidden = NO;
        self.rotaView.frame = endRect;
    }];
}


- (IBAction)closeRota:(id)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect endRect = self.rotaView.frame;
        endRect.origin.y = 568;
        
        self.rotaView.frame = endRect;
        
    } completion:^(BOOL finished) {
        self.rotaView.hidden = YES;
    }];
    
}


#pragma mark - iPhone UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([actionSheet.title isEqualToString:@"Opções de mapa"]) {
        switch (buttonIndex) {
            case 0: // Padrão
                [self changeMapType:kMKMapTypeStandard];
                break;
                
            case 1: // Híbrido
                [self changeMapType:1];
                break;
                
            case 2: // Satélite
                [self changeMapType:2];
                break;
                
            case 3: // Street
                [self changeMapType:3];
                break;
                
            case 4: // Rota
                [self changeMapType:4];
                self.verRotaButton.hidden = NO;
                break;
                
            default:
                break;
        }
    }
}


#pragma mark - Hide/Show views
- (void)showView:(BOOL)show
{
    [UIView animateWithDuration:1.0 animations:^{
        _distanceLabel.alpha = (show == YES) ? 1.0 : 0.0;
        _directionsTextView.alpha = (show == YES) ? 1.0 : 0.0;
        _typeSegment.alpha = (show == YES) ? 1.0 : 0.0;
    }];
}

#pragma mark - Add Pin
- (void)addAnnotation
{
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(self.selectedCoordinate, 1000, 1000);
    
    MKCoordinateRegion adjustedRegion = [_mapView regionThatFits:region];
    [_mapView setRegion:adjustedRegion animated:YES];
    
    _mapView.centerCoordinate = self.selectedCoordinate;
    
    
    [self addCircleOverlay];
    
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = self.selectedCoordinate;
    
    NSNumber *latitude = [NSNumber numberWithDouble:self.selectedCoordinate.latitude];
    NSNumber *longitude = [NSNumber numberWithDouble:self.selectedCoordinate.longitude];
    
    if (_selectedContato.endereco != nil) {
        
        if ([_selectedContato.endereco.latitude compare:latitude] != NSOrderedSame || [_selectedContato.endereco.longitude compare:longitude] != NSOrderedSame ) {
            
            _selectedContato.endereco.latitude = [NSNumber numberWithDouble:self.selectedCoordinate.latitude];
            _selectedContato.endereco.longitude = [NSNumber numberWithDouble:self.selectedCoordinate.longitude];
            
            [self saveMOC];
            [_selectedContato saveFirebase];
        }
        
        point.title = _selectedContato.endereco.logradouro;
        point.subtitle = _selectedContato.endereco.cidade;
    }
    
    if (_selectedImovel.endereco != nil) {
        
        if ([_selectedImovel.endereco.latitude compare:latitude] != NSOrderedSame || [_selectedImovel.endereco.longitude compare:longitude] != NSOrderedSame) {
            
            _selectedImovel.endereco.latitude = [NSNumber numberWithDouble:self.selectedCoordinate.latitude];
            _selectedImovel.endereco.longitude = [NSNumber numberWithDouble:self.selectedCoordinate.longitude];
            
            [self saveMOC];
            [_selectedImovel saveFirebase];
        }
        
        point.title = _selectedImovel.endereco.logradouro;
        point.subtitle = _selectedImovel.endereco.cidade;
        
    }
    
    [_mapView addAnnotation:point];
    
}

#pragma mark - Trace Route
- (void)traceRoute:(MKDirectionsTransportType)type
{
    if (_placemark != nil) {
        
        [self doTrace:type];
        
    } else {
        
        [Utils showProgress:@"Aguarde..."];
        NSString *address = [NSString stringWithFormat:@"%@ - %@",_selectedImovel.endereco.logradouro, _selectedImovel.endereco.estado];
        
        [self.geocoder geocodeAddressString:address completionHandler:^(NSArray *placemarks, NSError *error) {
            
            if ([placemarks count] > 0) {
                
                _placemark = [placemarks objectAtIndex:0];
                
                [self doTrace:type];
                
            }
        }];
    }
}

- (void)doTrace:(MKDirectionsTransportType)type
{
    MKDirectionsRequest *directionsRequest = [[MKDirectionsRequest alloc] init];
    
    MKPlacemark *mark = [[MKPlacemark alloc] initWithPlacemark:_placemark];
    
    [directionsRequest setSource:[MKMapItem mapItemForCurrentLocation]];
    [directionsRequest setDestination:[[MKMapItem alloc] initWithPlacemark:mark]];
    directionsRequest.transportType = type;
    MKDirections *directions = [[MKDirections alloc] initWithRequest:directionsRequest];
    
    
    __block NSMutableAttributedString *instructions = [NSMutableAttributedString new];
    
    [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        if (error) {
            NSLog(@"Error on MapaViewController traceRoute - %@", error.description);
        } else {
            
            _routeDetails = response.routes.lastObject;
            [_mapView addOverlay:_routeDetails.polyline];
            
            for (int i = 0; i < _routeDetails.steps.count; i++) {
                MKRouteStep *step = [_routeDetails.steps objectAtIndex:i];
                
                
                // distance
                
                //1 KM = 1000 metros
                //1 Milha = 1609.344 metros
                //1 Metro = 0.000621371192 milhas
                
                double meters = step.distance;
                double km = meters/1000;
                
                
                NSString *text;
                
                if (km <= 1) {
                    
                    if (meters > 0) {
                        text = [NSString stringWithFormat:@"Em %d metros", (int)meters];
                    }
                    
                } else {
                    text =  [NSString stringWithFormat:@"Em %d Km", (int)km];
                }
                
                
                // instructions
                NSString *instruction;
                
                if (meters == 0) {
                    instruction = [NSString stringWithFormat:@"%d. %@\n\n",i+1, step.instructions];
                } else {
                    instruction = [NSString stringWithFormat:@"%d. %@ - %@\n\n",i+1, text,step.instructions];
                }
                
                
                NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:instruction];
                [str addAttribute:NSFontAttributeName
                            value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15]
                            range:NSMakeRange(0, 2)];
                
                [str addAttribute:NSFontAttributeName
                            value:[UIFont fontWithName:@"HelveticaNeue" size:15]
                            range:NSMakeRange(2, instruction.length-4)];
                
                
                [instructions appendAttributedString:str];
                
            }
            
            if(_routeDetails.distance <= 1000) {
                _distanceLabel.text = [NSString stringWithFormat:@"Distância Total: %d metros", (int)_routeDetails.distance];
            } else {
                _distanceLabel.text = [NSString stringWithFormat:@"Distância Total: %d Km", (int)_routeDetails.distance/1000];
            }
            
            _directionsTextView.attributedText = instructions;
        }
        
        [self showView:YES];
        
        [_mapView showAnnotations:_mapView.annotations animated:YES];
        [Utils dismissProgress];
        
    }];
    
}

#pragma mark - Segment
- (IBAction)segmentDirectionsChange:(UISegmentedControl *)segment
{
    
    [_mapView removeOverlays:_mapView.overlays];
    
    switch (segment.selectedSegmentIndex) {
        case 0:
            [self traceRoute:MKDirectionsTransportTypeAutomobile];
            break;
        case 1:
            [self traceRoute:MKDirectionsTransportTypeWalking];
            break;
            
        default:
            break;
    }
}



@end