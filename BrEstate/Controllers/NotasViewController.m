//
//  NotasViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 2/6/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "NotasViewController.h"
#import "ListaNotasViewController.h"

@interface NotasViewController ()

@property (nonatomic, strong) ListaNotasViewController *listaNotasViewController;

@end

@implementation NotasViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

//    NSLog(@"children : %@", self.childViewControllers);
    self.listaNotasViewController = [self.childViewControllers objectAtIndex:0];
    self.listaNotasViewController.detalhesNotasViewController = [self.childViewControllers objectAtIndex:1];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    [Utils trackWithName:@"Aba Notas"];
//    [Utils trackFlurryWithName:@"Aba Notas"];

    [self configureAlert];
}

@end
