//
//  OpcoesMapaViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/5/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>
@import MapKit;

@protocol OpcoesMapaDelegate <NSObject>

@required
- (void)changeMapType:(long)type;

@end


@interface OpcoesMapaViewController : BaseViewController

@property (nonatomic, weak) id<OpcoesMapaDelegate> delegate;

@property (nonatomic, assign) long selectedType;

@end
