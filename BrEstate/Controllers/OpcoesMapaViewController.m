//
//  OpcoesMapaViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/5/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "OpcoesMapaViewController.h"

@interface OpcoesMapaViewController () <UIPopoverControllerDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UISegmentedControl *segmentControl;

@end

@implementation OpcoesMapaViewController

@synthesize segmentControl,selectedType,tableView,delegate;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    segmentControl.selectedSegmentIndex = selectedType;
    
    [Utils trackWithName:@"Tela Opcoes Mapa"];
    [Utils trackFlurryWithName:@"Tela Opcoes Mapa"];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changeSegment:(UISegmentedControl *)segment
{
    [self changeMapType:segment.selectedSegmentIndex];
}

- (void)changeMapType:(long)type
{
    [delegate changeMapType:type];
}

@end
