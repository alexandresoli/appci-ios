//
//  OpcoesViewController.m
//  Appci
//
//  Created by BrEstate LTDA on 25/04/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "OpcoesViewController.h"
#import "AddressBookHelper.h"
#import "Hp12cViewController.h"
#import "CustomCalendar.h"


@interface OpcoesViewController ()

@property (nonatomic, weak) IBOutlet UIImageView *syncImageView;
@property (weak, nonatomic) IBOutlet UITableViewCell *gerenciarUsuariosCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *planoTopCell;

@end


@implementation OpcoesViewController


#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    if (![Utils valueFromDefaults:FIREBASE_ALREADY_DOWNLOAD_ALL] && [[Utils iapHelper] checkPlanTopFromController:nil withMessage:nil]) {
//        [Utils showMessage:@"Seus dados estão sendo sincronizados. Aguarde o término do processo."];
//        [self dismissViewControllerAnimated:YES completion:nil];
//    }
    
//    [self configSync];
    
//    NSString *role = [Utils valueFromDefaults:CURRENT_ROLE];
//    
//    if (role) {
//        self.planoTopCell.hidden = YES;
//        
//        if ([role isEqualToString:@"Administrador"]) {
//            self.gerenciarUsuariosCell.hidden = NO;
//        }
//    }
}


#pragma mark - IBActions

- (IBAction)cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.hidden) {
        return 0;
        
    } else {
        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.tag == 5000) {
        
        if (![Utils valueFromDefaults:FIREBASE_ALREADY_DOWNLOAD_ALL] && [[Utils iapHelper] checkPlanTopFromController:nil withMessage:nil]) {
            [Utils showMessage:@"Seus dados estão sendo sincronizados. Aguarde o término do processo."];
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            return;
        }
        
        [Utils showProgress:@"Importando Contatos"];
        [self performSelector:@selector(checkContact) withObject:nil afterDelay:0.2];
        
    } else if (cell.tag == 6000) {
        
        if (![Utils valueFromDefaults:FIREBASE_ALREADY_DOWNLOAD_ALL] && [[Utils iapHelper] checkPlanTopFromController:nil withMessage:nil]) {
            [Utils showMessage:@"Seus dados estão sendo sincronizados. Aguarde o término do processo."];
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            return;
        }
        
        [Utils showProgress:@"Importando Eventos"];
        [self performSelector:@selector(checkEvents) withObject:nil afterDelay:0.2];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - Check contact and events

- (void)checkContact
{
    AddressBookHelper *addressBookHelper = [[AddressBookHelper alloc] initWithMoc:self.moc];
    [addressBookHelper checkContactsShowReturnMessage:YES];
}

- (void)checkEvents
{
    [CustomCalendar importEventsShowReturnMessage:YES];
}


#pragma mark - Segue

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if (![Utils valueFromDefaults:FIREBASE_ALREADY_DOWNLOAD_ALL] && [[Utils iapHelper] checkPlanTopFromController:nil withMessage:nil]) {
        
        if ([identifier isEqualToString:@"SyncSegue"]) {
            return YES;
            
        } else {
            [Utils showMessage:@"Seus dados estão sendo sincronizados. Aguarde o término do processo."];
            return NO;
        }
    }
    
    return YES;
}


@end
