//
//  OrganizarContatosViewController.h
//  Appci
//
//  Created by BrEstate LTDA on 22/05/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Cliente.h"

@interface OrganizarContatosViewController : BaseViewController

@property (nonatomic, strong) Cliente *selectedContato;

@end
