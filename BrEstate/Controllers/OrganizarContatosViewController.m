//
//  OrganizarContatosViewController.m
//  Appci
//
//  Created by BrEstate LTDA on 22/05/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "OrganizarContatosViewController.h"
#import "ContatoCellNoSWTable.h"
#import "Cliente+Utils.h"
#import "SubtipoCliente.h"
#import "TipoCliente.h"
#import "AddressBookHelper.h"
#import "ClienteDeletado+Create.h"
#import "Cliente+Firebase.h"
#import "ClienteDeletado+Firebase.h"

@interface OrganizarContatosViewController () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *comprarButton;
@property (weak, nonatomic) IBOutlet UIButton *alugarButton;
@property (weak, nonatomic) IBOutlet UIButton *vendedorButton;
@property (weak, nonatomic) IBOutlet UIButton *locadorButton;
@property (weak, nonatomic) IBOutlet UIButton *parceiroButton;
@property (weak, nonatomic) IBOutlet UIButton *fornecedorButton;
@property (weak, nonatomic) IBOutlet UIButton *pessoalButton;
@property (weak, nonatomic) IBOutlet UIButton *outrosButton;
@property (weak, nonatomic) IBOutlet UISearchBar *contatoSearchBar;
@property (weak, nonatomic) IBOutlet UIButton *excluirButton;
@property (weak, nonatomic) IBOutlet UIButton *limparButton;

@property (nonatomic, strong) NSCache *fotosCache;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end


@implementation OrganizarContatosViewController


#pragma mark - View Life Cycle

- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    
    [Utils trackWithName:@"Organizar Contatos"];
    [Utils trackFlurryTimedWithName:@"Organizar Contatos"];
    
    // border around
    self.comprarButton.layer.borderColor = [UIColor clearColor].CGColor;
    self.comprarButton.layer.borderWidth = 2.0;
    self.comprarButton.layer.cornerRadius = 8.0;
    
    self.alugarButton.layer.borderColor = [UIColor clearColor].CGColor;
    self.alugarButton.layer.borderWidth = 2.0;
    self.alugarButton.layer.cornerRadius = 8.0;
    
    self.vendedorButton.layer.borderColor = [UIColor clearColor].CGColor;
    self.vendedorButton.layer.borderWidth = 2.0;
    self.vendedorButton.layer.cornerRadius = 8.0;
    
    self.locadorButton.layer.borderColor = [UIColor clearColor].CGColor;
    self.locadorButton.layer.borderWidth = 2.0;
    self.locadorButton.layer.cornerRadius = 8.0;
    
    self.parceiroButton.layer.borderColor = [UIColor clearColor].CGColor;
    self.parceiroButton.layer.borderWidth = 2.0;
    self.parceiroButton.layer.cornerRadius = 8.0;
    
    self.fornecedorButton.layer.borderColor = [UIColor clearColor].CGColor;
    self.fornecedorButton.layer.borderWidth = 2.0;
    self.fornecedorButton.layer.cornerRadius = 8.0;
    
    self.pessoalButton.layer.borderColor = [UIColor clearColor].CGColor;
    self.pessoalButton.layer.borderWidth = 2.0;
    self.pessoalButton.layer.cornerRadius = 8.0;
    
    self.outrosButton.layer.borderColor = [UIColor clearColor].CGColor;
    self.outrosButton.layer.borderWidth = 2.0;
    self.outrosButton.layer.cornerRadius = 8.0;
    
    [self.tableView setEditing:YES animated:YES];

}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [Flurry endTimedEvent:@"Organizar Contatos" withParameters:nil];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
    if (self.selectedContato) {
        [self selectRow];
    }
}


- (NSCache *)fotosCache
{
    if (_fotosCache) {
        return _fotosCache;
    }
    
    _fotosCache = [[NSCache alloc] init];
    [_fotosCache setCountLimit:30];
    
    return _fotosCache;
}


#pragma mark - Select Specific Row

- (void)selectRow
{
    NSIndexPath *indexPath = [self.fetchedResultsController indexPathForObject:self.selectedContato];
    
    // trigger the selection
    [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionTop];
    
    [self.tableView.delegate tableView:self.tableView didSelectRowAtIndexPath:indexPath];
    
}


#pragma mark - NSFetchedResultsControllerDelegate

- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    _fetchedResultsController.delegate = self;
    
    _fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithAlphabetic:@"Cliente" andKey:@"nome" withPredicate:nil withOrder:YES withLimit:0 inContext:self.moc];
    
    return _fetchedResultsController;
}


#pragma mark - UITableViewDataSource, UITableViewDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ContatoCell";
    ContatoCellNoSWTable *cell = (ContatoCellNoSWTable *) [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;;
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    cell.selectedBackgroundView = bgColorView;
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


- (void)configureCell:(ContatoCellNoSWTable *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Cliente *cliente = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.nome.text = cliente.nome;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",cliente.subtipo];
    SubtipoCliente *subtipo = [SubtipoCliente MR_findFirstWithPredicate:predicate inContext:self.moc];
    
    // telefone
    NSString *telefone = cliente.telefone;
    
    if (telefone.length == 0) {
        telefone = @" - ";
    }
    
    if (cliente.telefoneExtra.length > 0) {
        telefone = [telefone stringByAppendingString:[NSString stringWithFormat:@" / %@",cliente.telefoneExtra]];
    }
    
    cell.telefone.text = telefone;
    
    
    // email
    if (cliente.email.length == 0) {
        cell.email.text = @" - ";
    } else {
        cell.email.text = cliente.email;
    }
    
    
    // Foto.
    cell.fotoLabel.text = cliente.iniciaisNome;
    cell.fotoLabel.hidden = NO;
    
    if (cliente.foto == nil) {
        cell.fotoImageView.image = nil;
        
    } else {
        cell.fotoImageView.layer.cornerRadius = cell.fotoImageView.frame.size.height /2;
        cell.fotoImageView.layer.masksToBounds = YES;
        cell.fotoImageView.layer.borderWidth = 0;
        cell.fotoImageView.image = [self.fotosCache objectForKey:indexPath];
        
        if (cell.fotoImageView.image) {
            cell.fotoLabel.hidden = YES;
            
        } else {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                UIImage *image = [UIImage imageWithData:cliente.foto];
                
                [self.fotosCache setObject:image forKey:indexPath];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    ContatoCellNoSWTable *updateCell = (ContatoCellNoSWTable *)[self.tableView cellForRowAtIndexPath:indexPath];
                    
                    [UIView transitionWithView:cell.fotoImageView
                                      duration:0.2f
                                       options:UIViewAnimationOptionTransitionCrossDissolve
                                    animations:^{
                                        
                                        updateCell.fotoLabel.hidden = YES;
                                        updateCell.fotoImageView.image = image;
                                        
                                    } completion:NULL];
                });
            });
        }
    }
    
    // tipo
    if (subtipo.tipoCliente.descricao.length > 0) {
        cell.subTipo.hidden = NO;
        cell.tipoImageView.hidden = NO;
        
        // border around label
        cell.subTipo.layer.borderColor = [UIColor whiteColor].CGColor;
        cell.subTipo.layer.borderWidth = 2.0;
        cell.subTipo.layer.cornerRadius = 8.0;
        
        cell.subTipo.text = subtipo.descricao;
        cell.subTipo.textColor = [UIColor whiteColor];
        
        if ([subtipo.tipoCliente.descricao isEqualToString:@"Proprietário"]) {
            cell.tipoImageView.image = [UIImage imageNamed:@"contato-p"];
            cell.subTipo.backgroundColor = [UIColor colorWithRed:77.0/255 green:219.0/255 blue:109.0/255 alpha:1];
        } else if ([subtipo.tipoCliente.descricao isEqualToString:@"Interessado"]) {
            cell.tipoImageView.image = [UIImage imageNamed:@"contato-i"];
            cell.subTipo.backgroundColor = [UIColor colorWithRed:252.0/255 green:150.0/255 blue:42.0/255 alpha:1];
        } else {
            cell.tipoImageView.image = [UIImage imageNamed:@"contato-o"];
            cell.subTipo.backgroundColor = [UIColor colorWithRed:255.0/255 green:44.0/255 blue:85.0/255 alpha:1];
        }
        
    } else {
        cell.subTipo.hidden = YES;
        cell.tipoImageView.hidden = YES;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContatoCellNoSWTable *cell = (ContatoCellNoSWTable *) [self.tableView cellForRowAtIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    [self updateExcluirButtonTitle];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self updateExcluirButtonTitle];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}


- (NSArray *) sectionIndexTitlesForTableView: (UITableView *) tableView
{
    return [self.fetchedResultsController sectionIndexTitles];
}


- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [self.fetchedResultsController sectionForSectionIndexTitle:title atIndex:index];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *labelView = [[UILabel alloc] init];
    labelView.textAlignment = NSTextAlignmentLeft;
    labelView.backgroundColor = [UIColor colorWithRed:0.851 green:0.851 blue:0.851 alpha:1];
    labelView.textColor = [UIColor blackColor];
    labelView.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
    
    NSString *headerText = [[self.fetchedResultsController sectionIndexTitles] objectAtIndex:section];
    labelView.text = [NSString stringWithFormat:@"   %@",headerText];
    
    return labelView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}


#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    [self.tableView reloadRowsAtIndexPaths:[self.tableView indexPathsForSelectedRows] withRowAnimation:UITableViewRowAnimationNone];
    
    searchBar.showsCancelButton = YES;
    
    [self updateExcluirButtonTitle];
    
    return YES;
}


-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self filterContentForSearchText:searchText];
}


-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self filterContentForSearchText:searchBar.text];
    searchBar.showsCancelButton = NO;
    [searchBar resignFirstResponder];
}


- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    searchBar.text = nil;
    searchBar.showsCancelButton = NO;
    [self filterContentForSearchText:nil];
    
    [searchBar resignFirstResponder];
}


- (void)filterContentForSearchText:(NSString*)searchText
{
    NSString *query = searchText;
    
    if (query && query.length) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(nome contains[c] %@ OR email contains[c] %@ OR subtipo.descricao contains[c] %@ OR endereco.cep contains[c] %@ OR endereco.logradouro contains[c] %@ OR endereco.complemento contains[c] %@ OR endereco.estado contains[c] %@ OR endereco.cidade contains[c] %@ OR endereco.bairro.nome contains[c] %@)", query,query,query,query,query,query,query,query,query];
        
        [self.fetchedResultsController.fetchRequest setPredicate:predicate];
        [self.fetchedResultsController.fetchRequest setFetchLimit:100]; // Optional, but with large datasets - this helps speed lots
        
    } else {
        
        NSPredicate *predicate = nil;
        self.fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithAlphabetic:@"Cliente" andKey:@"nome" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
        
    }
    
    [self.fetchedResultsController performFetch:nil];
    
    [self.fotosCache removeAllObjects];
    [self.tableView reloadData];
    
    [self updateExcluirButtonTitle];
}


#pragma mark - Update Excluir Button Title

- (void)updateExcluirButtonTitle
{
    // Update the delete button's title, based on how many items are selected
    NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
    
    if (selectedRows.count == 0) {
        [self.excluirButton setTitle:@"Excluir" forState:UIControlStateNormal];
        self.limparButton.enabled = NO;
        
    } else {
        [self.excluirButton setTitle:[NSString stringWithFormat:@"Excluir (%lu)", (unsigned long)selectedRows.count] forState:UIControlStateNormal];
        self.limparButton.enabled = YES;
    }
}


#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        
        NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
        
        for (NSIndexPath *indexPath in selectedRows) {
            Cliente *cliente = [self.fetchedResultsController objectAtIndexPath:indexPath];
            
            // Parse Delete
//            [cliente removeFromAllEventos];
//            [ParseUtils deleteEntity:cliente withName:@"Cliente" usingMoc:self.moc];
            
            // Grava o contato na tabela de deletado para não ser mais importado.
            ClienteDeletado *clienteDeletado = [ClienteDeletado createWithCliente:cliente andContext:self.moc];
            
            // Firebase
            [clienteDeletado saveFirebase];
//            [clienteDeletado saveParse:self.moc];
            
            // Firebase
            [cliente deleteFirebase];
            
            [cliente MR_deleteInContext:self.moc];
        }
        
        [self.moc MR_saveToPersistentStoreAndWait];
        
        [self.fetchedResultsController performFetch:nil];
        
        [self.fotosCache removeAllObjects];
        [self.tableView reloadData:YES];
        
        [self updateExcluirButtonTitle];
        
        [Utils postEntitytNotification:@"Contato"];
    }
}


#pragma mark - IBActions

- (IBAction)changeSubtipoCliente:(UIButton *)sender
{
    NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
    
    if (selectedRows.count == 0) {
        [Utils showMessage:@"Selecione algum contato"];
        
    } else {
        for (NSIndexPath *indexPath in selectedRows) {
            Cliente *cliente = [self.fetchedResultsController objectAtIndexPath:indexPath];
            
            cliente.subtipo = [NSNumber numberWithInteger:sender.tag];
            cliente.version = [NSDate date];
            
            // Firebase
            [cliente saveFirebase];
            
            [self.moc MR_saveToPersistentStoreAndWait];

            // parse sdk
//            [cliente saveParse:self.moc];

        }
        
        [self.tableView reloadData];
        
        [self updateExcluirButtonTitle];
        
        [Utils postEntitytNotification:@"Contato"];
    }
}


- (IBAction)deleteCliente:(id)sender
{
    NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
    
    if (selectedRows.count == 0) {
        [Utils showMessage:@"Selecione algum contato"];
        
    } else {
        UIActionSheet *sheet =[[UIActionSheet alloc] initWithTitle:@"Excluir os contatos selecionados?"
                                                          delegate:self
                                                 cancelButtonTitle:@"Cancelar"
                                            destructiveButtonTitle:@"Excluir" otherButtonTitles:nil];
        
        [sheet showInView:self.excluirButton];
    }
}


- (IBAction)cleanSelection:(id)sender
{
    [self.tableView reloadData];
    
    [self updateExcluirButtonTitle];
}


- (IBAction)selectAll:(id)sender
{
    [Utils showProgress:@"Selecionando..."];
    
    [self performSelector:@selector(selectAllRows) withObject:nil afterDelay:0.2];
}


- (void)selectAllRows
{
    for (int i = 0; i < [self.tableView numberOfSections]; i++) {
        for (int j = 0; j < [self.tableView numberOfRowsInSection:i]; j++) {
            
            NSUInteger ints[2] = {i,j};
            
            NSIndexPath *indexPath = [NSIndexPath indexPathWithIndexes:ints length:2];
            
            [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
            
            [self.tableView.delegate tableView:self.tableView didSelectRowAtIndexPath:indexPath];
        }
    }
    
    [Utils dismissProgress];
}


- (IBAction)done:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
