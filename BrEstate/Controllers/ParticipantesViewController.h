//
//  AdicionarClientesViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/7/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Imovel;
@interface ParticipantesViewController : BaseViewController

@property (nonatomic, strong) NSMutableArray *selectedClientes;
@property (nonatomic, strong) Imovel *selectedImovel;
@end
