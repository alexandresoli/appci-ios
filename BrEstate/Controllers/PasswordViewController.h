//
//  PasswordViewController.h
//  Appci
//
//  Created by BrEstate LTDA on 8/25/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PasswordViewController : BaseViewController

@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *loginUser;
@property (nonatomic) BOOL changePassword;

@end
