//
//  PasswordViewController.m
//  Appci
//
//  Created by BrEstate LTDA on 8/25/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "PasswordViewController.h"
#import "FirebaseUtilsUser.h"
#import "Corretor+Utils.h"

@interface PasswordViewController () <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField *password1;
@property (nonatomic, weak) IBOutlet UITextField *password2;
@property (weak, nonatomic) IBOutlet UILabel *tituloLabel;
@property (weak, nonatomic) IBOutlet UIButton *button;

@end


@implementation PasswordViewController


#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];

}


// http://stackoverflow.com/questions/3372333/ipad-keyboard-will-not-dismiss-if-modal-viewcontroller-presentation-style-is-uim
- (BOOL)disablesAutomaticKeyboardDismissal
{
    return NO;
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.changePassword) {
        //TODO: mudar label botão
        _password1.text = nil;
        _password2.text = nil;
        [_button setTitle:@"Alterar" forState:UIControlStateNormal];
        [_button setTitle:@"Alterar" forState:UIControlStateHighlighted];
        
    } else {
        
        if (self.password) {
            self.password1.text = self.password;
            self.password2.text = self.password;
        }
        
    }
    
        if (!self.loginUser || [self.loginUser isEqualToString:@""]) {
        [Utils showMessage:@"Necessário preencher o atributo loginUser do PasswordViewController no prepareForSegue antes de chamá-lo."];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


-(BOOL)shouldAutorotate
{
    return NO;
}


#pragma mark - Form Validation

- (BOOL)formIsValid
{
    if (_password1.text.length == 0 || _password2.text.length == 0) {
        [Utils showMessage:@"Todos os campos são obrigatórios!"];
        return NO;
    }
    
    if (![_password1.text isEqualToString:_password2.text]) {
        [Utils showMessage:@"Senha não confere!"];
        return NO;
    }
    
    return YES;
}

#pragma mark - Password Change
- (void)change
{
    Firebase *ref = [[Firebase alloc] initWithUrl:FIREBASE_APPCI_URL];
    
    [ref changePasswordForUser:_loginUser fromOld:_password toNew:self.password1.text withCompletionBlock:^(NSError *error) {
        
        [Utils stopNetworkIndicator];
        [Utils dismissProgress];
        
        if (error) {
            [Utils showMessage:[[Utils firebaseUtils] messageForError:error]];
            return;
        }
        
        [Utils saveLoginUserDefaultsWithEmail:_loginUser andPassword:_password1.text];
    
        [self performSegueWithIdentifier:@"UnwindFromChangePassword" sender:self];
        
        
    }];
}

#pragma mark - Password Creation

- (IBAction)create
{
    [self.view endEditing:YES];
    
    if (![self formIsValid]) {
        return;
    }
    
    if (![Utils isConnected]) {
        [Utils showMessage:@"Verifique sua conexão com a internet."];
        return;
    }
    
    [Utils showProgress:@"Aguarde ..."];
    
    
    if (self.changePassword) {
        [self change];
        return;
    }
    
    [FirebaseUtilsUser createUser:self.loginUser withPassword:self.password1.text andCompletionBlock:^(NSError *error) {
        
        [Utils dismissProgress];
        
        if (error) {
            
            // User already exists.
            if (error.code == FAuthenticationErrorEmailTaken) {
                [Utils showProgress:@"Verificando usuário ..."];
                
                [FirebaseUtilsUser loginUser:self.loginUser withPassword:self.password1.text andCompletionBlock:^(NSError *error) {
                    [Utils dismissProgress];
                    
                    if (error) {
                        [Utils showMessage:@"Ocorreu um erro. Tente realizar o login novamente."];
                        [Utils prepareLogout];
                        [Utils showLogin];
                        return;
                    }
                    
                    [Utils saveLoginUserDefaultsWithEmail:_loginUser andPassword:_password1.text];
                    [self performSegueWithIdentifier:@"unwindFromPassword" sender:self];
                }];
                
                return;
            }
            
            NSString *message = [[Utils firebaseUtils] messageForError:error];
            [Utils showMessage:message];
            return;
        }
        
        // Update the current corretor information in Firebase when create user.
        Corretor *corretor = [Corretor corretorWithContext:self.moc];
        [FirebaseUtilsUser updateUserInfoWithCorretor:corretor];
        
        // Set this to send local data to Firebase after login.
        [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:FIREBASE_FIRST_CREATE_USER];
        
        [Utils saveLoginUserDefaultsWithEmail:_loginUser andPassword:_password1.text];
        [self performSegueWithIdentifier:@"unwindFromPassword" sender:self];
    }];
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _password1) {
        [_password2 becomeFirstResponder];
    } else if (textField == _password2) {
        [textField resignFirstResponder];
        [self create];
    }
    
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([[UIScreen mainScreen] bounds].size.height < 568) {
        [self animateTextField:textField up:YES];
    }
    
    [Utils removeRedCorner:textField];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([[UIScreen mainScreen] bounds].size.height < 568) {
        [self animateTextField:textField up:NO];
    }
}


- (void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -90; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


#pragma mark - Dismiss Keyboard

- (IBAction)dismissKeyboard:(id)sender
{
    [self.view endEditing:YES];
}


@end
