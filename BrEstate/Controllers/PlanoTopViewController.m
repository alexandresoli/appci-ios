//
//  PlanoTopViewController.m
//  Appci
//
//  Created by BrEstate LTDA on 13/06/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "PlanoTopViewController.h"


@interface PlanoTopViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIButton *comeceAgoraButton;

@end


@implementation PlanoTopViewController


#pragma mark - View Life Cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Utils trackWithName:@"Entrou Tela Plano TOP"];
    [Utils trackFlurryWithName:@"Entrou Tela Plano TOP"];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    Corretor *corretor = [Corretor MR_findFirstInContext:self.moc];

    BOOL isTop = NO;
    
    if ([self.iapHelper isPlanTopValidForCorretor:corretor]) {
        isTop = YES;
        self.comeceAgoraButton.hidden = YES;
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        if ([[UIScreen mainScreen] bounds].size.height == 568) { // iphone 5
            
            if (isTop) {
                self.backgroundImageView.image = [UIImage imageNamed:@"plano-top-bg-iphone4-pago"];
            } else {
                self.backgroundImageView.image = [UIImage imageNamed:@"plano-top-bg-iphone4"];
            }

            
        } else { // iphone 4
            
            if (isTop) {
                self.backgroundImageView.image = [UIImage imageNamed:@"plano-top-bg-iphone3-pago"];
            } else {
                self.backgroundImageView.image = [UIImage imageNamed:@"plano-top-bg-iphone3"];
            }
            
        }
        
    } else { // ipad
        if (isTop) {
            self.backgroundImageView.image = [UIImage imageNamed:@"plano-top-bg-ipad-pago"];
        } else {
            self.backgroundImageView.image = [UIImage imageNamed:@"plano-top-bg-ipad"];
        }
        
    }
}


#pragma mark - IBActions

- (IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)buyPlanoTop:(id)sender
{
    // Verifica se os dados pessoais estão preenchidos.
    if (![Utils personalDataIsFilled:self
                         withContext:self.moc
                          andMessage:@"Para assinar o plano TOP é necessário o preenchimento de suas informações!"]) {
        return;
    }
    
    [self.iapHelper buyPlanTOP];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
