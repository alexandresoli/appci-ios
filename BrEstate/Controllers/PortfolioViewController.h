//
//  PortfolioViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/14/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Imovel;
@interface PortfolioViewController : BaseViewController

@property (nonatomic, strong) Imovel *selectedImovel;
@property (nonatomic, strong) NSPredicate *predicate;

@end
