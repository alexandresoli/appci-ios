//
//  PortfolioViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/14/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "PortfolioViewController.h"
#import "ListaImoveisViewController.h"

#import "Imovel.h"

@interface PortfolioViewController ()
@property (nonatomic, strong) ListaImoveisViewController *listaImoveisController;



@end

@implementation PortfolioViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // same order from storyboard controllers
    //   NSLog(@"children : %@", self.childViewControllers);
    self.listaImoveisController = [self.childViewControllers objectAtIndex:0];
    
    UINavigationController *navController = [self.childViewControllers objectAtIndex:1];
    self.listaImoveisController.detalhesImovelViewController = [[navController viewControllers] firstObject];
    self.listaImoveisController.selectedImovel = self.selectedImovel;
    self.listaImoveisController.selectedPredicate = self.predicate;
    
    self.selectedImovel = nil;
    self.predicate = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
//    [Utils trackWithName:@"Aba Portfolio"];
//    [Utils trackFlurryWithName:@"Aba Portfolio"];

    [self configureAlert];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



//- (BOOL)prefersStatusBarHidden
//{
//    return YES;
//}



@end
