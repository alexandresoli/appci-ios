//
//  ContatosViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 12/23/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "BaseTableViewController.h"
#import "DetalhesContatoViewController.h"
#import "DetalhesImovelViewController.h"

@class Cliente;

@interface SelecionarClienteViewController : BaseViewController
@property (nonatomic, strong) DetalhesContatoViewController *detalhesContatoViewController;
@property (nonatomic, strong) DetalhesImovelViewController *detalhesImovelViewController;

@property (nonatomic, strong) Cliente *selectedContato;
@end
