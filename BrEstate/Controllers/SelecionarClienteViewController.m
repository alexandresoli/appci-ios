//
//  ListaContatosViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/7/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "SelecionarClienteViewController.h"
#import "Cliente+Utils.h"
#import "SubtipoCliente.h"
#import "TipoCliente.h"
#import "ContatoCell.h"
#import "SubtipoCliente.h"
#import "EditarContatoViewController.h"
#import "ImageUtils.h"
#import "AddressBookHelper.h"
#import "MailViewController.h"
#import "MapaViewController.h"
#import "Acompanhamento.h"

@interface SelecionarClienteViewController () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate,UISearchBarDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, retain) NSFetchedResultsController *searchFetchedResultsController;
@property (nonatomic, weak) IBOutlet UISegmentedControl *tipoSegment;
@property (nonatomic, weak) IBOutlet UISearchBar *contatoSearchBar;

@property (nonatomic, strong) MailViewController *mailViewController;

@property (nonatomic, strong) NSCache *fotosCache;

@end

@implementation SelecionarClienteViewController

@synthesize fetchedResultsController,searchFetchedResultsController;
@synthesize contatoSearchBar;
@synthesize tipoSegment;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //  [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"ContatoCell"];
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    // add model changes notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDataModelChange:) name:NSManagedObjectContextObjectsDidChangeNotification object:self.moc];
    
    if (![[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        // No iPhone tem que reduzir a fonte para não cortar as palavras.
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:11.0f]};
        [self.tipoSegment setTitleTextAttributes:attributes forState:UIControlStateNormal];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Tela Selecao Cliente"];
    [Utils trackFlurryWithName:@"Tela Selecao Cliente"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSCache *)fotosCache
{
    if (_fotosCache) {
        return _fotosCache;
    }
    
    _fotosCache = [[NSCache alloc] init];
    [_fotosCache setCountLimit:30];
    
    return _fotosCache;
}


#pragma mark - Notification Center
- (void)handleDataModelChange:(NSNotification *)note
{
    NSSet *updatedObjects = [[note userInfo] objectForKey:NSUpdatedObjectsKey];
    NSSet *insertedObjects = [[note userInfo] objectForKey:NSInsertedObjectsKey];
    NSSet *deletedObjects = [[note userInfo] objectForKey:NSDeletedObjectsKey];
    
    
    if ([updatedObjects containsMemberOfClass:[Cliente class]] ||
        [insertedObjects containsMemberOfClass:[Cliente class]] ||
        [deletedObjects containsMemberOfClass:[Cliente class]])
    {
        [self.fetchedResultsController performFetch:nil];
        
        [self.fotosCache removeAllObjects];
        [self.tableView reloadData];
        
    }
    
}


#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (fetchedResultsController != nil) {
        return fetchedResultsController;
    }
    
    fetchedResultsController.delegate = self;
    
    NSPredicate *predicate = nil;
    
    fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithAlphabetic:@"Cliente" andKey:@"nome" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
    
    
    return fetchedResultsController;
}



#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (NSArray *) sectionIndexTitlesForTableView: (UITableView *) tableView
{
    return [self.fetchedResultsController sectionIndexTitles];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [self.fetchedResultsController sectionForSectionIndexTitle:title atIndex:index];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *labelView = [[UILabel alloc] init];
    labelView.textAlignment = NSTextAlignmentLeft;
    //labelView.backgroundColor = [UIColor colorWithRed:241.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
    //labelView.textColor = [UIColor colorWithRed:0.0/255 green:135.0/255 blue:204.0/255 alpha:1.0];
    labelView.backgroundColor = [UIColor colorWithRed:0.851 green:0.851 blue:0.851 alpha:1];
    labelView.textColor = [UIColor blackColor];
    labelView.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
    
    NSString *headerText = [[[self.fetchedResultsController sections] objectAtIndex:section] name];
    labelView.text = [NSString stringWithFormat:@"   %@",headerText];
    
    return labelView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ContatoCell";
    ContatoCell *cell = (ContatoCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;;
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];
    
    //    UIView *bgColorView = [[UIView alloc] init];
    //    bgColorView.backgroundColor = [UIColor colorWithRed:108.0/255 green:233.0/255 blue:162.0/255 alpha:1];
    //    cell.selectedBackgroundView = bgColorView;
    
    // SWTableViewCell
//    [cell setCellHeight:cell.frame.size.height];
//    cell.containingTableView = tableView;
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(ContatoCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Cliente *cliente = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.nome.text = cliente.nome;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",cliente.subtipo];
    
    SubtipoCliente *subtipo = [SubtipoCliente MR_findFirstWithPredicate:predicate inContext:self.moc];
    
    
    // Foto.
    cell.fotoLabel.text = cliente.iniciaisNome;
    cell.fotoLabel.hidden = NO;
    
    if (cliente.foto == nil) {
        cell.fotoImageView.image = nil;
        
    } else {
        cell.fotoImageView.layer.cornerRadius = cell.fotoImageView.frame.size.height /2;
        cell.fotoImageView.layer.masksToBounds = YES;
        cell.fotoImageView.layer.borderWidth = 0;
        cell.fotoImageView.image = [self.fotosCache objectForKey:indexPath];
        
        if (cell.fotoImageView.image) {
            cell.fotoLabel.hidden = YES;
            
        } else {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                UIImage *image = [UIImage imageWithData:cliente.foto];
                
                [self.fotosCache setObject:image forKey:indexPath];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    ContatoCell *updateCell = (ContatoCell *)[self.tableView cellForRowAtIndexPath:indexPath];
                    
                    [UIView transitionWithView:cell.fotoImageView
                                      duration:0.2f
                                       options:UIViewAnimationOptionTransitionCrossDissolve
                                    animations:^{
                                        
                                        updateCell.fotoLabel.hidden = YES;
                                        updateCell.fotoImageView.image = image;
                                        
                                    } completion:NULL];
                });
            });
        }
    }
    
    
    // tipo
    cell.tipoImageView.hidden = subtipo.tipoCliente.descricao.length > 0 ? NO : YES;
    cell.subTipo.hidden = subtipo.tipoCliente.descricao.length > 0 ? NO : YES;
    if (subtipo.tipoCliente.descricao.length > 0) {
        
        
        // border around label
        cell.subTipo.layer.borderColor = [UIColor whiteColor].CGColor;
        cell.subTipo.layer.borderWidth = 2.0;
        cell.subTipo.layer.cornerRadius = 8.0;
        
        cell.subTipo.text = subtipo.descricao;
        cell.subTipo.textColor = [UIColor whiteColor];
        
        if ([subtipo.tipoCliente.descricao isEqualToString:@"Proprietário"]) {
            cell.tipoImageView.image = [UIImage imageNamed:@"contato-p"];
            cell.subTipo.backgroundColor = [UIColor colorWithRed:77.0/255 green:219.0/255 blue:109.0/255 alpha:1];
        } else if ([subtipo.tipoCliente.descricao isEqualToString:@"Interessado"]) {
            cell.tipoImageView.image = [UIImage imageNamed:@"contato-i"];
            cell.subTipo.backgroundColor = [UIColor colorWithRed:252.0/255 green:150.0/255 blue:42.0/255 alpha:1];
        } else {
            cell.tipoImageView.image = [UIImage imageNamed:@"contato-o"];
            cell.subTipo.backgroundColor = [UIColor colorWithRed:255.0/255 green:44.0/255 blue:85.0/255 alpha:1];
        }
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedContato = [fetchedResultsController objectAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"UnwindFromContatos" sender:self];
    
}


#pragma mark - Filtro
- (IBAction)changeSegment:(UISegmentedControl *)segment
{
    
    [self.detalhesContatoViewController changeCliente:nil];
    
    NSPredicate *predicate = nil;
    
    switch (segment.selectedSegmentIndex) {
        case 1:
            predicate = [NSPredicate predicateWithFormat:@"subtipo >= 1 AND subtipo <= 2"];
            break;
        case 2:
            predicate = [NSPredicate predicateWithFormat:@"subtipo >= 3 AND subtipo <= 4"];
            break;
        case 3:
            predicate = [NSPredicate predicateWithFormat:@"subtipo >= 5"];
            break;
        default:
            break;
    }
    
    fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithAlphabetic:@"Cliente" andKey:@"nome" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
    
    [self.fotosCache removeAllObjects];
    [self.tableView reloadData];
    
}

#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    tipoSegment.selectedSegmentIndex = 0;
    [self changeSegment:tipoSegment];
    
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    searchBar.showsCancelButton = YES;
    
    return YES;
}

// First use the Searchbar delegate methods
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self.detalhesContatoViewController changeCliente:nil];
    
    [self filterContentForSearchText:searchText];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self filterContentForSearchText:searchBar.text];
    searchBar.showsCancelButton = NO;
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    searchBar.text = nil;
    searchBar.showsCancelButton = NO;
    [self filterContentForSearchText:nil];
    [searchBar resignFirstResponder];
}

// The method to change the predicate of the FRC
- (void)filterContentForSearchText:(NSString*)searchText
{
    NSString *query = searchText;
    
    NSPredicate *predicate = nil;
    
    if (query && query.length) {
        
        predicate = [NSPredicate predicateWithFormat:@"(nome contains[c] %@ OR email contains[c] %@ OR subtipo.tipoCliente.descricao contains[c] %@ OR subtipo.descricao contains[c] %@ OR endereco.cep contains[c] %@ OR endereco.logradouro contains[c] %@ OR endereco.complemento contains[c] %@ OR endereco.estado.nome contains[c] %@ OR endereco.cidade.nome contains[c] %@ OR endereco.bairro.nome contains[c] %@)", query,query,query,query,query,query,query,query,query,query];
        
        [self.fetchedResultsController.fetchRequest setPredicate:predicate];
        [self.fetchedResultsController.fetchRequest setFetchLimit:100]; // Optional, but with large datasets - this helps speed lots
        
    } else {
        fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithAlphabetic:@"Cliente" andKey:@"nome" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
        
    }
    
    [self.fetchedResultsController performFetch:nil];
    
    [self.fotosCache removeAllObjects];
    [self.tableView reloadData];
}


@end
