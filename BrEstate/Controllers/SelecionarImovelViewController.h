//
//  AdicionarImovelViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/8/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Imovel;
@interface SelecionarImovelViewController : BaseViewController

@property (nonatomic, strong) Imovel *selectedImovel;
@end
