//
//  AdicionarImovelViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/8/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "SelecionarImovelViewController.h"
#import "Imovel+Utils.h"
#import "Bairro.h"
#import "Endereco.h"
#import "FotoImovel+InsertDeleteImagem.h"
#import "TipoImovel.h"
#import "TipoOferta.h"
#import "DetalhesImovelCell.h"

#import "EditarImovelViewController.h"

@interface SelecionarImovelViewController () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate,UISearchBarDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, weak) IBOutlet UISearchBar *imovelSearchBar;
@property (nonatomic, strong) NSCache *fotosCache;

@end

@implementation SelecionarImovelViewController

#pragma mark - Queue

dispatch_queue_t queueLoadFoto;


#pragma mark - View Life Cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // add model changes notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDataModelChange:) name:NSManagedObjectContextObjectsDidChangeNotification object:self.moc];
    
    queueLoadFoto = dispatch_queue_create("SelecionarImovelViewController.LoadFoto", NULL);
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _selectedImovel = nil;
    [Utils trackWithName:@"Selecionar Imovel - Adicionar/Editar Evento (Agenda)"];
    [Utils trackFlurryWithName:@"Selecionar Imovel - Adicionar/Editar Evento (Agenda)"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSCache *)fotosCache
{
    if (_fotosCache) {
        return _fotosCache;
    }
    
    _fotosCache = [[NSCache alloc] init];
    [_fotosCache setCountLimit:15];
    
    return _fotosCache;
}


#pragma mark - Notification Center
- (void)handleDataModelChange:(NSNotification *)note
{
    NSSet *updatedObjects = [[note userInfo] objectForKey:NSUpdatedObjectsKey];
    NSSet *deletedObjects = [[note userInfo] objectForKey:NSDeletedObjectsKey];
    NSSet *insertedObjects = [[note userInfo] objectForKey:NSInsertedObjectsKey];
    
    if ([updatedObjects containsMemberOfClass:[Imovel class]] ||
        [insertedObjects containsMemberOfClass:[Imovel class]] ||
        [deletedObjects containsMemberOfClass:[Imovel class]])
    {
        self.fetchedResultsController = nil;
        [self fetchedResultsController];
        
        [self.tableView reloadData];
    }
    
    if ([updatedObjects  containsMemberOfClass:[Imovel class]] ||
        [insertedObjects containsMemberOfClass:[Imovel class]] ||
        [deletedObjects  containsMemberOfClass:[Imovel class]]
        ) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cliente = nil"];
        self.fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Imovel"
                                                                              withSortKey:@"endereco.bairro.nome"
                                                                          withSectionName:@"sectionIdentifier"
                                                                            withPredicate:predicate
                                                                                withOrder:YES
                                                                                withLimit:0
                                                                                inContext:self.moc];
        //[CoreDataUtils fetchedResultsControllerWithEntity:@"Imovel" andKey:@"endereco.bairro.nome" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
        [self.tableView reloadData];
        
    }
    
    
    
}

#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
   // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cliente = nil"];
    
    _fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Imovel"
                                                                      withSortKey:@"endereco.bairro.nome"
                                                                  withSectionName:@"sectionIdentifier"
                                                                    withPredicate:nil
                                                                        withOrder:YES
                                                                        withLimit:0
                                                                        inContext:self.moc];
    //[CoreDataUtils fetchedResultsControllerWithEntity:@"Imovel" andKey:@"endereco.bairro.nome" withPredicate:nil withOrder:YES withLimit:0 inContext:self.moc];
    
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
}



#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *labelView = [[UILabel alloc] init];
    labelView.textAlignment = NSTextAlignmentLeft;
    //labelView.backgroundColor = [UIColor colorWithRed:0.0/255 green:122.0/255 blue:255.0/255 alpha:1.0];
    //labelView.textColor = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0];
    labelView.backgroundColor = [UIColor colorWithRed:0.851 green:0.851 blue:0.851 alpha:1];
    labelView.textColor = [UIColor blackColor];
    labelView.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
    
    
    if ([[self.fetchedResultsController sections] count] > 0) {
        NSString *text = [[[self.fetchedResultsController sections] objectAtIndex:section] name];
        
        if (text.length == 0) {
            text = @"Bairro Não Cadastrado";
        }
        
        labelView.text = [NSString stringWithFormat:@"   %@",text];
    }
    
    
    return labelView;
}

/*
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 280;
}
*/

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"DetalhesImovelCell";
    DetalhesImovelCell *cell = (DetalhesImovelCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;;
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];

    Imovel *imovel = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [self configureTags:imovel cell:cell];
    [self configureCell:cell atIndexPath:indexPath forImovel:imovel];
    
    return cell;
}

- (void)configureCell:(DetalhesImovelCell *)cell atIndexPath:(NSIndexPath *)indexPath forImovel:(Imovel *)imovel
{
    NSArray *fotos = [imovel.fotos allObjects];
    
    if (imovel.identificacao && ![imovel.identificacao isEqualToString:@""]) {
        cell.codigo.text = [NSString stringWithFormat:@"%@",imovel.identificacao];
    } else {
        cell.codigo.text = @"Código não informado";
    }
    
    if(imovel.endereco.logradouro.length > 0) {
        cell.enderecoLabel.text = imovel.endereco.logradouro;
    } else {
        cell.enderecoLabel.text = @"Não informado.";
    }
    
    if (fotos.count == 0) {
        cell.fotoImageView.image = [UIImage imageNamed:@"nao-disponivel2"];
        
    } else {
        cell.fotoImageView.image = [self.fotosCache objectForKey:indexPath];
        
        if (!cell.fotoImageView.image) {
            __block FotoImovel *fotoImovel = [[fotos filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"capa = YES"]] firstObject];
            
            dispatch_async(queueLoadFoto, ^{
                
                if (![self.tableView.indexPathsForVisibleRows containsObject:indexPath]) {
                    return;
                }
                
                if (fotoImovel == nil) { // sem capa
                    fotoImovel = [fotos firstObject];
                }
                
                UIImage *image = [fotoImovel getImageForFotoImovelWithMaxPixelSize:500];
                
                if (image) {
                    [self.fotosCache setObject:image forKey:indexPath];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    DetalhesImovelCell *updateCell = (DetalhesImovelCell *)[self.tableView cellForRowAtIndexPath:indexPath];
                    [UIView transitionWithView:updateCell.fotoImageView
                                      duration:0.2f
                                       options:UIViewAnimationOptionTransitionCrossDissolve
                                    animations:^{
                                        
                                        updateCell.fotoImageView.image = image;
                                        
                                    } completion:NULL];
                });
            });
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _selectedImovel = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [self performSegueWithIdentifier:@"UnwindFromImovel" sender:self];
    
}

#pragma mark - Tag Configuration
- (void)configureTags:(Imovel *)imovel cell:(DetalhesImovelCell *)cell
{
    // reset tags
    cell.tag1.text = nil;
    cell.tag2.text = nil;
    cell.tag3.text = nil;
    cell.tag4.text = nil;
    cell.tag5.text = nil;
    cell.tag6.text = nil;
    
    UIColor *color = [UIColor colorWithRed:252.0/255 green:35.0/255 blue:88.0/255 alpha:1];;
    cell.tag1.backgroundColor = color;
    cell.tag2.backgroundColor = color;
    cell.tag3.backgroundColor = color;
    cell.tag4.backgroundColor = color;
    cell.tag5.backgroundColor = color;
    cell.tag6.backgroundColor = color;
    
    cell.tag1.text = imovel.tipo;
    cell.tag2.text = imovel.endereco.bairro.nome;
    cell.tag3.text = [self formatQuartos:[imovel.quartos intValue]];
    cell.tag4.text =  [self.formatter stringFromNumber:imovel.valor];
    
    if(imovel.areaUtil != nil) {
        cell.tag5.text =  [NSString stringWithFormat:@"%@ m²",imovel.areaUtil];
    }
    
    cell.tag6.text =  imovel.oferta;
    
    
}

#pragma mark - Filtro
- (IBAction)changeSegment:(UISegmentedControl *)segment
{
    
    NSPredicate *predicate = nil;
    
    if(segment.selectedSegmentIndex == 1) {
        predicate = [NSPredicate predicateWithFormat:@"oferta.descricao = %@",@"Locação"];
    } else if(segment.selectedSegmentIndex == 2) {
        predicate = [NSPredicate predicateWithFormat:@"oferta.descricao = %@",@"Venda"];
        
    }
    
    
    self.fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Imovel"
                                                                          withSortKey:@"endereco.bairro.nome"
                                                                      withSectionName:@"sectionIdentifier"
                                                                        withPredicate:predicate
                                                                            withOrder:YES
                                                                            withLimit:0
                                                                            inContext:self.moc];
    //[CoreDataUtils fetchedResultsControllerWithEntity:@"Imovel" andKey:@"endereco.bairro.nome" withPredicate:predicate withOrder:YES withLimit:0 inContext:self.moc];
    
    [self.tableView reloadData];
    
}


#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    
    [Utils trackWithName:@"Iniciou busca - Imovel (Agenda)"];
    [Utils trackFlurryWithName:@"Iniciou busca - Imovel (Agenda)"];

    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    searchBar.showsCancelButton = YES;
    
    return YES;
}


// First use the Searchbar delegate methods
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self filterContentForSearchText:searchText];
}


-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self filterContentForSearchText:searchBar.text];
    searchBar.showsCancelButton = NO;
    [searchBar resignFirstResponder];
}


- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    searchBar.text = nil;
    searchBar.showsCancelButton = NO;
    [self filterContentForSearchText:nil];
    [searchBar resignFirstResponder];
}


// The method to change the predicate of the FRC
- (void)filterContentForSearchText:(NSString*)searchText
{
    NSString *query = searchText;
    
    if (query && query.length) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"descricao contains[c] %@ OR endereco.cep contains[c] %@ OR endereco.logradouro contains[c] %@ OR endereco.complemento contains[c] %@ OR endereco.estado.nome contains[c] %@ OR endereco.cidade.nome contains[c] %@ OR endereco.bairro.nome contains[c] %@", query,query,query,query,query,query,query];
        
        [self.fetchedResultsController.fetchRequest setPredicate:predicate];
        [self.fetchedResultsController.fetchRequest setFetchLimit:100];
        
    } else {
        
        self.fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"Imovel"
                                                                              withSortKey:@"endereco.bairro.nome"
                                                                          withSectionName:@"sectionIdentifier"
                                                                            withPredicate:nil
                                                                                withOrder:YES
                                                                                withLimit:0
                                                                                inContext:self.moc];
        //[CoreDataUtils fetchedResultsControllerWithEntity:@"Imovel" andKey:@"endereco.bairro.nome" withPredicate:nil withOrder:YES withLimit:0 inContext:self.moc];
        
    }
    
    [self.fetchedResultsController performFetch:nil];
    
    [self.tableView reloadData];
}

#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"EditarImovelSegue"]) {
        
        EditarImovelViewController *controller = segue.destinationViewController;
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        controller.selectedImovel = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
}



@end
