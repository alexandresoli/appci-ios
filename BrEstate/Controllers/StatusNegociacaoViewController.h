//
//  StatusNegociacaoViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 12/7/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "StatusNegociacaoViewController.h"

@class StatusNegociacao;
@interface StatusNegociacaoViewController : BaseTableViewController

@property (nonatomic, strong) StatusNegociacao *selectedStatusNegociacao;

@end
