//
//  SyncViewController.m
//  Appci
//
//  Created by BrEstate LTDA on 9/10/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "SyncViewController.h"
#import "Cliente.h"
#import "Evento.h"
#import "Imovel.h"
#import "Nota.h"
#import "FotoImovel.h"
#import "Acompanhamento.h"
#import "Endereco.h"
#import "Bairro.h"
#import "TagImovel.h"

@interface SyncViewController ()

@property (nonatomic, weak) IBOutlet UILabel *totalLocalTitle;
@property (nonatomic, weak) IBOutlet UILabel *totalRemoteTitle;
@property (nonatomic, weak) IBOutlet UILabel *totalLocal;
@property (nonatomic, weak) IBOutlet UILabel *totalRemote;
@property (nonatomic, weak) IBOutlet UILabel *syncStatus;
@property (nonatomic, weak) IBOutlet UILabel *syncTitle;
@property (nonatomic, weak) IBOutlet UIImageView *syncImageView;
@property (nonatomic, weak) IBOutlet UIImageView *lineImageView;
@property (nonatomic, weak) IBOutlet UIButton *syncButton;
@property (weak, nonatomic) IBOutlet UILabel *warningPlanTopLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *syncProgressView;
@property (weak, nonatomic) IBOutlet UILabel *syncProgressLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *syncActivityIndicator;

@end


@implementation SyncViewController


#pragma mark - View Life Cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        if (self.moc == nil) {
            self.moc = [NSManagedObjectContext MR_defaultContext];
        }
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // add model changes notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTotal:) name:@"UpdateSyncCounter" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshCheckSync) name:@"Sync" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finishSync) name:@"FinishSync" object:nil];
    
    _syncButton.enabled = YES;
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [Utils trackWithName:@"Tela de Sincronização"];
    [Utils trackFlurryWithName:@"Tela de Sincronização"];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self checkSync];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [UIApplication sharedApplication].idleTimerDisabled = NO;
}


#pragma mark - Sync Check

- (void)finishSync
{
    [self.syncProgressView setProgress:1.0 animated:YES];
    
    [self checkSync];
    
    self.syncProgressLabel.hidden = YES;
    self.syncProgressView.hidden = YES;
    self.syncButton.enabled = YES;
    [self.syncButton setBackgroundColor:[UIColor colorWithRed:0.086 green:0.494 blue:0.984 alpha:1] /*#167efb*/];
    
    [self.syncActivityIndicator stopAnimating];
    
    // Error messages for upload of files.
    NSString *errorMessage = @"";
    NSString *errorFoto = [Utils valueFromDefaults:SYNC_ERROR_MESSAGE_FOTO];
    NSString *errorAcomp = [Utils valueFromDefaults:SYNC_ERROR_MESSAGE_ACOMP];
    NSString *errorNota = [Utils valueFromDefaults:SYNC_ERROR_MESSAGE_NOTA];
    
    if (errorFoto) {
        errorMessage = [NSString stringWithFormat:@"%@\n%@", errorMessage, errorFoto];
    }
    
    if (errorAcomp) {
        errorMessage = [NSString stringWithFormat:@"%@\n%@", errorMessage, errorAcomp];
    }
    
    if (errorNota) {
        errorMessage = [NSString stringWithFormat:@"%@\n%@", errorMessage, errorNota];
    }
    
    if (![errorMessage isEqualToString:@""]) {
        [Utils showMessage:errorMessage];
    }
}


- (void)refreshCheckSync
{
    
    [UIView animateWithDuration:1.0f animations:^{
       
        NSNumber *count = [Utils valueFromDefaults:COUNT_SYNC];
        
        if (count) {
            
            self.syncProgressView.hidden = NO;
            self.syncProgressLabel.hidden = NO;
            
            self.syncButton.enabled = NO;
            [self.syncButton setBackgroundColor:[UIColor lightGrayColor]];
            
            [self.syncActivityIndicator startAnimating];
            
            float stepProgress = [count floatValue]/14.0; // 14 total process to run
            [self.syncProgressView setProgress:stepProgress animated:YES];
            
            NSString *message = [Utils valueFromDefaults:MESSAGE_SYNC];
            self.syncProgressLabel.text = message;
        }
        
    }];
}

- (void)refreshTotal:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3f animations:^{
       
        long long newValue = [_totalLocal.text longLongValue] - [notification.object longLongValue];
        self.totalLocal.text = [NSString stringWithFormat:@"%lld",newValue];
        
    }];
}

- (void)checkSync
{
    // Check if sync is running. If running dont show messages.
    NSNumber *count = [Utils valueFromDefaults:COUNT_SYNC];
    
    if (!count && ![Utils isConnected]) {
        _syncImageView.hidden = NO;
        _syncStatus.text = @"Sem conexão com a internet";
        _syncImageView.image = [UIImage imageNamed:@"sync-alert"];
        return;
    }
    
    if (!count) {
        [Utils showProgress:@"Verificando informações, aguarde..."];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        long totalToSend = [[Utils firebaseUtils] countDataToSend];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateInterfaceWithTotalToSend:totalToSend];
        });
    });
}

- (void)updateInterfaceWithTotalToSend:(long)totalToSend
{
    _totalLocalTitle.hidden = NO;
    _totalRemoteTitle.hidden = NO;
    _lineImageView.hidden = NO;
    _totalLocal.text = [NSString stringWithFormat:@"%ld",totalToSend];
    _syncButton.hidden = NO;
    
    [self configureImageWithTotalToSend:totalToSend];
    
    if (![self.iapHelper checkPlanTopFromController:nil withMessage:nil]) {
        self.warningPlanTopLabel.hidden = NO;
    }
    
    [Utils dismissProgress];
    
    [self refreshCheckSync];
}


- (void)configureImageWithTotalToSend:(long)totalToSend
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    _syncImageView.hidden = NO;
    
    if (totalToSend == 0) {
        _syncTitle.text = @"Parabéns!";
        _syncStatus.text = @"Suas Informações estão seguras.";
        _syncImageView.image = [UIImage imageNamed:@"sync-ok"];
        
    } else {
        _syncTitle.text = @"Atenção!";
        _syncStatus.text = @"Existem informações a serem enviadas.";
        _syncImageView.image = [UIImage imageNamed:@"sync-problem"];
        [defaults setValue:[NSNumber numberWithLong:1] forKey:SYNC_STATUS];
    }
    
    [defaults synchronize];
}


#pragma mark - IBActions

- (IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)sync:(id)sender
{
    // If not TOP don't sync.
    if (![[Utils iapHelper] checkPlanTopFromController:self withMessage:PLAN_TOP_MESSAGE_SYNC]) {
        return;
    }
    
    if (![Utils isConnected]) {
        _syncTitle.text = @"Atenção!";
        [Utils showMessage:@"Sem conexão com a internet"];
        return;
    }
    
    self.syncProgressView.progress = 0;
    self.syncProgressView.hidden = NO;
    self.syncProgressLabel.hidden = NO;
    
    self.syncButton.enabled = NO;
    [self.syncButton setBackgroundColor:[UIColor lightGrayColor]];
    
    [self.syncActivityIndicator startAnimating];
    
    [[Utils firebaseUtils] sendToFirebase];
}


@end
