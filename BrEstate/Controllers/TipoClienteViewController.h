//
//  TipoClienteViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 12/7/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "BaseTableViewController.h"

@class SubtipoCliente;
@interface TipoClienteViewController : BaseTableViewController

@property (nonatomic, strong) SubtipoCliente *selectedTipo;

@end
