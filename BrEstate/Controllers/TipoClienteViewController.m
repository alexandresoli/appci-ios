//
//  TipoClienteViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 12/7/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "TipoClienteViewController.h"
#import "EditarContatoViewController.h"
#import "SubtipoCliente.h"

@interface TipoClienteViewController () <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation TipoClienteViewController

@synthesize fetchedResultsController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Tela Tipo Cliente"];
    [Utils trackFlurryWithName:@"Tela Tipo Cliente"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo name];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TipoClienteCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15];
    
    SubtipoCliente *subtipo = [fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.textLabel.text = subtipo.descricao;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedTipo = [fetchedResultsController objectAtIndexPath:indexPath];

    [self performSegueWithIdentifier:@"UnwindFromTipoCliente" sender:self];

}


#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (fetchedResultsController != nil) {
        return fetchedResultsController;
    }
    
    fetchedResultsController.delegate = self;
    
    fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"SubtipoCliente" andKey:@"tipoCliente.descricao" withPredicate:nil withOrder:YES withLimit:0 inContext:self.moc];
    
    
    return fetchedResultsController;
}


@end
