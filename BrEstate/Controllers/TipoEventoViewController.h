//
//  TipoEventoViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/25/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EditarEventoViewController;

@interface TipoEventoViewController : BaseTableViewController

@property (nonatomic, weak) EditarEventoViewController *editarEventoViewController;


@end
