//
//  TipoEventoViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/25/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "TipoEventoViewController.h"
#import "EditarEventoViewController.h"
#import "TipoEvento.h"

#import "UIImage+Custom.h"

@interface TipoEventoViewController () <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;


@end

@implementation TipoEventoViewController

@synthesize moc;
@synthesize fetchedResultsController;
@synthesize editarEventoViewController;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    [self init];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Selecionar Tipo Evento - Adicionar/Editar Evento (Agenda)"];
    [Utils trackFlurryWithName:@"Selecionar Tipo Evento - Adicionar/Editar Evento (Agenda)"];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TipoEventoCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    TipoEvento *tipoEvento = [fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.textLabel.text = tipoEvento.descricao;

    
    if ([tipoEvento.descricao isEqualToString:@"Visita"]) {
        cell.imageView.image = [UIImage squareImageWithColor:[UIColor colorWithRed:108.0/255 green:234.0/255 blue:162.0/255 alpha:0.8] dimension:24];
    } else if([tipoEvento.descricao isEqualToString:@"Captação"]) {
        cell.imageView.image = [UIImage squareImageWithColor:[UIColor colorWithRed:255.0/255 green:146.0/255 blue:113.0/255 alpha:0.8] dimension:24];
    } else if([tipoEvento.descricao isEqualToString:@"Acompanhamento"]) {
        cell.imageView.image = [UIImage squareImageWithColor:[UIColor colorWithRed:98.0/255 green:121.0/255 blue:235.0/255 alpha:0.8] dimension:24];
    } else if([tipoEvento.descricao isEqualToString:@"Reunião"]) {
        cell.imageView.image = [UIImage squareImageWithColor:[UIColor colorWithRed:28.0/255 green:121.0/255 blue:235.0/255 alpha:0.8] dimension:24];
    } else if([tipoEvento.descricao isEqualToString:@"Outros"]) {
        cell.imageView.image = [UIImage squareImageWithColor:[UIColor colorWithRed:98.0/255 green:101.0/255 blue:135.0/255 alpha:0.8] dimension:24];
    }

    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    editarEventoViewController.selectedTipoEvento =  [[fetchedResultsController objectAtIndexPath:indexPath] descricao];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (fetchedResultsController != nil) {
        return fetchedResultsController;
    }
    
    fetchedResultsController.delegate = self;
    
    fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"TipoEvento" andKey:@"codigo" withPredicate:nil withOrder:YES withLimit:0 inContext:moc];
    
    
    return fetchedResultsController;
}


@end
