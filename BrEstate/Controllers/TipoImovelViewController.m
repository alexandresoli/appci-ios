//
//  TipoImovelViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 12/12/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "TipoImovelViewController.h"
#import "TipoImovel.h"
@interface TipoImovelViewController () <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation TipoImovelViewController

@synthesize selectedTipoImovel;
@synthesize fetchedResultsController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Tela Tipo Imovel"];
    [Utils trackFlurryWithName:@"Tela Tipo Imovel"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TipoImovelCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15];
    
    TipoImovel *tipoImovel = [fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.textLabel.text = tipoImovel.descricao;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    selectedTipoImovel = [fetchedResultsController objectAtIndexPath:indexPath];
    
    [self performSegueWithIdentifier:@"UnwindFromTipoImovel" sender:self];
    
    
}

#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (fetchedResultsController != nil) {
        return fetchedResultsController;
    }
    
    fetchedResultsController.delegate = self;
    
    
    fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"TipoImovel" andKey:@"descricao" withPredicate:nil withOrder:YES withLimit:0 inContext:self.moc];
    
    
    return fetchedResultsController;
}

@end
