//
//  TipoOfertaViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 12/12/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "BaseTableViewController.h"

@interface TipoOfertaViewController : BaseTableViewController

@property (nonatomic, strong) NSString *selectedTipoOferta;


@end
