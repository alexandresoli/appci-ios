//
//  TipoOfertaViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 12/12/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "TipoOfertaViewController.h"
#import "TipoOferta.h"

@interface TipoOfertaViewController () <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation TipoOfertaViewController

@synthesize fetchedResultsController;
@synthesize selectedTipoOferta;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Utils trackWithName:@"Tela Tipo Oferta"];
    [Utils trackFlurryWithName:@"Tela Tipo Oferta"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TipoOfertaCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15];
    
    TipoOferta *tipoOferta = [fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.textLabel.text = tipoOferta.descricao;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    selectedTipoOferta = [[fetchedResultsController objectAtIndexPath:indexPath] descricao];
    
    [self performSegueWithIdentifier:@"UnwindFromTipoOferta" sender:self];
    
    
}

#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController
{
    
    if (fetchedResultsController != nil) {
        return fetchedResultsController;
    }
    
    fetchedResultsController.delegate = self;
    
    
    fetchedResultsController = [CoreDataUtils fetchedResultsControllerWithEntity:@"TipoOferta" andKey:@"descricao" withPredicate:nil withOrder:YES withLimit:0 inContext:self.moc];
    
    
    return fetchedResultsController;
}

@end
