//
//  TrocarSenhaViewController.m
//  Appci
//
//  Created by Leonardo Barros on 29/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "TrocarSenhaViewController.h"

@interface TrocarSenhaViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *oldPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *updatedPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *retypePasswordTextField;
@property (weak, nonatomic) IBOutlet UIButton *changePasswordButton;

@end


@implementation TrocarSenhaViewController


#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.changePasswordButton.enabled = NO;
    [self.changePasswordButton setBackgroundColor:[UIColor lightGrayColor]];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldDidChange:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:nil];
}


- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - IBActions

- (IBAction)changePassword:(id)sender {
    
    [self.view endEditing:YES];
    
    if (![self.updatedPasswordTextField.text isEqualToString:self.retypePasswordTextField.text]) {
        [Utils showMessage:@"A senha redigitada não confere com a nova senha. Tente novamente."];
        return;
    }
    
    [Utils showProgress:@"Processando"];
    
    Firebase *ref = [[Firebase alloc] initWithUrl:FIREBASE_APPCI_URL];
    [ref changePasswordForUser:[Utils valueFromDefaults:USER_LOGIN]
                       fromOld:self.oldPasswordTextField.text
                         toNew:self.updatedPasswordTextField.text
           withCompletionBlock:^(NSError *error) {
               
               [Utils dismissProgress];
               
               if (error) {
                   if (error.code == FAuthenticationErrorInvalidPassword) {
                       [Utils showMessage:@"A senha atual informada não confere. Tente novamente."];
                   } else {
                       [Utils showMessage:[[Utils firebaseUtils] messageForError:error]];
                   }
                   return;
               }
               
               [Utils saveLoginUserDefaultsWithEmail:[Utils valueFromDefaults:USER_LOGIN] andPassword:self.updatedPasswordTextField.text];
               
               [Utils showMessage:@"Senha modificada com sucesso!"];
               
               [self.navigationController popViewControllerAnimated:YES];
           }];
}


- (IBAction)dismissKeyboard:(id)sender {
    [self.view endEditing:YES];
}


#pragma mark - Observer TextField

- (void)textFieldDidChange:(NSNotification *)notification {
    
    if (self.oldPasswordTextField.text.length > 0 &&
        self.updatedPasswordTextField.text.length > 0 &&
        self.retypePasswordTextField.text.length > 0) {
        
        self.changePasswordButton.enabled = YES;
        [self.changePasswordButton setBackgroundColor:[UIColor colorWithRed:0.086 green:0.494 blue:0.984 alpha:1] /*#167efb*/];
        
    } else {
        self.changePasswordButton.enabled = NO;
        [self.changePasswordButton setBackgroundColor:[UIColor lightGrayColor]];
    }
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == self.oldPasswordTextField) {
        [self.updatedPasswordTextField becomeFirstResponder];
        
    } else if (textField == self.updatedPasswordTextField) {
        [self.retypePasswordTextField becomeFirstResponder];
        
    } else if (textField == self.retypePasswordTextField) {
        
        if (self.oldPasswordTextField.text.length > 0 &&
            self.updatedPasswordTextField.text.length > 0 &&
            self.retypePasswordTextField.text.length > 0) {
            
            [self.view endEditing:YES];
            [self changePassword:nil];
            
        } else {
            return NO;
        }
    }
    
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        if ([[UIScreen mainScreen] bounds].size.height < 568) {
            [self animateTextField:textField up:YES];
        }
    }
    
    [Utils removeRedCorner:textField];
}


- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        if ([[UIScreen mainScreen] bounds].size.height < 568) {
            [self animateTextField:textField up:NO];
        }
    }
}


- (void)animateTextField:(UITextField*)textField up:(BOOL)up {
    
    const int movementDistance = -70; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


@end
