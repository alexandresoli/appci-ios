//
//  VideoHelpViewController.m
//  Appci
//
//  Created by BrEstate LTDA on 28/04/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "VideoHelpViewController.h"
#import "VideoHelp.h"

@import MediaPlayer;


@interface VideoHelpViewController ()

@property (nonatomic, strong) MPMoviePlayerController *moviePlayerController;

@end


@implementation VideoHelpViewController

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    VideoHelp *video = [[VideoHelp alloc] init];
    self.moviePlayerController = [video showHelpIphoneOnController:self withCallback:@selector(videoFinish)];

}


- (void)videoFinish
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
