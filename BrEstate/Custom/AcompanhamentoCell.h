//
//  AcompanhamentoCell.h
//  BrEstate
//
//  Created by BrEstate LTDA on 1/24/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"


@interface AcompanhamentoCell : SWTableViewCell

@property (nonatomic, weak) IBOutlet UILabel *dia;
@property (nonatomic, weak) IBOutlet UILabel *hora;
@property (nonatomic, weak) IBOutlet UILabel *descricao;
@property (nonatomic, weak) IBOutlet UIImageView *anexoImagem;
@property (nonatomic, weak) IBOutlet UIImageView *selecionada;

@end
