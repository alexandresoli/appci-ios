//
//  Constants.h
//  BrEstate
//
//  Created by BrEstate LTDA on 2/23/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#define ALREADY_RUNNED @"brestate_carga_inicial"
#define IMPORTAR_ARQUIVO_NOTIFICATION @"importarArquivoNotification"
#define MENSAGEM_IMPORTAR_ARQUIVO_CRIAR_NOTA @"Deseja criar uma nota com o arquivo importado?"
#define DIRETORIO_NOTAS_ARQUIVOS @"notas-arquivos"
#define DIRETORIO_IMOVEIS_IMAGENS @"imoveis-imagens"
#define DIRETORIO_ACOMPANHAMENTO_CLIENTES_ARQUIVOS @"acompanhamento-clientes-arquivos"
#define CODE_ACCESS @"brestate_codigo_acesso"
#define USER_LOGIN @"brestate_login_usuario"
#define USER_LAST_LOGIN @"brestate_last_login_usuario"
#define USER_PASS @"brestate_password_usuario"
#define HELP_WAS_SHOWN @"brestate_help_foi_exibido"
#define ICLOUD_UPDATED @"ICloudUpdated"
#define STORE_FILE_NAME @"Appci-Local.sqlite"
#define OLD_STORE_FILE_NAME @"BrEstate.sqlite"
#define SOURCE_STORE_FILE_NAME @"DefaultData.sqlite"
#define ICLOUD_STORE_FILE_NAME @"Appci-iCloud.sqlite"
#define IPHONE_UPDATE_NOTIFICATION @"iphone_update_notification"
#define MIGRATE_PHOTOS @"migrate_photos"



#pragma mark - Status Negociação

#define NEGOCIACAO_PROPOSTA @"Proposta"     // 1
#define NEGOCIACAO_ANALISE @"Análise"       // 2
#define NEGOCIACAO_FECHADA @"Fechada"       // 3
#define NEGOCIACAO_CANCELADA @"Cancelada"   // 4



#pragma mark - Integrações CRM

#define SITE_ALOHIN @"site_alohin"
#define SITE_IMO @"site_imo"
#define EMAIL_VISTA @"email_vista"
#define DOCUMENTO_VISTA @"documento_vista"
#define SITE_CONSIR @"site_consir"
#define SITE_SIMBO @"site_simbo"
#define SITE_IMOBI_BRASIL @"site_imobibrasil"
#define SITE_IMOVELPRO @"site_imovelpro"
#define SITE_GAIA @"site_gaia"
#define SITE_SUB100 @"site_sub100"
#define SITE_WMB @"site_wmb"
#define CODIGO_WMB @"codigo_wmb"
#define SITE_ARCO @"site_arco"
#define USUARIO_ARCO @"usuario_arco"
#define SENHA_ARCO @"senha_arco"

#define SIMBO_CHECKSUM @"simbo_checksum"
#define IMOBIBRASIL_CHECKSUM @"imobibrasil_checksum"
#define GAIA_CHECKSUM @"gaia_checksum"
#define CONSIR_CHECKSUM @"consir_checksum"
#define IMOVELPRO_CHECKSUM @"imovelpro_checksum"
#define IMO_CHECKSUM @"imo_checksum"
#define VISTA_CHECKSUM @"vista_checksum"
#define ALOHIN_CHECKSUM @"alohin_checksum"
#define WMB_CHECKSUM @"wmb_checksum"
#define ARCO_CHECKSUM @"arco_checksum"
#define LAST_CRM_CHECK @"last_crm_check"



#pragma mark -

#define EVENTO_NOTIFICATION @"evento_notification"
#define SMS_NOTIFICATION @"sms_notification"

#define MESSAGE_PAID_FUNCTIONS_WAS_SHOWN @"message_paid_funciotns"
#define TEXT_PAID_FUNCTIONS @"As seguintes funcionalidades fazem parte do plano TOP e estão gratuitas por tempo limitado. Aproveite! \r\r - Enviar SMS para confirmar evento \r - Visão geral dos imóveis no mapa \r - Integrações com CRM e Sites \r - Escolher foto de capa do imóvel \r - Visualizar no mapa clientes e imóveis com o Google Street View"

#define MESSAGE_FINISH_IMPORT @"A importação foi um sucesso!"
#define MESSAGE_EMPTY_IMPORT  @"Não existem imóveis para importar!"
#define MESSAGE_IMPORT_ERROR @"Ocorreu um erro na importação. Verifique sua conexão com a internet e as informações inseridas."
#define MESSAGE_IMPORT_START  @"A importação dos seus dados foi feita com êxito! Foi iniciado o processo de download das fotos em segundo plano e você será avisado quando todas as fotos forem transferidas!"

#define NOTIFICATION_PLAN_TOP_WAS_SHOWN @"notification_plan_top"
#define NOTIFICATION_PLAN_TOP_WILL_END @"notification_plan_top_will_end"

#define ERROR_MESSAGE_ON_LOGIN @"error_message_on_login"
#define ERROR_CODE_ON_LOGIN @"error_code_on_login"
#define ARRAY_DEVICES_EXCEEDED @"array_devices_exceeded"
#define DEVICE_FOR_SUBSTITUTION @"device_for_substitution"

#define PARAMS_REGISTER_PURCHASE @"params_register_purchase"

#define ERROR_CODE_DEVICES_EXCEEDED 4

#define ALREADY_MIGRATE_STORE_TO_PARSE @"already_migrate_store_to_parse"

#define LOST_ACCESS @"lost_access"
#define LOGOFF @"logoff_appci"



#pragma mark - Plan TOP Messages

#define PLAN_TOP_MESSAGE_IMOVEIS_LIMIT @"Limite de imóveis atingido. \nUsuários que não são do plano TOP podem cadastrar no máximo 50 imóveis. \n Para ter cadastro ilimitado de imóveis assine o plano TOP."
#define PLAN_TOP_MESSAGE_IMOVEIS_MAPS @"Visão geral de imóveis no mapa está disponível apenas para assinantes do plano TOP."
#define PLAN_TOP_MESSAGE_FAVORITE_PHOTO @"Apenas assinantes do plano TOP podem escolher a foto de capa do imóvel."
#define PLAN_TOP_MESSAGE_CRM @"A integração com CRMs está disponível apenas para assinantes do plano TOP."
#define PLAN_TOP_MESSAGE_STREET_VIEW @"A visão de Street View está disponível apenas para assinantes do plano TOP."
#define PLAN_TOP_MESSAGE_SMS @"Apenas assinantes do plano TOP podem enviar SMS para confirmar eventos."
#define PLAN_TOP_MESSAGE_SYNC @"Apenas assinantes do plano TOP podem sincronizar e fazer backup dos seus dados."



#pragma mark - Sync

#define SYNC_STATUS @"sync_status"
#define SYNC_STATUS_DATE @"sync_status_date"
#define COUNT_SYNC @"count_sync"
#define MESSAGE_SYNC @"message_sync"

#define SYNC_ERROR_MESSAGE_FOTO @"sync_error_message_foto"
#define SYNC_ERROR_MESSAGE_NOTA @"sync_error_message_nota"
#define SYNC_ERROR_MESSAGE_ACOMP @"sync_error_message_acomp"



#pragma mark - Firebase

#define FIREBASE_CLIENTE_INSERT_VERSION @"firebase_cliente_insert_version"
#define FIREBASE_IMOVEL_INSERT_VERSION @"firebase_imovel_insert_version"
#define FIREBASE_FOTO_INSERT_VERSION @"firebase_foto_insert_version"
#define FIREBASE_BAIRRO_INSERT_VERSION @"firebase_bairro_insert_version"
#define FIREBASE_EVENTO_INSERT_VERSION @"firebase_evento_insert_version"
#define FIREBASE_NOTA_INSERT_VERSION @"firebase_nota_insert_version"
#define FIREBASE_ACOMPANHAMENTO_INSERT_VERSION @"firebase_acompanhamento_insert_version"
#define FIREBASE_NEGOCIACAO_INSERT_VERSION @"firebase_negociacao_insert_version"
#define FIREBASE_PERFIL_INSERT_VERSION @"firebase_perfil_insert_version"
#define FIREBASE_CLIENTE_DELETADO_INSERT_VERSION @"firebase_cliente_deletado_insert_version"
#define FIREBASE_DELETADOS_INSERT_VERSION @"firebase_deletados_insert_version"

#define FIREBASE_ALREADY_DOWNLOAD_ALL @"firebase_already_download_all"

#define REFRESH_CLIENTE @"refresh_cliente"
#define REFRESH_IMOVEL @"refresh_imovel"
#define REFRESH_EVENTO @"refresh_evento"
#define REFRESH_NOTA @"refresh_nota"

#define FIREBASE_PENDING_DELETED_OBJECTS_DICT @"pending_deleted_objects_dict"

#define FIREBASE_USER_ID @"firebase_user_id"

#define FIREBASE_FIRST_CREATE_USER @"firebase_first_create_user"
#define FIREBASE_AMAURY_HACK @"firebase_amaury_hack"
#define FIREBASE_AMAURY_HACK_CLIENTE @"firebase_amaury_hack_cliente"
#define FIREBASE_ADJUST_OBJECTS_CODIGO @"firebase_adjust_objects_codigo"
#define FIREBASE_SEND_ADJUST_OBJECTS_CODIGO @"firebase_send_adjust_objects_codigo"
#define FIREBASE_ALREADY_RUN @"firebase_already_run"


// Firebase Produção
//#define FIREBASE_APPCI_URL @"https://appci.firebaseIO.com/"

// Firebase Desenv
#define FIREBASE_APPCI_URL @"https://appci-desenv.firebaseio.com/"

// Firebase Homolog
//#define FIREBASE_APPCI_URL @"https://appci-homolog.firebaseio.com/"

// Firebase Teste Android
//#define FIREBASE_APPCI_URL @"https://appci-dev.firebaseio.com/"



#pragma mark - Amazon S3

// Produção
//#define S3_BUCKET_NAME @"s3.appci.com.br"

// Desenv
#define S3_BUCKET_NAME @"dsv.s3.appci.com.br"

// Homolog
//#define S3_BUCKET_NAME @"hml.s3.appci.com.br"

#define S3_ACCESS_KEY @"AKIAJ27MRPD33XOF4UCA"
#define S3_SECRET_KEY @"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"
#define S3_BASE_URL @"https://s3-sa-east-1.amazonaws.com/s3.appci.com.br/"
#define S3_PENDING_DELETED_IMAGE @"s3_pending_deleted_image"
#define S3_PENDING_DELETED_FILE @"s3_pending_deleted_file"



#pragma mark -  Completions Block

typedef void(^completionWithError)(NSError *error);
typedef void(^completionWithSuccess)(BOOL success);



