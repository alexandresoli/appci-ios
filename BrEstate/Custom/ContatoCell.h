//
//  ContatoCell.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/17/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface ContatoCell : SWTableViewCell

@property (nonatomic, weak) IBOutlet UILabel *nome;
@property (nonatomic, weak) IBOutlet UILabel *telefone;
@property (nonatomic, weak) IBOutlet UILabel *email;
@property (nonatomic, weak) IBOutlet UIImageView *tipoImageView;
@property (nonatomic, weak) IBOutlet UILabel *subTipo;
@property (nonatomic, weak) IBOutlet UIImageView *fotoImageView;
@property (nonatomic, weak) IBOutlet UIButton *detalhesButton;
@property (nonatomic, weak) IBOutlet UIImageView *selecionada;
@property (nonatomic, weak) IBOutlet UILabel *fotoLabel;
@property (nonatomic, weak) IBOutlet UIButton *importButton;

@end
