//
//  ContatoCellNoSWTable.h
//  Appci
//
//  Created by BrEstate LTDA on 22/05/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContatoCellNoSWTable : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *nome;
@property (nonatomic, weak) IBOutlet UILabel *telefone;
@property (nonatomic, weak) IBOutlet UILabel *email;
@property (nonatomic, weak) IBOutlet UIImageView *tipoImageView;
@property (nonatomic, weak) IBOutlet UILabel *subTipo;
@property (nonatomic, weak) IBOutlet UIImageView *fotoImageView;
@property (nonatomic, weak) IBOutlet UIButton *detalhesButton;
@property (nonatomic, weak) IBOutlet UIImageView *selecionada;
@property (nonatomic, weak) IBOutlet UILabel *fotoLabel;

@end
