//
//  ClienteCell.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/2/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetalhesContatoCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *nomeCliente;
@property (nonatomic, weak) IBOutlet UILabel *tagsCliente;;
@property (nonatomic, weak) IBOutlet UILabel *tag1;
@property (nonatomic, weak) IBOutlet UILabel *tag2;
@property (nonatomic, weak) IBOutlet UIButton *placeButton;
@property (nonatomic, weak) IBOutlet UIButton *emailButton;
@property (nonatomic, assign) long numberOfTaps;


@end
