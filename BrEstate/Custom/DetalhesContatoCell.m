//
//  ClienteCell.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/2/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "DetalhesContatoCell.h"

@implementation DetalhesContatoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super initWithCoder:decoder]) {
  //      [self setBackgroundView];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


//- (void)setBackgroundView
//{
//    UIView *bgColorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
//    bgColorView.backgroundColor = [UIColor colorWithRed:253.0/255 green:190.0/255 blue:117.0/255 alpha:1];
//    self.selectedBackgroundView = bgColorView;
//}


// Fix for iOS7, when backgroundView comes above "delete" button
- (void)willTransitionToState:(UITableViewCellStateMask)state {
    [super willTransitionToState:state];
    [self sendSubviewToBack:self.backgroundView];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self sendSubviewToBack:self.backgroundView];
    });
}

- (void)didTransitionToState:(UITableViewCellStateMask)state {
    [super didTransitionToState:state];
    [self sendSubviewToBack:self.backgroundView];
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *aTouch = [touches anyObject];
    self.numberOfTaps = [aTouch tapCount];
    [super touchesEnded:touches withEvent:event];
}

@end
