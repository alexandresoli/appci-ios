//
//  DetalhesImovelCell.h
//  BrEstate
//
//  Created by BrEstate LTDA on 1/22/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetalhesImovelCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *fotoImageView;
@property (nonatomic, weak) IBOutlet UILabel *enderecoLabel;
@property (nonatomic, weak) IBOutlet UILabel *tag1;
@property (nonatomic, weak) IBOutlet UILabel *tag2;
@property (nonatomic, weak) IBOutlet UILabel *tag3;
@property (nonatomic, weak) IBOutlet UILabel *tag4;
@property (nonatomic, weak) IBOutlet UILabel *tag5;
@property (nonatomic, weak) IBOutlet UILabel *tag6;
@property (nonatomic, weak) IBOutlet UILabel *codigo;
@property (nonatomic, weak) IBOutlet UIButton *detalhesButton;


@end
