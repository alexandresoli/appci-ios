//
//  DetalhesImovelCell.m
//  BrEstate
//
//  Created by BrEstate LTDA on 1/22/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "DetalhesImovelCell.h"

@implementation DetalhesImovelCell


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    
    // round corner
    //    self.tag1.layer.cornerRadius = 8.0;
    //    self.tag2.layer.cornerRadius = 8.0;
    //    self.tag3.layer.cornerRadius = 8.0;
    //    self.tag4.layer.cornerRadius = 8.0;
    //    self.tag5.layer.cornerRadius = 8.0;
    //    self.tag6.layer.cornerRadius = 8.0;
    
    // background color
    UIColor *color = [UIColor colorWithRed:252.0/255 green:35.0/255 blue:88.0/255 alpha:1];;
    self.tag1.backgroundColor = color;
    self.tag2.backgroundColor = color;
    self.tag3.backgroundColor = color;
    self.tag4.backgroundColor = color;
    self.tag5.backgroundColor = color;
    self.tag6.backgroundColor = color;
    
    // border around label
//    CGColorRef colorRef = [UIColor grayColor].CGColor;
//    self.tag1.layer.borderColor = colorRef;
//    self.tag1.layer.borderWidth = 2.0;
//    self.tag2.layer.borderColor = colorRef;
//    self.tag2.layer.borderWidth = 2.0;
//    self.tag3.layer.borderColor = colorRef;
//    self.tag3.layer.borderWidth = 2.0;
//    self.tag4.layer.borderColor = colorRef;
//    self.tag4.layer.borderWidth = 2.0;
//    self.tag5.layer.borderColor = colorRef;
//    self.tag5.layer.borderWidth = 2.0;
//    self.tag6.layer.borderColor = colorRef;
//    self.tag6.layer.borderWidth = 2.0;
    
}

//- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
//{
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    if (self) {
//        // Initialization code
//    }
//    return self;
//}
//
//- (void)setSelected:(BOOL)selected animated:(BOOL)animated
//{
//    [super setSelected:selected animated:animated];
//
//    // Configure the view for the selected state
//}

@end
