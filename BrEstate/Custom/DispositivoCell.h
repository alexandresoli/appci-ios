//
//  DispositivoCell.h
//  Appci
//
//  Created by BrEstate LTDA on 20/06/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DispositivoCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *iconImageView;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *idLabel;
@property (nonatomic, weak) IBOutlet UILabel *registerDate;

@end
