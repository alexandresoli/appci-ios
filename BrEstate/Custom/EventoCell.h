//
//  EventoCell.h
//  BrEstate
//
//  Created by BrEstate LTDA on 10/24/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface EventoCell : SWTableViewCell

@property (nonatomic, weak) IBOutlet UILabel *horarioInicio;
@property (nonatomic, weak) IBOutlet UILabel *horarioFim;
@property (nonatomic, weak) IBOutlet UILabel *tipo;
@property (nonatomic, weak) IBOutlet UILabel *descricao;
@property (nonatomic, weak) IBOutlet UILabel *localizacao;
@property (nonatomic, weak) IBOutlet UILabel *alarm;
@property (nonatomic, weak) IBOutlet UIImageView *alarmImage;
@property (nonatomic, weak) IBOutlet UIImageView *selecionada;



@end
