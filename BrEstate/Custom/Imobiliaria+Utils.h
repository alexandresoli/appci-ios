//
//  Imobiliaria+Utils.h
//  Appci
//
//  Created by Alexandre Oliveira on 11/4/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Imobiliaria.h"

@interface Imobiliaria (Utils)

+ (Imobiliaria *)imobiliariaWithContext:(NSManagedObjectContext *)moc;

@end
