//
//  Imobiliaria+Utils.m
//  Appci
//
//  Created by Alexandre Oliveira on 11/4/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Imobiliaria+Utils.h"

@implementation Imobiliaria (Utils)


+ (Imobiliaria *)imobiliariaWithContext:(NSManagedObjectContext *)moc
{
    Imobiliaria *imobiliaria = [Imobiliaria MR_findFirstInContext:moc];
    
    
    if (!imobiliaria) {
        imobiliaria = [Imobiliaria MR_createInContext:moc];
    }
    
    return imobiliaria;
}

@end
