//
//  ImovelCell.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/14/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface ImovelCell : SWTableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *photoView;
@property (nonatomic, weak) IBOutlet UILabel *tag1;
@property (nonatomic, weak) IBOutlet UILabel *tag2;
@property (nonatomic, weak) IBOutlet UILabel *tag3;
@property (nonatomic, weak) IBOutlet UILabel *tag4;
@property (nonatomic, weak) IBOutlet UILabel *tag5;
@property (nonatomic, weak) IBOutlet UILabel *tag6;

@property (nonatomic, assign) int numberOfTaps;
@end
