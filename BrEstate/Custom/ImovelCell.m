//
//  ImovelCell.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/14/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "ImovelCell.h"

@implementation ImovelCell

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    
    // round corner
//    self.tag1.layer.cornerRadius = 8.0;
//    self.tag2.layer.cornerRadius = 8.0;
//    self.tag3.layer.cornerRadius = 8.0;
//    self.tag4.layer.cornerRadius = 8.0;
//    self.tag5.layer.cornerRadius = 8.0;
//    self.tag6.layer.cornerRadius = 8.0;
    
    // background color
    UIColor *color = [UIColor colorWithRed:252.0/255 green:35.0/255 blue:88.0/255 alpha:1];;
    self.tag1.backgroundColor = color;
    self.tag2.backgroundColor = color;
    self.tag3.backgroundColor = color;
    self.tag4.backgroundColor = color;
    self.tag5.backgroundColor = color;
    self.tag6.backgroundColor = color;
    
    // border around label
//    CGColorRef colorRef = [UIColor grayColor].CGColor;
//    self.tag1.layer.borderColor = colorRef;
//    self.tag1.layer.borderWidth = 2.0;
//    self.tag2.layer.borderColor = colorRef;
//    self.tag2.layer.borderWidth = 2.0;
//    self.tag3.layer.borderColor = colorRef;
//    self.tag3.layer.borderWidth = 2.0;
//    self.tag4.layer.borderColor = colorRef;
//    self.tag4.layer.borderWidth = 2.0;
//    self.tag5.layer.borderColor = colorRef;
//    self.tag5.layer.borderWidth = 2.0;
//    self.tag6.layer.borderColor = colorRef;
//    self.tag6.layer.borderWidth = 2.0;

}
//- (void)layoutSubviews {
//    [super layoutSubviews];
//   // self.imageView.frame = CGRectMake(20,10,80,79);
//
//    
//
//}
//
//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
//    
//    UITouch *aTouch = [touches anyObject];
//    self.numberOfTaps = [aTouch tapCount];
//    [super touchesEnded:touches withEvent:event];
//}


@end
