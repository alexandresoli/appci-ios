//
//  Migrator.h
//  Appci
//
//  Created by BrEstate LTDA on 4/14/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Migrator : NSObject

- (void)checkMigrateStoreToParseWithCompletionBlock:(completionWithSuccess) completion;
+ (void)migrateFotoImovelFromCoreDataToDisk:(NSManagedObjectContext *)moc;

@end
