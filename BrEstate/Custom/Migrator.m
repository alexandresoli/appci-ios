//
//  Migrator.m
//  Appci
//
//  Created by BrEstate LTDA on 4/14/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Migrator.h"
#import "Acompanhamento.h"
#import "Estado.h"
#import "Cidade.h"
#import "Bairro.h"
#import "Cliente.h"
#import "Endereco.h"
#import "PerfilCliente.h"
#import "Area.h"
#import "Nota.h"
#import "SubtipoCliente.h"
#import "Imovel.h"
#import "Negociacao.h"
#import "FotoImovel+InsertDeleteImagem.h"
#import "TagImovel.h"
#import "Evento.h"
#import "Alarme.h"
#import "Corretor.h"
#import "TransacaoInAppPurchase.h"
#import "Sms.h"
#import "ClienteDeletado.h"


@interface Migrator ()

@property (nonatomic, readonly) NSManagedObjectModel         *migrationModel;
@property (nonatomic, readonly) NSManagedObjectContext       *migrationContext;
@property (nonatomic, readonly) NSPersistentStore            *migrationStore;
@property (nonatomic, readonly) NSPersistentStoreCoordinator *migrationCoordinator;

@end


@implementation Migrator


- (void)checkMigrateStoreToParseWithCompletionBlock:(completionWithSuccess) completion
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults boolForKey:ALREADY_MIGRATE_STORE_TO_PARSE]) {
        completion(YES);
        return;
    }
    
    NSURL *oldStoreURL = [self getURLOldStore];
    if (!oldStoreURL) {
        [defaults setBool:YES forKey:ALREADY_MIGRATE_STORE_TO_PARSE];
        [defaults synchronize];
        
        completion(YES);
        return;
    }
    
    [self loadOldStoreWithURL:oldStoreURL];
    if (!self.migrationContext) {
        completion(YES);
        return;
    }
    
    [Utils showProgress:@"O AppCi precisa migrar os seus dados. Isso pode demorar até 1 minuto. Aguarde."];
    
    NSManagedObjectContext *oldMoc = self.migrationContext;
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        
        NSArray *bairros = [Bairro MR_findAllInContext:oldMoc];
        for (Bairro *obj in bairros) {
            Bairro *bairro = [Bairro MR_createInContext:localContext];
            bairro.codigo = obj.codigo;
            bairro.nome = obj.nome;
            bairro.cidade =  obj.cidade;
            bairro.version = obj.version;
        }
        
        
        NSArray *perfis = [PerfilCliente MR_findAllInContext:oldMoc];
        for (PerfilCliente *obj in perfis) {
            PerfilCliente *perfil = [PerfilCliente MR_createInContext:localContext];
            perfil.codigo = obj.codigo;
            perfil.area = obj.area;
            perfil.cidade =  obj.cidade;
            perfil.descricao = obj.descricao;
            perfil.estado = obj.estado;
            perfil.quartos = obj.quartos;
            perfil.tipoImovel = obj.tipoImovel;
            perfil.tipoPerfil = obj.tipoPerfil;
            perfil.valor = obj.valor;
            perfil.version = obj.version;
            
            NSMutableArray *codigos = [NSMutableArray array];
            for (Bairro *bairro in [obj.bairro allObjects]) {
                [codigos addObject:bairro.codigo];
            }
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.codigo IN %@",codigos];
            NSArray *bairros = [Bairro MR_findAllWithPredicate:predicate inContext:localContext];
            perfil.bairro = [NSSet setWithArray:bairros];
        }
        
        
        NSArray *clientes = [Cliente MR_findAllInContext:oldMoc];
        for (Cliente *obj in clientes) {
            Cliente *cliente = [Cliente MR_createInContext:localContext];
            cliente.codigo = obj.codigo;
            cliente.nome = obj.nome;
            cliente.email = obj.email;
            cliente.foto = obj.foto;
            cliente.telefone = obj.telefone;
            cliente.telefoneExtra = obj.telefoneExtra;
            cliente.subtipo = obj.subtipo;
            cliente.version = obj.version;
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",obj.perfil.codigo];
            cliente.perfil = [PerfilCliente MR_findFirstWithPredicate:predicate inContext:localContext];
            
            cliente.endereco = [self convertEndereco:obj.endereco withMoc:localContext];
        }
        
        
        NSArray *clientesDeletados = [ClienteDeletado MR_findAllInContext:oldMoc];
        for (ClienteDeletado *obj in clientesDeletados) {
            ClienteDeletado *cliente = [ClienteDeletado MR_createInContext:localContext];
            cliente.codigo = obj.codigo;
            cliente.nome = obj.nome;
            cliente.email = obj.email;
            cliente.telefone = obj.telefone;
            cliente.telefoneExtra = obj.telefoneExtra;
        }
        
        
        NSArray *imoveis = [Imovel MR_findAllInContext:oldMoc];
        for (Imovel *obj in imoveis) {
            Imovel *imovel = [Imovel MR_createInContext:localContext];
            imovel.codigo = obj.codigo;
            imovel.areaUtil = obj.areaUtil;
            imovel.areaTotal = obj.areaTotal;
            imovel.comissao = obj.comissao;
            imovel.condominio = obj.condominio;
            imovel.descricao = obj.descricao;
            imovel.identificacao = obj.identificacao;
            imovel.notaPrivada = obj.notaPrivada;
            imovel.oferta = obj.oferta;
            imovel.quartos = obj.quartos;
            imovel.tipo = obj.tipo;
            imovel.valor = obj.valor;
            imovel.iptu = obj.iptu;
            imovel.version = obj.version;
            
            if (obj.tags) {
                imovel.tags = [self convertTag:obj.tags withMoc:localContext];
            }
            
            if (obj.endereco) {
                imovel.endereco = [self convertEndereco:obj.endereco withMoc:localContext];
            }
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",obj.cliente.codigo];
            imovel.cliente = [Cliente MR_findFirstWithPredicate:predicate inContext:localContext];
            
            NSMutableSet *fotos = [NSMutableSet set];
            for (FotoImovel *foto in [obj.fotos allObjects]) {
                FotoImovel *novaFoto = [FotoImovel MR_createInContext:localContext];
                novaFoto.codigo = foto.codigo;
                novaFoto.descricao = foto.descricao;
                novaFoto.capa = foto.capa;
                novaFoto.imagemURL = foto.imagemURL;
                novaFoto.version = [NSDate date];
                novaFoto.video = foto.video;
                novaFoto.importacao = foto.importacao;
                novaFoto.imovel = imovel;
                [fotos addObject:novaFoto];
            }
            
            imovel.fotos = fotos;
        }
        
        if ([Utils iCloudEnabledByUser] && [Utils iCloudAccountIsSignedIn]) {
            [Migrator migrateFotoImovelFromIcloudToLocalWithMoc:localContext];
        }
        
        
        NSArray *notas = [Nota MR_findAllInContext:oldMoc];
        for (Nota *obj in notas) {
            Nota *nota = [Nota MR_createInContext:localContext];
            nota.codigo = obj.codigo;
            nota.descricao = obj.descricao;
            nota.timeStamp = obj.timeStamp;
            nota.version = obj.version;
            
            if (obj.arquivo) {
                nota.arquivo = obj.arquivo;
                nota.extensao = obj.extensao;
            }
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",obj.cliente.codigo];
            nota.cliente = [Cliente MR_findFirstWithPredicate:predicate inContext:localContext];
        }
        
        
        NSArray *acompanhamentos = [Acompanhamento MR_findAllInContext:oldMoc];
        for (Acompanhamento *obj in acompanhamentos) {
            Acompanhamento *acompanhamento = [Acompanhamento MR_createInContext:localContext];
            acompanhamento.codigo = obj.codigo;
            
            if (obj.arquivo) {
                acompanhamento.arquivo = obj.arquivo;
                acompanhamento.extensao = obj.extensao;
            }
            
            acompanhamento.timeStamp = obj.timeStamp;
            acompanhamento.titulo = obj.titulo;
            acompanhamento.version = obj.version;
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",obj.cliente.codigo];
            acompanhamento.cliente =  [Cliente MR_findFirstWithPredicate:predicate inContext:localContext];
            
            predicate = [NSPredicate predicateWithFormat:@"codigo = %@",obj.imovel.codigo];
            acompanhamento.imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:localContext];
        }
        
        
        NSArray *negociacoes = [Negociacao MR_findAllInContext:oldMoc];
        for (Negociacao *obj in negociacoes) {
            Negociacao *negociacao = [Negociacao MR_createInContext:localContext];
            negociacao.codigo = obj.codigo;
            negociacao.comissao = obj.comissao;
            negociacao.data = obj.data;
            negociacao.notas = obj.notas;
            negociacao.parceria = obj.parceria;
            negociacao.percentualParceria = obj.percentualParceria;
            negociacao.status = obj.status;
            negociacao.valor = obj.valor;
            negociacao.version = obj.version;
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",obj.imovel.codigo];
            negociacao.imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:localContext];
            
            predicate = [NSPredicate predicateWithFormat:@"codigo = %@",obj.cliente.codigo];
            Cliente *cliente =  [Cliente MR_findFirstWithPredicate:predicate inContext:localContext];
            negociacao.cliente = cliente;
        }
        
        
        NSArray *eventos = [Evento MR_findAllInContext:oldMoc];
        for (Evento *obj in eventos) {
            Evento *evento = [Evento MR_createInContext:localContext];
            evento.codigo = obj.codigo;
            evento.dataFim = obj.dataFim;
            evento.dataInicio = obj.dataInicio;
            evento.identificadorEventoCalendario = obj.identificadorEventoCalendario;
            evento.local = obj.local;
            evento.notas = obj.notas;
            evento.titulo = obj.titulo;
            evento.version = obj.version;
            evento.tipoEvento = obj.tipoEvento;
            
            if (obj.alarmes) {
                NSArray *objAlarmes = [NSKeyedUnarchiver unarchiveObjectWithData:obj.alarmes];
                NSMutableArray *alarmes = [NSMutableArray new];
                
                for (NSNumber *objAlarme in objAlarmes) {
                    [alarmes addObject:objAlarme];
                }
                
                evento.alarmes =  [NSKeyedArchiver archivedDataWithRootObject:alarmes];
            }
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",obj.imovel.codigo];
            evento.imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:localContext];
            
            NSMutableSet *set = [NSMutableSet set];
            for (Cliente *objCliente in obj.cliente) {
                predicate = [NSPredicate predicateWithFormat:@"codigo = %@", objCliente.codigo];
                Cliente *cliente = [Cliente MR_findFirstWithPredicate:predicate inContext:localContext];
                [set addObject:cliente];
            }
            
            evento.cliente = set;
        }
        
        
        NSArray *corretorArray = [Corretor MR_findAllInContext:oldMoc];
        for (Corretor *obj in corretorArray) {
            Corretor *corretor = [Corretor MR_createInContext:localContext];
            corretor.nome = obj.nome;
            corretor.codigo = obj.codigo;
            corretor.dataExpiraPlanoTop = obj.dataExpiraPlanoTop;
            corretor.email = obj.email;
            corretor.planoTopValido = obj.planoTopValido;
            corretor.telefone = obj.telefone;
            corretor.version = obj.version;
        }
        
        
        NSArray *transacaoArray = [TransacaoInAppPurchase MR_findAllInContext:oldMoc];
        for (TransacaoInAppPurchase *obj in transacaoArray) {
            TransacaoInAppPurchase *transacao = [TransacaoInAppPurchase MR_createInContext:localContext];
            transacao.dataTransacao = obj.dataTransacao;
            transacao.identificadorProduto = obj.identificadorProduto;
            transacao.identificadorTransacao = obj.identificadorTransacao;
        }
        
        
        NSArray *smsArray = [Sms MR_findAllInContext:oldMoc];
        for (Sms *obj in smsArray) {
            Sms *sms = [Sms MR_createInContext:localContext];
            sms.codigoSMS = obj.codigoSMS;
            sms.dataEnvio = obj.dataEnvio;
            sms.respostaConfirmacao = obj.respostaConfirmacao;
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@", obj.cliente.codigo];
            sms.cliente = [Cliente MR_findFirstWithPredicate:predicate inContext:localContext];
            
            predicate = [NSPredicate predicateWithFormat:@"codigo = %@", obj.evento.codigo];
            sms.evento = [Evento MR_findFirstWithPredicate:predicate inContext:localContext];
        }
        
        
        
    } completion:^(BOOL success, NSError *error) {
        
        [self removeAllStoresFromCoordinator:oldMoc.persistentStoreCoordinator];
        
        [defaults setBool:YES forKey:ALREADY_MIGRATE_STORE_TO_PARSE];
        [defaults synchronize];
        
        [Utils dismissProgress];
        
        completion(success);
    }];
}

- (TagImovel *)convertTag:(TagImovel *)obj withMoc:(NSManagedObjectContext *)moc
{
    TagImovel *tag = [TagImovel MR_createInContext:moc];
    tag.codigo = obj.codigo;
    tag.armarioCozinha = obj.armarioCozinha;
    tag.armarioEmbutido = obj.armarioEmbutido;
    tag.childrenCare = obj.childrenCare;
    tag.churrasqueira = obj.churrasqueira;
    tag.garagem = obj.garagem;
    tag.piscina = obj.piscina;
    tag.playground = obj.playground;
    tag.quadraPoliesportiva = obj.quadraPoliesportiva;
    tag.salaGinastica = obj.salaGinastica;
    tag.salaoFestas = obj.salaoFestas;
    tag.salaoJogos = obj.salaoJogos;
    tag.sauna = obj.sauna;
    tag.varanda = obj.varanda;
    tag.wcEmpregada = obj.wcEmpregada;
    
    return tag;
}

- (Endereco *)convertEndereco:(Endereco *)obj withMoc:(NSManagedObjectContext *)moc
{
    if (!obj) {
        return nil;
    }
    
    Endereco *endereco = [Endereco MR_createInContext:moc];
    endereco.codigo = obj.codigo;
    endereco.cep = obj.cep;
    endereco.logradouro = obj.logradouro;
    endereco.complemento = obj.complemento;
    endereco.latitude = obj.latitude;
    endereco.longitude = obj.longitude;
    endereco.cidade = obj.cidade;
    endereco.estado = obj.estado;
    endereco.numero = obj.numero;
    endereco.version = obj.version;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@", obj.bairro.codigo];
    endereco.bairro = [Bairro MR_findFirstWithPredicate:predicate inContext:moc];
    
    return endereco;
}

- (void)removeAllStoresFromCoordinator:(NSPersistentStoreCoordinator*)psc
{
    for (NSPersistentStore *s in psc.persistentStores) {
        NSError *error = nil;
        if (![psc removePersistentStore:s error:&error]) {
            NSLog(@"Error removing persistent store: %@", error);
        }
    }
}

+ (void)migrateFotoImovelFromCoreDataToDisk:(NSManagedObjectContext *)moc
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"imagem != nil"];
    NSArray *arrayFotoImovel = [FotoImovel MR_findAllWithPredicate:predicate inContext:moc];
    
    if (arrayFotoImovel.count == 0) {
        return;
    }
    
    [Utils showProgress:@"Ajustando fotos dos imóveis. Aguarde..."];
    
    for (FotoImovel *fotoImovel in arrayFotoImovel) {
        [fotoImovel insertImageOnDisk:fotoImovel.imagem];
        fotoImovel.imagem = nil;
    }
    
    [Utils saveMOC:moc];
    
    [Utils dismissProgress];
}


#pragma mark - Migrate FotoImovel

+ (void)migrateFotoImovelFromIcloudToLocalWithMoc:(NSManagedObjectContext *)moc
{
    NSArray *arrayFotoImovel = [CoreDataUtils fetchEntity:@"FotoImovel" withPredicate:nil inContext:moc];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSURL *localURL = [Utils getLocalFotosURL];
    
    NSURL *icloudURL = [Utils getIcloudDocumentsURL];
    
    NSError *error;
    
    for (FotoImovel *fotoImovel in arrayFotoImovel) {
        [fileManager copyItemAtURL:[icloudURL URLByAppendingPathComponent:fotoImovel.imagemURL]
                             toURL:[localURL URLByAppendingPathComponent:fotoImovel.imagemURL]
                             error:&error];
        
    }
}


#pragma mark - Methods used for load old store and migrate.

- (void)loadOldStoreWithURL:(NSURL *)url
{
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"BrEstate.momd/Appci-v Novo 7" withExtension:@"mom"];
    _migrationModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    _migrationCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:_migrationModel];
    
    _migrationContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_migrationContext setPersistentStoreCoordinator:_migrationCoordinator];
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSReadOnlyPersistentStoreOption, nil];
    
    NSError *error = nil;
    _migrationStore = [_migrationCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                          configuration:nil
                                                                    URL:url
                                                                options:options
                                                                  error:&error];
    if (!_migrationStore) {
        NSLog(@"Failed to add store. Error: %@", error);
    }
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSURL *)applicationStoresDirectory
{
    NSURL *storesDirectory = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Stores"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:[storesDirectory path]]) {
        NSError *error = nil;
        if ([fileManager createDirectoryAtURL:storesDirectory
                  withIntermediateDirectories:YES
                                   attributes:nil
                                        error:&error]) {
            //NSLog(@"Successfully created Stores directory");
        }
        else {NSLog(@"FAILED to create Stores directory: %@", error);}
    }
    return storesDirectory;
}

- (NSURL *)getURLOldStore
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    
    NSArray *keys = [NSArray arrayWithObject:NSURLIsDirectoryKey];
    
    NSDirectoryEnumerator *enumerator = [fileManager enumeratorAtURL:[self applicationStoresDirectory]
                                          includingPropertiesForKeys:keys
                                                             options:0
                                                        errorHandler:^(NSURL *url, NSError *error) {
                                                            
                                                            // Handle the error.
                                                            
                                                            // Return YES if the enumeration should continue after the error.
                                                            
                                                            return YES;
                                                            
                                                        }];
    
    NSString *sqliteName;
    
    if ([Utils iCloudAccountIsSignedIn] && [Utils iCloudEnabledByUser]) {
        sqliteName = ICLOUD_STORE_FILE_NAME;
    } else {
        sqliteName = STORE_FILE_NAME;
    }
    
    for (NSURL *url in enumerator) {
        
        NSError *error;
        NSNumber *isDirectory = nil;
        
        if (![url getResourceValue:&isDirectory forKey:NSURLIsDirectoryKey error:&error]) {
            NSLog(@"%@",url);
            
        } else if (![isDirectory boolValue]) {
            NSString *urlString = url.absoluteString;
            
            if ([urlString rangeOfString:sqliteName].location != NSNotFound) {
                return url;
            }
        }
    }
    
    return nil;
}



@end
