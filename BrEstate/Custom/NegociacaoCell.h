//
//  NegociacaoCell.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/27/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NegociacaoCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *tipoImageView;
@property (nonatomic, weak) IBOutlet UIImageView *imovelImageView;
@property (nonatomic, weak) IBOutlet UIImageView *contatoImageView;
@property (nonatomic, weak) IBOutlet UILabel *nomeContatoLabel;
@property (nonatomic, weak) IBOutlet UILabel *valorLabel;
@property (nonatomic, weak) IBOutlet UILabel *descricaoLabel;

@property (nonatomic, assign) long numberOfTaps;
@end
