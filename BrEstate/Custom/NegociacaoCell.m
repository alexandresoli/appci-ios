//
//  NegociacaoCell.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/27/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "NegociacaoCell.h"

@implementation NegociacaoCell

- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super initWithCoder:decoder]) {
        
        [self setBackgroundView];
        
    }
    return self;
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setBackgroundView
{
//    UIView *bgColorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
//    bgColorView.backgroundColor = [UIColor colorWithRed:253.0f/255 green:145.0f/255 blue:117.0f/255 alpha:1];
//    self.selectedBackgroundView = bgColorView;
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *aTouch = [touches anyObject];
    self.numberOfTaps = [aTouch tapCount];
    [super touchesEnded:touches withEvent:event];
}

@end
