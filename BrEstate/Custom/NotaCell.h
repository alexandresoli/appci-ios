//
//  NotaCell.h
//  BrEstate
//
//  Created by BrEstate LTDA on 2/6/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface NotaCell : SWTableViewCell

@property (nonatomic, weak) IBOutlet UILabel *descricao;
@property (nonatomic, weak) IBOutlet UIImageView *anexo;
@property (nonatomic, weak) IBOutlet UILabel *dia;
@property (nonatomic, weak) IBOutlet UILabel *hora;
@property (nonatomic, weak) IBOutlet UIImageView *selecionada;

@end
