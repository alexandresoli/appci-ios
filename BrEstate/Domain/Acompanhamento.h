//
//  Acompanhamento.h
//  Appci
//
//  Created by BrEstate LTDA on 3/25/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Cliente, Imovel;

@interface Acompanhamento : NSManagedObject

@property (nonatomic, retain) NSData * arquivo;
@property (nonatomic, retain) NSString * extensao;
@property (nonatomic, retain) NSNumber * codigo;
@property (nonatomic, retain) NSString * sectionIdentifier;
@property (nonatomic, retain) NSDate * timeStamp;
@property (nonatomic, retain) NSString * titulo;
@property (nonatomic, retain) NSDate * version;
@property (nonatomic, retain) Cliente *cliente;
@property (nonatomic, retain) Imovel *imovel;
@property (nonatomic, retain) NSString * parseID;
@property (nonatomic, retain) NSNumber * pendingCreateParse;
@property (nonatomic, retain) NSNumber * pendingUpdateParse;
@property (nonatomic, retain) NSString * updatedBy;
@property (nonatomic, retain) NSString * createdBy;
@property (nonatomic, retain) NSString * firebaseID;
@property (nonatomic, retain) NSNumber * isSavedInFirebase;
@property (nonatomic, retain) NSNumber * pendingUpload;
@property (nonatomic, retain) NSNumber * pendingDownload;
@property (nonatomic, retain) NSString * filename;

@end
