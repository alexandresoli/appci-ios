//
//  Acompanhamento.m
//  Appci
//
//  Created by BrEstate LTDA on 3/25/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Acompanhamento.h"
#import "Cliente.h"
#import "Imovel.h"

@interface Acompanhamento ()

@property (nonatomic) NSDate *primitiveTimeStamp;
@property (nonatomic) NSString *primitiveSectionIdentifier;

@end


@implementation Acompanhamento

@dynamic arquivo;
@dynamic extensao;
@dynamic codigo;
@dynamic sectionIdentifier;
@dynamic timeStamp;
@dynamic titulo;
@dynamic version;
@dynamic cliente;
@dynamic imovel;
@dynamic primitiveTimeStamp;
@dynamic primitiveSectionIdentifier;
@dynamic parseID;
@dynamic pendingCreateParse;
@dynamic pendingUpdateParse;
@dynamic updatedBy;
@dynamic createdBy;
@dynamic firebaseID;
@dynamic isSavedInFirebase;
@dynamic pendingUpload;
@dynamic pendingDownload;
@dynamic filename;

#pragma mark - Transient properties

- (NSString *)sectionIdentifier
{
    // Create and cache the section identifier on demand.
    
    [self willAccessValueForKey:@"sectionIdentifier"];
    NSString *tmp = [self primitiveSectionIdentifier];
    [self didAccessValueForKey:@"sectionIdentifier"];
    
    if (!tmp)
    {
        /*
         Sections are organized by month and year. Create the section identifier as a string representing the number (year * 1000) + month; this way they will be correctly ordered chronologically regardless of the actual name of the month.
         */
        NSCalendar *calendar = [NSCalendar currentCalendar];
        
        NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:[self timeStamp]];
        
        long date = ([components year] * 1000) + [components month];
        
        tmp = [NSString stringWithFormat:@"%ld", date];
        [self setPrimitiveSectionIdentifier:tmp];
    }
    return tmp;
}

#pragma mark - Time stamp setter

- (void)setTimeStamp:(NSDate *)newDate
{
    // If the time stamp changes, the section identifier become invalid.
    [self willChangeValueForKey:@"timeStamp"];
    [self setPrimitiveTimeStamp:newDate];
    [self didChangeValueForKey:@"timeStamp"];
    
    [self setPrimitiveSectionIdentifier:nil];
}


#pragma mark - Key path dependencies

+ (NSSet *)keyPathsForValuesAffectingSectionIdentifier
{
    // If the value of timeStamp changes, the section identifier may change as well.
    return [NSSet setWithObject:@"timeStamp"];
}


@end
