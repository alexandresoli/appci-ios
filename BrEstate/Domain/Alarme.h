//
//  Alarme.h
//  Appci
//
//  Created by BrEstate LTDA on 4/9/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@class Evento;
@interface Alarme : NSManagedObject

@property (nonatomic, retain) NSNumber * codigo;
@property (nonatomic, retain) NSString * descricao;
@property (nonatomic, retain) NSNumber * tempo;

@end
