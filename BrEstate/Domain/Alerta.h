//
//  Alerta.h
//  Appci
//
//  Created by BrEstate LTDA on 4/9/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Alerta : NSManagedObject

@property (nonatomic, retain) NSNumber * ativo;
@property (nonatomic, retain) NSNumber * codigo;
@property (nonatomic, retain) NSString * descricao;
@property (nonatomic, retain) NSString * tipo;

@end
