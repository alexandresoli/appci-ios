//
//  Bairro.h
//  Appci
//
//  Created by Leonardo Barros on 06/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Endereco, PerfilCliente;

@interface Bairro : NSManagedObject

@property (nonatomic, retain) NSString * cidade;
@property (nonatomic, retain) NSNumber * codigo;
@property (nonatomic, retain) NSString * createdBy;
@property (nonatomic, retain) NSString * nome;
@property (nonatomic, retain) NSString * parseID;
@property (nonatomic, retain) NSNumber * pendingCreateParse;
@property (nonatomic, retain) NSNumber * pendingUpdateParse;
@property (nonatomic, retain) NSString * updatedBy;
@property (nonatomic, retain) NSDate * version;
@property (nonatomic, retain) NSString * firebaseID;
@property (nonatomic, retain) NSNumber * isSavedInFirebase;
@property (nonatomic, retain) NSSet *endereco;
@property (nonatomic, retain) NSSet *perfil;
@end

@interface Bairro (CoreDataGeneratedAccessors)

- (void)addEnderecoObject:(Endereco *)value;
- (void)removeEnderecoObject:(Endereco *)value;
- (void)addEndereco:(NSSet *)values;
- (void)removeEndereco:(NSSet *)values;

- (void)addPerfilObject:(PerfilCliente *)value;
- (void)removePerfilObject:(PerfilCliente *)value;
- (void)addPerfil:(NSSet *)values;
- (void)removePerfil:(NSSet *)values;

@end
