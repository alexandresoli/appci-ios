//
//  Bairro.m
//  Appci
//
//  Created by Leonardo Barros on 06/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "Bairro.h"
#import "Endereco.h"
#import "PerfilCliente.h"


@implementation Bairro

@dynamic cidade;
@dynamic codigo;
@dynamic createdBy;
@dynamic nome;
@dynamic parseID;
@dynamic pendingCreateParse;
@dynamic pendingUpdateParse;
@dynamic updatedBy;
@dynamic version;
@dynamic firebaseID;
@dynamic isSavedInFirebase;
@dynamic endereco;
@dynamic perfil;

@end
