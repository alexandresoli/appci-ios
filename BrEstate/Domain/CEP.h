//
//  CEP.h
//  Appci
//
//  Created by BrEstate LTDA on 6/20/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CEP : NSObject

@property (nonatomic, strong) NSString *uf;
@property (nonatomic, strong) NSString *cidade;
@property (nonatomic, strong) NSString *bairro;
@property (nonatomic, strong) NSString *tipoLogradouro;
@property (nonatomic, strong) NSString *logradouro;

@end
