//
//  Cliente.h
//  Appci
//
//  Created by Leonardo Barros on 06/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Acompanhamento, Endereco, Evento, Imovel, Negociacao, Nota, PerfilCliente, Sms;

@interface Cliente : NSManagedObject

@property (nonatomic, retain) NSNumber * codigo;
@property (nonatomic, retain) NSString * createdBy;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSData * foto;
@property (nonatomic, retain) NSString * nome;
@property (nonatomic, retain) NSString * parseID;
@property (nonatomic, retain) NSNumber * pendingCreateParse;
@property (nonatomic, retain) NSNumber * pendingUpdateParse;
@property (nonatomic, retain) NSNumber * subtipo;
@property (nonatomic, retain) NSString * telefone;
@property (nonatomic, retain) NSString * telefoneExtra;
@property (nonatomic, retain) NSString * updatedBy;
@property (nonatomic, retain) NSDate * version;
@property (nonatomic, retain) NSString * firebaseID;
@property (nonatomic, retain) NSNumber * isSavedInFirebase;
@property (nonatomic, retain) NSSet *acompanhamento;
@property (nonatomic, retain) Endereco *endereco;
@property (nonatomic, retain) NSSet *evento;
@property (nonatomic, retain) NSSet *imovel;
@property (nonatomic, retain) NSSet *negociacao;
@property (nonatomic, retain) NSSet *nota;
@property (nonatomic, retain) PerfilCliente *perfil;
@property (nonatomic, retain) NSSet *sms;
@end

@interface Cliente (CoreDataGeneratedAccessors)

- (void)addAcompanhamentoObject:(Acompanhamento *)value;
- (void)removeAcompanhamentoObject:(Acompanhamento *)value;
- (void)addAcompanhamento:(NSSet *)values;
- (void)removeAcompanhamento:(NSSet *)values;

- (void)addEventoObject:(Evento *)value;
- (void)removeEventoObject:(Evento *)value;
- (void)addEvento:(NSSet *)values;
- (void)removeEvento:(NSSet *)values;

- (void)addImovelObject:(Imovel *)value;
- (void)removeImovelObject:(Imovel *)value;
- (void)addImovel:(NSSet *)values;
- (void)removeImovel:(NSSet *)values;

- (void)addNegociacaoObject:(Negociacao *)value;
- (void)removeNegociacaoObject:(Negociacao *)value;
- (void)addNegociacao:(NSSet *)values;
- (void)removeNegociacao:(NSSet *)values;

- (void)addNotaObject:(Nota *)value;
- (void)removeNotaObject:(Nota *)value;
- (void)addNota:(NSSet *)values;
- (void)removeNota:(NSSet *)values;

- (void)addSmsObject:(Sms *)value;
- (void)removeSmsObject:(Sms *)value;
- (void)addSms:(NSSet *)values;
- (void)removeSms:(NSSet *)values;

@end
