//
//  Cliente.m
//  Appci
//
//  Created by Leonardo Barros on 06/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "Cliente.h"
#import "Acompanhamento.h"
#import "Endereco.h"
#import "Evento.h"
#import "Imovel.h"
#import "Negociacao.h"
#import "Nota.h"
#import "PerfilCliente.h"
#import "Sms.h"


@implementation Cliente

@dynamic codigo;
@dynamic createdBy;
@dynamic email;
@dynamic foto;
@dynamic nome;
@dynamic parseID;
@dynamic pendingCreateParse;
@dynamic pendingUpdateParse;
@dynamic subtipo;
@dynamic telefone;
@dynamic telefoneExtra;
@dynamic updatedBy;
@dynamic version;
@dynamic firebaseID;
@dynamic isSavedInFirebase;
@dynamic acompanhamento;
@dynamic endereco;
@dynamic evento;
@dynamic imovel;
@dynamic negociacao;
@dynamic nota;
@dynamic perfil;
@dynamic sms;

@end
