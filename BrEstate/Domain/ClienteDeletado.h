//
//  ClienteDeletado.h
//  Appci
//
//  Created by BrEstate LTDA on 26/05/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ClienteDeletado : NSManagedObject

@property (nonatomic, retain) NSNumber * codigo;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * nome;
@property (nonatomic, retain) NSString * telefone;
@property (nonatomic, retain) NSString * telefoneExtra;
@property (nonatomic, retain) NSString * parseID;
@property (nonatomic, retain) NSNumber * pendingCreateParse;
@property (nonatomic, retain) NSNumber * pendingUpdateParse;
@property (nonatomic, retain) NSDate * version;
@property (nonatomic, retain) NSString * updatedBy;
@property (nonatomic, retain) NSString * createdBy;
@property (nonatomic, retain) NSString * firebaseID;
@property (nonatomic, retain) NSNumber * isSavedInFirebase;

@end
