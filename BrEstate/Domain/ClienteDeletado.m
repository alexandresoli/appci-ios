//
//  ClienteDeletado.m
//  Appci
//
//  Created by BrEstate LTDA on 26/05/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "ClienteDeletado.h"


@implementation ClienteDeletado

@dynamic codigo;
@dynamic email;
@dynamic nome;
@dynamic telefone;
@dynamic telefoneExtra;
@dynamic parseID;
@dynamic pendingUpdateParse;
@dynamic pendingCreateParse;
@dynamic version;
@dynamic updatedBy;
@dynamic createdBy;
@dynamic firebaseID;
@dynamic isSavedInFirebase;

@end
