//
//  Corretor.h
//  Appci
//
//  Created by Leonardo Barros on 09/12/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Corretor : NSManagedObject

@property (nonatomic, retain) NSNumber * codigo;
@property (nonatomic, retain) NSDate * dataExpiraPlanoTop;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * nome;
@property (nonatomic, retain) NSString * parseID;
@property (nonatomic, retain) NSData * password;
@property (nonatomic, retain) NSNumber * pendingCreateParse;
@property (nonatomic, retain) NSNumber * pendingUpdateParse;
@property (nonatomic, retain) NSNumber * planoTopValido;
@property (nonatomic, retain) NSString * telefone;
@property (nonatomic, retain) NSDate * version;
@property (nonatomic, retain) NSNumber * limiteDevices;

@end
