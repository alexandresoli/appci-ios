//
//  Corretor.m
//  Appci
//
//  Created by Leonardo Barros on 09/12/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Corretor.h"


@implementation Corretor

@dynamic codigo;
@dynamic dataExpiraPlanoTop;
@dynamic email;
@dynamic nome;
@dynamic parseID;
@dynamic password;
@dynamic pendingCreateParse;
@dynamic pendingUpdateParse;
@dynamic planoTopValido;
@dynamic telefone;
@dynamic version;
@dynamic limiteDevices;

@end
