//
//  Endereco.h
//  Appci
//
//  Created by BrEstate LTDA on 4/9/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Bairro, Cliente, Imovel;

@interface Endereco : NSManagedObject

@property (nonatomic, retain) NSNumber * cep;
@property (nonatomic, retain) NSString * cidade;
@property (nonatomic, retain) NSNumber * codigo;
@property (nonatomic, retain) NSString * complemento;
@property (nonatomic, retain) NSString * estado;
@property (nonatomic, retain) NSString * logradouro;
@property (nonatomic, retain) NSNumber * numero;
@property (nonatomic, retain) Bairro *bairro;
@property (nonatomic, retain) NSSet *cliente;
@property (nonatomic, retain) NSSet *imovel;
@property (nonatomic, retain) NSNumber *latitude;
@property (nonatomic, retain) NSNumber *longitude;
@property (nonatomic, retain) NSString * parseID;
@property (nonatomic, retain) NSDate * version;
@property (nonatomic, retain) NSNumber * pendingCreateParse;
@property (nonatomic, retain) NSNumber * pendingUpdateParse;
@property (nonatomic, retain) NSString * updatedBy;
@property (nonatomic, retain) NSString * createdBy;

@end

@interface Endereco (CoreDataGeneratedAccessors)

- (void)addClienteObject:(Cliente *)value;
- (void)removeClienteObject:(Cliente *)value;
- (void)addCliente:(NSSet *)values;
- (void)removeCliente:(NSSet *)values;

- (void)addImovelObject:(Imovel *)value;
- (void)removeImovelObject:(Imovel *)value;
- (void)addImovel:(NSSet *)values;
- (void)removeImovel:(NSSet *)values;

@end
