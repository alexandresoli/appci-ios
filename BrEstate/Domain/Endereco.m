//
//  Endereco.m
//  Appci
//
//  Created by BrEstate LTDA on 4/9/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Endereco.h"
#import "Bairro.h"
#import "Cliente.h"
#import "Imovel.h"

@implementation Endereco

@dynamic cep;
@dynamic cidade;
@dynamic codigo;
@dynamic complemento;
@dynamic estado;
@dynamic logradouro;
@dynamic numero;
@dynamic bairro;
@dynamic cliente;
@dynamic imovel;
@dynamic latitude;
@dynamic longitude;
@dynamic parseID;
@dynamic version;
@dynamic pendingCreateParse;
@dynamic pendingUpdateParse;
@dynamic updatedBy;
@dynamic createdBy;

@end
