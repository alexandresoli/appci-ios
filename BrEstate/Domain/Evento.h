//
//  Evento.h
//  Appci
//
//  Created by BrEstate LTDA on 08/09/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Cliente, Imovel, Sms;

@interface Evento : NSManagedObject

@property (nonatomic, retain) NSData * alarmes;
@property (nonatomic, retain) NSNumber * codigo;
@property (nonatomic, retain) NSDate * dataFim;
@property (nonatomic, retain) NSDate * dataInicio;
@property (nonatomic, retain) NSString * identificadorEventoCalendario;
@property (nonatomic, retain) NSNumber *recursivo;
@property (nonatomic, retain) NSString * local;
@property (nonatomic, retain) NSString * notas;
@property (nonatomic, retain) NSString * parseID;
@property (nonatomic, retain) NSString * sectionIdentifier;
@property (nonatomic, retain) NSString * tipoEvento;
@property (nonatomic, retain) NSString * titulo;
@property (nonatomic, retain) NSDate * version;
@property (nonatomic, retain) NSNumber * pendingCreateParse;
@property (nonatomic, retain) NSNumber * pendingUpdateParse;
@property (nonatomic, retain) NSSet *cliente;
@property (nonatomic, retain) Imovel *imovel;
@property (nonatomic, retain) NSSet *sms;
@property (nonatomic, retain) NSString * updatedBy;
@property (nonatomic, retain) NSString * createdBy;
@property (nonatomic, retain) NSString * firebaseID;
@property (nonatomic, retain) NSNumber * isSavedInFirebase;


@end

@interface Evento (CoreDataGeneratedAccessors)

- (void)addClienteObject:(Cliente *)value;
- (void)removeClienteObject:(Cliente *)value;
- (void)addCliente:(NSSet *)values;
- (void)removeCliente:(NSSet *)values;

- (void)addSmsObject:(Sms *)value;
- (void)removeSmsObject:(Sms *)value;
- (void)addSms:(NSSet *)values;
- (void)removeSms:(NSSet *)values;

@end
