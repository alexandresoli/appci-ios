//
//  Evento.m
//  Appci
//
//  Created by BrEstate LTDA on 08/09/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Evento.h"
#import "Cliente.h"
#import "Imovel.h"
#import "Sms.h"


@interface Evento ()

@property (nonatomic) NSDate *primitiveDataInicio;
@property (nonatomic) NSString *primitiveSectionIdentifier;

@end


@implementation Evento

@dynamic alarmes;
@dynamic codigo;
@dynamic dataFim;
@dynamic dataInicio;
@dynamic identificadorEventoCalendario;
@dynamic recursivo;
@dynamic local;
@dynamic notas;
@dynamic parseID;
@dynamic sectionIdentifier;
@dynamic tipoEvento;
@dynamic titulo;
@dynamic version;
@dynamic pendingCreateParse;
@dynamic pendingUpdateParse;
@dynamic cliente;
@dynamic imovel;
@dynamic sms;
@dynamic primitiveDataInicio;
@dynamic primitiveSectionIdentifier;
@dynamic updatedBy;
@dynamic createdBy;
@dynamic firebaseID;
@dynamic isSavedInFirebase;


#pragma mark - Transient properties

- (NSString *)sectionIdentifier
{
    // Create and cache the section identifier on demand.
    
    [self willAccessValueForKey:@"sectionIdentifier"];
    NSString *tmp = [self primitiveSectionIdentifier];
    [self didAccessValueForKey:@"sectionIdentifier"];
    
    if (!tmp)
    {
        /*
         Sections are organized by day, month and year. Create the section identifier as a string representing the number (year * 1000) + (month * 100) + day; this way they will be correctly ordered chronologically regardless of the actual name of the month.
         */
        //        NSCalendar *calendar = [NSCalendar currentCalendar];
        //
        //        NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:[self dataInicio]];
        //        tmp = [NSString stringWithFormat:@"%d", ([components year] * 1000) + ([components month] * 100) + [components day] ];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyyMMdd"];
        tmp = [dateFormat stringFromDate:self.dataInicio];
        
        [self setPrimitiveSectionIdentifier:tmp];
    }
    return tmp;
}


#pragma mark - Data Inicio setter

- (void)setDataInicio:(NSDate *)dataInicio
{
    // If data inicio changes, the section identifier become invalid.
    [self willChangeValueForKey:@"dataInicio"];
    [self setPrimitiveDataInicio:dataInicio];
    [self didChangeValueForKey:@"dataInicio"];
    
    [self setPrimitiveSectionIdentifier:nil];
}


#pragma mark - Key path dependencies

+ (NSSet *)keyPathsForValuesAffectingSectionIdentifier
{
    // If the value of timeStamp changes, the section identifier may change as well.
    return [NSSet setWithObject:@"dataInicio"];
}


@end
