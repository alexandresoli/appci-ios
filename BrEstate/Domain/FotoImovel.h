//
//  FotoImovel.h
//  Appci
//
//  Created by Leonardo Barros on 06/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Imovel;

@interface FotoImovel : NSManagedObject

@property (nonatomic, retain) NSNumber * capa;
@property (nonatomic, retain) NSNumber * codigo;
@property (nonatomic, retain) NSNumber * codigoAnterior;
@property (nonatomic, retain) NSString * createdBy;
@property (nonatomic, retain) NSString * descricao;
@property (nonatomic, retain) NSData * imagem;
@property (nonatomic, retain) NSString * imagemURL;
@property (nonatomic, retain) NSNumber * importacao;
@property (nonatomic, retain) NSString * parseID;
@property (nonatomic, retain) NSNumber * pendingCreateParse;
@property (nonatomic, retain) NSNumber * pendingUpdateParse;
@property (nonatomic, retain) NSString * updatedBy;
@property (nonatomic, retain) NSDate * version;
@property (nonatomic, retain) NSNumber * video;
@property (nonatomic, retain) NSString * firebaseID;
@property (nonatomic, retain) NSNumber * isSavedInFirebase;
@property (nonatomic, retain) Imovel *imovel;
@property (nonatomic, retain) NSNumber * pendingUpload;
@property (nonatomic, retain) NSNumber * pendingDownload;
@end
