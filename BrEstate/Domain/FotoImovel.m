//
//  FotoImovel.m
//  Appci
//
//  Created by Leonardo Barros on 06/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "FotoImovel.h"
#import "Imovel.h"


@implementation FotoImovel

@dynamic capa;
@dynamic codigo;
@dynamic codigoAnterior;
@dynamic createdBy;
@dynamic descricao;
@dynamic imagem;
@dynamic imagemURL;
@dynamic importacao;
@dynamic parseID;
@dynamic pendingCreateParse;
@dynamic pendingUpdateParse;
@dynamic updatedBy;
@dynamic version;
@dynamic video;
@dynamic firebaseID;
@dynamic isSavedInFirebase;
@dynamic imovel;
@dynamic pendingUpload;
@dynamic pendingDownload;

@end
