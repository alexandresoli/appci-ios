//
//  Imobiliaria.h
//  Appci
//
//  Created by Alexandre Oliveira on 11/3/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Imobiliaria : NSManagedObject

@property (nonatomic, retain) NSString * nome;
@property (nonatomic, retain) NSDate * expiration;
@property (nonatomic, retain) NSDate * version;
@property (nonatomic, retain) NSString * parseID;

@end
