//
//  Imobiliaria.m
//  Appci
//
//  Created by Alexandre Oliveira on 11/3/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Imobiliaria.h"


@implementation Imobiliaria

@dynamic nome;
@dynamic expiration;
@dynamic version;
@dynamic parseID;

@end
