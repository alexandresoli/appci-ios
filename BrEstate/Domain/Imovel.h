//
//  Imovel.h
//  Appci
//
//  Created by Leonardo Barros on 06/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Acompanhamento, Cliente, Endereco, Evento, FotoImovel, Negociacao, TagImovel;

@interface Imovel : NSManagedObject

@property (nonatomic, retain) NSNumber * areaTotal;
@property (nonatomic, retain) NSNumber * areaUtil;
@property (nonatomic, retain) NSNumber * codigo;
@property (nonatomic, retain) NSNumber * codigoAnterior;
@property (nonatomic, retain) NSNumber * comissao;
@property (nonatomic, retain) NSDecimalNumber * condominio;
@property (nonatomic, retain) NSString * createdBy;
@property (nonatomic, retain) NSString * descricao;
@property (nonatomic, retain) NSString * identificacao;
@property (nonatomic, retain) NSNumber * importacao;
@property (nonatomic, retain) NSDecimalNumber * iptu;
@property (nonatomic, retain) NSDate * lastUpdate;
@property (nonatomic, retain) NSString * notaPrivada;
@property (nonatomic, retain) NSString * oferta;
@property (nonatomic, retain) NSString * parseID;
@property (nonatomic, retain) NSNumber * pendingCreateParse;
@property (nonatomic, retain) NSNumber * pendingUpdateParse;
@property (nonatomic, retain) NSNumber * quartos;
@property (nonatomic, retain) NSString * tipo;
@property (nonatomic, retain) NSString * updatedBy;
@property (nonatomic, retain) NSDecimalNumber * valor;
@property (nonatomic, retain) NSDate * version;
@property (nonatomic, retain) NSString * firebaseID;
@property (nonatomic, retain) NSNumber * isSavedInFirebase;
@property (nonatomic, retain) NSSet *acompanhamento;
@property (nonatomic, retain) Cliente *cliente;
@property (nonatomic, retain) Endereco *endereco;
@property (nonatomic, retain) NSSet *evento;
@property (nonatomic, retain) NSSet *fotos;
@property (nonatomic, retain) NSSet *negociacao;
@property (nonatomic, retain) TagImovel *tags;
@end

@interface Imovel (CoreDataGeneratedAccessors)

- (void)addAcompanhamentoObject:(Acompanhamento *)value;
- (void)removeAcompanhamentoObject:(Acompanhamento *)value;
- (void)addAcompanhamento:(NSSet *)values;
- (void)removeAcompanhamento:(NSSet *)values;

- (void)addEventoObject:(Evento *)value;
- (void)removeEventoObject:(Evento *)value;
- (void)addEvento:(NSSet *)values;
- (void)removeEvento:(NSSet *)values;

- (void)addFotosObject:(FotoImovel *)value;
- (void)removeFotosObject:(FotoImovel *)value;
- (void)addFotos:(NSSet *)values;
- (void)removeFotos:(NSSet *)values;

- (void)addNegociacaoObject:(Negociacao *)value;
- (void)removeNegociacaoObject:(Negociacao *)value;
- (void)addNegociacao:(NSSet *)values;
- (void)removeNegociacao:(NSSet *)values;

@end
