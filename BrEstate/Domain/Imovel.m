//
//  Imovel.m
//  Appci
//
//  Created by Leonardo Barros on 06/01/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "Imovel.h"
#import "Acompanhamento.h"
#import "Cliente.h"
#import "Endereco.h"
#import "Evento.h"
#import "FotoImovel.h"
#import "Negociacao.h"
#import "TagImovel.h"


@implementation Imovel

@dynamic areaTotal;
@dynamic areaUtil;
@dynamic codigo;
@dynamic codigoAnterior;
@dynamic comissao;
@dynamic condominio;
@dynamic createdBy;
@dynamic descricao;
@dynamic identificacao;
@dynamic importacao;
@dynamic iptu;
@dynamic lastUpdate;
@dynamic notaPrivada;
@dynamic oferta;
@dynamic parseID;
@dynamic pendingCreateParse;
@dynamic pendingUpdateParse;
@dynamic quartos;
@dynamic tipo;
@dynamic updatedBy;
@dynamic valor;
@dynamic version;
@dynamic firebaseID;
@dynamic isSavedInFirebase;
@dynamic acompanhamento;
@dynamic cliente;
@dynamic endereco;
@dynamic evento;
@dynamic fotos;
@dynamic negociacao;
@dynamic tags;

@end
