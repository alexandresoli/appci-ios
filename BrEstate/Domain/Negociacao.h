//
//  Negociacao.h
//  Appci
//
//  Created by BrEstate LTDA on 3/25/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Cliente, Imovel;

@interface Negociacao : NSManagedObject

@property (nonatomic, retain) NSNumber * codigo;
@property (nonatomic, retain) NSNumber * comissao;
@property (nonatomic, retain) NSDate * data;
@property (nonatomic, retain) NSString * notas;
@property (nonatomic, retain) NSNumber * parceria;
@property (nonatomic, retain) NSNumber * percentualParceria;
@property (nonatomic, retain) NSDecimalNumber * valor;
@property (nonatomic, retain) NSDate * version;
@property (nonatomic, retain) Cliente *cliente;
@property (nonatomic, retain) Imovel *imovel;
@property (nonatomic, retain) NSString *status;
@property (nonatomic, retain) NSString *parseID;
@property (nonatomic, retain) NSNumber * pendingCreateParse;
@property (nonatomic, retain) NSNumber * pendingUpdateParse;
@property (nonatomic, retain) NSString * updatedBy;
@property (nonatomic, retain) NSString * createdBy;
@property (nonatomic, retain) NSString * firebaseID;
@property (nonatomic, retain) NSNumber * isSavedInFirebase;

@end
