//
//  Negociacao.m
//  Appci
//
//  Created by BrEstate LTDA on 3/25/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Negociacao.h"
#import "Cliente.h"
#import "Imovel.h"


@implementation Negociacao

@dynamic codigo;
@dynamic comissao;
@dynamic data;
@dynamic notas;
@dynamic parceria;
@dynamic percentualParceria;
@dynamic valor;
@dynamic version;
@dynamic cliente;
@dynamic imovel;
@dynamic status;
@dynamic parseID;
@dynamic pendingCreateParse;
@dynamic pendingUpdateParse;
@dynamic updatedBy;
@dynamic createdBy;
@dynamic firebaseID;
@dynamic isSavedInFirebase;

@end
