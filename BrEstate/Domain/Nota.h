//
//  Nota.h
//  Appci
//
//  Created by BrEstate LTDA on 4/6/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Cliente;

@interface Nota : NSManagedObject

@property (nonatomic, retain) NSData * arquivo;
@property (nonatomic, retain) NSNumber * codigo;
@property (nonatomic, retain) NSString * descricao;
@property (nonatomic, retain) NSString * extensao;
@property (nonatomic, retain) NSString * sectionIdentifier;
@property (nonatomic, retain) NSDate * timeStamp;
@property (nonatomic, retain) NSDate * version;
@property (nonatomic, retain) Cliente *cliente;
@property (nonatomic, retain) NSString * parseID;
@property (nonatomic, retain) NSNumber * pendingCreateParse;
@property (nonatomic, retain) NSNumber * pendingUpdateParse;
@property (nonatomic, retain) NSString * updatedBy;
@property (nonatomic, retain) NSString * createdBy;
@property (nonatomic, retain) NSString * firebaseID;
@property (nonatomic, retain) NSNumber * isSavedInFirebase;
@property (nonatomic, retain) NSNumber * pendingUpload;
@property (nonatomic, retain) NSNumber * pendingDownload;
@property (nonatomic, retain) NSString * filename;

@end

@interface Nota (CoreDataGeneratedAccessors)

- (void)addClienteObject:(Cliente *)value;
- (void)removeClienteObject:(Cliente *)value;
- (void)addCliente:(NSSet *)values;
- (void)removeCliente:(NSSet *)values;

@end
