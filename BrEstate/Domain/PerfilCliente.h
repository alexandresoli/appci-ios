//
//  PerfilCliente.h
//  Appci
//
//  Created by BrEstate LTDA on 4/9/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Bairro, Cliente;

@interface PerfilCliente : NSManagedObject

@property (nonatomic, retain) NSNumber * codigo;
@property (nonatomic, retain) NSString * descricao;
@property (nonatomic, retain) NSNumber * quartos;
@property (nonatomic, retain) NSDate * version;
@property (nonatomic, retain) NSNumber * area;
@property (nonatomic, retain) NSString * cidade;
@property (nonatomic, retain) NSString * estado;
@property (nonatomic, retain) NSNumber * tipoImovel;
@property (nonatomic, retain) NSNumber * tipoPerfil;
@property (nonatomic, retain) NSNumber * valor;
@property (nonatomic, retain) NSSet *bairro;
@property (nonatomic, retain) NSSet *cliente;
@property (nonatomic, retain) NSString * parseID;
@property (nonatomic, retain) NSNumber * pendingCreateParse;
@property (nonatomic, retain) NSNumber * pendingUpdateParse;
@property (nonatomic, retain) NSString * updatedBy;
@property (nonatomic, retain) NSString * createdBy;
@property (nonatomic, retain) NSString * firebaseID;
@property (nonatomic, retain) NSNumber * isSavedInFirebase;

@end

@interface PerfilCliente (CoreDataGeneratedAccessors)

- (void)addBairroObject:(Bairro *)value;
- (void)removeBairroObject:(Bairro *)value;
- (void)addBairro:(NSSet *)values;
- (void)removeBairro:(NSSet *)values;

- (void)addClienteObject:(Cliente *)value;
- (void)removeClienteObject:(Cliente *)value;
- (void)addCliente:(NSSet *)values;
- (void)removeCliente:(NSSet *)values;

@end
