//
//  PerfilCliente.m
//  Appci
//
//  Created by BrEstate LTDA on 4/9/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "PerfilCliente.h"
#import "Bairro.h"
#import "Cliente.h"


@implementation PerfilCliente

@dynamic codigo;
@dynamic descricao;
@dynamic quartos;
@dynamic version;
@dynamic area;
@dynamic cidade;
@dynamic estado;
@dynamic tipoImovel;
@dynamic tipoPerfil;
@dynamic valor;
@dynamic bairro;
@dynamic cliente;
@dynamic parseID;
@dynamic pendingCreateParse;
@dynamic pendingUpdateParse;
@dynamic updatedBy;
@dynamic createdBy;
@dynamic firebaseID;
@dynamic isSavedInFirebase;

@end
