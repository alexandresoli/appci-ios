//
//  Sms.h
//  Appci
//
//  Created by BrEstate LTDA on 29/05/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Cliente, Evento;

@interface Sms : NSManagedObject

@property (nonatomic, retain) NSString * codigoSMS;
@property (nonatomic, retain) NSDate * dataEnvio;
@property (nonatomic, retain) NSString * respostaConfirmacao;
@property (nonatomic, retain) Cliente *cliente;
@property (nonatomic, retain) Evento *evento;
@property (nonatomic, retain) NSString * parseID;
@property (nonatomic, retain) NSDate * version;
@property (nonatomic, retain) NSNumber * pendingCreateParse;
@property (nonatomic, retain) NSNumber * pendingUpdateParse;
@property (nonatomic, retain) NSString * updatedBy;
@property (nonatomic, retain) NSString * createdBy;
@property (nonatomic, retain) NSString * firebaseID;
@property (nonatomic, retain) NSNumber * isSavedInFirebase;

@end
