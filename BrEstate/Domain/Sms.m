//
//  Sms.m
//  Appci
//
//  Created by BrEstate LTDA on 29/05/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Sms.h"
#import "Cliente.h"
#import "Evento.h"


@implementation Sms

@dynamic codigoSMS;
@dynamic dataEnvio;
@dynamic respostaConfirmacao;
@dynamic cliente;
@dynamic evento;
@dynamic version;
@dynamic parseID;
@dynamic pendingCreateParse;
@dynamic pendingUpdateParse;
@dynamic updatedBy;
@dynamic createdBy;
@dynamic firebaseID;
@dynamic isSavedInFirebase;

@end
