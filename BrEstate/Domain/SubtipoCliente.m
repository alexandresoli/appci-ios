//
//  SubtipoCliente.m
//  Appci
//
//  Created by BrEstate LTDA on 4/9/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "SubtipoCliente.h"
#import "TipoCliente.h"


@implementation SubtipoCliente

@dynamic codigo;
@dynamic descricao;
@dynamic tipoCliente;

@end
