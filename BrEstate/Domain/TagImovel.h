//
//  TagImovel.h
//  Appci
//
//  Created by BrEstate LTDA on 4/9/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Imovel;

@interface TagImovel : NSManagedObject

@property (nonatomic, retain) NSNumber * codigo;
@property (nonatomic, retain) NSNumber * armarioCozinha;
@property (nonatomic, retain) NSNumber * armarioEmbutido;
@property (nonatomic, retain) NSNumber * childrenCare;
@property (nonatomic, retain) NSNumber * churrasqueira;
@property (nonatomic, retain) NSNumber * garagem;
@property (nonatomic, retain) NSNumber * piscina;
@property (nonatomic, retain) NSNumber * playground;
@property (nonatomic, retain) NSNumber * quadraPoliesportiva;
@property (nonatomic, retain) NSNumber * salaGinastica;
@property (nonatomic, retain) NSNumber * salaoFestas;
@property (nonatomic, retain) NSNumber * salaoJogos;
@property (nonatomic, retain) NSNumber * sauna;
@property (nonatomic, retain) NSNumber * varanda;
@property (nonatomic, retain) NSNumber * wcEmpregada;
@property (nonatomic, retain) Imovel *imovel;
@property (nonatomic, retain) NSString *parseID;
@property (nonatomic, retain) NSNumber * pendingCreateParse;
@property (nonatomic, retain) NSNumber * pendingUpdateParse;
@property (nonatomic, retain) NSString * updatedBy;
@property (nonatomic, retain) NSString * createdBy;

@end
