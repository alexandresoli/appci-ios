//
//  TagImovel.m
//  Appci
//
//  Created by BrEstate LTDA on 4/9/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "TagImovel.h"
#import "Imovel.h"


@implementation TagImovel

@dynamic armarioCozinha;
@dynamic armarioEmbutido;
@dynamic childrenCare;
@dynamic churrasqueira;
@dynamic codigo;
@dynamic garagem;
@dynamic piscina;
@dynamic playground;
@dynamic quadraPoliesportiva;
@dynamic salaGinastica;
@dynamic salaoFestas;
@dynamic salaoJogos;
@dynamic sauna;
@dynamic varanda;
@dynamic wcEmpregada;
@dynamic imovel;
@dynamic parseID;
@dynamic pendingCreateParse;
@dynamic pendingUpdateParse;
@dynamic updatedBy;
@dynamic createdBy;

@end
