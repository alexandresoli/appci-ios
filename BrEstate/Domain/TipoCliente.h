//
//  TipoCliente.h
//  BrEstate
//
//  Created by BrEstate LTDA on 12/16/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SubtipoCliente;

@interface TipoCliente : NSManagedObject

@property (nonatomic, retain) NSNumber * codigo;
@property (nonatomic, retain) NSString * descricao;
@property (nonatomic, retain) NSSet *subtipoCliente;

@end

@interface TipoCliente (CoreDataGeneratedAccessors)

- (void)addSubtipoClienteObject:(SubtipoCliente *)value;
- (void)removeSubtipoClienteObject:(SubtipoCliente *)value;
- (void)addSubtipoCliente:(NSSet *)values;
- (void)removeSubtipoCliente:(NSSet *)values;

@end
