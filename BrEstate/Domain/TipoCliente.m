//
//  TipoCliente.m
//  BrEstate
//
//  Created by BrEstate LTDA on 12/16/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "TipoCliente.h"
#import "SubtipoCliente.h"


@implementation TipoCliente

@dynamic codigo;
@dynamic descricao;
@dynamic subtipoCliente;

@end
