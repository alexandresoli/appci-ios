//
//  TipoValor.h
//  BrEstate
//
//  Created by BrEstate LTDA on 1/23/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Valor;

@interface TipoValor : NSManagedObject

@property (nonatomic, retain) NSNumber * codigo;
@property (nonatomic, retain) NSString * descricao;
@property (nonatomic, retain) NSSet *valor;
@end

@interface TipoValor (CoreDataGeneratedAccessors)

- (void)addValorObject:(Valor *)value;
- (void)removeValorObject:(Valor *)value;
- (void)addValor:(NSSet *)values;
- (void)removeValor:(NSSet *)values;

@end
