//
//  TipoValor.m
//  BrEstate
//
//  Created by BrEstate LTDA on 1/23/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "TipoValor.h"
#import "Valor.h"


@implementation TipoValor

@dynamic codigo;
@dynamic descricao;
@dynamic valor;

@end
