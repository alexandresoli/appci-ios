//
//  TransacaoInAppPurchase.h
//  Appci
//
//  Created by BrEstate LTDA on 10/06/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TransacaoInAppPurchase : NSManagedObject

@property (nonatomic, retain) NSDate * dataTransacao;
@property (nonatomic, retain) NSString * identificadorTransacao;
@property (nonatomic, retain) NSString * identificadorProduto;

@end
