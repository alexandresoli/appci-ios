//
//  Usuario.m
//  Appci
//
//  Created by Alexandre Oliveira on 11/3/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Usuario.h"


@implementation Usuario

@dynamic email;
@dynamic imobiliaria;
@dynamic nome;
@dynamic role;
@dynamic telefone;
@dynamic version;
@dynamic parseID;

@end
