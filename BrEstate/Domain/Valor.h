//
//  Valor.h
//  BrEstate
//
//  Created by BrEstate LTDA on 1/23/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PerfilCliente, TipoValor;

@interface Valor : NSManagedObject

@property (nonatomic, retain) NSNumber * codigo;
@property (nonatomic, retain) NSString * descricao;
@property (nonatomic, retain) NSDecimalNumber * maximo;
@property (nonatomic, retain) NSDecimalNumber * minimo;
@property (nonatomic, retain) NSSet *perfilCliente;
@property (nonatomic, retain) TipoValor *tipo;
@end

@interface Valor (CoreDataGeneratedAccessors)

- (void)addPerfilClienteObject:(PerfilCliente *)value;
- (void)removePerfilClienteObject:(PerfilCliente *)value;
- (void)addPerfilCliente:(NSSet *)values;
- (void)removePerfilCliente:(NSSet *)values;

@end
