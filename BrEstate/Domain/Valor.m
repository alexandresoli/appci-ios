//
//  Valor.m
//  BrEstate
//
//  Created by BrEstate LTDA on 1/23/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Valor.h"
#import "PerfilCliente.h"
#import "TipoValor.h"


@implementation Valor

@dynamic codigo;
@dynamic descricao;
@dynamic maximo;
@dynamic minimo;
@dynamic perfilCliente;
@dynamic tipo;

@end
