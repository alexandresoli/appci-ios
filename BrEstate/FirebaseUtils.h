//
//  FirebaseUtils.h
//  Appci
//
//  Created by Leonardo Barros on 19/12/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Firebase/Firebase.h>
#import "Constants.h"

@interface FirebaseUtils : NSObject

@property (nonatomic, strong) Firebase *rootRefFirebase;

- (dispatch_queue_t)queueDownload;

- (void)startObservers;

- (void)saveObjectWithDictionary:(NSDictionary *)dict andClassName:(NSString *)className andCompletionBlock:(completionWithError)completion;

- (void)deleteObjectWithID:(NSString *)firebaseID andClassName:(NSString *)className;

- (void)sendToFirebase;

- (NSString *)messageForError:(NSError *)error;

- (void)removeAllObservers;

- (long)countDataToSend;

@end
