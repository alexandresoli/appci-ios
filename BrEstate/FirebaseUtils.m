//
//  FirebaseUtils.m
//  Appci
//
//  Created by Leonardo Barros on 19/12/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "FirebaseUtils.h"
#import "Cliente+Firebase.h"
#import "Imovel+Firebase.h"
#import "FotoImovel+Firebase.h"
#import "Bairro+Firebase.h"
#import "Corretor+Firebase.h"
#import "Corretor+Utils.h"
#import "Evento+Firebase.h"
#import "Nota+Firebase.h"
#import "Acompanhamento+Firebase.h"
#import "Negociacao+Firebase.h"
#import "PerfilCliente+Firebase.h"
#import "ClienteDeletado+Firebase.h"
#import "FotoImovel+InsertDeleteImagem.h"
#import "S3Utils.h"
#import "FotoImovel+Firebase.h"
#import "CWStatusBarNotification.h"
#import "Evento+AlarmeLocalNotification.h"
#import "TagImovel.h"
#import "Endereco.h"

@interface FirebaseUtils ()

@property (nonatomic, strong) CWStatusBarNotification *notificationStatus;

@end


@implementation FirebaseUtils

#pragma mark - Queue

dispatch_queue_t queueFirebaseUpload;
dispatch_queue_t queueFirebaseDeleted;

dispatch_queue_t queueFirebaseDownload() {
    static dispatch_once_t queueCreationGuard;
    static dispatch_queue_t queue;
    dispatch_once(&queueCreationGuard, ^{
        queue = dispatch_queue_create("FirebaseUtils.Download", 0);
    });
    return queue;
}


- (dispatch_queue_t)queueDownload
{
    return queueFirebaseDownload();
}


#pragma mark - Init

- (id)init
{
    self = [super init];
    
    if (self) {
        //        [Firebase setOption:@"persistence" to:@YES];
        
        self.rootRefFirebase = [[Firebase alloc] initWithUrl:FIREBASE_APPCI_URL];
        
        NSString *userID = [Utils valueFromDefaults:FIREBASE_USER_ID];
        if (userID) {
            self.rootRefFirebase = [self.rootRefFirebase childByAppendingPath:userID];
        }
        
        queueFirebaseUpload = dispatch_queue_create("FirebaseUtils.Upload", NULL);
        queueFirebaseDeleted = dispatch_queue_create("FirebaseUtils.Deleted", NULL);
    }
    
    return self;
}


#pragma mark - Start Observers

- (void)startObservers
{
    if (![Utils valueFromDefaults:FIREBASE_ADJUST_OBJECTS_CODIGO]) {
        [self adjustObjectsCodigo];
        return;
    }
    
    // If not TOP don't start observers.
    if (![[Utils iapHelper] checkPlanTopFromController:nil withMessage:nil]) {
        
        // Mark the alreary run to not erase local data when the TOP was activate.
        [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:FIREBASE_ALREADY_RUN];
        
        // Only start the observer of corretor to update your TOP information.
        [Corretor startFirebaseObservers];
        return;
    }
    
    if ([Utils valueFromDefaults:FIREBASE_FIRST_CREATE_USER]) {
        [self sendToFirebase];
        return;
    }
    
    NSNumber *alreadyDownloadAll = [Utils valueFromDefaults:FIREBASE_ALREADY_DOWNLOAD_ALL];
    
    if (!alreadyDownloadAll) {
        [self downloadAllData];
        return;
    }
    
    if ([Utils valueFromDefaults:FIREBASE_SEND_ADJUST_OBJECTS_CODIGO]) {
        [self sendToFirebase];
        return;
    }
    
    [Bairro startFirebaseObservers];
    [Cliente startFirebaseObservers];
    [Imovel startFirebaseObservers];
    [Corretor startFirebaseObservers];
    [Evento startFirebaseObservers];
    [Nota startFirebaseObservers];
    [Acompanhamento startFirebaseObservers];
    [Negociacao startFirebaseObservers];
    [PerfilCliente startFirebaseObservers];
    [ClienteDeletado startFirebaseObservers];
    [self startObserverDeletedObjects];
}


#pragma mark - Save Object

- (void)saveObjectWithDictionary:(NSDictionary *)dict andClassName:(NSString *)className andCompletionBlock:(completionWithError)completion
{
    if (!dict) {
        NSError *newError = [NSError errorWithDomain:@"firebase_save_object"
                                                code:-1
                                            userInfo:@{NSLocalizedDescriptionKey:@"Dicionário vazio."}];
        completion(newError);
        return;
    }
    
    Firebase *classRef = [self.rootRefFirebase childByAppendingPath:className];
    
    Firebase *objectRef;
    
    if (dict[@"firebaseID"]) {
        objectRef = [classRef childByAppendingPath:dict[@"firebaseID"]];
    } else {
        objectRef = [classRef childByAutoId];
    }
    
    [objectRef setValue:dict withCompletionBlock:^(NSError *error, Firebase *ref) {
        completion(error);
    }];
}


#pragma mark - Delete Object

- (void)deleteObjectWithID:(NSString *)firebaseID andClassName:(NSString *)className
{
    if (!firebaseID) {
        return;
    }
    
    NSString *path = className;
    
    // Treatment for FotoImovel because it is inside the Imovel.
    if ([className isEqualToString:@"FotoImovel"]) {
        FotoImovel *foto = [FotoImovel MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"firebaseID = %@", firebaseID]];
        path = [NSString stringWithFormat:@"Imovel/%@/fotos", foto.imovel.createFirebaseID];
    }
    
    Firebase *classRef = [self.rootRefFirebase childByAppendingPath:className];
    
    Firebase *objectRef = [classRef childByAppendingPath:firebaseID];
    [objectRef removeValue];
    
    if ([className isEqualToString:@"Device"]) {
        return;
    }
    
    
    
    // Save on deleted objects
    NSDictionary *dictDeleted = @{@"className": className,
                                  @"firebaseID": firebaseID,
                                  @"version": [DateUtils stringTimestampFromDate:[NSDate date]],
                                  @"deviceToken": [Utils deviceID],
                                  @"deviceName": [Utils deviceName]};
    
    [self saveDeletedObjectWithDictionary:dictDeleted];
}


- (void)saveDeletedObjectWithDictionary:(NSDictionary *)dictDeleted
{
    Firebase *deletedRef = [self.rootRefFirebase childByAppendingPath:@"Deleted"];
    Firebase *objectDeletedRef = [deletedRef childByAutoId];
    NSString *keyDeleted = objectDeletedRef.key;
    
    NSMutableDictionary *deletedDict = [[Utils valueFromDefaults:FIREBASE_PENDING_DELETED_OBJECTS_DICT] mutableCopy];
    
    if (!deletedDict) {
        deletedDict = [NSMutableDictionary new];
    }
    
    // Save in user defaults because if error on save in firebase it can be send later.
    [deletedDict setObject:dictDeleted forKey:keyDeleted];
    [Utils addObjectToDefaults:deletedDict withKey:FIREBASE_PENDING_DELETED_OBJECTS_DICT];
    
    [objectDeletedRef setValue:dictDeleted withCompletionBlock:^(NSError *error, Firebase *ref) {
        
        if (!error) {
            NSMutableDictionary *deletedDict = [[Utils valueFromDefaults:FIREBASE_PENDING_DELETED_OBJECTS_DICT] mutableCopy];
            [deletedDict removeObjectForKey:keyDeleted];
            [Utils addObjectToDefaults:deletedDict withKey:FIREBASE_PENDING_DELETED_OBJECTS_DICT];
        }
    }];
}


#pragma mark - Check Pending Deleted Objects

- (void)checkPendingDeletedObjects
{
    NSDictionary *objects = [Utils valueFromDefaults:FIREBASE_PENDING_DELETED_OBJECTS_DICT];
    
    if (objects.count > 0) {
        Firebase *deletedRef = [self.rootRefFirebase childByAppendingPath:@"Deleted"];
        
        [deletedRef updateChildValues:objects withCompletionBlock:^(NSError *error, Firebase *ref) {
            if (!error) {
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:FIREBASE_PENDING_DELETED_OBJECTS_DICT];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                // Try delete on Firebase
                NSMutableDictionary *deletedObjects = [NSMutableDictionary new];
                
                for (NSString *key in objects.allKeys) {
                    NSDictionary *object = [objects objectForKey:key];
                    NSString *className = object[@"className"];
                    NSString *firebaseID = object[@"firebaseID"];
                    
                    NSMutableDictionary *dictClass = [[deletedObjects objectForKey:className] mutableCopy];
                    if (!dictClass) {
                        dictClass = [NSMutableDictionary new];
                    }
                    [dictClass setObject:[NSNull null] forKey:firebaseID];
                    
                    [deletedObjects setObject:dictClass forKey:className];
                }
                
                for (NSString *className in deletedObjects.allKeys) {
                    Firebase *ref = [self.rootRefFirebase childByAppendingPath:className];
                    [ref updateChildValues:[deletedObjects objectForKey:className]];
                }
            }
        }];
    }
}


#pragma mark - Observer Deleted Objects

- (void)startObserverDeletedObjects
{
    Firebase *deletedRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"Deleted"];
    
    NSString *versionString = [Utils valueFromDefaults:FIREBASE_DELETADOS_INSERT_VERSION];
    
    if (!versionString) {
        versionString = [DateUtils stringTimestampFromDate:[NSDate dateWithTimeIntervalSince1970:1]];
    }
    
    // Add 1 millisecond because the firebase query is always >=
    versionString = [NSString stringWithFormat:@"%lld", [versionString longLongValue]+1];
    
    [[[deletedRef queryOrderedByChild:@"version"] queryStartingAtValue:versionString]
     observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
         
         NSDictionary *dict = snapshot.value;
         
         if ([dict isEqual:[NSNull null]] || dict.count == 0) {
             return;
         }
         
         dispatch_async(queueFirebaseDeleted, ^{
             
             NSManagedObjectContext *context = [NSManagedObjectContext MR_contextForCurrentThread];
             
             [context performBlockAndWait:^{
                 
                 BOOL hasDeleted = NO;
                 
                 for (NSString *reference in dict.allKeys) {
                     
                     NSDictionary *deletedDict = [dict objectForKey:reference];
                     
                     // Check if item was deleted by the current device
                     if ([deletedDict[@"deviceToken"] isEqualToString:[Utils deviceID]]) {
                         continue;
                     }
                     
                     // Find local object
                     NSString *className = deletedDict[@"className"];
                     NSPredicate *predicate = [NSPredicate predicateWithFormat:@"firebaseID = %@", deletedDict[@"firebaseID"]];
                     NSArray *objects = [CoreDataUtils fetchEntity:className withPredicate:predicate inContext:context];
                     
                     if (objects.count == 0) {
                         continue;
                     }
                     
                     hasDeleted = YES;
                     
                     // If FotoImovel remove local file.
                     if ([className isEqualToString:@"FotoImovel"]) {
                         FotoImovel *foto = (FotoImovel *)objects.firstObject;
                         [foto deleteFromDisk];
                         
                     } else if ([className isEqualToString:@"Evento"]) { // If Evento cancel local notification.
                         Evento *evento = (Evento *)objects.firstObject;
                         [evento cancelEventoLocalNotifications];
                     }
                     
                     NSManagedObject *object = objects.firstObject;
                     [object MR_deleteInContext:context];
                     [context MR_saveToPersistentStoreAndWait];
                 }
                 
                 // Save the last version.
                 NSMutableArray *deletedObjects = [dict.allValues mutableCopy];
                 
                 NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"version" ascending:YES];
                 [deletedObjects sortUsingDescriptors:@[descriptor]];
                 
                 NSDictionary *lastDeleted = deletedObjects.lastObject;
                 [Utils addObjectToDefaults:lastDeleted[@"version"] withKey:FIREBASE_DELETADOS_INSERT_VERSION];
                 
                 // Mark refresh.
                 if (hasDeleted) {
                     [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:REFRESH_CLIENTE];
                     [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:REFRESH_EVENTO];
                     [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:REFRESH_IMOVEL];
                     [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:REFRESH_NOTA];
                 }
             }];
         });
     }];
}


#pragma mark - Send To Firebase

- (void)sendToFirebase
{
    dispatch_async(queueFirebaseUpload, ^{
        
        [Utils addObjectToDefaults:[NSNumber numberWithInt:0] withKey:COUNT_SYNC];
        
        [self showProgressInStatusBarWithMessage:@"Enviando Clientes"];
        [Utils addObjectToDefaults:@"Enviando Clientes" withKey:MESSAGE_SYNC];
        [Utils postEntitytNotification:@"Sync"];
        [ClienteDeletado saveAllPendingFirebase];
        [self incrementCountSync];
        
        [Utils addObjectToDefaults:@"Enviando Clientes" withKey:MESSAGE_SYNC];
        [Utils postEntitytNotification:@"Sync"];
        [Cliente saveAllPendingFirebase];
        [self incrementCountSync];
        [NSThread sleepForTimeInterval:0.5];
        
        [self showProgressInStatusBarWithMessage:@"Enviando Bairros"];
        [Utils addObjectToDefaults:@"Enviando Bairros" withKey:MESSAGE_SYNC];
        [Utils postEntitytNotification:@"Sync"];
        [Bairro saveAllPendingFirebase];
        [self incrementCountSync];
        [NSThread sleepForTimeInterval:0.5];
        
        [self showProgressInStatusBarWithMessage:@"Enviando Eventos"];
        [Utils addObjectToDefaults:@"Enviando Eventos" withKey:MESSAGE_SYNC];
        [Utils postEntitytNotification:@"Sync"];
        [Evento saveAllPendingFirebase];
        [self incrementCountSync];
        [NSThread sleepForTimeInterval:0.5];
        
        [self showProgressInStatusBarWithMessage:@"Enviando Notas"];
        [Utils addObjectToDefaults:@"Enviando Notas" withKey:MESSAGE_SYNC];
        [Utils postEntitytNotification:@"Sync"];
        [Nota saveAllPendingFirebase];
        [Nota sendAllPendingNotasToAmazon];
        [self incrementCountSync];
        [NSThread sleepForTimeInterval:0.5];
        
        [self showProgressInStatusBarWithMessage:@"Enviando Acompanhamentos"];
        [Utils addObjectToDefaults:@"Enviando Acompanhamentos" withKey:MESSAGE_SYNC];
        [Utils postEntitytNotification:@"Sync"];
        [Acompanhamento saveAllPendingFirebase];
        [Acompanhamento sendAllPendingAcompanhamentosToAmazon];
        [self incrementCountSync];
        [NSThread sleepForTimeInterval:0.5];
        
        [self showProgressInStatusBarWithMessage:@"Enviando Negociações"];
        [Utils addObjectToDefaults:@"Enviando Negociações" withKey:MESSAGE_SYNC];
        [Utils postEntitytNotification:@"Sync"];
        [Negociacao saveAllPendingFirebase];
        [self incrementCountSync];
        [NSThread sleepForTimeInterval:0.5];
        
        [self showProgressInStatusBarWithMessage:@"Enviando Perfis"];
        [Utils addObjectToDefaults:@"Enviando Perfis" withKey:MESSAGE_SYNC];
        [Utils postEntitytNotification:@"Sync"];
        [PerfilCliente saveAllPendingFirebase];
        [self incrementCountSync];
        [NSThread sleepForTimeInterval:0.5];
        
        [self showProgressInStatusBarWithMessage:@"Enviando Imóveis"];
        [Utils addObjectToDefaults:@"Enviando Imóveis" withKey:MESSAGE_SYNC];
        [Utils postEntitytNotification:@"Sync"];
        [Imovel saveAllPendingFirebase];
        [self incrementCountSync];
        [NSThread sleepForTimeInterval:0.5];
        
        [self showProgressInStatusBarWithMessage:@"Enviando Fotos"];
        [Utils addObjectToDefaults:@"Enviando Fotos" withKey:MESSAGE_SYNC];
        [Utils postEntitytNotification:@"Sync"];
        [FotoImovel sendAllPendingPhotosToAmazon];
        [self incrementCountSync];
        [NSThread sleepForTimeInterval:0.5];
        
        [self showProgressInStatusBarWithMessage:@"Baixando Notas"];
        [Utils addObjectToDefaults:@"Baixando Notas" withKey:MESSAGE_SYNC];
        [Utils postEntitytNotification:@"Sync"];
        [self downloadAllPendingNotes];
        [self incrementCountSync];
        [NSThread sleepForTimeInterval:0.5];
        
        [self showProgressInStatusBarWithMessage:@"Baixando Acompanhamentos"];
        [Utils addObjectToDefaults:@"Baixando Acompanhamentos" withKey:MESSAGE_SYNC];
        [Utils postEntitytNotification:@"Sync"];
        [self downloadAllPendingAccompaniments];
        [self incrementCountSync];
        [NSThread sleepForTimeInterval:0.5];
        
        [self showProgressInStatusBarWithMessage:@"Baixando Fotos"];
        [Utils addObjectToDefaults:@"Baixando Fotos" withKey:MESSAGE_SYNC];
        [Utils postEntitytNotification:@"Sync"];
        [self downloadAllPendingPhotos];
        [self incrementCountSync];
        [NSThread sleepForTimeInterval:0.5];
        
        [self showProgressInStatusBarWithMessage:@"Enviando Registros Excluídos"];
        [Utils addObjectToDefaults:@"Enviando Registros Excluídos" withKey:MESSAGE_SYNC];
        [Utils postEntitytNotification:@"Sync"];
        [self checkPendingDeletedObjects];
        
        
        [NSThread sleepForTimeInterval:1.0];
        [Utils removeObjectFromDefaults:COUNT_SYNC];
        [Utils postEntitytNotification:@"FinishSync"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.notificationStatus dismissNotification];
        });
        
        if ([Utils valueFromDefaults:FIREBASE_FIRST_CREATE_USER]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [Utils removeObjectFromDefaults:FIREBASE_FIRST_CREATE_USER];
                [self startObservers];
                return;
            });
        }
        
        if ([Utils valueFromDefaults:FIREBASE_SEND_ADJUST_OBJECTS_CODIGO]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [Utils removeObjectFromDefaults:FIREBASE_SEND_ADJUST_OBJECTS_CODIGO];
                [self startObservers];
            });
        }
    });
}


- (void)incrementCountSync
{
    NSNumber *count = [Utils valueFromDefaults:COUNT_SYNC];
    
    if (count) {
        int intCount = [count intValue];
        intCount++;
        [Utils addObjectToDefaults:[NSNumber numberWithInt:intCount] withKey:COUNT_SYNC];
    }
}


#pragma mark - Message For Error

- (NSString *)messageForError:(NSError *)error
{
    NSString *message = @"Erro desconhecido. Tente novamente. Se o erro persistir entre em contato com suporte@brestate.com.br";
    
    // an error occurred while attempting login
    switch(error.code) {
        case FAuthenticationErrorUserDoesNotExist:
            message = @"E-mail não encontrado!";
            break;
            
        case FAuthenticationErrorInvalidEmail:
            message = @"E-mail inválido!";
            break;
            
        case FAuthenticationErrorInvalidPassword:
            message = @"Email ou senha inválidos. Verifique as informações digitadas e tente novamente.";
            break;
            
        case FAuthenticationErrorInvalidCredentials:
            message = @"Email ou senha inválidos. Verifique as informações digitadas e tente novamente.";
            break;
            
        case FAuthenticationErrorNetworkError:
            message = @"Erro na conexão com o servidor. Tente novamente.";
            break;
            
        case FAuthenticationErrorEmailTaken:
            message = @"Usuário já existente com o email informado.";
            break;
            
        default:
            break;
    }
    
    return message;
}


#pragma mark - Remove All Observers

- (void)removeAllObservers
{
    [self.rootRefFirebase removeAllObservers];
    
    [[self.rootRefFirebase childByAppendingPath:@"Bairro"] removeAllObservers];
    [[self.rootRefFirebase childByAppendingPath:@"Cliente"] removeAllObservers];
    [[self.rootRefFirebase childByAppendingPath:@"Imovel"] removeAllObservers];
    [[self.rootRefFirebase childByAppendingPath:@"UserInfo"] removeAllObservers];
    [[self.rootRefFirebase childByAppendingPath:@"Evento"] removeAllObservers];
    [[self.rootRefFirebase childByAppendingPath:@"Nota"] removeAllObservers];
    [[self.rootRefFirebase childByAppendingPath:@"Acompanhamento"] removeAllObservers];
    [[self.rootRefFirebase childByAppendingPath:@"Negociacao"] removeAllObservers];
    [[self.rootRefFirebase childByAppendingPath:@"PerfilCliente"] removeAllObservers];
    [[self.rootRefFirebase childByAppendingPath:@"ClienteDeletado"] removeAllObservers];
    [[self.rootRefFirebase childByAppendingPath:@"Deleted"] removeAllObservers];
}


#pragma mark - Count Data To Send

- (long)countDataToSend
{
    long total = 0;
    
    NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSavedInFirebase = NO OR isSavedInFirebase = nil"];
    NSPredicate *predicateUpload = [NSPredicate predicateWithFormat:@"arquivo != nil AND (pendingUpload = YES OR pendingUpload = nil)"];
    NSPredicate *predicateDownload = [NSPredicate predicateWithFormat:@"pendingDownload = YES"];
    
    // clientes
    long clienteCount = [Cliente MR_countOfEntitiesWithPredicate:predicate inContext:moc];
    total += clienteCount;
    
    // eventos
    long eventoCount = [Evento MR_countOfEntitiesWithPredicate:predicate inContext:moc];
    total += eventoCount;
    
    // imoveis
    long imoveisCount = [Imovel MR_countOfEntitiesWithPredicate:predicate inContext:moc];
    total += imoveisCount;
    
    // notas
    long notasCount = [Nota MR_countOfEntitiesWithPredicate:predicate inContext:moc];
    total += notasCount;
    
    long notasFileCount = [Nota MR_countOfEntitiesWithPredicate:predicateUpload inContext:moc];
    total += notasFileCount;
    
    long notasDownloadFileCount = [Nota MR_countOfEntitiesWithPredicate:predicateDownload inContext:moc];
    total += notasDownloadFileCount;
    
    // acompanhamento
    long acompanhamentosCount = [Acompanhamento MR_countOfEntitiesWithPredicate:predicate inContext:moc];
    total += acompanhamentosCount;
    
    long acompanhamentosFileCount = [Acompanhamento MR_countOfEntitiesWithPredicate:predicateUpload inContext:moc];
    total += acompanhamentosFileCount;
    
    long acompanhamentosDownloadFileCount = [Acompanhamento MR_countOfEntitiesWithPredicate:predicateDownload inContext:moc];
    total += acompanhamentosDownloadFileCount;
    
    // bairro
    long bairroCount = [Bairro MR_countOfEntitiesWithPredicate:predicate inContext:moc];
    total += bairroCount;
    
    // negociacao
    long negociacaoCount = [Negociacao MR_countOfEntitiesWithPredicate:predicate inContext:moc];
    total += negociacaoCount;
    
    // perfil
    long perfilCount = [PerfilCliente MR_countOfEntitiesWithPredicate:predicate inContext:moc];
    total += perfilCount;
    
    // cliente deletado
    long clienteDeletadoCount = [ClienteDeletado MR_countOfEntitiesWithPredicate:predicate inContext:moc];
    total += clienteDeletadoCount;
    
    // fotos
    NSPredicate *predicateFotos = [NSPredicate predicateWithFormat:@"pendingUpload = YES OR pendingUpload = nil OR pendingDownload = YES"];
    long fotosCount = [FotoImovel MR_countOfEntitiesWithPredicate:predicateFotos inContext:moc];
    total += fotosCount;
    
    return total;
}


#pragma mark - Download All Data

- (void)downloadAllData
{
    NSString *baseMessage = @"Baixando seus dados. Aguarde.\n\n";
    
    [Utils showProgressWithValue:0.01 andMessage:baseMessage];
    
    [self.rootRefFirebase observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        
        dispatch_async(queueFirebaseDownload(), ^{
            
            NSDictionary *value = snapshot.value;
            
            if ([value isEqual:[NSNull null]] || value.count == 0) {
                [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:FIREBASE_ALREADY_DOWNLOAD_ALL];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [Utils dismissProgress];
                    [self startObservers];
                });
                
                return;
            }
            
            // Bairro
            [Utils showProgressWithValue:0.1 andMessage:[NSString stringWithFormat:@"%@ Processando Bairro", baseMessage]];
            NSDictionary *bairros = value[@"Bairro"];
            [Bairro convertBairrosInBatchWithDictionary:bairros];
            [NSThread sleepForTimeInterval:0.5];
            
            // Perfil
            [Utils showProgressWithValue:0.2 andMessage:[NSString stringWithFormat:@"%@ Processando Perfil", baseMessage]];
            NSDictionary *perfis = value[@"PerfilCliente"];
            [PerfilCliente convertPerfisInBatchWithDictionary:perfis];
            [NSThread sleepForTimeInterval:0.5];
            
            // Cliente
            [Utils showProgressWithValue:0.3 andMessage:[NSString stringWithFormat:@"%@ Processando Cliente", baseMessage]];
            NSDictionary *clientes = value[@"Cliente"];
            [Cliente convertClientesInBatchWithDictionary:clientes];
            
            // Cliente Deletado
            NSDictionary *clientesDeletados = value[@"ClienteDeletado"];
            [ClienteDeletado convertClientesDeletadosInBatchWithDictionary:clientesDeletados];
            [NSThread sleepForTimeInterval:0.5];
            
            // Imovel
            [Utils showProgressWithValue:0.4 andMessage:[NSString stringWithFormat:@"%@ Processando Imóvel", baseMessage]];
            NSDictionary *imoveis = value[@"Imovel"];
            [Imovel convertImoveisInBatchWithDictionary:imoveis];
            [NSThread sleepForTimeInterval:0.5];
            
            // Evento
            [Utils showProgressWithValue:0.5 andMessage:[NSString stringWithFormat:@"%@ Processando Evento", baseMessage]];
            NSDictionary *eventos = value[@"Evento"];
            [Evento convertEventosInBatchWithDictionary:eventos];
            [NSThread sleepForTimeInterval:0.5];
            
            // Nota
            [Utils showProgressWithValue:0.6 andMessage:[NSString stringWithFormat:@"%@ Processando Nota", baseMessage]];
            NSDictionary *notas = value[@"Nota"];
            [Nota convertNotasInBatchWithDictionary:notas];
            [NSThread sleepForTimeInterval:0.5];
            
            // Acompanhamento
            [Utils showProgressWithValue:0.7 andMessage:[NSString stringWithFormat:@"%@ Processando Acompanhamento", baseMessage]];
            NSDictionary *acomp = value[@"Acompanhamento"];
            [Acompanhamento convertAcompanhamentosInBatchWithDictionary:acomp];
            [NSThread sleepForTimeInterval:0.5];
            
            // Negociacao
            [Utils showProgressWithValue:0.8 andMessage:[NSString stringWithFormat:@"%@ Processando Negociação", baseMessage]];
            NSDictionary *neg = value[@"Negociacao"];
            [Negociacao convertNegociacoesInBatchWithDictionary:neg];
            [NSThread sleepForTimeInterval:0.5];
            
            // Fotos
            [Utils showProgressWithValue:0.9 andMessage:[NSString stringWithFormat:@"%@ Processando Fotos", baseMessage]];
            [self downloadAllPhotos];
            
            // Deleted - Save the last version
            NSDictionary *deleted = value[@"Deleted"];
            NSMutableArray *deletedObjects = [[deleted allValues] mutableCopy];
            
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"version" ascending:YES];
            [deletedObjects sortUsingDescriptors:@[descriptor]];
            
            NSDictionary *lastDeleted = deletedObjects.lastObject;
            [Utils addObjectToDefaults:lastDeleted[@"version"] withKey:FIREBASE_DELETADOS_INSERT_VERSION];
            
            
            // Finish First Download
            [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:FIREBASE_ALREADY_DOWNLOAD_ALL];
            [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:FIREBASE_ALREADY_RUN];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [Utils dismissProgress];
                [self startObservers];
            });
        });
    }];
}


#pragma mark - Pending Download
- (void)downloadAllPendingAccompaniments
{
    NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pendingDownload = YES"];
    NSArray *accompaniments = [Acompanhamento MR_findAllWithPredicate:predicate inContext:moc];
    
    for (Acompanhamento *accompaniment in accompaniments) {
        
        @autoreleasepool {
            
            accompaniment.pendingDownload = [NSNumber numberWithBool:YES];
            accompaniment.pendingUpload = [NSNumber numberWithBool:NO];
            
            NSString *userID = [Utils valueFromDefaults:FIREBASE_USER_ID];
            NSString *filePath = [NSString stringWithFormat:@"%@/acompanhamentos/%@.%@",userID,accompaniment.filename,accompaniment.extensao];
            NSData *accompanimentData = [S3Utils download:filePath];
            
            // success
            if (accompanimentData != nil) {
                accompaniment.arquivo = accompanimentData;
                accompaniment.pendingUpload = [NSNumber numberWithBool:NO];
                accompaniment.pendingDownload = [NSNumber numberWithBool:NO];
            }
            
            [moc MR_saveToPersistentStoreAndWait];
            
            
            if (![accompaniment.pendingDownload boolValue]) {
                [Utils postEntitytNotificationWithValue:@"UpdateSyncCounter" value:1];
            }
            
        }
    }
    
}

- (void)downloadAllPendingNotes
{
    NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pendingDownload = YES"];
    NSArray *notes = [Nota MR_findAllWithPredicate:predicate inContext:moc];
    
    for (Nota *note in notes) {
        
        @autoreleasepool {
            
            note.pendingDownload = [NSNumber numberWithBool:YES];
            note.pendingUpload = [NSNumber numberWithBool:NO];
            
            NSString *userID = [Utils valueFromDefaults:FIREBASE_USER_ID];
            NSString *filePath = [NSString stringWithFormat:@"%@/notas/%@.%@",userID,note.filename,note.extensao];
            NSData *noteData = [S3Utils download:filePath];
            
            // success
            if (noteData != nil) {
                note.arquivo = noteData;
                note.pendingUpload = [NSNumber numberWithBool:NO];
                note.pendingDownload = [NSNumber numberWithBool:NO];
            }
            
            [moc MR_saveToPersistentStoreAndWait];
            
            
            if (![note.pendingDownload boolValue]) {
                [Utils postEntitytNotificationWithValue:@"UpdateSyncCounter" value:1];
            }
            
        }
    }
    
    if (notes.count > 0) {
        [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:REFRESH_NOTA];
    }
}

- (void)downloadAllPendingPhotos
{
    NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pendingDownload = YES"];
    NSArray *photos = [FotoImovel MR_findAllWithPredicate:predicate inContext:moc];
    
    for (FotoImovel *photo in photos) {
        
        @autoreleasepool {
            
            photo.pendingDownload = [NSNumber numberWithBool:YES];
            photo.pendingUpload = [NSNumber numberWithBool:NO];
            
            NSString *userID = [Utils valueFromDefaults:FIREBASE_USER_ID];
            NSString *filePath = [NSString stringWithFormat:@"%@/fotos/%@",userID,photo.imagemURL];
            NSData *imageData = [S3Utils download:filePath];
            
            // success
            if (imageData != nil) {
                [photo insertMediaOnDisk:imageData withURLString:photo.imagemURL];
                photo.pendingUpload = [NSNumber numberWithBool:NO];
                photo.pendingDownload = [NSNumber numberWithBool:NO];
            }
            
            [moc MR_saveToPersistentStoreAndWait];
            
            
            if (![photo.pendingDownload boolValue]) {
                [Utils postEntitytNotificationWithValue:@"UpdateSyncCounter" value:1];
            }
            
        }
    }
    
    if (photos.count > 0) {
        [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:REFRESH_IMOVEL];
    }
}


- (void)downloadAllPhotos
{
    
    NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
    NSArray *photos = [FotoImovel MR_findAllInContext:moc];
    
    for (FotoImovel *photo in photos) {
        
        @autoreleasepool {
            if ([photo isLocalFileExists]) {
                photo.pendingUpload = [NSNumber numberWithBool:NO];
                photo.pendingDownload = [NSNumber numberWithBool:NO];
                [moc MR_saveToPersistentStoreAndWait];
                continue;
            }
            
            photo.pendingDownload = [NSNumber numberWithBool:YES];
            photo.pendingUpload = [NSNumber numberWithBool:NO];
            
            NSString *userID = [Utils valueFromDefaults:FIREBASE_USER_ID];
            NSString *filePath = [NSString stringWithFormat:@"%@/fotos/%@",userID,photo.imagemURL];
            NSData *imageData = [S3Utils download:filePath];
            
            // success
            if (imageData != nil) {
                [photo insertMediaOnDisk:imageData withURLString:photo.imagemURL];
                photo.pendingUpload = [NSNumber numberWithBool:NO];
                photo.pendingDownload = [NSNumber numberWithBool:NO];
            }
            
            [moc MR_saveToPersistentStoreAndWait];
        }
    }
    
    if (photos.count > 0) {
        [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:REFRESH_IMOVEL];
    }
}


#pragma mark - Show Progress

- (void)showProgressInStatusBarWithMessage:(NSString *)message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.notificationStatus = [CWStatusBarNotification new];
        self.notificationStatus.notificationLabelBackgroundColor = [UIColor colorWithRed:0.086 green:0.494 blue:0.984 alpha:1];
        self.notificationStatus.notificationLabelTextColor = [UIColor whiteColor];
        self.notificationStatus.notificationAnimationInStyle = CWNotificationAnimationStyleTop;
        self.notificationStatus.notificationAnimationOutStyle = CWNotificationAnimationStyleTop;
        self.notificationStatus.notificationTappedBlock = nil;
        
        [self.notificationStatus displayNotificationWithMessage:message completion:^{
            
            // Activity Indicator.
            UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            CGAffineTransform transform = CGAffineTransformMakeScale(0.7, 0.7);
            activity.transform = transform;
            [activity startAnimating];
            [self.notificationStatus.notificationWindow.rootViewController.view addSubview:activity];
        }];
    });
}


#pragma mark - Adjust Objects Codigo

- (void)adjustObjectsCodigo
{
    dispatch_async(queueFirebaseUpload, ^{
        
        [NSThread sleepForTimeInterval:2.0]; // Necessary to wait the login terminate.
        
        [Utils showProgress:@"Ajustando dados. Por favor aguarde."];
        
        NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
        
        [moc performBlockAndWait:^{
            
            BOOL hasToSend = NO;
            
            // Clientes
            NSArray *clientes = [Cliente MR_findAllInContext:moc];
            
            for (Cliente *cliente in clientes) {
                
                if ([cliente.codigo longLongValue] < 1000000000000) {
                    cliente.codigo = [cliente getUniqueCode];
                    cliente.isSavedInFirebase = [NSNumber numberWithBool:NO];
                    cliente.version = [NSDate date];
                    hasToSend = YES;
                }
            }
            [moc MR_saveToPersistentStoreAndWait];
            
            // Imóveis
            NSArray *imoveis = [Imovel MR_findAllInContext:moc];
            
            for (Imovel *imovel in imoveis) {
                
                if ([imovel.codigo longLongValue] < 1000000000000) {
                    imovel.codigoAnterior = [imovel.codigo copy];
                    imovel.codigo = [imovel getUniqueCode];
                    imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
                    imovel.version = [NSDate date];
                    hasToSend = YES;
                }
            }
            [moc MR_saveToPersistentStoreAndWait];
            
            // Fotos
            NSArray *fotos = [FotoImovel MR_findAllInContext:moc];
            
            for (FotoImovel *foto in fotos) {
                
                if ([foto.codigo longLongValue] < 1000000000000) {
                    foto.codigoAnterior = [foto.codigo copy];
                    foto.codigo = [foto getUniqueCode];
                    foto.imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
                    foto.imovel.version = [NSDate date];
                    hasToSend = YES;
                }
            }
            [moc MR_saveToPersistentStoreAndWait];
            
            // Tags
            NSArray *tags = [TagImovel MR_findAllInContext:moc];
            
            for (TagImovel *tag in tags) {
                
                if ([tag.codigo longLongValue] < 1000000000000) {
                    tag.codigo = [tag getUniqueCode];
                    tag.imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
                    tag.imovel.version = [NSDate date];
                    hasToSend = YES;
                }
            }
            [moc MR_saveToPersistentStoreAndWait];
            
            // Endereços
            NSArray *enderecos = [Endereco MR_findAllInContext:moc];
            
            for (Endereco *endereco in enderecos) {
                
                if ([endereco.codigo longLongValue] < 1000000000000) {
                    endereco.codigo = [endereco getUniqueCode];
                    
                    for (Cliente *cliente in endereco.cliente) {
                        cliente.isSavedInFirebase = [NSNumber numberWithBool:NO];
                        cliente.version = [NSDate date];
                    }
                    
                    for (Imovel *imovel in endereco.imovel) {
                        imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
                        imovel.version = [NSDate date];
                    }
                    hasToSend = YES;
                }
            }
            [moc MR_saveToPersistentStoreAndWait];
            
            // Bairros
            NSArray *bairros = [Bairro MR_findAllInContext:moc];
            
            for (Bairro *bairro in bairros) {
                
                if ([bairro.codigo longLongValue] < 1000000000000) {
                    bairro.codigo = [bairro getUniqueCode];
                    bairro.isSavedInFirebase = [NSNumber numberWithBool:NO];
                    bairro.version = [NSDate date];
                    hasToSend = YES;
                }
            }
            [moc MR_saveToPersistentStoreAndWait];
            
            // Eventos
            NSArray *eventos = [Evento MR_findAllInContext:moc];
            
            for (Evento *evento in eventos) {
                
                if ([evento.codigo longLongValue] < 1000000000000) {
                    evento.codigo = [evento getUniqueCode];
                    evento.isSavedInFirebase = [NSNumber numberWithBool:NO];
                    evento.version = [NSDate date];
                    hasToSend = YES;
                }
            }
            [moc MR_saveToPersistentStoreAndWait];
            
            // Notas
            NSArray *notas = [Nota MR_findAllInContext:moc];
            
            for (Nota *nota in notas) {
                
                if ([nota.codigo longLongValue] < 1000000000000) {
                    nota.codigo = [nota getUniqueCode];
                    nota.isSavedInFirebase = [NSNumber numberWithBool:NO];
                    nota.version = [NSDate date];
                    hasToSend = YES;
                }
            }
            [moc MR_saveToPersistentStoreAndWait];
            
            // Acompanhamentos
            NSArray *acompanhamentos = [Acompanhamento MR_findAllInContext:moc];
            
            for (Acompanhamento *acomp in acompanhamentos) {
                
                if ([acomp.codigo longLongValue] < 1000000000000) {
                    acomp.codigo = [acomp getUniqueCode];
                    acomp.isSavedInFirebase = [NSNumber numberWithBool:NO];
                    acomp.version = [NSDate date];
                    hasToSend = YES;
                }
            }
            [moc MR_saveToPersistentStoreAndWait];
            
            // Negociações
            NSArray *negociacoes = [Negociacao MR_findAllInContext:moc];
            
            for (Negociacao *neg in negociacoes) {
                
                if ([neg.codigo longLongValue] < 1000000000000) {
                    neg.codigo = [neg getUniqueCode];
                    neg.isSavedInFirebase = [NSNumber numberWithBool:NO];
                    neg.version = [NSDate date];
                    hasToSend = YES;
                }
            }
            [moc MR_saveToPersistentStoreAndWait];
            
            // Perfis
            NSArray *perfis = [PerfilCliente MR_findAllInContext:moc];
            
            for (PerfilCliente *perfil in perfis) {
                
                if ([perfil.codigo longLongValue] < 1000000000000) {
                    perfil.codigo = [perfil getUniqueCode];
                    perfil.isSavedInFirebase = [NSNumber numberWithBool:NO];
                    perfil.version = [NSDate date];
                    hasToSend = YES;
                }
            }
            [moc MR_saveToPersistentStoreAndWait];
            
            // Clientes Deletados
            NSArray *clientesDeletados = [ClienteDeletado MR_findAllInContext:moc];
            
            for (ClienteDeletado *clienteDel in clientesDeletados) {
                
                if ([clienteDel.codigo longLongValue] < 1000000000000) {
                    clienteDel.codigo = [clienteDel getUniqueCode];
                    clienteDel.isSavedInFirebase = [NSNumber numberWithBool:NO];
                    clienteDel.version = [NSDate date];
                    hasToSend = YES;
                }
            }
            [moc MR_saveToPersistentStoreAndWait];
            
            [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:FIREBASE_ADJUST_OBJECTS_CODIGO];
            
            // Mark to send if is not the first create, because first create always send.
            if (hasToSend && ![Utils valueFromDefaults:FIREBASE_FIRST_CREATE_USER]) {
                [Utils addObjectToDefaults:[NSNumber numberWithBool:YES] withKey:FIREBASE_SEND_ADJUST_OBJECTS_CODIGO];
            }
            
            [Utils dismissProgress];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self startObservers];
            });
        }];
    });
}


@end
