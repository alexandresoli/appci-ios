//
//  FirebaseUtilsUser.h
//  Appci
//
//  Created by Leonardo Barros on 29/12/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TransacaoInAppPurchase.h"
#import "Corretor.h"

@interface FirebaseUtilsUser : NSObject

+ (void)createUser:(NSString *)email withPassword:(NSString *)password andCompletionBlock:(completionWithError) completion;
+ (void)loginUser:(NSString *)email withPassword:(NSString *)password andCompletionBlock:(completionWithError) completion;

+ (NSArray *)getDevices;
+ (void)registerDevice;
+ (void)removeDeviceWithID:(NSString *)deviceID andCompletionBlock:(completionWithError)completion;

+ (void)updateCorretorInformationFromFirebase;

+ (void)updateUserInfoWithEmail:(NSString *)email andName:(NSString *)name andPhone:(NSString *)phone andCompletionBlock:(completionWithError)completion;

+ (void)updateUserInfoWithCorretor:(Corretor *)corretor;

+ (void)changeEmailForUser:(NSString *)oldEmail toNewEmail:(NSString *)newEmail withCompletionBlock:(completionWithError)completion;

+ (void)registerPlanTopInFirebaseWithCorretor:(Corretor *)corretor andTransacao:(TransacaoInAppPurchase *)transacao andCompletionBlock:(completionWithError)completion;

+ (void)resendPurchaseToFirebase:(NSDictionary *)params;

+ (void)resetPassword:(NSString *)email;
+ (void)changePassword:(NSString *)email oldPassword:(NSString *)oldPassword newPassword:(NSString *)newPassword;

@end

