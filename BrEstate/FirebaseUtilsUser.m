//
//  FirebaseUtilsUser.m
//  Appci
//
//  Created by Leonardo Barros on 29/12/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "FirebaseUtilsUser.h"
#import "Corretor+Utils.h"
#import "Corretor+Firebase.h"

@implementation FirebaseUtilsUser


#pragma mark - Create User

+ (void)createUser:(NSString *)email withPassword:(NSString *)password andCompletionBlock:(completionWithError)completion
{
    Firebase *ref = [Utils firebaseUtils].rootRefFirebase;
    
    [ref createUser:email password:password withCompletionBlock:^(NSError *error) {
        
        if (error) {
            completion(error);
            return;
        }
        
        // If create user successful do the login.
        [self loginUser:email withPassword:password andCompletionBlock:^(NSError *error) {
            if (error) {
                completion(error);
                return;
            }
            
            [self updateUserInfoWithEmail:email andName:nil andPhone:nil andCompletionBlock:^(NSError *error) {
                
            }];
            
            completion(nil);
        }];
    }];
}


#pragma mark - Login User

+ (void)loginUser:(NSString *)email withPassword:(NSString *)password andCompletionBlock:(completionWithError)completion
{
    Firebase *ref = [Utils firebaseUtils].rootRefFirebase;
    
    [ref authUser:email password:password withCompletionBlock:^(NSError *error, FAuthData *authData) {
        
        if (error) {
            completion(error);
            return;
        }
        

        // temp password
        if ([[authData.providerData valueForKey:@"isTemporaryPassword"] boolValue]) {
            error = [NSError errorWithDomain:@"br.com.brestate.appci" code:-42 userInfo:@{NSLocalizedDescriptionKey:@"Defina uma nova senha de acesso"}];
            completion(error);
            return;
        }
        

        NSString *userID = authData.uid;//[authData.uid componentsSeparatedByString:@":"].lastObject;
        
        // Register on users tree
        Firebase *appciRef = [[Firebase alloc] initWithUrl:FIREBASE_APPCI_URL];
        
        NSMutableDictionary *newUser = [@{@"provider": authData.provider,
                                          @"email": authData.providerData[@"email"],
                                          @"ios": [Utils valueFromDefaults:@"version"],
                                          @"lastAccess": [DateUtils stringTimestampFromDate:[NSDate date]]} mutableCopy];
        
        Corretor *corretor = [Corretor corretorWithContext:[NSManagedObjectContext MR_contextForCurrentThread]];

        if (corretor.nome && ![corretor.nome isEqualToString:@""]) {
            [newUser setObject:corretor.nome forKey:@"name"];
        }
        
        if (corretor.telefone && ![corretor.telefone isEqualToString:@""]) {
            [newUser setObject:corretor.telefone forKey:@"phone"];
        }
        
        [[[appciRef childByAppendingPath:@"_Users"] childByAppendingPath:userID] updateChildValues:newUser];
        
        // Set the root reference firebase appending the user ID.
        Firebase *userRef = [[Firebase alloc] initWithUrl:FIREBASE_APPCI_URL];
        
        userRef = [userRef childByAppendingPath:userID];
        [Utils firebaseUtils].rootRefFirebase = userRef;
        
        [Utils addObjectToDefaults:userID withKey:FIREBASE_USER_ID];
        
        completion(nil);
    }];
}


#pragma mark - Device

+ (void)registerDevice
{
    NSDictionary *dictDevice = @{@"deviceID": [Utils deviceID],
                                 @"deviceName": [Utils deviceName],
                                 @"lastAccess": [DateUtils stringTimestampFromDate:[NSDate date]]};
    
    Firebase *objectRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:[NSString stringWithFormat:@"Device/%@", [Utils deviceID]]];
    
    [objectRef setValue:dictDevice];
}


+ (NSArray *)getDevices
{
    NSMutableArray *devicesArray = [NSMutableArray new];
    
    Firebase *ref = [Utils firebaseUtils].rootRefFirebase;
    
    ref = [ref childByAppendingPath:@"Device"];
    
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    [ref observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        
        if ([snapshot.value isEqual:[NSNull null]]) {
            dispatch_semaphore_signal(sema);
            
        } else {
            NSDictionary *devices = snapshot.value;
            
            for (NSString *deviceID in devices.allKeys) {
                [devicesArray addObject:[devices objectForKey:deviceID]];
            }
            dispatch_semaphore_signal(sema);
        }
    }];
    
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    return devicesArray;
}


+ (void)removeDeviceWithID:(NSString *)deviceID andCompletionBlock:(completionWithError)completion
{
    Firebase *ref = [Utils firebaseUtils].rootRefFirebase;
    ref = [ref childByAppendingPath:@"Device"];
    ref = [ref childByAppendingPath:deviceID];
    
    // Nedded to receive the item removed local and it doesn't come again on the next query. Seems a bug.
    FirebaseHandle handle = [ref observeEventType:FEventTypeChildRemoved withBlock:^(FDataSnapshot *snapshot) {
    }];
    
    [ref removeValueWithCompletionBlock:^(NSError *error, Firebase *ref) {
        [ref removeObserverWithHandle:handle];
        completion(error);
    }];
    
}


#pragma mark - Update Corretor Information From Firebase

+ (void)updateCorretorInformationFromFirebase
{
    Firebase *ref = [Utils firebaseUtils].rootRefFirebase;
    
    ref = [ref childByAppendingPath:@"UserInfo"];
    
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    [ref observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        
        if ([snapshot.value isEqual:[NSNull null]]) {
            dispatch_semaphore_signal(sema);
            
        } else {
            NSDictionary *userInfo = snapshot.value;
            
            NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
            Corretor *corretor = [Corretor corretorWithContext:moc];
            
            [corretor convertFirebaseDictionary:userInfo];
            
            [moc MR_saveToPersistentStoreAndWait];
            
            dispatch_semaphore_signal(sema);
        }
    }];
    
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
}


#pragma mark - Update User Info In Firebase

+ (void)updateUserInfoWithEmail:(NSString *)email andName:(NSString *)name andPhone:(NSString *)phone andCompletionBlock:(completionWithError)completion
{
    NSDictionary *dictUser = @{@"email": email != nil ? email : [NSNull null],
                               @"name": name != nil ? name : [NSNull null],
                               @"phone": phone != nil ? phone : [NSNull null]};
    
    Firebase *objectRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"UserInfo"];
    
    [objectRef updateChildValues:dictUser withCompletionBlock:^(NSError *error, Firebase *ref) {
        
        if (!error) {
            
            if (email) {
                // Update email on _Users tree
                Firebase *appciRef = [[Firebase alloc] initWithUrl:FIREBASE_APPCI_URL];
                
                [[[appciRef childByAppendingPath:@"_Users"] childByAppendingPath:[Utils valueFromDefaults:FIREBASE_USER_ID]] updateChildValues:dictUser];
            }
        }
        
        completion(error);
    }];
}


+ (void)updateUserInfoWithCorretor:(Corretor *)corretor
{
    NSString *premiumExpiration = [DateUtils stringTimestampFromDate:corretor.dataExpiraPlanoTop];
    
    NSDictionary *dictUser = @{@"email": corretor.email != nil ? corretor.email : [NSNull null],
                               @"name": corretor.nome != nil ? corretor.nome : [NSNull null],
                               @"phone": corretor.telefone != nil ? corretor.telefone : [NSNull null],
                               @"premiumExpiration": premiumExpiration != nil ? premiumExpiration : [NSNull null],
                               @"devicesLimit": corretor.limiteDevices != nil ? corretor.limiteDevices : [NSNull null]};
    
    Firebase *objectRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"UserInfo"];
    
    [objectRef updateChildValues:dictUser];
    
    if (corretor.email) {
        // Update _Users tree
        Firebase *appciRef = [[Firebase alloc] initWithUrl:FIREBASE_APPCI_URL];
        
        NSDictionary *newUser = @{@"email": corretor.email != nil ? corretor.email : [NSNull null],
                                  @"premiumExpiration": premiumExpiration != nil ? premiumExpiration : [NSNull null],
                                  @"name": corretor.nome != nil ? corretor.nome : [NSNull null],
                                  @"phone": corretor.telefone != nil ? corretor.telefone : [NSNull null]};
        
        [[[appciRef childByAppendingPath:@"_Users"] childByAppendingPath:[Utils valueFromDefaults:FIREBASE_USER_ID]] updateChildValues:newUser];
    }
}


#pragma mark - Change User Email

+ (void)changeEmailForUser:(NSString *)oldEmail toNewEmail:(NSString *)newEmail withCompletionBlock:(completionWithError)completion
{
    Firebase *ref = [[Firebase alloc] initWithUrl:FIREBASE_APPCI_URL];
    
    NSString *password = [[NSString alloc] initWithData:[Utils valueFromDefaults:USER_PASS] encoding:NSUTF8StringEncoding];
    
    [ref changeEmailForUser:oldEmail password:password toNewEmail:newEmail withCompletionBlock:^(NSError *error) {
        completion(error);
    }];
}


#pragma mark - Register Plan TOP In Firebase

+ (void)registerPlanTopInFirebaseWithCorretor:(Corretor *)corretor andTransacao:(TransacaoInAppPurchase *)transacao andCompletionBlock:(completionWithError)completion
{
    NSDictionary *dictUser = @{@"premiumExpiration": [DateUtils stringTimestampFromDate:corretor.dataExpiraPlanoTop]};
    
    Firebase *userRef = [[Utils firebaseUtils].rootRefFirebase childByAppendingPath:@"UserInfo"];
    
    [userRef updateChildValues:dictUser withCompletionBlock:^(NSError *error, Firebase *ref) {
        
        if (error) {
            completion(error);
            return;
        }
        
        NSDictionary *transacaoDict = @{@"dateTransaction": [DateUtils stringTimestampFromDate:transacao.dataTransacao],
                                        @"idTransaction": transacao.identificadorTransacao,
                                        @"idProduct": transacao.identificadorProduto,
                                        @"email": corretor.email};
        
        Firebase *rootRef = [[Firebase alloc] initWithUrl:FIREBASE_APPCI_URL];
        Firebase *transacaoRef = [rootRef childByAppendingPath:@"_TransactionInAppPurchase"];
        transacaoRef = [transacaoRef childByAutoId];
        
        [transacaoRef setValue:transacaoDict withCompletionBlock:^(NSError *error, Firebase *ref) {
            if (error) {
                NSLog(@"%@", error.localizedDescription);
                
                // Save the transaction to register later.
                [Utils addObjectToDefaults:transacaoDict withKey:PARAMS_REGISTER_PURCHASE];
            }
        }];
        
        completion(nil);
    }];
}


#pragma mark - Resend Purchase To Firebase

+ (void)resendPurchaseToFirebase:(NSDictionary *)params
{
    Firebase *rootRef = [[Firebase alloc] initWithUrl:FIREBASE_APPCI_URL];
    Firebase *transacaoRef = [rootRef childByAppendingPath:@"_TransactionInAppPurchase"];
    transacaoRef = [transacaoRef childByAutoId];
    
    [transacaoRef setValue:params withCompletionBlock:^(NSError *error, Firebase *ref) {
        
        // Se ocorreu erro mantem os parâmetros da compra no user defaults para tentar registrar novamente.
        if (error) {
            NSLog(@"%@", error.localizedDescription);
            return;
        }
        
        // Remove os parâmetros da compra pois a mesma foi registrada.
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:PARAMS_REGISTER_PURCHASE];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }];
}


#pragma mark - Reset Password
+ (void)resetPassword:(NSString *)email
{
    Firebase *ref = [[Firebase alloc] initWithUrl:FIREBASE_APPCI_URL];
    
    [ref resetPasswordForUser:email withCompletionBlock:^(NSError *error) {
        
        if (error) {
            [Utils stopNetworkIndicator];
            [Utils dismissProgress];
            [Utils showMessage:[[Utils firebaseUtils] messageForError:error]];
            return;
        }
    }];
    
}

#pragma mark - Change Password
+ (void)changePassword:(NSString *)email oldPassword:(NSString *)oldPassword newPassword:(NSString *)newPassword
{
    Firebase *ref = [[Firebase alloc] initWithUrl:FIREBASE_APPCI_URL];
    
    [ref changePasswordForUser:email fromOld:oldPassword toNew:newPassword withCompletionBlock:^(NSError *error) {
    
        [Utils stopNetworkIndicator];
        [Utils dismissProgress];
        
        if (error) {
            [Utils showMessage:[[Utils firebaseUtils] messageForError:error]];
            return;
        }
        
        [Utils showMessage:@"Senha modificada com sucesso!"];
    }];
}


@end
