//
//  FotosHeaderView.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/14/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FotosHeaderView : UICollectionReusableView
- (void) setSearchText:(NSString *)text;
@end
