//
//  IAPHelper.h
//  Appci
//
//  Created by BrEstate LTDA on 10/06/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "Corretor.h"

@interface IAPHelper : NSObject <SKPaymentTransactionObserver, SKProductsRequestDelegate, UIAlertViewDelegate>

- (void)buyPlanTOP;
- (BOOL)checkPlanTopFromController:(UIViewController *)sender withMessage:(NSString *)message;
- (BOOL)isPlanTopValidForCorretor:(Corretor *)corretor;
- (void)checkPlanTopIsOverToCreateNotification;
- (void)checkPlanTopWillEndToCreateNotification;
//- (void)resendPurchaseToParse:(NSDictionary *)params;

@end
