//
//  IAPHelper.m
//  Appci
//
//  Created by BrEstate LTDA on 10/06/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "IAPHelper.h"
#import "SVProgressHUD.h"
#import "DateUtils.h"
#import "Corretor+Utils.h"
#import "TransacaoInAppPurchase.h"
#import "Imobiliaria.h"
#import "FirebaseUtilsUser.h"

@interface IAPHelper () <SKPaymentTransactionObserver, SKProductsRequestDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) NSManagedObjectContext *moc;
@property (nonatomic, strong) NSString *productID;
@property (nonatomic, strong) UIViewController *activeController;

@end


@implementation IAPHelper


#pragma mark - Get MOC

- (NSManagedObjectContext *)moc
{
    if (_moc) {
        return _moc;
    }
    
    _moc = [NSManagedObjectContext MR_defaultContext];
    
    return _moc;
}


#pragma mark - Product ID

- (NSString *)productID
{
    if (_productID) {
        return _productID;
    }
    
    _productID = @"br.com.brestate.appci.inappappci.1mesplanotop";
    
    return _productID;
}


#pragma mark - Buy Plano TOP

- (void)buyPlanTOP
{
    [Utils trackWithName:@"Clicou Comprar Plano TOP"];
    [Utils trackFlurryWithName:@"Clicou Comprar Plano TOP"];
    
    // Verifica se o plano TOP ainda está ativo.
    Corretor *corretor = [Corretor MR_findFirstInContext:self.moc];
    
    if (![Utils isConnected]) {
        [Utils showMessage:@"Verifique sua conexão com a internet."];
        
    } else {
        
        [SVProgressHUD showWithStatus:@"Verificando assinatura. Aguarde..." maskType:SVProgressHUDMaskTypeBlack];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            [FirebaseUtilsUser updateCorretorInformationFromFirebase];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([self checkPlanTopFromController:nil withMessage:nil]) {
                    [SVProgressHUD dismiss];
                    
                    [Utils showMessage:[NSString stringWithFormat:@"Você já é um usuário TOP, seu plano foi restaurado com sucesso. \nSeu plano expira em %@", [DateUtils stringFromDate:corretor.dataExpiraPlanoTop]]];
                    
                } else {
                    [self startPayment];
                }
            });
        });
    }
}


- (void)startPayment
{
    // Verifica se o device tem acesso a internet.
    if (![Utils isConnected]) {
        [Utils showMessage:@"Para assinar o plano TOP é necessário estar conectado à internet."];
        
        return;
    }
    
    // Inicia a solicitação de pagamento.
    if ([SKPaymentQueue canMakePayments]) {
        
        SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:self.productID]];
        request.delegate = self;
        
        [request start];
        
        [Utils showProgress:@"Processando"];
        
    } else {
        [Utils showMessage:@"Habilite o In App Purchase na sua conta da App Store."];
    }
}


#pragma mark - SKProductsRequestDelegate

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    NSArray *products = response.products;
    
    if (products.count > 0) {
        
        SKProduct *product = [products firstObject];
        
        SKPayment *payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        
    } else {
        [SVProgressHUD dismiss];
        [Utils showMessage:@"Erro ao verificar o produto na App Store"];
    }
}


#pragma mark - SKPaymentTransactionObserver

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions) {
        
        switch (transaction.transactionState) {
            
            case SKPaymentTransactionStatePurchasing:
                
                [SVProgressHUD showWithStatus:@"Acessando App Store" maskType:SVProgressHUDMaskTypeBlack];
                break;
                
            case SKPaymentTransactionStatePurchased:
                
                [self unlockPlanoTop:transaction];
                
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                [SVProgressHUD dismiss];
                
                break;
                
            case SKPaymentTransactionStateFailed:
                
                NSLog(@"Transaction Failed - %@", transaction.error);
                
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                [SVProgressHUD dismiss];
                
                [Utils showMessage:@"Ocorreu um erro na comunicação com a App Store. Tenta novamente. Se persistir o erro entre em contato com o suporte."];
                
                break;
                
            default:
                break;
        }
    }
}


#pragma mark - Unlock Plano Top

- (void)unlockPlanoTop:(SKPaymentTransaction *)transaction
{
    // Busca o corretor para pegar a data de expiração do plano top e para atualizá-la.
    Corretor *corretor = [Corretor corretorWithContext:self.moc];
    
    NSDate *expirationDate = corretor.dataExpiraPlanoTop;
    
    // Calcula a nova data de expiração.
    NSDate *originDate = [DateUtils dateWithoutTime:[NSDate date]];
    
    if (expirationDate) {
        // Se a data de expiração é maior que a data atual o plano ainda não expirou,
        // então usa a data de expiração como base para somar o mês.
        if ([expirationDate compare:originDate] == NSOrderedDescending) {
            originDate = expirationDate;
        }
    }
    
    // Soma um mês na data de expiração.
    NSDateComponents *dateComp = [[NSDateComponents alloc] init];
	[dateComp setDay:30];
    
    expirationDate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComp toDate:originDate options:0];
    
    corretor.dataExpiraPlanoTop = expirationDate;
    corretor.planoTopValido = [NSNumber numberWithBool:YES];
    corretor.version = [NSDate date];
    
    TransacaoInAppPurchase *transacao = [TransacaoInAppPurchase MR_createInContext:self.moc];
    
    transacao.identificadorProduto = self.productID;
    transacao.identificadorTransacao = transaction.transactionIdentifier;
    transacao.dataTransacao = transaction.transactionDate;
    
    [self.moc MR_saveToPersistentStoreAndWait];
    
    // Remove a marcação de notificação sobre término do plano top para que volte a verificar quando o plano termina.
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:NOTIFICATION_PLAN_TOP_WAS_SHOWN];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
//    [self registerPlaTopOnParseWithCorretor:corretor andTransacao:transacao];
    
    [FirebaseUtilsUser registerPlanTopInFirebaseWithCorretor:corretor andTransacao:transacao andCompletionBlock:^(NSError *error) {
        
        [Utils dismissProgress];
        
        if (error) {
            [Utils showMessage:@"Ocorreu um erro ao registrar a sua assinatura. Entre em contato com suporte@brestate.com.br"];
            return;
        }
        
        [Utils showMessage:[NSString stringWithFormat:@"Plano TOP assinado com sucesso! Seu plano é válido até %@.", [DateUtils stringFromDate:corretor.dataExpiraPlanoTop]]];
    }];
    
    // Start firebase observers.
    dispatch_async(dispatch_get_main_queue(), ^{
        [[Utils firebaseUtils] startObservers];
    });
}


#pragma mark - Check Plano TOP

- (BOOL)checkPlanTopFromController:(UIViewController *)sender withMessage:(NSString *)message
{
    
    // verifica se o usuario é de imobiliaria
    Imobiliaria *imobiliaria = [[CoreDataUtils fetchEntity:@"Imobiliaria" withPredicate:nil inContext:self.moc] firstObject];
    
    if (imobiliaria != nil) {
        return YES;
    }
    
    
    // verificação de corretor autonomo
    
    BOOL planoTop;
    
    Corretor *corretor = [Corretor MR_findFirstInContext:self.moc];
    
    if (!corretor.dataExpiraPlanoTop) {
        planoTop = NO;
        
        if (sender) { // If sender was informed has to show message about plan TOP.
            self.activeController = sender;
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Funcionalidade TOP"
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancelar"
                                                  otherButtonTitles:@"Assinar Plano TOP", @"Mais Informações", nil];
            [alert show];
        }
        
    } else {
        
        if ([self isPlanTopValidForCorretor:corretor]) {
            planoTop = YES;
            
        } else {
            planoTop = NO;
            
            if (sender) {
                NSString *message = [NSString stringWithFormat:@"Sua assinatura do plano TOP terminou em %@. \n \nDeseja renovar assinatura por 1 mês agora?", [DateUtils stringFromDate:corretor.dataExpiraPlanoTop]];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Plano TOP Expirado"
                                                                message:message
                                                               delegate:self
                                                      cancelButtonTitle:@"Não"
                                                      otherButtonTitles:@"Sim", nil];
                [alert show];
            }
        }
    }
    
    return planoTop;
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView.title isEqualToString:@"Plano TOP Expirado"]) {
        if (buttonIndex == 1) {
            [self buyPlanTOP];
        }
        
    } else if ([alertView.title isEqualToString:@"Funcionalidade TOP"]) {
        switch (buttonIndex) {
            case 1:
                
                // Verifica se os dados pessoais estão preenchidos.
                if (![Utils personalDataIsFilled:self.activeController
                                     withContext:self.moc
                                      andMessage:@"Para assinar o plano TOP é necessário o preenchimento de suas informações!"]) {
                    return;
                }
                
                [self buyPlanTOP];
                break;
                
            case 2: {
                [Utils trackWithName:@"Clicou em Mais Informações do Plano TOP pelo Alert"];
                [Utils trackFlurryWithName:@"Clicou em Mais Informações do Plano TOP pelo Alert"];
                [self showPlanoTopScreenForController:self.activeController];
                break;
            }
                
            default:
                break;
        }
        
        self.activeController = nil;
    }
}


#pragma mark - Is Plan TOP Valid

- (BOOL)isPlanTopValidForCorretor:(Corretor *)corretor
{
    BOOL valid = NO;
    
    if (corretor) {
        
        if ([corretor.planoTopValido boolValue]) {
            
            NSDate *today = [DateUtils dateWithoutTime:[NSDate date]];
            
            if (([today compare:corretor.dataExpiraPlanoTop] == NSOrderedAscending)
                || ([today compare:corretor.dataExpiraPlanoTop] == NSOrderedSame)) {
                
                valid = YES;
                
            } else {
                // Atualiza a flag de plano top válido do corretor para NO.
                corretor.planoTopValido = [NSNumber numberWithBool:NO];
                [Utils saveMOC:self.moc];
            }
        }
    }
    
    return valid;
}


#pragma mark - Check if Plan Top is Over

- (void)checkPlanTopIsOverToCreateNotification
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    BOOL notificationPlanTopWasShown = [userDefaults boolForKey:NOTIFICATION_PLAN_TOP_WAS_SHOWN];
    
    if (!notificationPlanTopWasShown) {
        
        Corretor *corretor = [Corretor MR_findFirst];
        
        if (corretor.dataExpiraPlanoTop) {
            
            if (![self isPlanTopValidForCorretor:corretor]) {
                
                UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                
                localNotification.alertBody = [NSString stringWithFormat:@"Seu plano TOP expirou em %@. Renove seu plano para manter acesso as funcionalidades TOP.", [DateUtils stringFromDate:corretor.dataExpiraPlanoTop]];
                
                localNotification.alertAction = @"visualizar";
                localNotification.soundName = UILocalNotificationDefaultSoundName;
                localNotification.applicationIconBadgeNumber = 1;
                
                [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
                
                [userDefaults setBool:YES forKey:NOTIFICATION_PLAN_TOP_WAS_SHOWN];
                [userDefaults synchronize];
                
            }
        }
    }
}


- (void)checkPlanTopWillEndToCreateNotification
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    BOOL notificationPlanTopWillEnd = [userDefaults boolForKey:NOTIFICATION_PLAN_TOP_WILL_END];
    
    if (!notificationPlanTopWillEnd) {
        
        Corretor *corretor = [Corretor MR_findFirst];
        
        if (corretor.dataExpiraPlanoTop) {
            NSDate *today = [DateUtils dateWithoutTime:[NSDate date]];
            
            NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                                fromDate:today
                                                                  toDate:corretor.dataExpiraPlanoTop
                                                                 options:0];
            NSInteger daysLeft = [components day];
            
            if (daysLeft <= 3) {
                
                UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                
                localNotification.alertBody = [NSString stringWithFormat:@"Seu plano TOP vai expirar em %@. Renove sua assinatura pelo app após essa data ou acesse www.brestate.com.br e veja outras opções de assinatura.", [DateUtils stringFromDate:corretor.dataExpiraPlanoTop]];
                
                localNotification.alertAction = @"visualizar";
                localNotification.soundName = UILocalNotificationDefaultSoundName;
                localNotification.applicationIconBadgeNumber = 1;
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.fireDate = [[NSDate date] dateByAddingTimeInterval:120];
                
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification]; //presentLocalNotificationNow:localNotification];
                
                [userDefaults setBool:YES forKey:NOTIFICATION_PLAN_TOP_WILL_END];
                [userDefaults synchronize];
            }
        }
    }
}


#pragma mark - Show Plano TOP Screen

- (void)showPlanoTopScreenForController:(UIViewController *)controller
{
    UIStoryboard *storyboard;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        storyboard = [UIStoryboard storyboardWithName:@"Acessorios_iPad" bundle:nil];
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Acessorios_iPhone" bundle:nil];
    }
    
    UINavigationController *navController = [storyboard instantiateViewControllerWithIdentifier:@"AssinarPlanoTop"];
    navController.modalPresentationStyle = UIModalPresentationFormSheet;
    [controller presentViewController:navController animated:YES completion:nil];
}


@end
