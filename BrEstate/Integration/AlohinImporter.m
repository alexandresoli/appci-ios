//
//  AlohinImporter.m
//  Appci
//
//  Created by BrEstate LTDA on 4/28/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "AlohinImporter.h"
#import "TagImovel.h"
#import "Endereco.h"
#import "Estado.h"
#import "Cidade.h"
#import "Bairro.h"
#import "SVProgressHUD.h"
#import "FotoImovel+InsertDeleteImagem.h"
#import "NSString+HTML.h"
#import "NSString+Custom.h"
#import "DateUtils.h"
#import "Imovel.h"
#import "AlohinViewController.h"
#import "ASIHTTPRequest.h"
#import "XMLDictionary.h"

@interface AlohinImporter ()

#pragma mark - Properties

@property (nonatomic, strong) NSString *checksum;
@property (nonatomic, strong) NSMutableArray *retryPhotos;
@property (nonatomic, strong) NSDictionary *dictionary;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSMutableArray *imoveis;
@property (nonatomic) int totalPhotos;
@property (nonatomic) BOOL forceSync;

@end

@implementation AlohinImporter


#pragma mark - Normalize URL
- (NSString *)normalizeURL:(NSString *)strURL
{
    
    NSString *lastChar = [strURL substringFromIndex:[strURL length]-1];
    
    if([lastChar isEqualToString:@"/"]) {
        strURL = [strURL stringByAppendingString:@"exportar/alohin"];
    } else {
        strURL = [strURL stringByAppendingString:@"/exportar/alohin"];
    }
    
    if(![strURL hasPrefix:@"http"]) {
        strURL = [@"http://" stringByAppendingString:strURL];
    }
    
    return strURL;
}


#pragma mark - Start XML Parsing
- (void)startWithURL:(NSString *)strURL usingMoc:(NSManagedObjectContext *)moc forced:(BOOL)forced
{
    
    [Utils startNetworkIndicator];
    
    strURL = [self normalizeURL:strURL.trimmed];
    _forceSync = forced;
    _retryPhotos = [NSMutableArray array];
    _photos = [NSMutableArray array];
    
    NSURL *url = [NSURL URLWithString:strURL];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    request.timeOutSeconds = 30;
    [request startSynchronous];
    NSError *error = [request error];
    
    if (error) {
        [Utils dismissProgress];
        [Utils showMessage:MESSAGE_IMPORT_ERROR];
        [Utils stopNetworkIndicator];
        return;
    }
    
    
    NSString *xml = request.responseString;
    NSString *newChecksum = [xml MD5];
    
    if (!_forceSync) {
        
        // checksum check
        NSString *oldChecksum = [Utils valueFromDefaults:ALOHIN_CHECKSUM];
        if ([newChecksum isEqualToString:oldChecksum]) {
            
            [Utils dismissProgress];
            [Utils stopNetworkIndicator];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Aviso" message:@"As informações já estão atualizadas, deseja forçar a sincronização?" delegate:_parentController cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Forçar", nil];
                [alertView show];
            });
            
            
            return;
        }
        
    }
    
    _checksum = newChecksum;
    
    
    [Utils stopNetworkIndicator];
    [Utils dismissProgress];
    
    _parentController.hasStarted = YES;
    _dictionary = [NSDictionary dictionaryWithXMLData:request.responseData];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        
        [localContext performBlock:^{
            
            [self convert:localContext];
            
            if ([self.imoveis count] == 0) {
                [Utils showMessage:MESSAGE_EMPTY_IMPORT];
                [Utils dismissProgress];
                return;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                _progressView.progress = 0.0f;
                _progressLabel.text = @"";
                
            });
            
            // update checksum
            [Utils addObjectToDefaults:_checksum withKey:ALOHIN_CHECKSUM];
            
            [self startBackgroundDownloadsForBaseURL:localContext];
            
            if (_retryPhotos.count > 0) {
                [self redownloadPhotos:localContext];
            }
            
            [Utils dismissProgress];
            [Utils postEntitytNotification:@"Imovel"];
            [Utils stopNetworkIndicator];
            
            if (_parentController != nil) {
                [Utils showMessage:MESSAGE_FINISH_IMPORT];
            }
            
            [_parentController setHasStarted:NO];
            
        }];
        
    });
}

#pragma mark - Convert Objects to NSManagedObject
- (void)convert:(NSManagedObjectContext *)moc
{
    
    NSNumberFormatter *currencyFmt = [[NSNumberFormatter alloc] init];
    [currencyFmt setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyFmt setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [currencyFmt setDecimalSeparator:@","];
    [currencyFmt setGeneratesDecimalNumbers:YES];
    [currencyFmt setCurrencySymbol:@"R$ "];
    [currencyFmt setLenient:YES];
    
    _imoveis = [_dictionary objectForKey:@"Imovel"] ;
    
    __block long total = [_imoveis count];
    __block int step = 0;
    __block float progressStep = 0.0f;
    
    @autoreleasepool {
        
        for(NSDictionary *obj in _imoveis) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                step++;
                progressStep += 1.0/total;
                [_progressView setProgress:progressStep animated:YES];
                _progressLabel.text = [NSString stringWithFormat:@"Importando: %d/%ld",step, total];
            });
            
            long count = 0;
            
            Imovel *imovel;
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",obj[@"ImovelId"]];
            
            count = [Imovel MR_countOfEntitiesWithPredicate:predicate inContext:moc];
            
            BOOL exists = NO;
            if(count > 0 ) {
                imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:moc];
                exists = YES;
                imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
                
            } else {
                
                imovel = [Imovel MR_createInContext:moc];
                imovel.codigo = [NSNumber numberWithLongLong:[obj[@"ImovelId"] longLongValue]];
            }
            
            // only continues if date if different
            NSDate *lastUpdate = [DateUtils dateFromString:obj[@"ImovelUpdateDate"] withFormat:@"yyyy-MM-dd HH:mm:ss"];;
            
            if ([imovel.lastUpdate isEqualToDate:lastUpdate] && !_forceSync) {
                imovel.pendingUpdateParse = [NSNumber numberWithBool:NO];
                continue;
            }
            
            imovel.lastUpdate = lastUpdate;
            
            
            imovel.descricao = obj[@"ImovelHtmlDescription"];
            imovel.version = [NSDate date];
            imovel.identificacao = [obj[@"ImovelId"] stringByConvertingHTMLToPlainText];
            
            
            
            // Tipo Imovel
            NSString *subcategoria = obj[@"SubCategoria"];
            
            if([subcategoria isEqualToString:@"Casa Plana"]) {
                imovel.tipo = @"Casa";
            } else if([subcategoria isEqualToString:@"Casa em Condomínio"]) {
                imovel.tipo = @"Casa";
            } else if([subcategoria isEqualToString:@"Casa Plana"]) {
                imovel.tipo = @"Casa";
            } else if([subcategoria isEqualToString:@"Salas Comerciais"]) {
                imovel.tipo = @"Sala / Espaço Comercial";
            } else if([subcategoria isEqualToString:@"Lotes residenciais"]) {
                imovel.tipo = @"Outro";
            } else if([subcategoria isEqualToString:@"Condomínio Fechado"]) {
                imovel.tipo = @"Condomínio";
            } else if([subcategoria isEqualToString:@"Apartamento"]) {
                imovel.tipo = @"Apartamento";
            } else if([subcategoria isEqualToString:@"Loteamento Fechado"]) {
                imovel.tipo = @"Outro";
            } else if([subcategoria isEqualToString:@"Cobertura Duplex"]) {
                imovel.tipo = @"Cobertura";
            } else if([subcategoria isEqualToString:@"Casa Duplex"]) {
                imovel.tipo = @"Duplex";
            } else if([subcategoria isEqualToString:@"Loteamento Misto"]) {
                imovel.tipo = @"Outro";
            } else if([subcategoria isEqualToString:@"Fazenda"]) {
                imovel.tipo = @"Fazenda / Sítio";
            } else if([subcategoria isEqualToString:@"Cobertura Triplex"]) {
                imovel.tipo = @"Cobertura";
            } else if([subcategoria isEqualToString:@"Loteamento Aberto"]) {
                imovel.tipo = @"Outro";
            } else if([subcategoria isEqualToString:@"Flat"]) {
                imovel.tipo = @"Flat";
            } else if([subcategoria isEqualToString:@"Alvenaria"]) {
                imovel.tipo = @"Outro";
            } else if([subcategoria isEqualToString:@"Fazenda"]) {
                imovel.tipo = @"Fazenda / Sítio";
            } else if([subcategoria isEqualToString:@"Rancho"]) {
                imovel.tipo = @"Fazenda / Sítio";
            } else {
                imovel.tipo = @"Outro";
            }
            
            
            // Tipo Oferta
            NSString *oferta = [obj[@"ImovelFinalidadeDestaque"] uppercaseString];
            NSDictionary *finalidade;
            
            if([oferta isEqualToString:@"VENDA"]) {
                imovel.oferta = @"Venda";
                finalidade = [obj[@"ImovelFinalidade"] objectForKey:@"Venda"];
            } else if([oferta isEqualToString:@"ALUGUEL"]) {
                imovel.oferta = @"Locação";
                finalidade = [obj[@"ImovelFinalidade"] objectForKey:@"Locacao"];
            } else if([oferta isEqualToString:@"TEMPORADA"]) {
                imovel.oferta = @"Temporada";
                finalidade = [obj[@"ImovelFinalidade"] objectForKey:@"Temporada"];
            } else {
                imovel.oferta = @"Outro";
            }
            
            
            
            
            // valor
            NSNumber *valor = [currencyFmt numberFromString:finalidade[@"Valor"]];
            if ([valor intValue] > 0) {
                imovel.valor = [NSDecimalNumber decimalNumberWithDecimal:[valor decimalValue]];
            }
            
            // condominio
            valor = [currencyFmt numberFromString:finalidade[@"ValorCondominio"]];
            
            if ([valor intValue] > 0) {
                imovel.condominio = [NSDecimalNumber decimalNumberWithDecimal:[valor decimalValue]];
            }
            
            
            // iptu
            valor = [currencyFmt numberFromString:finalidade[@"ValorIPTU"]];
            
            if ([valor intValue] > 0) {
                imovel.iptu = [NSDecimalNumber decimalNumberWithDecimal:[valor decimalValue]];
            }
            
            
            // area util / area total
            NSNumber *areaTotal = [NSNumber numberWithLong:[obj[@"ImovelAreaTotal"] integerValue]];
            if ([areaTotal intValue] > 0) {
                imovel.areaTotal =  areaTotal;
            }
            
            NSNumber *areaUtil = [NSNumber numberWithLong:[obj[@"ImovelAreaUtil"] integerValue]];
            if ([areaUtil intValue] > 0) {
                imovel.areaUtil =  areaUtil;
            }
            
            
            // quartos
            NSNumber *quartos = [NSNumber numberWithLong:[obj[@"ImovelDormitorio"] integerValue]];
            
            if ([quartos intValue] > 0) {
                imovel.quartos = quartos;
            }
            
            // tags
            TagImovel *tagImovel;
            
            
            if (imovel.tags != nil) {
                
                tagImovel = imovel.tags;
                
            } else {
                
                tagImovel = [TagImovel MR_createInContext:moc];
                tagImovel.codigo = [tagImovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
                
            }
            
            if([obj[@"ImovelVaga"] intValue] > 0) {
                tagImovel.garagem = [NSNumber numberWithBool:YES];
            } else {
                tagImovel.garagem = [NSNumber numberWithBool:NO];
            }
            
            imovel.tags = tagImovel;
            
            
            // Endereco
            Endereco *endereco;
            if (imovel.endereco != nil) {
                endereco = imovel.endereco;
            } else {
                
                endereco = [Endereco MR_createInContext:moc];
                endereco.codigo = [endereco getUniqueCode]; //[NSNumber numberWithLong:codigo];
            }
            
            
            // cep
            if ([obj[@"imovelCep"] length] > 0) {
                endereco.cep = [NSNumber numberWithInt:[obj[@"imovelCep"] intValue]];
            }
            
            // logradouro
            if ([obj[@"ImovelRua"] length] > 0) {
                endereco.logradouro = obj[@"ImovelRua"];
                
                if ([obj[@"ImovelNumero"] length] > 0) {
                    endereco.logradouro = [endereco.logradouro stringByAppendingString:[NSString stringWithFormat:@", %@",obj[@"ImovelNumero"]]];
                }
            }
            
            // complemento
            if ([obj[@"ImovelComplemento"] length] > 0) {
                endereco.complemento = obj[@"ImovelComplemento"];
                
                if ([obj[@"ImovelPontoReferencia"] length] > 0) {
                    endereco.complemento = [obj[@"ImovelComplemento"] stringByAppendingString:[NSString stringWithFormat:@" - %@", obj[@"ImovelPontoReferencia"]]];
                }
                
            }
            
            
            
            if ([obj[@"Estado"] length] > 0) {
                
                predicate = [NSPredicate predicateWithFormat:@"sigla == [cd] %@",obj[@"Estado"]];
                Estado *estado = [Estado MR_findFirstWithPredicate:predicate inContext:moc];
                
                if (estado != nil) {
                    endereco.estado = estado.nome;
                }
                
            }
            
            
            Cidade *cidade;
            if ([obj[@"Cidade"] length] > 0) {
                
                predicate = [NSPredicate predicateWithFormat:@"nome == [cd] %@",obj[@"Cidade"]];
                cidade = [Cidade MR_findFirstWithPredicate:predicate inContext:moc];
                
                if (cidade != nil) {
                    endereco.cidade = cidade.nome;
                }
            }
            
            
            
            if ([obj[@"Bairro"] length] > 0) {
                
                predicate = [NSPredicate predicateWithFormat:@"cidade == [cd] %@ AND nome == [cd] %@",cidade.nome.trimmed, obj[@"Bairro"]];
                count = [Bairro MR_countOfEntitiesWithPredicate:predicate inContext:moc];
                
                Bairro *bairro = nil;
                
                if(count == 0 ) {
                    
                    bairro = [Bairro MR_createInContext:moc];
                    bairro.codigo = [bairro getUniqueCode]; //[NSNumber numberWithLong:codigo];
                    bairro.nome = obj[@"Bairro"];
                    bairro.cidade = cidade.nome;
                    bairro.version = [NSDate date];
                    
                } else {
                    
                    predicate = [NSPredicate predicateWithFormat:@"cidade == [cd] %@ AND nome == [cd] %@",cidade.nome, obj[@"Bairro"]];
                    
                    bairro = [Bairro MR_findFirstWithPredicate:predicate inContext:moc];
                }
                
                endereco.bairro = bairro;
                
            }
            
            
            imovel.endereco = endereco;
            
            if (exists) {
                [Utils deleteAllPhotos:imovel withMoc:moc];
            }
            
            // capa
            NSMutableDictionary *newMedia = [NSMutableDictionary dictionary];
            [newMedia setValue:obj[@"ImovelImagem"] forKey:@"Arquivo"];
            [newMedia setValue:imovel.codigo forKey:@"codigoImovel"];
            [newMedia setValue:[NSNumber numberWithBool:YES] forKey:@"capa"];
            [_photos addObject:newMedia];
            _totalPhotos++;
            
            // fotos
            NSDictionary *fotos = [[obj objectForKey:@"ImovelMidia"] objectForKey:@"Imagem"];
            
            for (NSDictionary *photo in fotos) {
                
                if (![photo isKindOfClass:[NSDictionary class]]) {
                    
                    newMedia = [NSMutableDictionary dictionary];
                    [newMedia setValue:fotos[@"Arquivo"] forKey:@"Arquivo"];
                    [newMedia setValue:imovel.codigo forKey:@"codigoImovel"];
                    [newMedia setValue:[NSNumber numberWithBool:YES] forKey:@"capa"];
                    [_photos addObject:newMedia];
                    _totalPhotos++;
                    break;
                    
                }
                
                newMedia = [NSMutableDictionary dictionaryWithDictionary:photo];
                [newMedia setValue:imovel.codigo forKey:@"codigoImovel"];
                
                [_photos addObject:newMedia];
                _totalPhotos++;
                
            }
            
            
            // Planta
            NSDictionary *plantas = [[obj objectForKey:@"ImovelMidia"] objectForKey:@"Planta"];
            
            for (NSDictionary *photo in plantas) {
                
                if (![photo isKindOfClass:[NSDictionary class]]) {
                    
                    newMedia = [NSMutableDictionary dictionary];
                    [newMedia setValue:plantas[@"Arquivo"] forKey:@"Arquivo"];
                    [newMedia setValue:imovel.codigo forKey:@"codigoImovel"];
                    [newMedia setValue:[NSNumber numberWithBool:YES] forKey:@"capa"];
                    [_photos addObject:newMedia];
                    _totalPhotos++;
                    break;
                    
                }
                
                newMedia = [NSMutableDictionary dictionaryWithDictionary:photo];
                [newMedia setValue:imovel.codigo forKey:@"codigoImovel"];
                [_photos addObject:newMedia];
                _totalPhotos++;
                
            }
            
            imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
        }
        
        [moc MR_saveToPersistentStoreAndWait];
    }
    
    
}



#pragma mark - Photo Download
- (void)startBackgroundDownloadsForBaseURL:(NSManagedObjectContext *)moc
{
    [Utils startNetworkIndicator];
    
    __block int step = 0;
    __block float progressStep = 0.0;
    
    for (NSDictionary *photo in _photos)
    {
        
        if (_parentController == nil) {
            break;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/_totalPhotos;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Transferindo fotos: %d/%d",step, _totalPhotos];
        });
        
        @autoreleasepool {
            [self downloadPhoto:photo usingMoc:moc];
        }
    }
}

- (void)redownloadPhotos:(NSManagedObjectContext *)moc
{
    __block int step = 0;
    __block float progressStep = 0.0;
    
    for(NSDictionary *photo in _retryPhotos){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/_retryPhotos.count;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Reparando fotos : %d/%lu",step, (unsigned long)_retryPhotos.count];
            
        });
        
        @autoreleasepool {
            [self downloadPhoto:photo usingMoc:moc];
        }
    }
}

- (void)downloadPhoto:(NSDictionary *)photo usingMoc:(NSManagedObjectContext *)moc
{
    
    NSString *strURL = [photo[@"Arquivo"] trimmed];
    
    if (strURL.length == 0) {
        return;
    }
    
    NSURL *photoURL = [NSURL URLWithString:strURL];
    
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:photoURL];
    request.timeOutSeconds = 60;
    [request startSynchronous];
    NSError *error = [request error];
    
    if (error) {
        
        [_retryPhotos addObject:photo];
        NSLog(@"%@",[error description]);
    }
    
    FotoImovel *fotoImovel = [FotoImovel MR_createInContext:moc];
    fotoImovel.codigo = [fotoImovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
    
    fotoImovel.descricao = [[strURL lastPathComponent] stringByDeletingPathExtension];
    fotoImovel.importacao = [NSNumber numberWithBool:YES];
    
    NSNumber *codigoImovel = photo[@"codigoImovel"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",codigoImovel];
    fotoImovel.imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:moc];
    
    fotoImovel.capa = photo[@"capa"];
    
    [fotoImovel insertImageOnDisk:request.responseData];
    
    [moc MR_saveToPersistentStoreAndWait];
}


@end
