//
//  ArcoInformatica.h
//  Appci
//
//  Created by Alexandre Oliveira on 10/3/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ArcoInformatica : NSObject

@property(nonatomic, strong) NSMutableString *identificacao; // codigo do imovel arco
@property(nonatomic, strong) NSMutableString *referencia;
@property(nonatomic, strong) NSMutableString *tipo;
@property(nonatomic, strong) NSMutableString *empreendimento;
@property(nonatomic, strong) NSMutableString *financiavel;
@property(nonatomic, strong) NSMutableString *categoria;
@property(nonatomic, strong) NSMutableString *titulo;
@property(nonatomic, strong) NSMutableString *preco;
@property(nonatomic, strong) NSMutableString *iptu;
@property(nonatomic, strong) NSMutableString *condominio;
@property(nonatomic, strong) NSMutableString *quartos;
@property(nonatomic, strong) NSMutableString *suites;
@property(nonatomic, strong) NSMutableString *endereco;
@property(nonatomic, strong) NSMutableString *bairro;
@property(nonatomic, strong) NSMutableString *cidade;
@property(nonatomic, strong) NSMutableString *uf;
@property(nonatomic, strong) NSMutableString *descricao;

@property(nonatomic, strong) NSMutableArray *fotos;
@property(nonatomic, strong) NSNumber *codigoImovel;

@end
