
//
//  ArcoInformatica.m
//  Appci
//
//  Created by Alexandre Oliveira on 10/3/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "ArcoInformatica.h"

@implementation ArcoInformatica

- (id)init
{
    if(self = [super init]) {
        
        self.identificacao = [NSMutableString string];
        self.referencia = [NSMutableString string];
        self.tipo = [NSMutableString string];
        self.empreendimento = [NSMutableString string];
        self.financiavel = [NSMutableString string];
        self.categoria = [NSMutableString string];
        self.titulo = [NSMutableString string];
        self.preco = [NSMutableString string];
        self.iptu = [NSMutableString string];
        self.condominio = [NSMutableString string];
        self.quartos = [NSMutableString string];
        self.suites = [NSMutableString string];
        self.endereco = [NSMutableString string];
        self.bairro = [NSMutableString string];
        self.cidade = [NSMutableString string];
        self.uf = [NSMutableString string];
        self.descricao = [NSMutableString string];
        
        self.fotos = [NSMutableArray array];
        self.codigoImovel = [NSNumber numberWithInt:0];
        
    }
    
    return self;
}

@end
