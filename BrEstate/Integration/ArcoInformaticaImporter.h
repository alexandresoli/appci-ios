//
//  ArcoInformaticaImporter.h
//  Appci
//
//  Created by Alexandre Oliveira on 10/3/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ArcoInformaticaController;
@interface ArcoInformaticaImporter : NSObject

@property (nonatomic, weak) UIProgressView *progressView;
@property (nonatomic, weak) UILabel *progressLabel;

@property (nonatomic, weak)  ArcoInformaticaController *parentController;

- (void)startWithURL:(NSString *)strURL usingMoc:(NSManagedObjectContext *)moc forced:(BOOL)forced;

@end
