//
//  ArcoInformaticaImporter.m
//  Appci
//
//  Created by Alexandre Oliveira on 10/3/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "ArcoInformaticaImporter.h"
#import "ArcoInformatica.h"
#import "Imovel.h"
#import "Endereco.h"
#import "Estado.h"
#import "Cidade.h"
#import "Bairro.h"
#import "NSString+Custom.h"
#import "FotoImovel+InsertDeleteImagem.h"
#import "ArcoInformaticaController.h"
#import "ASIHTTPRequest.h"


@interface ArcoInformaticaImporter () <NSXMLParserDelegate>

#pragma mark - Properties
@property(nonatomic, strong) NSXMLParser *parser;
@property(nonatomic, strong) NSString *element;
@property(nonatomic, strong) NSMutableArray *imoveis;
@property(nonatomic, strong) NSMutableArray *refs;
@property(nonatomic, strong) ArcoInformatica *arco;
@property(nonatomic, strong) NSMutableDictionary *foto;
@property (nonatomic) int totalPhotos;
@property (nonatomic, strong) NSString *checksum;
@property (nonatomic) BOOL forceSync;
@property(nonatomic, strong) NSMutableArray *retryPhotos;

@end

@implementation ArcoInformaticaImporter

#pragma mark - Normalize URL
- (NSString *)normalizeURL:(NSString *)strURL
{
    
    NSString *lastChar = [strURL substringFromIndex:[strURL length]-1];
    
    if([lastChar isEqualToString:@"/"]) {
        strURL = [strURL stringByAppendingString:@"xml.php"];
    } else {
        strURL = [strURL stringByAppendingString:@"/xml.php"];
    }
    
    if(![strURL hasPrefix:@"http"]) {
        strURL = [@"http://" stringByAppendingString:strURL];
    }
    
    return strURL;
}

#pragma mark - Start XML Parsing
- (void)startWithURL:(NSString *)strURL usingMoc:(NSManagedObjectContext *)moc forced:(BOOL)forced
{
    
    [Utils startNetworkIndicator];
    
    _foto = [NSMutableDictionary dictionary];
    [_foto setObject:[NSMutableString string] forKey:@"url"];
    [_foto setValue:[NSNumber numberWithBool:NO] forKey:@"principal"];
    
    _forceSync = forced;
    _imoveis = [NSMutableArray array];
    _refs = [NSMutableArray array];
    _retryPhotos = [NSMutableArray array];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user = [defaults valueForKey:USUARIO_ARCO];
    NSString *password = [defaults valueForKey:SENHA_ARCO];
    
    strURL = [self normalizeURL:strURL];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:user password:password];
    
    [manager GET:strURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [Utils stopNetworkIndicator];
        
        NSString *xml = [[NSString alloc] initWithData:responseObject encoding:NSISOLatin1StringEncoding];
        
        NSString *newChecksum = [xml MD5];
        
        if (!_forceSync) {
            
            // checksum check
            NSString *oldChecksum = [Utils valueFromDefaults:ARCO_CHECKSUM];
            if ([newChecksum isEqualToString:oldChecksum]) {
                
                [Utils dismissProgress];
                [Utils stopNetworkIndicator];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Aviso" message:@"As informações já estão atualizadas, deseja forçar a sincronização?" delegate:_parentController cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Forçar", nil];
                    [alertView show];
                });
                
                
                return;
            }
            
        }
        
        
        _checksum = newChecksum;
        
        _parser = [[NSXMLParser alloc] initWithData:responseObject];
        _parser.delegate = self;
        _parser.shouldResolveExternalEntities = NO;
        [_parser parse];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [Utils dismissProgress];
        [Utils showMessage:MESSAGE_IMPORT_ERROR];
        [Utils stopNetworkIndicator];
    }];
    
}

#pragma mark - NSXMLParserDelegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    _element = elementName;
    
    if([elementName isEqualToString:@"imovel"]) {
        _arco  = [[ArcoInformatica alloc] init];
    }
    
}



- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if(string.trimmed.length == 0) {
        return;
    }
    
    if([_element isEqualToString:@"id"]) {
        [_arco.identificacao appendString:string];
    } else if([_element isEqualToString:@"referencia"]) {
        [_arco.referencia appendString:string];
    } else if([_element isEqualToString:@"tipo"]) {
        [_arco.tipo appendString:string];
    } else if([_element isEqualToString:@"empreendimento"]) {
        [_arco.empreendimento appendString:string];
    } else if([_element isEqualToString:@"financiavel"]) {
        [_arco.financiavel appendString:string];
    } else if([_element isEqualToString:@"categoria"]) {
        [_arco.categoria appendString:string];
    } else if([_element isEqualToString:@"titulo"]) {
        [_arco.titulo appendString:string];
    } else if([_element isEqualToString:@"preco"]) {
        [_arco.preco appendString:string];
    } else if([_element isEqualToString:@"iptu"]) {
        [_arco.iptu appendString:string];
    } else if([_element isEqualToString:@"condominio"]) {
        [_arco.condominio appendString:string];
    } else if([_element isEqualToString:@"quartos"]) {
        [_arco.quartos appendString:string];
    } else if([_element isEqualToString:@"suites"]) {
        [_arco.suites appendString:string];
    } else if([_element isEqualToString:@"endereco"]) {
        [_arco.endereco appendString:string];
    } else if([_element isEqualToString:@"bairro"]) {
        [_arco.bairro appendString:string];
    } else if([_element isEqualToString:@"cidade"]) {
        [_arco.cidade appendString:string];
    } else if([_element isEqualToString:@"uf"]) {
        [_arco.uf appendString:string];
    } else if([_element isEqualToString:@"descricao"]) {
        [_arco.descricao appendString:string];
    } else if([_element isEqualToString:@"fotoprincipal"]) {
        [[_foto valueForKey:@"url" ] appendString:string];
        [_foto setValue:[NSNumber numberWithBool:YES] forKey:@"principal"];
    } else if([_element isEqualToString:@"foto"]) {
        [[_foto valueForKey:@"url" ] appendString:string];
    }
}



- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    
    if([elementName isEqualToString:@"fotoprincipal"]) {
        
        if(_foto != nil) {
            _totalPhotos++;
            
            [_arco.fotos addObject:_foto];
            
            _foto = [NSMutableDictionary dictionary];
            [_foto setObject:[NSMutableString string] forKey:@"url"];
            [_foto setValue:[NSNumber numberWithBool:NO] forKey:@"principal"];
        }
        
    } else if([elementName isEqualToString:@"foto"]) {
        
        if(_foto != nil) {
            _totalPhotos++;
            
            [_arco.fotos addObject:_foto];
            
            _foto = [NSMutableDictionary dictionary];
            [_foto setObject:[NSMutableString string] forKey:@"url"];
            [_foto setValue:[NSNumber numberWithBool:NO] forKey:@"principal"];
        }
        
    }
    
    else if([elementName isEqualToString:@"imovel"]) {
        [_imoveis addObject:_arco];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    
    [Utils dismissProgress];
    
    _parentController.hasStarted = YES;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        
        [localContext performBlock:^{
            
            [self convert:localContext];
            
            // check deleted objects from remote
            //[Utils checkDeleted:localContext withRefs:_refs];
            
            if ([self.imoveis count] == 0) {
                [Utils showMessage:MESSAGE_EMPTY_IMPORT];
                [Utils dismissProgress];
                return;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                _progressView.progress = 0.0f;
                _progressLabel.text = @"";
                
                // update checksum
                [Utils addObjectToDefaults:_checksum withKey:ARCO_CHECKSUM];
            });
            
            [self startBackgroundDownloadsForBaseURL:localContext];
            
            if (_retryPhotos.count > 0) {
                [self redownloadPhotos:localContext];
            }
            
            [Utils dismissProgress];
            [Utils postEntitytNotification:@"Imovel"];
            [Utils stopNetworkIndicator];
            
            if (_parentController != nil) {
                [Utils showMessage:MESSAGE_FINISH_IMPORT];
            }
            
            [_parentController setHasStarted:NO];
        }];
    });
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    NSLog(@"%@", parseError);
    
    [Utils dismissProgress];
    [Utils stopNetworkIndicator];
    
    [Utils showMessage:@"Não foi possível iniciar a importação. Verifique as informações preenchidas e tente novamente.\n Se o erro persistir entre em contato com suporte@brestate.com.br"];
}


#pragma mark - Convert Objects to NSManagedObject
- (void)convert:(NSManagedObjectContext *)moc
{
    
    NSNumberFormatter *currencyFmt = [[NSNumberFormatter alloc] init];
    [currencyFmt setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyFmt setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [currencyFmt setDecimalSeparator:@","];
    [currencyFmt setGeneratesDecimalNumbers:YES];
    [currencyFmt setCurrencySymbol:@"R$ "];
    [currencyFmt setLenient:YES];
    
    int contador = 0;
    
    __block long total = [_imoveis count];
    __block int step = 0;
    __block float progressStep = 0.0f;
    
    for(ArcoInformatica *obj in _imoveis) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/total;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Importando: %d/%ld",step, total];
        });
        
        @autoreleasepool {
            
            long count = 0;
            
            Imovel *imovel;
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"identificacao = %@",obj.referencia.trimmed];
            
            count = [Imovel MR_countOfEntitiesWithPredicate:predicate inContext:moc];
            
            BOOL exists = NO;
            if(count > 0 ) {
                imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:moc];
                exists = YES;
                imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
            } else {
                
                imovel = [Imovel MR_createInContext:moc];
                imovel.codigo = [imovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
            }
            
            if (obj.titulo.trimmed.length > 1) {
                imovel.descricao = [NSString stringWithFormat:@"%@\n\n%@",obj.titulo.trimmed,obj.descricao.trimmed];
            } else {
                imovel.descricao = obj.descricao.trimmed;
            }
            
            imovel.version = [NSDate date];
            imovel.identificacao = obj.referencia.trimmed;
            
            // observação
            NSMutableString *observacao = [NSMutableString string];
            if (obj.empreendimento.length > 1) {
                [observacao appendString:obj.empreendimento];
            }
            if ([obj.financiavel intValue] == 1) {
                [observacao appendString:[NSString stringWithFormat:@"\nAceita financiamento"]];
            } else {
                [observacao appendString:[NSString stringWithFormat:@"\nNão aceita financiamento"]];
            }
            
            // Tipo Oferta
            if([obj.tipo.trimmed containsString:@"Venda"]) {
                imovel.oferta = @"Venda";
            } else if([obj.tipo.trimmed containsString:@"Locação"]) {
                imovel.oferta = @"Locação";
            } else {
                imovel.oferta = @"Outro";
            }
            
            // valor
            NSNumber *valor = [currencyFmt numberFromString:obj.preco.trimmed];
            // valor
            if ([valor intValue] > 0) {
                imovel.valor = [NSDecimalNumber decimalNumberWithDecimal:[valor decimalValue]];
            }
            
            // condominio
            NSNumber *valorCondominio = [currencyFmt numberFromString:obj.condominio.trimmed];
            if ([valorCondominio intValue] > 0) {
                imovel.valor = [NSDecimalNumber decimalNumberWithDecimal:[valorCondominio decimalValue]];
            }
            
            // iptu
            NSNumber *valorIPTU = [currencyFmt numberFromString:obj.iptu.trimmed];
            if ([valorIPTU intValue] > 0) {
                imovel.valor = [NSDecimalNumber decimalNumberWithDecimal:[valorIPTU decimalValue]];
            }
            
            // Tipo Imovel
            if([obj.categoria.trimmed containsString:@"Apartamento"]) {
                imovel.tipo = @"Apartamento";
            } else if([obj.categoria.trimmed containsString:@"Casa"]) {
                imovel.tipo = @"Casa";
            } else if([obj.categoria.trimmed containsString:@"Terreno"]) {
                imovel.tipo = @"Terreno";
            } else if([obj.categoria.trimmed containsString:@"Kitnet"]) {
                imovel.tipo = @"JK / Kitnet";
            } else if([obj.categoria.trimmed containsString:@"Sítio"]) {
                imovel.tipo = @"Fazenda / Sítio";
            } else if([obj.categoria.trimmed containsString:@"Chácara"]) {
                imovel.tipo = @"Chácara";
            } else if([obj.categoria.trimmed containsString:@"Comercial"]) {
                imovel.tipo = @"Sala / Espaço Comercial";
            } else if([obj.categoria.trimmed containsString:@"Loja"]) {
                imovel.tipo = @"Sala / Espaço Comercial";
            } else {
                imovel.tipo = @"Outro";
            }
            
            // quartos
            NSNumber *quartos = [NSNumber numberWithLong:[obj.quartos.trimmed integerValue]];
            
            if ([quartos intValue] > 0) {
                imovel.quartos = quartos;
            }
            
            // Endereco
            Endereco *endereco;
            if (imovel.endereco != nil) {
                endereco = imovel.endereco;
            } else {
                endereco = [Endereco MR_createInContext:moc];
                endereco.codigo = [endereco getUniqueCode]; //[NSNumber numberWithLong:codigo];
            }
            
            endereco.logradouro = obj.endereco.trimmed;
            
            Cidade *cidade;
            if (obj.cidade.trimmed.length > 0) {
                
                predicate = [NSPredicate predicateWithFormat:@"nome == [cd] %@",obj.cidade.trimmed];
                cidade = [Cidade MR_findFirstWithPredicate:predicate inContext:moc];
                
                if (cidade != nil) {
                    endereco.cidade = cidade.nome;
                }
            }
            
            
            Bairro *bairro = nil;
            if (obj.bairro.trimmed.length > 1) {
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cidade == [cd] %@ AND nome == [cd] %@",cidade.nome.trimmed, obj.bairro.trimmed];
                count = [Bairro MR_countOfEntitiesWithPredicate:predicate inContext:moc];
                
                if(count == 0 ) {
                    
                    bairro = [Bairro MR_createInContext:moc];
                    bairro.codigo = [bairro getUniqueCode]; //[NSNumber numberWithLong:codigo];
                    bairro.nome = obj.bairro.trimmed;
                    bairro.cidade = cidade.nome;
                    bairro.version = [NSDate date];
                    
                } else {
                    
                    predicate = [NSPredicate predicateWithFormat:@"cidade == [cd] %@ AND nome == [cd] %@",cidade.nome, obj.bairro.trimmed];
                    bairro = [Bairro MR_findFirstWithPredicate:predicate inContext:moc];
                }
                
            }
            
            endereco.bairro = bairro;
            
            
            imovel.endereco = endereco;
            
            obj.codigoImovel = imovel.codigo;
            
            if (exists) {
                [Utils deleteAllPhotos:imovel withMoc:moc];
            }
            
            contador++;
            
            imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];            
            [moc MR_saveToPersistentStoreAndWait];
        }
    }
}


#pragma mark - Photo Download
- (void)startBackgroundDownloadsForBaseURL:(NSManagedObjectContext *)moc
{
    
    [Utils startNetworkIndicator];
    
    __block int step = 0;
    __block float progressStep = 0.0;
    
    for (ArcoInformatica *arco in _imoveis)
    {
        
        if (_parentController == nil) {
            break;
        }
        
        for(NSDictionary *foto in arco.fotos){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                step++;
                progressStep += 1.0/_totalPhotos;
                [_progressView setProgress:progressStep animated:YES];
                _progressLabel.text = [NSString stringWithFormat:@"Transferindo fotos: %d/%d",step, _totalPhotos];
                
            });
            
            
            @autoreleasepool {
                [self downloadPhoto:foto withCodigo:arco.codigoImovel usingMoc:moc];
            }
        }
    }
}

#pragma mark - Retry
- (void)redownloadPhotos:(NSManagedObjectContext *)moc
{
    __block int step = 0;
    __block float progressStep = 0.0;
    
    for(NSDictionary *foto in _retryPhotos){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/_retryPhotos.count;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Reparando fotos : %d/%lu",step, (unsigned long)_retryPhotos.count];
            
        });
        
        @autoreleasepool {
            NSNumber *codigoImovel = [NSNumber numberWithLong:[[foto objectForKey:@"codigoImovel"] integerValue]];
            [self downloadPhoto:foto withCodigo:codigoImovel usingMoc:moc];
        }
    }
}

#pragma mark - Photo Download
- (void)downloadPhoto:(NSDictionary *)foto withCodigo:(NSNumber *)codigoImovel usingMoc:(NSManagedObjectContext *)moc
{
    NSString *strURL = [[foto valueForKey:@"url"] trimmed];
    
    if (strURL.length == 0) {
        return;
    }
    
    NSURL *photoURL = [NSURL URLWithString:strURL];
    
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:photoURL];
    request.timeOutSeconds = 60;
    [request startSynchronous];
    NSError *error = [request error];
    
    if (error) {
        
        NSMutableDictionary *obj = [NSMutableDictionary dictionaryWithDictionary:foto];
        [obj setValue:codigoImovel forKey:@"codigoImovel"];
        [_retryPhotos addObject:obj];
        NSLog(@"%@",[error description]);
    }
    
    FotoImovel *fotoImovel = [FotoImovel MR_createInContext:moc];
    fotoImovel.codigo = [fotoImovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
    
    fotoImovel.descricao = [[strURL lastPathComponent] stringByDeletingPathExtension];
    fotoImovel.importacao = [NSNumber numberWithBool:YES];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",codigoImovel];
    fotoImovel.imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:moc];
    
    fotoImovel.capa = [NSNumber numberWithBool:[[foto valueForKey:@"principal"] boolValue]];
    
    [fotoImovel insertImageOnDisk:request.responseData];
    
    [moc MR_saveToPersistentStoreAndWait];
}


@end
