//
//  CRMUtils.h
//  Appci
//
//  Created by Alexandre Oliveira on 11/27/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CRMUtils : NSObject

+ (void)crmUpdateCheck;

@end
