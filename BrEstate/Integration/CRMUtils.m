//
//  CRMUtils.m
//  Appci
//
//  Created by Alexandre Oliveira on 11/27/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "CRMUtils.h"
#import "NSString+Custom.h"
#import "DateUtils.h"
#import "ASIHTTPRequest.h"


@implementation CRMUtils

#pragma mark - CRM Update Check
+ (void)crmUpdateCheck
{
    
    NSDate *date = [Utils valueFromDefaults:LAST_CRM_CHECK];
    
    NSDate *lastRun = [DateUtils dateWithoutTime:date];
    NSDate *today = [DateUtils dateWithoutTime:[NSDate date]];
    
    if ([lastRun compare:today] != NSOrderedSame || date == nil) {
        
        // CONSIR
        [self consir];
        
        // Imóvel PRO
        //      [self checkImovelPro];
        
        // SIMBO
        [self simbo];
        
        // IMOBI BRASIL
        [self imobiBrasil];
        
        // IMO
        //        [self imo];
        
        // VISTA
        [self vista];
        
        // ALOH IN
        [self alohin];
        
        // Value Gaia
        [self valueGaia];
        
        // WMB
        [self wmb];
        
        // ARCO
        [self arco];
        
        [Utils addObjectToDefaults:[NSDate date] withKey:LAST_CRM_CHECK];
    }
    
}

#pragma mark - IMO.ETC
+ (void)imo
{
    NSString *codigo = [Utils valueFromDefaults:SITE_IMO];
    
    if (!codigo) {
        return;
    }
    NSString *site = [NSString stringWithFormat:@"http://imo.etc.br/appci/%@",codigo];
    
    NSURL *url = [NSURL URLWithString:site];
    
    if (url) {
        
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request startSynchronous];
        NSError *error = [request error];
        
        if (!error) {
            
            NSString *newChecksum = [request.responseString MD5];
            NSString *oldChecksum = [Utils valueFromDefaults:IMO_CHECKSUM];
            
            if (![newChecksum isEqualToString:oldChecksum]) {
                
                UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                localNotification.soundName = UILocalNotificationDefaultSoundName;
                localNotification.alertBody = @"Existem informações atualizadas do seu crm Imo.etc!";
                [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
                
            }
            
        }
        
    }
    
}

#pragma mark - Vista
+ (void)vista
{
    NSString *email = [Utils valueFromDefaults:EMAIL_VISTA];
    NSString *documento = [Utils valueFromDefaults:DOCUMENTO_VISTA];
    
    if (!email) {
        return;
    }
    
    NSString *site = [NSString stringWithFormat:@"%@%@?email=%@&site=%@",[[Config instance] url], @"crm/query-vista.json", email,documento];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager GET:site parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSNumber *success = [responseObject valueForKey:@"success"];
        
        if ([success boolValue]) {
            
            NSString *strURL = [responseObject valueForKey:@"url"];
            
            NSURL *url = [NSURL URLWithString:strURL];
            
            if (url) {
                
                ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
                [request startSynchronous];
                NSError *error = [request error];
                
                if (!error) {
                    
                    NSString *newChecksum = [request.responseString MD5];
                    NSString *oldChecksum = [Utils valueFromDefaults:VISTA_CHECKSUM];
                    
                    if (![newChecksum isEqualToString:oldChecksum]) {
                        
                        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                        localNotification.soundName = UILocalNotificationDefaultSoundName;
                        localNotification.alertBody = @"Existem informações atualizadas do seu crm Vista!";
                        [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
                    }
                    
                }
            }
            
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [Utils dismissProgress];
            [Utils showMessage:@"Ocorreu um erro na integração. Se o erro persistir, entre em contato com nosso suporte."];
        });
    }];
    
    
}

#pragma mark - Aloh.in
+ (void)alohin
{
    NSString *site = [Utils valueFromDefaults:SITE_ALOHIN];
    
    if (!site) {
        return;
    }
    
    NSString *lastChar = [site substringFromIndex:[site length]-1];
    
    if([lastChar isEqualToString:@"/"]) {
        site = [site stringByAppendingString:@"exportar/alohin"];
    } else {
        site = [site stringByAppendingString:@"/exportar/alohin"];
    }
    
    if(![site hasPrefix:@"http"]) {
        site = [@"http://" stringByAppendingString:site];
    }
    
    NSURL *url = [NSURL URLWithString:site];
    
    if (url) {
        
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request startSynchronous];
        NSError *error = [request error];
        
        if (!error) {
            NSString *newChecksum = [request.responseString MD5];
            NSString *oldChecksum = [Utils valueFromDefaults:ALOHIN_CHECKSUM];
            
            if (![newChecksum isEqualToString:oldChecksum]) {
                
                UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                localNotification.soundName = UILocalNotificationDefaultSoundName;
                localNotification.alertBody = @"Existem informações atualizadas do seu crm Aloh.in!";
                [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
            }
        }
        
    }
}

#pragma mark - WMB
+ (void)wmb
{
    NSString *site = [Utils valueFromDefaults:SITE_WMB];
    NSString *codigo = [Utils valueFromDefaults:CODIGO_WMB];
    
    if (!site) {
        return;
    }
    
    NSString *strURL = site;
    NSString *lastChar = [strURL substringFromIndex:[strURL length]-1];
    
    if([lastChar isEqualToString:@"/"]) {
        strURL = [strURL stringByAppendingString:@"feed/zap/"];
    } else {
        strURL = [strURL stringByAppendingString:@"/feed/zap/"];
    }
    
    if(![strURL hasPrefix:@"http"]) {
        strURL = [@"http://" stringByAppendingString:strURL];
    }
    
    strURL = [strURL stringByAppendingString:[NSString stringWithFormat:@"%@.xml",codigo]];
    
    NSURL *url = [NSURL URLWithString:strURL];
    
    if (url) {
        
        NSURL *url = [NSURL URLWithString:strURL];
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request startSynchronous];
        NSError *error = [request error];
        
        if (!error) {
            
            NSString *newChecksum = [request.responseString MD5];
            NSString *oldChecksum = [Utils valueFromDefaults:WMB_CHECKSUM];
            
            if (![newChecksum isEqualToString:oldChecksum]) {
                
                UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                localNotification.soundName = UILocalNotificationDefaultSoundName;
                localNotification.alertBody = @"Existem informações atualizadas do seu crm WMB!";
                [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
                
            }
        }
    }
    
}

#pragma mark - Arco
+ (void)arco
{
    NSString *site = [Utils valueFromDefaults:SITE_ARCO];
    NSString *user = [Utils valueFromDefaults:USUARIO_ARCO];
    NSString *pass = [Utils valueFromDefaults:SENHA_ARCO];
    
    if (!site) {
        return;
    }
    
    NSString *lastChar = [site substringFromIndex:[site length]-1];
    
    if([lastChar isEqualToString:@"/"]) {
        site = [site stringByAppendingString:@"xml.php"];
    } else {
        site = [site stringByAppendingString:@"/xml.php"];
    }
    
    if(![site hasPrefix:@"http"]) {
        site = [@"http://" stringByAppendingString:site];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setAuthorizationHeaderFieldWithUsername:user password:pass];
    
    [manager GET:site parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *xml = [[NSString alloc] initWithData:responseObject encoding:NSISOLatin1StringEncoding];
        
        NSString *newChecksum = [xml MD5];
        NSString *oldChecksum = [Utils valueFromDefaults:ARCO_CHECKSUM];
        
        if (![newChecksum isEqualToString:oldChecksum]) {
            
            UILocalNotification *localNotification = [[UILocalNotification alloc] init];
            localNotification.soundName = UILocalNotificationDefaultSoundName;
            localNotification.alertBody = @"Existem informações atualizadas do seu crm Arco Informática!";
            [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
    
    
}


#pragma mark - Imobi Brasil
+ (void)imobiBrasil
{
    
    NSString *codigo = [Utils valueFromDefaults:SITE_IMOBI_BRASIL];
    
    if (!codigo) {
        return;
    }
    
    NSString *site = [NSString stringWithFormat:@"http://goo.gl/%@",codigo];
    
    NSURL *url = [NSURL URLWithString:site];
    
    if (url) {
        
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request startSynchronous];
        NSError *error = [request error];
        
        if (!error) {
            
            NSString *newChecksum = [request.responseString MD5];
            NSString *oldChecksum = [Utils valueFromDefaults:IMOBIBRASIL_CHECKSUM];
            
            if (![newChecksum isEqualToString:oldChecksum]) {
                
                UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                localNotification.soundName = UILocalNotificationDefaultSoundName;
                localNotification.alertBody = @"Existem informações atualizadas do seu crm ImobiBrasil!";
                [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
                
            }
        }
        
    }
    
    
}

#pragma mark - ValueGaia
+ (void)valueGaia
{
    
    NSString *site = [Utils valueFromDefaults:SITE_GAIA];
    
    if (site) {
        
        NSError *error = nil;
        NSString *xml = [NSString stringWithContentsOfURL:[NSURL URLWithString:site] encoding:NSISOLatin1StringEncoding error:&error];
        xml = [xml stringByReplacingOccurrencesOfString:@"<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>" withString:@""];
        
        if (!error) {
            
            NSString *newChecksum = [xml MD5];
            NSString *oldChecksum = [Utils valueFromDefaults:GAIA_CHECKSUM];
            
            if (![newChecksum isEqualToString:oldChecksum]) {
                
                UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                localNotification.soundName = UILocalNotificationDefaultSoundName;
                localNotification.alertBody = @"Existem informações atualizadas do seu crm ValueGaia!";
                [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
                
            }
            
        }
        
    }
    
}

#pragma mark - Imóvel PRO
+ (void)imovelPro
{
    
    NSString *site = [Utils valueFromDefaults:SITE_IMOVELPRO];
    
    if (site) {
        
        NSURL *url = [NSURL URLWithString:site];
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request startSynchronous];
        NSError *error = [request error];
        
        if (!error) {
            
            NSString *newChecksum = [request.responseString MD5];
            NSString *oldChecksum = [Utils valueFromDefaults:IMOVELPRO_CHECKSUM];
            
            if (![newChecksum isEqualToString:oldChecksum]) {
                
                UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                localNotification.soundName = UILocalNotificationDefaultSoundName;
                localNotification.alertBody = @"Existem informações atualizadas do seu crm Imóvel PRO!";
                [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
                
            }
            
        }
        
    }
    
}


#pragma mark - Consir
+ (void)consir
{
    
    NSString *site = [Utils valueFromDefaults:SITE_CONSIR];
    
    if (site) {
        
        // url normalize
        NSString *lastChar = [site substringFromIndex:[site length]-1];
        
        if([lastChar isEqualToString:@"/"]) {
            site = [site stringByAppendingString:@"i_adm/exportador.php"];
        } else {
            site = [site stringByAppendingString:@"/i_adm/exportador.php"];
        }
        
        if(![site hasPrefix:@"http"]) {
            site = [@"http://" stringByAppendingString:site];
        }
        
        
        NSURL *url = [NSURL URLWithString:site];
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request startSynchronous];
        NSError *error = [request error];
        
        if (!error) {
            
            NSString *newChecksum = [request.responseString MD5];
            NSString *oldChecksum = [Utils valueFromDefaults:CONSIR_CHECKSUM];
            
            if (![newChecksum isEqualToString:oldChecksum]) {
                
                UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                localNotification.soundName = UILocalNotificationDefaultSoundName;
                localNotification.alertBody = @"Existem informações atualizadas do seu crm Consir!";
                [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
            }
        }
    }
}


#pragma mark - Simbo Update Check
+ (void)simbo
{
    
    NSString *strURL = [Utils valueFromDefaults:SITE_SIMBO];
    
    NSURL *url = [NSURL URLWithString:strURL];
    if (url) {
        
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request startSynchronous];
        NSError *error = [request error];
        
        if (!error) {
            
            NSString *newChecksum = [request.responseString MD5];
            NSString *oldChecksum = [Utils valueFromDefaults:SIMBO_CHECKSUM];
            
            if (![newChecksum isEqualToString:oldChecksum]) {
                
                UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                localNotification.soundName = UILocalNotificationDefaultSoundName;
                localNotification.alertBody = @"Existem informações atualizadas do seu crm Simbo!";
                [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
                
            }
            
        }
        
        
    }
    
}

@end
