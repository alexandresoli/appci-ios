//
//  ConsirImporter.m
//  Appci
//
//  Created by BrEstate LTDA on 4/28/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "ConsirImporter.h"

#import "Imovel.h"
#import "Cliente.h"
#import "TagImovel.h"
#import "Endereco.h"
#import "Estado.h"
#import "Cidade.h"
#import "Bairro.h"
#import "FotoImovel+InsertDeleteImagem.h"
#import "NSString+HTML.h"
#import "NSString+Custom.h"
#import "ConsirViewController.h"
#import "ASIHTTPRequest.h"
#import "XMLDictionary.h"

#import "Imovel+Firebase.h"
#import "Cliente+Firebase.h"

@interface ConsirImporter ()

#pragma mark - Properties

@property (nonatomic, strong) NSString *checksum;
@property (nonatomic, strong) NSMutableArray *retryPhotos;
@property (nonatomic, strong) NSDictionary *dictionary;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSArray *imoveis;
@property (nonatomic) int totalPhotos;
@property (nonatomic) BOOL forceSync;

@end

@implementation ConsirImporter


#pragma mark - Normalize URL
- (NSString *)normalizeURL:(NSString *)strURL
{
    
    NSString *lastChar = [strURL substringFromIndex:[strURL length]-1];
    
    if([lastChar isEqualToString:@"/"]) {
        strURL = [strURL stringByAppendingString:@"i_adm/exportador.php"];
    } else {
        strURL = [strURL stringByAppendingString:@"/i_adm/exportador.php"];
    }
    
    if(![strURL hasPrefix:@"http"]) {
        strURL = [@"http://" stringByAppendingString:strURL];
    }
    
    return strURL;
}


#pragma mark - Start XML Parsing
- (void)startWithURL:(NSString *)strURL usingMoc:(NSManagedObjectContext *)moc forced:(BOOL)forced;
{
    
    [Utils startNetworkIndicator];
    
    strURL = [self normalizeURL:strURL];
    
    _forceSync  = forced;
    _retryPhotos = [NSMutableArray array];
    _photos = [NSMutableArray array];
    
    NSURL *url = [NSURL URLWithString:strURL];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    request.timeOutSeconds = 30;
    [request startSynchronous];
    NSError *error = [request error];
    
    if (error) {
        [Utils dismissProgress];
        [Utils showMessage:MESSAGE_IMPORT_ERROR];
        [Utils stopNetworkIndicator];
        return;
    }
    
    NSString *xml = request.responseString;
    NSString *newChecksum = [xml MD5];
    
    if (!_forceSync) {
        
        // checksum check
        NSString *oldChecksum = [Utils valueFromDefaults:CONSIR_CHECKSUM];
        if ([newChecksum isEqualToString:oldChecksum]) {
            
            [Utils dismissProgress];
            [Utils stopNetworkIndicator];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Aviso" message:@"As informações já estão atualizadas, deseja forçar a sincronização?" delegate:_parentController cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Forçar", nil];
                [alertView show];
            });
            
            
            return;
        }
        
    }
    
    _checksum = newChecksum;
    
    [Utils stopNetworkIndicator];
    [Utils dismissProgress];
    
    _parentController.hasStarted = YES;
    _dictionary = [NSDictionary dictionaryWithXMLData:request.responseData];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        
        [localContext performBlock:^{
            
            [self convert:localContext];
            
            if ([self.imoveis count] == 0) {
                [Utils showMessage:MESSAGE_EMPTY_IMPORT];
                [Utils dismissProgress];
                return;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                _progressView.progress = 0.0f;
                _progressLabel.text = @"";
                
            });
            
            // update checksum
            [Utils addObjectToDefaults:_checksum withKey:CONSIR_CHECKSUM];
            
            [self startBackgroundDownloadsForBaseURL:localContext];
            
            if (_retryPhotos.count > 0) {
                [self redownloadPhotos:localContext];
            }
            
            [Utils dismissProgress];
            [Utils postEntitytNotification:@"Contato"];
            [Utils postEntitytNotification:@"Imovel"];
            [Utils stopNetworkIndicator];
            
            if (_parentController != nil) {
                [Utils showMessage:MESSAGE_FINISH_IMPORT];
            }
            
            [_parentController setHasStarted:NO];
            
        }];
    });
    
}

#pragma mark - Convert Objects to NSManagedObject
- (void)convert:(NSManagedObjectContext *)moc
{
    
    NSNumberFormatter *currencyFmt = [[NSNumberFormatter alloc] init];
    [currencyFmt setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyFmt setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [currencyFmt setDecimalSeparator:@","];
    [currencyFmt setGeneratesDecimalNumbers:YES];
    [currencyFmt setCurrencySymbol:@"R$ "];
    [currencyFmt setLenient:YES];
    
    
    _imoveis = _dictionary[@"imovel"];
    
    __block long total = [_imoveis count];
    __block int step = 0;
    __block float progressStep = 0.0f;
    
    for(NSDictionary *obj in _imoveis) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/total;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Importando: %d/%ld",step, total];
        });
        
        @autoreleasepool {
            
            long count = 0;
            
            Imovel *imovel;
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"identificacao = %@",obj[@"referencia"]];
            
            count = [Imovel MR_countOfEntitiesWithPredicate:predicate inContext:moc];
            
            BOOL exists = NO;
            if(count > 0 ) {
                imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:moc];
                exists = YES;
                imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
            } else {
                
                imovel = [Imovel MR_createInContext:moc];
                imovel.codigo = [imovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
            }
            
            
            imovel.descricao = [obj[@"descricao"] stringByDecodingHTMLEntities];
            imovel.version = [NSDate date];
            imovel.identificacao = obj[@"referencia"];
            
            
            // nota
            if ([obj[@"financiamento"] isEqualToString:@"Sim"]) {
                imovel.notaPrivada = @"Aceita financiamento";
            } else {
                imovel.notaPrivada = @"Não aceita financiamento";
            }
            
            
            // Tipo Oferta
            if([obj[@"finalidade"] isEqualToString:@"Comprar"]) {
                imovel.oferta = @"Venda";
            } else if([obj[@"finalidade"] isEqualToString:@"Alugar"]) {
                imovel.oferta = @"Locação";
            } else {
                imovel.oferta = @"Outro";
            }
            
            // cliente
            NSString *proprietario = obj[@"proprietario"];
            
            if(proprietario != nil && ![proprietario isEqualToString:@" "] && ![proprietario isEqualToString:@""]) {
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"nome = %@",proprietario];
                count = [Cliente MR_countOfEntitiesWithPredicate:predicate inContext:moc];
                Cliente *cliente = nil;
                
                if(count == 0) {
                    
                    cliente = [Cliente MR_createInContext:moc];
                    cliente.codigo = [cliente getUniqueCode]; //[NSNumber numberWithLong:codigo];
                    
                    cliente.nome = proprietario;
                    
                    if([imovel.oferta.trimmed isEqualToString:@"Venda"]) {
                        cliente.subtipo = [NSNumber numberWithInt:1];
                    } else if([imovel.oferta.trimmed isEqualToString:@"Locação"]) {
                        cliente.subtipo = [NSNumber numberWithInt:2];
                    }
                    
                    cliente.version = [NSDate date];
                    
                } else {
                    
                    predicate = [NSPredicate predicateWithFormat:@"nome = %@",obj[@"proprietario"]];
                    cliente = [Cliente MR_findFirstWithPredicate:predicate inContext:moc];
                }
                
                cliente.isSavedInFirebase = [NSNumber numberWithBool:NO];
                imovel.cliente = cliente;
                
            }
            
            
            // Tipo Imovel
            if([obj[@"tipo"] isEqualToString:@"Pontos Comerciais"]) {
                imovel.tipo = @"Sala / Espaço Comercial";
            } else if([obj[@"tipo"] isEqualToString:@"Edículas"]) {
                imovel.tipo = @"Casa";
            } else if([obj[@"tipo"] isEqualToString:@"Aptos e Kitnets"]) {
                imovel.tipo = @"Apartamento";
            } else if([obj[@"tipo"] isEqualToString:@"Terrenos"]) {
                imovel.tipo = @"Terreno";
            } else if([obj[@"tipo"] isEqualToString:@"Casas"]) {
                imovel.tipo = @"Casa";
            } else if([obj[@"tipo"] isEqualToString:@"Chácaras e Sítios"]) {
                imovel.tipo = @"Chácara";
            } else if([obj[@"tipo"] isEqualToString:@"Barracões"]) {
                imovel.tipo = @"Armazém / Galpão";
            } else if([obj[@"tipo"] isEqualToString:@"Fazendas"]) {
                imovel.tipo = @"Fazenda / Sítio";
            } else {
                imovel.tipo = @"Outro";
            }
            
            
            // valor
            NSNumber *valor = [currencyFmt numberFromString:obj[@"valor"]];
            
            if ([valor intValue] > 0) {
                imovel.valor = [NSDecimalNumber decimalNumberWithDecimal:[valor decimalValue]];
            }
            
            
            // condominio
            valor = [currencyFmt numberFromString:obj[@"valor_condominio"]];
            
            if ([valor intValue] > 0) {
                imovel.condominio = [NSDecimalNumber decimalNumberWithDecimal:[valor decimalValue]];
            }
            
            
            // iptu
            valor = [currencyFmt numberFromString:obj[@"valor_iptu"]];
            
            if ([valor intValue] > 0) {
                imovel.iptu = [NSDecimalNumber decimalNumberWithDecimal:[valor decimalValue]];
            }
            
            
            // area util / total
            
            NSNumber *areaTotal = [NSNumber numberWithLong:[obj[@"area_total"] integerValue]];
            
            if ([areaTotal intValue] > 0) {
                imovel.areaTotal =  areaTotal;
            }
            
            NSNumber *areaUtil = [NSNumber numberWithLong:[obj[@"area_util"] integerValue]];
            
            if ([areaUtil intValue] > 0) {
                imovel.areaUtil =  areaUtil;
            }
            
            // quartos
            
            NSNumber *quartos = [NSNumber numberWithLong:[obj[@"quartos"] integerValue]];
            
            if ([quartos intValue] > 0) {
                imovel.quartos = quartos;
            }
            
            // tags
            
            TagImovel *tagImovel;
            
            if (imovel.tags != nil) {
                
                tagImovel = imovel.tags;
                
            } else {
                
                tagImovel = [TagImovel MR_createInContext:moc];
                tagImovel.codigo = [tagImovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
                
            }
            
            if([obj[@"garagem"] intValue] > 0) {
                tagImovel.garagem = [NSNumber numberWithBool:YES];
            } else {
                tagImovel.garagem = [NSNumber numberWithBool:NO];
            }
            
            imovel.tags = tagImovel;
            
            
            // Endereco
            Endereco *endereco;
            if (imovel.endereco != nil) {
                endereco = imovel.endereco;
            } else {
                
                endereco = [Endereco MR_createInContext:moc];
                endereco.codigo = [endereco getUniqueCode]; //[NSNumber numberWithLong:codigo];
            }
            
            
            if ([obj[@"estado"] length] > 0) {
                
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sigla == [cd] %@",obj[@"estado"]];
                Estado *estado = [Estado MR_findFirstWithPredicate:predicate inContext:moc];
                
                if (estado != nil) {
                    endereco.estado = estado.nome;
                }
                
            }
            
            
            Cidade *cidade;
            if ([obj[@"cidade"] length] > 0) {
                
                predicate = [NSPredicate predicateWithFormat:@"nome == [cd] %@",obj[@"cidade"]];
                cidade = [Cidade MR_findFirstWithPredicate:predicate inContext:moc];
                
                if (cidade != nil) {
                    endereco.cidade = cidade.nome;
                }
            }
            
            
            
            if ([obj[@"bairro"] length] > 0) {
                
                predicate = [NSPredicate predicateWithFormat:@"cidade == [cd] %@ AND nome == [cd] %@",cidade.nome.trimmed, obj[@"bairro"]];
                count = [Bairro MR_countOfEntitiesWithPredicate:predicate inContext:moc];
                
                Bairro *bairro = nil;
                
                if(count == 0 ) {
                    
                    bairro = [Bairro MR_createInContext:moc];
                    bairro.codigo = [bairro getUniqueCode]; //[NSNumber numberWithLong:codigo];
                    bairro.nome = obj[@"bairro"];
                    bairro.cidade = cidade.nome;
                    bairro.version = [NSDate date];
                    
                } else {
                    
                    predicate = [NSPredicate predicateWithFormat:@"cidade == [cd] %@ AND nome == [cd] %@",cidade.nome, obj[@"bairro"]];
                    bairro = [Bairro MR_findFirstWithPredicate:predicate inContext:moc];
                }
                
                endereco.bairro = bairro;
                
            }
            
            
            imovel.endereco = endereco;
            
            // fotos
            if (exists) {
                [Utils deleteAllPhotos:imovel withMoc:moc];
            }
            
            // foto de capa
            NSMutableDictionary *fotoPrincipal = [NSMutableDictionary dictionary];
            [fotoPrincipal setValue:obj[@"foto_principal"] forKey:@"url"];
            [fotoPrincipal setValue:@"YES" forKey:@"capa"];
            [fotoPrincipal setValue:imovel.codigo forKey:@"codigoImovel"];
            [_photos addObject:fotoPrincipal];
            _totalPhotos++;
            
            // outras fotos
            NSDictionary *photos = [[obj objectForKey:@"fotos"] objectForKey:@"foto"];
            
            if ([photos isKindOfClass:[NSString class]]) {
                NSMutableDictionary *photo = [NSMutableDictionary dictionary];
                [photo setValue:imovel.codigo forKey:@"codigoImovel"];
                [photo setValue:photos forKey:@"url"];
                [_photos addObject:photo];
                _totalPhotos++;
                continue;
                
            }
            
            for (NSString *strURL in photos) {
                NSMutableDictionary *photo = [NSMutableDictionary dictionary];
                [photo setValue:imovel.codigo forKey:@"codigoImovel"];
                [photo setValue:strURL.trimmed forKey:@"url"];
                [_photos addObject:photo];
                _totalPhotos++;
            }
            
            imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
            
            [moc MR_saveToPersistentStoreAndWait];
        }
        
    }
}


#pragma mark - Photo Download
- (void)startBackgroundDownloadsForBaseURL:(NSManagedObjectContext *)moc
{
    [Utils startNetworkIndicator];
    
    __block int step = 0;
    __block float progressStep = 0.0;
    
    for (NSDictionary *photo in _photos)
    {
        
        if (_parentController == nil) {
            break;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/_totalPhotos;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Transferindo fotos: %d/%d",step, _totalPhotos];
        });
        
        @autoreleasepool {
            [self downloadPhoto:photo usingMoc:moc];
        }
    }
}


- (void)redownloadPhotos:(NSManagedObjectContext *)moc
{
    __block int step = 0;
    __block float progressStep = 0.0;
    
    for(NSDictionary *photo in _retryPhotos){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/_retryPhotos.count;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Reparando fotos : %d/%lu",step, (unsigned long)_retryPhotos.count];
            
        });
        
        @autoreleasepool {
            [self downloadPhoto:photo usingMoc:moc];
        }
    }
}

- (void)downloadPhoto:(NSDictionary *)photo usingMoc:(NSManagedObjectContext *)moc
{
    
    NSString *strURL = [photo[@"url"] trimmed];
    
    if (strURL.length == 0) {
        return;
    }
    
    NSURL *photoURL = [NSURL URLWithString:strURL];
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:photoURL];
    request.timeOutSeconds = 60;
    [request startSynchronous];
    NSError *error = [request error];
    
    if (error) {
        
        [_retryPhotos addObject:photo];
        NSLog(@"%@",[error description]);
    }
    
    FotoImovel *fotoImovel = [FotoImovel MR_createInContext:moc];
    fotoImovel.codigo = [fotoImovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
    
    fotoImovel.descricao = [[strURL lastPathComponent] stringByDeletingPathExtension];
    fotoImovel.importacao = [NSNumber numberWithBool:YES];
    
    NSNumber *codigoImovel = photo[@"codigoImovel"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",codigoImovel];
    fotoImovel.imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:moc];
    
    fotoImovel.capa = [NSNumber numberWithBool:[photo[@"capa"] boolValue]];
    
    [fotoImovel insertImageOnDisk:request.responseData];
    
    fotoImovel.pendingUpload = [NSNumber numberWithBool:YES];
    [moc MR_saveToPersistentStoreAndWait];
}


@end
