//
//  GaiaImporter.m
//  Appci
//
//  Created by BrEstate LTDA on 4/28/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "GaiaImporter.h"
#import "Imovel.h"
#import "TagImovel.h"
#import "Endereco.h"
#import "Estado.h"
#import "Cidade.h"
#import "Bairro.h"
#import "FotoImovel+InsertDeleteImagem.h"
#import "SVProgressHUD.h"
#import "Utils.h"
#import "CoreDataUtils.h"
#import "NSString+Custom.h"
#import "DateUtils.h"
#import "GaiaViewController.h"
#import "ASIHTTPRequest.h"
#import "XMLDictionary.h"
\
@interface GaiaImporter ()

#pragma mark - Properties

@property (nonatomic, strong) NSString *checksum;
@property (nonatomic, strong) NSMutableArray *retryPhotos;
@property (nonatomic, strong) NSDictionary *dictionary;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSMutableArray *imoveis;
@property (nonatomic) int totalPhotos;
@property (nonatomic) BOOL forceSync;


@end

@implementation GaiaImporter

#pragma mark - Start XML Parsing
- (void)startWithURL:(NSString *)strURL usingMoc:(NSManagedObjectContext *)moc forced:(BOOL)forced
{
    
    [Utils startNetworkIndicator];
    
    _forceSync = forced;
    _retryPhotos = [NSMutableArray array];
    _photos = [NSMutableArray array];
    
    
    NSError *error = nil;
    NSURL *url = [NSURL URLWithString:strURL.trimmed] ;
    NSString *xml = [NSString stringWithContentsOfURL:url encoding:NSISOLatin1StringEncoding error:&error];
    xml = [xml stringByReplacingOccurrencesOfString:@"<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>" withString:@""];
    
    if (error != nil) {
        [Utils dismissProgress];
        [Utils showMessage:MESSAGE_IMPORT_ERROR];\
        [Utils stopNetworkIndicator];
        return;
    }
    
    NSString *newChecksum = [xml MD5];
    
    if (!_forceSync) {
        
        // checksum check
        NSString *oldChecksum = [Utils valueFromDefaults:GAIA_CHECKSUM];
        if ([newChecksum isEqualToString:oldChecksum]) {
            
            [Utils dismissProgress];
            [Utils stopNetworkIndicator];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Aviso" message:@"As informações já estão atualizadas, deseja forçar a sincronização?" delegate:_parentController cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Forçar", nil];
                [alertView show];
            });
            
            
            return;
        }
        
    }
    
    _checksum = newChecksum;
    
    [Utils stopNetworkIndicator];
    [Utils dismissProgress];
    
    _parentController.hasStarted = YES;
    NSData *data = [xml dataUsingEncoding:NSISOLatin1StringEncoding allowLossyConversion:YES];
    _dictionary = [NSDictionary dictionaryWithXMLData:data];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        
        [localContext performBlock:^{
            [self convert:localContext];
            
            if ([self.imoveis count] == 0) {
                [Utils showMessage:MESSAGE_EMPTY_IMPORT];
                [Utils dismissProgress];
                return;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                _progressView.progress = 0.0f;
                _progressLabel.text = @"";
                
            });
            
            // update checksum
            [Utils addObjectToDefaults:_checksum withKey:GAIA_CHECKSUM];
            
            [self startBackgroundDownloadsForBaseURL:localContext];
            
            if (_retryPhotos.count > 0) {
                [self redownloadPhotos:localContext];
            }
            
            [Utils dismissProgress];
            [Utils postEntitytNotification:@"Imovel"];
            [Utils stopNetworkIndicator];
            
            if (_parentController != nil) {
                [Utils showMessage:MESSAGE_FINISH_IMPORT];
            }
            
            [_parentController setHasStarted:NO];
            
        }];
        
    });
    
    
}

#pragma mark - Convert Objects to NSManagedObject

- (void)convert:(NSManagedObjectContext *)moc
{
    
    NSNumberFormatter *currencyFmt = [[NSNumberFormatter alloc] init];
    [currencyFmt setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"]];
    [currencyFmt setDecimalSeparator:@"."];
    [currencyFmt setGeneratesDecimalNumbers:YES];
    [currencyFmt setCurrencySymbol:@"R$ "];
    [currencyFmt setLenient:YES];
    
    _imoveis = [[_dictionary objectForKey:@"Imoveis"] objectForKey:@"Imovel"];
    
    __block long total = [_imoveis count];
    __block int step = 0;
    __block float progressStep = 0.0f;
    
    
    for(NSDictionary *obj in _imoveis) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/total;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Importando: %d/%ld",step, total];
        });
        
        @autoreleasepool {
            
            long count = 0;
            
            Imovel *imovel = nil;
            
            NSString *identificacao = obj[@"CodigoImovel"];
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"identificacao = %@", identificacao];
            
            count = [Imovel MR_countOfEntitiesWithPredicate:predicate inContext:moc];
            
            BOOL exists = NO;
            if(count > 0 ) {
                imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:moc];
                exists = YES;
                imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
            } else {
                imovel = [Imovel MR_createInContext:moc];
                imovel.codigo = [imovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
                
            }
            
            // only continues if date if different
            NSDate *lastUpdate = [DateUtils dateFromString:obj[@"DataAtualizacao"] withFormat:@"MM-dd-yyyy HH:mm:ss a"];;
            
            if ([imovel.lastUpdate isEqualToDate:lastUpdate] && !_forceSync) {
                imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
                continue;
            }
            
            imovel.lastUpdate = lastUpdate;
            
            imovel.version = [NSDate date];
            imovel.descricao = obj[@"Observacao"];
            imovel.identificacao = identificacao;
            
            if ([obj[@"AceitaFinanciamento"] boolValue]) {
                imovel.notaPrivada = [imovel.notaPrivada stringByAppendingString:[NSString stringWithFormat:@"\n Aceita Financiamento"]];
            }
            
            if ([obj[@"AceitaNegociacao"] boolValue]) {
                imovel.notaPrivada = [imovel.notaPrivada stringByAppendingString:[NSString stringWithFormat:@"\n Aceita Negociação"]];
            }
            
            if ([obj[@"AceitaPermuta"] boolValue]) {
                imovel.notaPrivada = [imovel.notaPrivada stringByAppendingString:[NSString stringWithFormat:@"\n Aceita Permuta"]];
            }
            
            
            // Tipo Oferta
            NSNumber *valor;
            if([obj[@"PrecoVenda"] length] > 0) {
                imovel.oferta = @"Venda";
                valor = [currencyFmt numberFromString:obj[@"PrecoVenda"]];
            } else if([obj[@"PrecoLocacao"] length] > 0) {
                imovel.oferta = @"Locação";
                valor = [currencyFmt numberFromString:obj[@"PrecoLocacao"]];
            } else if([obj[@"PrecoLocacaoTemporada"] length] > 0) {
                imovel.oferta = @"Temporada";
                valor = [currencyFmt numberFromString:obj[@"PrecoLocacaoTemporada"]];
            } else {
                imovel.oferta = @"Outro";
            }
            
            if ([valor intValue] > 0) {
                imovel.valor = [NSDecimalNumber decimalNumberWithDecimal:[valor decimalValue]];
            }
            
            NSNumber *condominio = [currencyFmt numberFromString:obj[@"PrecoCondominio"]];
            imovel.condominio = [NSDecimalNumber decimalNumberWithDecimal:[condominio decimalValue]];
            
            
            
            // Tipo Imovel
            NSString *subtipo = obj[@"SubTipoImovel"];
            if([subtipo isEqualToString:@"Loft"] || [subtipo isEqualToString:@"Apartamento Padrão"] || [subtipo isEqualToString:@"Apartamento Residencial"]) {
                imovel.tipo = @"Apartamento";
            } else if([subtipo isEqualToString:@"Kitchenette/Conjugados"]) {
                imovel.tipo = @"JK / Kitnet";
            } else if([subtipo isEqualToString:@"Casa de Condomínio"] ||
                      [subtipo isEqualToString:@"Casa de Vila"] ||
                      [subtipo isEqualToString:@"Casa Residencial"] ||
                      [subtipo isEqualToString:@"Casa Padrão"]) {
                imovel.tipo = @"Casa";
            } else if([subtipo isEqualToString:@"Terreno Padrão"] ||
                      [subtipo isEqualToString:@"Loteamento/Condomínio"]){
                imovel.tipo = @"Terreno";
            } else if([subtipo isEqualToString:@"Chácara"] ||
                      [subtipo isEqualToString:@"Sítio"] ||
                      [subtipo isEqualToString:@"Fazenda"] ||
                      [subtipo isEqualToString:@"Haras"]){
                imovel.tipo = @"Fazenda / Sítio";
            } else if([subtipo isEqualToString:@"Flat"]) {
                imovel.tipo = @"Flat";
            } else if([subtipo isEqualToString:@"Box/Garagem"] ||
                      [subtipo isEqualToString:@"Prédio Inteiro"] ||
                      [subtipo isEqualToString:@"Casa Comercial"] ||
                      [subtipo isEqualToString:@"Loja de Shopping/Centro Comercial"] ||
                      [subtipo isEqualToString:@"Loja/Salão"] ||
                      [subtipo isEqualToString:@"Galpão/Depósito/Armazém"] ||
                      [subtipo isEqualToString:@"Studio"] ||
                      [subtipo isEqualToString:@"Hotel Motel"] ||
                      [subtipo isEqualToString:@"Pousada/Chalé"] ||
                      [subtipo isEqualToString:@"Indústria"]){
                imovel.tipo = @"Sala / Espaço Comercial";
            } else {
                imovel.tipo = @"Outro";
            }
            
            // area
            imovel.areaUtil = [NSNumber numberWithLong:[obj[@"AreaUtil"] integerValue]];
            imovel.areaTotal = [NSNumber numberWithLong:[obj[@"AreaTotal"] integerValue]];
            
            // quartos
            imovel.quartos = [NSNumber numberWithLong:[obj[@"QtdDormitorios"] integerValue]];
            
            
            // tags
            
            TagImovel *tagImovel;
            
            if (imovel.tags != nil) {
                tagImovel = imovel.tags;
            } else {
                tagImovel = [TagImovel MR_createInContext:moc];
                tagImovel.codigo = [tagImovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
            }
            
            if([obj[@"QtdVagas"] intValue] > 0) {
                tagImovel.garagem = [NSNumber numberWithBool:YES];
            } else {
                tagImovel.garagem = [NSNumber numberWithBool:NO];
            }
            
            if ([obj[@"Piscina"] intValue] == 1) {
                tagImovel.piscina = [NSNumber numberWithBool:YES];
            }
            
            if ([obj[@"ArmarioEmbutido"] intValue] == 1) {
                tagImovel.armarioEmbutido = [NSNumber numberWithBool:YES];
            }
            
            if ([obj[@"ArmarioCozinha"] intValue] == 1) {
                tagImovel.armarioCozinha = [NSNumber numberWithBool:YES];
            }
            
            if ([obj[@"Churrasqueira"] intValue] == 1) {
                tagImovel.churrasqueira = [NSNumber numberWithBool:YES];
            }
            
            if ([obj[@"Sauna"] intValue] == 1) {
                tagImovel.sauna = [NSNumber numberWithBool:YES];
            }
            
            
            if ([obj[@"WCEmpregada"] intValue] == 1) {
                tagImovel.wcEmpregada = [NSNumber numberWithBool:YES];
            }
            
            
            if ([obj[@"QuadraPoliEsportiva"] intValue] == 1) {
                tagImovel.quadraPoliesportiva = [NSNumber numberWithBool:YES];
            }
            
            
            if ([obj[@"SalaGinastica"] intValue] == 1) {
                tagImovel.salaGinastica = [NSNumber numberWithBool:YES];
            }
            
            if ([obj[@"SalaoFestas"] intValue] == 1) {
                tagImovel.salaoFestas = [NSNumber numberWithBool:YES];
            }
            
            if ([obj[@"Varanda"] intValue] == 1) {
                tagImovel.varanda = [NSNumber numberWithBool:YES];
            }
            
            if ([obj[@"ChildrenCare"] intValue] == 1) {
                tagImovel.childrenCare = [NSNumber numberWithBool:YES];
            }
            
            if ([obj[@"SalaoJogos"] intValue] == 1) {
                tagImovel.salaoJogos = [NSNumber numberWithBool:YES];
            }
            
            
            imovel.tags = tagImovel;
            
            
            
            // Endereco
            Endereco *endereco;
            if (imovel.endereco != nil) {
                endereco = imovel.endereco;
            } else {
                endereco = [Endereco MR_createInContext:moc];
                endereco.codigo = [endereco getUniqueCode]; //[NSNumber numberWithLong:codigo];
            }
            
            
            double latitude = [obj[@"latitude"] doubleValue];
            double longitude = [obj[@"longitude"] doubleValue];
            
            endereco.latitude = [NSNumber numberWithDouble:latitude];
            endereco.longitude = [NSNumber numberWithDouble:longitude];
            
            endereco.logradouro = obj[@"Endereco"];
            
            // logradouro
            if ([obj[@"Numero"] length] > 0) {
                NSString *number = [NSString stringWithFormat:@", %@",obj[@"Numero"]];
                endereco.logradouro = [endereco.logradouro stringByAppendingString:number];
            }
            
            // complemento
            if ([obj[@"ComplementoEndereco"] length] > 1) {
                endereco.complemento = obj[@"ComplementoEndereco"];
            }
            
            if ([obj[@"PontoReferenciaEndereco"] length] > 0) {
                endereco.complemento = [endereco.complemento stringByAppendingString:[NSString stringWithFormat:@" %@ ", obj[@"PontoReferenciaEndereco"]]];
            }
            
            // cep
            if ([obj[@"CEP"] length] > 0) {
                endereco.cep = [NSNumber numberWithLong:[obj[@"CEP"] integerValue]];
            }
            
            // Estado
            
            if ([obj[@"Estado"] length] > 0) {
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sigla == [cd] %@",obj[@"Estado"]];
                Estado *estado = [Estado MR_findFirstWithPredicate:predicate inContext:moc];
                endereco.estado = estado.nome;
            }
            
            
            // Cidade
            Cidade *cidade;
            if ([obj[@"Cidade"] length] > 0) {
                
                predicate = [NSPredicate predicateWithFormat:@"nome == [cd] %@",obj[@"Cidade"]];
                cidade = [Cidade MR_findFirstWithPredicate:predicate inContext:moc];
                endereco.cidade = cidade.nome;
            }
            
            
            // Bairro
            if ([obj[@"Bairro"] length] > 0) {
                
                predicate = [NSPredicate predicateWithFormat:@"cidade == [cd] %@ AND nome == [cd] %@",cidade.nome, obj[@"Bairro"]];
                count = [Bairro MR_countOfEntitiesWithPredicate:predicate inContext:moc];
                Bairro *bairro = nil;
                
                if(count == 0 ) {
                    
                    bairro = [Bairro MR_createInContext:moc];
                    bairro.codigo = [bairro getUniqueCode]; //[NSNumber numberWithLong:codigo];
                    bairro.nome = obj[@"Bairro"];
                    bairro.cidade = cidade.nome;
                    bairro.version = [NSDate date];
                    
                } else {
                    
                    predicate = [NSPredicate predicateWithFormat:@"cidade == [cd] %@ AND nome == [cd] %@",cidade.nome, obj[@"Bairro"]];
                    bairro = [Bairro MR_findFirstWithPredicate:predicate inContext:moc];            }
                
                endereco.bairro = bairro;
            }
            
            imovel.endereco = endereco;
        
            // fotos
            if (exists) {
                [Utils deleteAllPhotos:imovel withMoc:moc];
            }
        
            NSDictionary *fotos = [[obj objectForKey:@"Fotos"] objectForKey:@"Foto"];
            NSMutableDictionary *newMedia;
            
            for (NSDictionary *photo in fotos) {
                
                if (![photo isKindOfClass:[NSDictionary class]]) {
                    
                    newMedia = [NSMutableDictionary dictionary];
                    [newMedia setValue:fotos[@"URLArquivo"] forKey:@"URLArquivo"];
                    [newMedia setValue:imovel.codigo forKey:@"codigoImovel"];
                    [newMedia setValue:[NSNumber numberWithInt:1] forKey:@"Principal"];
                    
                    [_photos addObject:newMedia];
                    _totalPhotos++;
                    break;
                    
                }
                    
                newMedia = [NSMutableDictionary dictionaryWithDictionary:photo];
                [newMedia setValue:imovel.codigo forKey:@"codigoImovel"];
                
                [_photos addObject:newMedia];
                _totalPhotos++;
                
            }
            
            imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
            [moc MR_saveToPersistentStoreAndWait];
        }
        
    }
}

#pragma mark - Photo Download
- (void)startBackgroundDownloadsForBaseURL:(NSManagedObjectContext *)moc
{
    [Utils startNetworkIndicator];
    
    __block int step = 0;
    __block float progressStep = 0.0;
    
    for (NSDictionary *photo in _photos)
    {
        
        if (_parentController == nil) {
            break;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/_totalPhotos;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Transferindo fotos: %d/%d",step, _totalPhotos];
        });
        
        @autoreleasepool {
            [self downloadPhoto:photo usingMoc:moc];
        }
    }
}

- (void)redownloadPhotos:(NSManagedObjectContext *)moc
{
    __block int step = 0;
    __block float progressStep = 0.0;
    
    for(NSDictionary *photo in _retryPhotos){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/_retryPhotos.count;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Reparando fotos : %d/%lu",step, (unsigned long)_retryPhotos.count];
            
        });
        
        @autoreleasepool {
            [self downloadPhoto:photo usingMoc:moc];
        }
    }
}

- (void)downloadPhoto:(NSDictionary *)photo usingMoc:(NSManagedObjectContext *)moc
{
    
    NSString *strURL = [photo[@"URLArquivo"] trimmed];
    
    if (strURL.length == 0) {
        return;
    }
    
    NSURL *photoURL = [NSURL URLWithString:strURL];
    
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:photoURL];
    request.timeOutSeconds = 60;
    [request startSynchronous];
    NSError *error = [request error];
    
    if (error) {
        
        [_retryPhotos addObject:photo];
        NSLog(@"%@",[error description]);
    }
    
    FotoImovel *fotoImovel = [FotoImovel MR_createInContext:moc];
    fotoImovel.codigo = [fotoImovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
    
    fotoImovel.descricao = [[strURL lastPathComponent] stringByDeletingPathExtension];
    fotoImovel.importacao = [NSNumber numberWithBool:YES];
    
    NSNumber *codigoImovel = photo[@"codigoImovel"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",codigoImovel];
    fotoImovel.imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:moc];
    fotoImovel.capa = [NSNumber numberWithLong:[[photo valueForKey:@"Principal"] integerValue]];
    
    [fotoImovel insertImageOnDisk:request.responseData];
    
    [moc MR_saveToPersistentStoreAndWait];
}

@end
