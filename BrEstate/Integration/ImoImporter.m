//
//  ImoImporter.m
//  Appci
//
//  Created by BrEstate LTDA on 4/28/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "ImoImporter.h"
#import "Imovel.h"
#import "Cliente.h"
#import "TagImovel.h"
#import "Endereco.h"
#import "Estado.h"
#import "Cidade.h"
#import "Bairro.h"
#import "Negociacao.h"
#import "Acompanhamento.h"
#import "FotoImovel+InsertDeleteImagem.h"
#import "NSString+HTML.h"
#import "NSString+Custom.h"
#import "DateUtils.h"
#import "ImoViewController.h"
#import "ASIHTTPRequest.h"
#import "XMLDictionary.h"
#import "Imovel+Firebase.h"

@interface ImoImporter ()

#pragma mark - Properties

@property (nonatomic, strong) NSString *checksum;
@property (nonatomic, strong) NSMutableArray *retryPhotos;
@property (nonatomic, strong) NSDictionary *dictionary;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSMutableArray *imoveis;
@property (nonatomic) int totalPhotos;
@property (nonatomic) BOOL forceSync;


@end

@implementation ImoImporter


#pragma mark - Start XML Parsing
- (void)startWithURL:(NSString *)strURL usingMoc:(NSManagedObjectContext *)moc forced:(BOOL)forced
{
    
    [Utils startNetworkIndicator];
    
    _forceSync  = forced;
    _imoveis = [NSMutableArray array];
    _retryPhotos = [NSMutableArray array];
    _photos  = [NSMutableArray array];
    
    NSURL *url = [NSURL URLWithString:strURL];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    request.timeOutSeconds = 30;
    [request startSynchronous];
    NSError *error = [request error];
    
    if (error) {
        [Utils dismissProgress];
        [Utils showMessage:MESSAGE_IMPORT_ERROR];
        [Utils stopNetworkIndicator];
        return;
    }
    
    NSString *xml = request.responseString;
    NSString *newChecksum = [xml MD5];
    
    if (!_forceSync) {
        
        // checksum check
        NSString *oldChecksum = [Utils valueFromDefaults:IMO_CHECKSUM];
        if ([newChecksum isEqualToString:oldChecksum]) {
            
            [Utils dismissProgress];
            [Utils stopNetworkIndicator];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Aviso" message:@"As informações já estão atualizadas, deseja forçar a sincronização?" delegate:_parentController cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Forçar", nil];
                [alertView show];
            });
            
            
            return;
        }
        
    }
    
    _checksum = newChecksum;
    
    [Utils stopNetworkIndicator];
    [Utils dismissProgress];
    
    _parentController.hasStarted = YES;
    _dictionary = [NSDictionary dictionaryWithXMLData:request.responseData];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        
        [localContext performBlock:^{
            
            [self convert:localContext];
            
            if ([self.imoveis count] == 0) {
                [Utils showMessage:MESSAGE_EMPTY_IMPORT];
                [Utils dismissProgress];
                return;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                _progressView.progress = 0.0f;
                _progressLabel.text = @"";
                
            });
            
            // update checksum
            [Utils addObjectToDefaults:_checksum withKey:IMO_CHECKSUM];
            
            
            [self startBackgroundDownloadsForBaseURL:localContext];
            
            if (_retryPhotos.count > 0) {
                [self redownloadPhotos:localContext];
            }
            
            [Utils dismissProgress];
            [Utils postEntitytNotification:@"Imovel"];
            [Utils postEntitytNotification:@"Contato"];
            [Utils stopNetworkIndicator];
            
            if (_parentController != nil) {
                [Utils showMessage:MESSAGE_FINISH_IMPORT];
            }
            
            [_parentController setHasStarted:NO];
            
        }];
        
    });
    
}


#pragma mark - Convert Objects to NSManagedObject
- (void)createNegociacao:(NSDictionary *)objNegociacao imovel:(Imovel *)imovel moc:(NSManagedObjectContext *)moc
{
    long count = 0;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@ AND imovel = %@",objNegociacao[@"negCodigo"], imovel];
    count = [Negociacao MR_countOfEntitiesWithPredicate:predicate inContext:moc];
    
    Negociacao *negociacao = nil;
    
    if (count > 0) {
        negociacao = [Negociacao MR_findFirstWithPredicate:predicate inContext:moc];
    } else {
        
        negociacao  = [Negociacao MR_createInContext:moc];
        negociacao.codigo = [NSNumber numberWithInteger:[objNegociacao[@"negCodigo"] integerValue]];
    }
    
    int codStatus = [objNegociacao[@"negStatus"] intValue];
    NSString *status = nil;
    
    if (codStatus == 1) {
        status = @"Proposta";
    } else if(codStatus == 2) {
        status = @"Análise";
    } else if(codStatus == 3) {
        status = @"Fechada";
    } else {
        status = @"Cancelada";
    }
    
    negociacao.status = status;
    
    negociacao.parceria = [NSNumber numberWithBool:[objNegociacao[@"negParceria"] boolValue]];
    negociacao.version = [DateUtils dateFromString:objNegociacao[@"negVersion"] withFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    // CLIENTE
    NSDictionary *negCliente = objNegociacao[@"negCliente"];
    
    if (negCliente != nil) {
        
        predicate = [NSPredicate predicateWithFormat:@"nome = %@ AND telefone = %@", negCliente[@"cliNome"], negCliente[@"cliTels"]];
        Cliente *cliente = [Cliente MR_findFirstWithPredicate:predicate inContext:moc];
        
        if (cliente == nil) {
            cliente = [Cliente MR_createInContext:moc];
            cliente.codigo = [NSNumber numberWithInteger:[negCliente[@"cliCodigo"] integerValue]];
            [self addAcompanhamento:cliente imovel:imovel data:negociacao.version moc:moc];
        }
        
        cliente.nome = negCliente[@"cliNome"];
        cliente.telefone = negCliente[@"cliTels"];
        cliente.email = negCliente[@"cliEmail"];
        cliente.subtipo = [NSNumber numberWithInteger:[negCliente[@"propSubtipo"] integerValue]];
        
        negociacao.imovel = imovel;
        
        cliente.isSavedInFirebase = [NSNumber numberWithBool:NO];
        
        negociacao.isSavedInFirebase = [NSNumber numberWithBool:NO];
        negociacao.cliente = cliente;
        
    }
    
}

- (void)addAcompanhamento:(Cliente *)cliente imovel:(Imovel *)imovel data:(NSDate *)date moc:(NSManagedObjectContext *)moc
{
    Acompanhamento *acompanhamento = [Acompanhamento MR_createInContext:moc];
    acompanhamento.codigo = [acompanhamento getUniqueCode]; //[NSNumber numberWithLong:codigo];
    acompanhamento.timeStamp = date;
    acompanhamento.titulo = [NSString stringWithFormat:@"Iniciada negociação do imóvel %@",imovel.identificacao];
    acompanhamento.cliente = cliente;
    acompanhamento.imovel = imovel;
}

- (void)convert:(NSManagedObjectContext *)moc
{
    
    NSNumberFormatter *currencyFmt = [[NSNumberFormatter alloc] init];
    [currencyFmt setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyFmt setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [currencyFmt setDecimalSeparator:@","];
    [currencyFmt setGeneratesDecimalNumbers:YES];
    [currencyFmt setCurrencySymbol:@"R$ "];
    [currencyFmt setLenient:YES];
    
    
    _imoveis = [_dictionary objectForKey:@"imovel"];
    
    __block long total = [_imoveis count];
    __block int step = 0;
    __block float progressStep = 0.0f;
    
    for(NSDictionary *obj in _imoveis) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/total;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Importando: %d/%ld",step, total];
        });
        
        @autoreleasepool {
            
            long count = 0;
            
            Imovel *imovel = nil;
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"identificacao = %@",obj[@"ref"]];
            
            count = [Imovel MR_countOfEntitiesWithPredicate:predicate inContext:moc];
            
            BOOL exists = NO;
            if(count > 0 ) {
                imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:moc];
                exists = YES;
                imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
            } else if(count == 0 && ![obj[@"exclusao"] boolValue]) {
                
                imovel = [Imovel MR_createInContext:moc];
                imovel.codigo = [imovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
            }
            
            // delete flagged
            BOOL shouldDelete = [obj[@"exclusao"] boolValue];
            
            if (shouldDelete) {
                
                if (imovel == nil) {
                    continue;
                }
                
                for (FotoImovel *fotoImovel in imovel.fotos) {
                    [fotoImovel deleteFromDisk];
                }
                
                [imovel deleteFirebase];
                [imovel MR_deleteInContext:moc];
                continue;
            }
            
            
            // only continues if date if different
            NSDate *lastUpdate = [DateUtils dateFromString:obj[@"update"] withFormat:@"yyyy-MM-dd HH:mm:ss"];;
            
            if ([imovel.lastUpdate isEqualToDate:lastUpdate] && !_forceSync) {
                imovel.pendingUpdateParse = [NSNumber numberWithBool:NO];
                continue;
            }
            
            imovel.lastUpdate = lastUpdate;
            
            imovel.descricao = [obj[@"descricao"] stringByDecodingHTMLEntities].trimmed;
            imovel.version = [NSDate date];
            imovel.identificacao = [obj[@"ref"] stringByDecodingHTMLEntities].trimmed;
            
            // Tipo Oferta
            if([obj[@"operacao"] isEqualToString:@"For Sale"]) {
                imovel.oferta = @"Venda";
            } else if([obj[@"operacao"] isEqualToString:@"For Rent"]) {
                imovel.oferta = @"Locação";
            } else {
                imovel.oferta = @"Outro";
            }
            
            
            // proprietario
            NSDictionary *objProprietario = [obj[@"proprietarios"] objectForKey:@"proprietario"];
            
            if ([objProprietario isKindOfClass:[NSArray class]]) {
                objProprietario = [[obj[@"proprietarios"] objectForKey:@"proprietario"] firstObject];
            }
            
            if (objProprietario != nil) {
                
                predicate = [NSPredicate predicateWithFormat:@"nome = %@ AND telefone = %@",objProprietario[@"propNome"],objProprietario[@"propTels"]];
                Cliente *proprietario = [Cliente MR_findFirstWithPredicate:predicate inContext:moc];
                
                if (proprietario == nil) {
                    proprietario = [Cliente MR_createInContext:moc];
                    proprietario.codigo = [NSNumber numberWithInteger:[objProprietario[@"propId"] integerValue]];
                }
                
                proprietario.nome = objProprietario[@"propNome"];
                proprietario.subtipo = [NSNumber numberWithInteger:[objProprietario[@"propSubtipo"] integerValue]];
                proprietario.telefone = objProprietario[@"propTels"];
                proprietario.email = objProprietario[@"propEmail"];
                
                imovel.cliente = proprietario;
                
            }
            
            // negociações
            NSDictionary *negociacoes = obj[@"negociacoes"];
            
            
            if (negociacoes.count == 1) {
                
                negociacoes = [obj[@"negociacoes"] objectForKey:@"negociacao"];
                
                if ([negociacoes isKindOfClass:[NSArray class]]) {
                    
                    for (NSDictionary *objNegociacao in negociacoes) {
                        [self createNegociacao:objNegociacao imovel:imovel moc:moc];
                        
                    }
                    
                } else {
                    
                    [self createNegociacao:negociacoes imovel:imovel moc:moc];
                    
                }
                
                
            } else {
                
                for (NSDictionary *objNegociacao in negociacoes) {
                    [self createNegociacao:objNegociacao imovel:imovel moc:moc];
                    
                }
                
            }
            
            // Tipo Imovel
            
            NSString *tipo = obj[@"tipo"];
            
            if([tipo isEqualToString:@"Fazenda"]) {
                imovel.tipo = @"Fazenda / Sítio";
            } else if([tipo isEqualToString:@"Lote"]) {
                imovel.tipo = @"Terreno";
            } else if([tipo isEqualToString:@"Fundo de comércio"]) {
                imovel.tipo = @"Loja";
            } else if([tipo isEqualToString:@"Área Rural"]) {
                imovel.tipo = @"Fazenda / Sítio";
            } else if([tipo isEqualToString:@"Loft"]) {
                imovel.tipo = @"Outro";
            } else if([tipo isEqualToString:@"Área Urbana"]) {
                imovel.tipo = @"Outro";
            } else if([tipo isEqualToString:@"Área"]) {
                imovel.tipo = @"Outro";
            } else if([tipo isEqualToString:@"Edícula"]) {
                imovel.tipo = @"Casa";
            } else if([tipo isEqualToString:@"Sobrado"]) {
                imovel.tipo = @"Sobrado";
            } else if([tipo isEqualToString:@"Ponto comercial"]) {
                imovel.tipo = @"Sala / Espaço Comercial";
            } else if([tipo isEqualToString:@"Pousada"]) {
                imovel.tipo = @"Pousada / Hotel / Resort";
            } else if([tipo isEqualToString:@"Sala comercial"]) {
                imovel.tipo = @"Sala / Espaço Comercial";
            } else if([tipo isEqualToString:@"Casa"]) {
                imovel.tipo = @"Casa";
            } else if([tipo isEqualToString:@"Galpão"]) {
                imovel.tipo = @"Armazém / Galpão";
            } else if([tipo isEqualToString:@"Duplex"]) {
                imovel.tipo = @"Duplex";
            } else if([tipo isEqualToString:@"Apartamento"]) {
                imovel.tipo = @"Apartamento";
            }  else if([tipo isEqualToString:@"Kitnet"]) {
                imovel.tipo = @"JK / Kitnet";
            }  else if([tipo isEqualToString:@"Chácara"]) {
                imovel.tipo = @"Fazenda / Sítio";
            }  else if([tipo isEqualToString:@"Terreno"]) {
                imovel.tipo = @"Terreno";
            }  else if([tipo isEqualToString:@"Casa Comercial"]) {
                imovel.tipo = @"Casa Comercial";
            }  else if([tipo isEqualToString:@"Salão Comercial"]) {
                imovel.tipo = @"Sala / Espaço Comercial";
            }  else if([tipo isEqualToString:@"Chalé"]) {
                imovel.tipo = @"Fazenda / Sítio";
            }  else if([tipo isEqualToString:@"Condomínio"]) {
                imovel.tipo = @"Condomínio";
            } else {
                imovel.tipo = @"Outro";
            }
            
            
            // valor
            NSNumber *valor = [currencyFmt numberFromString:obj[@"valor"]];
            
            if ([valor intValue] > 0) {
                imovel.valor = [NSDecimalNumber decimalNumberWithDecimal:[valor decimalValue]];
            }
            
            // area util / area total
            NSNumber *areaTotal = [NSNumber numberWithLong:[obj[@"areaTotal"] integerValue]];
            if ([areaTotal intValue] > 0) {
                imovel.areaTotal =  areaTotal;
            }
            
            NSNumber *areaUtil = [NSNumber numberWithLong:[obj[@"areaPrivativa"] integerValue]];
            if ([areaUtil intValue] > 0) {
                imovel.areaUtil =  areaUtil;
            }
            
            
            // quartos
            NSNumber *quartos = [NSNumber numberWithLong:[obj[@"quartos"] integerValue]];
            
            if ([quartos intValue] > 0) {
                imovel.quartos = quartos;
            }
            
            // tags
            NSString *itemList = obj[@"itens"];
            
            if (itemList.length > 0) {
                
                TagImovel *tagImovel;
                
                if (imovel.tags != nil) {
                    
                    tagImovel = imovel.tags;
                    
                } else {
                    
                    tagImovel = [TagImovel MR_createInContext:moc];
                    tagImovel.codigo = [tagImovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
                    
                }
                
                
                NSArray *itens = [itemList componentsSeparatedByString:@","];
                tagImovel.armarioCozinha = [NSNumber numberWithBool:NO];
                
                if ( [itens indexOfObject:@"1"] != NSNotFound) {
                    tagImovel.armarioCozinha = [NSNumber numberWithBool:YES];
                } else {
                    tagImovel.armarioCozinha = [NSNumber numberWithBool:NO];
                }
                
                if ( [itens indexOfObject:@"2"] != NSNotFound) {
                    tagImovel.armarioEmbutido = [NSNumber numberWithBool:YES];
                } else {
                    tagImovel.armarioEmbutido = [NSNumber numberWithBool:NO];
                }
                
                if ( [itens indexOfObject:@"3"] != NSNotFound) {
                    tagImovel.childrenCare = [NSNumber numberWithBool:YES];
                } else {
                    tagImovel.childrenCare = [NSNumber numberWithBool:NO];
                }
                
                if ( [itens indexOfObject:@"4"] != NSNotFound) {
                    tagImovel.churrasqueira = [NSNumber numberWithBool:YES];
                } else {
                    tagImovel.churrasqueira = [NSNumber numberWithBool:NO];
                }
                
                if ( [itens indexOfObject:@"5"] != NSNotFound) {
                    tagImovel.garagem = [NSNumber numberWithBool:YES];
                } else {
                    tagImovel.garagem = [NSNumber numberWithBool:NO];
                }
                
                if ( [itens indexOfObject:@"6"] != NSNotFound) {
                    tagImovel.piscina = [NSNumber numberWithBool:YES];
                } else {
                    tagImovel.piscina = [NSNumber numberWithBool:NO];
                }
                
                if ( [itens indexOfObject:@"7"] != NSNotFound) {
                    tagImovel.playground = [NSNumber numberWithBool:YES];
                } else {
                    tagImovel.playground = [NSNumber numberWithBool:NO];
                }
                
                if ( [itens indexOfObject:@"8"] != NSNotFound) {
                    tagImovel.quadraPoliesportiva = [NSNumber numberWithBool:YES];
                } else {
                    tagImovel.quadraPoliesportiva = [NSNumber numberWithBool:NO];
                }
                
                if ( [itens indexOfObject:@"9"] != NSNotFound) {
                    tagImovel.salaGinastica = [NSNumber numberWithBool:YES];
                } else {
                    tagImovel.salaGinastica = [NSNumber numberWithBool:NO];
                }
                
                if ( [itens indexOfObject:@"10"] != NSNotFound) {
                    tagImovel.salaoFestas = [NSNumber numberWithBool:YES];
                } else {
                    tagImovel.salaoFestas = [NSNumber numberWithBool:NO];
                }
                
                if ( [itens indexOfObject:@"11"] != NSNotFound) {
                    tagImovel.salaoJogos = [NSNumber numberWithBool:YES];
                } else {
                    tagImovel.salaoJogos = [NSNumber numberWithBool:NO];
                }
                
                if ( [itens indexOfObject:@"12"] != NSNotFound) {
                    tagImovel.sauna = [NSNumber numberWithBool:YES];
                } else {
                    tagImovel.sauna = [NSNumber numberWithBool:NO];
                }
                
                if ( [itens indexOfObject:@"13"] != NSNotFound) {
                    tagImovel.varanda = [NSNumber numberWithBool:YES];
                } else {
                    tagImovel.varanda = [NSNumber numberWithBool:NO];
                }
                
                if ( [itens indexOfObject:@"14"] != NSNotFound) {
                    tagImovel.wcEmpregada = [NSNumber numberWithBool:YES];
                } else {
                    tagImovel.wcEmpregada = [NSNumber numberWithBool:NO];
                }
                
                
                imovel.tags = tagImovel;
                
            }
            
            
            
            // Endereco
            
            Endereco *endereco = nil;
            
            if (imovel.endereco != nil) {
                endereco = imovel.endereco;
            } else {
                
                endereco = [Endereco MR_createInContext:moc];
                endereco.codigo = [endereco getUniqueCode]; //[NSNumber numberWithLong:codigo];
            }
            
            
            double latitude = [obj[@"lat"] doubleValue];
            double longitude = [obj[@"lon"] doubleValue];
            
            endereco.latitude = [NSNumber numberWithDouble:latitude];
            endereco.longitude = [NSNumber numberWithDouble:longitude];
            
            if ([obj[@"uf"] length] > 0) {
                
                predicate = [NSPredicate predicateWithFormat:@"sigla == [cd] %@",obj[@"uf"]];
                Estado *estado = [Estado MR_findFirstWithPredicate:predicate inContext:moc];
                
                if (estado != nil) {
                    endereco.estado = estado.nome;
                }
                
            }
            
            Cidade *cidade = nil;
            if ([obj[@"cidade"] length] > 0) {
                
                
                predicate = [NSPredicate predicateWithFormat:@"nome == [cd] %@",obj[@"cidade"]];
                cidade = [Cidade MR_findFirstWithPredicate:predicate inContext:moc];
                
                if (cidade != nil) {
                    endereco.cidade = cidade.nome;
                }
            }
            
            
            if ([obj[@"bairro"] length] > 0) {
                
                predicate = [NSPredicate predicateWithFormat:@"cidade == [cd] %@ AND nome == [cd] %@",cidade.nome, obj[@"bairro"]];
                count = [Bairro MR_countOfEntitiesWithPredicate:predicate inContext:moc];
                Bairro *bairro = nil;
                
                if(count == 0 ) {
                    
                    bairro = [Bairro MR_createInContext:moc];
                    bairro.codigo = [bairro getUniqueCode]; //[NSNumber numberWithLong:codigo];
                    bairro.nome = obj[@"bairro"];
                    bairro.cidade = cidade.nome;
                    bairro.version = [NSDate date];
                    
                } else {
                    
                    predicate = [NSPredicate predicateWithFormat:@"cidade == [cd] %@ AND nome == [cd] %@",cidade.nome, obj[@"bairro"]];
                    bairro = [Bairro MR_findFirstWithPredicate:predicate inContext:moc];                }
                
                endereco.bairro = bairro;
                
            }
            
            // cep
            if ([obj[@"cep"] length] > 0) {
                NSString *cep = [obj[@"cep"] stringByReplacingOccurrencesOfString:@"-" withString:@""];
                endereco.cep = [NSNumber numberWithLong:[cep integerValue]];
            }
            
            imovel.endereco = endereco;
            
            // fotos
            if (exists) {
                [Utils deleteAllPhotos:imovel withMoc:moc];
            }
            
            NSDictionary *media = [[obj objectForKey:@"fotos"] objectForKey:@"foto"];
            for (NSString *strURL in media) {
                NSMutableDictionary *photo = [NSMutableDictionary dictionary];
                [photo setValue:imovel.codigo forKey:@"codigoImovel"];
                [photo setValue:strURL.trimmed forKey:@"url"];
                [_photos addObject:photo];
                _totalPhotos++;
            }
            
            imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
            [moc MR_saveToPersistentStoreAndWait];
            
        }
    }
}

#pragma mark - Photo Download
- (void)startBackgroundDownloadsForBaseURL:(NSManagedObjectContext *)moc
{
    [Utils startNetworkIndicator];
    
    __block int step = 0;
    __block float progressStep = 0.0;
    
    for (NSDictionary *photo in _photos)
    {
        
        if (_parentController == nil) {
            break;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/_totalPhotos;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Transferindo fotos: %d/%d",step, _totalPhotos];
        });
        
        @autoreleasepool {
            [self downloadPhoto:photo usingMoc:moc];
        }
    }
}

- (void)redownloadPhotos:(NSManagedObjectContext *)moc
{
    __block int step = 0;
    __block float progressStep = 0.0;
    
    for(NSDictionary *photo in _retryPhotos){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/_retryPhotos.count;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Reparando fotos : %d/%lu",step, (unsigned long)_retryPhotos.count];
            
        });
        
        @autoreleasepool {
            [self downloadPhoto:photo usingMoc:moc];
        }
    }
}

- (void)downloadPhoto:(NSDictionary *)photo usingMoc:(NSManagedObjectContext *)moc
{
    
    NSString *strURL = [photo[@"url"] trimmed];
    
    if (strURL.length == 0) {
        return;
    }
    
    NSURL *photoURL = [NSURL URLWithString:strURL];
    
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:photoURL];
    request.timeOutSeconds = 60;
    [request startSynchronous];
    NSError *error = [request error];
    
    if (error) {
        
        [_retryPhotos addObject:photo];
        NSLog(@"%@",[error description]);
    }
    
    FotoImovel *fotoImovel = [FotoImovel MR_createInContext:moc];
    fotoImovel.codigo = [fotoImovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
    
    fotoImovel.descricao = [[strURL lastPathComponent] stringByDeletingPathExtension];
    fotoImovel.importacao = [NSNumber numberWithBool:YES];
    
    NSNumber *codigoImovel = photo[@"codigoImovel"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",codigoImovel];
    fotoImovel.imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:moc];
    
    [fotoImovel insertImageOnDisk:request.responseData];
    
    [moc MR_saveToPersistentStoreAndWait];
}
@end
