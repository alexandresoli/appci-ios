//
//  ImobiBrasilImporter.h
//  Appci
//
//  Created by BrEstate LTDA on 4/28/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ImobiBrasilViewController;
@interface ImobiBrasilImporter : NSObject

@property (nonatomic, weak) UIProgressView *progressView;
@property (nonatomic, weak) UILabel *progressLabel;
@property (nonatomic, weak)  ImobiBrasilViewController *parentController;

- (void)startWithURL:(NSString *)strURL usingMoc:(NSManagedObjectContext *)moc forced:(BOOL)forced;

@end
