//
//  ImobiBrasilImporter.m
//  Appci
//
//  Created by BrEstate LTDA on 4/28/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "ImobiBrasilImporter.h"
#import "Imovel.h"
#import "TagImovel.h"
#import "Endereco.h"
#import "Estado.h"
#import "Cidade.h"
#import "Bairro.h"
#import "SVProgressHUD.h"
#import "FotoImovel+InsertDeleteImagem.h"
#import "NSString+HTML.h"
#import "NSString+Custom.h"
#import "DateUtils.h"
#import "ImobiBrasilViewController.h"
#import "ASIHTTPRequest.h"
#import "XMLDictionary.h"


@interface ImobiBrasilImporter ()

#pragma mark - Properties

@property (nonatomic, strong) NSString *checksum;
@property (nonatomic, strong) NSMutableArray *retryPhotos;
@property (nonatomic, strong) NSDictionary *dictionary;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSMutableArray *imoveis;
@property (nonatomic) int totalPhotos;
@property (nonatomic) BOOL forceSync;

@end

@implementation ImobiBrasilImporter

#pragma mark - Start XML Parsing
- (void)startWithURL:(NSString *)strURL usingMoc:(NSManagedObjectContext *)moc forced:(BOOL)forced
{
    
    [Utils startNetworkIndicator];
    
    _forceSync = forced;
    _retryPhotos = [NSMutableArray array];
    _photos = [NSMutableArray array];
    
    NSString *str = [NSString stringWithFormat:@"http://goo.gl/%@",strURL.trimmed];
    
    NSURL *url = [NSURL URLWithString:str];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    request.timeOutSeconds = 30;
    [request startSynchronous];
    NSError *error = [request error];
    
    if (error) {
        [Utils dismissProgress];
        [Utils showMessage:MESSAGE_IMPORT_ERROR];
        [Utils stopNetworkIndicator];
        return;
    }
    
    NSString *xml = request.responseString;
    
    NSString *newChecksum = [xml MD5];
    
    if (!_forceSync) {
        
        // checksum check
        NSString *oldChecksum = [Utils valueFromDefaults:IMOBIBRASIL_CHECKSUM];
        if ([newChecksum isEqualToString:oldChecksum]) {
            
            [Utils dismissProgress];
            [Utils stopNetworkIndicator];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Aviso" message:@"As informações já estão atualizadas, deseja forçar a sincronização?" delegate:_parentController cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Forçar", nil];
                [alertView show];
            });
            
            
            return;
        }
        
    }
    
    
    _checksum = newChecksum;
    
    
    [Utils stopNetworkIndicator];
    [Utils dismissProgress];
    
    _parentController.hasStarted = YES;
    _dictionary = [NSDictionary dictionaryWithXMLData:request.responseData];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        
        [localContext performBlock:^{
            [self convert:localContext];
            
            if ([self.imoveis count] == 0) {
                [Utils showMessage:MESSAGE_EMPTY_IMPORT];
                [Utils dismissProgress];
                return;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                _progressView.progress = 0.0f;
                _progressLabel.text = @"";
                
            });
            
            // update checksum
            [Utils addObjectToDefaults:_checksum withKey:IMOBIBRASIL_CHECKSUM];
            
            [self startBackgroundDownloadsForBaseURL:localContext];
            
            if (_retryPhotos.count > 0) {
                [self redownloadPhotos:localContext];
            }
            
            [Utils dismissProgress];
            [Utils postEntitytNotification:@"Imovel"];
            [Utils stopNetworkIndicator];
            
            if (_parentController != nil) {
                [Utils showMessage:MESSAGE_FINISH_IMPORT];
            }
            
            [_parentController setHasStarted:NO];
            
        }];
        
    });
    
    
}

#pragma mark - Convert Objects to NSManagedObject
- (void)convert:(NSManagedObjectContext *)moc
{
    
    NSNumberFormatter *currencyFmt = [[NSNumberFormatter alloc] init];
    [currencyFmt setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyFmt setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [currencyFmt setDecimalSeparator:@","];
    [currencyFmt setGeneratesDecimalNumbers:YES];
    [currencyFmt setCurrencySymbol:@"R$ "];
    [currencyFmt setLenient:YES];
    
    
    _imoveis = [_dictionary objectForKey:@"imovel"];
    
    __block long total = [_imoveis count];
    __block int step = 0;
    __block float progressStep = 0.0f;
    
    for(NSDictionary *obj in _imoveis) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/total;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Importando: %d/%ld",step, total];
        });
        
        @autoreleasepool {
            
            long count = 0;
            
            Imovel *imovel;
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",obj[@"id"]];
            
            count = [Imovel MR_countOfEntitiesWithPredicate:predicate inContext:moc];
            
            BOOL exists = NO;
            if(count > 0 ) {
                exists = YES;
                imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:moc];
                imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
            } else {
                imovel = [Imovel MR_createInContext:moc];
                imovel.codigo = [NSNumber numberWithLongLong:[obj[@"id"] longLongValue]];
                
            }
            
            NSString *descricao = [obj[@"descricao"] stringByDecodingHTMLEntities];
            
            imovel.descricao = descricao;
            imovel.lastUpdate = [NSDate date];
            imovel.version = [NSDate date];
            
            // Identificacao
            imovel.identificacao = obj[@"ref"];
            
            
            // Tipo Oferta
            NSString *transacao = obj[@"transacao"];
            if([transacao isEqualToString:@"Venda"]) {
                imovel.oferta = @"Venda";
            } else if([transacao isEqualToString:@"Locação"]) {
                imovel.oferta = @"Locação";
            } else if([transacao isEqualToString:@"Temporada"]) {
                imovel.oferta = @"Temporada";
            } else {
                imovel.oferta = @"Outro";
            }
            
            
            // Tipo Imovel
            NSString *tipo = obj[@"tipoimovel"];
            if([tipo isEqualToString:@"Apartamento"]) {
                imovel.tipo = @"Apartamento";
            } else if([tipo isEqualToString:@"Área"]) {
                imovel.tipo = @"Outro";
            } else if([tipo isEqualToString:@"Casa"]) {
                imovel.tipo = @"Casa";
            } else if([tipo isEqualToString:@"Chácara"]) {
                imovel.tipo = @"Chácara";
            } else if([tipo isEqualToString:@"Comercial"]) {
                imovel.tipo = @"Sala / Espaço Comercial";
            } else if([tipo isEqualToString:@"Diversos"]) {
                imovel.tipo = @"Outro";
            } else if([tipo isEqualToString:@"Escritório"]) {
                imovel.tipo = @"Sala / Espaço Comercial";
            } else if([tipo isEqualToString:@"Fazenda"]) {
                imovel.tipo = @"Fazenda / Sítio";
            }  else if([tipo isEqualToString:@"Flat"]) {
                imovel.tipo = @"Flat";
            }  else if([tipo isEqualToString:@"Galpão"]) {
                imovel.tipo = @"Armazém / Galpão";
            }  else if([tipo isEqualToString:@"Kitnet"]) {
                imovel.tipo = @"JK / Kitnet";
            }  else if([tipo isEqualToString:@"Loja"]) {
                imovel.tipo = @"Loja";
            }  else if([tipo isEqualToString:@"Pousada"]) {
                imovel.tipo = @"Pousada / Hotel / Resort";
            }  else if([tipo isEqualToString:@"Terreno"]) {
                imovel.tipo = @"Terreno";
            } else {
                imovel.tipo = @"Outro";
            }
            
            
            // valor
            NSNumber *valor = [currencyFmt numberFromString:obj[@"valor"]];
            
            if (valor != nil) {
                imovel.valor = [NSDecimalNumber decimalNumberWithDecimal:[valor decimalValue]];
            }
            
            // condominio
            valor = [currencyFmt numberFromString:obj[@"valor_condominio"]];
            
            if (valor != nil) {
                imovel.condominio = [NSDecimalNumber decimalNumberWithDecimal:[valor decimalValue]];
            }
            
            // area util / area total
            NSNumber *areaTotal = [NSNumber numberWithLong:[obj[@"area_total"] integerValue]];
            if ([areaTotal intValue] > 0) {
                imovel.areaTotal =  areaTotal;
            }
            
            NSNumber *areaUtil = [NSNumber numberWithLong:[obj[@"area_construida"] integerValue]];
            if ([areaUtil intValue] > 0) {
                imovel.areaUtil =  areaUtil;
            }
            
            
            // quartos
            NSNumber *quartos = [NSNumber numberWithLong:[obj[@"dormitorios"] integerValue]];
            
            if ([quartos intValue] > 0) {
                imovel.quartos = quartos;
            }
            
            
            // tags
            
            TagImovel *tag;
            
            if (imovel.tags != nil) {
                tag = imovel.tags;
            } else {
                
                tag = [TagImovel MR_createInContext:moc];
                tag.codigo = [tag getUniqueCode]; //[NSNumber numberWithLong:codigo];
            }
            
            if ([obj[@"vagas"] intValue] > 0) {
                tag.garagem = [NSNumber numberWithBool:YES];
            } else {
                tag.garagem = [NSNumber numberWithBool:NO];
            }
            
            imovel.tags = tag;
            
            
            
            // Endereco
            Endereco *endereco;
            if (imovel.endereco != nil) {
                endereco = imovel.endereco;
            } else {
                
                endereco = [Endereco MR_createInContext:moc];
                endereco.codigo = [endereco getUniqueCode]; //[NSNumber numberWithLong:codigo];
            }
            
            
            NSString *logradouro = obj[@"endereco_logradouro"];
            
            if([obj[@"endereco_numero"] length] > 0) {
                endereco.logradouro =  [NSString stringWithFormat:@"%@, %@",logradouro,obj[@"endereco_numero"]];
            }
            
            
            // cep
            if([obj[@"endereco_cep"] length] > 0) {
                NSString *cep = [obj[@"endereco_cep"] stringByReplacingOccurrencesOfString:@"-" withString:@""];
                endereco.cep = [NSNumber numberWithInt:[cep intValue]];
            }
            
            if ([obj[@"endereco_complemento"] length] > 0) {
                endereco.complemento = obj[@"endereco_complemento"];
            }
            
            
            if ([obj[@"endereco_estado"] length] > 0) {
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sigla == [cd] %@",obj[@"endereco_estado"]];
                Estado *estado = [Estado MR_findFirstWithPredicate:predicate inContext:moc];
                
                if (estado != nil) {
                    endereco.estado = estado.nome.trimmed;
                }
                
            }
            
            
            Cidade *cidade;
            if ([obj[@"endereco_cidade"] length] > 0) {
                
                predicate = [NSPredicate predicateWithFormat:@"nome == [cd] %@",obj[@"endereco_cidade"]];
                cidade = [Cidade MR_findFirstWithPredicate:predicate inContext:moc];
                
                if (cidade != nil) {
                    endereco.cidade = cidade.nome.trimmed;
                }
                
            }
            
            
            if ([obj[@"endereco_bairro"] length] > 0) {
                
                predicate = [NSPredicate predicateWithFormat:@"cidade == [cd] %@ AND nome == [cd] %@",cidade.nome.trimmed, obj[@"endereco_bairro"]];
                count = [Bairro MR_countOfEntitiesWithPredicate:predicate inContext:moc];
                Bairro *bairro = nil;
                
                if(count == 0 ) {
                    
                    bairro = [Bairro MR_createInContext:moc];
                    
                    bairro.codigo = [bairro getUniqueCode]; //[NSNumber numberWithLong:codigo];
                    bairro.nome = obj[@"endereco_bairro"];
                    bairro.cidade = cidade.nome.trimmed;
                    bairro.version = [NSDate date];
                    
                } else {
                    
                    predicate = [NSPredicate predicateWithFormat:@"cidade == [cd] %@ AND nome == [cd] %@",cidade.nome.trimmed, obj[@"endereco_bairro"]];
                    bairro = [Bairro MR_findFirstWithPredicate:predicate inContext:moc];
                }
                
                endereco.bairro = bairro;
                
            }
            
            imovel.endereco = endereco;
            
            if (exists) {
                [Utils deleteAllPhotos:imovel withMoc:moc];
            }
            
            NSDictionary *fotos = [[obj objectForKey:@"fotos"] objectForKey:@"foto"];
            NSMutableDictionary *newMedia;
            
            for (NSDictionary *photo in fotos) {
                
                if (![photo isKindOfClass:[NSDictionary class]]) {
                    
                    newMedia = [NSMutableDictionary dictionary];
                    [newMedia setValue:fotos[@"foto_url"] forKey:@"foto_url"];
                    [newMedia setValue:imovel.codigo forKey:@"codigoImovel"];
                    [newMedia setValue:@"Sim" forKey:@"foto_principal"];
                    [_photos addObject:newMedia];
                    _totalPhotos++;
                    break;
                    
                }
                
                newMedia = [NSMutableDictionary dictionaryWithDictionary:photo];
                [newMedia setValue:imovel.codigo forKey:@"codigoImovel"];
                
                [_photos addObject:newMedia];
                _totalPhotos++;
                
            }
            
            imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
            [moc MR_saveToPersistentStoreAndWait];
            
        }
        
    }
}


#pragma mark - Photo Download
- (void)startBackgroundDownloadsForBaseURL:(NSManagedObjectContext *)moc
{
    [Utils startNetworkIndicator];
    
    __block int step = 0;
    __block float progressStep = 0.0;
    
    for (NSDictionary *photo in _photos)
    {
        
        if (_parentController == nil) {
            break;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/_totalPhotos;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Transferindo fotos: %d/%d",step, _totalPhotos];
        });
        
        @autoreleasepool {
            [self downloadPhoto:photo usingMoc:moc];
        }
    }
}

- (void)redownloadPhotos:(NSManagedObjectContext *)moc
{
    __block int step = 0;
    __block float progressStep = 0.0;
    
    for(NSDictionary *photo in _retryPhotos){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/_retryPhotos.count;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Reparando fotos : %d/%lu",step, (unsigned long)_retryPhotos.count];
            
        });
        
        @autoreleasepool {
            [self downloadPhoto:photo usingMoc:moc];
        }
    }
}

- (void)downloadPhoto:(NSDictionary *)photo usingMoc:(NSManagedObjectContext *)moc
{
    
    NSString *strURL = [photo[@"foto_url"] trimmed];
    
    if (strURL.length == 0) {
        return;
    }
    
    NSURL *photoURL = [NSURL URLWithString:strURL];
    
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:photoURL];
    request.timeOutSeconds = 60;
    [request startSynchronous];
    NSError *error = [request error];
    
    if (error) {
        
        [_retryPhotos addObject:photo];
        NSLog(@"%@",[error description]);
    }
    
    FotoImovel *fotoImovel = [FotoImovel MR_createInContext:moc];
    fotoImovel.codigo = [fotoImovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
    
    fotoImovel.descricao = [[strURL lastPathComponent] stringByDeletingPathExtension];
    fotoImovel.importacao = [NSNumber numberWithBool:YES];
    
    NSNumber *codigoImovel = photo[@"codigoImovel"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",codigoImovel];
    fotoImovel.imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:moc];
    
    if ([photo[@"foto_principal"] isEqualToString:@"Sim"]) {
        fotoImovel.capa  = [NSNumber numberWithBool:YES];
    }
    [fotoImovel insertImageOnDisk:request.responseData];
    
    [moc MR_saveToPersistentStoreAndWait];
}


@end
