//
//  ImovelProImporter.h
//  Appci
//
//  Created by BrEstate LTDA on 5/6/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ImovelProViewController;
@interface ImovelProImporter : NSObject

@property (nonatomic, weak) UIProgressView *progressView;
@property (nonatomic, weak) UILabel *progressLabel;

@property (nonatomic, weak)  ImovelProViewController *parentController;

- (void)startWithURL:(NSString *)strURL usingMoc:(NSManagedObjectContext *)moc forced:(BOOL)forced;

@end
