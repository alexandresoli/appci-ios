//
//  ImovelProImporter.m
//  Appci
//
//  Created by BrEstate LTDA on 5/6/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "ImovelProImporter.h"
#import "SVProgressHUD.h"
#import "Imovel.h"
#import "TagImovel.h"
#import "Endereco.h"
#import "Estado.h"
#import "Cidade.h"
#import "Bairro.h"
#import "FotoImovel+InsertDeleteImagem.h"
#import "NSString+HTML.h"
#import "NSString+Custom.h"
#import "ImovelProViewController.h"
#import "ASIHTTPRequest.h"
#import "XMLDictionary.h"

@interface ImovelProImporter ()

#pragma mark - Properties

@property (nonatomic, strong) NSString *checksum;
@property (nonatomic, strong) NSMutableArray *retryPhotos;
@property (nonatomic, strong) NSDictionary *dictionary;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSMutableArray *imoveis;
@property (nonatomic) int totalPhotos;
@property (nonatomic) BOOL forceSync;

@end

@implementation ImovelProImporter


#pragma mark - Start XML Parsing
- (void)startWithURL:(NSString *)strURL usingMoc:(NSManagedObjectContext *)moc forced:(BOOL)forced
{
    [Utils startNetworkIndicator];
    
    
    _forceSync = forced;
    _retryPhotos = [NSMutableArray array];
    _photos = [NSMutableArray array];
    
    NSURL *url = [NSURL URLWithString:strURL.trimmed];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    request.timeOutSeconds = 30;
    [request startSynchronous];
    NSError *error = [request error];
    
    if (error) {
        [Utils dismissProgress];
        [Utils showMessage:MESSAGE_IMPORT_ERROR];
        [Utils stopNetworkIndicator];
        return;
    }
    
    NSString *xml = request.responseString;
    
    if([xml rangeOfString:@"hash invÃ¡lido."].location != NSNotFound ){
        
        [Utils showMessage:@"Código da importação inválido!"];
        
        [Utils dismissProgress];
        [Utils stopNetworkIndicator];
        return;
    }
    
    
    NSString *newChecksum = [xml MD5];
    
    if (!_forceSync) {
        
        // checksum check
        NSString *oldChecksum = [Utils valueFromDefaults:IMOVELPRO_CHECKSUM];
        if ([newChecksum isEqualToString:oldChecksum]) {
            
            [Utils dismissProgress];
            [Utils stopNetworkIndicator];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Aviso" message:@"As informações já estão atualizadas, deseja forçar a sincronização?" delegate:_parentController cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Forçar", nil];
                [alertView show];
            });
            
            
            return;
        }
        
    }
    
    _checksum = newChecksum;
    
    
    [Utils stopNetworkIndicator];
    [Utils dismissProgress];
    
    _parentController.hasStarted = YES;
    _dictionary = [NSDictionary dictionaryWithXMLData:request.responseData];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        
        [localContext performBlock:^{
            [self convert:localContext];
            
            if ([self.imoveis count] == 0) {
                [Utils showMessage:MESSAGE_EMPTY_IMPORT];
                [Utils dismissProgress];
                return;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                _progressView.progress = 0.0f;
                _progressLabel.text = @"";
                
            });
            
            // update checksum
            [Utils addObjectToDefaults:_checksum withKey:IMOVELPRO_CHECKSUM];
            
            [self startBackgroundDownloadsForBaseURL:localContext];
            
            if (_retryPhotos.count > 0) {
                [self redownloadPhotos:localContext];
            }
            
            [Utils dismissProgress];
            [Utils postEntitytNotification:@"Imovel"];
            [Utils stopNetworkIndicator];
            
            if (_parentController != nil) {
                [Utils showMessage:MESSAGE_FINISH_IMPORT];
            }
            
            [_parentController setHasStarted:NO];
            
        }];
        
    });
}

#pragma mark - Convert Objects to NSManagedObject
- (void)convert:(NSManagedObjectContext *)moc
{
    
    NSNumberFormatter *numberFmt = [[NSNumberFormatter alloc] init];
    numberFmt.numberStyle = NSNumberFormatterDecimalStyle;
    numberFmt.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"];
    numberFmt.negativeFormat = @"0.00";
    
    
    NSNumberFormatter *currencyFmt = [[NSNumberFormatter alloc] init];
    [currencyFmt setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyFmt setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [currencyFmt setDecimalSeparator:@","];
    [currencyFmt setGeneratesDecimalNumbers:YES];
    [currencyFmt setCurrencySymbol:@"R$ "];
    [currencyFmt setLenient:YES];
    currencyFmt.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"];
    
    _imoveis = [_dictionary objectForKey:@"imovel"];
    
    __block long total = [_imoveis count];
    __block int step = 0;
    __block float progressStep = 0.0f;
    
    
    
    for(NSDictionary *obj in _imoveis) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/total;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Importando: %d/%ld",step, total];
        });
        
        @autoreleasepool {
            
            long count = 0;
            
            Imovel *imovel;
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",obj[@"_id"]];
            
            count = [Imovel MR_countOfEntitiesWithPredicate:predicate inContext:moc];
            
            BOOL exists = NO;
            if(count > 0 ) {
                imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:moc];
                exists = YES;
                imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
            } else {
                
                imovel = [Imovel MR_createInContext:moc];
                imovel.codigo = [NSNumber numberWithLong:[obj[@"_id"] integerValue]];
                
            }
            
            imovel.descricao = [[obj[@"descricao"] objectForKey:@"descricao_externa"] stringByReplacingOccurrencesOfString:@"<br>" withString:@""];
            imovel.version = [NSDate date];
            imovel.identificacao = obj[@"referencia"];
            imovel.notaPrivada = [obj[@"descricao"] objectForKey:@"super_comentario"];
            
            // Tipo Oferta
            NSDictionary *venda = [obj[@"negociacao"] objectForKey:@"venda"];
            NSDictionary *locacao = [obj[@"negociacao"] objectForKey:@"locacao"];
            NSNumber *valor;
            
            if([venda[@"imovel_a_venda"] isEqualToString:@"Sim"]) {
                imovel.oferta = @"Venda";
                valor = [currencyFmt numberFromString:venda[@"preco"]];
            } else if([locacao[@"imovel_para_locacao"] isEqualToString:@"Sim"]) {
                imovel.oferta = @"Locação";
                valor = [currencyFmt numberFromString:locacao[@"preco"]];
            } else {
                imovel.oferta = @"Outro";
            }
            
            
            // valor
            if ([valor intValue] > 0) {
                imovel.valor = [NSDecimalNumber decimalNumberWithDecimal:[valor decimalValue]];
            }
            
            // Tipo Imovel
            NSDictionary *caracteristicas = obj[@"caracteristicas"];
            NSString *tipoImovel = caracteristicas[@"tipo_imovel"];
            
            if([tipoImovel isEqualToString:@"Casa"]) {
                imovel.tipo = @"Casa";
            } else if([tipoImovel isEqualToString:@"Prédio"]) {
                imovel.tipo = @"Prédio";
            } else if([tipoImovel isEqualToString:@"Kitinete / JK"]) {
                imovel.tipo = @"JK / Kitnet";
            } else if([tipoImovel isEqualToString:@"Loja"]) {
                imovel.tipo = @"Loja";
            } else if([tipoImovel isEqualToString:@"Apartamento"]) {
                imovel.tipo = @"Apartamento";
            } else if([tipoImovel isEqualToString:@"Sala / Salão comercial"]) {
                imovel.tipo = @"Sala / Espaço Comercial";
            } else if([tipoImovel isEqualToString:@"Chácara"]) {
                imovel.tipo = @"Chácara";
            } else if([tipoImovel isEqualToString:@"Terreno"]) {
                imovel.tipo = @"Terreno";
            } else if([tipoImovel isEqualToString:@"Cobertura"]) {
                imovel.tipo = @"Cobertura";
            } else if([tipoImovel isEqualToString:@"Studio"]) {
                imovel.tipo = @"Outro";
            } else if([tipoImovel isEqualToString:@"Hotel"]) {
                imovel.tipo = @"Pousada / Hotel / Resort";
            } else if([tipoImovel isEqualToString:@"Casa comercial"]) {
                imovel.tipo = @"Casa Comercial";
            } else if([tipoImovel isEqualToString:@"Terreno em condomínio"]) {
                imovel.tipo = @"Terreno";
            } else if([tipoImovel isEqualToString:@"Fazenda"]) {
                imovel.tipo = @"Fazenda / Sítio";
            } else if([tipoImovel isEqualToString:@"Flat"]) {
                imovel.tipo = @"Flat";
            } else if([tipoImovel isEqualToString:@"Chalé"]) {
                imovel.tipo = @"Outro";
            } else if([tipoImovel isEqualToString:@"Casa em condomínio"]) {
                imovel.tipo = @"Casa";
            } else if([tipoImovel isEqualToString:@"Área Rural"]) {
                imovel.tipo = @"Outro";
            } else {
                imovel.tipo = @"Outro";
            }
            
            // area util / area total
            NSDictionary *medidas = obj[@"medidas"];
            NSNumber *areaTotal = [numberFmt numberFromString:[medidas[@"area_total"] objectForKey:@"__text"]];
            if ([areaTotal intValue] > 0) {
                imovel.areaTotal = areaTotal;
            }
            
            NSNumber *areaUtil = [numberFmt numberFromString:[medidas[@"area_util"] objectForKey:@"__text"]];
            if ([areaUtil intValue] > 0) {
                imovel.areaUtil = areaUtil;
            }
            
            // quartos
            NSDictionary *ambientes = obj[@"ambientes"];
            NSNumber *quartos = [numberFmt numberFromString:ambientes[@"dormitorios"]];
            
            if ([quartos intValue] > 0) {
                imovel.quartos = quartos;
            }
            
            
            // tags
            TagImovel *tag;
            if(imovel.tags != nil) {
                tag = imovel.tags;
            } else {
                
                tag = [TagImovel MR_createInContext:moc];
                tag.codigo = [tag getUniqueCode]; //[NSNumber numberWithLong:codigo];
                
            }
            
            tag.piscina = [NSNumber numberWithBool:[ambientes[@"piscinas"] boolValue]];
            tag.churrasqueira = [NSNumber numberWithBool:[ambientes[@"churrasqueiras"] boolValue]];
            tag.garagem = [NSNumber numberWithBool:[ambientes[@"garagens"] boolValue]];
            
            imovel.tags = tag;
            
            
            
            // Endereco
            Endereco *endereco;
            if (imovel.endereco != nil) {
                endereco = imovel.endereco;
            } else {
                
                endereco = [Endereco MR_createInContext:moc];
                endereco.codigo = [endereco getUniqueCode]; //[NSNumber numberWithLong:codigo];
            }
            
            NSDictionary *maps = obj[@"google_maps"];
            if (maps.count > 0) {
                
                endereco.latitude = [NSNumber numberWithDouble:[maps[@"latitude"] doubleValue]];
                endereco.longitude = [NSNumber numberWithDouble:[maps[@"longitude"] doubleValue]];
            }
            
            
            NSDictionary *enderecoDic = obj[@"endereco"];
            if ([enderecoDic[@"cep"] length] > 0) {
                
                NSString *cep = [enderecoDic[@"cep"] stringByReplacingOccurrencesOfString:@"-" withString:@""];
                endereco.cep = [NSNumber numberWithInt:[cep intValue]];
                
            }
            
            NSString *logradouro = enderecoDic[@"logradouro"];
            
            if (logradouro.length > 0) {
                
                int numero = [enderecoDic[@"numero"] intValue];
                
                if(numero > 0) {
                    logradouro = [NSString stringWithFormat:@"%@, %d",logradouro,numero];
                }
                
                int andar = [enderecoDic[@"andar"] intValue];
                if(andar > 0) {
                    logradouro = [logradouro stringByAppendingString:[NSString stringWithFormat:@", %d andar", andar]];
                }
                
                
                endereco.logradouro = logradouro;
                
            }
            
            
            NSString *complemento =  enderecoDic[@"complemento"];
            
            if (complemento.length > 0) {
                
                NSString *quadra =  enderecoDic[@"quadra"];
                NSString *lote = enderecoDic[@"lote"];
                
                if(quadra.length > 0) {
                    complemento = [complemento stringByAppendingString:[NSString stringWithFormat:@" quadra %@",quadra]];
                }
                
                if(lote.length > 0) {
                    complemento = [complemento stringByAppendingString:[NSString stringWithFormat:@" lote %@",lote]];
                }
                
                endereco.complemento = complemento;
                
            }
            
            
            NSString *sigla = enderecoDic[@"estado"];
            
            if (sigla.length > 0) {
                
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sigla == [cd] %@",sigla];
                Estado *estado = [Estado MR_findFirstWithPredicate:predicate inContext:moc];
                endereco.estado = estado.nome;
            }
            
            
            NSString *strCidade = enderecoDic[@"cidade"];
            
            if (strCidade.length > 0) {
                
                predicate = [NSPredicate predicateWithFormat:@"nome == [cd] %@",strCidade];
                Cidade *cidade = [Cidade MR_findFirstWithPredicate:predicate inContext:moc];
                endereco.cidade = cidade.nome;
                
                
                NSString *strBairro = enderecoDic[@"bairro"];
                
                if (strBairro.length > 0) {
                    
                    predicate = [NSPredicate predicateWithFormat:@"cidade == [cd] %@ AND nome == [cd] %@",cidade.nome, strBairro];
                    count = [Bairro MR_countOfEntitiesWithPredicate:predicate inContext:moc];
                    
                    Bairro *bairro = nil;
                    
                    if(count == 0 ) {
                        
                        bairro = [Bairro MR_createInContext:moc];
                        bairro.codigo = [bairro getUniqueCode]; //[NSNumber numberWithLong:codigo];
                        bairro.nome = strBairro;
                        bairro.cidade = cidade.nome;
                        bairro.version = [NSDate date];
                        
                    } else {
                        
                        predicate = [NSPredicate predicateWithFormat:@"cidade == [cd] %@ AND nome == [cd] %@",cidade.nome, strBairro];
                        bairro = [Bairro MR_findFirstWithPredicate:predicate inContext:moc];
                    }
                    
                    endereco.bairro = bairro;
                    
                }
                
            }
            
            imovel.endereco = endereco;
            
            
            // fotos
            if (exists) {
                [Utils deleteAllPhotos:imovel withMoc:moc];
            }
            
            NSDictionary *fotos = [[obj objectForKey:@"fotos"] objectForKey:@"foto"];
            NSMutableDictionary *newMedia;
            
            for (NSDictionary *photo in fotos) {
                
                if (![photo isKindOfClass:[NSDictionary class]]) {
                    
                    newMedia = [NSMutableDictionary dictionary];
                    [newMedia setValue:fotos[@"url"] forKey:@"url"];
                    [newMedia setValue:imovel.codigo forKey:@"codigoImovel"];
                    [newMedia setValue:@"Sim" forKey:@"principal"];
                    [_photos addObject:newMedia];
                    _totalPhotos++;
                    break;
                    
                }
                
                newMedia = [NSMutableDictionary dictionaryWithDictionary:photo];
                [newMedia setValue:imovel.codigo forKey:@"codigoImovel"];
                [_photos addObject:newMedia];
                _totalPhotos++;
                
            }
            
            
            imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
            [moc MR_saveToPersistentStoreAndWait];
        }
    }
}


#pragma mark - Photo Download
- (void)startBackgroundDownloadsForBaseURL:(NSManagedObjectContext *)moc
{
    [Utils startNetworkIndicator];
    
    __block int step = 0;
    __block float progressStep = 0.0;
    
    for (NSDictionary *photo in _photos)
    {
        
        if (_parentController == nil) {
            break;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/_totalPhotos;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Transferindo fotos: %d/%d",step, _totalPhotos];
        });
        
        @autoreleasepool {
            [self downloadPhoto:photo usingMoc:moc];
        }
    }
}

- (void)redownloadPhotos:(NSManagedObjectContext *)moc
{
    __block int step = 0;
    __block float progressStep = 0.0;
    
    for(NSDictionary *photo in _retryPhotos){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/_retryPhotos.count;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Reparando fotos : %d/%lu",step, (unsigned long)_retryPhotos.count];
            
        });
        
        @autoreleasepool {
            [self downloadPhoto:photo usingMoc:moc];
        }
    }
}

- (void)downloadPhoto:(NSDictionary *)photo usingMoc:(NSManagedObjectContext *)moc
{
    
    
    NSString *strURL = [photo[@"url"] trimmed];
    
    if (strURL.length == 0) {
        return;
    }
    
    NSURL *photoURL = [NSURL URLWithString:strURL];
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:photoURL];
    request.timeOutSeconds = 60;
    [request startSynchronous];
    NSError *error = [request error];
    
    if (error) {
        [_retryPhotos addObject:photo];
        NSLog(@"%@",[error description]);
    }
    
    FotoImovel *fotoImovel = [FotoImovel MR_createInContext:moc];
    fotoImovel.codigo = [fotoImovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
    
    fotoImovel.descricao = [[strURL lastPathComponent] stringByDeletingPathExtension];
    fotoImovel.importacao = [NSNumber numberWithBool:YES];
    
    NSNumber *codigoImovel = photo[@"codigoImovel"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",codigoImovel];
    fotoImovel.imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:moc];
    
    if ([photo[@"principal"] isEqualToString:@"Sim"]) {
        fotoImovel.capa  = [NSNumber numberWithBool:YES];
    }
    
    [fotoImovel insertImageOnDisk:request.responseData];
    
    [moc MR_saveToPersistentStoreAndWait];
}


@end
