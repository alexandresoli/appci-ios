//
//  Sub100.h
//  Appci
//
//  Created by Alexandre Oliveira on 9/23/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Sub100 : NSObject

@property(nonatomic, strong) NSMutableString *bairro;
@property(nonatomic, strong) NSMutableString *cidade;
@property(nonatomic, strong) NSMutableString *uf;
@property(nonatomic, strong) NSMutableString *referencia;
@property(nonatomic, strong) NSMutableString *operacao;
@property(nonatomic, strong) NSMutableString *valor;
@property(nonatomic, strong) NSMutableString *areaPrivativa;
@property(nonatomic, strong) NSMutableString *areaTotal;
@property(nonatomic, strong) NSMutableString *tipo;
@property(nonatomic, strong) NSMutableString *quartos;
@property(nonatomic, strong) NSMutableString *suites;
@property(nonatomic, strong) NSMutableString *descricao;
@property(nonatomic, strong) NSMutableString *latitude;
@property(nonatomic, strong) NSMutableString *longitude;

@property(nonatomic, strong) NSMutableArray *fotos;
@property(nonatomic, strong) NSMutableDictionary *proprietario;

@property(nonatomic, strong) NSNumber *codigoImovel;


@end
