//
//  Sub100.m
//  Appci
//
//  Created by Alexandre Oliveira on 9/23/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Sub100.h"

@implementation Sub100

- (id)init
{
    if(self = [super init]) {
        
        self.bairro = [NSMutableString string];
        self.cidade = [NSMutableString string];
        self.uf = [NSMutableString string];
        self.referencia = [NSMutableString string];
        self.operacao = [NSMutableString string];
        self.valor = [NSMutableString string];
        self.areaPrivativa = [NSMutableString string];
        self.areaTotal = [NSMutableString string];
        self.tipo = [NSMutableString string];
        self.quartos = [NSMutableString string];
        self.suites = [NSMutableString string];
        self.descricao = [NSMutableString string];
        self.latitude = [NSMutableString string];
        self.longitude = [NSMutableString string];
        
        self.proprietario = [NSMutableDictionary dictionary];
        [self.proprietario setValue:[NSMutableString string] forKey:@"nome"];
        [self.proprietario setValue:[NSMutableString string] forKey:@"email"];
        [self.proprietario setValue:[NSMutableString string] forKey:@"telefones"];

        self.fotos = [NSMutableArray array];
        self.codigoImovel = [NSNumber numberWithInt:0];
        
    }
    
    return self;
}

@end
