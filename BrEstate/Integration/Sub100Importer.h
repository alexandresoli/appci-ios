//
//  Sub100Importer.h
//  Appci
//
//  Created by Alexandre Oliveira on 9/23/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Sub100Importer : NSObject

@property (nonatomic, weak) UIProgressView *progressView;
@property (nonatomic, weak) UILabel *progressLabel;

- (void)startWithURL:(NSString *)strURL usingMoc:(NSManagedObjectContext *)moc;

@end
