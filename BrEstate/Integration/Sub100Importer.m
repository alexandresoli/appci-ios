//
//  Sub100Importer.m
//  Appci
//
//  Created by Alexandre Oliveira on 9/23/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Sub100Importer.h"
#import "Sub100.h"
#import "NSString+Custom.h"
#import "Imovel.h"
#import "Endereco.h"
#import "Estado.h"
#import "Cidade.h"
#import "Bairro.h"
#import "Cliente.h"
#import "FotoImovel+InsertDeleteImagem.h"

@interface Sub100Importer () <NSXMLParserDelegate>

#pragma mark - Properties
@property(nonatomic, strong) NSXMLParser *parser;
@property(nonatomic, strong) NSString *element;
@property(nonatomic, strong) NSMutableArray *imoveis;
@property(nonatomic, strong) Sub100 *sub100;
@property(nonatomic, strong) NSMutableString *foto;
@property(nonatomic, strong) NSMutableString *fotoCapa;
@property (nonatomic) int totalPhotos;
@property(nonatomic, strong) NSMutableArray *proprietarios;

@end

@implementation Sub100Importer

#pragma mark - Start XML Parsing
- (void)startWithURL:(NSString *)strURL usingMoc:(NSManagedObjectContext *)moc
{
    
    [Utils startNetworkIndicator];
    
    _foto = [NSMutableString string];
    _fotoCapa = [NSMutableString string];
    _imoveis = [NSMutableArray array];
    
    NSError *error = nil;
    //    NSString *xml = [NSString stringWithContentsOfURL:[NSURL URLWithString:strURL] encoding:NSISOLatin1StringEncoding error:&error];
    //
    //    if (error != nil) {
    //        [Utils dismissProgress];
    //        [Utils showMessage:MESSAGE_IMPORT_ERROR];
    //         [Utils stopNetworkIndicator];
    //        return;
    //    }
    //
    //    [Utils stopNetworkIndicator];
    //
    //    NSData *data = [xml dataUsingEncoding:NSISOLatin1StringEncoding];
    //
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"sub100" withExtension:@"xml"];
    NSString *xml = [NSString stringWithContentsOfURL:url encoding:NSISOLatin1StringEncoding error:&error];
    NSData *data = [xml dataUsingEncoding:NSISOLatin1StringEncoding];
    
    [Utils stopNetworkIndicator];
    
    _parser = [[NSXMLParser alloc] initWithData:data];
    _parser.delegate = self;
    _parser.shouldResolveExternalEntities = NO;
    [_parser parse];
    
}



#pragma mark - NSXMLParserDelegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    
    _element = elementName;
    
    if([elementName isEqualToString:@"imovel"]) {
        _sub100 = [[Sub100 alloc] init];
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    
    if([string isEqualToString:@"\n"]) {
        return;
    }
    
    if([_element isEqualToString:@"bairro"]) {
        [_sub100.bairro appendString:string];
    } else if([_element isEqualToString:@"cidade"]) {
        [_sub100.cidade appendString:string];
    } else if([_element isEqualToString:@"uf"]) {
        [_sub100.uf appendString:string];
    } else if([_element isEqualToString:@"ref"]) {
        [_sub100.referencia appendString:string];
    } else if([_element isEqualToString:@"operacao"]) {
        [_sub100.operacao appendString:string];
    } else if([_element isEqualToString:@"valor"]) {
        [_sub100.valor appendString:string];
    } else if([_element isEqualToString:@"areaPrivativa"]) {
        [_sub100.areaPrivativa appendString:string];
    } else if([_element isEqualToString:@"areaTotal"]) {
        [_sub100.areaTotal appendString:string];
    } else if([_element isEqualToString:@"tipo"]) {
        [_sub100.tipo appendString:string];
    } else if([_element isEqualToString:@"quartos"]) {
        [_sub100.quartos appendString:string];
    } else if([_element isEqualToString:@"suites"]) {
        [_sub100.suites appendString:string];
    } else if([_element isEqualToString:@"descricao"]) {
        [_sub100.descricao appendString:string];
    } else if([_element isEqualToString:@"latitude"]) {
        [_sub100.latitude appendString:string];
    } else if([_element isEqualToString:@"longitude"]) {
        [_sub100.longitude appendString:string];
    } else if([_element isEqualToString:@"foto"]) {
        [_foto appendString:string];
    } else if([_element isEqualToString:@"propNome"]) {
        [[_sub100.proprietario objectForKey:@"nome"] appendString:string];
    } else if([_element isEqualToString:@"propEmail"]) {
        [[_sub100.proprietario objectForKey:@"email"] appendString:string];
    } else if([_element isEqualToString:@"propTels"]) {
        [[_sub100.proprietario objectForKey:@"telefones"] appendString:string];
    }
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    
    if([elementName isEqualToString:@"proprietario"]) {
        
        [_proprietarios addObject:_sub100.proprietario];
        
    } else if([elementName isEqualToString:@"foto"]) {
        
        if(_foto != nil) {
            _totalPhotos++;
            [_sub100.fotos addObject:_foto.trimmed];
        }
        
        _foto = [NSMutableString string];
        
    } else if([elementName isEqualToString:@"imovel"]) {
        [_imoveis addObject:_sub100];
    }
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    
    [Utils dismissProgress];
    
    //    NSMutableSet *set = [NSMutableSet set];
    //
    //    for(Sub100 *obj in _imoveis) {
    //        [set addObject:obj.operacao];
    //    }
    //
    //    NSLog(@"%@",[set allObjects]);
    //
    //    NSLog(@"Total de registros do xml: %d",[set count]);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        
        [localContext performBlock:^{
            
            [self convert:localContext];
            
            if ([self.imoveis count] == 0) {
                [Utils showMessage:MESSAGE_EMPTY_IMPORT];
                [Utils dismissProgress];
                return;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                _progressView.progress = 0.0f;
                _progressLabel.text = @"";
                
                [Utils postEntitytNotification:@"Contato"];
                [Utils postEntitytNotification:@"Imovel"];
            });
            
            [self startBackgroundDownloadsForBaseURL:localContext];
            
            [Utils dismissProgress];
            [Utils stopNetworkIndicator];
            [Utils postEntitytNotification:@"Imovel"];
        }];
    });
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    NSLog(@"%@", parseError);
    
    [Utils dismissProgress];
    [Utils stopNetworkIndicator];
    
    [Utils showMessage:@"Não foi possível iniciar a importação. Verifique as informações preenchidas e tente novamente.\n Se o erro persistir entre em contato com suporte@brestate.com.br"];
}


#pragma mark - Convert Objects to NSManagedObject
- (void)convert:(NSManagedObjectContext *)moc
{
    
    NSNumberFormatter *currencyFmt = [[NSNumberFormatter alloc] init];
    [currencyFmt setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyFmt setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [currencyFmt setDecimalSeparator:@","];
    [currencyFmt setGeneratesDecimalNumbers:YES];
    [currencyFmt setCurrencySymbol:@"R$ "];
    [currencyFmt setLenient:YES];
    
    int contador = 0;
    
    __block long total = [_imoveis count];
    __block int step = 0;
    __block float progressStep = 0.0f;
    
    
    @autoreleasepool {
        
        for(Sub100 *obj in _imoveis) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                step++;
                progressStep += 1.0/total;
                [_progressView setProgress:progressStep animated:YES];
                _progressLabel.text = [NSString stringWithFormat:@"Importando: %d/%ld",step, total];
            });
            
            long count = 0;
            
            Imovel *imovel;
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",obj.referencia.trimmed];
            
            count = [Imovel MR_countOfEntitiesWithPredicate:predicate inContext:moc];
            
            BOOL exists = NO;
            if(count > 0 ) {
                imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:moc];
                exists = YES;
                imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
                
            } else {
                
                imovel = [Imovel MR_createInContext:moc];
                imovel.codigo = [NSNumber numberWithLongLong:obj.referencia.trimmed.longLongValue];
            }
            
            imovel.descricao = obj.descricao.trimmed;
            imovel.version = [NSDate date];
            
            
            // Tipo Imovel
            NSString *subcategoria = obj.tipo.trimmed;
            
            if([subcategoria containsString:@"Casa"]) {
                imovel.tipo = @"Casa";
            } else if([subcategoria containsString:@"Armazém / Galpão"]) {
                imovel.tipo = @"Armazém / Galpão";
            } else if([subcategoria containsString:@"Chácara"]) {
                imovel.tipo = @"Chácara";
            } else if([subcategoria containsString:@"Loja"]) {
                imovel.tipo = @"Loja";
            } else if([subcategoria containsString:@"Sala / Espaço Comercia"]) {
                imovel.tipo = @"Sala / Espaço Comercial";
            } else if([subcategoria containsString:@"Prédio"]) {
                imovel.tipo = @"Prédio";
            } else if([subcategoria containsString:@"Terreno"]) {
                imovel.tipo = @"Terreno";
            } else if([subcategoria containsString:@"Apartamento"]) {
                imovel.tipo = @"Apartamento";
            } else {
                imovel.tipo = @"Outro";
            }
            
            NSNumber *valor;
            
            // Tipo Oferta
            NSString *oferta = obj.operacao.trimmed;
            
            if([oferta containsString:@"Venda"]) {
                imovel.oferta = @"Venda";
                valor = [currencyFmt numberFromString:obj.valor.trimmed];
            } else if([oferta containsString:@"Locação"]) {
                imovel.oferta = @"Locação";
                valor = [currencyFmt numberFromString:obj.valor.trimmed];
            } else {
                imovel.oferta = @"Outro";
                valor = [currencyFmt numberFromString:obj.valor.trimmed];
            }
            
            // valor
            if ([valor intValue] > 0) {
                imovel.valor = [NSDecimalNumber decimalNumberWithDecimal:[valor decimalValue]];
            }
            
            // area util / area total
            NSNumber *areaTotal = [NSNumber numberWithInteger:obj.areaTotal.trimmed.integerValue];
            if ([areaTotal intValue] > 0) {
                imovel.areaTotal =  areaTotal;
            }
            
            NSNumber *areaUtil = [NSNumber numberWithInteger:obj.areaPrivativa.trimmed.integerValue];
            if ([areaUtil intValue] > 0) {
                imovel.areaUtil =  areaUtil;
            }
            
            
            // quartos
            NSNumber *quartos = [NSNumber numberWithInteger:obj.quartos.trimmed.integerValue];
            
            if ([quartos intValue] > 0) {
                imovel.quartos = quartos;
            }
            
            // Endereco
            Endereco *endereco;
            if (imovel.endereco != nil) {
                endereco = imovel.endereco;
            } else {
                
                endereco = [Endereco MR_createInContext:moc];
                endereco.codigo = [endereco getUniqueCode]; //[NSNumber numberWithLong:codigo];
            }
            
            // latitude / longitude
            endereco.latitude = [NSNumber numberWithDouble:[obj.latitude.trimmed doubleValue]];
            endereco.longitude = [NSNumber numberWithDouble:[obj.longitude.trimmed doubleValue]];
            
            if (obj.uf.trimmed.length > 0) {
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sigla == [cd] %@",obj.uf.trimmed];
                Estado *estado = [Estado MR_findFirstWithPredicate:predicate inContext:moc];
                
                if (estado != nil) {
                    endereco.estado = estado.nome;
                }
                
            }
            
            
            Cidade *cidade;
            if (obj.cidade.trimmed.length > 0) {
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"nome == [cd] %@",obj.cidade.trimmed];
                cidade = [Cidade MR_findFirstWithPredicate:predicate inContext:moc];
                
                if (cidade != nil) {
                    endereco.cidade = cidade.nome;
                }
            }
            
            
            
            if (obj.bairro.trimmed.length > 0) {
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cidade == [cd] %@ AND nome == [cd] %@",cidade.nome.trimmed, obj.bairro.trimmed];
                count = [Bairro MR_countOfEntitiesWithPredicate:predicate inContext:moc];
                
                Bairro *bairro = nil;
                
                if(count == 0 ) {
                    
                    bairro = [Bairro MR_createInContext:moc];
                    bairro.codigo = [bairro getUniqueCode]; //[NSNumber numberWithLong:codigo];
                    bairro.nome = obj.bairro.trimmed;
                    bairro.cidade = cidade.nome;
                    bairro.version = [NSDate date];
                    
                } else {
                    
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cidade == [cd] %@ AND nome == [cd] %@",cidade.nome, obj.bairro.trimmed];
                    bairro = [Bairro MR_findFirstWithPredicate:predicate inContext:moc];
                }
                
                endereco.bairro = bairro;
                
            }
            
            imovel.endereco = endereco;
            
            
            // cliente
            
            NSString *nome = [[obj.proprietario valueForKey:@"nome"] trimmed];
            NSString *email = [[obj.proprietario valueForKey:@"email"] trimmed];
            NSArray *telefones = [[obj.proprietario valueForKey:@"telefones"] componentsSeparatedByString:@","];
            
            predicate = [NSPredicate predicateWithFormat:@"nome = %@",nome];
            count = [Cliente MR_countOfEntitiesWithPredicate:predicate inContext:moc];
            
            Cliente *cliente = nil;
            
            if(count == 0) {
                
                cliente = [Cliente MR_createInContext:moc];
                cliente.codigo = [cliente getUniqueCode]; //[NSNumber numberWithLong:codigo];
                cliente.nome = nome;
                cliente.email = email;
                cliente.telefone = [telefones firstObject];
                cliente.telefoneExtra = [telefones lastObject];
                
                if([imovel.oferta.trimmed containsString:@"Venda"]) {
                    cliente.subtipo = [NSNumber numberWithInt:1];
                } else if([imovel.oferta.trimmed containsString:@"Locação"]) {
                    cliente.subtipo = [NSNumber numberWithInt:2];
                }
                
                cliente.version = [NSDate date];
                
            } else {
                predicate = [NSPredicate predicateWithFormat:@"nome = %@",nome];
                cliente = [Cliente MR_findFirstWithPredicate:predicate inContext:moc];            }
            
            imovel.cliente = cliente;
            
            
            obj.codigoImovel = imovel.codigo;
            
            if (exists) {
                [Utils deleteAllPhotos:imovel withMoc:moc];
            }
            
            contador++;
            
            imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
            [moc MR_saveToPersistentStoreAndWait];
        }
        
    }
    
}



- (void)startBackgroundDownloadsForBaseURL:(NSManagedObjectContext *)moc
{
    
    [Utils startNetworkIndicator];
    
    
    __block int step = 0;
    __block float progressStep = 0.0;
    
    for (Sub100 *sub100 in _imoveis)
    {
        
//        if (_parentController == nil) {
//            break;
//        }
        
        for(NSString *url in sub100.fotos){
            
            NSArray *parts = [url componentsSeparatedByString:@"/"];
            NSString *urlFix = [NSString stringWithFormat:@"http://imagens.sub100.com.br/imov/img/%@",[parts lastObject]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                step++;
                progressStep += 1.0/_totalPhotos;
                [_progressView setProgress:progressStep animated:YES];
                _progressLabel.text = [NSString stringWithFormat:@"Transferindo fotos: %d/%d",step, _totalPhotos];
                
            });
            
            
            NSURL *photoURL = [NSURL URLWithString:urlFix.trimmed];
            
            if (photoURL == nil) {
                continue;
            }
            
            NSError *error = nil;
            NSData *imageData = [NSData dataWithContentsOfURL:photoURL options:kNilOptions error:&error];
            
            if (error) {
                NSLog(@"%@",[error description]);
                continue;
            }
            
            FotoImovel *fotoImovel = [FotoImovel MR_createInContext:moc];
            fotoImovel.codigo = [fotoImovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"imovel.codigo = %@",sub100.codigoImovel];
            long imageCount = [FotoImovel MR_countOfEntitiesWithPredicate:predicate inContext:moc]+1;
            fotoImovel.descricao = [NSString stringWithFormat:@"Foto %ld",imageCount];
            fotoImovel.importacao = [NSNumber numberWithBool:YES];
            
            predicate = [NSPredicate predicateWithFormat:@"codigo = %@",sub100.codigoImovel];
            fotoImovel.imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:moc];
            
            [fotoImovel insertImageOnDisk:imageData];
            
            [moc MR_saveToPersistentStoreAndWait];
        }
        
    }
    
}


@end
