//
//  VistaImporter.h
//  Appci
//
//  Created by BrEstate LTDA on 4/28/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VistaViewController;
@interface VistaImporter : NSObject

@property (nonatomic, weak) UIProgressView *progressView;
@property (nonatomic, weak) UILabel *progressLabel;

@property (nonatomic, weak)  VistaViewController *parentController;

- (void)startWithEmail:(NSString *)email andCpfCnpj:(NSString *)cpfCnpj usingMoc:(NSManagedObjectContext *)moc forced:(BOOL)isForced;
@end
