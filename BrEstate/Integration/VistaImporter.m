//
//  VistaImporter.m
//  Appci
//
//  Created by BrEstate LTDA on 4/28/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "VistaImporter.h"
#import "Imovel.h"
#import "Cliente.h"
#import "TagImovel.h"
#import "Endereco.h"
#import "Estado.h"
#import "Cidade.h"
#import "Bairro.h"
#import "SVProgressHUD.h"
#import "FotoImovel+InsertDeleteImagem.h"
#import "NSString+HTML.h"
#import "NSString+Custom.h"
#import "VistaViewController.h"
#import "ASIHTTPRequest.h"
#import "XMLDictionary.h"

@interface VistaImporter ()

#pragma mark - Properties

@property (nonatomic, strong) NSString *checksum;
@property (nonatomic, strong) NSMutableArray *retryPhotos;
@property (nonatomic, strong) NSDictionary *dictionary;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSMutableArray *imoveis;
@property (nonatomic) int totalPhotos;
@property (nonatomic) BOOL forceSync;
@property (nonatomic, strong) NSString *chavePortal;
@property (nonatomic, strong) NSString *codigoCliente;

@end

@implementation VistaImporter



#pragma mark - Start XML Parsing

- (void)startWithEmail:(NSString *)email andCpfCnpj:(NSString *)cpfCnpj usingMoc:(NSManagedObjectContext *)moc forced:(BOOL)isForced
{
    
    NSString *url = [NSString stringWithFormat:@"%@%@?email=%@&site=%@",[[Config instance] url], @"crm/query-vista.json", email,cpfCnpj];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSNumber *success = [responseObject valueForKey:@"success"];
        
        if ([success boolValue]) {
            
            NSString *strURL = [responseObject valueForKey:@"url"];
            NSNumber *update = [responseObject valueForKey:@"updateDate"];
            _chavePortal = [responseObject valueForKey:@"chavePortal"];
            _codigoCliente = [responseObject valueForKey:@"codigoCliente"];
            
            NSDate *updateDate = [NSDate dateWithTimeIntervalSince1970:[update longLongValue] / 1000];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self startWithURL:strURL andUpdate:updateDate usingMoc:moc forced:isForced];
            });
            
            
        } else {
            
            [Utils dismissProgress];
            NSString *message = [responseObject valueForKey:@"message"];
            [Utils showMessage:message];
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [Utils dismissProgress];
            [Utils showMessage:@"Ocorreu um erro na integração. Se o erro persistir, entre em contato com nosso suporte."];
        });
    }];
    
}

- (void)startWithURL:(NSString *)strURL andUpdate:(NSDate *)updateDate usingMoc:(NSManagedObjectContext *)moc forced:(BOOL)isForced
{
    
    [Utils startNetworkIndicator];
    
    _forceSync = isForced;
    _retryPhotos = [NSMutableArray array];
    _photos = [NSMutableArray array];
    
    NSURL *url = [NSURL URLWithString:strURL.trimmed];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    request.timeOutSeconds = 30;
    [request startSynchronous];
    NSError *error = [request error];
    
    
    if (error) {
        [Utils dismissProgress];
        [Utils showMessage:MESSAGE_IMPORT_ERROR];
        [Utils stopNetworkIndicator];
        return;
    }
    
    
    NSString *xml = request.responseString;
    
    NSString *newChecksum = [xml MD5];
    
    if (!_forceSync) {
        
        // checksum check
        NSString *oldChecksum = [Utils valueFromDefaults:VISTA_CHECKSUM];
        if ([newChecksum isEqualToString:oldChecksum]) {
            
            [Utils dismissProgress];
            [Utils stopNetworkIndicator];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Aviso" message:@"As informações já estão atualizadas, deseja forçar a sincronização?" delegate:_parentController cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Forçar", nil];
                [alertView show];
            });
            
            
            return;
        }
        
    }
    
    _checksum = newChecksum;
    
    
    [Utils stopNetworkIndicator];
    [Utils dismissProgress];
    
    _parentController.hasStarted = YES;
    _dictionary = [NSDictionary dictionaryWithXMLData:request.responseData];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        
        [localContext performBlock:^{
            [self convert:localContext];
            
            if ([self.imoveis count] == 0) {
                [Utils showMessage:MESSAGE_EMPTY_IMPORT];
                [Utils dismissProgress];
                return;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                _progressView.progress = 0.0f;
                _progressLabel.text = @"";
                
            });
            
            // update checksum
            [Utils addObjectToDefaults:_checksum withKey:VISTA_CHECKSUM];
            
            [self startBackgroundDownloadsForBaseURL:localContext];
            
            if (_retryPhotos.count > 0) {
                [self redownloadPhotos:localContext];
            }
            
            [Utils dismissProgress];
            [Utils postEntitytNotification:@"Imovel"];
            [Utils stopNetworkIndicator];
            
            if (_parentController != nil) {
                [Utils showMessage:MESSAGE_FINISH_IMPORT];
            }
            
            [_parentController setHasStarted:NO];
            
        }];
        
    });
    
    
}

#pragma mark - Convert Objects to NSManagedObject
- (void)convert:(NSManagedObjectContext *)moc
{
    
    NSNumberFormatter *currencyFmt = [[NSNumberFormatter alloc] init];
    [currencyFmt setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyFmt setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [currencyFmt setDecimalSeparator:@","];
    [currencyFmt setGeneratesDecimalNumbers:YES];
    [currencyFmt setCurrencySymbol:@"R$ "];
    [currencyFmt setLenient:YES];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"];
    [currencyFmt setLocale:locale];
    
    
    _imoveis = [_dictionary objectForKey:@"Imovel"];
    
    __block long total = [_imoveis count];
    __block int step = 0;
    __block float progressStep = 0.0f;
    
    
    
    for(NSDictionary *obj in _imoveis) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/total;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Importando: %d/%ld",step, total];
        });
        
        @autoreleasepool {
            
            long count = 0;
          //  long codigo;
            
            
            Imovel *imovel;
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",obj[@"CodigoImovel"]];
            
            count = [Imovel MR_countOfEntitiesWithPredicate:predicate inContext:moc];
            
            BOOL exists = NO;
            if(count > 0 ) {
                imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:moc];
                exists = YES;
                imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
            } else {
                
//                codigo = [Imovel MR_countOfEntitiesWithContext:moc]+1;
                imovel = [Imovel MR_createInContext:moc];
                imovel.codigo = [NSNumber numberWithLongLong:[obj[@"CodigoImovel"] longLongValue]];
            }
            
            
            imovel.descricao = obj[@"Descricao"];
            imovel.version = [NSDate date];
            imovel.identificacao = obj[@"CodigoImovel"];
            
            // valor
            NSNumber *valor = nil;
            
            
            // Tipo Oferta
            if([obj[@"Venda"] isEqualToString:@"Sim"]) {
                imovel.oferta = @"Venda";
                valor = [currencyFmt numberFromString:obj[@"PrecoVenda"]];
            } else if([obj[@"Locacao"] isEqualToString:@"Sim"]) {
                imovel.oferta = @"Locação";
                valor = [currencyFmt numberFromString:obj[@"PrecoLocacao"]];
            } else if([obj[@"LocacaoTemporada"] isEqualToString:@"Sim"]) {
                imovel.oferta = @"Temporada";
                valor = [currencyFmt numberFromString:obj[@"PrecoLocacaoTemporada"]];
            } else {
                imovel.oferta = @"Outro";
            }
            
            imovel.valor = [NSDecimalNumber decimalNumberWithDecimal:[valor decimalValue]];
            
            // condominio
            valor = [currencyFmt numberFromString:obj[@"PrecoCondominio"]];
            imovel.condominio = [NSDecimalNumber decimalNumberWithDecimal:valor.decimalValue];
            
            // Tipo Imovel
            NSString *tipo = obj[@"TipoImovel"];
            
            if([tipo isEqualToString:@"Apartamentos"]) {
                imovel.tipo = @"Apartamento";
            } else if([tipo isEqualToString:@"Área"]) {
                imovel.tipo = @"Outro";
            } else if([tipo isEqualToString:@"Box"]) {
                imovel.tipo = @"Outro";
            } else if([tipo isEqualToString:@"Casa"]) {
                imovel.tipo = @"Casa";
            } else if([tipo isEqualToString:@"Casa Comercial"]) {
                imovel.tipo = @"Casa Comercial";
            } else if([tipo isEqualToString:@"Casa em Condominio"]) {
                imovel.tipo = @"Casa";
            } else if([tipo isEqualToString:@"Chacara"]) {
                imovel.tipo = @"Chácara";
            } else if([tipo isEqualToString:@"Coberturas"]) {
                imovel.tipo = @"Coberturas";
            } else if([tipo isEqualToString:@"Depositos"]) {
                imovel.tipo = @"Armazém / Galpão";
            } else if([tipo isEqualToString:@"Empreendimento"]) {
                imovel.tipo = @"Lançamento";
            } else if([tipo isEqualToString:@"Loft"]) {
                imovel.tipo = @"Outro";
            } else if([tipo isEqualToString:@"Loja"]) {
                imovel.tipo = @"Loja";
            } else if([tipo isEqualToString:@"Pavilhão"]) {
                imovel.tipo = @"Armazém / Galpão";
            } else if([tipo isEqualToString:@"Prédio"]) {
                imovel.tipo = @"Prédio";
            } else if([tipo isEqualToString:@"Predio Comercial"]) {
                imovel.tipo = @"Prédio";
            } else if([tipo isEqualToString:@"Salas/Conjuntos"]) {
                imovel.tipo = @"Sala / Espaço Comercial";
            }  else if([tipo isEqualToString:@"Sitio"]) {
                imovel.tipo = @"Fazenda / Sítio";
            }  else if([tipo isEqualToString:@"Sobrado"]) {
                imovel.tipo = @"Sobrado";
            }  else if([tipo isEqualToString:@"Terreno"]) {
                imovel.tipo = @"terreno";
            } else {
                imovel.tipo = @"Outro";
            }
            
            // area util / area total
            
            NSString *area = obj[@"AreaTotal"];
            if (area.length > 0) {
                NSNumber *areaTotal = [NSNumber numberWithLong:[area integerValue]];
                
                if ([areaTotal intValue] > 0) {
                    imovel.areaTotal =  areaTotal;
                }
            }
            
            area = obj[@"AreaUtil"];
            if (area.length > 0) {
                NSNumber *areaUtil = [NSNumber numberWithLong:[area integerValue]];
                
                if ([areaUtil intValue] > 0) {
                    imovel.areaUtil =  areaUtil;
                }
            }
            
            // quartos
            NSNumber *quartos = [NSNumber numberWithLong:[obj[@"QtdDormitorios"] integerValue]];
            
            if ([quartos intValue] > 0) {
                imovel.quartos = quartos;
            }
            
            // tags
            TagImovel *tagImovel;
            
            if (imovel.tags != nil) {
                
                tagImovel = imovel.tags;
                
            } else {
                
                tagImovel = [TagImovel MR_createInContext:moc];
                tagImovel.codigo = [tagImovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
                
            }
            
            if ([obj[@"ArmarioEmbutido"] isEqualToString:@"Sim"]) {
                tagImovel.armarioEmbutido = [NSNumber numberWithBool:YES];
            }
            if ([obj[@"Churrasqueira"] isEqualToString:@"Sim"]) {
                tagImovel.churrasqueira = [NSNumber numberWithBool:YES];
            }
            if ([obj[@"Piscina"] isEqualToString:@"Sim"]) {
                tagImovel.piscina = [NSNumber numberWithBool:YES];
            }
            if ([obj[@"Playground"] isEqualToString:@"Sim"]) {
                tagImovel.playground = [NSNumber numberWithBool:YES];
            }
            if ([obj[@"QuadraPoliEsportiva"] isEqualToString:@"Sim"]) {
                tagImovel.quadraPoliesportiva = [NSNumber numberWithBool:YES];
            }
            if ([obj[@"SalaGinastica"] isEqualToString:@"Sim"]) {
                tagImovel.salaGinastica = [NSNumber numberWithBool:YES];
            }
            if ([obj[@"SalaoFestas"] isEqualToString:@"Sim"]) {
                tagImovel.salaoFestas = [NSNumber numberWithBool:YES];
            }
            if ([obj[@"SalaoJogos"] isEqualToString:@"Sim"]) {
                tagImovel.salaoJogos = [NSNumber numberWithBool:YES];
            }
            if ([obj[@"Sauna"] isEqualToString:@"Sim"]) {
                tagImovel.sauna = [NSNumber numberWithBool:YES];
            }
            if ([obj[@"Varanda"] isEqualToString:@"Sim"]) {
                tagImovel.varanda = [NSNumber numberWithBool:YES];
            }
            if ([obj[@"WCEmpregada"] isEqualToString:@"Sim"]) {
                tagImovel.wcEmpregada = [NSNumber numberWithBool:YES];
            }
            
            imovel.tags = tagImovel;
            
            
            // Endereco
            Endereco *endereco;
            if (imovel.endereco != nil) {
                endereco = imovel.endereco;
            } else {
                
                endereco = [Endereco MR_createInContext:moc];
                endereco.codigo = [endereco getUniqueCode]; //[NSNumber numberWithLong:codigo];
            }
            
            
            double latitude = [obj[@"GMapsLatitude"] doubleValue];
            double longitude = [obj[@"GMapsLongitude"] doubleValue];
            
            endereco.latitude = [NSNumber numberWithDouble:latitude];
            endereco.longitude = [NSNumber numberWithDouble:longitude];
            
            
            // cep
            if ([obj[@"CEP"] length] > 0) {
                NSString *cep = [obj[@"CEP"] stringByReplacingOccurrencesOfString:@"-" withString:@""];
                endereco.cep = [NSNumber numberWithInteger:cep.intValue];
            }
            
            
            predicate = [NSPredicate predicateWithFormat:@"sigla == [cd] %@",obj[@"RS"]];
            Estado *estado = [Estado MR_findFirstWithPredicate:predicate inContext:moc];
            if (estado != nil) {
                endereco.estado = estado.nome;
            }
            
            
            Cidade *cidade;
            if ([obj[@"Cidade"] length] > 0) {
                
                predicate = [NSPredicate predicateWithFormat:@"nome == [cd] %@",obj[@"Cidade"]];
                cidade = [Cidade MR_findFirstWithPredicate:predicate inContext:moc];
                
                if (cidade != nil) {
                    endereco.cidade = cidade.nome;
                }
            }
            
            
            
            if ([obj[@"Bairro"] length] > 0) {
                
                predicate = [NSPredicate predicateWithFormat:@"cidade == [cd] %@ AND nome == [cd] %@",cidade.nome, obj[@"Bairro"]] ;
                count = [Bairro MR_countOfEntitiesWithPredicate:predicate inContext:moc];
                
                Bairro *bairro = nil;
                
                if(count == 0 ) {
                    
                    bairro = [Bairro MR_createInContext:moc];
                    bairro.codigo = [bairro getUniqueCode]; //[NSNumber numberWithLong:codigo];
                    bairro.nome = obj[@"Bairro"];
                    bairro.cidade = cidade.nome;
                    bairro.version = [NSDate date];
                    
                } else {
                    
                    predicate = [NSPredicate predicateWithFormat:@"cidade == [cd] %@ AND nome == [cd] %@",cidade.nome, obj[@"Bairro"]];
                    bairro = [Bairro MR_findFirstWithPredicate:predicate inContext:moc];                }
                
                endereco.bairro = bairro;
                
            }
            
            
            imovel.endereco = endereco;
            
            // fotos
            if (exists) {
                [Utils deleteAllPhotos:imovel withMoc:moc];
            }
            
            NSDictionary *fotos = [[obj objectForKey:@"Fotos"] objectForKey:@"Foto"];
            NSMutableDictionary *newMedia;
            
            for (NSDictionary *photo in fotos) {
                
                if (![photo isKindOfClass:[NSDictionary class]]) {
                    
                    newMedia = [NSMutableDictionary dictionary];
                    [newMedia setValue:fotos[@"URL"] forKey:@"URL"];
                    [newMedia setValue:imovel.codigo forKey:@"codigoImovel"];
                    [_photos addObject:newMedia];
                    _totalPhotos++;
                    break;
                }
                
                newMedia = [NSMutableDictionary dictionaryWithDictionary:photo];
                [newMedia setValue:imovel.codigo forKey:@"codigoImovel"];
                
                
                [_photos addObject:newMedia];
                _totalPhotos++;
                
            }
            
            
            imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
            [moc MR_saveToPersistentStoreAndWait];
            
            
        }
        
    }
    
}




#pragma mark - Photo Download
- (void)startBackgroundDownloadsForBaseURL:(NSManagedObjectContext *)moc
{
    [Utils startNetworkIndicator];
    
    __block int step = 0;
    __block float progressStep = 0.0;
    
    for (NSDictionary *photo in _photos)
    {
        
        if (_parentController == nil) {
            break;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/_totalPhotos;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Transferindo fotos: %d/%d",step, _totalPhotos];
        });
        
        @autoreleasepool {
            [self downloadPhoto:photo usingMoc:moc];
        }
    }
}

- (void)redownloadPhotos:(NSManagedObjectContext *)moc
{
    __block int step = 0;
    __block float progressStep = 0.0;
    
    for(NSDictionary *photo in _retryPhotos){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/_retryPhotos.count;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Reparando fotos : %d/%lu",step, (unsigned long)_retryPhotos.count];
            
        });
        
        @autoreleasepool {
            [self downloadPhoto:photo usingMoc:moc];
        }
    }
}

- (void)downloadPhoto:(NSDictionary *)photo usingMoc:(NSManagedObjectContext *)moc
{
    
    NSString *strURL = [photo[@"URL"] trimmed];
    
    if (strURL.length == 0) {
        return;
    }
    
    NSURL *photoURL = [NSURL URLWithString:strURL];
    
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:photoURL];
    request.timeOutSeconds = 60;
    [request startSynchronous];
    NSError *error = [request error];
    
    if (error) {
        
        [_retryPhotos addObject:photo];
        NSLog(@"%@",[error description]);
    }
    
    FotoImovel *fotoImovel = [FotoImovel MR_createInContext:moc];
    fotoImovel.codigo = [fotoImovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
    
    fotoImovel.descricao = [[strURL lastPathComponent] stringByDeletingPathExtension];
    fotoImovel.importacao = [NSNumber numberWithBool:YES];
    
    NSNumber *codigoImovel = photo[@"codigoImovel"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",codigoImovel];
    fotoImovel.imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:moc];
    
    [fotoImovel insertImageOnDisk:request.responseData];
    
    [moc MR_saveToPersistentStoreAndWait];
}


#pragma mark - Post Vista
- (void)postVista
{
    
    NSArray *imoveisComFotos = [_imoveis filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"hasFotos = YES"]];
    
    NSString *xml =
    @"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
    "<anuncios>"
    "<resumo>"
    "<chave_portal>chave-do-portal</chave_portal>"
    "<codigo_cliente>1</codigo_cliente>"
    "<anuncios_no_arquivo>100</anuncios_no_arquivo>"
    "<anuncios_rejeitados>0</anuncios_rejeitados>"
    "<anuncios_no_portal>100</anuncios_no_portal>"
    "<anuncios_no_portal_com_fotos>80</anuncios_no_portal_com_fotos>"
    "</resumo>"
    "<anuncios_incluidos>";
    
    
    
    xml = [xml stringByReplacingOccurrencesOfString:@"chave-do-portal" withString:_chavePortal];
    xml = [xml stringByReplacingOccurrencesOfString:@"<codigo_cliente>1</codigo_cliente>" withString:[NSString stringWithFormat:@"<codigo_cliente>%@</codigo_cliente>",_codigoCliente]];
    xml = [xml stringByReplacingOccurrencesOfString:@"<anuncios_no_arquivo>100</anuncios_no_arquivo>" withString:[NSString stringWithFormat:@"<anuncios_no_arquivo>%d</anuncios_no_arquivo>", _imoveis.count]];
    xml = [xml stringByReplacingOccurrencesOfString:@"<anuncios_no_portal>100</anuncios_no_portal>" withString:[NSString stringWithFormat:@"<anuncios_no_portal>%d</anuncios_no_portal>", _imoveis.count]];
    xml = [xml stringByReplacingOccurrencesOfString:@"<anuncios_no_portal_com_fotos>80</anuncios_no_portal_com_fotos>" withString:[NSString stringWithFormat:@"<anuncios_no_portal_com_fotos>%d</anuncios_no_portal_com_fotos>", imoveisComFotos.count]];
    
    
    for (NSDictionary *obj in _imoveis) {
        
        NSString *str = @"<imovel>"
        "<codigo>1</codigo>"
        "</imovel>\n";
        
        
        str = [str stringByReplacingOccurrencesOfString:@"<codigo>1</codigo>" withString:[NSString stringWithFormat:@"<codigo>%@</codigo>",obj[@"CodigoImovel"]]];
        
        xml = [xml stringByAppendingString:str];
        
    }
    
    NSString *closeTags =
    @"</anuncios_incluidos>"
    "</anuncios>";
    
    xml = [xml stringByAppendingString:closeTags];
    
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:@"arquivo.xml"];
    
    NSError *error;
    
    [xml writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:&error];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSURL *filePath = [NSURL fileURLWithPath:path];
    
    [manager POST:@"http://portais.vistahosting.com.br/retorno.php" parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:filePath name:@"xml" error:nil];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"Success: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error on VistaImporter postVista - %@", error);
    }];
    
}

@end
