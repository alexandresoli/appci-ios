//
//  WmbImporter.h
//  Appci
//
//  Created by Alexandre Oliveira on 10/2/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WmbViewController;
@interface WmbImporter : NSObject
@property (nonatomic, weak) UIProgressView *progressView;
@property (nonatomic, weak) UILabel *progressLabel;

@property (nonatomic, weak)  WmbViewController *parentController;

- (void)startWithURL:(NSString *)strURL usingMoc:(NSManagedObjectContext *)moc forced:(BOOL)forced;
@end
