//
//  WmbImporter.m
//  Appci
//
//  Created by Alexandre Oliveira on 10/2/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "WmbImporter.h"
#import "Imovel.h"
#import "FotoImovel+InsertDeleteImagem.h"
#import "NSString+Custom.h"
#import "TagImovel.h"
#import "Endereco.h"
#import "Cidade.h"
#import "Bairro.h"
#import "WmbViewController.h"
#import "ASIHTTPRequest.h"
#import "XMLDictionary.h"

@interface WmbImporter ()

#pragma mark - Properties

@property (nonatomic, strong) NSString *checksum;
@property (nonatomic, strong) NSMutableArray *retryPhotos;
@property (nonatomic, strong) NSDictionary *dictionary;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSMutableArray *imoveis;
@property (nonatomic) int totalPhotos;
@property (nonatomic) BOOL forceSync;

@end

@implementation WmbImporter

#pragma mark - Start XML Parsing
- (void)startWithURL:(NSString *)strURL usingMoc:(NSManagedObjectContext *)moc forced:(BOOL)forced
{
    
    [Utils startNetworkIndicator];
    
    _forceSync = forced;
    _retryPhotos = [NSMutableArray array];
    _photos = [NSMutableArray array];
    
    
    NSURL *url = [NSURL URLWithString:strURL.trimmed];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    request.timeOutSeconds = 30;
    [request startSynchronous];
    NSError *error = [request error];
    
    if (error) {
        [Utils dismissProgress];
        [Utils showMessage:MESSAGE_IMPORT_ERROR];
        [Utils stopNetworkIndicator];
        return;
    }
    
    NSString *xml = request.responseString;
    
    NSString *newChecksum = [xml MD5];
    
    if (!_forceSync) {
        
        // checksum check
        NSString *oldChecksum = [Utils valueFromDefaults:WMB_CHECKSUM];
        if ([newChecksum isEqualToString:oldChecksum]) {
            
            [Utils dismissProgress];
            [Utils stopNetworkIndicator];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Aviso" message:@"As informações já estão atualizadas, deseja forçar a sincronização?" delegate:_parentController cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Forçar", nil];
                [alertView show];
            });
            
            
            return;
        }
        
    }
    
    
    _checksum = newChecksum;
    
    [Utils stopNetworkIndicator];
    [Utils dismissProgress];
    
    _parentController.hasStarted = YES;
    _dictionary = [NSDictionary dictionaryWithXMLData:request.responseData];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        
        [localContext performBlock:^{
            
            [self convert:localContext];
            
            if ([self.imoveis count] == 0) {
                [Utils showMessage:MESSAGE_EMPTY_IMPORT];
                [Utils dismissProgress];
                return;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                _progressView.progress = 0.0f;
                _progressLabel.text = @"";
                
            });
            
            // update checksum
            [Utils addObjectToDefaults:_checksum withKey:WMB_CHECKSUM];
            
            [self startBackgroundDownloadsForBaseURL:localContext];
            
            if (_retryPhotos.count > 0) {
                [self redownloadPhotos:localContext];
            }
            
            [Utils dismissProgress];
            [Utils postEntitytNotification:@"Imovel"];
            [Utils stopNetworkIndicator];
            
            if (_parentController != nil) {
                [Utils showMessage:MESSAGE_FINISH_IMPORT];
            }
            
            [_parentController setHasStarted:NO];
            
        }];
        
    });
    
    
}


#pragma mark - Convert Objects to NSManagedObject
- (void)convert:(NSManagedObjectContext *)moc
{
    
    NSNumberFormatter *currencyFmt = [[NSNumberFormatter alloc] init];
    [currencyFmt setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyFmt setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [currencyFmt setDecimalSeparator:@","];
    [currencyFmt setGeneratesDecimalNumbers:YES];
    [currencyFmt setCurrencySymbol:@"R$ "];
    [currencyFmt setLenient:YES];
    
    _imoveis = [[_dictionary objectForKey:@"Imoveis"] objectForKey:@"Imovel"];
    
    
    __block long total = [_imoveis count];
    __block int step = 0;
    __block float progressStep = 0.0f;
    
    
    for(NSDictionary *obj in _imoveis) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/total;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Importando: %d/%ld",step, total];
        });
        
        @autoreleasepool {
            
            long count = 0;
            
            Imovel *imovel;
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"identificacao = %@",obj[@"CodigoImovel"]];
            
            count = [Imovel MR_countOfEntitiesWithPredicate:predicate inContext:moc];
            
            BOOL exists = NO;
            if(count > 0 ) {
                imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:moc];
                exists = YES;
                imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
            } else {
                
                
                imovel = [Imovel MR_createInContext:moc];
                imovel.codigo = [NSNumber numberWithLong:[obj[@"CodigoImovel"] integerValue]];
            }
            
            
            imovel.descricao = obj[@"Observacao"];
            imovel.version = [NSDate date];
            imovel.identificacao = obj[@"CodigoImovel"];
            
            
            // Tipo Oferta
            NSNumber *valor;
            
            if([obj[@"PrecoVenda"] length] > 0) {
                imovel.oferta = @"Venda";
                valor = [currencyFmt numberFromString:obj[@"PrecoVenda"]];
            } else if([obj[@"PrecoLocacao"] length] > 0) {
                imovel.oferta = @"Locação";
                valor = [currencyFmt numberFromString:obj[@"PrecoLocacao"]];
            } else {
                imovel.oferta = @"Outro";
            }
            
            // valor
            if ([valor intValue] > 0) {
                imovel.valor = [NSDecimalNumber decimalNumberWithDecimal:[valor decimalValue]];
            }
            
            
            // Tipo Imovel
            NSString *subtipo = obj[@"SubTipoImovel"];
            if([subtipo containsString:@"Casa de Condomínio"]) {
                imovel.tipo = @"Casa";
            } else if([subtipo containsString:@"Apartamento Padrão"]) {
                imovel.tipo = @"Apartamento";
            } else if([subtipo containsString:@"Galpão"]) {
                imovel.tipo = @"Armazém / Galpão";
            } else if([subtipo containsString:@"Comercial/Industrial"]) {
                imovel.tipo = @"Espaço Industrial";
            } else if([subtipo containsString:@"Chácara"]) {
                imovel.tipo = @"Chácara";
            } else if([subtipo containsString:@"Loteamento"]) {
                imovel.tipo = @"Outro";
            } else if([subtipo containsString:@"Sítio"]) {
                imovel.tipo = @"Fazenda / Sítio";
            } else if([subtipo containsString:@"Terreno"]) {
                imovel.tipo = @"Terreno";
            } else if([subtipo containsString:@"Casa Padrão"]) {
                imovel.tipo = @"Casa";
            } else {
                imovel.tipo = @"Outro";
            }
            
            
            // area util / total
            NSNumber *areaTotal = [NSNumber numberWithLong:[obj[@"AreaTotal"] integerValue]];
            
            if ([areaTotal intValue] > 0) {
                imovel.areaTotal =  areaTotal;
            }
            
            NSNumber *areaUtil = [NSNumber numberWithLong:[obj[@"AreaUtil"] integerValue]];
            
            if ([areaUtil intValue] > 0) {
                imovel.areaUtil =  areaUtil;
            }
            
            // quartos
            NSNumber *quartos = [NSNumber numberWithLong:[obj[@"QtdDormitorios"] integerValue]];
            
            if ([quartos intValue] > 0) {
                imovel.quartos = quartos;
            }
            
            
            // tags
            TagImovel *tagImovel;
            
            if (imovel.tags != nil) {
                tagImovel = imovel.tags;
            } else {
                tagImovel = [TagImovel MR_createInContext:moc];
                tagImovel.codigo = [tagImovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
            }
            
            if([obj[@"QtdVagas"] intValue] > 0) {
                tagImovel.garagem = [NSNumber numberWithBool:YES];
            } else {
                tagImovel.garagem = [NSNumber numberWithBool:NO];
            }
            
            // churrasqueira
            tagImovel.churrasqueira = [NSNumber numberWithBool:[obj[@"Churrasqueira"] boolValue]];
            
            // piscina
            tagImovel.piscina = [NSNumber numberWithBool:[obj[@"InformacoesComplementares"] boolValue]];
            
            // WC empregada
            tagImovel.wcEmpregada = [NSNumber numberWithBool:[obj[@"QuartoWCEmpregada"] boolValue]];
            
            imovel.tags = tagImovel;
            
            
            // Endereco
            Endereco *endereco;
            if (imovel.endereco != nil) {
                endereco = imovel.endereco;
            } else {
                endereco = [Endereco MR_createInContext:moc];
                endereco.codigo = [endereco getUniqueCode]; //[NSNumber numberWithLong:codigo];
            }
            
            
            
            Cidade *cidade;
            if ([obj[@"Cidade"] length] > 0) {
                
                predicate = [NSPredicate predicateWithFormat:@"nome == [cd] %@",obj[@"Cidade"]];
                cidade = [Cidade MR_findFirstWithPredicate:predicate inContext:moc];
                
                if (cidade != nil) {
                    endereco.cidade = cidade.nome;
                }
            }
            
            
            
            if ([obj[@"Bairro"] length] > 0) {
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cidade == [cd] %@ AND nome == [cd] %@",cidade.nome.trimmed, obj[@"Bairro"]];
                count = [Bairro MR_countOfEntitiesWithPredicate:predicate inContext:moc];
                
                Bairro *bairro = nil;
                
                if(count == 0 ) {
                    
                    bairro = [Bairro MR_createInContext:moc];
                    bairro.codigo = [bairro getUniqueCode]; //[NSNumber numberWithLong:codigo];
                    bairro.nome = obj[@"Bairro"];
                    bairro.cidade = cidade.nome;
                    bairro.version = [NSDate date];
                    
                } else {
                    
                    predicate = [NSPredicate predicateWithFormat:@"cidade == [cd] %@ AND nome == [cd] %@",cidade.nome, obj[@"Bairro"]];
                    bairro = [Bairro MR_findFirstWithPredicate:predicate inContext:moc];
                }
                
                endereco.bairro = bairro;
                
            }
            
            
            imovel.endereco = endereco;
            
            // fotos
            if (exists) {
                [Utils deleteAllPhotos:imovel withMoc:moc];
            }
            
            NSDictionary *fotos = [[obj objectForKey:@"Fotos"] objectForKey:@"Foto"];
            NSMutableDictionary *newMedia;
            
            for (NSDictionary *photo in fotos) {
                
                if (![photo isKindOfClass:[NSDictionary class]]) {
                    
                    newMedia = [NSMutableDictionary dictionary];
                    [newMedia setValue:fotos[@"URLArquivo"] forKey:@"Arquivo"];
                    [newMedia setValue:imovel.codigo forKey:@"codigoImovel"];
                    [newMedia setValue:[NSNumber numberWithBool:YES] forKey:@"Principal"];
                    [_photos addObject:newMedia];
                    _totalPhotos++;
                    break;
                }
                
                newMedia = [NSMutableDictionary dictionaryWithDictionary:photo];
                [newMedia setValue:imovel.codigo forKey:@"codigoImovel"];
                
                [_photos addObject:newMedia];
                _totalPhotos++;
                
            }
            
            imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
            [moc MR_saveToPersistentStoreAndWait];
        }
        
    }
    
}



#pragma mark - Photo Download
- (void)startBackgroundDownloadsForBaseURL:(NSManagedObjectContext *)moc
{
    [Utils startNetworkIndicator];
    
    __block int step = 0;
    __block float progressStep = 0.0;
    
    for (NSDictionary *photo in _photos)
    {
        
        if (_parentController == nil) {
            break;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/_totalPhotos;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Transferindo fotos: %d/%d",step, _totalPhotos];
        });
        
        @autoreleasepool {
            [self downloadPhoto:photo usingMoc:moc];
        }
    }
}

- (void)redownloadPhotos:(NSManagedObjectContext *)moc
{
    __block int step = 0;
    __block float progressStep = 0.0;
    
    for(NSDictionary *photo in _retryPhotos){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/_retryPhotos.count;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Reparando fotos : %d/%lu",step, (unsigned long)_retryPhotos.count];
            
        });
        
        @autoreleasepool {
            [self downloadPhoto:photo usingMoc:moc];
        }
    }
}

- (void)downloadPhoto:(NSDictionary *)photo usingMoc:(NSManagedObjectContext *)moc
{
    
    NSString *strURL = [photo[@"URLArquivo"] trimmed];
    
    if (strURL.length == 0) {
        return;
    }
    
    NSURL *photoURL = [NSURL URLWithString:strURL];
    
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:photoURL];
    request.timeOutSeconds = 60;
    [request startSynchronous];
    NSError *error = [request error];
    
    if (error) {
        
        [_retryPhotos addObject:photo];
        NSLog(@"%@",[error description]);
    }
    
    FotoImovel *fotoImovel = [FotoImovel MR_createInContext:moc];
    fotoImovel.codigo = [fotoImovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
    
    fotoImovel.descricao = [[strURL lastPathComponent] stringByDeletingPathExtension];
    fotoImovel.importacao = [NSNumber numberWithBool:YES];
    
    NSNumber *codigoImovel = photo[@"codigoImovel"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",codigoImovel];
    fotoImovel.imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:moc];
    
    fotoImovel.capa = [NSNumber numberWithBool:[photo[@"Principal"] boolValue]];
    
    [fotoImovel insertImageOnDisk:request.responseData];
    
    [moc MR_saveToPersistentStoreAndWait];
}


@end
