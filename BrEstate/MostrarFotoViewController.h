//
//  MostrarFotoViewController.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/14/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Imovel,FotoImovel;

@interface MostrarFotoViewController : BaseViewController

@property (nonatomic, strong) Imovel *selectedImovel;
@property (nonatomic, strong) FotoImovel *selectedFoto;
@end
