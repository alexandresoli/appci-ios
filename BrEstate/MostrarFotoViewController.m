//
//  MostrarFotoViewController.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/14/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//


#import "MostrarFotoViewController.h"
#import "FotoImovel+InsertDeleteImagem.h"

@interface MostrarFotoViewController () <UIScrollViewDelegate,UIGestureRecognizerDelegate>

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) IBOutlet UIScrollView *picScrollView;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;
@property (nonatomic, weak) IBOutlet UILabel *label;

@property (nonatomic, strong) NSArray *pageImages;
@property (nonatomic, strong) NSMutableArray *pageViews;
@property (nonatomic, strong) NSManagedObjectContext *moc;

@property BOOL isFullScreen;
@property CGRect prevFrame;

@end

@implementation MostrarFotoViewController


#pragma mark - View Life Cycle

- (BOOL)prefersStatusBarHidden
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        return YES;
    } else {
        return NO;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addTapGesture];
    
    // Set up the images we want to scroll & zoom and add it to the scroll view
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(video = nil or video = NO) and imovel = %@",self.selectedImovel];
    self.pageImages = [FotoImovel MR_findAllSortedBy:@"version" ascending:YES withPredicate:predicate inContext:self.moc];

    NSInteger pageCount = self.pageImages.count;
    
    
    self.pageControl.currentPage = 0;
    
    // total number of pages
    self.pageControl.numberOfPages = pageCount;
    
    // Set up the array to hold the views for each page
    self.pageViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < pageCount; ++i) {
        [self.pageViews addObject:[NSNull null]];
    }
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Set up the content size of the scroll view
    CGSize pagesScrollViewSize = self.scrollView.frame.size;
    
    self.scrollView.contentSize = CGSizeMake(pagesScrollViewSize.width * self.pageImages.count, pagesScrollViewSize.height);
    
    // Load the initial set of pages that are on screen
    [self loadVisiblePages];
    
    
    // select the proper image
    [self gotoPage];
    
    //    self.scrollView.minimumZoomScale = 1.0f;
    //    self.scrollView.maximumZoomScale = 2.0f;
    //    self.scrollView.zoomScale = 1.0f;
    
    [Utils trackWithName:@"Mostrar Foto"];
    [Utils trackFlurryWithName:@"Mostrar Foto"];

    
}

-(BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [Flurry endTimedEvent:@"Mostrar Foto" withParameters:nil];
}

- (void)loadVisiblePages
{
    // First, determine which page is currently visible
    NSInteger page = self.scrollView.contentOffset.x / self.scrollView.frame.size.width;
    
    
    // Update the page control with the selected photo index
    self.pageControl.currentPage = page;
    
    // Work out which pages we want to load
    NSInteger firstPage = page - 1;
    NSInteger lastPage = page + 1;
    
    
    // Purge anything before the first page
    for (NSInteger i=0; i<firstPage; i++) {
        [self purgePage:i];
    }
    
	// Load pages in our range
    for (NSInteger i=firstPage; i<=lastPage; i++) {
        [self loadPage:i];
    }
    
	// Purge anything after the last page
    for (NSInteger i=lastPage+1; i<self.pageImages.count; i++) {
        [self purgePage:i];
    }
}


- (void)loadPage:(NSInteger)page
{
    
    // If it's outside the range of what we have to display, then do nothing
    if (page < 0 || page >= self.pageImages.count) {
        return;
    }
    
    // Load an individual page, first seeing if we've already loaded it
    UIView *pageView = [self.pageViews objectAtIndex:page];
    
    if ((NSNull*)pageView == [NSNull null]) {
        
        CGRect frame = self.scrollView.bounds;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 1.0f;
        
        FotoImovel *fotoImovel = [self.pageImages objectAtIndex:page];
        
        UIImage *image = [fotoImovel getImageForFotoImovelURL];
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.frame = frame;
        [self.scrollView addSubview:imageView];
        
        [self.pageViews replaceObjectAtIndex:page withObject:imageView];\
        
    }
}

- (void)purgePage:(NSInteger)page {
    if (page < 0 || page >= self.pageImages.count) {
        // If it's outside the range of what you have to display, then do nothing
        return;
    }
    
    // Remove a page from the scroll view and reset the container array
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView != [NSNull null]) {
        [pageView removeFromSuperview];
        [self.pageViews replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}


- (void)gotoPage
{
    // update the scroll view to the appropriate page
    CGRect bounds = self.scrollView.bounds;

    long index = [self.pageImages indexOfObject:self.selectedFoto];
    
    bounds.origin.x = CGRectGetWidth(bounds) * index;
    [self.scrollView scrollRectToVisible:bounds animated:YES];
}



#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // Load the pages which are now on screen
    [self loadVisiblePages];
}

//- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
//{
//
//    long currentPage = self.pageControl.currentPage;
//    UIImageView *imageView = [self.pageViews objectAtIndex:currentPage];
//
//    return imageView;
//}




#pragma mark - Gestures
- (void)addTapGesture
{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapGesture.numberOfTapsRequired = 2;
    tapGesture.cancelsTouchesInView = NO;
    self.scrollView.userInteractionEnabled = YES;
    [self.scrollView addGestureRecognizer:tapGesture];
}


#pragma mark - Full Screen
-(void)handleTap:(UIGestureRecognizer *)sender
{
    
    if (!self.isFullScreen) {
        [self makeFull];
    } else {
        [self makeSmall];
    }
    
}

- (void)addImageGestures:(UIImageView *)imageView
{
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scalePiece:)];
    [pinchGesture setDelegate:self];
    [imageView addGestureRecognizer:pinchGesture];;
}

- (void)scalePiece:(UIPinchGestureRecognizer *)gestureRecognizer
{
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
        [gestureRecognizer view].transform = CGAffineTransformScale([[gestureRecognizer view] transform], [gestureRecognizer scale], [gestureRecognizer scale]);
        [gestureRecognizer setScale:1.0F];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    // if the gesture recognizers are on different views, don't allow simultaneous recognition
    if (gestureRecognizer.view != otherGestureRecognizer.view)
        return NO;
    
    // if either of the gesture recognizers is the long press, don't allow simultaneous recognition
    if ([gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]] || [otherGestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]])
        return NO;
    
    return YES;
}

// enable full screen
- (void)makeFull
{
    [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
        
        self.navigationController.navigationBarHidden = YES;
        self.pageControl.hidden = YES;
        self.prevFrame = self.scrollView.frame;
        self.scrollView.frame = self.view.bounds;
        [self changeImageSize];
        
    }completion:^(BOOL finished){
        self.isFullScreen = YES;
    }];
    
}

// disable full screen
- (void)makeSmall {
    
    [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
        
        self.navigationController.navigationBarHidden = NO;
        self.pageControl.hidden = NO;
        self.scrollView.frame = self.prevFrame;
        [self changeImageSize];
        
    }completion:^(BOOL finished){
        self.isFullScreen = NO;
    }];
}

// update all images to new size
- (void)changeImageSize
{
    int page = 0;
    
    for (UIImageView *imageView in self.pageViews) {
        
        if ((NSNull*)imageView != [NSNull null]) {
            CGRect frame = self.scrollView.bounds;
            frame.origin.x = frame.size.width * page;
            frame.origin.y = 1.0f;
            
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            imageView.frame = frame;
        }
        page++;
    }
}
@end