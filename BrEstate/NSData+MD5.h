//
//  NSData+MD5.h
//  Appci
//
//  Created by Alexandre Oliveira on 11/21/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSData (MD5)

- (NSString *)MD5;


@end
