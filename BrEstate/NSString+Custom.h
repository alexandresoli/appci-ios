//
//  NSString+Contains.h
//  Appci
//
//  Created by BrEstate LTDA on 6/26/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Custom)

- (BOOL) containsString: (NSString *) substring;
- (NSString *)trimmed;  
- (NSString *)MD5;
;
@end
