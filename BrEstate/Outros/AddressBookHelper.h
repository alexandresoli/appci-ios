//
//  ContactDatabaseHelper.h
//  BrEstate
//
//  Created by BrEstate LTDA on 1/10/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Cliente;

@interface AddressBookHelper : NSObject

- (id)initWithMoc:(NSManagedObjectContext *)moc;

- (void)addContact:(Cliente *)cliente;
- (void)checkContactsShowReturnMessage:(BOOL)showMessage;

@end
