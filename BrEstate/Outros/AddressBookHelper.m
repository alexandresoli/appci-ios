//
//  ContactDatabaseHelper.m
//  BrEstate
//
//  Created by BrEstate LTDA on 1/10/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "AddressBookHelper.h"

@import AddressBook;

#import "Endereco.h"
#import "Estado.h"
#import "Cidade.h"
#import "AlertaUtils.h"
#import "NSString+Custom.h"
#import "ClienteDeletado.h"
#import "Cliente.h"

@interface AddressBookHelper ()

@property (nonatomic, strong) NSManagedObjectContext *moc;

@end


@implementation AddressBookHelper


#pragma mark - Init

- (id)initWithMoc:(NSManagedObjectContext *)moc
{
    self = [super init];
    
    if (self) {
        self.moc = moc;
    }
    
    return self;
}

void MyAddressBookExternalChangeCallback (ABAddressBookRef ntificationaddressbook,CFDictionaryRef info,void *context)
{
    
    AddressBookHelper *helper = [[AddressBookHelper alloc] init];
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        [helper importAllContacts:localContext];
    } completion:^(BOOL success, NSError *error) {
        [Utils postEntitytNotification:@"Contato"];
    }];
    
}


#pragma mark - Add Contact

- (void)saveContact:(Cliente *)cliente
{
    CFErrorRef error = NULL;
    
    ABAddressBookRef addressBook =  ABAddressBookCreateWithOptions(NULL, NULL);
    
    ABRecordRef newPerson = ABPersonCreate();
    
    
    // Name
    ABRecordSetValue(newPerson, kABPersonFirstNameProperty, (__bridge CFTypeRef)(cliente.nome), &error);
    
    
    // Phone
    if (cliente.telefone != nil) {
        ABMutableMultiValueRef multiPhone =  ABMultiValueCreateMutable(kABMultiStringPropertyType);
        ABMultiValueAddValueAndLabel(multiPhone, (__bridge CFTypeRef)(cliente.telefone), kABPersonPhoneMainLabel, NULL);
        
        if (cliente.telefoneExtra != nil) {
            ABMultiValueAddValueAndLabel(multiPhone, (__bridge CFTypeRef)(cliente.telefoneExtra), kABOtherLabel, NULL);
        }
        
        ABRecordSetValue(newPerson, kABPersonPhoneProperty, multiPhone,nil);
        CFRelease(multiPhone);
    }
    
    
    
    // Email
    if (cliente.email != nil) {
        ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        ABMultiValueAddValueAndLabel(multiEmail, (__bridge CFTypeRef)(cliente.email), kABWorkLabel, NULL);
        ABRecordSetValue(newPerson, kABPersonEmailProperty, multiEmail, &error);
        CFRelease(multiEmail);
    }
    
    // Address
    Endereco *endereco = cliente.endereco;
    
    if (endereco.logradouro != nil) {
        
        ABMutableMultiValueRef multiAddress = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
        NSMutableDictionary *addressDictionary = [[NSMutableDictionary alloc] init];
        [addressDictionary setObject:endereco.logradouro forKey:(NSString *) kABPersonAddressStreetKey];
        
        if (endereco.cidade != nil) {
            [addressDictionary setObject:endereco.cidade forKey:(NSString *)kABPersonAddressCityKey];
        }
        
        
        if (endereco.estado != nil) {
            [addressDictionary setObject:endereco.estado forKey:(NSString *)kABPersonAddressStateKey];
        }
        
        if ([endereco.cep intValue] > 0) {
            NSMutableString *cep = [NSMutableString stringWithFormat:@"%@",endereco.cep];
            [cep insertString:@"-" atIndex:5];
            [addressDictionary setObject:cep forKey:(NSString *)kABPersonAddressZIPKey];
        }
        
        ABMultiValueAddValueAndLabel(multiAddress, (__bridge CFTypeRef)(addressDictionary), kABWorkLabel, NULL);
        ABRecordSetValue(newPerson, kABPersonAddressProperty, multiAddress,&error);
        CFRelease(multiAddress);
        
    }
    
    
    // Save
    ABAddressBookAddRecord(addressBook, newPerson, &error);
    
    ABAddressBookSave(addressBook, &error);
    CFRelease(newPerson);
    
    if (addressBook != nil) {
        CFRelease(addressBook);
    }
    
    if (error != NULL)
    {
        CFStringRef errorDesc = CFErrorCopyDescription(error);
        NSLog(@"Contact not saved: %@", errorDesc);
        CFRelease(errorDesc);
    }
}


- (void)addContact:(Cliente *)cliente;
{
    
    // Request authorization to Address Book
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
            // First time access has been granted
            
            if (granted) {
                [self saveContact:cliente];
            }
            
        });
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        // The user has previously given access
        [self saveContact:cliente];
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied) {
        
        [AlertaUtils addAlertNoAccessContacts:self.moc];
        [Utils postEntitytNotification:@"Alerta"];
    }
    
    if (addressBookRef != nil) {
        CFRelease(addressBookRef);
    }

    
}


#pragma mark - iOS Contact Management

- (void)checkContactsShowReturnMessage:(BOOL)showMessage
{
    // Request authorization to Address Book
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
            // First time access has been granted
            if (granted) {
                
                [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                    [self importAllContacts:localContext];
                    
                } completion:^(BOOL success, NSError *error) {
                    [Utils postEntitytNotification:@"Contato"];
                    
                    if (showMessage) {
                        [Utils dismissProgress];
                        [Utils showMessage:@"Seus contatos foram importados para o AppCi."];
                        
                        NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
                        [AlertaUtils removeAlertNoAccessContacts:moc];
                    }
                }];
                
            } else {
                if (showMessage) {
                    [Utils dismissProgress];
                    [Utils showMessage:@"Você precisa autorizar o acesso aos seus contatos.\n Para isso siga os passos: \n\n- Saia do AppCi. \n- Abra o Ajustes do dispositivo. \n- Selecione Privacidade. \n- Selecione Contatos. \n- Altere a opção Ativado/Desativado ao lado do AppCi para Ativado."];
                }
            }
        });
        
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        // The user has previously given access
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            [self importAllContacts:localContext];
            
        } completion:^(BOOL success, NSError *error) {
            [Utils postEntitytNotification:@"Contato"];
            
            if (showMessage) {
                [Utils dismissProgress];
                [Utils showMessage:@"Seus contatos foram importados para o AppCi."];
                
                NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
                [AlertaUtils removeAlertNoAccessContacts:moc];
            }
        }];
        
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied) {
        NSManagedObjectContext *moc = [NSManagedObjectContext MR_contextForCurrentThread];
        [AlertaUtils addAlertNoAccessContacts:moc];
        
        if (showMessage) {
            [Utils dismissProgress];
            [Utils showMessage:@"Você precisa autorizar o acesso aos seus contatos.\n Para isso siga os passos: \n\n- Saia do AppCi. \n- Abra o Ajustes do dispositivo. \n- Selecione Privacidade. \n- Selecione Contatos. \n- Altere a opção Ativado/Desativado ao lado do AppCi para Ativado."];
        }
    }
    
    if (addressBookRef != nil) {
        CFRelease(addressBookRef);    
    }
}


#pragma mark - Check Duplicate

- (BOOL)isDuplicated:(ABRecordRef)record
{
    // name
    NSString *firstName = (__bridge_transfer NSString *)ABRecordCopyValue(record, kABPersonFirstNameProperty);
    NSString *lastName = (__bridge_transfer NSString *)ABRecordCopyValue(record, kABPersonLastNameProperty);
    NSString *fullName;
    
    
    if (lastName != nil) {
        fullName = [NSString stringWithFormat:@"%@ %@", [self trim:firstName], [self trim:lastName]];
    } else {
        fullName = [self trim:firstName];
    }
    
    
    // email
    NSMutableArray *emails = [NSMutableArray array];
    
    ABMultiValueRef multiEmail = ABRecordCopyValue(record, kABPersonEmailProperty);
    
    NSArray *contactEmails = (__bridge_transfer NSArray *)ABMultiValueCopyArrayOfAllValues(multiEmail);
    
    for (NSString *email in contactEmails) {
        [emails addObject:email];

    }
    
    // release if nil
    if (multiEmail != nil) {
        CFRelease(multiEmail);        
    }
    
    
    NSMutableArray *predicateArray = [NSMutableArray array];
    NSPredicate *predicate = nil;
    
    // name
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"nome = %@", fullName]];
    
    // email
    if (emails.count == 0) {
        predicate = [NSPredicate predicateWithFormat:@"email = nil or email = ''"];
    } else {
        predicate = [NSPredicate predicateWithFormat:@"email in %@",emails];
    }
    
    [predicateArray addObject:predicate];
    
    NSPredicate *coumpoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    
    NSArray *resultArray = [Cliente MR_findAllWithPredicate:coumpoundPredicate];
    

//    NSPredicate *predicate;
//    
//    if (emails.count > 0) {
//        predicate = [NSPredicate predicateWithFormat:@"nome ==[c] %@ AND email in %@",fullName,emails];
//    } else {
//        predicate = [NSPredicate predicateWithFormat:@"nome ==[c] %@",fullName];
//    }
//    
//    // check if exists
//    NSArray *resultArray = [contactArray filteredArrayUsingPredicate:predicate];
    
    return (resultArray.count > 0);
    
}


#pragma mark - Check Deleted

- (BOOL)isDeleted:(ABRecordRef)record
{
    // name
    NSString *firstName = (__bridge_transfer NSString *)ABRecordCopyValue(record, kABPersonFirstNameProperty);
    NSString *lastName = (__bridge_transfer NSString *)ABRecordCopyValue(record, kABPersonLastNameProperty);
    NSString *fullName;
    
    
    if (lastName != nil) {
        fullName = [NSString stringWithFormat:@"%@ %@", [self trim:firstName], [self trim:lastName]];
    } else {
        fullName = [self trim:firstName];
    }
    
    // email
    NSMutableArray *emails = [NSMutableArray array];
    
    ABMultiValueRef multiEmail = ABRecordCopyValue(record, kABPersonEmailProperty);
    
    NSArray *contactEmails = (__bridge_transfer NSArray *)ABMultiValueCopyArrayOfAllValues(multiEmail);
    
    for (NSString *email in contactEmails) {
        [emails addObject:email];
    }
    
    // release if nil
    if (multiEmail != nil) {
        CFRelease(multiEmail);
    }
    
    NSMutableArray *predicateArray = [NSMutableArray array];
    NSPredicate *predicate = nil;
    
    // name
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"nome = %@", fullName]];
    
    // email
    if (emails.count == 0) {
        predicate = [NSPredicate predicateWithFormat:@"email = nil or email = ''"];
    } else {
        predicate = [NSPredicate predicateWithFormat:@"email in %@",emails];
    }
    
    [predicateArray addObject:predicate];
    
    NSPredicate *coumpoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    
    NSArray *resultArray = [ClienteDeletado MR_findAllWithPredicate:coumpoundPredicate];
    
//    NSPredicate *predicate;
//    
//    if (emails.count > 0) {
//        predicate = [NSPredicate predicateWithFormat:@"nome ==[c] %@ AND email in %@",fullName,emails];
//    } else {
//        predicate = [NSPredicate predicateWithFormat:@"nome ==[c] %@",fullName];
//    }
//    
//    // check if exists like deleted
//    NSArray *resultArray = [ClienteDeletado MR_findAllWithPredicate:predicate inContext:self.moc];;
    
    return (resultArray.count > 0);
}


#pragma mark - Import Contatcts

-(void)importAllContacts:(NSManagedObjectContext *)localContext
{
        
    CFErrorRef *error = nil;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
    NSArray *contacts =  (__bridge_transfer NSArray *) ABAddressBookCopyArrayOfAllPeople(addressBook);
    
    CFIndex total = ABAddressBookGetPersonCount(addressBook);
    
    
    for (CFIndex i = 0; i < total; i++)
    {
        // get contact representation
        ABRecordRef record = CFArrayGetValueAtIndex((__bridge CFArrayRef)(contacts), i);
        
        NSString *firstName = (__bridge_transfer NSString *)ABRecordCopyValue(record, kABPersonFirstNameProperty);
        NSString *lastName = (__bridge_transfer NSString *)ABRecordCopyValue(record, kABPersonLastNameProperty);
        
        
        // skip empty contact
        if (firstName.length == 0) {
            continue;
        }
        
        // avoid duplicate entries
        if ([self isDuplicated:record]) {
            continue;
        }
        
        // don't import if is deleted on AppCi previously
        if ([self isDeleted:record]) {
            continue;
        }
        
        // codigo
        Cliente *cliente = [Cliente MR_createInContext:localContext];
        cliente.codigo = [cliente getUniqueCode]; //[NSNumber numberWithLong:codigo];
        
        // name
        if (lastName != nil) {
            cliente.nome = [NSString stringWithFormat:@"%@ %@", [self trim:firstName], [self trim:lastName]];
        } else {
            cliente.nome = [self trim:firstName];
        }
        
        // email
        ABMultiValueRef multiEmail = ABRecordCopyValue(record, kABPersonEmailProperty);
        
        NSArray *emails = (__bridge_transfer NSArray *)ABMultiValueCopyArrayOfAllValues(multiEmail);
        
        for (NSString *email in emails) {
            cliente.email = email;
        }
        
        if (multiEmail != nil) {
            CFRelease(multiEmail);
        }
        
        // photo
        cliente.foto = (__bridge_transfer NSData *)ABPersonCopyImageDataWithFormat(record, kABPersonImageFormatThumbnail);
        
        // address
        ABMultiValueRef multiAddress = ABRecordCopyValue(record, kABPersonAddressProperty);
        NSArray *addresses = (__bridge_transfer NSArray *)ABMultiValueCopyArrayOfAllValues(multiAddress);
        
        for (NSDictionary *addressRef in addresses) {
            
            if ( ( [addressRef objectForKey:@"ZIP"]     && ![[addressRef objectForKey:@"ZIP"] isEqualToString:@""]      ) ||
                 ( [addressRef objectForKey:@"Street"]  && ![[addressRef objectForKey:@"Street"] isEqualToString:@""]   ) ||
                 ( [addressRef objectForKey:@"State"]   && ![[addressRef objectForKey:@"State"] isEqualToString:@""]    ) ||
                 ( [addressRef objectForKey:@"City"]    && ![[addressRef objectForKey:@"City"] isEqualToString:@""]     ) ) {
                
                Endereco *endereco = [Endereco MR_createInContext:localContext];
                
                endereco.codigo = [endereco getUniqueCode]; //[NSNumber numberWithLong:codigo];
                
                NSString *cep = [[addressRef objectForKey:@"ZIP"] stringByReplacingOccurrencesOfString:@"-" withString:@""];
                
                if (cep != nil) {
                    endereco.cep = [NSNumber numberWithInt:[cep intValue]];
                }
                
                // Logradouro
                endereco.logradouro = [addressRef objectForKey:@"Street"];
                
                // Estado
                NSString *estadoAddress = [addressRef objectForKey:@"State"];
                NSPredicate *predicate;
                
                if (estadoAddress.trimmed.length == 2) {
                    predicate = [NSPredicate predicateWithFormat:@"sigla ==[c] %@", estadoAddress.trimmed];
                    
                } else {
                    predicate = [NSPredicate predicateWithFormat:@"nome ==[c] %@", estadoAddress.trimmed];
                }
                
                Estado *estado = [Estado MR_findFirstWithPredicate:predicate inContext:localContext];
                endereco.estado = estado.nome;
                
                // Cidade
                predicate = [NSPredicate predicateWithFormat:@"estado == %@ AND nome ==[c] %@ ",estado.nome, [addressRef objectForKey:@"City"]];
                Cidade *cidade = [Cidade MR_findFirstWithPredicate:predicate inContext:localContext];
                endereco.cidade = cidade.nome;
                
                
                cliente.endereco = endereco;
            }
        }
        
        
        if (multiAddress != nil) {
            CFRelease(multiAddress);
        }
        
        // Phone Numbers
        ABMultiValueRef multiPhone = ABRecordCopyValue(record, kABPersonPhoneProperty);
        NSArray *phones = (__bridge_transfer NSArray *)ABMultiValueCopyArrayOfAllValues(multiPhone);
        
        BOOL extraPhone = NO;
        for (NSString *phone in phones) {
            
            if (extraPhone) {
                cliente.telefoneExtra = phone;
            } else {
                cliente.telefone = phone;
                
                // next phone will be a extra one
                extraPhone = YES;
            }
            
        }
        
        if (multiPhone != nil) {
            CFRelease(multiPhone);
        }
        
    }
    
    if (addressBook != nil) {
        CFRelease(addressBook);
    }

}


- (NSString *)trim:(NSString *)string {
    NSInteger i = 0;
    
    while ((i < [string length])
           && [[NSCharacterSet whitespaceCharacterSet] characterIsMember:[string characterAtIndex:i]]) {
        i++;
    }
    return [string substringFromIndex:i];
}


@end
