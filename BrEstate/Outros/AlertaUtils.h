//
//  AlertaUtils.h
//  Appci
//
//  Created by BrEstate LTDA on 25/04/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlertaUtils : NSObject

+ (void)checkIncompleteContacts:(NSManagedObjectContext *)moc;
+ (void)addAlertNoAccessContacts:(NSManagedObjectContext *)moc;
+ (void)removeAlertIncompleteContacts:(NSManagedObjectContext *)moc;
+ (void)removeAlertNoAccessContacts:(NSManagedObjectContext *)moc;

@end
