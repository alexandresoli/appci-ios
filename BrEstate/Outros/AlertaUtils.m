//
//  AlertaUtils.m
//  Appci
//
//  Created by BrEstate LTDA on 25/04/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "AlertaUtils.h"
#import "Alerta.h"
#import "Cliente.h"

@implementation AlertaUtils

#pragma mark - Check for incomplete contacts

+ (void)checkIncompleteContacts:(NSManagedObjectContext *)moc
{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"subtipo = nil OR subtipo = 0"];
    long count = [Cliente MR_countOfEntitiesWithPredicate:predicate inContext:moc];
    [AlertaUtils removeAlertIncompleteContacts:moc];
    
    if (count > 0) {
        [AlertaUtils addAlertIncompleteContacts:moc];
    }
    
}


#pragma mark - Remove Alert Incomplete Contacts

+ (void)removeAlertIncompleteContacts:(NSManagedObjectContext *)moc
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo == 2"];
    Alerta *alerta = [Alerta MR_findFirstWithPredicate:predicate inContext:moc];
    
    if (alerta != nil) {
        
        [alerta MR_deleteInContext:moc];
        
        [Utils saveMOC:moc];
    }
}


#pragma mark - Add Alert Incomplete Contacts

+ (void)addAlertIncompleteContacts:(NSManagedObjectContext *)moc
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = 2"];
    long count = [Alerta MR_countOfEntitiesWithPredicate:predicate inContext:moc];
    
    if (count == 0) {
        // add a new alert about contact completion
        Alerta *alerta = [Alerta MR_createInContext:moc];
        alerta.codigo = [NSNumber numberWithInt:2];
        alerta.tipo = @"contato-incompleto";
        alerta.ativo = [NSNumber numberWithBool:YES];
        
        alerta.descricao = @"Existem contatos com cadastro incompleto";
        
        [Utils saveMOC:moc];
        
    }
}

#pragma mark - Add Alert No Access on Contacts

+ (void)addAlertNoAccessContacts:(NSManagedObjectContext *)moc
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo == 1"];
    long count = [Alerta MR_countOfEntitiesWithPredicate:predicate inContext:moc];
    
    if (count == 0) {
        // add a new alert about contact permissions
        Alerta *alerta = [Alerta MR_createInContext:moc];
        alerta.codigo = [NSNumber numberWithInt:1];
        alerta.tipo = @"permissao-contatos";
        alerta.ativo = [NSNumber numberWithBool:YES];
        
        NSString *message =  @"Acesso aos contatos";
        
        alerta.descricao = message;
        [Utils saveMOC:moc];
        [Utils postEntitytNotification:@"Alerta"];
        
    }
}


#pragma mark - Remove Alert No Access on Contacts

+ (void)removeAlertNoAccessContacts:(NSManagedObjectContext *)moc
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo == 1"];
    Alerta *alerta = [Alerta MR_findFirstWithPredicate:predicate inContext:moc];
    
    if (alerta != nil) {
        
        [alerta MR_deleteInContext:moc];
        
        [Utils saveMOC:moc];
        [Utils postEntitytNotification:@"Alerta"];
    }
}


@end
