//
//  Carga.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/2/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Carga : NSObject

- (id)initWithMoc:(NSManagedObjectContext *)moc;
- (void)iniciar;

@end
