//
//  Carga.m
//  BrEstate
//
//  Created by BrEstate LTDA on 11/2/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "Carga.h"
#import "CoreDataUtils.h"
#import "Evento.h"
#import "TipoEvento.h"
#import "DateUtils.h"
#import "AppDelegate.h"
#import "Cliente.h"
#import "Endereco.h"
#import "TipoCliente.h"
#import "Imovel.h"
#import "TipoImovel.h"
#import "FotoImovel.h"
#import "Estado.h"
#import "Cidade.h"
#import "Bairro.h"
#import "TipoOferta.h"
#import "Negociacao.h"
#import "StatusNegociacao.h"
#import "SubtipoCliente.h"
#import "Valor.h"
#import "Area.h"
#import "TipoPerfilCliente.h"
#import "TipoValor.h"
#import "Alarme.h"
#import "AddressBookHelper.h"

@interface Carga()

@property (strong, nonatomic) NSManagedObjectContext *moc;

@end

@implementation Carga

- (id)initWithMoc:(NSManagedObjectContext *)moc
{
    self = [super init];
    if (self)
    {
        self.moc = moc;
    }
    return self;
}



- (void)iniciar
{
    
    self.moc = [NSManagedObjectContext MR_defaultContext];
    
    NSArray *tipos = [TipoEvento MR_findAllInContext:self.moc];
    
    if ([tipos count] == 0) {
        
        [self carregarEstadosCidades];
        
        [self carregarTipoCliente];
        [self carregarSubtipoCliente];
        
      //  [self carregarBairros];
        [self carregarTipoImovel];
        [self carregarTipoOferta];
        
        [self carregarTipoEvento];
        
        [self carregarStatusNegociacao];
        
        [self carregarTipoValor];
        [self carregarValores];
        [self carregarAreas];
        
        [self carregarTipoPerfilCliente];
        
        [self carregarAlarmes];

        [Utils saveMOC:self.moc];
        
    }
}


#pragma mark - Tipo Cliente
- (void)carregarTipoCliente
{
    TipoCliente *proprietario = [TipoCliente MR_createInContext:self.moc];
    proprietario.codigo = [NSNumber numberWithInt:1];
    proprietario.descricao = @"Proprietário";
    
    TipoCliente *interessado = [TipoCliente MR_createInContext:self.moc];
    interessado.codigo = [NSNumber numberWithInt:2];
    interessado.descricao = @"Interessado";
    
    TipoCliente *outros = [TipoCliente MR_createInContext:self.moc];
    outros.codigo = [NSNumber numberWithInt:3];
    outros.descricao = @"Outros";
}

#pragma mark - Subtipo Cliente
- (void)carregarSubtipoCliente
{
    SubtipoCliente *vendedor = [SubtipoCliente MR_createInContext:self.moc];
    vendedor.codigo = [NSNumber numberWithInt:1];
    vendedor.descricao = @"Vendedor";
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = 1"];
    vendedor.tipoCliente = [TipoCliente MR_findFirstWithPredicate:predicate inContext:self.moc];
    
    
    SubtipoCliente *locador = [SubtipoCliente MR_createInContext:self.moc];
    locador.codigo = [NSNumber numberWithInt:2];
    locador.descricao = @"Locador";
    
    predicate = [NSPredicate predicateWithFormat:@"codigo = 1"];
    locador.tipoCliente = [TipoCliente MR_findFirstWithPredicate:predicate inContext:self.moc];
    
    SubtipoCliente *comprar = [SubtipoCliente MR_createInContext:self.moc];
    comprar.codigo = [NSNumber numberWithInt:3];
    comprar.descricao = @"Comprar";
    
    predicate = [NSPredicate predicateWithFormat:@"codigo = 2"];
    comprar.tipoCliente = [TipoCliente MR_findFirstWithPredicate:predicate inContext:self.moc];
    SubtipoCliente *alugar = [SubtipoCliente MR_createInContext:self.moc];
    alugar.codigo = [NSNumber numberWithInt:4];
    alugar.descricao = @"Alugar";
    
    predicate = [NSPredicate predicateWithFormat:@"codigo = 2"];
    alugar.tipoCliente = [TipoCliente MR_findFirstWithPredicate:predicate inContext:self.moc];
    
    
    SubtipoCliente *parceiro = [SubtipoCliente MR_createInContext:self.moc];
    parceiro.codigo = [NSNumber numberWithInt:5];
    parceiro.descricao = @"Parceiro";
    
    predicate = [NSPredicate predicateWithFormat:@"codigo = 3"];
    parceiro.tipoCliente = [TipoCliente MR_findFirstWithPredicate:predicate inContext:self.moc];
    SubtipoCliente *fornecedor = [SubtipoCliente MR_createInContext:self.moc];
    fornecedor.codigo = [NSNumber numberWithInt:6];
    fornecedor.descricao = @"Fornecedor";
    
    predicate = [NSPredicate predicateWithFormat:@"codigo = 3"];
    fornecedor.tipoCliente = [TipoCliente MR_findFirstWithPredicate:predicate inContext:self.moc];
    
    SubtipoCliente *pessoal = [SubtipoCliente MR_createInContext:self.moc];
    pessoal.codigo = [NSNumber numberWithInt:7];
    pessoal.descricao = @"Pessoal";
    
    predicate = [NSPredicate predicateWithFormat:@"codigo = 3"];
    pessoal.tipoCliente =  [TipoCliente MR_findFirstWithPredicate:predicate inContext:self.moc];
    
    SubtipoCliente *outros = [SubtipoCliente MR_createInContext:self.moc];
    outros.codigo = [NSNumber numberWithInt:8];
    outros.descricao = @"Outros";
    
    predicate = [NSPredicate predicateWithFormat:@"codigo = 3"];
    outros.tipoCliente = [TipoCliente MR_findFirstWithPredicate:predicate inContext:self.moc];
}

#pragma mark - Alarme
- (void)carregarAlarmes
{
    
    Alarme *naHora =  [Alarme MR_createInContext:self.moc];
    naHora.codigo = [NSNumber numberWithInt:1];
    naHora.descricao = @"Na hora do evento";
    naHora.tempo = nil;
    
    Alarme *cinco =  [Alarme MR_createInContext:self.moc];
    cinco.codigo = [NSNumber numberWithInt:2];
    cinco.descricao = @"5 minutos antes";
    cinco.tempo = [NSNumber numberWithInt:-300];
    
    Alarme *quinze =  [Alarme MR_createInContext:self.moc];
    quinze.codigo = [NSNumber numberWithInt:3];
    quinze.descricao = @"15 minutos antes";
    quinze.tempo = [NSNumber numberWithInt:-900];
    
    Alarme *trinta =  [Alarme MR_createInContext:self.moc];
    trinta.codigo = [NSNumber numberWithInt:4];
    trinta.descricao = @"30 minutos antes";
    trinta.tempo = [NSNumber numberWithInt:-1800];
    
    Alarme *umaHora =  [Alarme MR_createInContext:self.moc];
    umaHora.codigo = [NSNumber numberWithInt:5];
    umaHora.descricao = @"1 hora antes";
    umaHora.tempo = [NSNumber numberWithInt:-3600];
    
    Alarme *duasHoras =  [Alarme MR_createInContext:self.moc];
    duasHoras.codigo = [NSNumber numberWithInt:6];
    duasHoras.descricao = @"2 hora antes";
    duasHoras.tempo = [NSNumber numberWithInt:-7200];
    
    
    Alarme *umDia =  [Alarme MR_createInContext:self.moc];
    umDia.codigo = [NSNumber numberWithInt:7];
    umDia.descricao = @"1 dia antes";
    umDia.tempo = [NSNumber numberWithInt:-86400];
    
    
    Alarme *doisDias =  [Alarme MR_createInContext:self.moc];
    doisDias.codigo = [NSNumber numberWithInt:8];
    doisDias.descricao = @"2 dias antes";
    doisDias.tempo = [NSNumber numberWithInt:-172800];
    
    Alarme *umaSemana =  [Alarme MR_createInContext:self.moc];
    umaSemana.codigo = [NSNumber numberWithInt:9];
    umaSemana.descricao = @"1 semana antes";
    umaSemana.tempo = [NSNumber numberWithInt:-604800];
    
}

#pragma mark - Tipo Perfil Cliente
- (void)carregarTipoPerfilCliente
{
    TipoPerfilCliente *proprietario = [TipoPerfilCliente MR_createInContext:self.moc];
    proprietario.codigo = [NSNumber numberWithInt:1];
    proprietario.descricao = @"Alugar";
    
    TipoPerfilCliente *interessado = [TipoPerfilCliente MR_createInContext:self.moc];
    interessado.codigo = [NSNumber numberWithInt:2];
    interessado.descricao = @"Comprar";
}

#pragma mark - Area
- (void)carregarAreas
{
    Area *area = [Area MR_createInContext:self.moc];
    area.codigo = [NSNumber numberWithInt:0];
    area.descricao = @"indiferente";
    area.valor = nil;
    
    area = [Area MR_createInContext:self.moc];
    area.codigo = [NSNumber numberWithInt:1];
    area.descricao = @"5 m²";
    area.valor = [NSNumber numberWithInt:5];
    
    area = [Area MR_createInContext:self.moc];
    area.codigo = [NSNumber numberWithInt:2];
    area.descricao = @"30 m²";
    area.valor = [NSNumber numberWithInt:30];
    
    area = [Area MR_createInContext:self.moc];
    area.codigo = [NSNumber numberWithInt:3];
    area.descricao = @"60 m²";
    area.valor = [NSNumber numberWithInt:60];
    
    area = [Area MR_createInContext:self.moc];
    area.codigo = [NSNumber numberWithInt:4];
    area.descricao = @"90 m²";
    area.valor = [NSNumber numberWithInt:90];
    
    area = [Area MR_createInContext:self.moc];
    area.codigo = [NSNumber numberWithInt:5];
    area.descricao = @"120 m²";
    area.valor = [NSNumber numberWithInt:120];
    
    area = [Area MR_createInContext:self.moc];
    area.codigo = [NSNumber numberWithInt:6];
    area.descricao = @"150 m²";
    area.valor = [NSNumber numberWithInt:150];
    
    area = [Area MR_createInContext:self.moc];
    area.codigo = [NSNumber numberWithInt:7];
    area.descricao = @"180 m²";
    area.valor = [NSNumber numberWithInt:180];
    
    area = [Area MR_createInContext:self.moc];
    area.codigo = [NSNumber numberWithInt:8];
    area.descricao = @"210 m²";
    area.valor = [NSNumber numberWithInt:210];
    
    area = [Area MR_createInContext:self.moc];
    area.codigo = [NSNumber numberWithInt:9];
    area.descricao = @"240 m²";
    area.valor = [NSNumber numberWithInt:240];
    
    area = [Area MR_createInContext:self.moc];
    area.codigo = [NSNumber numberWithInt:10];
    area.descricao = @"270 m²";
    area.valor = [NSNumber numberWithInt:270];
    
    area = [Area MR_createInContext:self.moc];
    area.codigo = [NSNumber numberWithInt:11];
    area.descricao = @"300 m²";
    area.valor = [NSNumber numberWithInt:300];
    
    area = [Area MR_createInContext:self.moc];
    area.codigo = [NSNumber numberWithInt:12];
    area.descricao = @"500 m²";
    area.valor = [NSNumber numberWithInt:500];
    
    area = [Area MR_createInContext:self.moc];
    area.codigo = [NSNumber numberWithInt:13];
    area.descricao = @"700 m²";
    area.valor = [NSNumber numberWithInt:700];
    
    area = [Area MR_createInContext:self.moc];
    area.codigo = [NSNumber numberWithInt:14];
    area.descricao = @"1000 m²";
    area.valor = [NSNumber numberWithInt:1000];
    
    area = [Area MR_createInContext:self.moc];
    area.codigo = [NSNumber numberWithInt:15];
    area.descricao = @"1500 m²";
    area.valor = [NSNumber numberWithInt:1500];
    
    area = [Area MR_createInContext:self.moc];
    area.codigo = [NSNumber numberWithInt:16];
    area.descricao = @"2000 m²";
    area.valor = [NSNumber numberWithInt:2000];
    
    area = [Area MR_createInContext:self.moc];
    area.codigo = [NSNumber numberWithInt:17];
    area.descricao = @"4000 m²";
    area.valor = [NSNumber numberWithInt:4000];
}

#pragma mark - Valores

#pragma mark - Tipo Valor
- (void)carregarTipoValor
{
    TipoValor *tipoCompra = [TipoValor MR_createInContext:self.moc];
    tipoCompra.codigo = [NSNumber numberWithInt:1];
    tipoCompra.descricao = @"Comprar";
    
    TipoValor *tipoAluguel = [TipoValor MR_createInContext:self.moc];
    tipoAluguel.codigo = [NSNumber numberWithInt:2];
    tipoAluguel.descricao = @"Alugar";
    
}


- (void)carregarValores
{
    [self carregarValoresCompra];
    [self carregarValoresAluguel];
    
}
- (void)carregarValoresAluguel
{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = 1"];
    TipoValor *tipoValor = [TipoValor MR_findFirstWithPredicate:predicate inContext:self.moc];
    Valor *valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:0];
    valor.descricao = @"indiferente";
    valor.minimo = [NSDecimalNumber zero];
    valor.maximo = [NSDecimalNumber maximumDecimalNumber];
    valor.tipo = tipoValor;
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:1];
    valor.descricao = @"até R$ 300.00";
    valor.minimo = [NSDecimalNumber zero];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:300.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:2];
    valor.descricao = @"R$ 300.00 a R$ 400.00";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:300.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:400.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:3];
    valor.descricao = @"R$ 400.00 a R$ 500.00";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:400.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:500.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:4];
    valor.descricao = @"R$ 500.00 a R$ 600.00";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:500.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:600.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:5];
    valor.descricao = @"R$ 600.00 a R$ 700.00";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:600.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:700.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:6];
    valor.descricao = @"R$ 700.00 a R$ 800.00";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:700.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:800.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:7];
    valor.descricao = @"R$ 800.00 a R$ 900.00";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:800.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:900.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:8];
    valor.descricao = @"R$ 900.00 a R$ 1000.00";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:900.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:1000.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:9];
    valor.descricao = @"R$ 1000.00 a R$ 1250.00";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:1000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:1250.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:10];
    valor.descricao = @"R$ 1250.00 a R$ 1500.00";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:1250.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:1500.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:11];
    valor.descricao = @"R$ 1500.00 a R$ 2000.00";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:1500.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:2000.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:12];
    valor.descricao = @"R$ 2000.00 a R$ 5000.00";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:2000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:5000.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:13];
    valor.descricao = @"R$ 5000.00 a R$ 8000.00";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:5000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:8000.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:14];
    valor.descricao = @"R$ 8000.00 a R$ 12000.00";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:8000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:12000.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:15];
    valor.descricao = @"R$ 12000.00 a R$ 15000.00";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:12000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:15000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber maximumDecimalNumber];
    valor.tipo = tipoValor;
    
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:16];
    valor.descricao = @"R$ 15000.00 a R$ 30000.00";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:15000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:30000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber maximumDecimalNumber];
    valor.tipo = tipoValor;
    
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:17];
    valor.descricao = @"acima de R$ 30000.00";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:30000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber maximumDecimalNumber];
    valor.tipo = tipoValor;
    
    
    
}
- (void)carregarValoresCompra
{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = 2"];
    TipoValor *tipoValor = [TipoValor MR_findFirstWithPredicate:predicate inContext:self.moc];
    Valor *valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:0];
    valor.descricao = @"indiferente";
    valor.minimo = [NSDecimalNumber zero];
    valor.maximo = [NSDecimalNumber maximumDecimalNumber];
    valor.tipo = tipoValor;
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:1];
    valor.descricao = @"até R$ 100.000";
    valor.minimo = [NSDecimalNumber zero];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:100000.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:2];
    valor.descricao = @"R$ 100.000 a R$ 150.000";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:100000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:150000.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:3];
    valor.descricao = @"R$ 150.000 a R$ 200.000";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:150000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:200000.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:4];
    valor.descricao = @"R$ 200.000 a R$ 250.000";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:200000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:250000.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:5];
    valor.descricao = @"R$ 250.000 a R$ 300.000";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:250000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:300000.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:6];
    valor.descricao = @"R$ 300.000 a R$ 350.000";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:300000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:350000.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:7];
    valor.descricao = @"R$ 350.000 a R$ 400.000";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:350000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:400000.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:8];
    valor.descricao = @"R$ 400.000 a R$ 600.000";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:400000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:600000.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:9];
    valor.descricao = @"R$ 600.000 a R$ 800.000";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:600000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:800000.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:10];
    valor.descricao = @"R$ 800.000 a 1 milhão";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:800000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:1000000.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:11];
    valor.descricao = @"1 milhão a 1,25 milhões";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:1000000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:1250000.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:12];
    valor.descricao = @"1,25 milhões a 1,5 milhões";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:1250000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:1500000.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:13];
    valor.descricao = @"1,5 milhões a 2 milhões";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:1500000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:2000000.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:14];
    valor.descricao = @"2 milhões a 3 milhões";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:2000000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:3000000.00f] decimalValue]];
    valor.tipo = tipoValor;
    
    valor = [Valor MR_createInContext:self.moc];
    valor.codigo = [NSNumber numberWithInt:15];
    valor.descricao = @"acima de 3 milhões";
    valor.minimo = [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithFloat:3000000.00f] decimalValue]];
    valor.maximo = [NSDecimalNumber maximumDecimalNumber];
    valor.tipo = tipoValor;
    
    
}

#pragma mark - Status Negociacao
- (void)carregarStatusNegociacao
{
    StatusNegociacao *proposta = [StatusNegociacao MR_createInContext:self.moc];
    proposta.codigo = [NSNumber numberWithInt:1];
    proposta.descricao = @"Proposta";
    
    StatusNegociacao *analise = [StatusNegociacao MR_createInContext:self.moc];
    analise.codigo = [NSNumber numberWithInt:2];
    analise.descricao = @"Análise";
    
    StatusNegociacao *fechada = [StatusNegociacao MR_createInContext:self.moc];
    fechada.codigo = [NSNumber numberWithInt:3];
    fechada.descricao = @"Fechada";
    
    StatusNegociacao *perdida = [StatusNegociacao MR_createInContext:self.moc];
    perdida.codigo = [NSNumber numberWithInt:4];
    perdida.descricao = @"Cancelada";
    
}

#pragma mark - Tipo Evento
- (void)carregarTipoEvento
{
    // Tipos
    
    TipoEvento *acompanhamento = [TipoEvento MR_createInContext:self.moc];
    acompanhamento.codigo = [NSNumber numberWithInt:1];
    acompanhamento.descricao = @"Acompanhamento";
    
    TipoEvento *captacao = [TipoEvento MR_createInContext:self.moc];
    captacao.codigo = [NSNumber numberWithInt:2];
    captacao.descricao = @"Captação";
    
    TipoEvento *reuniao = [TipoEvento MR_createInContext:self.moc];
    reuniao.codigo = [NSNumber numberWithInt:3];
    reuniao.descricao = @"Reunião";
    
    TipoEvento *visita = [TipoEvento MR_createInContext:self.moc];
    visita.codigo = [NSNumber numberWithInt:4];
    visita.descricao = @"Visita";
    
    TipoEvento *outro = [TipoEvento MR_createInContext:self.moc];
    outro.codigo = [NSNumber numberWithInt:5];
    outro.descricao = @"Outros";
    
    
}


#pragma mark - Tipo Imovel
- (void)carregarTipoImovel
{
    TipoImovel *tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:0];
    tipoImovel.descricao = @"Outro";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:1];
    tipoImovel.descricao = @"Casa";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:2];
    tipoImovel.descricao = @"Apartamento";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:3];
    tipoImovel.descricao = @"Duplex";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:4];
    tipoImovel.descricao = @"Fazenda / Sítio";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:5];
    tipoImovel.descricao = @"JK / Kitnet";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:6];
    tipoImovel.descricao = @"Casa de Campo";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:7];
    tipoImovel.descricao = @"Terreno";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:8];
    tipoImovel.descricao = @"Flat";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:9];
    tipoImovel.descricao = @"Chácara";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:10];
    tipoImovel.descricao = @"Mansão";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:11];
    tipoImovel.descricao = @"Prédio";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:12];
    tipoImovel.descricao = @"Condomínio";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:13];
    tipoImovel.descricao = @"Casa Comercial";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:14];
    tipoImovel.descricao = @"Sala / Espaço Comercial";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:15];
    tipoImovel.descricao = @"Loja";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:16];
    tipoImovel.descricao = @"Sobrado";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:17];
    tipoImovel.descricao = @"Cobertura";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:18];
    tipoImovel.descricao = @"Lançamento";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:19];
    tipoImovel.descricao = @"Casa Flutuante";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:20];
    tipoImovel.descricao = @"Vaga de Estacionamento";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:21];
    tipoImovel.descricao = @"Pousada / Hotel / Resort";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:22];
    tipoImovel.descricao = @"Espaço Industrial";
    
    tipoImovel = [TipoImovel MR_createInContext:self.moc];
    tipoImovel.codigo = [NSNumber numberWithInt:23];
    tipoImovel.descricao = @"Armazém / Galpão";
    
}



#pragma mark - Tipo Oferta
- (void)carregarTipoOferta
{
    TipoOferta *tipoOferta = [TipoOferta MR_createInContext:self.moc];
    tipoOferta.codigo = [NSNumber numberWithInt:1];
    tipoOferta.descricao = @"Outro";
    
    tipoOferta = [TipoOferta MR_createInContext:self.moc];
    tipoOferta.codigo = [NSNumber numberWithInt:2];
    tipoOferta.descricao = @"Locação";
    
    tipoOferta = [TipoOferta MR_createInContext:self.moc];
    tipoOferta.codigo = [NSNumber numberWithInt:3];
    tipoOferta.descricao = @"Venda";
    
    tipoOferta = [TipoOferta MR_createInContext:self.moc];
    tipoOferta.codigo = [NSNumber numberWithInt:4];
    tipoOferta.descricao = @"Temporada";
    
    tipoOferta = [TipoOferta MR_createInContext:self.moc];
    tipoOferta.codigo = [NSNumber numberWithInt:5];
    tipoOferta.descricao = @"Retomado";
    
}


#pragma mark - Bairro
- (void)carregarBairros
{
    Bairro *bairro = [Bairro MR_createInContext:self.moc];
    bairro.codigo = [NSNumber numberWithInt:1];
    bairro.nome = @"Centro";
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"nome = %@",@"Rio de Janeiro"];
    bairro.cidade = [[Cidade MR_findFirstWithPredicate:predicate inContext:self.moc] nome];
    
    bairro = [Bairro MR_createInContext:self.moc];
    bairro.codigo = [NSNumber numberWithInt:2];
    bairro.nome = @"Ilha do Governador";
    
    predicate = [NSPredicate predicateWithFormat:@"nome = %@",@"Rio de Janeiro"];
    bairro.cidade = [[Cidade MR_findFirstWithPredicate:predicate inContext:self.moc] nome];
}

#pragma mark - Estados e Cidades
- (void)carregarEstadosCidades
{
    NSError *error = nil;
    NSURL *url = [NSURL URLWithString:@"https://dl.dropboxusercontent.com/u/17091432/estados-cidades.json"];
    NSData *data = [NSData dataWithContentsOfURL:url options:kNilOptions error:&error];
    
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    int codigoEstado = 1;
    int codigoCidade = 1;
    
    NSDictionary *estados = [json objectForKey:@"estados"];
    Estado *estado;
    Cidade *cidade;
    
    for (NSArray *item in estados) {
        
        estado = [Estado MR_createInContext:self.moc];
        estado.codigo = [NSNumber numberWithInt:codigoEstado];
        estado.sigla = [item valueForKey:@"sigla"];
        estado.nome = [item valueForKey:@"nome"];
        
        NSArray *cidades = [item valueForKey:@"cidades"];
        
        for(NSString *nome in cidades) {
            
            cidade = [Cidade MR_createInContext:self.moc];
            cidade.codigo = [NSNumber numberWithInt:codigoCidade];
            cidade.nome = nome;
            cidade.estado = estado.nome;
            codigoCidade++;
        }
        
        codigoEstado++;
    }
    
}

@end
