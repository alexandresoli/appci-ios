//
//  ImageUtils.h
//  BrEstate
//
//  Created by BrEstate LTDA on 1/10/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageUtils : NSObject

+ (UIImage *)resize:(UIImage *)image scaledToSize:(CGSize)newSize;

@end
