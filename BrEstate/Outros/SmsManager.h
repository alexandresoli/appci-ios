//
//  SmsManager.h
//  Appci
//
//  Created by BrEstate LTDA on 28/05/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Cliente+Utils.h"
#import "Evento.h"

@interface SmsManager : NSObject

typedef void(^completionBlock)(BOOL newData);

+ (void)sendSmsTo:(Cliente *)cliente forEvento:(Evento *)evento withContext:(NSManagedObjectContext *)moc;
+ (void)checkConfirmationSmsWithMoc:(NSManagedObjectContext *)moc andCompletionBlock:(completionBlock)completion;

@end
