//
//  SmsManager.m
//  Appci
//
//  Created by BrEstate LTDA on 28/05/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "SmsManager.h"
#import "Corretor.h"
#import "DateUtils.h"
#import "SVProgressHUD.h"
#import "Acompanhamento+Firebase.h"
#import "Evento+Firebase.h"
#import "Sms.h"

@implementation SmsManager


+ (void)sendSmsTo:(Cliente *)cliente forEvento:(Evento *)evento withContext:(NSManagedObjectContext *)moc;
{
    
    [SVProgressHUD showWithStatus:@"Enviando..." maskType:SVProgressHUDMaskTypeBlack];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    //NSString *url = @"http://appci-web.appspot.com/send-sms.json";
    NSString *url = [NSString stringWithFormat:@"%@%@",[[Config instance] url], @"send-sms.json"];
    
    NSString *email = [[NSUserDefaults standardUserDefaults] objectForKey:USER_LOGIN];
    
    NSString *tipoEvento;
    
    if (evento.tipoEvento) {
        tipoEvento = evento.tipoEvento;
    } else {
        tipoEvento = @" ";
    }
    
    tipoEvento = [Utils removeAllSpecialCharacters:tipoEvento];
    
    Corretor *corretor = [Corretor MR_findFirstInContext:moc];
    
    NSString *dataFormatada = [DateUtils stringFromDate:evento.dataInicio withFormat:@"dd/MM 'as' HH:mm"];
    
    NSString *firstName =[[corretor.nome componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] firstObject];
    firstName = [Utils removeAllSpecialCharacters:firstName];
    
    NSString *phone = [[[[corretor.telefone stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""] componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] lastObject];
    
    NSString *dadosCorretor = [NSString stringWithFormat:@"Corretor %@ - %@",firstName,phone];
    
    
    NSDictionary *params = @{@"codigoCorretor": email,
                             @"tipoEvento": tipoEvento,
                             @"telefone": cliente.telefone,
                             @"nomeContato": cliente.primeiroNome,
                             @"evento": evento.local,
                             @"dataFormatada": dataFormatada,
                             @"dadosCorretor": dadosCorretor};
    
    [manager POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (!responseObject) {
            [Utils showMessage:@"Ocorreu um erro ao enviar o SMS. Verifique o correto preenchimento do telefone dos convidados e tente novamente."];
            [SVProgressHUD dismiss];
            return;
        }
        
        NSString *status = [responseObject valueForKey:@"status"];
        if (!status || [status isEqual:[NSNull null]] || ![status isEqualToString:@"OK"]) {
            [Utils showMessage:@"Ocorreu um erro ao enviar o SMS. Verifique o correto preenchimento do telefone dos convidados e tente novamente."];
            [SVProgressHUD dismiss];
            return;
        }
        
        NSNumber *codigoSMSNumber = [responseObject objectForKey:@"id"];
        NSString *codigoSMS = [codigoSMSNumber stringValue];
        
        if (codigoSMS) {
            Sms *sms = [Sms MR_createInContext:moc];
            
            sms.codigoSMS = codigoSMS;
            sms.dataEnvio = [NSDate date];
            sms.cliente = cliente;
            sms.evento = evento;
            
            [sms.evento saveFirebase];
            
            [moc MR_saveToPersistentStoreAndWait];
            
//            [sms saveParse:moc];
            
            [Utils showMessage:@"Envio realizado. Você receberá uma notificação quando o cliente responder o SMS."];
            
            
            NSString *textEvento;
            if (sms.evento.tipoEvento) {
                textEvento = [NSString stringWithFormat:@"%@ %@", sms.evento.tipoEvento, sms.evento.titulo];
            } else {
                textEvento = sms.evento.titulo;
            }
            
            [self salvarAcompanhamento:cliente usingMessage:[NSString stringWithFormat:@"Foi solicitado confirmação do evento %@ via SMS.",textEvento] withMoc:moc];
            
            [Utils postEntitytNotification:@"Evento"];
            [SVProgressHUD dismiss];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [Utils showMessage:@"Ocorreu um erro no envio da mensagem. Tente novamente mais tarde. Caso o erro persista, entre em contato com nosso suporte."];
        [SVProgressHUD dismiss];
    }];
}


+ (void)checkConfirmationSmsWithMoc:(NSManagedObjectContext *)moc andCompletionBlock:(completionBlock)completion
{
    if (![Utils isConnected]) {
        if (completion) {
            completion(NO);
        }
        return;
    }
    
    NSMutableArray *smsParam = [[NSMutableArray alloc] init];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"respostaConfirmacao = nil"];
    
    NSArray *smsToCheck = [Sms MR_findAllWithPredicate:predicate inContext:moc];
    
    for (Sms *sms in smsToCheck) {
        [smsParam addObject:sms.codigoSMS];
    }
    
    if (smsToCheck.count == 0) {
        if (completion) {
            completion(NO);
        }
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    //NSString *url = @"http://appci-web.appspot.com/busca-sms-mo.json";
    NSString *url = [NSString stringWithFormat:@"%@%@",[[Config instance] url], @"busca-sms-mo.json"];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:smsParam options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSDictionary *jsonParam = @{@"json": jsonString};
    
    [manager POST:url parameters:jsonParam success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *resultArray = responseObject;
        
        if (!resultArray || resultArray.count == 0) {
            if (completion) {
                completion(NO);
            }
            return;
        }
        
        for (NSDictionary *resultDict in resultArray) {
            NSString *statusConfirmation = [resultDict objectForKey:@"status"];
            
            if ([statusConfirmation isEqualToString:@"OK"] || [statusConfirmation isEqualToString:@"NOK"]) {
                NSNumber *codigoSMS = [resultDict objectForKey:@"id"];
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigoSMS = %@", codigoSMS];
                
                Sms *sms = [Sms MR_findFirstWithPredicate:predicate inContext:moc];
                
                sms.respostaConfirmacao = statusConfirmation;
                
                if ([statusConfirmation isEqualToString:@"OK"]) {
                    statusConfirmation = @"confirmou";
                } else if ([statusConfirmation isEqualToString:@"NOK"]) {
                    statusConfirmation = @"recusou";
                }
                
                UILocalNotification *localNotif = [[UILocalNotification alloc] init];
                
                NSString *textEvento;
                if (sms.evento.tipoEvento) {
                    textEvento = [NSString stringWithFormat:@"%@ %@", sms.evento.tipoEvento, sms.evento.titulo];
                } else {
                    textEvento = sms.evento.titulo;
                }
                
                localNotif.alertBody = [NSString stringWithFormat:@"O cliente %@ %@ o evento %@", sms.cliente.nome, statusConfirmation, textEvento];
                localNotif.alertAction = @"visualizar";
                localNotif.soundName = UILocalNotificationDefaultSoundName;
                localNotif.applicationIconBadgeNumber = 1;
                localNotif.timeZone = [NSTimeZone defaultTimeZone];
                localNotif.fireDate = [[NSDate date] dateByAddingTimeInterval:120];
                
                NSDictionary *infoDict = @{@"SMS": sms.codigoSMS};
                localNotif.userInfo = infoDict;
                
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
                
                [self salvarAcompanhamento:sms.cliente usingMessage:[NSString stringWithFormat:@"O cliente %@ o evento %@.",statusConfirmation,textEvento] withMoc:moc];
                
                [sms.evento saveFirebase];
                
                [moc MR_saveToPersistentStoreAndWait];
                
                [Utils trackFlurryWithName:@"Recebeu Resposta de SMS"];
            }
        }
        
        if (completion) {
            completion(YES);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error on SmsManager checkConfirmationSms - %@", error.localizedDescription);
        if (completion) {
            completion(NO);
        }
    }];
}


#pragma mark - Acompanhamento Cliente
+ (void)salvarAcompanhamento:(Cliente *)contato usingMessage:(NSString *)message withMoc:(NSManagedObjectContext *)moc
{
    Acompanhamento *acompanhamento = [Acompanhamento MR_createInContext:moc];
    acompanhamento.codigo = [acompanhamento getUniqueCode]; //[NSNumber numberWithLong:codigo];
    acompanhamento.titulo = message;
    acompanhamento.timeStamp = [NSDate date];
    acompanhamento.cliente = contato;
    
    [acompanhamento saveFirebase];
    
    [moc MR_saveToPersistentStoreAndWait];
//    [acompanhamento saveParse:moc];
}


@end
