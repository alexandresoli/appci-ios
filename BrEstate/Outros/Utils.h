//
//  Utils.h
//  Appci
//
//  Created by BrEstate LTDA on 4/17/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FotoImovel.h"

@class CEP;

@interface Utils : NSObject

typedef void(^completionBlock)(BOOL success);

+ (void)showMessage:(NSString *)message;
+ (void)addRedCorner:(UIView *)view;
+ (void)removeRedCorner:(UIView *)view;
+ (BOOL)validateEmail:(NSString *)checkString;
+ (BOOL)isConnected;
+ (void)trackWithName:(NSString *)name;
+ (NSString *)checkDiskSpace;
+ (void)postEntitytNotification:(NSString *)entityName;
+ (void)postEntitytNotificationWithValue:(NSString *)entityName value:(long)value;
+ (NSURL *)getIcloudDocumentsURL;
+ (NSArray *)sortArray:(NSArray *)array usingKey:key ascending:(BOOL)ascending;
+ (NSString *)removeSpecialCharacters:(NSString *)source;
+ (NSString *)removeAllSpecialCharacters:(NSString *)source;
+ (NSNumber *)numberFromString:(NSString *)string;

+ (void)dismissProgress;
+ (void)showProgress:(NSString *)message;
+ (void)showProgressWithValue:(float)value andMessage:(NSString *)message;

+ (NSString *)trim:(NSString *)string;
+ (void)deleteAllPhotos:(Imovel *)imovel withMoc:(NSManagedObjectContext *)moc;
+ (BOOL)personalDataIsFilled:(id)sender withContext:(NSManagedObjectContext *)moc andMessage:(NSString *)message;
+ (void)initFrameworks;
+ (NSString *)deviceID;
+ (NSString *)deviceName;
+ (void)checkFirstRunForPrepareDataWithMoc:(NSManagedObjectContext *)moc;
//+ (void)checkSync:(NSManagedObjectContext *)moc;
+ (BOOL)hasProperSize:(NSData *)data;
+ (void)startNetworkIndicator;
+ (void)stopNetworkIndicator;
+ (void)saveMOC:(NSManagedObjectContext *)moc;
+ (NSManagedObject *)objectForURIRepresentationString:(NSString *)uriString withMoc:(NSManagedObjectContext *)moc;
+ (BOOL)iCloudEnabledByUser;
+ (BOOL)iCloudAccountIsSignedIn;
+ (void)resetDatabaseIfNeededWithEmail:(NSString *)email andMoc:(NSManagedObjectContext *)moc;
+ (void)sendTokenToEmail:(NSString *)email withCompletionBlock:(completionWithSuccess)completionSuccess;

+ (id)valueFromDefaults:(NSString *)key;
+ (void)addObjectToDefaults:(id)value withKey:(NSString *)key;
+ (void)removeObjectFromDefaults:(NSString *)key;

+ (NSString *)replaceCrazyChar:(NSString *)string;

+ (void)saveLoginUserDefaultsWithEmail:(NSString *)email andPassword:(NSString *)password;

+ (void)showLogin;
+ (void)prepareLogout;
//+ (void)checkDeleted:(NSManagedObjectContext *)context withRefs:(NSArray *)refs;

+ (FirebaseUtils *)firebaseUtils;

+ (IAPHelper *)iapHelper;

+ (NSURL *)getLocalFotosURL;

+ (void)trackFlurryWithName:(NSString *)name;
+ (void)trackFlurryTimedWithName:(NSString *)name;


@end
