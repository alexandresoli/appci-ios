//
//  Utils.m
//  Appci
//
//  Created by BrEstate LTDA on 4/17/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Utils.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"
#import "SVProgressHUD.h"
#import "Corretor.h"
#import "FotoImovel.h"
#import "FotoImovel+InsertDeleteImagem.h"
#import "CEP.h"

#import <UAAppReviewManager.h>
#import <GoogleMaps/GoogleMaps.h>
#import "AlertaUtils.h"
#import "AddressBookHelper.h"
#import "Migrator.h"
#import "CustomCalendar.h"
#import "SyncViewController.h"
#import "LoginViewController.h"
#import "Imovel.h"

@implementation Utils

#pragma mark - Show Alert
+ (void)showMessage:(NSString *)message
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Aviso" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    });
}

#pragma mark - Red Corner
+ (void)addRedCorner:(UIView *)view
{
    view.layer.borderColor   = [[UIColor redColor] CGColor];
    view.layer.cornerRadius  = 5;
    view.layer.borderWidth   = 2;
}

+ (void)removeRedCorner:(UIView *)view
{
    view.layer.borderColor   = [[UIColor clearColor] CGColor];
    view.layer.cornerRadius  = 0;
    view.layer.borderWidth   = 9;
}

#pragma mark - Validate Email
+ (BOOL)validateEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark - Check connection
+ (BOOL)isConnected
{
    BOOL isConnected = [AFNetworkReachabilityManager sharedManager].reachable;
    return isConnected;
}

#pragma mark - Google Analytics Tracker
+ (void)trackWithName:(NSString *)name
{
    // returns the same tracker you created in your app delegate
    // defaultTracker originally declared in AppDelegate.m
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    // This screen name value will remain set on the tracker and sent with
    // hits until it is set to a new value or to nil.
    [tracker set:kGAIScreenName value:name];
    
    // manual screen tracking
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}


#pragma mark - Check Disk Space
+ (NSString *)checkDiskSpace
{
    
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    NSNumber *totalSizeBytes = [dictionary objectForKey: NSFileSystemSize];
    NSNumber *freeSizeBytes = [dictionary objectForKey:NSFileSystemFreeSize];
    
    uint64_t total = [totalSizeBytes unsignedLongLongValue];
    uint64_t free = [freeSizeBytes unsignedLongLongValue];
    
    NSString *message = [NSString stringWithFormat:@"Total de %llu MB com %llu MB livres.", ((total/1024ll)/1024ll), ((free/1024ll)/1024ll)];
    
    return message;
}


#pragma mark - Post Notification
+ (void)postEntitytNotification:(NSString *)entityName
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:entityName object:nil];
    });
}

+ (void)postEntitytNotificationWithValue:(NSString *)entityName value:(long)value

{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSNumber *object = [NSNumber numberWithLong:value];
        [[NSNotificationCenter defaultCenter] postNotificationName:entityName object:object];
    });
}


#pragma mark - iCloud Documents URL
+ (NSURL *)getIcloudDocumentsURL
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSURL *icloudURL = [fileManager URLForUbiquityContainerIdentifier:nil];
    
    if (icloudURL) {
        icloudURL = [icloudURL URLByAppendingPathComponent:@"Documents"];
        
        icloudURL = [icloudURL URLByAppendingPathComponent:@"Fotos"];
        
        if (![fileManager fileExistsAtPath:icloudURL.path]) {
            NSError *error;
            
            [fileManager createDirectoryAtURL:icloudURL withIntermediateDirectories:YES attributes:nil error:&error];
        }
    }
    
    return icloudURL;
}



+ (NSArray *)sortArray:(NSArray *)array usingKey:key ascending:(BOOL)ascending
{
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:ascending];
    
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    NSArray *sortedArray = [array sortedArrayUsingDescriptors:sortDescriptors];
    
    return sortedArray;
}


+ (NSString *)removeSpecialCharacters:(NSString *)source
{
    NSCharacterSet *allowed = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"] invertedSet];
    NSString *resultString = [[source componentsSeparatedByCharactersInSet:allowed] componentsJoinedByString:@""];
    
    return resultString;
}

+ (NSString *)removeAllSpecialCharacters:(NSString *)source
{
    
    if (source == nil || source.length == 0) {
        return source;
    }
    
    NSData *data = [source dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
    
}

+ (NSNumber *)numberFromString:(NSString *)string
{
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    fmt.numberStyle = NSNumberFormatterNoStyle;
    return [fmt numberFromString:string];
}

#pragma mark - Progress
+ (void)showProgress:(NSString *)message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD showWithStatus:message maskType:SVProgressHUDMaskTypeBlack];
    });
}
+ (void)dismissProgress
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}

+ (void)showProgressWithValue:(float)value andMessage:(NSString *)message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD showProgress:value status:message maskType:SVProgressHUDMaskTypeBlack];
    });
}


#pragma mark - NSString TRIM START and END
+ (NSString *)trim:(NSString *)string
{
    return [string stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
}


#pragma mark - Image CleanUp
+ (void)deleteAllPhotos:(Imovel *)imovel withMoc:(NSManagedObjectContext *)moc
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"imovel = %@ AND importacao = YES",imovel];
    NSArray *photos = [FotoImovel MR_findAllWithPredicate:predicate inContext:moc];
    
    for (FotoImovel *photo in photos) {
        
        // amazon s3
        [photo deleteImageFromS3];
        
        [photo deleteFromDisk];
        [photo MR_deleteInContext:moc];
        
    }
}

#pragma mark - Check if personal data is filled

+ (BOOL)personalDataIsFilled:(id)sender withContext:(NSManagedObjectContext *)moc andMessage:(NSString *)message
{
    BOOL isValid = YES;
    
    Corretor *corretor = [Corretor MR_findFirstInContext:moc];
    
    if (!corretor || corretor.nome.length == 0 || corretor.telefone.length == 0) {
        [Utils showMessage:message];
        
        UIStoryboard *storyboard;
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            storyboard = [UIStoryboard storyboardWithName:@"Acessorios_iPad" bundle:nil];
        } else {
            storyboard = [UIStoryboard storyboardWithName:@"Acessorios_iPhone" bundle:nil];
        }
        
        UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"ConfigScreen"];
        controller.modalPresentationStyle = UIModalPresentationFormSheet;
        [sender presentViewController:controller animated:YES completion:nil];
        
        isValid = NO;
    }
    
    return isValid;
}

#pragma mark - Device Details
+ (NSString *)deviceID
{
    NSString *deviceID = nil;
    
    UIDevice *currentDevice = [UIDevice currentDevice];
    if ([currentDevice.model rangeOfString:@"Simulator"].location == NSNotFound) {
        deviceID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    } else {
        deviceID = @"SIMULADOR-ID";
    }
    
    return deviceID;
    
}

+ (NSString *)deviceName
{
    NSString *deviceName = nil;
    
    UIDevice *currentDevice = [UIDevice currentDevice];
    if ([currentDevice.model rangeOfString:@"Simulator"].location == NSNotFound) {
        deviceName = [[UIDevice currentDevice] name];
    } else {
        deviceName = @"NOME-SIMULADOR";
    }
    
    return deviceName;
    
}

#pragma mark - Init Frameworks
+ (void)initFrameworks
{
    // App Review
    [self configureAppReviewManager];
    
    // Google Maps
    [GMSServices provideAPIKey:@"AIzaSyCfJeDyQtWVasW9zjQpwfxXaT23h5VV0wM"];
    
    // Google Analytics
    [self initGoogleAnalytics];
    
    // Mix Panel
//    [Mixpanel sharedInstanceWithToken:MIXPANEL_TOKEN];
    
}


+ (void)configureAppReviewManager
{
    [UAAppReviewManager setAppID:@"823722678"];
    [UAAppReviewManager setDaysUntilPrompt:1];
    [UAAppReviewManager setUsesUntilPrompt:2];
    [UAAppReviewManager setDaysBeforeReminding:3];
    [UAAppReviewManager showPromptIfNecessary];
}


+ (void)initGoogleAnalytics
{
    
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 120;
    
    // Optional: set Logger to VERBOSE for debug information.
    //    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    
    UIDevice *currentDevice = [UIDevice currentDevice];
    if ([currentDevice.model rangeOfString:@"Simulator"].location == NSNotFound) {
        
        // Initialize tracker.
        [[GAI sharedInstance] trackerWithTrackingId:@"UA-39963498-2"];
        //[[GAI sharedInstance] defaultTracker];
        
    }
}


#pragma mark - Check First Run For Prepare Data

+ (void)checkFirstRunForPrepareDataWithMoc:(NSManagedObjectContext *)moc
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    // Verifica se a primeira inicialização para realizar a carga de contatos.
    if (![defaults boolForKey:ALREADY_RUNNED]) {
        
        // cancel all previous notifications
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
        [AlertaUtils removeAlertIncompleteContacts:moc];
        
        [defaults setBool:YES forKey:ALREADY_RUNNED];
        [defaults synchronize];
    }
    
    // Migra as fotos do core data para o disco.
    if (![defaults boolForKey:MIGRATE_PHOTOS]) {
        [Migrator migrateFotoImovelFromCoreDataToDisk:moc];
        
        [defaults setBool:YES forKey:MIGRATE_PHOTOS];
        [defaults synchronize];
    }
}


#pragma mark - Check File Size Limit (10MB)
+ (BOOL)hasProperSize:(NSData *)data
{
    if (data == nil) {
        return NO;
    }
    
    // There are 1024 bytes in a kilobyte and 1024 kilobytes in a megabyte
    float sizeMB = (float)data.length/1024.0f/1024.0f;
    NSLog(@"File size: %.2f MB",sizeMB);
    
    if (sizeMB >= 10) {
        [self showMessage:@"Arquivo acima do limite de 10 Megabyte!"];
        return NO;
    }
    
    return YES;
}

#pragma mark - Network Indicator
+ (void)startNetworkIndicator
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    });
}

+ (void)stopNetworkIndicator
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    });
}

#pragma mark - MOC Save
+ (void)saveMOC:(NSManagedObjectContext *)moc
{
    [moc MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if (error) {
            NSLog(@"%@",error.localizedDescription);
        }
    }];
}

#pragma mark - Get core data object with URIRepresentation string
+ (NSManagedObject *)objectForURIRepresentationString:(NSString *)uriString withMoc:(NSManagedObjectContext *)moc
{
    NSManagedObject *object;
    NSError *error;
    
    NSURL *urlObjectID = [NSURL URLWithString:uriString];
    NSManagedObjectID *coreDataObjectID = [moc.persistentStoreCoordinator managedObjectIDForURIRepresentation:urlObjectID];
    
    object = [moc existingObjectWithID:coreDataObjectID error:&error];
    
    if (error) {
        NSLog(@"%@", error);
        object = nil;
    }
    
    return object;
}

#pragma mark - Check iCloud Account
+ (BOOL)iCloudEnabledByUser
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([[defaults objectForKey:@"iCloudEnabled"] boolValue]) {
        return YES;
    }
    
    return NO;
}

+ (BOOL)iCloudAccountIsSignedIn
{
    id token = [[NSFileManager defaultManager] ubiquityIdentityToken];
    
    if (token) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:[NSString stringWithFormat:@"%@",token] forKey:@"iCloud-token"];
        [defaults synchronize];
        
        return YES;
    }
    
    return NO;
}


+ (void)resetDatabaseIfNeededWithEmail:(NSString *)email andMoc:(NSManagedObjectContext *)moc
{
    NSString *lastEmailUsed = [Utils valueFromDefaults:USER_LAST_LOGIN];
    
    if (!lastEmailUsed || [lastEmailUsed isEqualToString:email.lowercaseString]) {
        return; // dont need reset database.
    }
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    // limpa a data de atualizacao no userdefaults
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    // Sync
    [defaults removeObjectForKey:COUNT_SYNC];
    [defaults removeObjectForKey:SYNC_ERROR_MESSAGE_FOTO];
    [defaults removeObjectForKey:SYNC_ERROR_MESSAGE_NOTA];
    [defaults removeObjectForKey:SYNC_ERROR_MESSAGE_ACOMP];
    
    // Firebase
    [defaults removeObjectForKey:FIREBASE_CLIENTE_INSERT_VERSION];
    [defaults removeObjectForKey:FIREBASE_IMOVEL_INSERT_VERSION];
    [defaults removeObjectForKey:FIREBASE_FOTO_INSERT_VERSION];
    [defaults removeObjectForKey:FIREBASE_BAIRRO_INSERT_VERSION];
    [defaults removeObjectForKey:FIREBASE_EVENTO_INSERT_VERSION];
    [defaults removeObjectForKey:FIREBASE_NOTA_INSERT_VERSION];
    [defaults removeObjectForKey:FIREBASE_ACOMPANHAMENTO_INSERT_VERSION];
    [defaults removeObjectForKey:FIREBASE_NEGOCIACAO_INSERT_VERSION];
    [defaults removeObjectForKey:FIREBASE_PERFIL_INSERT_VERSION];
    [defaults removeObjectForKey:FIREBASE_CLIENTE_DELETADO_INSERT_VERSION];
    [defaults removeObjectForKey:FIREBASE_DELETADOS_INSERT_VERSION];
    [defaults removeObjectForKey:FIREBASE_PENDING_DELETED_OBJECTS_DICT];
    [defaults removeObjectForKey:FIREBASE_USER_ID];
    [defaults removeObjectForKey:FIREBASE_ALREADY_DOWNLOAD_ALL];
    [defaults removeObjectForKey:FIREBASE_ALREADY_RUN];
    [defaults removeObjectForKey:FIREBASE_ADJUST_OBJECTS_CODIGO];
    [defaults removeObjectForKey:FIREBASE_SEND_ADJUST_OBJECTS_CODIGO];
    
    [[Utils firebaseUtils] removeAllObservers];
    
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    delegate.fireBaseUtils = nil;
    
    [defaults synchronize];
    
    
    // limpa os dados das tabelas
    NSArray *acompanhamentos = [CoreDataUtils fetchEntityID:@"Acompanhamento" withPredicate:nil inContext:moc];
    
    [self deleteAllEntities:acompanhamentos withMoc:moc];
    
    NSArray *bairros = [CoreDataUtils fetchEntityID:@"Bairro" withPredicate:nil inContext:moc];
    
    [self deleteAllEntities:bairros withMoc:moc];
    
    NSArray *clientes = [CoreDataUtils fetchEntityID:@"Cliente" withPredicate:nil inContext:moc];
    
    [self deleteAllEntities:clientes withMoc:moc];
    
    NSArray *clientesDeletados = [CoreDataUtils fetchEntityID:@"ClienteDeletado" withPredicate:nil inContext:moc];
    [self deleteAllEntities:clientesDeletados withMoc:moc];
    
    NSArray *corretor = [CoreDataUtils fetchEntityID:@"Corretor" withPredicate:nil inContext:moc];
    [self deleteAllEntities:corretor withMoc:moc];
    
    NSArray *enderecos = [CoreDataUtils fetchEntityID:@"Endereco" withPredicate:nil inContext:moc];
    [self deleteAllEntities:enderecos withMoc:moc];
    
    NSArray *eventos = [CoreDataUtils fetchEntityID:@"Evento" withPredicate:nil inContext:moc];
    [self deleteAllEntities:eventos withMoc:moc];
    
    NSArray *fotos = [CoreDataUtils fetchEntityID:@"FotoImovel" withPredicate:nil inContext:moc];
    [self deleteAllEntities:fotos withMoc:moc];
    
    NSArray *imoveis = [CoreDataUtils fetchEntityID:@"Imovel" withPredicate:nil inContext:moc];
    [self deleteAllEntities:imoveis withMoc:moc];
    
    NSArray *negociacoes = [CoreDataUtils fetchEntityID:@"Negociacao" withPredicate:nil inContext:moc];
    [self deleteAllEntities:negociacoes withMoc:moc];
    
    NSArray *notas = [CoreDataUtils fetchEntityID:@"Nota" withPredicate:nil inContext:moc];
    [self deleteAllEntities:notas withMoc:moc];
    
    NSArray *perfis = [CoreDataUtils fetchEntityID:@"PerfilCliente" withPredicate:nil inContext:moc];
    [self deleteAllEntities:perfis withMoc:moc];
    
    NSArray *sms = [CoreDataUtils fetchEntityID:@"Sms" withPredicate:nil inContext:moc];
    [self deleteAllEntities:sms withMoc:moc];
    
    NSArray *tags = [CoreDataUtils fetchEntityID:@"TagImovel" withPredicate:nil inContext:moc];
    [self deleteAllEntities:tags withMoc:moc];
    
    NSArray *transacao = [CoreDataUtils fetchEntityID:@"TransacaoInAppPurchase" withPredicate:nil inContext:moc];
    [self deleteAllEntities:transacao withMoc:moc];
    
    NSArray *imobiliarias = [CoreDataUtils fetchEntityID:@"Imobiliaria" withPredicate:nil inContext:moc];
    [self deleteAllEntities:imobiliarias withMoc:moc];
    
    NSArray *usuarios = [CoreDataUtils fetchEntityID:@"Usuario" withPredicate:nil inContext:moc];
    [self deleteAllEntities:usuarios withMoc:moc];
    
    // apaga as fotos do disco
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSURL *url = [self getLocalFotosURL];
    
    NSError *error;
    [fileManager removeItemAtURL:url error:&error];
    
    if (error) {
        NSLog(@"%@",error.localizedDescription);
    }
    
    
    
    
}

+ (void)deleteAllEntities:(NSArray *)entities withMoc:(NSManagedObjectContext *)moc
{
    for (NSManagedObject *obj in entities) {
        [moc deleteObject:obj];
    }
    
    [moc MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if (error) {
            NSLog(@"%@",error.localizedDescription);
        }
    }];
}


#pragma mark - Send Token

+ (void)sendTokenToEmail:(NSString *)email withCompletionBlock:(completionWithSuccess)completionSuccess
{
    [Utils showProgress:@"Enviando Token"];
    
    NSString *url = [NSString stringWithFormat:@"%@%@",[[Config instance] url], @"auth/ask-token.json"];
    
    NSDictionary *params = @{@"email": email};
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSNumber *success = [responseObject objectForKey:@"success"];
        
        if ([success boolValue]) {
            
            [Utils dismissProgress];
            
            if (completionSuccess) {
                completionSuccess(YES);
            }
            
        } else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [Utils dismissProgress];
                
                NSString *errorMessage = [responseObject objectForKey:@"message"];
                
                if (!errorMessage) {
                    errorMessage = @"Ocorreu um erro ao solicitar código de ativação. Tente novamente. Se o erro persistir entre em contato com suporte@brestate.com.br";
                }
                
                [Utils showMessage:errorMessage];
                
                if (completionSuccess) {
                    completionSuccess(NO);
                }
            });
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [Utils dismissProgress];
            
            [Utils showMessage:[NSString stringWithFormat:@"Ocorreu um erro ao solicitar código de ativação. Tente novamente. Caso o erro persista entre em contato com suporte@brestate.com.br \nErro - %@", error]];
            
            if (completionSuccess) {
                completionSuccess(NO);
            }
        });
    }];
}


#pragma mark - User Defaults
+ (void)addObjectToDefaults:(id)value withKey:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:key];
    [defaults synchronize];
    
}

+ (void)removeObjectFromDefaults:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:key];
    [defaults synchronize];
    
}

+ (id)valueFromDefaults:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:key];
}


#pragma mark - Replace Char

+ (NSString *)replaceCrazyChar:(NSString *)string
{
    if ([string isEqualToString:@"Æ"]) {
        return @"A";
    } else if ([string isEqualToString:@"Ð"]) {
        return @"D";
    } else if ([string isEqualToString:@"Ø"]) {
        return @"O";
    } else if ([string isEqualToString:@"Þ"]) {
        return @"Z";
    } else if ([string isEqualToString:@"ß"]) {
        return @"S";
    } else if ([string isEqualToString:@"ℓ"]) {
        return @"L";
    }
    
    return string;
}


#pragma mark - Save login on user defaults

+ (void)saveLoginUserDefaultsWithEmail:(NSString *)email andPassword:(NSString *)password
{
    if (!email || [email isEqualToString:@""]) {
        return;
    }
    
    if (!password || [password isEqualToString:@""]) {
        return;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:email forKey:USER_LOGIN];
    
    NSData *passData = [password dataUsingEncoding:NSUTF8StringEncoding];
    [defaults setObject:passData forKey:USER_PASS];
    
    [defaults synchronize];
}


#pragma mark - Prepare Logout

+ (void)prepareLogout
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *email = [defaults objectForKey:USER_LOGIN];
    [defaults setObject:email forKey:USER_LAST_LOGIN];
    
    [defaults removeObjectForKey:USER_LOGIN];
    [defaults removeObjectForKey:USER_PASS];
    [defaults synchronize];
}


#pragma mark - Show Login

+ (void)showLogin
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"_Login_iPad" bundle:nil];
            UINavigationController *navController = [storyboard instantiateInitialViewController];
            LoginViewController *loginController = [[navController viewControllers] firstObject];
            loginController.lostAccess = YES;
            
            AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
            delegate.window.rootViewController = navController;
            [delegate.window makeKeyAndVisible];
            
        } else {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
            LoginViewController *loginController = [storyboard instantiateInitialViewController];
            loginController.lostAccess = YES;
            
            AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
            delegate.window.rootViewController = loginController;
            [delegate.window makeKeyAndVisible];
        }
    });
}

//#pragma mark - Delete unused objects
//+ (void)checkDeleted:(NSManagedObjectContext *)context withRefs:(NSArray *)refs
//{
//    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"importacao = YES"];
//    
//    NSArray *imoveis = [Imovel MR_findAllWithPredicate:predicate inContext:context];
//    NSMutableArray *imoveisToDelete = [NSMutableArray array];
//    
//    // filter non existing objects
//    for (Imovel *imovel in imoveis) {
//        
//        if (![refs containsObject:imovel.identificacao]) {
//            [imoveisToDelete addObject:imovel];
//        }
//    }
//    
//    // delete non existing objects
//    for (Imovel *imovel in imoveisToDelete) {
//        
//        [Utils deleteAllPhotos:imovel withMoc:context];
//        [imovel MR_deleteInContext:context];
//        
//    }
//    
//}


#pragma mark - Firebase Instance

+ (FirebaseUtils *)firebaseUtils
{
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    return appDelegate.fireBaseUtils;
}


#pragma mark - IAPHelper Instance

+ (IAPHelper *)iapHelper
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    return appDelegate.iapHelper;
}


#pragma mark - Photo Paths
+ (NSURL *)getLocalFotosURL
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSURL *localURL = [[fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    localURL = [localURL URLByAppendingPathComponent:@"Fotos"];
    
    if (![fileManager fileExistsAtPath:localURL.path]) {
        NSError *error;
        
        [fileManager createDirectoryAtURL:localURL withIntermediateDirectories:YES attributes:nil error:&error];
    }
    
    return localURL;
}


#pragma mark - Flurry

+ (void)trackFlurryWithName:(NSString *)name
{
    NSDictionary *userInfo = @{@"user": [Utils valueFromDefaults:USER_LOGIN]};
    [Flurry logEvent:name withParameters:userInfo];
}


+ (void)trackFlurryTimedWithName:(NSString *)name
{
    NSDictionary *userInfo = @{@"user": [Utils valueFromDefaults:USER_LOGIN]};
    [Flurry logEvent:name withParameters:userInfo timed:YES];
}


@end
