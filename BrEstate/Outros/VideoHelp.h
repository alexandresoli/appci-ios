//
//  VideoHelp.h
//  Appci
//
//  Created by BrEstate LTDA on 28/04/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
@import MediaPlayer;

@interface VideoHelp : NSObject

- (MPMoviePlayerController *)showHelpIphoneOnController:(UIViewController *)sender withCallback:(SEL)callback;

@end
