//
//  VideoHelp.m
//  Appci
//
//  Created by BrEstate LTDA on 28/04/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "VideoHelp.h"

@implementation VideoHelp


- (MPMoviePlayerController *)showHelpIphoneOnController:(UIViewController *)sender withCallback:(SEL)callback
{
    UIView *viewMovie = [[UIView alloc] initWithFrame:sender.view.frame];
    viewMovie.backgroundColor = [UIColor whiteColor];
    
    NSString *videoPath = [[NSBundle mainBundle] pathForResource:@"video-help-iphone" ofType:@"mp4"];
    NSURL *videoUrl = [NSURL fileURLWithPath:videoPath];
    
    MPMoviePlayerController *moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:videoUrl];
    
    moviePlayer.controlStyle = MPMovieControlStyleNone;
    
    CGRect rect = sender.view.frame;
    rect.size.height -= 40;
    moviePlayer.view.frame = rect;
    [viewMovie addSubview:moviePlayer.view];
    
    UIButton *buttonOk = [UIButton buttonWithType:UIButtonTypeSystem];
    buttonOk.frame = CGRectMake(0, rect.size.height, rect.size.width, 40);
    [buttonOk setTitle:@"Pular ajuda" forState:UIControlStateNormal];
    [buttonOk addTarget:sender action:callback forControlEvents:UIControlEventTouchUpInside];
    buttonOk.backgroundColor = [UIColor whiteColor];
    [viewMovie addSubview: buttonOk];
    
    [sender.view addSubview:viewMovie];
    
    [moviePlayer prepareToPlay];
    [moviePlayer play];
    
    [[NSNotificationCenter defaultCenter] addObserver:sender
                                             selector:callback
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:moviePlayer];
    
    return moviePlayer;
}


@end
