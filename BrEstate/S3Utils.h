//
//  S3Utils.h
//  Appci
//
//  Created by Alexandre Oliveira on 1/13/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface S3Utils : NSObject

+ (void)delete:(NSString *)filePath;
+ (NSData *)download:(NSString *)filePath;

@end
