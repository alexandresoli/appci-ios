//
//  S3Utils.m
//  Appci
//
//  Created by Alexandre Oliveira on 1/13/15.
//  Copyright (c) 2015 BrEstate LTDA. All rights reserved.
//

#import "S3Utils.h"
#import "ASIS3ObjectRequest.h"

@implementation S3Utils

#pragma mark - DELETE
+ (void)delete:(NSString *)filePath
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [self addPendingDelete:filePath];
        
        if(![Utils isConnected]) {
            return;
        }
        
        ASIS3ObjectRequest *request = [ASIS3ObjectRequest DELETERequestWithBucket:S3_BUCKET_NAME
                                                                              key:filePath];
        [request startSynchronous];
        if (request.error) {
            NSLog(@"%@", request.error.localizedDescription);
        } else {
            [self removePendingDelete:filePath];
        }
        
    });
    
}

+ (void)addPendingDelete:(NSString *)filename
{
    
    NSMutableArray *toDeleteArray = [[Utils valueFromDefaults:S3_PENDING_DELETED_FILE] mutableCopy];
    if (!toDeleteArray) {
        toDeleteArray = [NSMutableArray array];
    }
    
    [toDeleteArray addObject:filename];
    
}

+ (void)removePendingDelete:(NSString *)filename
{
    NSMutableArray *toDeleteArray = [[Utils valueFromDefaults:S3_PENDING_DELETED_FILE] mutableCopy];
    
    [toDeleteArray removeObject:filename];
    
}

#pragma mark - DOWNLOAD
+ (NSData *)download:(NSString *)filePath
{
    @autoreleasepool {
        
        ASIS3ObjectRequest *request = [ASIS3ObjectRequest requestWithBucket:S3_BUCKET_NAME key:filePath];
        [request setSecretAccessKey:S3_SECRET_KEY];
        [request setAccessKey:S3_ACCESS_KEY];
        
        [request startSynchronous];
        if (![request error]) {
            return [request responseData];
        } else {
            NSLog(@"%@ - %@",[[request error] localizedDescription],filePath);
        }
        
    }
    return nil;
}


@end
