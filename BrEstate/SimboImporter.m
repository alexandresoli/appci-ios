//
//  SimboImporter.m
//  Appci
//
//  Created by BrEstate LTDA on 4/28/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "SimboImporter.h"
#import "Imovel.h"
#import "TagImovel.h"
#import "Endereco.h"
#import "Estado.h"
#import "Cidade.h"
#import "Bairro.h"
#import "DateUtils.h"
#import "FotoImovel+InsertDeleteImagem.h"
#import "NSString+HTML.h"
#import "NSString+Custom.h"
#import "SimboViewController.h"
#import "ASIHTTPRequest.h"
#import "XMLDictionary.h"

@interface SimboImporter ()

#pragma mark - Properties

@property (nonatomic, strong) NSString *checksum;
@property (nonatomic, strong) NSMutableArray *retryPhotos;
@property (nonatomic, strong) NSDictionary *dictionary;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSMutableArray *imoveis;
@property (nonatomic) int totalPhotos;
@property (nonatomic) BOOL forceSync;

@end

@implementation SimboImporter


#pragma mark - Start XML Parsing
- (void)startWithURL:(NSString *)strURL usingMoc:(NSManagedObjectContext *)moc forced:(BOOL)forced
{
    
    [Utils startNetworkIndicator];
    
    _forceSync = forced;
    _retryPhotos = [NSMutableArray array];
    _photos = [NSMutableArray array];
    
    NSURL *url = [NSURL URLWithString:strURL.trimmed];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    request.timeOutSeconds = 30;
    [request startSynchronous];
    NSError *error = [request error];
    
    if (error) {
        [Utils dismissProgress];
        [Utils showMessage:MESSAGE_IMPORT_ERROR];\
        [Utils stopNetworkIndicator];
        return;
    }
    
    NSString *xml = request.responseString;
    NSString *newChecksum = [xml MD5];
    
    if (!_forceSync) {
        
        // checksum check
        NSString *oldChecksum = [Utils valueFromDefaults:SIMBO_CHECKSUM];
        if ([newChecksum isEqualToString:oldChecksum]) {
            
            [Utils dismissProgress];
            [Utils stopNetworkIndicator];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Aviso" message:@"As informações já estão atualizadas, deseja forçar a sincronização?" delegate:_parentController cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Forçar", nil];
                [alertView show];
            });
            
            
            return;
        }
        
    }
    
    
    _checksum = newChecksum;
    
    [Utils stopNetworkIndicator];
    [Utils dismissProgress];
    
    _parentController.hasStarted = YES;
    _dictionary = [NSDictionary dictionaryWithXMLData:request.responseData];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        
        [localContext performBlock:^{
            [self convert:localContext];
            
            if ([self.imoveis count] == 0) {
                [Utils showMessage:MESSAGE_EMPTY_IMPORT];
                [Utils dismissProgress];
                return;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                _progressView.progress = 0.0f;
                _progressLabel.text = @"";
                
            });
            
            // update checksum
            [Utils addObjectToDefaults:_checksum withKey:SIMBO_CHECKSUM];
            
            [self startBackgroundDownloadsForBaseURL:localContext];
            
            if (_retryPhotos.count > 0) {
                [self redownloadPhotos:localContext];
            }
            
            [Utils dismissProgress];
            [Utils postEntitytNotification:@"Imovel"];
            [Utils stopNetworkIndicator];
            
            if (_parentController != nil) {
                [Utils showMessage:MESSAGE_FINISH_IMPORT];
            }
            
            [_parentController setHasStarted:NO];
            
        }];
        
    });
    
}


- (void)convert:(NSManagedObjectContext *)moc
{
    NSNumberFormatter *currencyFmt = [[NSNumberFormatter alloc] init];
    
    
    _imoveis = [[_dictionary objectForKey:@"SimboRealtyList"] objectForKey:@"SimboRealty"];
    
    __block long total = [_imoveis count];
    __block int step = 0;
    __block float progressStep = 0.0f;
    
    for (NSDictionary *obj in _imoveis) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/total;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Importando: %d/%ld",step, total];
        });
        
        @autoreleasepool {
            
            long count = 0;
            
            Imovel *imovel = nil;
            
            
            NSString *identificacao = [obj[@"Ref"] trimmed];
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"identificacao = %@",identificacao];
            
            count = [Imovel MR_countOfEntitiesWithPredicate:predicate inContext:moc];
            
            BOOL exists = NO;
            if(count > 0 ) {
                imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:moc];
                exists = YES;
                imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
            } else {
                
                imovel = [Imovel MR_createInContext:moc];
                imovel.codigo = [imovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
                
            }
            
            // flag importação
            imovel.importacao = [NSNumber numberWithBool:YES];
            
            // only continues if date if different
            NSDate *lastUpdate = [DateUtils dateFromString:[obj[@"LastUpdateDate"] trimmed] withFormat:@"MM-dd-yyyy HH:mm:ss"];;
            
            if ([imovel.lastUpdate isEqualToDate:lastUpdate] && !_forceSync) {
                imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
                continue;
            }
            
            imovel.lastUpdate = lastUpdate;
            imovel.version = [NSDate date];
            
            // Identificacao
            if (identificacao.length > 0) {
                imovel.identificacao = [identificacao stringByDecodingHTMLEntities].trimmed;
            }
            
            imovel.descricao = [obj[@"Info"] stringByDecodingHTMLEntities].trimmed;
            
            // Tipo Oferta
            NSNumber *valor;
            if([[obj[@"AvailableToSell"] trimmed] boolValue]) {
                imovel.oferta = @"Venda";
                
                NSDictionary *sellPrice = obj[@"SellPrice"];
                
                NSString *str = [[sellPrice[@"__text"] trimmed] stringByReplacingOccurrencesOfString:@".0" withString:@""];
                valor = [currencyFmt numberFromString:str];
                
            } else if([[obj[@"AvailableToRent"] trimmed] boolValue]) {
                imovel.oferta = @"Locação";
                
                NSDictionary *rentPrice = obj[@"RentPrice"];
                NSString *str = [[rentPrice[@"__text"] trimmed] stringByReplacingOccurrencesOfString:@".0" withString:@""];
                valor = [currencyFmt numberFromString:str];
            } else {
                imovel.oferta = @"Outro";
            }
            
            if ([valor intValue] > 0) {
                imovel.valor = [NSDecimalNumber decimalNumberWithDecimal:[valor decimalValue]];
            }
            
            // Tipo Imovel
            NSString *type = [obj[@"Type"] trimmed];
            
            if([type isEqualToString:@"Apartamento"]) {
                imovel.tipo = @"Apartamento";
            } else if([type isEqualToString:@"Casa"]) {
                imovel.tipo = @"Casa";
            } else if([type isEqualToString:@"Comercial"]) {
                imovel.tipo = @"Sala / Espaço Comercial";
            } else if([type isEqualToString:@"Rural"]) {
                imovel.tipo = @"Fazenda / Sítio";
            } else if([type isEqualToString:@"Terreno"]) {
                imovel.tipo = @"Terreno";
            } else {
                imovel.tipo = @"Outro";
            }
            
            // area util / area total
            
            NSDictionary *utilArea = obj[@"UtilArea"];
            NSNumber *areaUtil = [NSNumber numberWithLong:[utilArea[@"__text"] integerValue]];
            if ([areaUtil intValue] > 0) {
                imovel.areaUtil = areaUtil;
            }
            
            NSDictionary *totalArea = obj[@"TotalArea"];
            NSNumber *areaTotal = [NSNumber numberWithLong:[totalArea[@"__text"] integerValue]];
            if ([areaTotal intValue] > 0) {
                imovel.areaTotal = areaTotal;
            }
            
            
            // quartos
            NSNumber *quartos = [NSNumber numberWithLong:[obj[@"Rooms"] integerValue]];
            
            if ([quartos intValue] > 0) {
                imovel.quartos = quartos;
            }
            
            
            // tags
            TagImovel *tag;
            
            NSMutableArray *features = [NSMutableArray array];
            if ([obj objectForKey:@"Features"] != nil && ![[obj objectForKey:@"Features"] isKindOfClass:[NSArray class]]) {
                
                NSDictionary *feature = [[obj objectForKey:@"Features"] objectForKey:@"Feature"];
                
                [features addObject:feature];
            } else {
                features = [obj objectForKey:@"Features"];
            }
            
            if (features.count > 0) {
                
                if (imovel.tags == nil) {
                    
                    tag = [TagImovel MR_createInContext:moc];
                    tag.codigo = [tag getUniqueCode]; //[NSNumber numberWithLong:codigo];
                    
                } else {
                    tag = imovel.tags;
                }
                
            }
            
            for (NSDictionary *feature in features.firstObject) {
                
                if ([feature isKindOfClass:[NSString class]]) {
                    continue;
                }
                
                NSString *text = [feature[@"__text"] trimmed];
                
                if ([text containsString:@"Armários nos dormitórios"]) {
                    tag.armarioEmbutido = [NSNumber numberWithBool:YES];
                }
                
                if ([text containsString:@"Salão de festas"]) {
                    tag.salaoFestas = [NSNumber numberWithBool:YES];
                }
                
                if ([text containsString:@"Varanda"]) {
                    tag.varanda = [NSNumber numberWithBool:YES];
                }
                
                if ([text containsString:@"Armários na cozinha"]) {
                    tag.armarioCozinha = [NSNumber numberWithBool:YES];
                }
                
                if ([text containsString:@"Churrasqueira"]) {
                    tag.churrasqueira = [NSNumber numberWithBool:YES];
                }
                
                if ([text containsString:@"Piscina"]) {
                    tag.piscina = [NSNumber numberWithBool:YES];
                }
                
                if ([text containsString:@"Quadra poliesportiva"]) {
                    tag.quadraPoliesportiva = [NSNumber numberWithBool:YES];
                }
                
                if ([text containsString:@"Salão jogos"]) {
                    tag.salaoJogos = [NSNumber numberWithBool:YES];
                }
                
                if ([text containsString:@"Salão festas"]) {
                    tag.salaoFestas = [NSNumber numberWithBool:YES];
                }
                
                if ([text containsString:@"Sauna"]) {
                    tag.sauna = [NSNumber numberWithBool:YES];
                }
                
                if ([text containsString:@"Academia"]) {
                    tag.salaGinastica = [NSNumber numberWithBool:YES];
                }
                
                if ([text containsString:@"Playground"]) {
                    tag.playground = [NSNumber numberWithBool:YES];
                }
                
                if ([text containsString:@"Children care"]) {
                    tag.childrenCare = [NSNumber numberWithBool:YES];
                }
                
                
                
                
                
                NSNumber *parking = [NSNumber numberWithLong:[obj[@"Parking"] integerValue]];
                
                if (parking > 0) {
                    tag.garagem = parking;
                }
                
            }
            
            imovel.tags = tag;
            
            
            // Endereco
            Endereco *endereco;
            if (imovel.endereco != nil) {
                
                endereco = imovel.endereco;
                
            } else {
                
                endereco = [Endereco MR_createInContext:moc];
                endereco.codigo = [endereco getUniqueCode]; //[NSNumber numberWithLong:codigo];
            }
            
            NSDictionary *location = obj[@"Location"];
            
            double latitude = [[[location valueForKey:@"Latitude"] trimmed] doubleValue];
            double longitude = [[[location valueForKey:@"Longitude"] trimmed] doubleValue];
            
            endereco.latitude = [NSNumber numberWithDouble:latitude];
            endereco.longitude = [NSNumber numberWithDouble:longitude];
            
            endereco.logradouro = [[location valueForKey:@"Address"] trimmed];
            
            // logradouro
            if ([[[location valueForKey:@"Number"] trimmed] length] > 0) {
                
                NSString *number = [NSString stringWithFormat:@", %@",[[location valueForKey:@"Number"] trimmed]];
                endereco.logradouro = [endereco.logradouro stringByAppendingString:number];
            }
            
            
            // cep
            if ([[[location objectForKey:@"PostalCode"] trimmed] length] > 0) {
                NSString *cep = [[[location objectForKey:@"PostalCode"] trimmed] stringByReplacingOccurrencesOfString:@"-" withString:@""];
                endereco.cep = [NSNumber numberWithInt:[cep intValue]];
            }
            
            // Estado
            NSString *state = [[location valueForKey:@"State"] trimmed];
            
            if (state.length > 0) {
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sigla == [cd] %@",state];
                Estado *estado = [Estado MR_findFirstWithPredicate:predicate inContext:moc];
                endereco.estado = estado.nome;
                
            }
            
            
            // Cidade
            NSString *city = [[location valueForKey:@"City"] trimmed];
            
            Cidade *cidade;
            if (city.length > 0) {
                
                predicate = [NSPredicate predicateWithFormat:@"nome == [cd] %@",city];
                cidade = [Cidade MR_findFirstWithPredicate:predicate inContext:moc];
                endereco.cidade = cidade.nome;
            }
            
            
            // Bairro
            NSString *neighborhood = [[location valueForKey:@"Neighborhood"] trimmed];
            
            if (neighborhood.length > 0) {
                
                predicate = [NSPredicate predicateWithFormat:@"cidade == [cd] %@ AND nome == [cd] %@",city, neighborhood];
                count = [Bairro MR_countOfEntitiesWithPredicate:predicate inContext:moc];
                Bairro *bairro = nil;
                
                if(count == 0 ) {
                    
                    bairro = [Bairro MR_createInContext:moc];
                    bairro.codigo = [bairro getUniqueCode]; //[NSNumber numberWithLong:codigo];
                    bairro.nome = neighborhood;
                    bairro.cidade = cidade.nome;
                    bairro.version = [NSDate date];
                    
                } else {
                    
                    predicate = [NSPredicate predicateWithFormat:@"cidade == [cd] %@ AND nome == [cd] %@",cidade.nome, neighborhood];
                    bairro = [Bairro MR_findFirstWithPredicate:predicate inContext:moc];
                }
                
                endereco.bairro = bairro;
            }
            
            
            imovel.endereco = endereco;
            
            
            // fotos
            if (exists) {
                [Utils deleteAllPhotos:imovel withMoc:moc];
            }
            
            NSDictionary *media = [[obj objectForKey:@"Media"] objectForKey:@"MediaItem"];
            for (NSDictionary *photo in media) {
                NSMutableDictionary *newMedia = [NSMutableDictionary dictionaryWithDictionary:photo];
                [newMedia setValue:imovel.codigo forKey:@"codigoImovel"];
                [_photos addObject:newMedia];
                _totalPhotos++;
            }
            
            imovel.isSavedInFirebase = [NSNumber numberWithBool:NO];
            [moc MR_saveToPersistentStoreAndWait];
            
        }
        
    }
    
    
}

#pragma mark - Photo Download
- (void)startBackgroundDownloadsForBaseURL:(NSManagedObjectContext *)moc
{
    [Utils startNetworkIndicator];
    
    __block int step = 0;
    __block float progressStep = 0.0;
    
    for (NSDictionary *photo in _photos)
    {
        
        if (_parentController == nil) {
            break;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/_totalPhotos;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Transferindo fotos: %d/%d",step, _totalPhotos];
        });
        
        @autoreleasepool {
            [self downloadPhoto:photo usingMoc:moc];
        }
    }
}

- (void)redownloadPhotos:(NSManagedObjectContext *)moc
{
    __block int step = 0;
    __block float progressStep = 0.0;
    
    for(NSDictionary *photo in _retryPhotos){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            step++;
            progressStep += 1.0/_retryPhotos.count;
            [_progressView setProgress:progressStep animated:YES];
            _progressLabel.text = [NSString stringWithFormat:@"Reparando fotos : %d/%lu",step, (unsigned long)_retryPhotos.count];
            
        });
        
        @autoreleasepool {
            [self downloadPhoto:photo usingMoc:moc];
        }
    }
}

- (void)downloadPhoto:(NSDictionary *)photo usingMoc:(NSManagedObjectContext *)moc
{
    
    NSString *strURL = [photo[@"__text"] trimmed];
    
    if (strURL.length == 0) {
        return;
    }
    
    NSURL *photoURL = [NSURL URLWithString:strURL];
    
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:photoURL];
    request.timeOutSeconds = 60;
    [request startSynchronous];
    NSError *error = [request error];
    
    if (error) {
        
        [_retryPhotos addObject:photo];
        NSLog(@"%@",[error description]);
    }
    
    FotoImovel *fotoImovel = [FotoImovel MR_createInContext:moc];
    fotoImovel.codigo = [fotoImovel getUniqueCode]; //[NSNumber numberWithLong:codigo];
    
    fotoImovel.descricao = [[strURL lastPathComponent] stringByDeletingPathExtension];
    fotoImovel.importacao = [NSNumber numberWithBool:YES];
    
    NSNumber *codigoImovel = photo[@"codigoImovel"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"codigo = %@",codigoImovel];
    fotoImovel.imovel = [Imovel MR_findFirstWithPredicate:predicate inContext:moc];
    
    fotoImovel.capa = [NSNumber numberWithBool:[photo[@"_main"] boolValue]];
    
    [fotoImovel insertImageOnDisk:request.responseData];
    
    [moc MR_saveToPersistentStoreAndWait];
}

@end
