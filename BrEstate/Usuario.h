//
//  Usuario.h
//  Appci
//
//  Created by Alexandre Oliveira on 11/3/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Usuario : NSManagedObject

@property (nonatomic, retain) NSString * nome;
@property (nonatomic, retain) NSString * telefone;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSDate *version;
@property (nonatomic, retain) NSString *parseID;
@property (nonatomic, retain) NSString * role;
@property (nonatomic, retain) NSString * imobiliaria;

@end
