//
//  Usuario.m
//  Appci
//
//  Created by Alexandre Oliveira on 11/3/14.
//  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
//

#import "Usuario.h"


@implementation Usuario

@dynamic nome;
@dynamic telefone;
@dynamic email;
@dynamic role;
@dynamic imobiliaria;
@dynamic version;
@dynamic parseID;

@end
