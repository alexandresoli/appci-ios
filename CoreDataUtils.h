//
//  CoreDataUtils.h
//  CatalogoLeisANS
//
//  Created by BrEstate LTDA on 11/24/11.
//  Copyright (c) 2011 BRQ IT Services. All rights reserved.
//

#import <Foundation/Foundation.h>

/** Responsável pelas principais operações envolvendo o banco de dados (CoreData). */
@interface CoreDataUtils : NSObject


/**---------------------------------------------------------------------------------------
 * @name Métodos
 *  ---------------------------------------------------------------------------------------
 */

/** Método utilizado para obter o conteúdo de uma entidade.
 
 @param entityName nome da entidade.
 @param predicate NSPredicate contendo o filtro necessário para obter a entidade.
 @param moc NSManagedObjectContext a ser utilizado.
 @return NSArray contendo a lista de entidades recuperadas.
 
 */
+ (NSArray *)fetchEntity:(NSString *)entityName  withPredicate:(NSPredicate *)predicate inContext:(NSManagedObjectContext *)moc;

/** Método utilizado para obter o total de objetos de uma entidade.
 
 @param entityName nome da entidade.
 @param predicate NSPredicate contendo o filtro necessário para obter a entidade.
 @param moc NSManagedObjectContext a ser utilizado.
 @return int contendo o total de entidades encontradas.
 
 */
+ (long)countEntity:(NSString *)entityName  withPredicate:(NSPredicate *)predicate inContext:(NSManagedObjectContext *)moc;

/** Método utilizado para obter o total de objetos de uma entidade.
 
 Este método normalmente é utilizado para o gerenciamento de registros em uma tabela.
 
 @param entityName nome da entidade.
 @param key chave utilizada para indicar qual atributo irá ser utilizado para ordenar a lista.
 @param predicate NSPredicate contendo o filtro necessário para obter a entidade.
 @param order BOOL indicando se o resultado virá na ordem crescente ou decrescente.
 @param limit limite de registros buscados por vez.
 @param moc NSManagedObjectContext a ser utilizado.
 @return int contendo o total de entidades encontradas.
 
 */
+ (NSFetchedResultsController *)fetchedResultsControllerWithEntity:(NSString *)entityName andKey:(NSString *)key withPredicate:(NSPredicate *)predicate withOrder:(BOOL) order withLimit:(int)limit inContext:(NSManagedObjectContext *)moc;

+ (NSFetchedResultsController *)fetchedResultsControllerWithEntity:(NSString *)entityName andKey1:(NSString *)key1 andKey2:(NSString *)key2 withSectionName:(NSString *)sectionName withPredicate:(NSPredicate *)predicate withOrder:(BOOL)order withLimit:(int)limit inContext:(NSManagedObjectContext *)moc;
+ (NSArray *)fetchEntityID:(NSString *)entityName  withPredicate:(NSPredicate *)predicate inContext:(NSManagedObjectContext *)moc;


+ (NSFetchedResultsController *)fetchedResultsControllerWithAlphabetic:(NSString *)entityName andKey:(NSString *)key withPredicate:(NSPredicate *)predicate withOrder:(BOOL)order withLimit:(int)limit inContext:(NSManagedObjectContext *)moc;

+ (NSArray *)fetchEntity:(NSString *)entityName  withPredicate:(NSPredicate *)predicate withKey:(NSString *)key withOrder:(BOOL)order inContext:(NSManagedObjectContext *)moc;
+ (NSFetchedResultsController *)fetchedResultsControllerWithEntity2:(NSString *)entityName andKey:(NSString *)key withPredicate:(NSPredicate *)predicate withOrder:(BOOL)order withLimit:(int)limit inContext:(NSManagedObjectContext *)moc;

+ (NSArray *)fetchEntityAsDictionary:(NSString *)entityName  withPredicate:(NSPredicate *)predicate inContext:(NSManagedObjectContext *)moc;

+ (void)emptyEntity:(NSString *)name inContext:(NSManagedObjectContext *)moc;

+ (NSFetchedResultsController *)fetchedResultsControllerWithEntity:(NSString *)entityName
                                                       withSortKey:(NSString *)sortKey
                                                   withSectionName:(NSString *)sectionName
                                                     withPredicate:(NSPredicate *)predicate
                                                         withOrder:(BOOL)order
                                                         withLimit:(int)limit
                                                         inContext:(NSManagedObjectContext *)moc;

@end
