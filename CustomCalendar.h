//
//  MyCalendar.h
//  BrEstate
//
//  Created by BrEstate LTDA on 11/14/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Evento;
@interface CustomCalendar : NSObject

+ (void)requestAccess:(void (^)(BOOL granted, NSError *error))success;
+ (BOOL)addEvent:(Evento *)evento withMoc:(NSManagedObjectContext *)moc andOldStartDate:(NSDate *)oldStartDate andOldEndDate:(NSDate *)oldEndDate;
+ (BOOL)deleteEventWithIdentifier:(NSString *)identifier andStartDate:(NSDate *)startDate andEndDate:(NSDate *)endDate;
+ (void)importEventsShowReturnMessage:(BOOL)showMessage;

@end
