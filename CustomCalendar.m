//
//  MyCalendar.,
//  BrEstate
//
//  Created by BrEstate LTDA on 11/14/13.
//  Copyright (c) 2013 BrEstate LTDA. All rights reserved.
//

#import "CustomCalendar.h"
#import "Evento+Firebase.h"

@import EventKit;

#import "Evento+AlarmeLocalNotification.h"
#import "Alarme.h"
#import "CoreDataUtils.h"


static EKEventStore *eventStore = nil;

@implementation CustomCalendar


+ (void)requestAccess:(void (^)(BOOL granted, NSError *error))callback;
{
    if (eventStore == nil) {
        eventStore = [[EKEventStore alloc] init];
    }
    // request permissions
    [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:callback];
}


+ (EKCalendar *)getAppciCalendar
{
    return [eventStore defaultCalendarForNewEvents];
}


+ (EKEvent *)eventWithIdentifier:(NSString *)identifier andStartDate:(NSDate *)startDate andEndDate:(NSDate *)endDate
{
    EKEvent *returnEvent;
    
    NSPredicate *predicate = [eventStore predicateForEventsWithStartDate:startDate
                                                                 endDate:endDate
                                                               calendars:@[[CustomCalendar getAppciCalendar]]];
    
    NSArray *eventArray = [eventStore eventsMatchingPredicate:predicate];
    
    for (EKEvent *event in eventArray) {
        if ([identifier isEqualToString:event.calendarItemExternalIdentifier]) {
            returnEvent = event;
            break;
        }
    }
    
    return returnEvent;
}


+ (BOOL)addEvent:(Evento *)evento withMoc:(NSManagedObjectContext *)moc andOldStartDate:(NSDate *)oldStartDate andOldEndDate:(NSDate *)oldEndDate
{
    EKEvent *event;
    BOOL needUpdateFirebase = YES;
    
    if (evento.identificadorEventoCalendario) { // Evento já existe e está sendo alterado.
        
        event = [CustomCalendar eventWithIdentifier:evento.identificadorEventoCalendario andStartDate:oldStartDate andEndDate:oldEndDate];
        
        // Se não encontrou na agenda um evento com o identificador gravado não cria pois pode ter vindo de outro device.
        if (!event) {
            return YES;
        }
        
        // Não precisa atualizar no firebase porque o identificadorEventoCalendario já existe.
        needUpdateFirebase = NO;
        
    } else {
        event = [EKEvent eventWithEventStore:eventStore];
    }
    
    EKCalendar *calendar = [CustomCalendar getAppciCalendar];
    
    // this shouldn't happen
    if (!calendar) {
        return NO;
    }
    
    // assign basic information to the event; location is optional
    event.calendar = calendar;
    
    if (evento.local != nil) {
        event.location = evento.local;
    }
    
    event.title = evento.titulo;
    
    if (evento.notas != nil) {
        event.notes = evento.notas;
    }
    
    event.URL = [NSURL URLWithString:@"http://www.brestate.com.br"];
    
    event.alarms = nil;
    
    event.startDate = evento.dataInicio;
    event.endDate = evento.dataFim;
    
    NSError *error = nil;
    
    // save event to the callendar
    BOOL result = [eventStore saveEvent:event span:EKSpanThisEvent commit:YES error:&error];
    
    if (result) {
        evento.identificadorEventoCalendario = event.calendarItemExternalIdentifier; //event.eventIdentifier;
        
        [moc MR_saveToPersistentStoreAndWait];
        
        // Firebase
        if (needUpdateFirebase) {
            [evento saveFirebase];
        }
        
        return YES;
        
    } else {
        // unable to save event to the calendar
        NSLog(@"Error saving event: %@", error);
        return NO;
    }
    
}


+ (BOOL)deleteEventWithIdentifier:(NSString *)identifier andStartDate:(NSDate *)startDate andEndDate:(NSDate *)endDate
{
    NSError *error = nil;
    EKEvent *event;
    
    if (identifier) {
        event = [CustomCalendar eventWithIdentifier:identifier andStartDate:startDate andEndDate:endDate];
        
        if (event) {
            [eventStore removeEvent:event span:EKSpanThisEvent commit:YES error:&error];
        }
    }
    
    if (error) {
        return NO;
        
    } else {
        return YES;
    }
}


+ (void)importEventsShowReturnMessage:(BOOL)showMessage
{
    [CustomCalendar requestAccess:^(BOOL granted, NSError *error) {
        
        if (granted) {
            
            __block BOOL saveEventos = NO;
            
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                
                // Obtem o calendário default utilizado pelo usuário.
//                EKCalendar *defaultCalendar = [CustomCalendar getAppciCalendar];
//                
//                if (defaultCalendar == nil) {
//                    return;
//                }
                
                NSArray *calendars = [eventStore calendarsForEntityType:EKEntityTypeEvent];
                
                NSPredicate *predicateCalendars = [NSPredicate predicateWithFormat:@"allowsContentModifications = YES"];
                NSArray *editableCalendars = [calendars filteredArrayUsingPredicate:predicateCalendars];
                
                NSCalendar *calendar = [NSCalendar currentCalendar];
                
                // Obtem a data de um mês atrás da data atual.
                NSDateComponents *oneMonthAgoComponents = [[NSDateComponents alloc] init];
                oneMonthAgoComponents.month = -1;
                NSDate *oneMonthAgo = [calendar dateByAddingComponents:oneMonthAgoComponents
                                                                toDate:[NSDate date]
                                                               options:0];
                
                // Obtem a data de um ano a frente da data atual.
                NSDateComponents *oneYearFromNowComponents = [[NSDateComponents alloc] init];
                oneYearFromNowComponents.year = 1;
                NSDate *oneYearFromNow = [calendar dateByAddingComponents:oneYearFromNowComponents
                                                                   toDate:[NSDate date]
                                                                  options:0];
                
                // Cria o predicate para buscar os eventos.
                NSPredicate *predicate = [eventStore predicateForEventsWithStartDate:oneMonthAgo
                                                                             endDate:oneYearFromNow
                                                                           calendars:editableCalendars];
                
                NSArray *events = [eventStore eventsMatchingPredicate:predicate];
                
                long count = 0;
                for (EKEvent *calendarEvent in events) {
                    count++;
                    
                    // Verifica se o evento já existe na base.
                    NSPredicate *checkPredicate = [NSPredicate predicateWithFormat:@"titulo = %@ AND dataInicio = %@ AND dataFim = %@", calendarEvent.title, calendarEvent.startDate, calendarEvent.endDate];
                    
                    NSArray *checkEventoArray = [Evento MR_findAllWithPredicate:checkPredicate];
                    
                    // Se o evento já existe na base atualiza o identificadorEventoCalendario para o externalIdentifier.
                    // Necessário pois até a versão 1.2 era utilizado o identifier normal do evento.
                    if (checkEventoArray.count > 0) {
                        
                        NSString *externalIdentifier = calendarEvent.calendarItemExternalIdentifier;
                        

                        
                        saveEventos = YES;
                        
                        for (Evento *eventoFromArray in checkEventoArray) {
                            
                            // evento recursivo
                            if (calendarEvent.recurrenceRules.count > 0) {
                                eventoFromArray.recursivo = [NSNumber numberWithBool:YES];
                                saveEventos = YES;
                            }
                            
                            if (![eventoFromArray.identificadorEventoCalendario isEqualToString:externalIdentifier]) {
                                eventoFromArray.identificadorEventoCalendario = externalIdentifier;
                                
                                saveEventos = YES;
                            }
                            
                            
                        }
                        
                    } else {
                        
                        Evento *evento;
                        
                        // Busca o evento com o mesmo identificador de calendário.
//                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"identificadorEventoCalendario = %@", calendarEvent.calendarItemExternalIdentifier];
//                        evento = [Evento MR_findFirstWithPredicate:predicate inContext:localContext];
                        
//                        if (!evento) {
                        evento = [Evento MR_createInContext:localContext];
                        evento.codigo = [evento getUniqueCode]; //[NSNumber numberWithLong:time]; //[NSNumber numberWithLong:count];
  //                      }
  
                        if (calendarEvent.title > 0) {
                            evento.titulo = calendarEvent.title;
                            
                        } else {
                            evento.titulo = @"Novo Evento";
                        }
                        
                        evento.local = calendarEvent.location;
                        
                        evento.dataInicio = calendarEvent.startDate;
                        evento.dataFim = calendarEvent.endDate;
                        
                        // evento recursivo
                        if (calendarEvent.recurrenceRules.count > 0) {
                            evento.recursivo = [NSNumber numberWithBool:YES];
                        }
                        
                        // Alarmes.
                        NSMutableArray *eventoAlarmes = [[NSMutableArray alloc] init];
                        int countAlarmes = 0;
                        
                        for (EKAlarm *eventAlarm in calendarEvent.alarms) {
                            
                            if (countAlarmes < 2) {
                                NSNumber *timeInterval = [NSNumber numberWithInt:(int)eventAlarm.relativeOffset];
                                
                                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"tempo = %@", timeInterval];
                                NSArray *alarmesArray = [Alarme MR_findAllWithPredicate:predicate];
                                
                                Alarme *alarme = [alarmesArray firstObject];
                                
                                if (alarme) {
                                    [eventoAlarmes addObject:alarme.codigo];
                                    countAlarmes++;
                                }
                            }
                        }
                        
                        evento.alarmes = [NSKeyedArchiver archivedDataWithRootObject:eventoAlarmes];
                        
                        [evento registerAlarmesForLocalNotification:localContext];
                        
                        // Não preenche o version para que o deduplicator dê preferência ao evento que já foi atualizado no AppCi.
                        //evento.version = [NSDate date];
                        
                        evento.identificadorEventoCalendario = calendarEvent.calendarItemExternalIdentifier; //calendarEvent.eventIdentifier;
                        
                        if (evento.firebaseID) {
                            [evento saveFirebase];
                        }
                        
                        saveEventos = YES;
                    }
                }
                
            } completion:^(BOOL success, NSError *error) {
                
                if (saveEventos) {
                    [Utils postEntitytNotification:@"Evento"];
                }
                
                if (showMessage) {
                    [Utils dismissProgress];
                    [Utils showMessage:@"Seus eventos foram importados para o AppCi."];
                }
                
            }];
            
        } else if (showMessage) {
            [Utils dismissProgress];
            [Utils showMessage:@"Você precisa autorizar o acesso aos seus eventos.\n Para isso siga os passos: \n\n- Saia do AppCi. \n- Abra o Ajustes do dispositivo. \n- Selecione Privacidade. \n- Selecione Calendários. \n- Altere a opção Ativado/Desativado ao lado do AppCi para Ativado."];
            
        }
    }];
}


@end