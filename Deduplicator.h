////
////  Deduplicator.h
////  Appci
////
////  Created by BrEstate LTDA on 05/04/14.
////  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
////
//
//#import <Foundation/Foundation.h>
//
//@interface Deduplicator : NSObject
//
//
//// Dedup Methods
//- (void)deDuplicateEntityWithName:(NSString*)entityName
//          withUniqueAttributeName:(NSString*)uniqueAttributeName;
//
//- (void)deDupeCliente;
//
//- (void)deDupeEvento;
//
//
//@end
