////
////  Deduplicator.m
////  Appci
////
////  Created by BrEstate LTDA on 05/04/14.
////  Copyright (c) 2014 BrEstate LTDA. All rights reserved.
////
//
//#import "Deduplicator.h"
//#import "Cliente.h"
//#import "Evento.h"
//
//
//@interface Deduplicator ()
//
//
//@property(nonatomic, strong) NSManagedObjectContext *localContext;
//
//@end
//
//
//@implementation Deduplicator
//
//
//#pragma mark - Designated Initializer
//
//- (id)init
//{
//    self = [super init];
//    
//    if (self) {
//        self.localContext = [NSManagedObjectContext MR_contextWithParent:[NSManagedObjectContext MR_defaultContext]];
//    }
//    
//    return self;
//}
//
//
//
//#pragma mark - Default Deduplicator - Suport Method - Get duplicates
//
//- (NSArray *)duplicatesForEntityWithName:(NSString*)entityName withUniqueAttributeName:(NSString*)uniqueAttributeName
//{
//    
//    // GET UNIQUE ATTRIBUTE
//    NSDictionary *allEntities = [[self.localContext.persistentStoreCoordinator managedObjectModel] entitiesByName];
//    NSAttributeDescription *uniqueAttribute = [[[allEntities objectForKey:entityName] propertiesByName] objectForKey:uniqueAttributeName];
//    
//    // CREATE COUNT EXPRESSION
//    NSExpressionDescription *countExpression = [NSExpressionDescription new];
//    [countExpression setName:@"count"];
//    [countExpression setExpression: [NSExpression expressionWithFormat:@"count:(%K)",uniqueAttributeName]];
//    [countExpression setExpressionResultType:NSInteger64AttributeType];
//    
//    // CREATE AN ARRAY OF _UNIQUE_ ATTRIBUTE VALUES
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entityName];
//    [fetchRequest setIncludesPendingChanges:NO];
//    [fetchRequest setFetchBatchSize:100];
//    [fetchRequest setPropertiesToFetch: [NSArray arrayWithObjects:uniqueAttribute, countExpression, nil]];
//    [fetchRequest setPropertiesToGroupBy:[NSArray arrayWithObject:uniqueAttribute]];
//    [fetchRequest setResultType:NSDictionaryResultType];
//    
//    NSError *error;
//    
//    NSArray *instances = [self.localContext executeFetchRequest:fetchRequest error:&error];
//    if (error) {NSLog(@"Fetch Error: %@", error);}
//    
//    // RETURN AN ARRAY OF _DUPLICATE_ ATTRIBUTE VALUES
//    NSArray *duplicates = [instances filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"count > 1"]];
//    
//    
//    return duplicates;
//}
//
//
//#pragma mark - Default Deduplicator - Main Method
//
//- (void)deDuplicateEntityWithName:(NSString *)entityName withUniqueAttributeName:(NSString *)uniqueAttributeName
//{
//    
//    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
//        
//        NSArray *duplicates = [self duplicatesForEntityWithName:entityName withUniqueAttributeName:uniqueAttributeName];
//        
//        if (duplicates.count > 0) {
//            
//            // FETCH DUPLICATE OBJECTS
//            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entityName];
//            NSArray *sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:uniqueAttributeName ascending:YES],nil];
//            
//            [fetchRequest setSortDescriptors:sortDescriptors];
//            [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"%K IN (%@.%K)", uniqueAttributeName, duplicates, uniqueAttributeName]];
//            [fetchRequest setFetchBatchSize:100];
//            [fetchRequest setIncludesPendingChanges:NO];
//            
//            NSError *error;
//            NSArray *duplicateObjects = [localContext executeFetchRequest:fetchRequest error:&error];
//            
//            if (error) {
//                NSLog(@"Fetch Error: %@", error);
//            }
//            
//            // DELETE DUPLICATES
//            NSManagedObject *lastObject;
//            
//            // Se for a entidade alerta pode apagar qualquer um dos objetos duplicados pois só pode ter um por código.
//            if ([entityName isEqualToString:@"Alerta"]) {
//                for (NSManagedObject *object in duplicateObjects) {
//                    
//                    NSError *error = nil;
//                    if (![localContext existingObjectWithID:object.objectID error:&error]) {
//                        NSLog(@"%@",error);
//                        continue;
//                    }
//                    
//                    
//                    if (lastObject) {
//                        
//                        if ([[object valueForKey:uniqueAttributeName] isEqual:[lastObject valueForKey:uniqueAttributeName]]) {
//                            
//                            //NSLog(@"*** Deleting Duplicate %@ with %@ '%@' ***", entityName, uniqueAttributeName, [object valueForKey:uniqueAttributeName]);
//                            
//                            [object MR_deleteInContext:localContext];
//                        }
//                    }
//                    
//                    lastObject = object;
//                }
//                
//            } else {
//                for (NSManagedObject *object in duplicateObjects) {
//                    
//                    NSError *error = nil;
//                    if (![localContext existingObjectWithID:object.objectID error:&error]) {
//                        NSLog(@"%@",error);
//                        continue;
//                    }
//                    
//                    if (lastObject) {
//                        
//                        if ([[object valueForKey:uniqueAttributeName] isEqual:[lastObject valueForKey:uniqueAttributeName]]) {
//                            
//                            //NSLog(@"*** Deleting Duplicate %@ with %@ '%@' ***", entityName, uniqueAttributeName, [object valueForKey:uniqueAttributeName]);
//                            
//                            NSDate *versionObj = [object valueForKey:@"version"];
//                            NSDate *vesionLastObj = [lastObject valueForKey:@"version"];
//                            
//                            if(versionObj == nil) {
//                                [object MR_deleteInContext:localContext];
//                                
//                            } else  if(vesionLastObj == nil) {
//                                [lastObject MR_deleteInContext:localContext];
//                                
//                            } else if ([versionObj compare:vesionLastObj] == NSOrderedAscending) {                                [object MR_deleteInContext:localContext];
//                                
//                            } else if ([versionObj compare:vesionLastObj] == NSOrderedDescending) {
//                                [lastObject MR_deleteInContext:localContext];
//                                
//                            } else {
//                                [object MR_deleteInContext:localContext];
//                            }
//                            
//                        }
//                    }
//                    
//                    lastObject = object;
//                }
//            }
//            
//        }
//    } completion:^(BOOL success, NSError *error) {
//        
//        [Utils postEntitytNotification:entityName];
//        
//    }];
//    
//}
//
//
//#pragma mark - Dedupe Cliente
//
//- (void)deDupeCliente
//{
//    
//    __block BOOL foundDupe = NO;
//    
//    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
//        
//        NSError *error = nil;
//        
//        NSFetchRequest *fr = [[NSFetchRequest alloc] initWithEntityName:@"Cliente"];
//        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Cliente"];
//        
//        [request setIncludesPendingChanges:NO]; //distinct has to go down to the db, not implemented for in memory filtering
//        [request setFetchBatchSize:1000]; //protect thy memory
//        [request setReturnsDistinctResults:YES];
//        [request setResultType:NSDictionaryResultType];
//        [request setPropertiesToFetch:[NSArray arrayWithObjects:@"nome", @"email", nil]];
//        
//        NSArray *countDictionaries = [localContext executeFetchRequest:request error:&error];
//        
//        NSPredicate *predicate;
//        
//        
//        
//        for (NSDictionary *dict in countDictionaries) {
//            [fr setIncludesPendingChanges:NO];
//            
//            NSString *email = [dict objectForKey:@"email"];
//            
//            if (email == nil) {
//                predicate = [NSPredicate predicateWithFormat:@"nome = %@ AND email = nil", [dict objectForKey:@"nome"]];
//            } else {
//                if ([email isEqualToString:@""]) {
//                    predicate = [NSPredicate predicateWithFormat:@"nome = %@ AND (email = %@ OR email = nil)", [dict objectForKey:@"nome"], [dict objectForKey:@"email"]];
//                } else {
//                    predicate = [NSPredicate predicateWithFormat:@"nome = %@ AND email = %@", [dict objectForKey:@"nome"], [dict objectForKey:@"email"]];
//                }
//                
//            }
//            
//            [fr setPredicate:predicate];
//            
//            NSArray *dupes = [localContext executeFetchRequest:fr error:&error];
//            
//            if (dupes.count > 1) {
//                foundDupe = YES;
//                
//                Cliente *prevPerson = nil;
//                
//                for (Cliente *person in dupes) {
//                    if (prevPerson) {
//                        if (person.version == nil) {
//                            
//                            [person MR_deleteInContext:localContext];
//                            
//                        } else if (prevPerson.version == nil) {
//                            [prevPerson MR_deleteInContext:localContext];
//                            prevPerson = person;
//                            
//                        } else if ([person.version compare:prevPerson.version] == NSOrderedAscending) {
//                            [person MR_deleteInContext:localContext];
//                            
//                        } else {
//                            [prevPerson MR_deleteInContext:localContext];
//                            prevPerson = person;
//                        }
//                        
//                    } else {
//                        prevPerson = person;
//                    }
//                }
//                
//            }
//            
//        }
//        
//    } completion:^(BOOL success, NSError *error) {
//        
//        if (foundDupe) {
//            [Utils postEntitytNotification:@"Contato"];
//        }
//        
//    }];
//}
//
//
//#pragma mark - Dedupe Evento
//
//- (void)deDupeEvento
//{
//    
//    __block BOOL foundDupe = NO;
//    
//    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
//        
//        
//        NSError *error = nil;
//        
//        NSFetchRequest *fr = [[NSFetchRequest alloc] initWithEntityName:@"Evento"];
//        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Evento"];
//        
//        [request setIncludesPendingChanges:NO]; //distinct has to go down to the db, not implemented for in memory filtering
//        [request setFetchBatchSize:1000]; //protect thy memory
//        
//        [request setReturnsDistinctResults:YES];
//        
//        [request setResultType:NSDictionaryResultType];
//        [request setPropertiesToFetch:[NSArray arrayWithObjects:@"titulo", @"dataInicio", @"dataFim", nil]];
//        
//        NSArray *countDictionaries = [localContext executeFetchRequest:request error:&error];
//        
//        NSPredicate *predicate;
//        
//        
//        for (NSDictionary *dict in countDictionaries) {
//            [fr setIncludesPendingChanges:NO];
//            
//            predicate = [NSPredicate predicateWithFormat:@"titulo = %@ AND dataInicio = %@ AND dataFim = %@", [dict objectForKey:@"titulo"], [dict objectForKey:@"dataInicio"], [dict objectForKey:@"dataFim"]];
//            
//            [fr setPredicate:predicate];
//            
//            NSArray *dupes = [localContext executeFetchRequest:fr error:&error];
//            
//            if (dupes.count > 1) {
//                foundDupe = YES;
//                
//                Evento *prevEvento = nil;
//                
//                for (Evento *evento in dupes) {
//                    if (prevEvento) {
//                        if (evento.version == nil) {
//                            [evento MR_deleteInContext:localContext];
//                            
//                        } else if (prevEvento.version == nil) {
//                            [prevEvento MR_deleteInContext:localContext];
//                            prevEvento = evento;
//                            
//                        } else if ([evento.version compare:prevEvento.version] == NSOrderedAscending) {
//                            [evento MR_deleteInContext:localContext];
//                            
//                        } else {
//                            [prevEvento MR_deleteInContext:localContext];
//                            prevEvento = evento;
//                        }
//                        
//                    } else {
//                        prevEvento = evento;
//                    }
//                }
//            }
//        }
//        
//        
//    } completion:^(BOOL success, NSError *error) {
//        
//        if (foundDupe) {
//            [Utils postEntitytNotification:@"Evento"];
//        }
//        
//    }];
//}
//
//
//@end
