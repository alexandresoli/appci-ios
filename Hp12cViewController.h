
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


@import AudioToolbox;

@interface Hp12cViewController : UIViewController <UIWebViewDelegate> {
    
    SystemSoundID audio_id, audio2_id;
    long click;
    long comma;
    IBOutlet UIWebView *html;
    int layout;
    int old_layout;
    long lock;
    BOOL iphone5;
}

- (void) playClick;
- (BOOL) webView:(UIWebView *)view shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;


@end

