

#import "Hp12cViewController.h"

@implementation Hp12cViewController

- (id)init
{
    self = [super initWithNibName:@"hp12c" bundle:nil];
    if (self != nil)
    {
        // Further initialization if needed
    }
    return self;
}



- (void)playClick
{
    if (click) {
        AudioServicesPlaySystemSound(audio_id);
    } else {
        AudioServicesPlaySystemSound(audio2_id);
    }
}

- (void)loadView {
    [super loadView];
    
    {
        NSURL *aurl = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"click" ofType:@"wav"] isDirectory:NO];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)(aurl), &audio_id);
    }
    
    {
        NSURL *aurl = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"clickoff" ofType:@"wav"] isDirectory:NO];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)(aurl), &audio2_id);
    }
    
    NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
    [prefs registerDefaults:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt: 1], @"click", nil]];
    [prefs registerDefaults:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt: 0], @"comma", nil]];
    click = [prefs integerForKey: @"click"];
    comma = [prefs integerForKey: @"comma"];
    lock = [prefs integerForKey: @"lock"];
}

- (BOOL) getSB: (BOOL) is_vertical {
	BOOL hide_bar = is_vertical;
	if (iphone5) {
		// iPhone5 proportions ask the opposite logic
		hide_bar = !hide_bar;
	}
    return hide_bar;
};


- (void) setComma {
    if (comma) {
        [html stringByEvaluatingJavaScriptFromString:@"ios_comma_on();"];
    } else {
        [html stringByEvaluatingJavaScriptFromString:@"ios_comma_off();"];
    }
}

- (void) loadPage
{
    [[UIApplication sharedApplication] setStatusBarHidden: [self getSB: (layout == 2)]];
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"index" ofType:@"html"] isDirectory:NO];
    [html loadRequest:[NSURLRequest requestWithURL:url]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    layout = UIDeviceOrientationIsPortrait(self.interfaceOrientation) ? 2 : 1;
    if (lock == 1) {
        layout = 1;
    } else if (lock == 2) {
        layout = 2;
    }
    old_layout = layout;
    
   // NSLog(@"Screen size: %f", [UIScreen mainScreen].bounds.size.height);
    iphone5 = [UIScreen mainScreen].bounds.size.height == 568;
    
    [self loadPage];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{    
    [self setComma];
    [self playClick];
    
}

- (BOOL) webView:(UIWebView *)view
shouldStartLoadWithRequest:(NSURLRequest *)request
  navigationType:(UIWebViewNavigationType)navigationType {
    
	NSString *requestString = [[request URL] absoluteString];
	NSArray *components = [requestString componentsSeparatedByString:@":"];
    
	if ([(NSString *)[components objectAtIndex:0] isEqualToString:@"touch12i"] &&
        [components count] > 1) {
		if ([(NSString *)[components objectAtIndex:1] isEqualToString:@"click"]) {
            

		} else if ([(NSString *)[components objectAtIndex:1] isEqualToString:@"tclick"]) {
            click = (click ? 0 : 1);
            NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
            [prefs setInteger: click forKey: @"click"];
		} else if ([(NSString *)[components objectAtIndex:1] isEqualToString:@"commaon"]) {
            comma = 1;
            NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
            [prefs setInteger: comma forKey: @"comma"];
		} else if ([(NSString *)[components objectAtIndex:1] isEqualToString:@"commaoff"]) {
            comma = 0;
            NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
            [prefs setInteger: comma forKey: @"comma"];
        }
        
        [self playClick];
 		return NO;
 	}
    
	return YES;
}

- (IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
