//
//  Macros.h
//  CoreDataLibraryApp
//
//  Created by Duncan Groenewald on 12/02/2014.
//  Copyright (c) 2014 OSSH Pty Ltd. All rights reserved.
//

#ifndef CoreDataLibraryApp_Macros_h
#define CoreDataLibraryApp_Macros_h

#define FLOG(format, ...)               NSLog(@"%@.%@ %@", [self class], NSStringFromSelector(_cmd), [NSString stringWithFormat:format, ##__VA_ARGS__])


#endif
