//
//  OSDocumentManager.h
//  Common
//
//  Created by Duncan Groenewald on 15/07/13.
//  Copyright (c) 2013 Duncan Groenewald. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Macros.h"
//#import "OSManagedDocument.h"
//#import "Utilities.h"
//#define FLOG(format, ...)               NSLog(@"%@.%@ %@", [self class], NSStringFromSelector(_cmd), [NSString stringWithFormat:format, ##__VA_ARGS__])
//#define FLOG(format, ...)

@interface FileRepresentation : NSObject

@property (nonatomic, readonly) NSString* filename;
@property (nonatomic, readonly) NSURL* url;
@property (nonatomic, retain)   NSURL* previewURL;
@property (nonatomic, readonly) NSString* fileDate;
@property (nonatomic, retain) NSString* downloadStatus;
@property (nonatomic, readonly) NSString* nameOfSavingComputer;
@property (nonatomic, readonly) NSNumber* percentDownloaded;
@property (retain) NSNumber* isDownloaded;
@property (retain) NSNumber* isDownloading;
@property  bool ready;

- (id)initWithFileName:(NSString*)filename url:(NSURL*)url;
- (id)initWithFileName:(NSString*)filename url:(NSURL*)url date:(NSString*)fileDate;
- (id)initWithFileName:(NSString*)filename url:(NSURL*)url percentDownloaded:(NSNumber*)percent;
- (NSString*)modifiedDate;

@end

// CloudManager performs file management asynchronously so we need callbacks to let the requester know when we are done
@protocol OSFileManagerProtocol <NSObject>
@required
-(void)fileHasBeenOpened:(NSURL*)file;
-(void)fileHasBeenCreated:(NSURL*)file;
-(void)fileHasBeenDeleted:(NSURL*)file;
-(void)fileHasBeenClosed:(NSURL*)file;
@end


@protocol OSCDStackManagerProtocol <NSObject>

- (void)showUpgradeViewer;
- (void)dismissUpgradeViewer;

@end


@interface OSCDStackManager : NSObject {
    
    BOOL _useICloudStorage;

    NSArray * _localURLs;
    NSArray * _iCloudURLs;
    NSArray* _localDocuments;
    NSMutableArray* _icloudBackupFileList;

    BOOL _isBusy;
    bool _isOpening;
    BOOL _isMigratingToICloud;
    BOOL _isMigratingToLocal;
    BOOL _isDownloadingBackup;
    NSURL *_migratingFileURL;
    BOOL _import_or_save;
    BOOL _load_seed_data;
    BOOL _icloud_file_exists;
    BOOL _has_checked_cloud;
    BOOL _isFirstInstall;
    BOOL _has_just_migrated;
    BOOL _icloud_container_available;
    BOOL _icloud_files_synced;
    BOOL _rebuildFromICloud;
    BOOL _use_old_model;
    BOOL _isUpgrading;

    
    bool _deletingDocument;
    bool _creatingDocument;
    bool _creatingNewFile;

    int _migrationCounter;
    
    int _job_counter;
    
    bool _clearFiles;  // set this to delete all files on the device on startup
    
    bool _storesChanging;
    
    UIAlertView* _storesUpdatingAlert;
    UIAlertView* _cloudChoiceAlert;
    UIAlertView* _cloudChangedAlert;
    UIAlertView* _migratingAlert;
    UIAlertView* _cloudMergeChoiceAlert;
    UIAlertView* _storeCantRestoreAlert;
    UIAlertView* _upgradingAlert;
    int _upgradingAlertResponse;
    UIAlertView* _movingBackToICloudAlert;
    int _movingBackToICloudAlertResponse;

    NSManagedObjectModel *_managedObjectModel;
    NSManagedObjectContext *_managedObjectContext;
    NSPersistentStoreCoordinator *_persistentStoreCoordinator;
    NSURL *_storeURL;

    NSMetadataQuery* _query;
    NSManagedObjectModel *_sourceModel;  // Used if an Cloud upgrade is required

}
@property (nonatomic,weak) id <OSCDStackManagerProtocol> delegate;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong, nonatomic) NSURL *persistentStoreName;
@property (strong, nonatomic) NSURL *storeURL;
@property (readonly) int loadJobCount;
@property (readonly) int deleteJobCount;


- (void)saveContext;
- (void)setVersion;
- (NSURL *)applicationDocumentsDirectory;
- (NSArray *)getData:(NSString*)entityName sortField:(NSString*)sortKey predicate:(NSPredicate*)predicate;

@property (readwrite, retain)   NSTimer *iCloudUpdateTimer;

@property (nonatomic)           BOOL isCloudEnabled;
@property (nonatomic)           BOOL deleteICloudFiles;
@property (nonatomic, readonly) NSURL* dataDirectoryURL;
@property (nonatomic, readonly) NSURL* documentsDirectoryURL;
@property (nonatomic)           NSString* ubiquityID;
@property (nonatomic, strong)   NSString *cloudPreferenceKey;
@property (nonatomic, strong)   NSString *cloudPreferenceSet;
@property (nonatomic, strong)   NSString *makeBackupPreferenceKey;
@property (nonatomic, strong)   NSString *loadInBackgroundPreferenceKey;
@property (nonatomic, strong)   NSString *ubiquityContainerKey;
@property (nonatomic, strong)   NSString *ubiquityIDToken;


+ (OSCDStackManager*)sharedManager;
- (void)setUbiquityID:(NSString*)ubiquityID;
- (void)checkUserICloudPreferenceAndSetupIfNecessary;
- (void)performApplicationWillEnterForegroundCheck;
- (void)loadDataInBackground;
- (void)deleteDataInBackground;
- (void)rebuildFromICloud;
- (void)setUpgradingResponse:(int) response;
- (bool)checkCDModelVersion;
- (void)setICloudPreference:(bool)pref;

- (NSString*)version;
- (NSString*)build;

- (NSURL*)iCloudCoreDataURL;
- (bool)canUseICloud;
- (bool)isICloudAvailable;
- (bool)isBusy;

- (void)saveDocument;
- (void)closeDocument;
- (void)postUIUpdateNotification;

- (NSDictionary*)icloudStoreOptions;
- (NSDictionary*)localStoreOptions;
- (bool)moveStoreToLocal;
- (bool)moveStoreToICloud;
- (bool)moveStoreFileToICloud:(NSURL*)fileURL delete:(bool)shouldDelete backup:(bool)shouldBackup;

- (NSArray*)listAllLocalDocuments;
- (NSArray*)listAllLocalBackupDocuments;
- (NSArray*)listAllICloudBackupDocuments;
- (bool)backupCurrentStore;
- (bool)backupCurrentStoreWithNoCheck;
- (void)deleteBackupFile:(NSURL*)fileURL;
- (void)saveFile:(NSURL*)fileURL;
- (NSString*)nameOfSavingComputer:(NSURL*)fileURL;

- (void)asynchronousTaskWithCompletion:(void (^)(void))completion;

- (void)asynchronousCopyToICloudWithCompletion:(NSURL*)fileURL completionHandler:(void (^)(void))completion;
- (void)asynchronousCopyFromICloudWithCompletion:(NSURL*)fileURL completionHandler:(void (^)(void))completion;
- (void)asynchronousRestoreFile:(NSURL*)fileURL completionHandler:(void (^)(void))completion;
- (void)downloadFile:(FileRepresentation*)fr;

- (BOOL) upgradeStep1;
- (BOOL) upgradeStep2;
- (BOOL) upgradeStep3;
- (BOOL) upgradeStep4;
- (BOOL) upgradeStep5;
- (BOOL) cancelUpgrade;


@end

NSString* const ICloudStateUpdatedNotification;

NSString* const OSFileDeletedNotification;
NSString* const OSFileCreatedNotification;
NSString* const OSFileClosedNotification;
NSString* const OSFilesUpdatedNotification;
NSString* const OSDataUpdatedNotification;
NSString* const OSStoreChangeNotification;
NSString* const OSJobStartedNotification;
NSString* const OSJobDoneNotification;
