#import "PersistentStack.h"

@interface PersistentStack ()

@property (nonatomic,strong,readwrite) NSManagedObjectContext* managedObjectContext;
@property (nonatomic,strong) NSURL* modelURL;
@property (nonatomic,strong) NSURL* storeURL;

@end



// delete from iCloud
//
//NSDictionary *iCloudOptions = [NSDictionary dictionaryWithObjectsAndKeys:
//                               @"Appci", NSPersistentStoreUbiquitousContentNameKey, nil];


//  BOOL b  = [NSPersistentStoreCoordinator removeUbiquitousContentAndPersistentStoreAtURL:self.storeURL options:iCloudOptions error:&error];

@implementation PersistentStack

- (id)initWithStoreURL:(NSURL*)storeURL modelURL:(NSURL*)modelURL
{
    self = [super init];
    if (self) {
        self.storeURL = storeURL;
        self.modelURL = modelURL;
        [self setupManagedObjectContext];
    }
    return self;
}

- (void)setupManagedObjectContext
{
    self.managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    self.managedObjectContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy;
    self.managedObjectContext.persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
    
    
    __weak NSPersistentStoreCoordinator *psc = self.managedObjectContext.persistentStoreCoordinator;
    
    // iCloud notification subscriptions
    NSNotificationCenter *dc = [NSNotificationCenter defaultCenter];
    [dc addObserver:self
           selector:@selector(storesWillChange:)
               name:NSPersistentStoreCoordinatorStoresWillChangeNotification
             object:psc];
    
    [dc addObserver:self
           selector:@selector(storesDidChange:)
               name:NSPersistentStoreCoordinatorStoresDidChangeNotification
             object:psc];
    
    [dc addObserver:self
           selector:@selector(persistentStoreDidImportUbiquitousContentChanges:)
               name:NSPersistentStoreDidImportUbiquitousContentChangesNotification
             object:psc];
    
    
    
#pragma mark - Migration check
    
    NSError* error;
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[self databasePath]] ) {
        
        NSLog(@"Migrando dados!");
        
        NSPersistentStoreCoordinator *migrationPSC = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
        
        // Open the existing local store using the original options
        id sourceStore = [migrationPSC addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:self.storeURL options:[self localStoreOptions] error:nil];
        
        id migrationSuccess = [migrationPSC migratePersistentStore:sourceStore toURL:[self icloudStoreURL] options:[self icloudStoreOptions] withType:NSSQLiteStoreType error:&error];
        
    
        if(migrationSuccess) {
            
            NSLog(@"Sucesso!");
            
            // Path
            NSURL *documentsURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:NULL];
            NSURL *sqliteURL =[documentsURL URLByAppendingPathComponent:@"BrEstate.sqlite"];
            NSURL *shmURL =[documentsURL URLByAppendingPathComponent:@"BrEstate.sqlite-shm"];
            NSURL *wallURL =[documentsURL URLByAppendingPathComponent:@"BrEstate.sqlite-wal"];
            
            
            // delete the old local database
            [[NSFileManager defaultManager] removeItemAtPath:sqliteURL.path error:&error]; // sqlite
            
            // shm
            if ([[NSFileManager defaultManager] fileExistsAtPath:shmURL.path]) {
                [[NSFileManager defaultManager] removeItemAtPath:shmURL.path error:&error];
            }
            
            // wall
            if([[NSFileManager defaultManager] fileExistsAtPath:wallURL.path]) {
                [[NSFileManager defaultManager] removeItemAtPath:wallURL.path error:&error];
            }
            
            
            
            //      NSLog(@"%@",migrationSuccess);
        }
        

        
    }
        
        
        NSURL *url = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Appci-iCloud.sqlite"];
        
        //NSPersistentStore * iCloudStore =
    [psc addPersistentStoreWithType:NSSQLiteStoreType
                                                            configuration:nil
                                                                      URL:url
                                                                  options:[self icloudStoreOptions]
                                                                    error:&error];
//        NSLog(@"%@",[iCloudStore URL]);
        
    NSUbiquitousKeyValueStore* store = [NSUbiquitousKeyValueStore defaultStore];
    [store setBool:YES forKey:@"iCloud-Enabled"];
    [store synchronize];

//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(updateKVStoreItems:)
//                                                 name:NSUbiquitousKeyValueStoreDidChangeExternallyNotification
//                                               object:store];

}



- (NSURL *)icloudStoreURL {
    return [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Appci-iCloud.sqlite"];
}

- (NSDictionary*)localStoreOptions {
    return @{NSMigratePersistentStoresAutomaticallyOption:@YES,
             NSInferMappingModelAutomaticallyOption:@YES,
             NSSQLitePragmasOption:@{ @"journal_mode" : @"DELETE" }};
}

- (NSDictionary *)icloudStoreOptions {
    

     NSDictionary *options = @{NSPersistentStoreUbiquitousContentNameKey:@"Appci",
                    NSMigratePersistentStoresAutomaticallyOption:@YES,
                    NSInferMappingModelAutomaticallyOption:@YES,
                    NSSQLitePragmasOption:@{ @"journal_mode" : @"DELETE" }};
    
    return options;
}


- (NSManagedObjectModel*)managedObjectModel
{
    return [[NSManagedObjectModel alloc] initWithContentsOfURL:self.modelURL];
}

// Subscribe to NSPersistentStoreDidImportUbiquitousContentChangesNotification
- (void)persistentStoreDidImportUbiquitousContentChanges:(NSNotification*)note
{
//    NSLog(@"%s", __PRETTY_FUNCTION__);
//    NSLog(@"%@", note.userInfo.description);
    
    NSManagedObjectContext *moc = self.managedObjectContext;
    [moc performBlock:^{
        [moc mergeChangesFromContextDidSaveNotification:note];
        
        // you may want to post a notification here so that which ever part of your app
        // needs to can react appropriately to what was merged.
        // An exmaple of how to iterate over what was merged follows, although I wouldn't
        // recommend doing it here. Better handle it in a delegate or use notifications.
        // Note that the notification contains NSManagedObjectIDs
        // and not NSManagedObjects.
//        NSDictionary *changes = note.userInfo;
//        NSMutableSet *allChanges = [NSMutableSet new];
//        [allChanges unionSet:changes[NSInsertedObjectsKey]];
//        [allChanges unionSet:changes[NSUpdatedObjectsKey]];
//        [allChanges unionSet:changes[NSDeletedObjectsKey]];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:NSPersistentStoreDidImportUbiquitousContentChangesNotification object:note userInfo:nil];
        });
        
//        for (NSManagedObjectID *objID in allChanges) {
//            // do whatever you need to with the NSManagedObjectID
//            // you can retrieve the object from with [moc objectWithID:objID]
//        }
        
    }];
}

// Subscribe to NSPersistentStoreCoordinatorStoresWillChangeNotification
// most likely to be called if the user enables / disables iCloud
// (either globally, or just for your app) or if the user changes
// iCloud accounts.
- (void)storesWillChange:(NSNotification *)note {
    NSManagedObjectContext *moc = self.managedObjectContext;
    [moc performBlockAndWait:^{
        NSError *error = nil;
        if ([moc hasChanges]) {
            [moc save:&error];
        }
        
        [moc reset];
    }];
    
    // now reset your UI to be prepared for a totally different
    // set of data (eg, popToRootViewControllerAnimated:)
    // but don't load any new data yet.
}

// Subscribe to NSPersistentStoreCoordinatorStoresDidChangeNotification
- (void)storesDidChange:(NSNotification *)note {
    // here is when you can refresh your UI and
    // load new data from the new store
}


- (NSString *)databasePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"BrEstate.sqlite"];
    
    return filePath;
}


- (NSURL *)applicationDocumentsDirectory
{
    NSURL* documentsDirectory = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:NULL];
    return documentsDirectory;
}

@end