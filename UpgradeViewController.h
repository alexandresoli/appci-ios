//
//  UpgradeViewController.h
//  CoreDataLibraryApp
//
//  Created by Duncan Groenewald on 28/02/2014.
//  Copyright (c) 2014 OSSH Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@class AppDelegate;

@protocol UpgradeViewControllerProtocol <NSObject>

- (void)dismissUpgradeViewer;

@end

@interface UpgradeViewController : UIViewController {
    IBOutlet UILabel *message;
    IBOutlet UILabel *step1;
    IBOutlet UILabel *step2;
    IBOutlet UILabel *step3;
    IBOutlet UILabel *step4;
    IBOutlet UILabel *step5;
    IBOutlet UIButton *cancelButton;
    IBOutlet UIButton *nextButton;
    
    int _step;
    
    UIColor *busyColor;
    UIColor *redColor;
    UIColor *greenColor;
    
    
}

@property (nonatomic,weak) AppDelegate *ad;

@property (nonatomic,weak) id <UpgradeViewControllerProtocol> delegate;

@end
