//
//  UpgradeViewController.m
//  CoreDataLibraryApp
//
//  Created by Duncan Groenewald on 28/02/2014.
//  Copyright (c) 2014 OSSH Pty Ltd. All rights reserved.
//

#import "UpgradeViewController.h"
#import "OSCDStackManager.h"

@interface UpgradeViewController ()

@end

@implementation UpgradeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    FLOG(@" called");
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{ FLOG(@" called");
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _step = 0;
    
    redColor = step1.backgroundColor;
    busyColor = step2.backgroundColor;
    greenColor = step3.backgroundColor;
    
    step1.backgroundColor = redColor;
    step2.backgroundColor = redColor;
    step3.backgroundColor = redColor;
    step4.backgroundColor = redColor;
    step5.backgroundColor = redColor;
    
    cancelButton.hidden = YES;
    
    message.text = @"You iCloud data needs to be upgraded, press Next to move data store to local.";

}
- (void)step1 {
    FLOG(@" called");

    // Migrate to local
    nextButton.enabled = NO;
    step1.backgroundColor = busyColor;
    
    if ([[OSCDStackManager sharedManager] upgradeStep1]) {
    
        step1.backgroundColor = greenColor;
        nextButton.enabled = YES;
        message.text = @"Data moved to local, press Next to remove iCloud store or Cancel to use local store.";
        cancelButton.hidden = NO;
    } else {
        message.text = @"Error moving iCloud data to local store, can't continue.";
        nextButton.enabled = YES;
        cancelButton.hidden = NO;
        step1.backgroundColor = redColor;
    }

}
- (void)step2 {
    FLOG(@" called");
    // Remove iCloud store
    cancelButton.hidden = YES;
    nextButton.enabled = NO;
    step2.backgroundColor = busyColor;
    
    if ([[OSCDStackManager sharedManager] upgradeStep2]) {
    
        step2.backgroundColor = greenColor;
        nextButton.enabled = YES;
        message.text = @"Store removed from iCloud, press Next to upgrade or Cancel to upgrade and use local store.";
        cancelButton.hidden = NO;
    } else {
        message.text = @"Error removing iCloud store, will continue with local upgrade.";
        nextButton.enabled = YES;
        cancelButton.hidden = YES;
        step2.backgroundColor = redColor;
        
    }
}
- (void)step3 {
    FLOG(@" called");
    // Upgrade
    cancelButton.hidden = YES;
    nextButton.enabled = NO;
    step3.backgroundColor = busyColor;
    
    if ([[OSCDStackManager sharedManager] upgradeStep3]) {
    
        step3.backgroundColor = greenColor;
        nextButton.enabled = YES;
        message.text = @"Data upgraded, press Next to move data store to iCloud or Cancel to use local store.";
        cancelButton.hidden = NO;
    } else {
        message.text = @"Error removing iCloud store, will continue with local upgrade.";
        nextButton.enabled = YES;
        cancelButton.hidden = YES;
        step3.backgroundColor = redColor;
        
    }
}
- (void)step4 {
    FLOG(@" called");
    // Move to iCloud
    cancelButton.hidden = YES;
    nextButton.enabled = NO;
    step4.backgroundColor = busyColor;
    
    if ([[OSCDStackManager sharedManager] upgradeStep4]) {
    
        step4.backgroundColor = greenColor;
        message.text = @"Data store moved to iCloud, press Next to continue using the App.";
        nextButton.enabled = YES;
    } else {
        message.text = @"Error data to iCloud, press Next to continue using the local store.";
        nextButton.enabled = YES;
        cancelButton.hidden = YES;
        step4.backgroundColor = redColor;
    }
}
- (void)step5 {
    FLOG(@" called");
    // Dismiss
    [[OSCDStackManager sharedManager] upgradeStep5];
    [[OSCDStackManager sharedManager] setUpgradingResponse:1];
  //  [self.ad openMainViews];
}
- (IBAction)next:(id)sender {
    _step++;
    switch (_step) {
        case 1:
            [self step1];
            break;
            
        case 2:
            [self step2];
            break;

        case 3:
            [self step3];
            break;

        case 4:
            [self step4];
            break;

        case 5:
            [self step5];
            break;

        default:
            break;
    }
    
}
- (IBAction)cancel:(id)sender {
    FLOG(@" called");
    if (_step == 0) {
        exit(0);
    }
    [[OSCDStackManager sharedManager] cancelUpgrade];
    [[OSCDStackManager sharedManager] setUpgradingResponse:1];
//    [self.ad openMainViews];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
